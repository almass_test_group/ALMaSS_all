/*
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Newt.cpp
\brief <B>The source code for newt lifestages</B>
*
Version of  1 January 2016 \n
By Chris J. Topping \n \n
*/

#include <string>
#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Newt/Newt.h"
#include "../Newt/Newt_Population_Manager.h"

extern MapErrorMsg *g_msg;
extern const int Vector_x[ 8 ];
extern const int Vector_y[ 8 ];

using namespace std;

/** \brief  Based on Baker (1998), the start size is the snout-vent length averaging 42.3mm after metamorphosis */
static CfgFloat cfg_newtJuvenileStartSize( "NEWT_JUVESTARTSIZE", CFG_CUSTOM, 42.3 );


/*________________________________________________________________________________________________________________________________*/
/*______________________________________________________Newt_Base_________________________________________________________________*/
/*________________________________________________________________________________________________________________________________*/

Newt_Base::Newt_Base(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib) : TAnimal(a_x, a_y, a_L) {
	// Assign the pointer to the population manager
	Init(a_pond, a_NPM, a_reproinhib);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Base::ReInit (int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib) {
	TAnimal::ReinitialiseObject(a_x, a_y, a_L);
	// Assign the pointer to the population manager
	Init(a_pond, a_NPM, a_reproinhib);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Base::Init(vector<unsigned> a_pond, Newt_Population_Manager* a_NPM, bool a_reproinhib) {
	m_OurPopulationManager = a_NPM;
	m_CurrentNewtState = toNewts_InitialState;
	m_Age = 0;
	m_pondlist = a_pond; // the home pond is added here
	m_body_burden = 0.0;
	m_reproductiveinhibition = a_reproinhib;
}

Newt_Base::~Newt_Base(void)
{
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Base::st_Dying( void ) {
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
}
//---------------------------------------------------------------------------------------------------------------------------------


/*________________________________________________________________________________________________________________________________*/
/*______________________________________________________Newt_Egg__________________________________________________________________*/
/*________________________________________________________________________________________________________________________________*/

Newt_Egg::Newt_Egg( int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib ) : Newt_Base( a_x, a_y, a_pond, a_L, a_NPM, a_reproinhib )
{
	Init();
}
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Egg::~Newt_Egg(void)
{
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Egg::ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib) {
	Newt_Base::ReInit(a_x, a_y, a_pond, a_L, a_NPM, a_reproinhib);
	Init();
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Egg::Init()
{
	m_AgeDegrees = 0;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Egg::Step(void) {
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentNewtState) {
		case toNewts_InitialState: // Initial state always starts with develop
			m_CurrentNewtState = toNewts_Develop;
			break;
		case toNewts_Develop:
			m_CurrentNewtState = st_Develop(); // Will return movement or die
			m_StepDone = true;
			break;
		case toNewts_NextStage:
			st_NextStage();
			break;
		case toNewts_Die:
			st_Dying(); // No return
			m_StepDone = true;
			break;
		case toNewts_Remove:
			m_CurrentStateNo = -1;
			m_StepDone = true;
			break;
		default:
			m_OurLandscape->Warn( "Newt_egg::Step()", " unknown state" );
			exit( 1 );
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Egg::st_Develop( void ) {
	/**
	* Determines whether the egg sucumbs to daily mortality using a probability test against a daily probability (input parameter), if so return toNewts_Die state. \n
	* Next if there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,
	* then pesticide at this location is determined and any intake stored. If the body-burden is greater than the threshold for effects, then
	* #InternalPesticideHandlingAndResponse is called and the impacts determined.
	* If the egg survives:
	* - The age is incremented
	* - The sum of day degrees is increased by the mean temperature today
	* - If the total sum of day degrees exceeds the parameter value held in m_EggDevelopmentThreshold, the egg should hatch and the state toNewts_NextStage is returned
	* - If not fully developed the toNewts_Develop stage is returned and the process will be repeated the following day
	*
	*/
	if (g_rand_uni() < m_EggMortalityChance) return toNewts_Die;
	// We need to test pesticide if m_test_pesticide is set
	if (m_test_pesticide_egg) {
		// Eliminate some/all of yesterdays body burden of PPP by multiplying by 1-the daily elimination rate
		m_body_burden *= m_EggPPPElimRate;
		// Pick up the pesticide to add to the body burden
		// This differs from the usual way of doing things because we assume mixing in pond water, so ask the pond.
		m_body_burden += m_OurLandscape->SupplyPondPesticide(m_pondlist[0]);
		InternalPesticideHandlingAndResponse();
		if (m_CurrentNewtState == toNewts_Die) return toNewts_Die;
	}
	m_Age++;
	m_AgeDegrees += m_OurPopulationManager->GetEggDDRateTransformation( m_OurLandscape->SupplyTemp());
	if (m_AgeDegrees > m_EggDevelopmentDDTotal) return toNewts_NextStage;
	return toNewts_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Egg::st_NextStage( void ) {
	/**
	* Creates a new Newt_Larva object and passes the data from the young to it, then signals young object removal.
	*/
	struct_Newt sN;
	sN.NPM = m_OurPopulationManager;
	sN.L = m_OurLandscape;
	sN.age = m_Age;
	sN.x = m_Location_x;
	sN.y = m_Location_y;
	sN.pondrefs = m_pondlist;
	sN.reproinhib = m_reproductiveinhibition;
	m_OurPopulationManager->CreateObjects( tton_Larva, NULL, &sN,1  ); // 
	m_CurrentStateNo = -1;
	m_StepDone = true;
	return toNewts_Remove; // Not necessary, but for neatness
}
//---------------------------------------------------------------------------------------------------------------------------------

/*________________________________________________________________________________________________________________________________*/
/*______________________________________________________Newt_Larva______________________________________________________________*/
/*________________________________________________________________________________________________________________________________*/

Newt_Larva::Newt_Larva(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) : Newt_Egg(a_x, a_y, a_pond, a_L, a_NPM, a_reproinhib)
{
	/**
	* Newt_Larva constructor Assigns default values to specific larva attributes and passes the age of the larva from the age of the hatching egg.
	*/
	Init(a_age);
}
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Larva::~Newt_Larva(void)
{
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Larva::ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) {
	Newt_Egg::ReInit(a_x, a_y, a_pond, a_L, a_NPM, a_reproinhib);
	Init(a_age);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Larva::Init(int a_age)
{
	m_AgeDegrees = 0;
	m_LarvalSize = 1.0;
	m_Age = a_age;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Larva::st_Develop( void ) {
	/**
	* Determines whether the larva sucumbs to daily mortality using a probability test against a daily probability (input parameter), 
	* if so return toNewts_Die state. \n
	* Next there is a test for the availability of food in the pond. If there is not enough food available then they also die.\n
	* Then if there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,
	* then pesticide at this location is determined and any intake stored. If the body-burden is greater than the threshold for effects, then
	* #InternalPesticideHandlingAndResponse is called and the impacts determined.
	* If the larva survives:
	* - The age is incremented
	* - The number days in the larval stage is incremented
	* - We assume that the larva eats Newt_Base::m_LarvalFoodProportion multiplied by its age in g of food per day. If this is not available the larva dies. See the larval food model is controlled
	* by the Newt_Population_Manager and accessed through Newt_Population_Manager::SubtractLarvalFood
	* - The current model assumes a linear increase in size up to cfg_newtJuvenileStartSize from the starting length of 1mm
	* - The larva matures when it reaches either a specified size (age) or a the day-degrees needed for development, assuming the larvae is above the lower limit of size. Currently upper and 
	* lower size limit are the same, resulting in a age-specific maturation date.
	*
	*/
	if (g_rand_uni() < m_LarvaMortalityChance) return toNewts_Die;
	if (!m_OurLandscape->SubtractPondLarvalFood(m_Age * m_LarvalFoodProportion, m_pondlist[0])) return toNewts_Die;
	// We need to test pesticide if m_test_pesticide is set
	if (m_test_pesticide_larva) {
		// Eliminate some/all of yesterdays body burden of PPP by multiplying by 1-the daily elimination rate
		m_body_burden *= m_LarvaPPPElimRate;
		// Pick up the pesticide to add to the body burden. 
		// This differs from the usual way of doing things because we assume mixing in pond water, so ask the pond.
		m_body_burden += m_OurLandscape->SupplyPondPesticide(m_pondlist[0]);
		// Test against the threshold level for adult to determine if an effect needs to be tested for
		InternalPesticideHandlingAndResponse();
		if (m_CurrentNewtState == toNewts_Die) return toNewts_Die;
	}
	m_Age++;
	m_AgeDegrees++;
	m_LarvalSize += m_NewtLarvaDailyGrowthIncrement;
	if (m_LarvalSize > m_LarvaDevelopmentUpperSz ) return toNewts_NextStage;
	if (m_AgeDegrees > m_LarvaDevelopmentTime) if (m_LarvalSize > m_LarvaDevelopmentLowerSz) return toNewts_NextStage; else return toNewts_Die;
	return toNewts_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Larva::Step( void ) {
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentNewtState) {
		case toNewts_InitialState: // Initial state always starts with develop
			m_CurrentNewtState = toNewts_Develop;
			break;
		case toNewts_Develop:
			m_CurrentNewtState = st_Develop(); // Will return movement or die
			m_StepDone = true;
			break;
		case toNewts_NextStage:
			st_NextStage();
			break;
		case toNewts_Die:
			st_Dying(); // No return
			m_StepDone = true;
			break;
		case toNewts_Remove:
			m_CurrentStateNo = -1;
			m_StepDone = true;
			break;
		default:
			m_OurLandscape->Warn( "Newt_Larva::Step()", " unknown state" );
			exit( 1 );
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Larva::st_NextStage( void ) {
	/**
	* Creates a new Newt_Juvenile object and passes the data from the young to it, then signals young object removal.
	*/
#ifdef __RECORDNEWTMETAMORPHOSIS
	m_OurPopulationManager->RecordMetamorphosis(m_Age);
#endif
	struct_Newt sN;
	sN.NPM = m_OurPopulationManager;
	sN.L = m_OurLandscape;
	sN.age = m_Age;
	sN.x = m_Location_x;
	sN.y = m_Location_y;
	sN.reproinhib = m_reproductiveinhibition;
	sN.pondrefs = m_pondlist;
	m_OurPopulationManager->CreateObjects( tton_Juvenile, NULL, &sN, 1 ); // 
	m_CurrentStateNo = -1;
	m_StepDone = true;
	return toNewts_Remove; // Not necessary, but for neatness
}
//---------------------------------------------------------------------------------------------------------------------------------

/*________________________________________________________________________________________________________________________________*/
/*______________________________________________________Newt_Juvenile_____________________________________________________________*/
/*________________________________________________________________________________________________________________________________*/

Newt_Juvenile::Newt_Juvenile(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) : Newt_Base(a_x, a_y, a_pond, a_L, a_NPM, a_reproinhib)
{
	/**
	* Newt_Juvenile constructor Assigns default values to specific juvenile attributes and passes the age of the juvenile from the age of the larva.
	*/
	m_weight = cfg_newtJuvenileStartSize.value();
	m_OurVector = unsigned (floor(g_rand_uni()*7.0 +0.5));
	m_forcedisperse = false;
	m_Age = a_age;
}
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Juvenile::~Newt_Juvenile(void)
{
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Juvenile::ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) {
	Newt_Base::ReInit(a_x, a_y, a_pond, a_L, a_NPM, a_reproinhib);
	m_weight = cfg_newtJuvenileStartSize.value();
	m_OurVector = unsigned(floor(g_rand_uni()*7.0 + 0.5));
	m_forcedisperse = false;
	m_Age = a_age;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Juvenile::Step(void) {
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentNewtState) {
		case toNewts_InitialState: // Initial state always starts with develop
			m_CurrentNewtState = toNewts_Develop;
			break;
		case toNewts_Develop:
			m_CurrentNewtState = st_Develop(); // Will return evaluate location, overwinter, next stage or die
			break;
		case toNewts_Dispersal:
			m_CurrentNewtState = st_Disperse(); 
			break;
		case toNewts_EvaluateLocation:
			m_CurrentNewtState = st_EvaluateHabitat();
			m_StepDone = true;
			break;
		case toNewts_Overwinter:
			m_CurrentNewtState = st_Overwinter();
			m_StepDone = true;
			break;
		case toNewts_NextStage:
			st_NextStage();
			break;
		case toNewts_Die:
			st_Dying(); // No return
			m_StepDone = true;
			break;
		case toNewts_Remove:
			m_CurrentStateNo = -1;
			m_StepDone = true;
			break;
		default:
			m_OurLandscape->Warn( "Newt_Juvenile::Step()", " unknown state" );
			exit( 1 );
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Juvenile::st_NextStage( void ) {
	/**
	* Creates a new Newt_Male or Newt_Female object and passes the data from the young to it, then signals young object removal.
	*/
	struct_Newt sN;
	sN.NPM = m_OurPopulationManager;
	sN.L = m_OurLandscape;
	sN.age = m_Age;
	sN.x = m_Location_x;
	sN.y = m_Location_y;
	sN.pondrefs = m_pondlist;
	sN.weight = m_weight;
	sN.reproinhib = m_reproductiveinhibition;
	if (g_rand_uni()<0.5) m_OurPopulationManager->CreateObjects( tton_Male, NULL, &sN, 1 ); 
	else m_OurPopulationManager->CreateObjects( tton_Female, NULL, &sN, 1 ); 
	m_CurrentStateNo = -1;
	m_StepDone = true;
	return toNewts_Remove; // Not necessary, but for neatness
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Juvenile::st_Develop( void ) {
	/**
	* First determines whether the juvenile sucumbs to daily mortality, if so return toNewts_Die state. \n
	* Next if there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,
	* then pesticide at this location is determined and any intake stored. If the body-burden is greater than the threshold for effects, then
	* #InternalPesticideHandlingAndResponse is called and the impacts determined.
	* If the juvenile survives:
	* - The age is incremented
	* - Length is added as a daily increment based on an input parameter
	* - If the total length exceeds the parameter value held in #m_JuvenileDevelopmentSize, the juvenile should mature and the state toNewts_NextStage is returned
	* - the behavioural state st_EvaluateHabitat is called following st_Develop, which leads to either a dispersal state or return to this state on the next day.
	* - If the newt is in pond then dispersal if forced.
	*
	*/
	if (g_rand_uni() < m_JuvenileMortalityChance) return toNewts_Die;
	// We need to test pesticide if m_test_pesticide is set
	if (m_test_pesticide_terrestrial) {
		// Eliminate some/all of yesterdays body burden of PPP by multiplying by 1-the daily elimination rate
		m_body_burden *= m_JuvenilePPPElimRate;
		// Pick up the pesticide to add to the body burden
		m_body_burden += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
		// Test against the threshold level for adult to determine if an effect needs to be tested for
		InternalPesticideHandlingAndResponse();
		if (m_CurrentNewtState == toNewts_Die) return toNewts_Die;
	}
	m_Age++; // we get older but not heavier if cold
	if (m_OurLandscape->SupplyTempPeriod( g_date->Date(), 5 ) < m_NewtDormancyTemperature) {
		return toNewts_Overwinter;
	}	
	m_weight += m_JuvenileDailyWeightGain;
	if (m_weight > m_JuvenileDevelopmentSize) return toNewts_NextStage;
	if (m_InPond) m_forcedisperse = true; 
	return toNewts_EvaluateLocation;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Juvenile::st_Disperse( void ) {
	/**
	* The newt can only move around when the humidity is high. Here we assume this is related to the rainfall and temperature of the preceding days.
	* If the humidity is high enough then the newt moves otherwise it stays where it is and returns toNewts_Develop.\n
	* The newt moves around using a random walk. At each step it evaluates if the habitat is walks onto is legal (if not it does not move), 
	* and if a pond it remembers this. The new pond is added to the m_pondlist vector.\n
	* The rate of the random walk is set by a input parameter stored in the static attribute m_newtwalkspeed, and is a uniform random distribution between 0 and this value. 
	* During the dispersal roads have associated mortality risks. These are flagged and probability tests taken.\n
	* If killed in dispersal, the current state will be changed to die by MoveTo->NewtDoWalking, so if this has been altered from toNewts_Dispersal then return the new state, 
	* otherwise returns toNewts_Develop.
	*/
	TTypeDirectedWalk  directedness = directedwalk_random;
	if (m_forcedisperse) directedness = directedwalk_high;
	double temp = m_OurLandscape->SupplyTempPeriod(g_date->Date(), 5);
	double humidity = m_OurLandscape->SupplyHumidity();
	if ((temp > m_NewtDormancyTemperature) && (humidity > m_NewtDormancyHumidity))
	{
		MoveTo(directedness, m_newtwalkstepsize, int(m_newtwalkspeed*g_rand_uni()));
		if (m_CurrentNewtState == toNewts_Die) return toNewts_Die;
	}
	return toNewts_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Juvenile::st_EvaluateHabitat(void) {
	/**
	* Assesses where it is stood. If the habitat is good it sets the dispersal chance to #m_goodhabitatdispersalprob otherwise to #m_poorhabitatdispersalprob
	* It then takes a prbobability test to determine whether to dispserse. Will return disperse or develop states depending on the result of the test.
	*/
	TTypesOfLandscapeElement tole = m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y);
	double chance = m_goodhabitatdispersalprob;
	if (tole == tole_Pond)
	{
		m_forcedisperse = true;
		return toNewts_Dispersal;
	}
	else
	{
		m_forcedisperse = false;
		switch (tole) {
		case tole_Marsh:
		case tole_NaturalGrassWet:
			break;
		default:
			chance = m_poorhabitatdispersalprob;
		}
		if (g_rand_uni() < chance) return toNewts_Dispersal;
	}
	return toNewts_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Juvenile::st_Overwinter( void ) {
	if (g_rand_uni() < m_JuvenileMortalityChance) return toNewts_Die;
	if (m_OurLandscape->SupplyTempPeriod( g_date->Date(), 5 )> m_NewtDormancyTemperature) {
		return toNewts_Develop;
	}
	return toNewts_Overwinter;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Juvenile::MoveTo(TTypeDirectedWalk a_directedness, int a_stepsize, int a_steps ) {
	/**
	* This is part of the movement method for all newts. It checks whether there is any need to consider wrap-around and then calls movement the appropriate 
	* number of times passing a_vector, which is used to determine the direction (if any), and a_directedness, which determines the degree of directed movement:\n
	* - directedwalk_totally = 1,
	* - directedwalk_high = 3,
	* - directedwalk_medium = 5,
	* - directedwalk_low = 7,
	* - directedwalk_random = 8
	*/
	int direction = int( g_rand_uni()*a_directedness + m_OurVector ) & 0x07;
	int MaxPossDistance = a_stepsize*a_steps;
	if ((m_Location_x - MaxPossDistance < 0) || (m_Location_x + MaxPossDistance >= m_SimW) || (m_Location_y - MaxPossDistance < 0) || (m_Location_y + MaxPossDistance >= m_SimH)) {
		// Need correct coords
		// Make sure that the coords can't become -ve
		int vx = m_Location_x + m_SimW;
		int vy = m_Location_y + m_SimH;
		NewtDoWalkingCorrect( direction, a_stepsize, a_steps, vx, vy );
		m_Location_x = vx % m_SimW;
		m_Location_y = vy % m_SimH;
	}
	else {
		// Don't need correct coords
		int vx = m_Location_x;
		int vy = m_Location_y;
		NewtDoWalking( direction, a_stepsize, a_steps, vx, vy );
		// alter the newts location (& correct coords)
		m_Location_x = vx;
		m_Location_y = vy;
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Juvenile::NewtDoWalking( int &a_vector, int a_stepsize, int a_steps, int &vx, int &vy ) {
	/**
	* This method does the actual stepping - there is no look ahead here, so steps are taken one at a time based on the habitat type and vector given.
	* This is a very time critical method.
	* The relative locations of each direction compared to the present x,y, are stored in StepsX and StepsY. These locations use the step size.
	* For the newt we only care whether they can walk there, and if we are on a road. So it will rarely necessary to test more than a single location, hence
	* this implementation differs from e.g. vole, that makes decisions based on habitat choice. For the newt this is more based on whether to move and how much than
	* active choices.
	* \param a_vector [in] The initial direction to move in.
	* \param a_steps [in] The number of steps to take
	* \param a_stepsize [in] The size of each step in m - this effectively allows the newt to skip over testing each and every step
	* \param vx [in][out] The starting x-coordinate for the movement
	* \param vy [in][out] The starting y-coordinate for the movement
	*
	* If a newt steps onto a road then the chance of mortality is dependent on an external parameter stored in #m_roadmortalityprob This value is multiplied by the return
	* value of #NewtMoveQuality which will increase with the size of the road (or could be linked to traffic information in the future).
	*/
	int StepsX;
	int StepsY;
	int testquality;
	TTypesOfLandscapeElement tole;

	StepsX = Vector_x[ a_vector ] * a_stepsize;
	StepsY = Vector_y[ a_vector ] * a_stepsize;
	for (int i = 0; i < a_steps; i++) {
		// test the squares at Vector, Vector+1+2, Vector-1-2
		// They have either a quality or are inaccessible (water,buildings)
		// if can go to one of these squares then pick the best one
		// if all or some are equal then take a random pick.
		// if all are 'bad' then add or subtract one from vector and try again
		testquality = NewtMoveQuality( (vx + StepsX), (vy + StepsY), tole );
		if (testquality == -1) {
			// can't go anywhere so change the vector
			if (random( 2 )) ++a_vector; else (--a_vector);
			a_vector &= 0x07;
		}
		else {
			if (testquality > 0) {
				// On a dangerous habitat. take a mortality test
				if (g_rand_uni() < m_roadmortalityprob * testquality) {
					m_CurrentNewtState = toNewts_Die;
					return;
				}
			}
			// change co-ordinates
			vx += StepsX;
			vy += StepsY;
			m_CurrentHabitat = tole;
		}
	}
}
//----------------------------------------------------------------------------------------------------------------------------------

void Newt_Juvenile::NewtDoWalkingCorrect( int &a_vector, int a_stepsize, int a_steps, int &vx, int &vy ) {
	/**
	* This method does the actual stepping. It is identical to NewtDoWalking except it corrects for warparound issues.
	* There is no look ahead here, so steps are taken one at a time based on the habitat type and vector given.
	* This is a very time critical method.
	* The relative locations of each direction compared to the present x,y, are stored in StepsX and StepsY. These locations use the step size.
	* For the newt we only care whether they can walk there, and if we are on a road. So it will rarely necessary to test more than a single location, hence
	* this implementation differs from e.g. vole, that makes decisions based on habitat choice. For the newt this is more based on whether to move and how much than
	* active choices.
	* \param a_vector [in] The initial direction to move in.
	* \param a_steps [in] The number of steps to take
	* \param a_stepsize [in] The size of each step in m - this effectively allows the newt to skip over testing each and every step
	* \param vx [in][out] The starting x-coordinate for the movement
	* \param vy [in][out] The starting y-coordinate for the movement
	*
	* If a newt steps onto a road then the chance of mortality is dependent on an external parameter stored in #m_roadmortalityprob This value is multiplied by the return
	* value of #NewtMoveQuality which will increase with the size of the road (or could be linked to traffic information in the future).
	*/
	int StepsX;
	int StepsY;
	int testquality;
	TTypesOfLandscapeElement tole;
	StepsX = Vector_x[ a_vector ] * a_stepsize + m_SimW;
	StepsY = Vector_x[ a_vector ] * a_stepsize + m_SimH;
	for (int i = 0; i < a_steps; i++) {
		// test the squares at Vector, Vector+1+2, Vector-1-2
		// They have either a quality or are inaccessible (water,buildings)
		// if can go to one of these squares then pick the best one
		// if all or some are equal then take a random pick.
		// if all are 'bad' then add or subtract one from vector and try again
		testquality = NewtMoveQuality( (vx + StepsX) % m_SimW, (vy + StepsY) % m_SimH, tole );
		if (testquality == -1) {
			// can't go anywhere so change the vector
			if (random( 2 )) ++a_vector; else (--a_vector);
			a_vector &= 0x07;
		}
		else {
			if (testquality > 0) {
				// On a dangerous habitat. take a mortality test
				if (g_rand_uni() < m_roadmortalityprob) {
					m_CurrentNewtState = toNewts_Die;
					return;
				}
			}
			// change co-ordinates and save the current habitat type we are in
			vx += (m_SimW + StepsX) % m_SimW;
			vy += (m_SimH + StepsY) % m_SimH;
			m_CurrentHabitat = tole;
		}
	}
}
//----------------------------------------------------------------------------------------------------------------------------------

int Newt_Juvenile::NewtMoveQuality( int a_x, int a_y, TTypesOfLandscapeElement & a_tole) {
	/**
	* This is where the movement onto a habitat is evaluted and either returns a yes can move here, a n cannot move hereo, or a yes with a mortality chance.
	* If the habitat is breeding habitat this is recorded otherwise
	*/
	unsigned poly_index = m_OurLandscape->SupplyPolyRefIndex( a_x, a_y );
	a_tole = m_OurLandscape->SupplyElementTypeFromVector( poly_index );
	switch (a_tole) {
		case tole_UrbanNoVeg:
		case tole_Building: // 5
		case tole_River: // 96
		case tole_Saltmarsh: // 95
		case tole_Saltwater: // 80
		case tole_Coast: // 100
			return -1; // Cannot move here
		case tole_SmallRoad: // 122
			m_InPond = -1;
			return 1; // Road mortality x1
		case tole_LargeRoad: // 121
			m_InPond = -1;
			return 2; // Road mortality x2
		case tole_Pond:
		{
			m_InPond = poly_index;
			bool found = false;
			for (unsigned p = 0; p < m_pondlist.size(); p++)
				if (m_pondlist[ p ] == poly_index) found = true;
			if (!found) m_pondlist.push_back( poly_index );
		}
		return 0;
		default:
			m_InPond = -1;
			return 0; // Can move here
	}
}
//----------------------------------------------------------------------------------------------------------------------------------

/*________________________________________________________________________________________________________________________________*/
/*______________________________________________________Newt_Adult_________________________________________________________________*/
/*________________________________________________________________________________________________________________________________*/

Newt_Adult::Newt_Adult(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) 
	: Newt_Juvenile(a_x, a_y, a_pond, a_L, a_NPM, a_age, a_reproinhib) 
{
	Init(a_x, a_y, a_pond, a_L, a_NPM, a_age);
}
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Adult::~Newt_Adult( void ) {
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Adult::ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) {
	Newt_Juvenile::ReInit(a_x, a_y, a_pond, a_L, a_NPM, a_age, a_reproinhib);
	Init(a_x, a_y, a_pond, a_L, a_NPM, a_age);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Adult::Init(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age) {
	/**
	* Newt_Adult Initialise object, assigns default values to specific adult attributes for in pond and target pond for migration,
	* and passes the age to the Newt_Juvenile constructor.
	*/
	int apolyref = a_L->SupplyPolyRefIndex(a_x, a_y);
	if (a_L->SupplyElementTypeFromVector(apolyref) != tole_Pond)
		m_InPond = -1;
	else {
		m_InPond = apolyref;
	}
	m_targetpondx = -1;
	m_targetpondy = -1;
#ifdef __RECORDNEWTMETAMORPHOSIS
	a_NPM->RecordAdultProduction(a_age);
#endif
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Adult::st_Disperse(void) {
	/**
	* The newt can only move around when the humidity is high. Here we assume this is related to the rainfall and temperature of the preceeding days.
	* If the humidity is high enough then the newt moves otherwise it stays where it is and returns toNewts_Develop.\n
	* The newt moves around using a random walk. At each step it evaluates if the habitat is walks onto is legal (if not it does not move),
	* and if a pond it remembers this. The new pond is added to the m_pondlist vector.\n
	* The rate of the random walk is set by a input parameter stored in the static attribute m_newtwalkspeed.
	* During the dispersal roads have associated mortality risks. These are flagged and probability tests taken.\n
	* If killed in dispersal, the current state will be changed to die by MoveTo->NewtDoWalking, so if this has been altered from toNewts_Dispersal then return the new state,
	* otherwise returns toNewts_Develop.
	* Note that adult dispersal in identical to juvenile dispersal, but differs in the distance assumed to be moved - which is reduced to 10% of the juvenile.
	*/
	TTypeDirectedWalk  directedness = directedwalk_random;
	if (m_forcedisperse) directedness = directedwalk_high;
	double temp = m_OurLandscape->SupplyTempPeriod(g_date->Date(), 5);
	double humidity = m_OurLandscape->SupplyHumidity();
	if ((temp > m_NewtDormancyTemperature) && (humidity > m_NewtDormancyHumidity))
	{
		MoveTo(directedness, m_newtwalkstepsize, m_newtadultwalkspeed);
		if (m_CurrentNewtState == toNewts_Die) return toNewts_Die;
	}
	return toNewts_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Adult::st_Develop(void) {
	/**
	* First determines whether the newt sucumbs to daily mortality, if so return toNewts_Die state. \n
	* If the adult survives:
	* - The age is incremented
	* - If the total age exceeds the parameter value held in m_AdultLifespan, the newt dies
	* - the behavioural state st_EvaluateHabitat is called following st_Develop, which leads to either a dispersal state or return to this state on the next day.
	*
	*/
	if (AgeMortTest()) return toNewts_Die;
	if (m_OurLandscape->SupplyTempPeriod(g_date->Date(), 5) < m_NewtDormancyTemperature) {
		return toNewts_Overwinter;
	}
	return toNewts_EvaluateLocation;
}
//---------------------------------------------------------------------------------------------------------------------------------

bool Newt_Adult::AgeMortTest(void) {
	/**
	* Ages the adult and tests for daily mortality using the supplied parameter for daily adult mortality stored in #m_AdultMortalityChance. 
	* This function is used by #st_Develop, st_Overwinter and Newt_Female::st_Breed behaviours.
	*/
	if (g_rand_uni() < m_AdultMortalityChance) return true;
	m_Age++;
	if (m_Age > m_AdultLifespan) return true;
	return false;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Adult::st_EvaluateHabitat( void ) {
	/**
	* The first thing to determine is whether this is the breeding season or not. This is controlled by the population manager.
	* If it is breeding then: \n
	* - Check if we are in a pond, if so return st_Breed
	* - If not, then return migrate
	*
	* If in a pond and after the NEWT_BREEDINGSEAONEND but before NEWT_BREEDINGSEAONEND+30 then it will disperse based on a probability which reaches unity by NEWT_BREEDINGSEAONEND+30.
	* If its not the breeding season and not in pond then evaluate habitat and determine the dispersal necessity. This is identical to the juvenile st_EvaluateHabitat, which is what is called.
	*
	*/
	int breedornot = m_OurPopulationManager->IsBreedingSeason();
	if (breedornot == 0) {
		if (m_InPond != -1) return toNewts_Breed;
		else return toNewts_Migrate;
	}
	else
	{
		if (m_InPond)
		{
			if (breedornot > 30)
			{
				m_forcedisperse = true;
			}
			else 	if (g_rand_uni() < (breedornot / 30.0)) return toNewts_Develop;
		}
	}
	return Newt_Juvenile::st_EvaluateHabitat();
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Adult::st_Overwinter( void ) {
	if (AgeMortTest()) return toNewts_Die;
	if (m_OurLandscape->SupplyTempPeriod( g_date->Date(), 5 )> m_NewtDormancyTemperature) {
		return toNewts_Develop;
	}
	return toNewts_Overwinter;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Adult::st_Migrate( void ) {
	/**
	* The newt needs to return to its home pond or another on its list of ponds found if any. This requires a directed migration.
	* Along the way the newt tests each step to see if it is a possible breeding location, or road mortality possibility.\n
	* First it tests to see if m_targetpondx is set, if its equal to -1 we have not target and need to aquire this. This is done by comparing the pondlist distances and picking the closest one.\n
	* Next the direction to the pond is needed. This is stored in #m_OurVector, but may need updating.\n
	* The newt now needs to move towards the target pond. This is done using the standard newt movement code, but the stopping rule is that we get to a breeding pond, or that breeding season is over.
	*
	*/
	if (m_targetpondx == -1) {
		// Need to select the target pond
		// Check each pond on our list to find the closest
		APoint pt;
		int range = m_SimW + m_SimH; // Too far
		int index = 0;
		for (unsigned p = 0; p < m_pondlist.size(); p++) {
			pt = m_OurLandscape->SupplyCentroidIndex( m_pondlist[ p ]); 
			/**  Calculates distance from location a_x, a_y. This is done using an approximation to pythagorus to avoid a speed problem. \n
			NB this will crash if either a_dy or a_dx are zero, therefore the addition of 1! */
			int dist;
			int dx = abs( m_Location_x - pt.m_x ) + 1;
			int dy = abs(m_Location_y - pt.m_y) + 1;
			if (dx>dy) dist = dx + (dy * dy) / (2 * dx); else dist = dy + (dx * dx) / (2 * dy);
			if (dist <= range) {
				range = dist;
				index = p;
			}
		}
		// Calculate the direction to the pond pointed to by index
		pt = m_OurLandscape->SupplyCentroidIndex( m_pondlist[ index ] );
		m_targetpondx = pt.m_x;
		m_targetpondy = pt.m_y;
	}
	int movedist;
	int dx = abs(m_Location_x - m_targetpondx) + 1;
	int dy = abs(m_Location_y - m_targetpondy) + 1;
	if (dx>dy) movedist = dx + (dy * dy) / (2 * dx); else movedist = dy + (dx * dx) / (2 * dy);
	if (movedist > m_newtwalkspeed) movedist = m_newtwalkspeed; else movedist = int(movedist*g_rand_uni()); // this is to prevent 'flipping' due to equal sized diagonal steps around the target
	CalcMovementVector();
	MoveTo(directedwalk_totally, m_newtwalkstepsize, movedist);
	if (m_CurrentNewtState == toNewts_Die) return toNewts_Die; // If we died in movement
	if (m_CurrentHabitat == tole_Pond)
	{
		// We made it, so reset target and start breeding
		m_targetpondx = -1;
		m_InPond = m_OurLandscape->SupplyPolyRefIndex( m_Location_x, m_Location_y );
		// Now we turn the newts around to face out of the pond.
		m_OurVector = (m_OurVector + 4) & 0x07;
		return toNewts_Breed;
	}
	return toNewts_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------

inline void Newt_Adult::CalcMovementVector( void ) {
	/** 
		This is a very simple direction calculator. It will fail if the pond is over a wrap around boundary. 
		To simplify this we only assume diagonal movement.
	*/
	int dx = m_targetpondx - m_Location_x;
	int dy = m_targetpondy - m_Location_y;
	if (dx > 0)
		if (dy > 0) m_OurVector = direction_se;
		else m_OurVector = direction_ne;
	else if (dy > 0) m_OurVector = direction_sw;
	else m_OurVector = direction_nw;
}
/*________________________________________________________________________________________________________________________________*/
/*______________________________________________________Newt_Male_________________________________________________________________*/
/*________________________________________________________________________________________________________________________________*/

Newt_Male::Newt_Male(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) : Newt_Adult(a_x, a_y, a_pond, a_L, a_NPM, a_age, a_reproinhib) {
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Male::~Newt_Male( void ) {
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Male::ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) {
	Newt_Adult::ReInit(a_x, a_y, a_pond, a_L, a_NPM, a_age, a_reproinhib);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Male::BeginStep(void) {
	/**
	* This is a speed improvement. The newt records that there is a male in the pond to prevent the long search time if a female is looking.
	* This is done by true/false because currently there is no need to identify a particular male. This must be done in the begin step to 
	* prevent problems with the timing for females, and resetting by DoDevelopment for pond flags.
	*/
	if (m_InPond != -1) m_OurLandscape->SetMaleNewtPresent(m_InPond);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Male::Step( void ) {
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentNewtState) {
		case toNewts_InitialState: // Initial state always starts with develop
			m_CurrentNewtState = toNewts_Develop;
			break;
		case toNewts_Develop:
		case toNewts_Breed:
			m_CurrentNewtState = st_Develop(); // Will return toNewts_Die, toNewts_Overwinter, toNewts_EvaluateLocation
			break;
		case toNewts_Dispersal:
			m_CurrentNewtState = st_Disperse(); // Will return toNewts_Develop or toNewts_Die
			break;
		case toNewts_Migrate:
			m_CurrentNewtState = st_Migrate(); // Will return toNewts_Develop, toNewts_Die, or toNewts_Breed which equates to develop in males
			m_StepDone = true;
			break;
		case toNewts_EvaluateLocation:
			m_CurrentNewtState = st_EvaluateHabitat(); // will return toNewts_Migrate, toNewts_Develop, toNewts_Dispersal
			m_StepDone = true;
			break;
		case toNewts_Overwinter:
			m_CurrentNewtState = st_Overwinter(); // will return toNewts_Migrate, toNewts_Develop, toNewts_Dispersal
			m_StepDone = true;
			break;
		case toNewts_Die:
			st_Dying(); // No return
			m_StepDone = true;
			break;
		case toNewts_Remove:
			m_CurrentStateNo = -1;
			m_StepDone = true;
			break;
		default:
			m_OurLandscape->Warn( "Newt_Male::Step()", " unknown state" );
			exit( 1 );
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Male::st_Develop(void) {
	/**
	* Adds pesticide handling to the Adult version of st_Develop
	* If there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,
	* then pesticide at this location is determined and any intake stored. #InternalPesticideHandlingAndResponse is called, and if the body-burden is greater than the threshold for effects then the impacts are determined.
	*/
	if (m_test_pesticide_terrestrial) {
		// Eliminate some/all of yesterdays body burden of PPP by multiplying by 1-the daily elimination rate
		m_body_burden *= m_AdultPPPElimRate;
		// Pick up the pesticide to add to the body burden
		m_body_burden += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
		InternalPesticideHandlingAndResponse();
		if (m_CurrentNewtState == toNewts_Die) return toNewts_Die;
	}
	return Newt_Adult::st_Develop();
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Male::st_EvaluateHabitat(void) {
	return Newt_Adult::st_EvaluateHabitat();
}
//---------------------------------------------------------------------------------------------------------------------------------

/*________________________________________________________________________________________________________________________________*/
/*______________________________________________________Newt_Female_______________________________________________________________*/
/*________________________________________________________________________________________________________________________________*/

Newt_Female::Newt_Female(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) : Newt_Adult(a_x, a_y, a_pond, a_L, a_NPM, a_age, a_reproinhib)
{
	/**
	* Sets the mated status to false and the number of eggs produced to zero
	*/
	m_mated = false;
	m_eggsproduced = 0;
}
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Female::~Newt_Female(void)
{
	;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Female::ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib) {
	Newt_Adult::ReInit(a_x, a_y, a_pond, a_L, a_NPM, a_age, a_reproinhib);
	m_mated = false;
	m_eggsproduced = 0;
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Female::Step( void ) {
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentNewtState) {
		case toNewts_InitialState: // Initial state always starts with develop
			m_CurrentNewtState = toNewts_Develop;
			break;
		case toNewts_Develop:
			m_CurrentNewtState = st_Develop(); // Will return toNewts_Die, toNewts_Overwinter, toNewts_EvaluateLocation
			break;
		case toNewts_Dispersal:
			m_CurrentNewtState = st_Disperse(); // Will return toNewts_Develop or toNewts_Die
			break;
		case toNewts_Migrate:
			m_CurrentNewtState = st_Migrate(); // Will return toNewts_Develop, toNewts_Breed or toNewts_Die
			m_StepDone = true;
			break;
		case toNewts_EvaluateLocation:
			m_CurrentNewtState = st_EvaluateHabitat(); // will return toNewts_Breed, toNewts_Migrate, toNewts_Develop, toNewts_Dispersal
			m_StepDone = true;
			break;
		case toNewts_Overwinter:
			m_CurrentNewtState = st_Overwinter(); // will return toNewts_Migrate, toNewts_Develop, toNewts_Dispersal
			m_StepDone = true;
			break;
		case toNewts_Breed:
			m_CurrentNewtState = st_Breed(); // will return  toNewts_Breed, toNewts_Develop, or toNewts_Die
			m_StepDone = true;
			break;
		case toNewts_Die:
			st_Dying(); // No return
			m_StepDone = true;
			break;
		case toNewts_Remove:
			m_CurrentStateNo = -1;
			m_StepDone = true;
			break;
		default:
			m_OurLandscape->Warn( "Newt_Female::Step()", " unknown state" );
			exit( 1 );
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Female::st_Develop(void) {
	/**
	* Adds pesticide handling to the Adult version of st_Develop
	* If there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,
	* then pesticide at this location is determined and any intake stored. #InternalPesticideHandlingAndResponse is called, and if the body-burden is greater than the threshold for effects then the impacts are determined.
	*/
	if (m_test_pesticide_terrestrial) {
		// Eliminate some/all of yesterdays body burden of PPP by multiplying by 1-the daily elimination rate
		m_body_burden *= m_AdultPPPElimRate;
		// Pick up the pesticide to add to the body burden
		m_body_burden += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
		InternalPesticideHandlingAndResponse();
		if (m_CurrentNewtState == toNewts_Die) return toNewts_Die;
	}
	return Newt_Adult::st_Develop();
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Female::st_Breed(void) {
	/**
	* This is the behaviour of the female in the breeding pond during breeding season. Behaviour consists of:\n
	* - A mortality check against daily mortality chance
	* - A check whether breeding season is over, if so change state to normal develop and set mated status to false.
	* - A check to see if she is mated or not, if not then look for a mate
	* - If mated and not reproductively inhibited, then production of eggs can start. This is a constant rate per day set by the parameter stored in m_eggdailyproductionvolume
	* - If the maximum number of eggs has been produced then egg production stops and non-breeding behaviour starts
	*/
# ifdef __NewtDebug
	if (m_InPond == -1) {
		g_msg->Warn("Newt_Female calling breeding but not in pond. Pond ref is equal to  ", m_InPond);
		g_msg->Warn("CurrentStateNo  ", m_CurrentStateNo);
		g_msg->Warn("x ", this->m_Location_x);
		g_msg->Warn("y ", this->m_Location_y);
		g_msg->Warn("Poly type ", m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y));
		exit(0);
	}
#endif
	if (AgeMortTest()) return toNewts_Die;
	if (m_OurPopulationManager->IsBreedingSeason()) {
		m_mated = false;
		return toNewts_Develop;
	}
	if (!m_mated) {
		if (m_OurLandscape->SupplyMaleNewtPresent(m_InPond)) m_mated = true;
		return toNewts_Breed;
	}
	else {
		if (!m_reproductiveinhibition) { // No reproduction if affected by pesticide
			if (m_eggsproduced < m_eggproductionvolume) {	// Must produce some eggs
				struct_Newt sN;
				sN.NPM = m_OurPopulationManager;
				sN.L = m_OurLandscape;
				sN.age = m_Age;
				sN.x = m_Location_x;
				sN.y = m_Location_y;
				sN.pondrefs.resize(1);
				sN.pondrefs[0] = m_InPond;
				sN.weight = 0;
				sN.reproinhib = false;
				m_OurPopulationManager->CreateObjects(tton_Egg, NULL, &sN, m_eggdailyproductionvolume);
				m_eggsproduced += m_eggdailyproductionvolume;
				return toNewts_Breed;
			}
		}
	}
	// We must have produced all our eggs, so stop breeding and return to normal behaviour
	m_mated = false;
	return toNewts_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------

TTypeOfNewtState Newt_Female::st_EvaluateHabitat(void) {
	/**
	* The female must not stay in breeding behaviour if she has produced her eggs, so this augmentation of evaluate habitat prevents that
	* happening.
	*
	* The first thing to determine is whether this is the breeding season or not. This is controlled by the population manager.
	* If it is breeding then: \n
	* - Check if we have already bred this year - if so don't start breeding behaviour again. This is reset at the beginning of the year by the population manager (which is more efficient than all newts asking if its 1st January).
	* - Check if we are in a pond, if so return st_Breed
	* - Check if we have already produced all the eggs we needed to
	* - If not, then return migrate
	*
	* If in a pond and after the NEWT_BREEDINGSEAONEND but before NEWT_BREEDINGSEAONEND+30 then it will disperse based on a probability 
	* which reaches unity by NEWT_BREEDINGSEAONEND+30.
	* If its not the breeding season and not in pond then evaluate habitat and determine the dispersal necessity. 
	* This is identical to the juvenile st_EvaluateHabitat, which is what is called.
	*
	*/
	int breedornot = m_OurPopulationManager->IsBreedingSeason();
	if (breedornot == 0) {
		if (m_eggsproduced < m_eggproductionvolume)
		{
			if (m_InPond != -1) return toNewts_Breed;
			else
			{
				return toNewts_Migrate;
			}
		}
	}
	else
	{
		if (m_InPond)
		{
			if (breedornot > 30)
			{
				m_forcedisperse = true;
			}
			else 	if (g_rand_uni() < (breedornot / 30.0)) return toNewts_Develop;
		}
	}
	return Newt_Juvenile::st_EvaluateHabitat();
}
//----------------------------------------------------------------------------------------------------------------------------

//____________________________________________________________________________________________________________________________
//__________________________________________________ PESTICIDE HANDLING CODE _________________________________________________
//____________________________________________________________________________________________________________________________

void Newt_Egg::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented from Newt_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for Egg to determine if an effect needs to be tested for
	if (m_body_burden > m_EggPPPThreshold) {
		// We are above the PPP body burden threshold, so make a test for effect
		if (g_rand_uni() > m_EggPPPEffectProbability) {
			TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
			switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects: // Reproductive effects
				m_reproductiveinhibition = true;
				break;
			case ttop_AcuteEffects: // Acute mortality
				m_CurrentNewtState = toNewts_Die;
				KillThis();
				break;
			case ttop_MultipleEffects:
				break;
			default:
				g_msg->Warn("Unknown pesticide type used in Newt_Female::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
				exit(47);
			}
		}
	}
}
//-------------------------------------------------------------------------------------
void Newt_Larva::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented from Newt_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for Larva to determine if an effect needs to be tested for
	if (m_body_burden > m_LarvaPPPThreshold) {
		// We are above the PPP body burden threshold, so make a test for effect
		if (g_rand_uni() > m_LarvaPPPEffectProbability) {
			TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
			switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects: // Reproductive effects
				m_reproductiveinhibition = true;
				break;
			case ttop_AcuteEffects: // Acute mortality
				m_CurrentNewtState = toNewts_Die;
				KillThis();
				break;
			case ttop_MultipleEffects:
				break;
			default:
				g_msg->Warn("Unknown pesticide type used in Newt_Female::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
				exit(47);
			}
		}
	}
}
//-------------------------------------------------------------------------------------
void Newt_Juvenile::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented from Newt_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for Juvenile to determine if an effect needs to be tested for
	if (m_body_burden > m_JuvenilePPPThreshold_Min) {
		// We are above the PPP body burden threshold, so make a test for effect
		TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
		switch (tp) {
		case ttop_NoPesticide:
			break;
		case ttop_ReproductiveEffects: // Reproductive effects
			if (g_rand_uni() > m_JuvenilePPPEffectProbability_EnvConc) {
				m_reproductiveinhibition = true;
			}
			break;
		case ttop_AcuteEffects: // Acute mortality
			if (g_rand_uni() > m_JuvenilePPPEffectProbability_EnvConc) {
				m_CurrentNewtState = toNewts_Die;
				KillThis();
			}
			break;
		case ttop_MultipleEffects:
			/** m_JuvenilePPPThreshold_Min holds the lowest trigger of any multiple effects - but we need to figure out which we have and respond accordingly
			* there are two possibilities - a env. concentration trigger and an overspray trigger
			*/
			// Are we in an overspray situation?
			if (m_OurLandscape->SupplyOverspray(m_Location_x, m_Location_y)) {
				// If so, have we exceeded the overspray trigger
				if (m_body_burden > m_JuvenilePPPThreshold_Overspray) {
					if (g_rand_uni() > m_JuvenilePPPEffectProbability_Overspray) {
						m_CurrentNewtState = toNewts_Die;
						KillThis();
					}
				}
			}
			// Not overspray so test the env conc trigger
			else if (m_body_burden > m_JuvenilePPPThreshold_EnvConc) {
				if (g_rand_uni() > m_JuvenilePPPEffectProbability_EnvConc) {
					m_CurrentNewtState = toNewts_Die;
					KillThis();
				}
			}
			break;
		default:
			g_msg->Warn("Unknown pesticide type used in Newt_Juvenile::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
			exit(47);
		}
	}
}
//-------------------------------------------------------------------------------------
void Newt_Male::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented from Newt_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for Adult to determine if an effect needs to be tested for
	if (m_body_burden > m_AdultPPPThreshold) {
		// We are above the PPP body burden threshold, so make a test for effect
		if (g_rand_uni() > m_AdultPPPEffectProbability) {
			TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
			switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects: // Reproductive effects
				// TODO
				break;
			case ttop_AcuteEffects: // Acute mortality
				m_CurrentNewtState = toNewts_Die;
				KillThis();
				break;
			case ttop_MultipleEffects:
				break;
			default:
				g_msg->Warn("Unknown pesticide type used in Newt_Female::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
				exit(47);
			}
		}
	}
}
//-------------------------------------------------------------------------------------
void Newt_Female::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented from Newt_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for Adult to determine if an effect needs to be tested for
	if (m_body_burden > m_AdultPPPThreshold) {
		// We are above the PPP body burden threshold, so make a test for effect
		if (g_rand_uni() > m_AdultPPPEffectProbability) {
			TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
			switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects: // Reproductive effects
				// TODO
				break;
			case ttop_AcuteEffects: // Acute mortality
				m_CurrentNewtState = toNewts_Die;
				KillThis();
				break;
			case ttop_MultipleEffects:
				break;
			default:
				g_msg->Warn("Unknown pesticide type used in Newt_Female::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
				exit(47);
			}
		}
	}
}
//-------------------------------------------------------------------------------------
