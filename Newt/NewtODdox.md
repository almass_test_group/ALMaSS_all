ALMaSS Newt ODdox Documentation                        {#Newt_page}
=========

<h1><small><small><small><small>Created by:
<small>Chris J. Topping
Department of Bioscience,
Grenaavej 14
DK-8410 Roende
Denmark
4th April 2016 <br>

Last updated 6th February 2017
</small></small></small></small></small></h1>
<HR> 

# Overview


## 1. Purpose
The Newt model has been developed with the intention to determine the resiliance of the population to agrochemical usage assuming that the stressors affect mortality or reproduction
of the newts exposed. The aim is to be able to quantify impacts of stressors on populations of newts in different landscape structures under different agricultural management regimes.

##  2. State variables and scales

A class attribute overview is presented below, navigate to the individual entry to obtain further details, or look at 'Process Overview and Scheduling' for functions and behaviours 
below:

### Newt_Base
- Attribute: Newt_Base::m_Age The age of the Newt (units days).
- Attribute: Newt_Base::m_OurPopulationManager This is a time saving pointer to the correct Newt_Population_Manager object.
- Attribute: Newt_Base::m_CurrentNewtState This is the current behavioural state. The possible list is held by TTypeOfNewtState
- Attribute: Newt_Base::m_pondlist This is the list of pond locations found by the newt, the first value being the pond of birth. Each entry is an index to the list of polygons held by the Landscape class.
- Attribute: Newt_Base::m_body_burden This is the current body-burden of pesticide, it is total amount.
- Attribute: Newt_Base::m_reproductiveinhibition This is a flag denoting reproductive inhibition if true. This can be set by descendent class responses to pesticides typically during developmental stages.

### Newt_Egg
 Class Newt_Egg represents the egg stage of the newt in ponds. Each egg has a location in a pond polygon represented by it's home pond polygon reference (first pond in Newt_Base::m_pondlist). The egg starts life at zero days old with zero day degrees. It ages each day ( see Newt_Egg::st_Develop ),
 and the temperature experiences is summed each day to create a cumulative day-degree sum. When this sum reaches a pre-defined number of day-degrees the egg hatches to produce a larva.
 In addition to inherited attributes from Newt_Base, each egg has:

- Attribute: Newt_Egg::m_AgeDegrees This is an internal variable used to sum up the day-degrees experienced by the egg/larva in degree days (units degree days).

### Newt_Larva
Class Newt_Larva represents the larval stage of the newt in ponds. Each larva has a location in a pond polygon represented by it's it's home pond polygon reference (first pond in Newt_Base::m_pondlist). The larva starts with zero day degrees and inherits the egg age at hatch. It ages each day ( see Newt_Larva::st_Develop ), and the temperature experiences is summed each day to create a cumulative day-degree sum. When this sum reaches a pre-defined number of day-degrees the larva metamorphoses to be a juvenile.	Death can occur by daily mortality probability or due to failing to get daily food intake. In addition to inherited attributes from Newt_Egg, each larva has:

- Attribute Newt_Larva::m_LarvalFoodProportion Holds an input parameter - it is the proportion of food eaten per day relative to weight
- Attribute Newt_Larva::m_LarvaDevelopmentUpperSz Holds an input parameter - it is the upper size at which larvae will undergo metamorphosis
- Attribute Newt_Larva::m_LarvaDevelopmentLowerSz Holds an input parameter - it is the lower size below which larvae cannot undergo metamorphosis
- Attribute Newt_Larva::m_LarvaDevelopmentTime Holds an input parameter - it is the time before a larva can undergo metamorphosis
- Attribute Newt_Larva::m_NewtLarvaDailyGrowthIncrement Holds an input parameter - it is the daily growth increment if a larva survives to grow that day
- Attribute Newt_Larva::m_LarvaMortalityChance Holds an input parameter - it is the daily probability of death from unspecified causes for a larva

### Newt_Juvenile
Class Newt_Juvenile represents the juvenile stage of the newt. It is free living and moves in the landscape. 
In addition to inherited attributes from Newt_Base, each juvenile has:

- Attribute: Newt_Juvenile::m_weight which is the weight of the newt in g
- Attribute: Newt_Juvenile::m_SimW - the width of the simulation map, a static variable used for speed
- Attribute: Newt_Juvenile::m_SimH - the height of the simulation map, a static variable used for speed
- Attribute: Newt_Juvenile::m_CurrentHabitat - the current habitat type the newt is located in
- Attribute: Newt_Juvenile::m_InPond is the polyrefindex for the pond the newt is in, or -1 if not in a pond
- Attribute: Newt_Juvenile::m_SimW The width of the simulation map, stored for fast access
- Attribute: Newt_Juvenile::m_SimH The height of the simulation map, stored for fast access
- Attribute: Newt_Juvenile::m_OurVector The last direction the newt moved in

 Other new attributes are included here for efficient storage of input parameters:

- Attribute: Newt_Juvenile::m_JuvenileDailyWeightGain Parameter Used in determining daily growth, this is the daily increment. 
- Attribute: Newt_Juvenile::m_roadmortalityprob  Parameter The probability of death when crossing a road, this is per movement (1 x Newt_Juvenile::m_newtwalkspeed).
- Attribute: Newt_Juvenile::m_newtwalkspeed  Parameter The max walking speed of a newt per movement (1 day)
- Attribute: Newt_Juvenile::m_newtwalkstepsize  Parameter The size of a step when evaluating habitat during walking - used to speed up movement if necessary, default is 1m
- Attribute: Newt_Juvenile::m_goodhabitatdispersalprob  Parameter Probability of dispersal in good habitat
- Attribute: Newt_Juvenile::m_poorhabitatdispersalprob  Parameter Probability of dispersal in poor habitat
- Attribute: Newt_Juvenile::m_NewtDormancyTemperature  Parameter Temperature in degrees that the newts become dormant and stop moving or breeding (summed over previous 5 days). Default value is 22.5 based on Langton et al (2001) who state 4-5 degrees.

### Newt_Adult
The adult class handles those attributes and functions common to all adults. 
In addition to inherited attributes from Newt_Juvenile, each adult has:\n
	Newt_Adult adds the following attributes:

- Attribute: Newt_Adult::m_AdultLifespan Parameter which is the maximum lifespan of a newt. Set at 14 years following Francillon-Vieillot et al (1990).
- Attribute: Newt_Adult::m_targetpondx which is the x-coordinate of the pond the newt decides to head to in migration
- Attribute: Newt_Adult::m_targetpondy which is the y-coordinate of the pond the newt decides to head to in migration
	
Four behavioural states are defined for the adult:

- Attribute: Newt_Adult::st_Develop which redefines the development state to take account of the adult rather than juvenile parameters
- Attribute: Newt_Adult::st_Overwinter which redefines the dormant state to use adult daily mortality
- Attribute: Newt_Adult::st_EvaluateHabitat which adds a breeding season test and the possibility to switch to migration or breeding behaviour
- Attribute: Newt_Adult::st_Migrate which returns the newt to its home pond or another on its list of ponds found if any


### Newt_Male
Newt_Male adds no new attributes to Newt_Adult.
The only special behaviour defined here is the Step code, which although specific to the Newt_Male class functions essentially the same way
 as the juvenile step code with the exception that the adult state Newt_Adult::Migrate can be called to return the male to a breeding pond.

### Newt_Female
In addition to inherited attributes from Newt_Adult, each female has:

 - Attribute: Newt_Female::m_eggproductionvolume  Parameter - holds the max number of eggs that can be produced during breeding
 - Attribute: Newt_Female::m_eggdailyproductionvolume  Parameter - holds the number of eggs produced per day during breeding
 - Attribute: Newt_Female::m_eggsproduced is used to keep track of the actual number of eggs produced
 - Attribute: Newt_Female::m_mated which is used to record whether the newt is mated or not this season

The only new behaviour added to the Newt_Female class is Newt_Female::st_Breed which is responsible for carrying out the breeding behaviour when the newt is in the pond

### Newt_Population_Manager
The Newt population manager is responsible for handling all lists of Newts and scheduling their behaviour. 
It provides the facilities for output and handles central Newt functions needed to interact with the landscape.

 - Attribute: Newt_Population_Manager::m_BreedingSeasonFlag Set to 0 when it is newt breeding season, then records the number of days after
 - Attribute: Newt_Population_Manager::m_NewtEgg_DDTempRate An array to hold a precalulated day-degree rate transformation for egg development
 - Attribute: Newt_Population_Manager:: m_NewtMetamorphosisStats A class for holding the statistics on newt metamorphosis development times
 - Attribute: Newt_Population_Manager::m_NewtEggProdStats A class for holding the statistics on newt egg production
 - Attribute: Newt_Population_Manager::m_NewtAdultProdStats A class for holding the statistics on newt adult production

## 3. Process Overview and Scheduling

### General structure
As with all ALMaSS models the Newt model is based on a state machine which can be described by a state-transition diagram. 
Newts are in a behavioural state and due to decisions or external events move via transitions to another state where they will exhibit 
different behaviour.

### Ecotoxicological Interface
A key component of this model needed to fulfil its purpose is the ecotoxicological modelling. 
The ecotoxicological interface is the connection between the newt and the pesticides that can have direct and indirect effects on the newts. 
Unlike the other processes the ecotoxicological interface is not a single piece of code or class, but is a set of functions and attributes that provide the interface between the pesticide module as defined by the Pesticide class and the newt.\n
Methods and functions are therefore placed under the class descriptions below, but are also listed here to provide easy access.

The functionality built into the newt model facilitates the following types of effect on newts of pesticides:
	- 1. Acute toxicity to free-living stages (juvs + adults)
	- 2. Reproductive effects on developing stages once they become adult reducing fecundity
	- 3. Acute toxicity to larvae and eggs in pond
	- 4. Indirect effects on larvae by killing their food.
	(- 5. Indirect effects on free-living stages)

To facilitate the use of pesticides for testing a relatively large number of inputs are needed to describe pesticide effects. 
Intially, for each of the direct effects (1-3) these are considered as being threshold effects and therefore  a toxicity threshold needs to be provided.
To keep the model as simple as possible these thresholds are considered as total body burden. Elimination is considered as a first order decay function.
This requires three specific pesticide response parameters per stage considered:

- The threshold in mg PPP - we are not considering body weight since this is not specified, but assume mean stage body weights in calculation of this threshold.
- The daily elimination rate as a proportion of the pesticide (expressed as 1.0-rate to facilitate computation by multiplication).
- The daily probability of exhibiting and effect when above the PPP body burden threshold specified in 'a'

#### Pesticide related functions:

- Newt_Egg::InternalPesticideHandlingAndResponse This function deals with the responses of the egg to pesticide body burden. When the function is called and the body burden exceeds the threshold given by #cfg_NewtEggPPPToxTrigger then depending on the type of pesticide effect either mortality or reproductive inhibition can occur.
- Newt_Larva::InternalPesticideHandlingAndResponse This function deals with the responses of the larva to pesticide body burden. When the function is called and the body burden exceeds the threshold given by #cfg_NewtLarvaPPPToxTrigger then depending on the type of pesticide effect either mortality or reproductive inhibition can occur.
- Newt_Juvenile::InternalPesticideHandlingAndResponse This function deals with the responses of the juvenile to pesticide body burden. When the function is called and the body burden exceeds the threshold given by #cfg_NewtJuvenilePPPToxTrigger_EnvConc or #cfg_NewtJuvenilePPPToxTrigger_Overspray then depending on the type of pesticide effect either mortality or reproductive inhibition can occur.
- Newt_Female::InternalPesticideHandlingAndResponse This function deals with the responses of the female to pesticide body burden. When the function is called and the body burden exceeds the threshold given by #cfg_NewtAdultPPPToxTrigger In this case the only response is mortality, no chronic endpoints are defined.
- Newt_Male::InternalPesticideHandlingAndResponse This function deals with the responses of the male to pesticide body burden. When the function is called and the body burden exceeds the threshold given by #cfg_NewtAdultPPPToxTrigger In this case the only response is mortality, no chronic endpoints are defined.


#### Pesticide input parameters

- NEWT_EGG_PPP_TOX_TRIGGER	#cfg_NewtEggPPPToxTrigger	Pesticide input parameter - the mg of pesticide assumed to trigger a response in embryonic newts
- NEWT_LARVA_PPP_TOX_TRIGGER	#cfg_NewtLarvaPPPToxTrigger	Pesticide input parameter - the mg of pesticide assumed to trigger a response in larval newts
- NEWT_JUVENILE_PPP_TOX_TRIGGER_ENVCONC	#cfg_NewtJuvenilePPPToxTrigger_EnvConc	Pesticide input parameter - the mg of pesticide assumed to trigger a response in juvenile newts for environmental concentration
- NEWT_JUVENILE_PPP_TOX_TRIGGER_OVERSPRAY	#cfg_NewtJuvenilePPPToxTrigger_Overspray	Pesticide input parameter - the mg of pesticide assumed to trigger a response in juvenile newts for overspray
- NEWT_ADULT_PPP_TOX_TRIGGER	#cfg_NewtAdultPPPToxTrigger	Pesticide input parameter - the mg of pesticide assumed to trigger a response in adult newts
- NEWT_EGG_PPP_TOX_ELIMINATIONRATE	#cfg_NewtEggPPPToxEliminationRate	Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in embryonic newts
- NEWT_LARVA_PPP_TOX_ELIMINATIONRATE	#cfg_NewtLarvaPPPToxEliminationRate	Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in larval newts
- NEWT_JUVENILE_PPP_TOX_ELIMINATIONRATE	#cfg_NewtJuvenilePPPToxEliminationRate	Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in juvenile newts
- NEWT_ADULT_PPP_TOX_ELIMINATIONRATE	#cfg_NewtAdultPPPToxEliminationRate	Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in adult newts
- NEWT_EGG_PPP_TOX_EFFECTPROBABILITY	#cfg_NewtEggPPPEffectProbability	Pesticide input parameter - The daily probability of effects if above the PPP body burden trigger for eggs
- NEWT_LARVA_PPP_TOX_EFFECTPROBABILITY	#cfg_NewtLarvaPPPEffectProbability	Pesticide input parameter - The daily probability of effects if above the PPP body burden trigger for larvae
- NEWT_JUV_PPP_TOX_EFFECTPROBABILITY_ENVCONC	#cfg_NewtJuvenilePPPEffectProbability_EnvConc	Pesticide input parameter - The daily probability of effects if above the PPP body burden trigger for juveniles for environmental concentration effects
- NEWT_JUV_PPP_TOX_EFFECTPROBABILITY_OVERSPRAY	#cfg_NewtJuvenilePPPEffectProbability_Overspray	Pesticide input parameter - The daily probability of effects if above the PPP body burden trigger for juveniles for overspray
- NEWT_ADULT_PPP_TOX_EFFECTPROBABILITY	#cfg_NewtAdultPPPEffectProbability	Pesticide input parameter - The daily probability of effects if above the PPP body burden trigger for adults


### Newt_Base Behaviours & Functions
The Newt_Base contains all the behaviour that is common to all newts, but there should never be a object of this type created. Therefore it does not have a state-transition diagram.

- Behaviour: Newt_Juvenile::st_Dying This is the way that dead newts are removed from the simulation.
- Function: Newt_Base::Newt_Base The constructor for the Newt_Base class.
- Function: Newt_Base::~Newt_Base Newt destructor.

### Newt_Egg Behaviours & Functions
In this case it is quite simple and consists of development leading to hatching or death.
State-Transition diagram:
\image html Newt_Egg_STDiagram.png

- Behaviour Newt_Egg::st_Develop Determines whether the egg succumbs to daily mortality using a probability test against a daily probability (input parameter), if so return toNewts_Die state.
 - Next, if the egg survives and there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,then pesticide at this location is determined and any intake stored. If the body-burden is greater than the threshold for effects, then #Newt_Egg::InternalPesticideHandlingAndResponse is called and the impacts determined.
 - Assuming survival to this point, the age is incremented. 
 - Next day degree model is updated. The model used here is fitted to D'Amen et al (2007) for Triturus carnifex. The sum of day degrees is increased by the mean temperature 
 today adjusted for temperature induced development rate. 
 The model for the daily contribution to hatch is: DD += DT/(DT+T^-0.534). DT is 1508 fitted to T.carnifex data from D'Amesn et al (2007), but 
 then calibrated downwards to obtain developmental rates fitting with *T.cristatus*
 - If the total sum of day degrees (DD) exceeds the parameter value held in m_EggDevelopmentThreshold, the egg should hatch and the state toNewts_NextStage is returned
 - If not fully developed the toNewts_Develop stage is returned and the process will be repeated the following day
- Behaviour Newt_Egg::st_NextStage The egg hatches and a larva is produced at the same location with the same age
- Function: Newt_Egg::Newt_Egg The constructor for the Newt_Egg class.
- Function: Newt_Egg::~Newt_Egg Newt destructor.
- Function: Newt_Egg::Step Step is a standard method that is required by all ALMaSS animals that are created as objects. It controls the flow of behaviour. 

### Newt_Larva Behaviours & Functions
In this case it is quite simple and consists of development leading to hatching or death. State-Transition diagram:
\image html Newt_Larva_STDiagram.png

- Behaviour Newt_Larva::st_Develop Determines whether the larva succumbs to daily mortality using a probability test against a daily probability (input parameter), if so return toNewts_Die state.
 - Next, if the larva survives and there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,then pesticide at this location is determined and any intake stored. Newt_Larva::InternalPesticideHandlingAndResponse is called and if the body-burden is greater than the threshold for effects, then the impacts are determined.
 - The age is incremented
 - The number days in the larval stage is incremented
 - We assume that the larva eats Newt_Base::m_LarvalFoodProportion multiplied by its age in g of food per day. If this is not available the larva dies. See the larval food model is controlled by the Newt_Population_Manager and accessed through Newt_Population_Manager::SubtractLarvalFood If this food is not available the larva dies.
 - The current model assumes a linear increase in size up to cfg_newtJuvenileStartSize from the starting length of 1mm - If not fully developed the toNewts_Develop stage is returned and the process will be repeated the following day
- Behaviour Newt_Larva::st_NextStage The larva matures and a juvenile is produced at the same location with the same age
- Function: Newt_Larva::Newt_Larva The constructor for the Newt_Larva class.
- Function: Newt_Larva::~Newt_Larva Newt destructor.
- Function: Newt_Larva::Step Step is a standard method that is required by all ALMaSS animals that are created as objects. It controls the flow of behaviour. 

### Newt_Juvenile Behaviours & Functions
State-Transition diagram:
\image html Newt_Juvenile_STDiagram.png
New behaviours are introduced here st_Disperse, st_EvaluateHabitat & st_Overwinter.

- Behaviour Newt_Juvenile::st_Develop
 - First determines whether the juvenile sucumbs to daily mortality, if so return toNewts_Die state.
 - Next if there is the need to test for pesticide effects then the body-burden is adjusted for yesterdays elimination,
 - then pesticide at this location is determined and any intake stored.  Newt_Juvenile::InternalPesticideHandlingAndResponse is called and if the body-burden is greater than the threshold for effects, then the impacts are determined.
 - If the juvenile survives the age is incremented
 - Length is added as a daily increment based on an input parameter contingent on non-dormancy due to weather. Newts may take 2 to 4 years to reach 
  maturity (Dolmen 1983), and adult size is not a good indication of age (Hagstrom, 1977; Francillon-Vieillot et al 1990; Arntzen, 2000). Therefor we need a method to allow newts to grow in good conditions but include variabilty in time to first breeding. This is achieved by allowing the newts to grow at a constant rate until m_JuvenileDevelopmentSize is reached when it is assumed to be adult. NB adult size differences are not simulated.
 The value of the parameter is based on increasing snout to vent length, based on an increase of 21.9mm over 458 days (Baker 1998), starting at a 42mm
 - If the total weight exceeds the parameter value held in Newt_Juvenile::m_JuvenileDevelopmentSize, the juvenile matures and the state toNewts_NextStage is returned
 - The behavioural state st_EvaluateHabitat is called following st_Develop, which leads to either a dispersal state or return to this state on the next day.
- Behaviour Newt_Juvenile::st_EvaluateHabitat Assesses where it is stood. If the habitat is good it sets the dispersal chance to Newt_Juvenile::m_goodhabitatdispersalprob otherwise to	Newt_Juvenile::m_poorhabitatdispersalprob It then takes a prbobability test to determine whether to dispserse. Will return disperse or develop states depending on the result of the test. 
- Behaviour Newt_Juvenile::st_Dispersal Juvenile Dispersal – It is assumed that the newt can only move around when the humidity is high. This is related to the rainfall and temperature of the preceding days. If the humidity is high enough then the newt may move otherwise it is forced to stay where it is. The newt moves around using a guided random walk. This means that movement is random except that it will evaluate each step and will not walk onto habitats considered ‘illegal’ e.g. houses. At each step it evaluates if the habitat is walks onto is legal (if not it does not move), it also checks if it is a road in which case there is a probability of road mortality. Finally, it checks if it is a pond and if so remembers this (if not experienced already). The number of steps is controlled by the parameter NEWT_WALKSPEED held in Newt_Juvenile::m_newtwalkspeed and is a uniform random distribution between 0 and this value. When the newt is leaves the pond this behaviour is altered to remove the random walk element, meaning that the first movement after leaving the pond is always in a straight line away from it. Subsequent movements follow the behaviour above.
- Behaviour Newt_Juvenile::st_NextStage The Juvenile matures and a male or female is produced at the same location with the same age
- Behaviour Newt_Juvenile::st_Overwinter The Juvenile takes a daily mortality test, ages but does not grow and tests each day for temperatures above Newt_Juvenile::m_NewtDormancyTemperature, once this is reached there is a transition to st_Develop
- Function: Newt_Juvenile::Newt_Juvenile The constructor for the Newt_Juvenile class.
- Function: Newt_Juvenile::~Newt_Juvenile Newt destructor.
- Function: Newt_Juvenile::Step Step is a standard method that is required by all ALMaSS animals that are created as objects. It controls the flow of behaviour. 

### Newt_Adult Behaviours & Functions
Like Newt_Base there should never be an object of Newt_Adult. This class is just used to house the behaviours common to both sexes as adults.
New behaviours are introduced here st_Migrate, but others are modified st_EvaluateHabitat, st_Develop & st_Overwinter.

- Behaviour Newt_Adult::st_Develop 	Called by both male and female st_Develop. This checks for mortality, and assuming the newt survives then deals with growth and calls the next behaviour depending on whether it is breeding season and whether the newt is in a pond or not.
 - First determines whether the juvenile sucumbs to daily mortality, if so return toNewts_Die state. 
 - Assuming the newt survives this far, the age is incremented
 - The behavioural state st_EvaluateHabitat is called following st_Develop, which leads to either a dispersal state or return to this state on the next day.
- Behaviour Newt_Adult::st_EvaluateHabitat First determines whether it is the breeding season and whether in pond. If not in pond and it is the breeding season transitions to st_Migrate. Otherwise like the juvenile it assesses where it is stood. If in a pond and after the NEWT_BREEDINGSEAONEND then it will disperse based on a probability which reaches unity by June 30th. Otherwise, if the habitat is good it sets the dispersal chance to Newt_Juvenile::m_goodhabitatdispersalprob otherwise to  Newt_Juvenile::m_poorhabitatdispersalprob It then takes a prbobability test to determine whether to dispserse. Will return disperse or develop states depending on the result of the test. 
- Behaviour Newt_Adult::st_Dispersal Adult dispersal follows the same pattern as juvenile behaviour except that daily movement distances are reduced to 10% of juvenile dispersal following (Karlsson, Betzholtz et al. 2007).
- Behaviour Newt_Adult::st_Migrate The newt needs to return to its home pond or another on its list of ponds found if any. This requires a directed migration. Along the way the newt tests each step to see if it is a possible breeding location, or road mortality possibility. First it tests to see if m_targetpondx is set, if its equal to -1 we have not target and need to aquire this. This is done by comparing the pondlist distances and picking the closest one. Next the direction to the pond is needed. This is stored in Newt_Juvenile::m_OurVector, but may need updating. The newt now needs to move towards the target pond. This is done using the standard newt movement code, but the stopping rule is that we get to a breeding pond, or that breeding season is over.
- Behaviour Newt_Juvenile::st_Overwinter The adult takes a daily mortality test and tests for excedence of lifespan, ages and tests each day for mean weekly temperatures above Newt_Juvenile::m_NewtDormancyTemperature, once this is reached there is a transition to st_Develop
- Function: Newt_Adult::Newt_Juvenile The constructor for the Newt_Juvenile class.
- Function: Newt_Adult::~Newt_Juvenile Newt destructor.
- Function: Newt_Adult::Step Step is a standard method that is required by all ALMaSS animals that are created as objects. It controls the flow of behaviour. 

### Newt_Male Behaviours & Functions
State-Transition diagram:
\image html Newt_Male_STDiagram.png

- Function: Newt_Male::Newt_Male The Newt_Male constructor
- Function: Newt_Male::~Newt_Male The Newt_Male destructor
- Function: Newt_Male::Step Newt_Male Step code. This is called continuously until all animals report that they are 'DONE'
- Behaviour: Newt_Male::st_EvaluateHabitat In case it is necessary this can be modified from the adult evaluate habitat behaviour
- Behaviour: Newt_Male::st_Develop - Handles pesticide tests. If the test for pesticide effects is necessary then the body-burden is adjusted for yesterdays elimination,then pesticide at this location is determined and any intake stored. Newt_Male::InternalPesticideHandlingAndResponse is called and if the body-burden is greater than the threshold for effects, then the impacts are determined. Calls Adult::st_Develop.

### Newt_Female Behaviours & Functions
State-Transition diagram:
\image html Newt_Female_STDiagram.png

- Function: Newt_Female::Newt_Male The Newt_Male constructor
- Function: Newt_Female::~Newt_Male The Newt_Male destructor
- Function: Newt_Female::Step Newt_Male Step code. This is called continuously until all animals report that they are 'DONE'
- Function: Newt_Female::ResetEggProduction Resets the eggs produced record to zero
- Behaviour: Newt_Female::st_EvaluateHabitat Modified from the Newt_Juvenile::st_EvaluateHabitat If the it is breeding time then the behaviour 
exhibited is different and modified here, otherwise the juvenile behaviour is followed.
- Behaviour: Newt_Female::st_Develop - Handles pesticide tests. If the test for pesticide effects is necessary then the body-burden is adjusted for yesterdays elimination,then pesticide at this location is determined and any intake stored. Calls Adult::st_Develop. Newt_Male::InternalPesticideHandlingAndResponse is called and if the body-burden is greater than the threshold for effects, then the impacts are determined.
- Behaviour: Newt_Female::st_Breed - This is the behaviour of the female when in the pond and consists of:
    - A mortality check against daily mortality chance
    - A check whether breeding season is over, if so change state to normal develop and set mated status to false.
    - A check to see if she is mated or not, if not then look for a mate
    - If mated and not reproductively inhibited (probably by pesticide in developing stages), then production of eggs can start. This is a constant rate per day set by the parameter stored in m_eggdailyproductionvolume
    - If the maximum number of eggs has been produced then egg production stops and non-breeding behaviour starts
    - Note that if inhibited then the newt produces no eggs but stays in the pond until the end of the breeding season.


### Newt_Population_Manager
The Newt population manager is responsible for handling all lists of Newts and warrens and scheduling their behaviour. 
It provides the facilities for output and handles central Newt functions needed to interact with the landscape. 

- Function: Newt_Population_Manager::Newt_Population_Manager Newt_Population_Manager Constructor.
- Function: Newt_Population_Manager::~Newt_Population_Manager The destructor for the Newt_Population_Manager class.
- Function: Newt_Population_Manager::CreateObjects The method for creating a new individual Newt
- Function: Newt_Population_Manager::IsBreedingSeason Returns whether at this moment in time it is breeding season or not 
- Function: Newt_Population_Manager::SetFreeLivingMortChance Calculate the daily background mortality chance for free-living newts based on weather
- Function: Newt_Population_Manager::GetEggDDRateTransformation Returns the value from the pre-calculated m_NewtEgg_DDTempRate to get effective day degrees
- Function: Newt_Population_Manager::RecordAdultProduction Output function. Add a new adult to the statistics record
- Function: Newt_Population_Manager::RecordEggProduction Output function. Add a new egg production to the statistics record 
- Function: Newt_Population_Manager::RecordMetamorphosis Output function. Add a new metamorphosis time to the stats record 
- Function: Newt_Population_Manager::InitOutputMetamorphosisStats Output function. Initialises output mean and variance for meatamorphosis times this year
- Function: Newt_Population_Manager::OutputMetamorphosisStats Output function. Output mean and variance for metamorphosis times this year
- Function: Newt_Population_Manager::DoFirst Things to do before anything else at the start of a time-step
- Function: Newt_Population_Manager::SetUnsetBreedingSeason Controls when it is breeding season
- Function:  Newt_Population_Manager::TheNWordOutputProbe This is a tailored function producing gridded output to support the calculation of Abundance/Occupancy ratios for the Newt-


## Design


## 4. Design Concepts

### 4.a Emergence
Todo

### 4.b Adaptation
Todo

### 4.c Fitness
Fitness is measured for the Newt in terms of survival and reproduction only, there is no special metric.
### 4.d Prediction
Todo

### 4.e Sensing
Eggs sense temperature. Larvae sense temperature and the availability of larval food. Juveniles, and Males and Females sense temperature, moisture and habitat types (pond, good or poor habitat and roads).

###  4.f Interaction
Interactions between newts are limited to 

### 4.g Stochasticity
Stochasticity is used extensively. All Newts are subject to stochastic mortality, the severity of which is modified by their status and behaviours (e.g. if dispersing). Exploration
of local warrens is also stochastic, but modified by distance such that the chance of visiting a nearby warren is distance dependent. Litter size also has a stochastic component.

### 4.h Collectives
Collectives are not used, although lists of newts in a pond are used to determine mate availability in adult newts.

### 4.i Observation
Todo

## 5. Initialisation
Todo

## 6. Inputs 
### Input variables
The upper-case names are the names used in the configuration file. See example below.
Parameters from literature:

- NEWT_EGG_DEVELPARAMETER	#cfg_NewtEggDevelDDParameter	The temperature slope for egg development change with temperature.	
	Default value is -0.534 fitted to T.carnifex data from D'Amesn et al (2007). 
- NEWT_JUVENILE_DEVELSIZE	#cfg_NewtJuvenileDevelSize	Newt model input parameter - the length needed to be achieved before a juvenile matures, from Baker (1998)	Default value is 53.9mm 
- NEWT_JUVENILEDAILYLENGTHGAIN	#cfg_NewtJuvenileDailyLengthGain	If temperature is above the NEWT_DORMANCYTEMP the newt grows by this many mm per day  and free-living stages can move. 
	Total growth under lab conditions is 21.9mm over 458 days (Baker 1998) 	Default value is 0.047827
- NEWT_NAR_MORTALITYFACTOR	#cfg_NewtNARMortalityFactor	This is the per mm decrease in survival in non-aquatic rainfall (from Griffiths et al, 2010). Default value is 0.0016
- NEWT_WT_MORTALITYFACTOR	#cfg_NewtWTMortalityFactor	This is the per degree decrease in survival in winter minimum temp (from Griffiths et al, 2010). Default value is 0.10 
- NEWT_ROADMORTALITYPROB	#cfg_NewtRoadMortalityProb	Chance of mortality per road crossing	
	Default value is 0.45 for a typical road, assumed to be 0.9 for busy roads (Hels & Buchwal, 2001) 
- NEWT_BREEDINGSEASONSTART	#cfg_breedingseasonstart	First possible breeding season date	Default value is 1st March  Griffiths et al (2010) & Langton et al (2001) 
- NEWT_BREEDINGSEASONEND	#cfg_breedingseasonend	Last possible breeding season date	Default value is 1st June  Griffiths et al (2010) & 	Langton et al (2001). Here we use a fixed value, but 	this is not really the case, the newts emerge from the pond over a period of a month (Langton et al (2001)). This is simulated in Newt_Adult::st_EvaluateHabitat by using a daily probability of emergence from pond which is 1 after 30 days.
- NEWT_ADULTLIFESPAN	#cfg_NewtAdultLifespan	The maximum lifespan in days for an adult (including egg and larval stages)	Default value is 5110 
- NEWT_DORMANCYTEMP	#cfg_newtdormancytemp	The temperature needed for newt movement (over last 5 days)	Default value is 22.5 based on Langton et al (2001) who state 4-5 degrees 
- NEWT_EGGPRODUCTIONVOLUME	#cfg_newteggproductionvolume	Total number of eggs produced per breeding season Default value is 100 based on Arntzen & Teunis 1993, but halved because Macgregor, H. C. and H. Horner (1980) suggests 50% are non-viable.
-NEWT_EGGDAILYPRODUCTIONVOLUME	#cfg_newtdailyeggproductionvolume	The number of eggs laid per day when breeding. At the start of the breeding season females may lay just a few eggs per night, but as air and then water temperatures rise, by April they may lay ten or so eggs each day, with consecutively laid eggs often deposited on the same plant (Langton et al, 2001). Currently this mechanism is not built into the model and the mean value of 5 is used. Default value is 5 
- NEWT_LARVA_DEVELUPPERSZ	#cfg_NewtLarvaDevelThresholdUpperSz	The upper size at which larvae will undergo metamorphosis. Default value is 42.3 Baker (1998) 
- 
Modified literature parameters:

 - NEWT_EGG_DEVELTOTAL	#cfg_NewtEggDevelTotal	The sum of day degrees before an egg hatches. Default value is 1508 fitted to T.carnifex data from D'Amesn et al (2007), but then calibrated downwards to obtain developmental rates fitting with *T.cristatus*
- NEWT_WALKSPEED	#cfg_NewtWalkSpeed	This parameter controls the distance moved per day (m) when a newt disperses. 	Default value is 20.  The value  of 20 comes from Jehle and Arntzen (2000), but other authors also record longer migrations and suggest 1km per year (Arntzen and Wallis 1991, Halley, Oldham et al. 1996). (Kovar, Brabec et al. 2009) estimated spring migration distances at 105-866m. In a radio-telemetry study of Triturus cristatus, Jehle (2000) found that more than 50% of adult newts leaving breeding ponds utilised refuges within 15m of the water and that 95 % could be found within 50m of the pond (but note that Jehle & Arntzen recorded examples of daily movement of up to 136.7m). The value of 20 allows for longer juvenile migration and is assumed to be reduced by 90% for adults (following Karlsson, Betzholtz et al. (2007)).
- NEWT_LARVA_DAILYGROWTHINCREMENT #cfg_NewtLarvaDailyGrowthIncrement The daily increment assuming a linear growth model to the juvenile start length. Is 42.3mm - start length of 1mm divided by 12 weeks. Default value is therefore (42.3 - 1.0) / (12.0*7.0)
- NEWT_LARVA_DEVELTIME	#cfg_NewtLarvaDevelThresholdTime The development time at which the larva will undergo metamorphosis if its size is between cfg_NewtLarvaDevelThresholdUpperSz and cfg_NewtLarvaDevelThresholdLowerSz thresholds. Default value is 12 weeks based on the assumption of 16 weeks total development time on average from Langton et al (2001)
- NEWT_EGG_MORTALITYCHANCE	#cfg_NewtEggMortalityChance	Daily egg mortality probability, excluding environmental mortality. Fitted to the assumption of 95% mortality between embryo and metamorphosis Hedlund (1990). Default value is 0.048
- NEWT_LARVAL_MORTALITYCHANCE	#cfg_NewtLarvaMortalityChance	Daily larva mortality probability, excluding environmental mortality and starvation. Fitted to the assumption of 95% mortality between embryo and metamorphosis Hedlund (1990).Default value is 0.005

Fitting parameters:

- NEWT_DORMANCYHUMIDITY	#cfg_newtdormancyhumidity	The humidity level needed for activity.	Default value is 0.003125 which represents 1mm rain 5 days ago at 20 degrees.
- POND_LARVALFOODBIOMASSCONST #cfg_PondLarvalFoodBiomassConst A constant relating the proportion of food units per m2. The value is calibrated to estimates of newt density. Default value of 215.
- POND_LARVALFOODFOODR #cfg_PondLarvalFoodR The instanteous rate of growth for larval food (r from logistic equation), a fitting parameter. Default value 0.15
- NEWT_LARVALFOODPROPCONST #cfg_LarvalFoodProportion A scaling parameter, this is multiplied by larval age is the larval food consumption units of food per day. This is linked to POND_LARVALFOODBIOMASSCONST and POND_LARVALFOODFOODR. Default value is 0.01.
- NEWT_GOODHABITATDISPPROB	#cfg_newtgoodhabitatdispersalprob	The daily probability of dispersal in good habitat if weather is suitable (temp and precipication)	Default value is 0.25 
- NEWT_POORHABITATDISPPROB	#cfg_newtpoorhabitatdispersalprob	The daily probability of dispersal in poor habitat if weather is suitable 	(temp and precipication)	Default value is 1.0 

Parameters that are present but currently have no impact:

- NEWT_WALKSSTEPSIZE	#cfg_NewtWalkStepsize	This is the step size the newt can take in the model (can be set >1 to speed model up)	Default value is 1
- NEWT_LARVA_DEVELOWERSZ	#cfg_NewtLarvaDevelThresholdUpperSz	The lower size at which larvae cannot undergo metamorphosis. This is currently set to the same default value is 42.3 based on Baker (1998) as the upper size limit. This means that there is no variation in size only variation in timing of when the newts emerge. This parameter is here to allow future versions to implement more detailed emergence behaviour.
- NEWT_STARTNUMBER #cfg_newt_startnumber The starting numbers for male and female newts when initialising the simulation. 


## 7. Interconnections
The Newt model relies on environmental data primarily from the Landscape and Weather classes. They are also dependent on interactions with the Farm and Crop classes
for information on management. Pesticide use and uptake depends on the Pesticide class.
The Newt_Larvae is dependent on the Pond class and the attributes of the pond which provide larval food sources. 
Ponds are assumed to be <0.5ha and from a newt perspective we assume that they have a maximum usable area of 400 m2. Whether a female finds a mate is determined via the landscape function Landscape::SupplyMaleNewtPresent which calls pond functionality. See Pond for more details.
The Newt_Population_Manager is descended from Population_Manager and performs the role of an auditor in the simulation. Many of the functions and behaviours needed to execute and
ALMaSS model are maintained by the Population_Manager.

## 8. References

-	Arntzen, J.W., A growth curve for the newt Triturus cristatus. Journal of Herpetology, 2000. 34(2): p. 227-232.
-   Arntzen, J. W. and G. P. Wallis (1991). Restricted Gene Flow in a Moving Hybrid Zone of the Newts Triturus-Cristatus and Triturus-Marmoratus in Western France. Evolution 45(4): 805-826.
-   Arntzen, J. W. and S. F. M. Teunis (1993). A 6-Year Study on the Population-Dynamics of the Crested Newt (Triturus-Cristatus) Following the Colonization of a Newly Created Pond. Herpetological Journal 3(3): 99-110.
-	Baker, J., Growth of juvenile newts Triturus cristatus and T-vulgaris in captivity. Amphibia-Reptilia, 1998. 19(3): p. 335-340.
-   D'Amen, M., L. Vignoli and M. A. Bologna (2007). The effects of temperature and pH on the embryonic development of two species of Triturus (Caudata : Salamandridae). Amphibia-Reptilia 28(2): 295-300.
-	Dolmen, D., Growth and size of Triturus-vulgaris L and T cristatus (Amphibia) in different parts of Norway. Holarctic Ecology, 1983. 6(4): p. 356-371.
-   Francillon-Vieillot, H., J. W. Arntzen and J. Geraudie (1990). Age, growth and longevity of sympatric Triturus-cristatus, Triturus-marmoratus and their hybrids (Amphibia, Urodela) - a skeletochronological comparison. Journal of Herpetology 24(1): 13-22.
-   Griffiths, R. A., D. Sewell and R. S. McCrea (2010). "Dynamics of a declining amphibian metapopulation: Survival, dispersal and the impact of climate." Biological Conservation 143(2): 485-491.
-	Hagstrom, T., Notes on growth and age distribution of adult Triturus-vulgaris L and T cristatus Laurenti (Urodela, Salamandridae) in SE Sweden. Bulletin De La Societe Zoologique De France-Evolution Et Zoologie, 1975. 100(4): p. 680-680.
-   Halley, J. M., R. S. Oldham and J. W. Arntzen (1996). "Predicting the Persistence of Amphibian Populations with the Help of a Spatial Model." Journal of Applied Ecology 33(3): 455-470.
-   Hedlund, L. (1990). Reproductive ecology of crested newts, Triturus cristatus (Laur.). PhD Thesis. Swedish Unversity of Agricultural Sciences, report no. 16.
-   Hels, T. and E. Buchwald (2001). The effect of road kills on amphibian populations. Biological Conservation 99(3): 331-340.
-   Jehle, R. (2000). The terrestrial summer habitat of radio-tracked great crested newts (Triturus cristatus) and marbled newts (T-marmoratus). Herpetological Journal 10(4): 137-142.
-   Jehle, R. and J. W. Arntzen (2000). Post-breeding migrations of newts (Triturus cristatus and T. marmoratus) with contrasting ecological requirements. Journal of Zoology 251: 297-306.
-   Karlsson, T., Betzholtz, P.-E. and Malmgren, J. C.(2007)  Estimating viability and sensitivity of the great crested newt Triturus cristatus at a regional scale. – Web Ecology 7: 63-76.
-   Kovar, R., M. Brabec, R. Vita and R. Bocek (2009). "Spring migration distances of some Central European amphibian species." Amphibia-Reptilia 30(3): 367-378.
-   Langton, T.E.S., Beckett, C.L., and Foster, J.P. (2001), Great Crested Newt Conservation Handbook, Froglife, Halesworth
-   Macgregor, H. C. and H. Horner (1980). Heteromorphism for chromosome-1 - requirement for normal development in crested newts. Chromosoma 76(2): 111-122.
