/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>SE_WinterRape_Seed.cpp This file contains the source for the SE_WinterRape_Seed class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// SE_WinterRape_Seed.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SE_WinterRape_Seed.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_SE_WinterRape_Seed_SkScrapes("SE_CROP_WRS_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_winterwheat_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WRS_InsecticideDay;
extern CfgInt   cfg_WRS_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool SE_WinterRape_Seed::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).

					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case se_wrs_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		SE_WRS_HERBICIDE = false;
		SE_WRS_DECIDE_TO_HERB = 1;
		SE_WRS_DECIDE_TO_FI = 1;

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_SE_WinterRape_Seed_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 3 start and stop dates for all events after harvest for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(25, 8)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(25, 8)); // last possible day of management after harvest
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "SE_WinterRape_Seed::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "SE_WinterRape_Seed::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "SE_WinterRape_Seed::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), se_wrs_npk, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		// OK, let's go.
		// LKM: Here we queue up the first event - a fork with parallel events
		SimpleEvent(d1, se_wrs_straw_removal, false);
	}
	break;
	// This is the first real farm operation
	case se_wrs_straw_removal:
		if (a_ev->m_lock || a_farm->DoIt(25)) { // crop scheme says 10-40%
			if (!a_farm->StrawRemoval(a_field, 0.0, g_date->DayInYear(9, 8) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again 
				SimpleEvent(g_date->Date() + 1, se_wrs_straw_removal, true);
				break;
			}
		}
		// Queue up the next event - in this case herbicide
		SimpleEvent(g_date->Date(), se_wrs_herbicide1a, false);
		break;
	case se_wrs_herbicide1a:
		if (a_ev->m_lock || a_farm->DoIt_prob(.50)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again 
				SimpleEvent(g_date->Date() + 1, se_wrs_herbicide1a, true);
				break;
			}
			else {
				//Rest of farmers herbicide later so we need to remember who already did it
				SE_WRS_HERBICIDE = true;
			}
		}
		// Queue up the next event - in this case stubble cultivator
		SimpleEvent(g_date->Date(), se_wrs_stubble_cultivator1, false);
		break;
	case se_wrs_stubble_cultivator1:
		if (a_ev->m_lock || a_farm->DoIt(85)) { // crop scheme says 80-90%
			if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_stubble_cultivator1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 5, se_wrs_stubble_cultivator2, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 2, se_wrs_autumn_plough, false);
		break;
	case se_wrs_autumn_plough:
		if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, se_wrs_autumn_plough, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, se_wrs_herbicide1b, false);
		break;
	case se_wrs_stubble_cultivator2:
		if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, se_wrs_stubble_cultivator2, true);
			break;
		}
		// Queue up the next event - in this case slurry 
		SimpleEvent(g_date->Date(), se_wrs_herbicide1b, false); // no dates in scheme
		break;
	case se_wrs_herbicide1b:
		if (a_ev->m_lock || a_farm->DoIt_prob(.50 / .50) && (SE_WRS_HERBICIDE == false)) { 
			if (!a_farm->HerbicideTreat(a_field, 0.0,
				g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_herbicide1b, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), se_wrs_autumn_harrow, false); // seedbed preparation - no dates
		break;
	case se_wrs_autumn_harrow:
		if (a_ev->m_lock || a_farm->DoIt(30)) { // crop scheme says 20-40%
			if (!a_farm->ShallowHarrow(a_field, 0.0,
				g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_autumn_harrow, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), se_wrs_autumn_sow, false);
		break;
	case se_wrs_autumn_sow:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->AutumnSowWithFerti(a_field, 0.0,
				g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_autumn_sow, true);
				break;
			}
		}
		// LKM: Here is a fork leading to parallel events
		SimpleEvent(g_date->Date()+1, se_wrs_insecticide1, false); // insecticide thread // no dates in scheme
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 8), se_wrs_herbicide2, false); // herbicide thread // no dates in scheme
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 8), se_wrs_growth_regulator, false); // growth regulator thread - main thread // no dates in scheme
		break;

	case se_wrs_insecticide1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.30)) { // crop scheme says 10-50% 
			if (!a_farm->InsecticideTreat(a_field, 0.0,
				g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_insecticide1, true);
				break;
			}
		}
		else SimpleEvent(g_date->Date()+15, se_wrs_insecticide2, false);
		break;
	case se_wrs_insecticide2:
		if (a_ev->m_lock || a_farm->DoIt_prob(.40/.70)) { // crop scheme says 30-50% - 40% of all = 57% of remaining 70%
			if (!a_farm->InsecticideTreat(a_field, 0.0,
				g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_insecticide2, true);
				break;
			}
		}
		//LKM: insecticide thread
		break;
	case se_wrs_herbicide2:
		if (a_ev->m_lock || a_farm->DoIt(65)) { // crop scheme says 50-80%
			if (!a_farm->HerbicideTreat(a_field, 0.0,
				g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_herbicide2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 10, se_wrs_herbicide3, false);
		break;
	case se_wrs_herbicide3:
		if (a_ev->m_lock || a_farm->DoIt(65)) { // crop scheme says 50-80%
			if (!a_farm->HerbicideTreat(a_field, 0.0,
				g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_herbicide3, true);
				break;
			}
		}
		//LKM: herbicide thread
		break;
	case se_wrs_growth_regulator:
		if (a_ev->m_lock || a_farm->DoIt(12)) { // crop scheme says 10-15 %
			if (!a_farm->GrowthRegulator(a_field, 0.0,
				g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_growth_regulator, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4)+365, se_wrs_npk, false);
		break;
	case se_wrs_npk:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->FP_NPK(a_field, 0.0,
				g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_npk, true);
				break;
			}
			// Queue up the next events - fork of parallel events
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 4), se_wrs_ns2, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 4), se_wrs_herbicide4, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), se_wrs_ns1, false);
		break;
	case se_wrs_ns1:
		if (!a_farm->FP_NS(a_field, 0.0,
			g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, se_wrs_ns1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 4), se_wrs_ns2, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 4), se_wrs_herbicide4, false);
		break;
	case se_wrs_ns2:
		if (a_ev->m_lock || a_farm->DoIt(80)) { // crop scheme says 60-100%
			if (!a_farm->FP_NS(a_field, 0.0,
				g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_ns2, true);
				break;
			}
		}
		break;
	case se_wrs_herbicide4:
		if (a_ev->m_lock || a_farm->DoIt(25)) { // crop scheme says 20-30%
			if (!a_farm->HerbicideTreat(a_field, 0.0,
				g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_herbicide4, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 5), se_wrs_insecticide3, false);
		break;
	case se_wrs_insecticide3:
		if (a_ev->m_lock || a_farm->DoIt(30)) { // crop scheme says 20-40% // no dates
			if (!a_farm->InsecticideTreat(a_field, 0.0,
				g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_insecticide3, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 6), se_wrs_herbicide5, false);
		break;
	case se_wrs_herbicide5:
		if (a_ev->m_lock || a_farm->DoIt(7)) { // crop scheme says 5-10 %
			if (!a_farm->HerbicideTreat(a_field, 0.0,
				g_date->DayInYear(11, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, se_wrs_herbicide5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+14, se_wrs_harvest, false); // no dates in scheme
		break;
	case se_wrs_harvest:
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, se_wrs_harvest, true);
			break;
		}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "SE_WinterRape_Seed::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}