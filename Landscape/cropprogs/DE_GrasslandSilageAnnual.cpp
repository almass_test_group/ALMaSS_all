/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_GrasslandSilageAnnual.cpp This file contains the source for the DE_GrasslandSilageAnnual class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Susanne Stein \n
 Version of August 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_GrasslandSilageAnnual.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_GrasslandSilageAnnual.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional grassland silage annual.
*/
bool DE_GrasslandSilageAnnual::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEGrasslandSilageAnnual; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (m_ev->m_todo)
	{
	case de_gsa_start:
	{
		// de_gsa_start just sets up all the starting conditions and reference dates that are needed to start a de_gsa crop off
		DE_GSA_FERTI_P1 = false;
		DE_GSA_FERTI_S1 = false;
		DE_GSA_STUBBLE_PLOUGH = false;
		DE_GSA_WINTER_PLOUGH = false;
		DE_GSA_SPRING_FERTI = 0;
		DE_GSA_HERBICIDE1 = false;
		DE_GSA_HERBI_DATE = 0;
		m_field->ClearManagementActionSum();


		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 3 start and stop dates for all events after harvest for this crop
		int noDates = 2;
		m_field->SetMDates(0, 0, g_date->DayInYear(25, 8)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(29, 8)); // last possible day of straw chopping
		m_field->SetMDates(0, 1, 0); // start day of hay bailing (not used as it depend on previous treatment)
		m_field->SetMDates(1, 1, g_date->DayInYear(29, 8)); // end day of hay bailing
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "DE_GrasslandSilageAnnual::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "DE_GrasslandSilageAnnual::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "DE_GrasslandSilageAnnual::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start woth the first event on the after winter time)
				d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
				if (g_date->Date() >= d1) d1 + 365;
				SimpleEvent_(d1, de_gsa_spring_harrow, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(31, 10);
		// OK, let's go.
		// LKM: Here we queue up the first event - 
		//
		SimpleEvent(d1, de_gsa_herbicide0, false);
	}
	break;
	// This is the first real farm operation
	case de_gsa_herbicide0:
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_herbicide0, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 14, de_gsa_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 14, de_gsa_ferti_p1, false, m_farm, m_field);
		break;
	case de_gsa_ferti_p1:
		// In total 40% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (20%) do it now
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again (until 20/8 when we will suceed)
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				DE_GSA_FERTI_P1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_plough, false, m_farm, m_field);
		break;
	case de_gsa_ferti_s1:
		// In total 40% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (20%) do it now
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				DE_GSA_FERTI_S1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_plough, false, m_farm, m_field);
		break;
	case de_gsa_stubble_plough:
		// 30% will do stubble plough, but rest will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(30))
		{
			if (!m_farm->StubblePlough(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				// 30% of farmers will do this, but the other 70% won't so we need to remember whether we are in one or the other group
				DE_GSA_STUBBLE_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->Date() + 1, de_gsa_autumn_harrow1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_harrow, false, m_farm, m_field);
		break;
	case de_gsa_autumn_harrow1: 
		// all farmers who did stubble plough (30%)
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_autumn_harrow1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 2, de_gsa_autumn_harrow2, false, m_farm, m_field);
		break;
	case de_gsa_autumn_harrow2:
		// 70% of the 30% who did stubble plough = 21% of all farmers
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->Date() + 7 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_autumn_harrow2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_gsa_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_gsa_ferti_p2, false, m_farm, m_field);
		break;
	case de_gsa_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_gsa_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_gsa_ferti_p2, false, m_farm, m_field);
		break;
	case de_gsa_ferti_p2:
		// In total 40% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (20%) do it now (if haven't done before)
		if ((m_ev->m_lock || m_farm->DoIt(static_cast<int>((20.0 / 80.0) * 100))) && (DE_GSA_FERTI_P1 == false))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p3, false, m_farm, m_field);
		break;
	case de_gsa_ferti_s2:
		// In total 40% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (20%) do it now (if haven't done before)
		if ((m_ev->m_lock || m_farm->DoIt(static_cast<int>((20.0 / 80.0) * 100))) && (DE_GSA_FERTI_S1 == false))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s3, false, m_farm, m_field);
		break;
	case de_gsa_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_winter_plough, false, m_farm, m_field);
		break;
	case de_gsa_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_winter_plough, false, m_farm, m_field);
		break;
	case de_gsa_winter_plough:
		// 60% will do winter plough, but rest will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(60))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_winter_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				// 60% of farmers will do this, but the other 40% won't so we need to remember whether we are in one or the other group
				DE_GSA_WINTER_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_gsa_spring_harrow, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_winter_stubble_cultivator_heavy, false, m_farm, m_field);
		break;
	case de_gsa_winter_stubble_cultivator_heavy:
		// the rest 40% who did not plough do heavy stubble cultivation
		if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_winter_stubble_cultivator_heavy, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_gsa_spring_harrow, false, m_farm, m_field);
		break;
	case de_gsa_spring_harrow:
		if ((m_ev->m_lock) || m_farm->DoIt(90))
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_spring_harrow, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(5, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(5, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_gsa_ferti_s4, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_gsa_ferti_p4, false, m_farm, m_field);
		break;
	case de_gsa_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p4, true, m_farm, m_field);
				break;
			}
			DE_GSA_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_heavy_cultivator, false, m_farm, m_field);
		break;
	case de_gsa_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s4, true, m_farm, m_field);
				break;
			}
			DE_GSA_SPRING_FERTI = 1;
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_heavy_cultivator, false, m_farm, m_field);
		break;
	case de_gsa_heavy_cultivator:
		if (DE_GSA_SPRING_FERTI == 1)
		{
			if (!m_farm->HeavyCultivatorAggregate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_heavy_cultivator, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_gsa_herbicide1, false, m_farm, m_field);
		break;
	case de_gsa_herbicide1:
		// Together 20% of farmers will do glyphosate spraying, but 15% before sow and the rest before emergence
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_herbicide1, true, m_farm, m_field);
				break;
			}
			DE_GSA_HERBICIDE1 = true;
		}
		// Queue up the next event
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
		}
		SimpleEvent_(d1, de_gsa_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_gsa_preseeding_cultivator:
		if (m_ev->m_lock || m_farm->DoIt(98))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_preseeding_cultivator, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_spring_sow, false, m_farm, m_field);
		break;
	case de_gsa_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, de_gsa_herbicide2, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_gsa_cut_to_silage1, false, m_farm, m_field); // Cutting thread
		break;
	case de_gsa_herbicide2:
		// Here comes the herbicide thread
		// Check biomass
		if (m_field->GetGreenBiomass() <= 0)
		{
			// The rest 10% of farmers do glyphosate spraying before emergence
			if ((m_ev->m_lock || (m_farm->DoIt(static_cast<int>((10.0 / 90.0) * 100))) && (DE_GSA_HERBICIDE1 == false)))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_gsa_herbicide2, true, m_farm, m_field);
					break;		
				}
				DE_GSA_HERBI_DATE = g_date->Date();
			}
		}
		// End of thread
		break;
	case de_gsa_cut_to_silage1:
		// Here comes cutting thread
		if (DE_GSA_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage1, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 7), de_gsa_cut_to_silage2, false, m_farm, m_field);
		break;
	case de_gsa_cut_to_silage2:
		if (DE_GSA_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage2, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt(73))
			{
				if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage2, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_GrasslandSilageAnnual::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}