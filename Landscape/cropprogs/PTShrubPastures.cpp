/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PTShrubPastures.cpp This file contains the source for the PTShrubPastures class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PTShrubPastures.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PTShrubPastures.h"

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional PermanentGrassGrazed.
*/
bool PTShrubPastures::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_PTShrubPastures; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case pt_sp_start:
	{

		// Set up the date management stuff

		PT_FL1_CATTLEOUT_DATE = 0;
		PT_FL2_CATTLEOUT_DATE = 0;
		PT_FL3_CATTLEOUT_DATE = 0;
		PT_SP_YEARS_AFTER_PLOUGH = 0; // Here, similar to PTPermanentGrassGrazed, the initial conditions for field (no of years after ploughing) should be defined outside the crop management plan,
										// at the beginning of simulation. For now all fields are starting with 0

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {
			//Checking the future...
			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(30, 12) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PTShrubPasture::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PTShrubPasture::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PTShrubPasture::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				//Only for the first year...
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1), pt_sp_cattle_out2, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // OK, let's go.
		 // Here we queue up the first event
		d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
		if (g_date->Date() >= d1) d1 += 365;
		if ((PT_SP_YEARS_AFTER_PLOUGH + g_date->GetYearNumber()) % 3 == 0)
		{
			SimpleEvent_(d1, pt_sp_cattle_out2, false, m_farm, m_field);
		}
		SimpleEvent_(d1, pt_sp_cattle_out1, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case pt_sp_cattle_out1:
		if (!m_farm->CattleOutLowGrazing(m_field, 0.0, g_date->DayInYear(10, 1) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_out1, true, m_farm, m_field);
			break;
		}
		// Keep them out there
		PT_FL1_CATTLEOUT_DATE = g_date->DayInYear();
		SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_is_out1, false, m_farm, m_field);
		break;
	case pt_sp_cattle_is_out1:    // Keep the cattle out there
								   // CattleIsOutLow() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOutLow2(m_field, 0.0, g_date->DayInYear() - PT_FL1_CATTLEOUT_DATE, g_date->DayInYear(28, 12), 365)) {
			SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_is_out1, false, m_farm, m_field);
			break;
		}
		done = true;
		break;
	case pt_sp_cattle_out2:
		if (!m_farm->CattleOutLowGrazing(m_field, 0.0, g_date->DayInYear(10, 1) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_out2, true, m_farm, m_field);
			break;
		}
		// Keep them out there
		PT_FL2_CATTLEOUT_DATE = g_date->DayInYear();
		SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_is_out2, false, m_farm, m_field);
		break;
	case pt_sp_cattle_is_out2:    // Keep the cattle out there
								   // CattleIsOutLow() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOutLow2(m_field, 0.0, g_date->DayInYear() - PT_FL2_CATTLEOUT_DATE, g_date->DayInYear(30, 1), 30)) {
			SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_is_out2, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2), pt_sp_spring_plough, false, m_farm, m_field);
		break;
	case pt_sp_spring_plough:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_sp_spring_plough, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_out3, false, m_farm, m_field); //need to check if cattle can go out next day after ploughing or the soil needs to rest a little bit
		break;
	case pt_sp_cattle_out3:
		if (!m_farm->CattleOutLowGrazing(m_field, 0.0, g_date->Date() + 3 - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_out3, true, m_farm, m_field);
			break;
		}
		PT_FL3_CATTLEOUT_DATE = g_date->DayInYear();
		// Keep them out there
		SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_is_out3, false, m_farm, m_field);
		break;
	case pt_sp_cattle_is_out3:    // Keep the cattle out there
								   // CattleIsOutLow() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOutLow2(m_field, 0.0, g_date->DayInYear() - PT_FL3_CATTLEOUT_DATE, g_date->DayInYear(28, 12), 330)) {
			SimpleEvent_(g_date->Date() + 1, pt_sp_cattle_is_out3, false, m_farm, m_field);
			break;
		}
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "PTShrubPastures::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}