/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_ChristmasTrees_Perm1_est.cpp This file contains the source for the DK_ChristmasTrees_Perm1_est class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 \n
*/
//
// DK_ChristmasTrees_Perm1.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_ChristmasTrees_Perm1_est.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_ChristmasTrees_Perm1_SkScrapes("DK_CROP_CTP1_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_ChristmasTrees_Perm1_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_CTP1_InsecticideDay;
extern CfgInt   cfg_CTP1_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_ChristmasTrees_Perm1_est::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKChristmasTrees_Perm1_est;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_ctp1_start:
	{
		a_field->ClearManagementActionSum();
		m_last_date = g_date->DayInYear(6, 10); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(5, 10); // last possible day of in this case row cultivation autumn (no harvest in this code) 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(6, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) harrow autumn

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(0, flexdates, int(dk_ctp1_plough1_autumn))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(20, 8);
		// OK, let's go.
		// LKM: Here we queue up the first event 
		SimpleEvent(d1, dk_ctp1_plough1_autumn, false);
		break;
	}
	break; // this code represents 1st year establishment in autumn (spring is also possible, but not coded)

	case dk_ctp1_plough1_autumn:
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) // on clay soils (NL KLEI & VEEN)
		{
			if (!a_farm->AutumnPlough(a_field, 0.0,
				g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp1_plough1_autumn, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp1_plough2_autumn, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp1_depth_plough_autumn, false);
		break; // sandy soils

	case dk_ctp1_depth_plough_autumn:
		if (!a_farm->DeepPlough(a_field, 0.0,
			g_date->DayInYear(10, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp1_depth_plough_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp1_stubble_harrow_autumn, false);
		break;

	case dk_ctp1_plough2_autumn:
		if (!a_farm->AutumnPlough(a_field, 0.0,
			g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp1_plough2_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp1_sow_cover_crop_autumn, false);
		break;

	case dk_ctp1_stubble_harrow_autumn:
		if (!a_farm->StubbleHarrowing(a_field, 0.0,
			g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp1_stubble_harrow_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp1_sow_cover_crop_autumn, false);
		break;

	case dk_ctp1_sow_cover_crop_autumn:
		if (!a_farm->AutumnSow(a_field, 0.0,
			g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp1_sow_cover_crop_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp1_plant_trees_autumn, false);
		break;
		
	case dk_ctp1_plant_trees_autumn:
		if (!a_farm->AutumnSow(a_field, 0.0,
			g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp1_plant_trees_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp1_row_cultivation_autumn, false);
		break;

	case dk_ctp1_row_cultivation_autumn:
		if (!a_farm->RowCultivation(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp1_row_cultivation_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp1_harrow_autumn, false);
		break;

	case dk_ctp1_harrow_autumn:
		if (!a_farm->AutumnHarrow(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp1_harrow_autumn, true);
			break;
		}
		done = true;
		break;

		// So we are done, and somewhere else the farmer will queue up the start event of the DK_ChristmasTrees_Perm2 (for 4 years) if conventional christmas tree management, the start event of the DK_OChristmasTrees_Perm2 (for 4 years) if organic christmas tree management, 
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "DK_ChristmasTrees_Perm1_est::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}