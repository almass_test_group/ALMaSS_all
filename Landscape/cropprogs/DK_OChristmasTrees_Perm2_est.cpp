/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OChristmasTrees_Perm2_est.cpp This file contains the source for the DK_OChristmasTrees_Perm2_est class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 \n
*/
//
// DK_OChristmasTrees_Perm2_est.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OChristmasTrees_Perm2_est.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_OChristmasTrees_Perm2_est_SkScrapes("DK_CROP_OCTP2E_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_OChristmasTrees_Perm2_est_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_OCTP2E_InsecticideDay;
extern CfgInt   cfg_OCTP2E_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DK_OChristmasTrees_Perm2_est::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKOChristmasTrees_Perm2_est;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_octp2e_start:
	{
		DK_OCTP2E = 0; //Here flags should get randomly for each field a value from 1 to 10 ->
		
		a_field->ClearManagementActionSum();
		m_last_date = g_date->DayInYear(30, 9); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 8); // last possible day of in this case harrow (no harvest in this code) 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(29, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) harrow
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 12
		flexdates[2][1] = g_date->DayInYear(30, 9); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) plant trees

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(365, flexdates, int(dk_octp2e_crush_trees))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(10, 3);
		// OK, let's go.
		// LKM: Here we queue up the first event 
		SimpleEvent(d1, dk_octp2e_crush_trees, false);
		break;
	}
	break;
 
// crush/cut all "old trees"
	case  dk_octp2e_crush_trees:
		if (!m_farm->CutOrch(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp2e_crush_trees, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_octp2e_sow_catch_crops, false);
		break;

	case dk_octp2e_sow_catch_crops: //some do this (suggest 50%) others sow inter crops
		if (m_ev->m_lock || m_farm->DoIt_prob(.50)) {
			if (!m_farm->AutumnSow(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_octp2e_sow_catch_crops, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_octp2e_plant_trees, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_octp2e_sow_inter_crops, false);
		break; 

	case dk_octp2e_sow_inter_crops:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp2e_sow_inter_crops, true);
			break;
		}
		SimpleEvent(g_date->Date() + 50, dk_octp2e_harrow, false);
		break;

	case dk_octp2e_harrow:
		if (!m_farm->AutumnHarrow(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp2e_harrow, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp2e_plant_trees, false);
		break;

	case dk_octp2e_plant_trees:
		if (!m_farm->AutumnSow(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp2e_plant_trees, true);
			break;
		}
		done = true;
		break;

		// So we are done, and somewhere else the farmer will queue up the start event of the next crop - DKOChristmasTrees_Perm2 (4 years)
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "DK_OChristmasTrees_Perm2_est::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}