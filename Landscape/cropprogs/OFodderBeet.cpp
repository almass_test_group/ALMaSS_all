//
// OFodderBeet.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OFodderBeet.h"

bool OFodderBeet::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	//  int d1;

	bool done = false;

	switch (m_ev->m_todo)
	{
	case ofb_start:
	{
		a_field->ClearManagementActionSum();

					  m_field->SetVegPatchy(true); // Root crop so open
					  // Set up the date management stuff
					 // Could save the start day in case it is needed later
					 // m_field->m_startday = m_ev->m_startday;
					 m_last_date = g_date->DayInYear(10, 11);
					 // Start and stop dates for all events after harvest
					 int noDates = 1;
					 m_field->SetMDates(0, 0, g_date->DayInYear(10, 10));
					 // 0,0 determined by harvest date - used to see if at all possible
					 m_field->SetMDates(1, 0, g_date->DayInYear(10, 11));
					 // Check the next crop for early start, unless it is a spring crop
					 // in which case we ASSUME that no checking is necessary!!!!
					 // So DO NOT implement a crop that runs over the year boundary

					 //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
					 int d1;
					 if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

						 if (m_ev->m_startday>g_date->DayInYear(1, 7))
						 {
							 if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
							 {
								 g_msg->Warn(WARN_BUG, "OFodderBeet::Do(): "
									 "Harvest too late for the next crop to start!!!", "");
								 exit(1);
							 }
							 // Now fix any late finishing problems
							 for (int i = 0; i<noDates; i++) {
								 if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
									 m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
								 }
								 if (m_field->GetMDates(1, i) >= m_ev->m_startday){
									 m_field->SetMConstants(i, 0);
									 m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
								 }
							 }
						 }
						 // Now no operations can be timed after the start of the next crop.

						 // CJT note:
						 // Start single block date checking code to be cut-'n-pasted...

						 if (!m_ev->m_first_year)
						 {
							 // Are we before July 1st?
							 d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
							 if (g_date->Date() < d1)
							 {
								 // Yes, too early. We assumme this is because the last crop was late
								 g_msg->Warn(WARN_BUG, "OFodderBeet::Do(): "
									 "Crop start attempt between 1st Jan & 1st July", "");
								 exit(1);
							 }
							 else
							 {
								 d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
								 if (g_date->Date() > d1)
								 {
									 // Yes too late - should not happen - raise an error
									 g_msg->Warn(WARN_BUG, "OFodderBeet::Do(): "
										 "Crop start attempt after last possible start date", "");
									 exit(1);
								 }
							 }
						 }
						 else
						 {
							 // If this is the first year of running then it is possibel to start
							 // on day 0, so need this to tell us what to do:
							 SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3),
								 ofb_spring_plough, false);
							 break;
						 }
					 }//if

					 // End single block date checking code. Please see next line
					 // comment as well.
					 // Reinit d1 to first possible starting date.
					 d1 = g_date->OldDays() + g_date->DayInYear(1, 10); // Was 1,10
					 if (g_date->Date() > d1) {
						 d1 = g_date->Date();
					 }
					 // OK, let's go.
					 SimpleEvent(d1, ofb_autumn_plough, false);
	}
		break;

	case ofb_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->AutumnPlough(m_field, 0.0,
				g_date->DayInYear(15, 12) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_autumn_plough, true);
				break;
			}
			// Did autumn plough, so skip manure and spring plough.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 3) + 365,
				ofb_start_threads_one, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 3) + 365,
			ofb_fertmanure, false);
		break;

	case ofb_fertmanure:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->FA_Manure(m_field, 0.0,
				g_date->DayInYear(9, 4) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_fertmanure, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), ofb_spring_plough, false);
		break;

	case ofb_spring_plough:
		if (m_ev->m_lock || m_farm->DoIt(60))
		{
			if (!m_farm->SpringPlough(m_field, 0.0,
				g_date->DayInYear(10, 4) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_spring_plough, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, ofb_start_threads_one, false);
		break;

	case ofb_start_threads_one:
		// Today is the 15th of March, at the least.
		OFB_DID_HARROW = false;
		OFB_DID_NPKS_ONE = false;
		OFB_DID_SLURRY = false;
		OFB_SOW_DATE = 0;
		SimpleEvent(g_date->Date(), ofb_spring_harrow, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 4),
			ofb_fertnpks_one, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 4),
			ofb_fertslurry, false);
		break;

	case ofb_spring_harrow:
		if (!m_farm->SpringHarrow(m_field, 0.0,
			g_date->DayInYear(11, 4) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ofb_spring_harrow, true);
			break;
		}
		OFB_DID_HARROW = true;
		if (OFB_DID_NPKS_ONE && OFB_DID_SLURRY) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4),
				ofb_spring_sow, false);
		}
		break;

	case ofb_fertnpks_one:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(5, 5) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ofb_fertnpks_one, true);
			break;
		}
		OFB_DID_NPKS_ONE = true;
		if (OFB_DID_HARROW && OFB_DID_SLURRY) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4),
				ofb_spring_sow, false);
		}
		break;

	case ofb_fertslurry:
		if (!m_farm->FA_Slurry(m_field, 0.0,
			g_date->DayInYear(5, 5) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ofb_fertslurry, true);
			break;
		}
		OFB_DID_SLURRY = true;
		if (OFB_DID_HARROW && OFB_DID_NPKS_ONE) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4),
				ofb_spring_sow, false);
		}
		break;

	case ofb_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0,
			g_date->DayInYear(1, 5) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ofb_spring_sow, true);
			break;
		}
		OFB_SOW_DATE = g_date->Date();
		SimpleEvent(g_date->Date(), ofb_spring_roll, false);
		break;

	case ofb_spring_roll:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->SpringRoll(m_field, 0.0,
				g_date->DayInYear(15, 4) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_spring_roll, true);
				break;
			}
		}
		{
			int d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
			if (d1 < OFB_SOW_DATE + 14) {
				d1 = OFB_SOW_DATE;
			}
			SimpleEvent(d1, ofb_row_cultivation_one, false);
		}
		break;


	case ofb_row_cultivation_one:
		if (m_ev->m_lock || m_farm->DoIt(100))
		{
			if (!m_farm->RowCultivation(m_field, 0.0,
				g_date->DayInYear(25, 5) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_row_cultivation_one, true);
				break;
			}
			// Did first row cultivation, queue up the second too.
			OFB_DID_ROW_TWO = false;
			{
				int d1 = g_date->Date() + 14;
				if (d1 < g_date->OldDays() + g_date->DayInYear(17, 5)) {
					d1 = g_date->OldDays() + g_date->DayInYear(17, 5);
				}
				SimpleEvent(d1, ofb_row_cultivation_two, false);
			}
		}
		else {
			// Didn't do row cultivation on this field. Signal already done.
			OFB_DID_ROW_TWO = true;
		}
		OFB_DID_NPKS_TWO = false;
		OFB_DID_WATER_ONE = false;
		OFB_TRULY_DID_WATER_ONE = false;
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6),
			ofb_fertnpks_two, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7),
			ofb_water_one, false);
		break;

	case ofb_row_cultivation_two:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(15, 6) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ofb_row_cultivation_two, true);
			break;
		}
		OFB_DID_ROW_TWO = true;
		if (OFB_DID_NPKS_TWO   &&
			OFB_DID_WATER_ONE
			) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8),
				ofb_water_two, false);
		}
		break;

	case ofb_fertnpks_two:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(15, 6) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_fertnpks_two, true);
				break;
			}
		}
		OFB_DID_NPKS_TWO = true;
		if (OFB_DID_ROW_TWO    &&
			OFB_DID_WATER_ONE
			) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8),
				ofb_water_two, false);
		}
		break;

	case ofb_water_one:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->Water(m_field, 0.0,
				g_date->DayInYear(30, 7) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_water_one, true);
				break;
			}
			OFB_TRULY_DID_WATER_ONE = true;
		}
		OFB_DID_WATER_ONE = true;
		if (OFB_DID_ROW_TWO    &&
			OFB_DID_NPKS_TWO
			) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8),
				ofb_water_two, false);
		}
		break;


	case ofb_water_two:
		if (OFB_TRULY_DID_WATER_ONE)
		{
			if (!m_farm->Water(m_field, 0.0,
				g_date->DayInYear(30, 8) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_water_two, true);
				break;
			}
		}
		ChooseNextCrop(1);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 10),
			ofb_harvest, false);
		break;

	case ofb_harvest:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->Harvest(m_field, 0.0, -1)) { //raise an error
				g_msg->Warn(WARN_BUG, "OFodderBeet::Do(): failure in 'Harvest' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ofb_harvest, true);
				break;
			}
		}
		m_field->SetVegPatchy(false);
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "OFodderBeet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}

	return done;
}


