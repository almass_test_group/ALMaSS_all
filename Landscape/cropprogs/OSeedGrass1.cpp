//
// OSeedGrass1.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OSeedGrass1.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool OSeedGrass1::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  // NB IF SEED GRASS IS USED IN A ROTATION IT MUST FOLLOW AN UNDERSOWN CROP!!!

  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case osg1_start:
  {
    // End date checking block.
    SG1_WATER_DATE = g_date->Date();
    a_field->ClearManagementActionSum();

          // Set up the date management stuff
      m_last_date=g_date->DayInYear(15,8);
      // Start and stop dates for all events after harvest
      int noDates= 4;
      m_field->SetMDates(0,0,g_date->DayInYear(10,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,7));
      m_field->SetMDates(0,1,g_date->DayInYear(11,7));
      m_field->SetMDates(1,1,g_date->DayInYear(25,7));
      m_field->SetMDates(0,2,g_date->DayInYear(26,7));
      m_field->SetMDates(1,2,g_date->DayInYear(20,7));
      m_field->SetMDates(0,3,g_date->DayInYear(15,7));
      m_field->SetMDates(1,3,g_date->DayInYear(15,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	  int d1;
	  if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OSeedGrass1::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.

      int today=g_date->Date();
      d1 = g_date->OldDays() + m_first_date;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if (today > d1)
      {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, "OSeedGrass1::Do(): "
		 "Crop start attempt after last possible start date", "" );
          exit( 1 );
      }
	  }//if

      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
	  m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification
	  // OK, let's go.
    SimpleEvent( d1, osg1_ferti_zero, false );
    break;
  }

  case osg1_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
      if (!m_farm->FP_NPK( m_field, 0.0,
			   g_date->DayInYear( 15, 4 ) -
			   g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osg1_ferti_zero, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ),
                 osg1_water_zero, false );
    break;

  case osg1_water_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 70 )) 
    {
      if ( !m_farm->Water( m_field, 0.0,
			   g_date->DayInYear( 30, 5 ) -
			   g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osg1_water_zero, true );
	break;
      }
      SG1_WATER_DATE = g_date->Date();
      {
	int d1 = g_date->Date() + 7;
	if ( d1 > g_date->OldDays() + g_date->DayInYear( 1, 6 ))
	  d1 = g_date->OldDays() + g_date->DayInYear( 1, 6 );
	SimpleEvent( d1, osg1_water_zero_b, true );
	break;
      }
    }
    // Didn't do first water, so skip the second one too.
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
		 osg1_swarth, false );
    break;

  case osg1_water_zero_b:
    if ( m_ev->m_lock ) 
    {
      if ( !m_farm->Water( m_field, 0.0, g_date->DayInYear( 30, 6 ) - g_date->DayInYear())) 
      {
            SimpleEvent( g_date->Date() + 1, osg1_water_zero_b, true );
            break;
      }
      SG1_WATER_DATE = g_date->Date();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
		 osg1_swarth, false );
    break;

  case osg1_swarth:
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if ( !m_farm->Swathing( m_field, 0.0,
			      g_date->DayInYear( 15, 7 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osg1_swarth, true );
        break;
      }
	  ChooseNextCrop (4);
      SimpleEvent( g_date->Date() + 2, osg1_harvest, false );
      break;
    }
    ChooseNextCrop (4);
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ), osg1_harvest, false );
    break;

  case osg1_harvest:
    if (m_field->GetMConstants(0)==0) {
			if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "OSeedGrass1::Do(): failure in 'Harvest' execution", "" );
				exit( 1 );
			} 
	}
	else { 
		if ( !m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, osg1_harvest, false );
			break;
		}
	}

    SimpleEvent( g_date->Date(), osg1_strawchopping, false );
    break;

  case osg1_strawchopping:
    if ( m_ev->m_lock || m_farm->DoIt( 30 )) {
		if (m_field->GetMConstants(0)==0) {
			if (!m_farm->StrawChopping( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "OSeedGrass1::Do(): failure in 'StrawChopping' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if ( !m_farm->StrawChopping( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, osg1_strawchopping, true );
			break;
			}
		}
		done = true;
		break;
    }

    {
      int d1 = g_date->Date()+1;
      int d2 = g_date->OldDays() + m_field->GetMDates(0,1);
      if (d1>d2) d1=d2;
      SimpleEvent( d1, osg1_compress, false );
    }
    break;

  case osg1_compress:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
		if (m_field->GetMConstants(1)==0) {
			if (!m_farm->HayTurning( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "OSeedGrass1::Do(): failure in 'HayTurning' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if ( !m_farm->HayTurning( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, osg1_compress, true );
				break;
			}
		}
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + m_field->GetMDates(0,3))
		d1 = g_date->OldDays() + m_field->GetMDates(0,3);
		SimpleEvent( d1, osg1_burn_straw, false );
    }
    break;

  case osg1_burn_straw:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
		if (m_field->GetMConstants(3)==0) {
			if (!m_farm->BurnStrawStubble( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "OSeedGrass1::Do(): failure in 'BurnStrawStubble' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if ( !m_farm->BurnStrawStubble( m_field, 0.0,m_field->GetMDates(1,3) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, osg1_burn_straw, true );
				break;
			}
		}
    }
	done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OSeedGrass1::Do(): " "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


