//
// SpringBarleyCloverGrassStrigling.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SpringBarleyCloverGrassStrigling.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgFloat cfg_strigling_prop;

bool SpringBarleyCloverGrassStrigling::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1=0;
  int noDates=3;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case sbcgs_start:
    {
      SBCGS_ISAUTUMNPLOUGH=false;
      SBCGS_FERTI_DONE=false;
      SBCGS_SPRAY=0;
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(15,10);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(1,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(25,8));
      m_field->SetMDates(0,1,g_date->DayInYear(5,8));
      m_field->SetMDates(1,1,g_date->DayInYear(30,8));
      m_field->SetMDates(0,2,g_date->DayInYear(1,9));
      m_field->SetMDates(1,2,g_date->DayInYear(15,10));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // SO DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.
      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // start day
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
		  m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification
		  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 ),
                                                    sbcgs_spring_plough, false );
         break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
	  d1 = g_date->OldDays() + g_date->DayInYear( 1, 11 );
      if (g_date->Date() >= d1) d1 += 365;
      SimpleEvent( d1, sbcgs_autumn_plough, false );
    }
    break;

  case sbcgs_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 70 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 30,11 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbcgs_autumn_plough, true );
        break;
      }
      SBCGS_ISAUTUMNPLOUGH=true;
    }
    // +365 for next year
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365,
                 sbcgs_ferti_s1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365,
		             sbcgs_ferti_s2, false );
    break;

  case sbcgs_ferti_s1:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbcgs_ferti_s1, true );
        break;
      }
      // Done fertilizer so remember
      SBCGS_FERTI_DONE=true;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 ),
                 sbcgs_spring_plough, false );
    break;

  case sbcgs_ferti_s2:
    if (( m_ev->m_lock || m_farm->DoIt( 67 ) ) && (!SBCGS_ISAUTUMNPLOUGH))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date()+1, sbcgs_ferti_s2, true );
        break;
      }
      else
      {
        // Done fertilizer so remember
        SBCGS_FERTI_DONE=true;
      }
    }
    break;

  case sbcgs_spring_plough:
    if ( !SBCGS_ISAUTUMNPLOUGH ) // Don't plough if you have already
    {
      if (!m_farm->SpringPlough( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbcgs_spring_plough, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
                 sbcgs_spring_harrow, false );
    break;

  case sbcgs_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 10,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbcgs_spring_harrow, true );
      break;
    }
    SimpleEvent( g_date->Date(), sbcgs_ferti_s3, false );
    break;

  case sbcgs_ferti_s3: // The catch all
    if ((!SBCGS_FERTI_DONE)||( m_ev->m_lock || m_farm->DoIt( 70 ) ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date()+1, sbcgs_ferti_s3, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
                 sbcgs_spring_sow, false );
    break;

  case sbcgs_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbcgs_spring_sow, true );
      break;
    }
    SimpleEvent( g_date->Date()+1, sbcgs_spring_roll, false );
    break;

  case sbcgs_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 21,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbcgs_spring_roll, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 ),
             sbcgs_strigling1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
            sbcgs_strigling_sow, false );
    break;

case sbcgs_strigling1:
  if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 90 )))
  {
    if (!m_farm->Strigling( m_field, 0.0,
              g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbcgs_strigling1, true );
      break;
    }
  }
// End of thread

  case sbcgs_strigling_sow:
  if ( m_ev->m_lock || m_farm->DoIt( 100 ))
  {
    if (!m_farm->StriglingSow( m_field, 0.0,
               g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbcgs_strigling_sow, true );
      break;
    }
  }
  // Carry on with the next after first treatment or no treatment
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
             sbcgs_GR, false );
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
             sbcgs_fungicide, false );
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
             sbcgs_water1, false );
  break;


  // GR Thread
  case sbcgs_GR:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (5*cfg_greg_app_prop.value())))      // was 60
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
           g_date->DayInYear( 25,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbcgs_GR, true );
        break;
      }
    }
    break;

  // Water thread
  case sbcgs_water1:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if ((!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 15,6 ) - g_date->DayInYear()))
              || (SBCGS_SPRAY==g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, sbcgs_water1, true );
        break;
      }
      if (g_date->DayInYear()+5<g_date->DayInYear( 16,6 ))
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16,6 ),
                     sbcgs_water2, false );
      else
        SimpleEvent( g_date->Date()+5, sbcgs_water2, false );
    }
    break;

  case sbcgs_water2:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if ((!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 1,7 ) - g_date->DayInYear()))
              || (SBCGS_SPRAY==g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, sbcgs_water2, true );
        break;
      }
    }
    break;

  // Fungicide thread  & MAIN THREAD
  case sbcgs_fungicide:
    if ( m_ev->m_lock || m_farm->DoIt(  (int) (40*cfg_herbi_app_prop.value() )))    // was 60
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
           g_date->DayInYear( 25,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbcgs_fungicide, true );
        break;
      }
      else
      {
        SBCGS_SPRAY=g_date->DayInYear();
      }
    }
    // can also try insecticide
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
                 sbcgs_insecticide, false );
    break;

  // Insecticide & MAIN THREAD
  case sbcgs_insecticide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (35*cfg_ins_app_prop1.value() )))     // was 50
    {
      if (!m_farm->InsecticideTreat( m_field, 0.0,
           g_date->DayInYear( 10,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbcgs_insecticide, true );
        break;
      }
      else SBCGS_SPRAY=g_date->DayInYear();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                 sbcgs_harvest, false );
    break;

  case sbcgs_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         m_field->GetMDates(1,0) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbcgs_harvest, true );
      break;
    }
     SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
                 sbcgs_hay_baling, false );
    break;

  case sbcgs_hay_baling:
    if (!m_farm->HayBailing( m_field, 0.0,
         m_field->GetMDates(1,1) - g_date->DayInYear()))
    {
      SimpleEvent( g_date->Date() + 1, sbcgs_hay_baling, true );
      break;
    }
    // Something special here.
    // If the cattle out period is very short then we don't want to do it at all
    if (m_field->GetMDates(1,2)-m_field->GetMDates(0,2)<14) done=true;
    else SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
                   sbcgs_cattle_out, false );
    break;

  case sbcgs_cattle_out:
// ***CJT*** 010904 possibly causing a problem with unsprayed margins
	  // Uncomment the break if this is a problem
done=true;
//break;
// **CJT*** END
    if (!m_farm->CattleOut( m_field, 0.0,
         m_field->GetMDates(1,2) - g_date->DayInYear()))
    {
      SimpleEvent( g_date->Date() + 1, sbcgs_cattle_out, true );
      break;
    }
    SimpleEvent( g_date->Date() + 1, sbcgs_cattle_is_out, false );
    break;

  case sbcgs_cattle_is_out:
    if (!m_farm->CattleIsOut( m_field, 0.0,
         m_field->GetMDates(1,2) - g_date->DayInYear(),m_field->GetMDates(1,2)))
    {
      SimpleEvent( g_date->Date() + 1, sbcgs_cattle_is_out, false );
      break;
    }
    // END OF MAIN THREAD
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


