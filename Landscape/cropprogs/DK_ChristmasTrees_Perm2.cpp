/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_ChristmasTrees_Perm2.cpp This file contains the source for the DK_ChristmasTrees_Perm2 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 \n
*/
//
// DK_ChristmasTrees_Perm2.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_ChristmasTrees_Perm2.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_ChristmasTrees_Perm2_SkScrapes("DK_CROP_CTP2_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_ChristmasTrees_Perm2_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_CTP1_InsecticideDay;
extern CfgInt   cfg_CTP1_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DK_ChristmasTrees_Perm2::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKChristmasTrees_Perm2;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_ctp2_start:
	{
		DK_CTP2_AFTER_EST = 0; //Here flags should get randomly for each field a value
		
		a_field->ClearManagementActionSum();
		m_last_date = g_date->DayInYear(16, 11); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(16, 8); // last possible day in this case of sowing catch crop (no harvest in this code) 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(16, 11); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) herbicide

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_ctp2_ferti_sand_s2))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + isSpring;
		// OK, let's go.
		// LKM: Here we queue up the first event 
		if ((DK_CTP2_AFTER_EST + g_date->GetYearNumber()) % 4 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, dk_ctp2_ferti_sand_s2, false);
		}
		else if ((DK_CTP2_AFTER_EST + g_date->GetYearNumber()) % 4 == 3)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, dk_ctp2_npk_s5, false);
		}
		else
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, dk_ctp2_ferti_sand_s3_4, false);
		}
		break;
	}
	break;

// year 2 (after establishment)
	case dk_ctp2_ferti_sand_s2: 
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6)  // on sandy soils (NL ZAND & LOSS)
		{
			if (a_farm->IsStockFarmer()) {
				if (!m_farm->FA_Manure(m_field, 0.0,
					g_date->DayInYear(30, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ctp2_ferti_sand_s2, true);
					break;
				}
				SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_2, false);
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp2_ferti_sand_p2, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp2_ferti_clay_s2, false);
		break; // clay soils

	case dk_ctp2_ferti_sand_p2:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_ferti_sand_p2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_2, false);
		break;

	case dk_ctp2_ferti_clay_s2:
		if (a_farm->IsStockFarmer()) {
			if (!m_farm->FA_Manure(m_field, 0.0,
				g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp2_ferti_clay_s2, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_2, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp2_ferti_clay_p2, false);
		break;

	case dk_ctp2_ferti_clay_p2:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_ferti_clay_p2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_2, false);
		break;

	case dk_ctp2_manual_weeding_2:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide1_2, false);
		break;

	case dk_ctp2_herbicide1_2:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide1_2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp2_herbicide2_2, false);
		break;

	case dk_ctp2_herbicide2_2:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide2_2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp2_herbicide3_2, false);
		break;

	case dk_ctp2_herbicide3_2:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide3_2, true);
			break;
		}// end of year 2 (after establishment)
		done = true;
		break;

		// year 3+4 (after establishment)
	case dk_ctp2_ferti_sand_s3_4:
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6)  // on sandy soils (NL ZAND & LOSS)
		{
			if (a_farm->IsStockFarmer())
			{
				if (!m_farm->FA_Manure(m_field, 0.0,
					g_date->DayInYear(30, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ctp2_ferti_sand_s3_4, true);
					break;
				}
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp2_npk_sand1_s3_4, false); // fertilizer thread main thread
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp2_ferti_sand_p3_4, false); // fertilizer thread main thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp2_ferti_clay_s3_4, false);
		break; 


	case dk_ctp2_ferti_sand_p3_4:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_ferti_sand_p3_4, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp2_npk_sand1_p3_4, false); // fertilizer thread main thread
		break;

	case dk_ctp2_ferti_clay_s3_4:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_Manure(m_field, 0.0,
				g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp2_ferti_clay_s3_4, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_clay_s3_4, false);
			break;
		}
		else 

	case dk_ctp2_npk_clay_s3_4:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_clay_s3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_3_4, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp2_grazing_3_4, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp2_sow_catch_crop, false);
		break;

	case dk_ctp2_npk_sand1_s3_4:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_sand1_s3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_3_4, false); // weeding / herbicide thread
		SimpleEvent(g_date->Date() + 1, dk_ctp2_grazing_3_4, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp2_npk_sand2_s3_4, false);
		break;

	case dk_ctp2_npk_sand1_p3_4:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_sand1_p3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_3_4, false); // weeding / herbicide thread
		SimpleEvent(g_date->Date() + 1, dk_ctp2_grazing_3_4, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp2_npk_sand2_p3_4, false);
		break;

	case dk_ctp2_manual_weeding_3_4:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide1_3_4, false);
		break;

	case dk_ctp2_herbicide1_3_4:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide1_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp2_herbicide2_3_4, false);
		break;

	case dk_ctp2_herbicide2_3_4:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide2_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp2_herbicide3_3_4, false);
		break;

	case dk_ctp2_herbicide3_3_4:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide3_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_cutting_3_4, false);
		break;

	case dk_ctp2_manual_cutting_3_4: // no specific timing for this trimming event
		if (!m_farm->CutOrch(m_field, 0.0,
			g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_cutting_3_4, true);
			break;
		}
		break;
		
		// end of weeding / herbicide thread

	case dk_ctp2_grazing_3_4:
		if (!m_farm->PigsOut(m_field, 0.0,
			g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_grazing_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_pig_is_out_3_4, false);
		break;
	case dk_ctp2_pig_is_out_3_4:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_pig_is_out_3_4, false);
			break;
		}
		break; // end of grazing thread
	
	case dk_ctp2_npk_sand2_s3_4:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_sand2_s3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_sow_catch_crop, false);
		break;

	case dk_ctp2_npk_sand2_p3_4:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_sand2_p3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_sow_catch_crop, false);
		break;

	// year 5 (after establishment)
	case dk_ctp2_npk_s5:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_s5, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_5, false); // weeding / herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp2_grazing_5, false); // grazing thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp2_npk_sand_s5, false); // fertilizer thread - main thread
			break;
		}// here comes a fork of parallel events:
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp2_npk_p5, false);
		break;

	case dk_ctp2_npk_p5:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_p5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_5, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp2_grazing_5, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp2_npk_sand_p5, false); // fertilizer thread - main thread
		break;
		
	case dk_ctp2_manual_weeding_5:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_weeding_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide1_5, false);
		break;

	case dk_ctp2_herbicide1_5:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide1_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp2_herbicide2_5, false);
		break;

	case dk_ctp2_herbicide2_5:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide2_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp2_herbicide3_5, false);
		break;

	case dk_ctp2_herbicide3_5:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide3_5, true);
			break;
		} 
		SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_cutting_5, false);
		break;

	case dk_ctp2_manual_cutting_5: // no specific timing for this trimming event
		if (!m_farm->CutOrch(m_field, 0.0,
			g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_manual_cutting_5, true);
			break;
		}
		break;
	
	case dk_ctp2_grazing_5:
		if (!m_farm->PigsOut(m_field, 0.0,
			g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_grazing_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_pig_is_out_5, false);
		break;
	case dk_ctp2_pig_is_out_5:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_pig_is_out_5, false);
			break;
		}
		break; // end of grazing thread

	case dk_ctp2_npk_sand_s5:
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6)  // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_sand_s5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_sow_catch_crop, false);
		break;

	case dk_ctp2_npk_sand_p5:
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6)  // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->FP_NPK(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp2_npk_sand_p5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp2_sow_catch_crop, false);
		break;

	case dk_ctp2_sow_catch_crop:
		if (!m_farm->AutumnSow(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_sow_catch_crop, true);
			break;
		}
		SimpleEvent(g_date->Date() + 90, dk_ctp2_herbicide4, false);
		break;

	case dk_ctp2_herbicide4:
		if (!m_farm->HerbicideTreat(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp2_herbicide4, true);
			break;
		}
		//end of year 3+4 and 5 (after establishment)
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop - 5 years of DK_ChristmasTrees_Perm3
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "DK_ChristmasTrees_Perm2::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}