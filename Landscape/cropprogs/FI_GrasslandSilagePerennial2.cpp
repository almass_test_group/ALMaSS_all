/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>FI_FI_GrasslandSilagePerennial2.cpp This file contains the source for the FI_GrasslandSilagePerennial2 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
*/
//
// FI_GrasslandSilagePerennial2.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FI_GrasslandSilagePerennial2.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_FI_GrasslandSilagePerennial2_SkScrapes("DK_CROP_GSP2_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_FI_GrasslandSilagePerennial2_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_GSP2_InsecticideDay;
extern CfgInt   cfg_GSP2_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool FI_GrasslandSilagePerennial2::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case fi_gsp2_start:
	{
		a_field->ClearManagementActionSum();

		// Set up the date management stuff
	 // Could save the start day in case it is needed later
	// m_field->m_startday = m_ev->m_startday;
	//m_last_date = g_date->DayInYear(20, 8);
	// Start and stop dates for all events after harvest
		int noDates = 4;
		//m_field->SetMDates(0, 0, g_date->DayInYear(15, 8)); // last possible day of harvest
		// Determined by harvest date - used to see if at all possible
		//m_field->SetMDates(1, 0, g_date->DayInYear(17, 8)); // end day of straw chopping
		//m_field->SetMDates(0, 1, g_date->DayInYear(18, 8)); // end day of hay baling
		//m_field->SetMDates(1, 1, g_date->DayInYear(19, 8)); // end day of stubble harrow
		//a_field->SetMDates(1, 2, g_date->DayInYear(20, 8)); // end day of plough

		//a_field->SetMConstants(0, 1);
		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)

		if (m_ev->m_startday > g_date->DayInYear(1, 7))
		{
			if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
			{
				g_msg->Warn(WARN_BUG, "FI_GrasslandSilagePerennial2::Do(): "
					"Harvest too late for the next crop to start!!!", "");
				exit(1);
			}
		}
		// Now fix any late finishing problems
		for (int i = 0; i < noDates; i++) {
			if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
				m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
			}
			if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
				m_field->SetMConstants(i, 0);
				m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
			}

		}
		// Now no operations can be timed after the start of the next crop.

		int d1;
		int today = g_date->Date();
		d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
		if (today > d1)
		{
			// Yes too late - should not happen - raise an error
			g_msg->Warn(WARN_BUG, "FI_GrasslandSilagePerennial2::Do(): "
				"Crop start attempt after last possible start date", "");
			exit(1);
		}
		// End single block date checking code. Please see next line
		// comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + m_first_date;;
		if (!m_ev->m_first_year) d1 += 365; // Add 365 for spring crop (not 1st yr)
		if (g_date->Date() > d1) {
			d1 = g_date->Date();
		}
		// OK, let's go.
		// LKM: Here we queue up the first event - NPK1
		SimpleEvent(d1, fi_gsp2_npk1, false);
	}
	break;

	case fi_gsp2_npk1: // 90% do this 
		if (a_ev->m_lock || a_farm->DoIt(90)) {
			if (!a_farm->FA_NPK(a_field, 0.0,
				g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_npk1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), fi_gsp2_cutting1, false);
		break;
	case fi_gsp2_cutting1: // 100% do this
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->CutToSilage(a_field, 0.0,
				g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_cutting1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() < 5, fi_gsp2_npk2, false);
		break;
	case fi_gsp2_npk2: // 80% do this 
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->FA_NPK(a_field, 0.0,
				g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_npk2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fi_gsp2_slurry2, false);
		break;
	case fi_gsp2_slurry2: // 30% do this 
		if (a_ev->m_lock || a_farm->DoIt(30)) {
			if (!a_farm->FA_Slurry(a_field, 0.0,
				g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_slurry2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 21, fi_gsp2_cutting2, false);
		break;
	case fi_gsp2_cutting2: // 90% do this
		if (a_ev->m_lock || a_farm->DoIt(90)) {
			if (!a_farm->CutToSilage(a_field, 0.0,
				g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_cutting2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() < 5, fi_gsp2_npk3, false);
		break;
	case fi_gsp2_npk3: // 80% do this 
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->FA_NPK(a_field, 0.0,
				g_date->DayInYear(5, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_npk3, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fi_gsp2_slurry3, false);
		break;
	case fi_gsp2_slurry3: // 20% do this 
		if (a_ev->m_lock || a_farm->DoIt(20)) {
			if (!a_farm->FA_Slurry(a_field, 0.0,
				g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_slurry3, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 21, fi_gsp2_cutting3, false);
		break;	
	case fi_gsp2_cutting3: // 40% do grazing
		if (a_ev->m_lock || a_farm->DoIt_prob(.20)) {
			if (!a_farm->CutToSilage(a_field, 0.0,
				g_date->DayInYear(26, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_cutting3, true);
				break;
			}
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.20 / .80)) {
			SimpleEvent(g_date->Date(), fi_gsp2_grazing1, false);
			break;
		}
		SimpleEvent(g_date->Date() < 5, fi_gsp2_npk4, false);
		break;

	case fi_gsp2_grazing1: // 
		if (!a_farm->CattleOut(a_field, 0.0,
			g_date->DayInYear(26, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_gsp2_grazing1, true);
			break;
		}

		// here comes two parallel events:
		SimpleEvent(g_date->Date() + 1, fi_gsp2_cattle_is_out1, false);
		SimpleEvent(g_date->Date() < 5, fi_gsp2_npk4, false);
		break;
		
	case fi_gsp2_cattle_is_out1:    // Keep the cattle out there
					   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(26, 10) - g_date->DayInYear(), g_date->DayInYear(26, 10))) {
			SimpleEvent(g_date->Date() + 1, fi_gsp2_cattle_is_out1, false);
			break;
		}

	case fi_gsp2_npk4: // 5% do this 
		if (a_ev->m_lock || a_farm->DoIt(5)) {
			if (!a_farm->FA_NPK(a_field, 0.0,
				g_date->DayInYear(1, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_npk4, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 21, fi_gsp2_cutting4, false);
		break;
	case fi_gsp2_cutting4: // 5% do this
		if (a_ev->m_lock || a_farm->DoIt_prob(.025)) {
			if (!a_farm->CutToSilage(a_field, 0.0,
				g_date->DayInYear(22, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_gsp2_cutting4, true);
				break;
			}
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.025 / .975)) {
			SimpleEvent(g_date->Date(), fi_gsp2_grazing2, false);
			break;
		}
		done = true;
		break;

	case fi_gsp2_grazing2: // 
		if (!a_farm->CattleOut(a_field, 0.0,
			g_date->DayInYear(22, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_gsp2_grazing1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, fi_gsp2_cattle_is_out2, false);
		break;

	case fi_gsp2_cattle_is_out2:    // Keep the cattle out there
					   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(22, 11) - g_date->DayInYear(), g_date->DayInYear(26, 10))) {
			SimpleEvent(g_date->Date() + 1, fi_gsp2_cattle_is_out2, false);
			break;
		}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "FI_GrasslandSilagePerennial2::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}