/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PLPotatoes.cpp This file contains the source for the PLPotatoes class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PLPotatoes.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PLPotatoes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_potatoes_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_POT_InsecticideDay;
extern CfgInt   cfg_POT_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional potatoes.
*/
bool PLPotatoes::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_PLPotatoes; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case pl_pot_start:
	{
		// pl_pot_start just sets up all the starting conditions and reference dates that are needed to start a pl_pot
		PL_POT_FERTI_P1 = false;
		PL_POT_FERTI_S1 = false;
		PL_POT_STUBBLE_PLOUGH = false;
		PL_POT_DID_STRIG1 = false;
		PL_POT_DID_STRIG2 = false;
		PL_POT_DID_STRIG3 = false;
		PL_POT_HILL_DATE = 0;
		PL_POT_DID_HILL = false;
		PL_POT_DID_DESS = false;
		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 2;
		m_field->SetMDates(0, 0, g_date->DayInYear(25, 9)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(5, 10)); // end day of calcium application
		m_field->SetMDates(0, 1, 0);
		m_field->SetMDates(1, 1, g_date->DayInYear(5, 10)); 

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PLPotatoes::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PLPotatoes::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PLPotatoes::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 3), pl_pot_spring_harrow, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(20, 7);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		if (m_farm->IsStockFarmer()) { // StockFarmer
			SimpleEvent_(d1, pl_pot_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, pl_pot_ferti_p1, false, m_farm, m_field);
	}
	break;

	// This is the first real farm operation
	case pl_pot_ferti_p1:
		// In total 10% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (5%) do it now
		if (m_ev->m_lock || m_farm->DoIt(5))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again (until 20/8 when we will suceed)
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				PL_POT_FERTI_P1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, pl_pot_stubble_plough, false, m_farm, m_field);
		break;
	case pl_pot_ferti_s1:
		// In total 40% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (40%) do it now
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				PL_POT_FERTI_S1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, pl_pot_stubble_plough, false, m_farm, m_field);
		break;
	case pl_pot_stubble_plough:
		// 65% will do stubble plough, but rest will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(65))
		{
			if (!m_farm->StubblePlough(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_stubble_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				// 50% of farmers will do this, but the other 50% won't so we need to remember whether we are in one or the other group
				PL_POT_STUBBLE_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->Date() + 1, pl_pot_autumn_harrow1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_stubble_harrow, false, m_farm, m_field);
		break;
	case pl_pot_autumn_harrow1:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_autumn_harrow1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 2, pl_pot_autumn_harrow2, false, m_farm, m_field);
		break;
	case pl_pot_autumn_harrow2:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->Date() + 7 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_autumn_harrow2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_pot_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, pl_pot_ferti_p2, false, m_farm, m_field);
		break;
	case pl_pot_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_stubble_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_pot_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, pl_pot_ferti_p2, false, m_farm, m_field);
		break;
	case pl_pot_ferti_p2:
		// In total 10% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (5%) do it now (if haven't done before)
		if ((m_ev->m_lock || m_farm->DoIt(static_cast<int>((5.0 / 95.0) * 100))) && (PL_POT_FERTI_P1 == false))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p3, false, m_farm, m_field);
		break;
	case pl_pot_ferti_s2:
		// In total 80% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (40%) do it now (if haven't done before)
		if ((m_ev->m_lock || m_farm->DoIt(static_cast<int>((40.0 / 60.0) * 100))) && (PL_POT_FERTI_S1 == false))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s3, false, m_farm, m_field);
		break;
	case pl_pot_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt(7))
		{
			if (!m_farm->FP_NPKS(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_winter_plough, false, m_farm, m_field);
		break;
	case pl_pot_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt(7))
		{
			if (!m_farm->FA_NPKS(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_winter_plough, false, m_farm, m_field);
		break;
	case pl_pot_winter_plough:
		if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_winter_plough, true, m_farm, m_field);
			break;
		}

		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 3) + 365, pl_pot_spring_harrow, false, m_farm, m_field);
		break;
	case pl_pot_spring_harrow:
		if ((m_ev->m_lock) || m_farm->DoIt(98))
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_spring_harrow, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_pot_ferti_s4, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, pl_pot_ferti_p4, false, m_farm, m_field);
		break;
	case pl_pot_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_spring_harrow2, false, m_farm, m_field);
		break;
	case pl_pot_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_spring_harrow2, false, m_farm, m_field);
		break;
	case pl_pot_spring_harrow2:
		if ((m_ev->m_lock) || m_farm->DoIt(22))
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_spring_harrow2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(5, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(5, 4);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_pot_ferti_s5, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, pl_pot_ferti_p5, false, m_farm, m_field);
		break;
	case pl_pot_ferti_p5:
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p5, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_spring_plough, false, m_farm, m_field);
		break;
	case pl_pot_ferti_s5:
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s5, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_spring_plough, false, m_farm, m_field);
		break;
	case pl_pot_spring_plough:
		if (m_ev->m_lock || m_farm->DoIt(92))
		{
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_spring_plough, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
		}
		SimpleEvent_(d1, pl_pot_bed_forming, false, m_farm, m_field);
		break;
	case pl_pot_bed_forming:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_bed_forming, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), pl_pot_spring_planting, false, m_farm, m_field);
		break;
	case pl_pot_spring_planting:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_spring_planting, true, m_farm, m_field);
			break;
		}
		// Fork of events
		SimpleEvent_(g_date->Date() + 2, pl_pot_hilling1, false, m_farm, m_field); // Strigling followed by hilling and herbicides
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s6, false, m_farm, m_field);	// N before emergence
		}
		else SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p6, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 6), pl_pot_fungicide1, false, m_farm, m_field); // Fungicide treat = MAIN TREAT
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 6), pl_pot_insecticide, false, m_farm, m_field); // Insecticide treat
		break;
	case pl_pot_hilling1:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(90))
			{
				if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_hilling1, true, m_farm, m_field);
					break;
				}
				// 90% of farmers remove weeds with strigling (and hilling), the rest will do herbicide
				PL_POT_DID_STRIG1 = true;
				// Queue up the second strigling_hill event after 7 days
				SimpleEvent_(g_date->Date() + 5, pl_pot_hilling2, false, m_farm, m_field);
				break;
			}
			else {
				SimpleEvent_(g_date->Date(), pl_pot_herbicide1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_hilling4, false, m_farm, m_field);
		break;
	case pl_pot_hilling2:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || (m_farm->DoIt(81) && PL_POT_DID_STRIG1)) // which gives 73% of all farmers
			{
				if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_hilling2, true, m_farm, m_field);
					break;
				}
				// 81% of farmers do strigling_hill second time, the rest will end up with herbicide
				PL_POT_DID_STRIG2 = true;
				// Queue up the third strigling_hill event after 7 days
				SimpleEvent_(g_date->Date() + 5, pl_pot_hilling3, false, m_farm, m_field);
				break;
			}
			else {
				SimpleEvent_(g_date->Date(), pl_pot_herbicide1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_hilling4, false, m_farm, m_field);
		break;
	case pl_pot_hilling3:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || (m_farm->DoIt(73) && PL_POT_DID_STRIG2)) // which gives 53% of all farmers
			{
				if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_hilling3, true, m_farm, m_field);
					break;
				}
				// 73% of farmers do strigling_hill third time, the rest will end up with herbicide. 
				PL_POT_DID_STRIG3 = true;
			}
			else {
				SimpleEvent_(g_date->Date()+3, pl_pot_herbicide1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_hilling4, false, m_farm, m_field);
		break;
	case pl_pot_herbicide1:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(93))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_herbicide1, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 1, pl_pot_hilling4, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_pot_hilling4, false, m_farm, m_field);
		break;
	case pl_pot_hilling4:
		if (m_ev->m_lock || m_farm->DoIt(38))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_hilling4, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_hilling4, true, m_farm, m_field);
					break;
				}
				PL_POT_HILL_DATE = g_date->Date();
				SimpleEvent_(g_date->Date() + 7, pl_pot_hilling5, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 3, pl_pot_herbicide2, false, m_farm, m_field);
		break;
	case pl_pot_hilling5:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_hilling5, true, m_farm, m_field);
			break;
		}
		PL_POT_HILL_DATE = g_date->Date();
		SimpleEvent_(g_date->Date() + 7, pl_pot_hilling6, false, m_farm, m_field);
		break;
	case pl_pot_hilling6:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_hilling6, true, m_farm, m_field);
			break;
		}
		PL_POT_HILL_DATE = g_date->Date();
		PL_POT_DID_HILL = true;
		
		SimpleEvent_(g_date->Date() + 7, pl_pot_herbicide2, false, m_farm, m_field);
		break;
	case pl_pot_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case pl_pot_ferti_s6:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(80))
			{
				if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s6, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_pot_ferti_s7, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_pot_ferti_s7, false, m_farm, m_field);
		break;

	case pl_pot_ferti_p6:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(80))
			{
				if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p6, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_pot_ferti_p7, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_pot_ferti_p7, false, m_farm, m_field);
		break;
	case pl_pot_ferti_s7:
		if (m_ev->m_lock || m_farm->DoIt(53))
		{
			if (PL_POT_HILL_DATE >= g_date->Date() - 2){ // Should by at least 3 days after hilling
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s7, false, m_farm, m_field); }
			else
			{
				if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s7, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case pl_pot_ferti_p7:
		if (m_ev->m_lock || m_farm->DoIt(53))
		{
			if (PL_POT_HILL_DATE >= g_date->Date() - 2) { // Should by at least 3 days after hilling
				SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p7, false, m_farm, m_field);
			}
			else
			{
				if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p7, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case pl_pot_fungicide1:
		// Here comes the fungicide thread = MAIN THREAD
		if (PL_POT_HILL_DATE >= g_date->Date() - 2) { // Should by at least 3 days after hilling
			SimpleEvent_(g_date->Date() + 1, pl_pot_fungicide1, true, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt(78))
			{
				if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_fungicide1, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 10, pl_pot_fungicide2, false, m_farm, m_field);
			break;
		}
		break;
	case pl_pot_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt(63))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, pl_pot_fungicide3, false, m_farm, m_field);
		break;
	case pl_pot_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt(63))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, pl_pot_fungicide4, false, m_farm, m_field);
		break;
	case pl_pot_fungicide4:
		if (m_ev->m_lock || m_farm->DoIt(38))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_fungicide4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 8), pl_pot_dessication1, false, m_farm, m_field);
		break;
	case pl_pot_insecticide:
		if (m_ev->m_lock || m_farm->DoIt(75))
		{
			if (PL_POT_HILL_DATE >= g_date->Date() - 2) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_insecticide, false, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				if (!cfg_pest_potatoes_on.value() ||
					!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, pl_pot_insecticide, true, m_farm, m_field);
						break;
					}
				}
				else {
					m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
			}
		}
		// End of thread
		break;
	case pl_pot_dessication1:
		// Here the MAIN thread continues
		// This is the desiccation thread with glyphosate or diquat
		if (m_ev->m_lock || m_farm->DoIt(35))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_dessication1, true, m_farm, m_field);
				break;
			}
			PL_POT_DID_DESS = true;
		}
		SimpleEvent_(g_date->Date() + 7, pl_pot_dessication2, false, m_farm, m_field);
		break;
	case pl_pot_dessication2:
		if (m_ev->m_lock || (m_farm->DoIt(17) && PL_POT_DID_DESS)) //which is 6% of all farmers
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_pot_dessication2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, pl_pot_harvest, false, m_farm, m_field);
		break;
	case pl_pot_harvest:
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_pot_harvest, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s8, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p8, false, m_farm, m_field);
		break;
	case pl_pot_ferti_p8:
		if (m_ev->m_lock || m_farm->DoIt(23))
		{
			if (m_field->GetMConstants(3) == 0) {
				if (!m_farm->FP_Calcium(m_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLPotatoes::Do(): failure in 'FP_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->FP_Calcium(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_p8, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case pl_pot_ferti_s8:
		if (m_ev->m_lock || m_farm->DoIt(23))
		{
			if (m_field->GetMConstants(3) == 0) {
				if (!m_farm->FA_Calcium(m_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLPotatoes::Do(): failure in 'FA_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->FA_Calcium(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_pot_ferti_s8, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "PLPotatoes::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}