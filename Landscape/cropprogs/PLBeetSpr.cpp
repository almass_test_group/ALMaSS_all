/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PLBeetSpr.cpp This file contains the source for the PLBeetSpr class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PLBeetSpr.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PLBeetSpr.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_beet_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_BES_InsecticideDay;
extern CfgInt   cfg_BES_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional beet.
*/
bool PLBeetSpr::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_PLBeetSpr; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case pl_bes_start:
	{
		// pl_bes_start just sets up all the starting conditions and reference dates that are needed to start a pl_sw
		PL_BES_SPRING_FERTI = 0;
		PL_BES_WATER_DATE = 0;
		PL_BES_HERBI_DATE = 0;
		PL_BES_HERBI1 = false;
		PL_BES_HERBI3 = false;
		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 2;
		m_field->SetMDates(0, 0, g_date->DayInYear(30, 12)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(30, 12)); //  end day of calcium application
		m_field->SetMDates(0, 1, 0);
		m_field->SetMDates(1, 1, g_date->DayInYear(30, 12));

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), pl_bes_spring_plough, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.


		d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		if (g_date->Date() >= d1) d1 += 365;
		SimpleEvent_(d1, pl_bes_spring_plough, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case pl_bes_spring_plough:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_plough, true, m_farm, m_field);
			break;
		}

		SimpleEvent_(g_date->Date() + 1, pl_bes_spring_harrow, false, m_farm, m_field);
		break;
	case pl_bes_spring_harrow:
		if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_bes_ferti_s4, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, pl_bes_ferti_p4, false, m_farm, m_field);
		break;
	case pl_bes_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt(98))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p4, true, m_farm, m_field);
				break;
			}
			PL_BES_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p5, false, m_farm, m_field);
		break;
	case pl_bes_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt(98))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s4, true, m_farm, m_field);
				break;
			}
			PL_BES_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s5, false, m_farm, m_field);
		break;
	case pl_bes_ferti_p5:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p5, true, m_farm, m_field);
				break;
			}
			PL_BES_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_heavy_cultivator, false, m_farm, m_field);
		break;
	case pl_bes_ferti_s5:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s5, true, m_farm, m_field);
				break;
			}
			PL_BES_SPRING_FERTI = 1;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_heavy_cultivator, false, m_farm, m_field);
		break;
	case pl_bes_heavy_cultivator:
		if (PL_BES_SPRING_FERTI == 1)
		{
			if (!m_farm->HeavyCultivatorAggregate(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_heavy_cultivator, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, pl_bes_preseeding_cultivator, false, m_farm, m_field);
		break;
	case pl_bes_preseeding_cultivator:
		// 90% will do preseeding cultivation, the rest will do it together with sow
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(19, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_preseeding_cultivator, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_sow, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_preseeding_cultivator_sow, false, m_farm, m_field);
		break;
	case pl_bes_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, pl_bes_harrow_before_emergence, false, m_farm, m_field); // Harrowing before emergence, followed by thinning after emergence
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_watering1, false, m_farm, m_field);	// Watering thread
		SimpleEvent_(g_date->Date() + 5, pl_bes_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pl_bes_fungicide1, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_bes_insecticide, false, m_farm, m_field);	// Insecticide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), pl_bes_ferti_s6, false, m_farm, m_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), pl_bes_ferti_p6, false, m_farm, m_field);
		break;
	case pl_bes_preseeding_cultivator_sow:
		// 10% will do preseeding cultivation with sow
		if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_preseeding_cultivator_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, pl_bes_harrow_before_emergence, false, m_farm, m_field); // Harrowing before emergence, followed by thinning after emergence
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_watering1, false, m_farm, m_field);	// Watering thread
		SimpleEvent_(g_date->Date() + 5, pl_bes_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pl_bes_fungicide1, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_bes_insecticide, false, m_farm, m_field);	// Insecticide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_ferti_s6, false, m_farm, m_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_ferti_p6, false, m_farm, m_field);
		break;
	case pl_bes_harrow_before_emergence:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(3))
			{
				if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_harrow_before_emergence, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 5, pl_bes_thinning, false, m_farm, m_field);
			break;
		}
		else {
			SimpleEvent_(g_date->Date() + 1, pl_bes_thinning, false, m_farm, m_field);
		}
		break;
	case pl_bes_thinning:
		if (m_ev->m_lock || m_farm->DoIt(5))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_thinning, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_thinning, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case pl_bes_watering1:
		if (m_ev->m_lock || m_farm->DoIt(3))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_watering1, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_watering1, true, m_farm, m_field);
					break;
				}
				PL_BES_WATER_DATE = g_date->Date();
			}
			d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
				d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
			}
			SimpleEvent_(d1, pl_bes_watering2, false, m_farm, m_field);
			break;
		}
		break;

	case pl_bes_watering2:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_watering2, true, m_farm, m_field);
			break;
		}
		PL_BES_WATER_DATE = g_date->Date();
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		}
		SimpleEvent_(d1, pl_bes_watering3, false, m_farm, m_field);
		break;
	case pl_bes_watering3:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_watering3, true, m_farm, m_field);
			break;
		}
		PL_BES_WATER_DATE = g_date->Date();
		// End of thread
		break;
	case pl_bes_ferti_p6:
		// Here comes fertilizers thread
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p6, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p6, true, m_farm, m_field);
				break;
			}
			d1 = g_date->Date() + 1;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 5);
			}
			SimpleEvent_(d1, pl_bes_ferti_p7, false, m_farm, m_field);
			break;
		}
		break;
	case pl_bes_ferti_s6:
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s6, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s6, true, m_farm, m_field);
				break;
			}
			d1 = g_date->Date() + 1;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 5);
			}
			SimpleEvent_(d1, pl_bes_ferti_s7, false, m_farm, m_field);
			break;
		}
		break;
	case pl_bes_ferti_p7:
		if (m_ev->m_lock || m_farm->DoIt(89))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p7, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case pl_bes_ferti_s7:
		if (m_ev->m_lock || m_farm->DoIt(89))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s7, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case pl_bes_herbicide1: // The first of the pesticide managements.
						   // Here comes the herbicide thread
						   // We assume that farmers do max 3 herbicide treatments
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(43))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide1, true, m_farm, m_field);
					break;
				}
				PL_BES_HERBI1 = true;
				PL_BES_HERBI_DATE = g_date->Date();
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), pl_bes_herbicide2, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), pl_bes_herbicide2, false, m_farm, m_field);
		break;
	case pl_bes_herbicide2:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide2, true, m_farm, m_field);
			break;
		}
		PL_BES_HERBI_DATE = g_date->Date();
		SimpleEvent_(g_date->Date() + 10, pl_bes_herbicide3, false, m_farm, m_field);
		break;
	case pl_bes_herbicide3:
		if (m_ev->m_lock || m_farm->DoIt(93))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide3, true, m_farm, m_field);
				break;
			}
			PL_BES_HERBI_DATE = g_date->Date();
			PL_BES_HERBI3 = true;
		}
		SimpleEvent_(g_date->Date() + 10, pl_bes_herbicide4, false, m_farm, m_field);
		break;
	case pl_bes_herbicide4:
		if (m_ev->m_lock || (m_farm->DoIt(81) && (PL_BES_HERBI1 == false || PL_BES_HERBI3 == false))) // which gives 46% of all farmers (and 81% of those who did not apply herbicides before beet emergence)
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide4, true, m_farm, m_field);
				break;
			}
			PL_BES_HERBI_DATE = g_date->Date();
		}
		// End of thread
		break;
	case pl_bes_fungicide1:
		// Here comes the fungicide thread
		if (m_ev->m_lock || m_farm->DoIt(83))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_fungicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, pl_bes_fungicide2, false, m_farm, m_field);
		break;
	case pl_bes_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt(63))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(25, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 9), pl_bes_harvest, false, m_farm, m_field);
		break;
	case pl_bes_insecticide:
		// Here comes the insecticide thread

		if (PL_BES_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, pl_bes_insecticide, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt(58))
			{
				// here we check wheter we are using ERA pesticide or not
				if (!cfg_pest_beet_on.value() ||
					!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, pl_bes_insecticide, true, m_farm, m_field);
						break;
					}
				}
				else {
					m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
			}
		}
		// End of thread
		break;
	case pl_bes_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_harvest, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s8, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p8, false, m_farm, m_field);
		break;

	case pl_bes_ferti_p8:
		if (m_ev->m_lock || m_farm->DoIt(28))
		{
			if (m_field->GetMConstants(3) == 0) {
				if (!m_farm->FP_Calcium(m_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): failure in 'FP_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->FP_Calcium(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p8, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case pl_bes_ferti_s8:
		if (m_ev->m_lock || m_farm->DoIt(28))
		{
			if (m_field->GetMConstants(3) == 0) {
				if (!m_farm->FA_Calcium(m_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): failure in 'FA_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->FA_Calcium(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s8, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}