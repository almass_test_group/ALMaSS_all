/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>FR_Sunflower.cpp This file contains the source for the FR_Sunflower class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of September 2021 \n
 \n
*/
//
// FR_Sunflower.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FR_Sunflower.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_FR_Sunflower_SkScrapes("FR_CROP_SF_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_sunflower_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_SF_InsecticideDay;
extern CfgInt   cfg_SF_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool FR_Sunflower::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case fr_sf_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		FR_SF_PLOUGH = false;
		FR_SF_SLURRY = false;

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_FR_Sunflower_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 3 start and stop dates for all events after harvest for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(5, 10)); // last possible day of harvest
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FR_Sunflower::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "FR_Sunflower::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FR_Sunflower::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start woth the first event on the after winter time) - Date here is a suggestion (not stated in crop scheme!)
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 2), fr_sf_fertilizer, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(10, 10);
		SimpleEvent(d1, fr_sf_mineral, false);
		break;

	}
	break;
	// This is the first real farm operation
	case fr_sf_mineral:
		if (a_ev->m_lock || a_farm->DoIt_prob(.20)) {
			if (!a_farm->FP_Calcium(a_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_mineral, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fr_sf_stubble_plough, false);
		break;

	case fr_sf_stubble_plough:
		if (a_ev->m_lock || a_farm->DoIt_prob(.30)) {
			if (!a_farm->StubblePlough(a_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_stubble_plough, true);
				break;
			}
			SimpleEvent(g_date->Date() + 7, fr_sf_autumn_harrow, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.50 / .70)) {
			SimpleEvent(g_date->Date(), fr_sf_stubble_harrow, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 10), fr_sf_slurry, false);
		break;

	case fr_sf_stubble_harrow:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_sf_stubble_harrow, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 10), fr_sf_slurry, false);
		break;

	case fr_sf_autumn_harrow:
		if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->DayInYear(17, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_sf_autumn_harrow, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 10), fr_sf_slurry, false);
		break;

	case fr_sf_slurry:
		if (m_farm->IsStockFarmer()) { // StockFarmer
			if (a_ev->m_lock || a_farm->DoIt_prob(.30)) {
				if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 12) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, fr_sf_slurry, true);
					break;
				}
				else {
					//remember who use manure
					FR_SF_SLURRY = true;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 12), fr_sf_winter_plough, false);
		break;

	case fr_sf_winter_plough:
		if (a_ev->m_lock || a_farm->DoIt_prob(.20)) { //
			if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(31, 12) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_winter_plough, true);
				break;
			}
			else {
				//remember who ploughs
				FR_SF_PLOUGH = true;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 2)+365, fr_sf_fertilizer, false);
		break;

	case fr_sf_fertilizer:
		if (FR_SF_SLURRY == false && a_ev->m_lock || a_farm->DoIt_prob(.10/.70)) {
			if (!a_farm->FP_Manure(a_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_fertilizer, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), fr_sf_spring_plough, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 3), fr_sf_herbicide1, false);
		break;

	case fr_sf_spring_plough:
		if (FR_SF_PLOUGH == true) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), fr_sf_harrow1, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.50/.80)) { // suggested to fit with the crop scheme text (70% in crop scheme) - 70% in total ploughs - 20% in autumn, 50% in spring
			if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_spring_plough, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), fr_sf_harrow1, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.20 / .30)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), fr_sf_spring_harrow, false);
			break;
		}
		else 
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 3), fr_sf_preseed_cultivation, false);
		break;

	case fr_sf_herbicide1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.50)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_herbicide1, true);
				break;
			}
		}
		break;

	case fr_sf_spring_harrow:
		if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_sf_spring_harrow, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 3), fr_sf_preseed_cultivation, false);
		break;

	case fr_sf_harrow1:
		if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(23, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_sf_harrow1, true);
			break;
		}
		SimpleEvent(g_date->Date()+7, fr_sf_preseed_cultivation, false);
		break;

	case fr_sf_preseed_cultivation:
		if (a_ev->m_lock || a_farm->DoIt_prob(.90)) {
			if (!a_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_preseed_cultivation, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fr_sf_sow, false);
		break;
	case fr_sf_sow:
		if (!a_farm->SpringSow(a_field, 0.0,
			g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_sf_sow, true);
			break;
		}
		SimpleEvent(g_date->Date(), fr_sf_fertilizer2, false);
		SimpleEvent(g_date->Date() + 2, fr_sf_herbicide2, false);
		SimpleEvent(g_date->Date() + 7, fr_sf_harrow2, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), fr_sf_n, false);
		SimpleEvent(g_date->Date() + 44, fr_sf_insecticide, false); // main thread
		break;

	case fr_sf_fertilizer2:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->FP_P(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_fertilizer2, true);
				break;
			}
		}
		break;

	case fr_sf_herbicide2:
		if (a_ev->m_lock || a_farm->DoIt(90)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(2, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_herbicide2, true);
				break;
			}
			SimpleEvent(g_date->Date() + 30, fr_sf_herbicide3, false);
			break;
		}
		break;

	case fr_sf_herbicide3:
		if (a_ev->m_lock || a_farm->DoIt(50)) { //50% of the 90% doing herbicide2
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(2, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_herbicide3, true);
				break;
			}
		}
		break;

	case fr_sf_harrow2:
		if (a_ev->m_lock || a_farm->DoIt(15)) {
			if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_harrow2, true);
				break;
			}
		}
		break;

	case fr_sf_n:
		if (!a_farm->FP_N(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_sf_n, true);
			break;
		}
		break;

	case fr_sf_insecticide:
		if (a_ev->m_lock || a_farm->DoIt(5)) {
			if (!a_farm->InsecticideTreat(a_field, 0.0,
				g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_sf_insecticide, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 8), fr_sf_harvest, false);
		break;
	case fr_sf_harvest:
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_sf_harvest, true);
			break;
		}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "FR_Sunflower::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}