/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_Cabbage.cpp This file contains the source for the DE_Cabbage class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Susanne Stein \n
 Version of August 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_Cabbage.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_Cabbage.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_cabbage_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_CAB_InsecticideDay;
extern CfgInt   cfg_CAB_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_Cabbage::Do(Farm* m_farm, LE* m_field, FarmEvent* m_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = m_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = m_field;
	m_ev = m_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DECabbage; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (m_ev->m_todo)
	{
	case de_cab_start:
	{
		// de_cab_start just sets up all the starting conditions and reference dates that are needed to start a de_cab crop off
		DE_CAB_WINTER_PLOUGH = false;
		m_field->ClearManagementActionSum();


		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(15, 11)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(15, 11));
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "DE_Cabbage::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "DE_Cabbage::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "DE_Cabbage::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start woth the first event on the after winter time)
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4), de_cab_spring_planting, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		SimpleEvent_(d1, de_cab_winter_plough_clay, false, m_farm, m_field);
	}
	break;

	// This is the first real farm operation
	case de_cab_winter_plough_clay:
		if (m_field->GetSoilType() != 15 && m_field->GetSoilType() != 16 && m_field->GetSoilType() != 17 && m_field->GetSoilType() != 18 && m_field->GetSoilType() != 19 && m_field->GetSoilType() != 20 && m_field->GetSoilType() != 21 && m_field->GetSoilType() != 22 && m_field->GetSoilType() != 23 && m_field->GetSoilType() != 24 && m_field->GetSoilType() != 25 && m_field->GetSoilType() != 26) // on clay soils
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
			{
				if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_cab_winter_plough_clay, true, m_farm, m_field);
					break;
				}
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_cab_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_cab_ferti_p1, false, m_farm, m_field);
		break;
	case de_cab_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_cab_spring_plough_sandy, false, m_farm, m_field);
		break;
	case de_cab_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_cab_spring_plough_sandy, false, m_farm, m_field);
		break;
	case de_cab_spring_plough_sandy:
		if (m_field->GetSoilType() == 15 && m_field->GetSoilType() == 16 && m_field->GetSoilType() == 17 && m_field->GetSoilType() == 18 && m_field->GetSoilType() == 19 && m_field->GetSoilType() == 20 && m_field->GetSoilType() == 21 && m_field->GetSoilType() == 22 && m_field->GetSoilType() == 23 && m_field->GetSoilType() == 24 && m_field->GetSoilType() == 25 && m_field->GetSoilType() == 26) // on sandy soils
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
			{
				if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_cab_spring_plough_sandy, true, m_farm, m_field);
					break;
				}
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(25, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(25, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_cab_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_cab_ferti_p2, false, m_farm, m_field);
		break;
	case de_cab_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 4);
		}
		SimpleEvent_(d1, de_cab_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_cab_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 4);
		}
		SimpleEvent_(d1, de_cab_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_cab_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 3, de_cab_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_cab_spring_planting, false, m_farm, m_field);
		break;

	case de_cab_spring_planting:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_cab_spring_planting, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, de_cab_weeding1, false, m_farm, m_field);	// Weeding thread
		SimpleEvent_(g_date->Date() + 5, de_cab_herbicide, false, m_farm, m_field); // herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 6), de_cab_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), de_cab_insecticide1, false, m_farm, m_field);	// Insecticide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), de_cab_watering, false, m_farm, m_field);	// Watering thread
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_cab_ferti_s3, false, m_farm, m_field); // N thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_cab_ferti_p3, false, m_farm, m_field);
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_cab_ferti_s5, false, m_farm, m_field); // microelements thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_cab_ferti_p5, false, m_farm, m_field);
		break;
	case de_cab_ferti_s3:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_cab_ferti_s3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(31, 5), de_cab_ferti_s4, false, m_farm, m_field);
		break;
	case de_cab_ferti_p3:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_cab_ferti_p3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(31, 5), de_cab_ferti_p4, false, m_farm, m_field);
		break;
	case de_cab_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_cab_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_cab_ferti_s5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_s5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_cab_ferti_p5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_ferti_p5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_cab_watering:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_watering, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_cab_weeding1:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
			{
				if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_cab_weeding1, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_cab_weeding2, false, m_farm, m_field);
		break;
	case de_cab_weeding2:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_cab_weeding2, true, m_farm, m_field);
			break;
		}
		// end of thread
		break;
	case de_cab_herbicide:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_cab_herbicide, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread 
		break;
	case de_cab_fungicide1:
		// Here comes the fungicide thread
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_cab_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, de_cab_fungicide2, false, m_farm, m_field);
		break;
	case de_cab_fungicide2:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_cab_fungicide2, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, de_cab_fungicide3, false, m_farm, m_field);
		break;
	case de_cab_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_cab_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_cab_insecticide1:
		// Here comes the insecticide thread
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(25, 8) - g_date->DayInYear();
		if (!cfg_pest_cabbage_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, d1)) {
				SimpleEvent_(g_date->Date() + 1, de_cab_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		else {
			if (!m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1)) {
				SimpleEvent_(g_date->Date() + 1, de_cab_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_cab_insecticide2, false, m_farm, m_field);
		break;
	case de_cab_insecticide2:
		// here we check wheter we are using ERA pesticide or not
		d1 = g_date->DayInYear(10, 9) - g_date->DayInYear();
		if (!cfg_pest_cabbage_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, d1)) {
				SimpleEvent_(g_date->Date() + 1, de_cab_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		else {
			if (!m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1))
			{
				SimpleEvent_(g_date->Date() + 1, de_cab_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_cab_insecticide3, false, m_farm, m_field);
		break;
	case de_cab_insecticide3:
		// here we check wheter we are using ERA pesticide or not
		d1 = g_date->DayInYear(25, 9) - g_date->DayInYear();
		if (!cfg_pest_cabbage_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, d1)) {
				SimpleEvent_(g_date->Date() + 1, de_cab_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		else {
			if (!m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1))
			{
				SimpleEvent_(g_date->Date() + 1, de_cab_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
		}
		SimpleEvent_(d1, de_cab_harvest, false, m_farm, m_field);
		break;
	case de_cab_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_cab_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_Cabbage::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}



