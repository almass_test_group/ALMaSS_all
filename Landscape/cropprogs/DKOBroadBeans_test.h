//
// DKOBroadBeans_test.h
//
/*
*******************************************************************************************************
Copyright (c) 2015, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DKOBroadBeans_test.cpp This file contains the source for the DKOBroadBeans_test class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Marie Riddervold \n
Version of May 2020 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DKOBroadBeans_test.cpp
//

#ifndef DKOBroadBeans_test_H
#define DKOBroadBeans_test_H

#define DKOBEANStest_BASE 8900


typedef enum {
	dkobbtest_start = 1, // Compulsory, start event must always be 1 (one).
	dkobbtest_sleep_all_day = DKOBEANStest_BASE,
	dkobbtest_spring_harrow,
	dkobbtest_spring_plough,
	dkobbtest_ferti_k_s,
	dkobbtest_spring_harrow2,
	dkobbtest_broad_sow,
	dkobbtest_row_sow,
	dkobbtest_strigling1,
	dkobbtest_strigling2,
	dkobbtest_strigling3,
	dkobbtest_watering,  //8910
	dkobbtest_harvest,
	dkobbtest_autumn_harrow,
	dkobbtest_autumn_plough,
	dkobbtest_sow_catchcrop,
	dkobbtest_sow_wintercrop,
	dkobbtest_wait  //8916
} DKOBroadBeans_testToDo;



class DKOBroadBeans_test : public Crop {
public:
	bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
	DKOBroadBeans_test( ) {
		m_first_date = g_date->DayInYear( 25,2 ),
	}

};

#endif // DKOBroadBeans_test_H
