/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University and Susanne Stein, Julius-Kuehn-Institute
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_OCabbages.cpp This file contains the source for the DE_OCabbages class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of March 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DE_OCabbages.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OCabbages.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_cabbage_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_CAB_InsecticideDay;
extern CfgInt   cfg_CAB_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional cabbage.
*/
bool DE_OCabbages::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_oca_start:
	{
		// de_ocab_start just sets up all the starting conditions and reference dates that are needed to start a de_oca
		DE_OCAB_WINTER_PLOUGH = false;

		a_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(30, 11)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(30, 11));

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "DE_OCabbages::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
			// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "DE_OCabbages::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "DE_OCabbages::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				if (m_farm->IsStockFarmer()) //Stock Farmer
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_oca_ferti_s1, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_oca_ferti_p1, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
	// Here we queue up the first event - this differs depending on whether we have field on sandy or clay soils
		SimpleEvent(d1, de_oca_autumn_plough_clay, false);
	}
	break;

	// This is the first real farm operation - done before the 31st of October
	case de_oca_autumn_plough_clay:
		if (m_field->GetSoilType() != 15 && m_field->GetSoilType() != 16 && m_field->GetSoilType() != 17 && m_field->GetSoilType() != 18 && m_field->GetSoilType() != 19 && m_field->GetSoilType() != 20 && m_field->GetSoilType() != 21 && m_field->GetSoilType() != 22 && m_field->GetSoilType() != 23 && m_field->GetSoilType() != 24 && m_field->GetSoilType() != 25 && m_field->GetSoilType() != 26) // here the right soil code have to be inserted
		{

			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, de_oca_autumn_plough_clay, true);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_oca_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_oca_ferti_p1, false, m_farm, m_field);
		break;
	case de_oca_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oca_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 2), de_oca_spring_harrow_sandy, false);
		break;
	case de_oca_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oca_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		//LKM: Queue up next event - spring harrow if sandy soil, done before the 10th of May
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 2), de_oca_spring_harrow_sandy, false);
		break;
	case de_oca_spring_harrow_sandy:
		if (m_field->GetSoilType() == 15 && m_field->GetSoilType() == 16 && m_field->GetSoilType() == 17 && m_field->GetSoilType() == 18 && m_field->GetSoilType() == 19 && m_field->GetSoilType() == 20 && m_field->GetSoilType() == 21 && m_field->GetSoilType() == 22 && m_field->GetSoilType() == 23 && m_field->GetSoilType() == 24 && m_field->GetSoilType() == 25 && m_field->GetSoilType() == 26) // on sandy soils
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, de_oca_spring_harrow_sandy, true);
				break;
			}
		}
	// LKM: Queue up the next event - shallow harrow1 (making seedbed) done after the 1st of March and before the 30th of April - if not done, try again +1 day until the 30th of April when we will succeed
	SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), de_oca_sharrow1, false);
	break;
	case de_oca_sharrow1:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_sharrow1, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow2 (making seedbed) done 5 days after, and before the 10th of May - if not done, try again +1 day until the 10th of May when we will succeed
		SimpleEvent(g_date->Date() + 5, de_oca_sharrow2, false);
		break;
	case de_oca_sharrow2:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_sharrow2, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow3 (making seedbed) done 5 days after, and before the 20th of May - if not done, try again +1 day until the 20th of May when we will succeed
		SimpleEvent(g_date->Date() + 5, de_oca_sharrow3, false);
		break;
	case de_oca_sharrow3:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_sharrow3, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow4 (making seedbed) done 5 days after, and before the 30th of May - if not done, try again +1 day until the 30th of May when we will succeed
		SimpleEvent(g_date->Date() + 5, de_oca_sharrow4, false);
		break;
	case de_oca_sharrow4:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_sharrow4, true);
			break;
		}
		// LKM: Queue up the next event - planting done (together with manure) the day after, and before 31th of May - if not done, try again +1 day until the 31th of May when we will succeed
		SimpleEvent(g_date->Date() + 1, de_oca_plant, false);
		break;
	case de_oca_plant:
		if (!m_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_plant, true);
			break;
		}
		// LKM: Queue up the next event - watering done same day as planting, and before 31th of May - if not done, try again +1 day until the 31th of May when we will succeed
		SimpleEvent(g_date->Date(), de_oca_water, false);
		break;
	case de_oca_water:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_water, true);
			break;
		}

		// LKM: Queue up the next event - strigling1 (making seedbed) done after the +7 days after planting, and before the 8th of June - if not done, try again +1 day until the 8th of June when we will succeed
		SimpleEvent(g_date->Date() + 7, de_oca_strigling1, false);
		break;
	case de_oca_strigling1:
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_strigling1, true);
			break;
		}
		// LKM: Queue up the next event - strigling2 (making seedbed) done 8 days after, and before the 15th of June - if not done, try again +1 day until the 15th of June when we will succeed
		SimpleEvent(g_date->Date() + 8, de_oca_strigling2, false);
		break;
	case de_oca_strigling2:
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_strigling2, true);
			break;
		}
		// LKM: Queue up the next event - strigling3 (making seedbed) done 8 days after, and before the 23th of June - if not done, try again +1 day until the 23th of June when we will succeed
		SimpleEvent(g_date->Date() + 8, de_oca_strigling3, false);
		break;
	case de_oca_strigling3:
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(23, 6) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_strigling3, true);
			break;
		}
		// LKM: Queue up the next event - row cultivation (if clay soil) done before the 30th of June - if not done, try again +1 day until the 30th of June when we will succeed
		SimpleEvent(g_date->Date() + 1, de_oca_row_cultivation_clay, false);
		break;
	case de_oca_row_cultivation_clay:
		if (m_field->GetSoilType() != 15 && m_field->GetSoilType() != 16 && m_field->GetSoilType() != 17 && m_field->GetSoilType() != 18 && m_field->GetSoilType() != 19 && m_field->GetSoilType() != 20 && m_field->GetSoilType() != 21 && m_field->GetSoilType() != 22 && m_field->GetSoilType() != 23 && m_field->GetSoilType() != 24 && m_field->GetSoilType() != 25 && m_field->GetSoilType() != 26) // on clay soils
		{

			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, de_oca_row_cultivation_clay, true);
				break;
			}
		}
		// LKM: Queue up the next event - cover cabbages with net or fiber to prevent insects, done before the 25th of July - if not done, try again +1 day until the 25th of July when we will succeed
		SimpleEvent(g_date->Date() + 1, de_oca_covering1, false);
		break;
	case de_oca_covering1:
		if (!m_farm->StrawCovering(m_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_covering1, true);
			break;
		}
		// LKM: Queue up the next event - remove cover from cabbages, done before the 30th of July - if not done, try again +1 day until the 30th of July when we will succeed
		SimpleEvent(g_date->Date() + 1, de_oca_remove_cover1, false);
		break;
	case de_oca_remove_cover1:
		if (!m_farm->StrawRemoval(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_remove_cover1, true);
			break;
		}
		// LKM: Queue up the next event - manual weeding, done before the 31th of July - if not done, try again +1 day until the 31th of July when we will succeed
		SimpleEvent(g_date->Date() + 1, de_oca_manual_weeding, false);
		break;
	case de_oca_manual_weeding:
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_manual_weeding, true);
			break;
		}
		// LKM: Queue up the next event - harvest cabbages, done before the 30th of November - if not done, try again +1 day until the 30th of November when we will succeed
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), de_oca_harvest, false);
		break;
	case de_oca_harvest:
		if (!m_farm->Harvest(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, de_oca_harvest, true);
			break;
		}
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), de_oca_wait, false);
			// Because we are ending harvest before 1.7 so we need to wait until the 1.7
			break;
		}
		else {
			done = true; // end of plan
		}
	case de_oca_wait:
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_OCabbages::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}