//
// FI_SugarBeet.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FI_SugarBeet.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;


bool FI_SugarBeet::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	//  int d1;

	bool done = false;

	switch (m_ev->m_todo)
	{
	case fi_sbe_start:
	{
		FI_SB_DECIDE_TO_HERB = 1;
		FI_SB_DECIDE_TO_FI = 1;

		a_field->ClearManagementActionSum();

		m_field->SetVegPatchy(true); // Root crop so is open until tall
		// Set up the date management stuff
		// Could save the start day in case it is needed later
		// m_field->m_startday = m_ev->m_startday;
		m_last_date = g_date->DayInYear(15, 11);
		// Start and stop dates for all events after harvest
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(1, 10));
		// 0,0 determined by harvest date - used to see if at all possible
		m_field->SetMDates(1, 0, g_date->DayInYear(10, 11));
		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7))
			{
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FI_SugarBeet::Do(): "
						"Harvest too late for the next crop to start!!!", "");
					exit(1);
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0);
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			// CJT note:
			// Start single block date checking code to be cut-'n-pasted...

			if (!m_ev->m_first_year)
			{
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1)
				{
					// Yes, too early. We assumme this is because the last crop was late
					g_msg->Warn(WARN_BUG, "FI_SugarBeet::Do(): "
						"Crop start attempt between 1st Jan & 1st July", "");
					exit(1);
				}
				else
				{
					d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
					if (g_date->Date() > d1)
					{
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FI_SugarBeet::Do(): "
							"Crop start attempt after last possible start date", "");
						exit(1);
					}
				}
			}
			else
			{
				// If this is the first year of running then it is possible to start
				// on day 0, so need this to tell us what to do:
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), fi_sbe_slurry, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line
		// comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		// This is the first real farm operation
		SimpleEvent(d1, fi_sbe_autumn_plough1, false);
	}
	break;
		// OK, Let's go - LKM: first treatment, autumn plough, do it before the 15 of November - if not done, try again +1 day until the the 15 of November when we succeed - 100% of farmers (w clay soil) do this
	case fi_sbe_autumn_plough1:
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_sbe_autumn_plough1, true);
				break;
			}
		}
		// LKM: Queue up next event - Slurry, done after 15th of April (next year) 
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, fi_sbe_slurry, false);
		break;
		// LKM: Do slurry before 30th of April - 30% do this
	case fi_sbe_slurry:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_sbe_slurry, true);
				break;
			}
		}
		// LKM: spring plough done after the 1st of March and before the 25th of March - 20% of farmers without clay soils
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), fi_sbe_spring_plough, false);
		break;
	case fi_sbe_spring_plough:
		if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS) - Maybe not the same scale for Finland?
			if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
			{
				if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear()))
				{
					SimpleEvent(g_date->Date() + 1, fi_sbe_spring_plough, true);
					break;
				}
			}
		}
		// LKM: Next event is preseeding cultivation (a few days before or on the same day as sowing)
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), fi_sbe_preseeding_cultivation, false);
		break;
	case fi_sbe_preseeding_cultivation:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_preseeding_cultivation, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), fi_sbe_preseeding_sow, false);
		break;
	case fi_sbe_preseeding_sow:
			// LKM: Next event differs depending on whether there is preseeding cultivation with sow or not - suggests 50% each (is not stated)
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_sbe_preseeding_sow, true);
				break;
			}
			// LKM: Queue up the next event - fertilizer done the same day
			SimpleEvent(g_date->Date(), fi_sbe_fertilizer1, false);
			break;
		}
		else if (m_ev->m_lock || m_farm->DoIt_prob(0.50 / 0.50)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), fi_sbe_sow, false);
		}
	    break;
		// LKM: sow without preseeding cultivator at the same time (fertilizer at the same time) (50%)
	case fi_sbe_sow:
		if (!m_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_sow, true);
			break;
		}
		// LKM: Queue up the next event - harrow before emergence
		SimpleEvent(g_date->Date()+7, fi_sbe_harrow, false);
		break;
	case fi_sbe_fertilizer1:
		// LKM: done on same day as preseeding sow - no later than May 10
		if (!m_farm->FP_NPKS(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_fertilizer1, true);
			break;
		}
		// LKM: Queue up the next event - harrow before emergence
		SimpleEvent(g_date->Date() + 7, fi_sbe_harrow, false);
		break;
	case fi_sbe_harrow:
		// LKM: 20-30% does this
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30)) {
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(17, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_sbe_harrow, true);
				break;
			}
		}
		//Here comes a fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 5), fi_sbe_herbicide1, false); //herbicide1 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), fi_sbe_insecticide, false); //insecticide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 6), fi_sbe_fertilizer2, false); //fertilizer thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 7), fi_sbe_fungicide, false); //fungicide thread - main thread
		break;
	case fi_sbe_herbicide1:
		// LKM: start of herbicide1 thread - done before 10th of June
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_herbicide1, true);
			break;
		} 
		SimpleEvent(g_date->Date()+10, fi_sbe_herbicide2, false);
		break;
	case fi_sbe_herbicide2:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(24, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_herbicide2, true);
			break;
		} 
		SimpleEvent(g_date->Date() + 10, fi_sbe_herbicide3, false);
		break;
	case fi_sbe_herbicide3:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(8, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_herbicide3, true);
			break;
		} //end of thread
		break;
	case fi_sbe_insecticide:
		// LKM: start of insecticide thread - done before 15th of June
		if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_insecticide, true);
			break;
		} //end of thread
		break;
	
	case fi_sbe_fertilizer2:
		// LKM: start of fertilizer2 thread - done before 5th of July
		// LKM: 20-30% does this
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30)) {
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_sbe_fertilizer2, true);
				break;
			}
		} //end of thread
		break;
	case fi_sbe_fungicide:
		// LKM: start of fungicide thread - done before 5th of August
		// LKM: 10-20% does this
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_sbe_fungicide, true);
				break;
			}
		} 
		// LKM: Queue up the next event - harvest done after the 1st of October
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), fi_sbe_harvest, false);
		break;
		// LKM: do harvest before 10th of November 
	case fi_sbe_harvest:
		if (!m_farm->HarvestLong(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_harvest, true);
			break;
		}
		m_field->SetVegPatchy(false);
		// LKM: Queue up the next event - plough done after the 25th of October
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 10), fi_sbe_autumn_plough2, false);
		break;
		// LKM: do plough before 31st of November 
	case fi_sbe_autumn_plough2:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(31, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_sbe_autumn_plough2, true);
			break;
		}
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "FI_Sugarbeet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}





	


