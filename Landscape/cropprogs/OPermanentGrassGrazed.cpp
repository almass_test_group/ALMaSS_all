//
// PermanentGrassGrazed.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OPermanentGrassGrazed.h"

extern CfgBool cfg_organic_extensive;

bool OPermanentGrassGrazed::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case opgg_start:
    {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(1,10);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(25,6));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(1,10));
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OPermanentGrassGrazed::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // No need to try to fix late fininshing, this crop cannot be in
        // rotation, and must be followed by itself
      }
      int d1;

      int today=g_date->Date();
      d1 = g_date->OldDays() + m_first_date;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if (today > d1)
      {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, "OPermanentGrassGrazed::Do(): "
		 "Crop start attempt after last possible start date", "" );
          exit( 1 );
      }
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
	  m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification
	  
      OPGG_FERTI_DATE=0;
      OPGG_CUT_DATE=0;
      if (m_farm->IsStockFarmer())
      	SimpleEvent( d1, opgg_ferti_s, false );
      else SimpleEvent( d1+60, opgg_cut_to_hay, false );
    }
    break;

  case opgg_ferti_s:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 15,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, opgg_ferti_s, true );
        break;
      }
      OPGG_FERTI_DATE=g_date->DayInYear();
      SimpleEvent( g_date->Date() + 28,opgg_cut_to_hay, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,6 ),
                   opgg_cut_to_hay, false );
    break;

  case opgg_cut_to_hay:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->CutToHay( m_field, 0.0,
           g_date->DayInYear( 15,8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, opgg_cut_to_hay, true );
        break;
      }
      // did cut to hay so try turning
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 26,6 ),
		               opgg_raking1, false );
      OPGG_CUT_DATE=g_date->DayInYear();
      break;
    }
    if (OPGG_FERTI_DATE+28 < g_date->DayInYear( 15, 6 ))
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 6 ),
                 opgg_cattle_out2, false ); // 80% of non-cutters graze
    else
    SimpleEvent( g_date->OldDays() + OPGG_FERTI_DATE+28,
                 opgg_cattle_out2, false ); // 80% of non-cutters graze

    break;

  case opgg_raking1:
    if (!m_farm->HayTurning( m_field, 0.0,
         (OPGG_CUT_DATE+10) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, opgg_raking1, true );
      break;
    }
    else
    	OPGG_TURN_DATE=g_date->DayInYear()+7;
    SimpleEvent( g_date->Date()+1, opgg_raking2, false );
    break;

  case opgg_raking2:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->HayTurning( m_field, 0.0,
           OPGG_TURN_DATE - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, opgg_raking2, true );
        break;
      }
      else
      	OPGG_TURN_DATE=g_date->DayInYear()+7;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear()+1,
                 opgg_compress_straw, false );
    break;

  case opgg_compress_straw:
    if (!m_farm->HayBailing( m_field, 0.0,
           OPGG_TURN_DATE - g_date->DayInYear()))
    {
      SimpleEvent( g_date->Date() + 1, opgg_compress_straw, true );
      break;
    }
    // Did compress straw
    SimpleEvent( g_date->OldDays() + g_date->DayInYear()+21,
                 opgg_cattle_out1, false );    // cut but no straw so 100% graze
    break;

  case opgg_cattle_out1:
  if (cfg_organic_extensive.value()){
    if (!m_farm->CattleOutLowGrazing( m_field, 0.0,
             g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
          SimpleEvent( g_date->Date() + 1, opgg_cattle_out1, true );
          break;
        }
  }
  else {
    if (!m_farm->CattleOut( m_field, 0.0,
             g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
          SimpleEvent( g_date->Date() + 1, opgg_cattle_out1, true );
          break;
        }
  }
    SimpleEvent( g_date->Date() + 1, opgg_cattle_is_out, false );
    break;

  case opgg_cattle_out2:
    if ( ( m_ev->m_lock || m_farm->DoIt( 80 )) || (OPGG_FERTI_DATE!=0) )
    {
      if (cfg_organic_extensive.value()){
        if (!m_farm->CattleOutLowGrazing( m_field, 0.0,
             g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
          SimpleEvent( g_date->Date() + 1, opgg_cattle_out2, true );
          break;
        }
      }
      else {
        if (!m_farm->CattleOut( m_field, 0.0,
             g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
          SimpleEvent( g_date->Date() + 1, opgg_cattle_out2, true );
          break;
        }
      }
      // Success
      SimpleEvent( g_date->Date() + 1, opgg_cattle_is_out, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear(5,9),
                 opgg_cut_weeds, false );
    break;

  case opgg_cattle_is_out:
  if (cfg_organic_extensive.value()){
    if (!m_farm->CattleIsOutLow( m_field, 0.0,
         g_date->DayInYear( 1, 10 ) - g_date->DayInYear(),g_date->DayInYear( 1, 10 )))
    {
      SimpleEvent( g_date->Date() + 1, opgg_cattle_is_out, true );
      break;
    }
  }
  else {
    if (!m_farm->CattleIsOut( m_field, 0.0,
         g_date->DayInYear( 1, 10 ) - g_date->DayInYear(),g_date->DayInYear( 1, 10 )))
    {
      SimpleEvent( g_date->Date() + 1, opgg_cattle_is_out, true );
      break;
    }
  }
    // if they come in the send them out if too early
    if (g_date->DayInYear()<g_date->DayInYear(10,9))
    {
      SimpleEvent( g_date->Date()+1, opgg_cattle_out2, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear(15,6),
                 opgg_cut_weeds, false );
    break;

  case opgg_cut_weeds:
    if (( m_ev->m_lock || m_farm->DoIt( 25 ))&&
                                     (g_date->Date()<g_date->DayInYear(15,9)))
    {
      if (!m_farm->CutWeeds( m_field, 0.0,
           g_date->DayInYear( 15,9 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, opgg_cut_weeds, true );
        break;
      }
    }
    // END MAIN THREAD
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "PermanantGrassGrazed::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


