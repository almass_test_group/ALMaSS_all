/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_Carrots_Spring.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_carrots_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_Carrots_Spring::Do( Farm * a_farm, LE * a_field, FarmEvent * a_ev ) {
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKCarrots_Spring;

  switch (m_ev->m_todo) {
  case dk_cars_start:
  {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
              // The next bit of code just allows for altering dates after harvest if it is necessary
              // to allow for a crop which starts its management early.

              // 2 start and stop dates for all 'movable' events for this crop
      int noDates = 1;
      m_field->SetMDates(0, 0, g_date->DayInYear(30, 11)); 
      m_field->SetMDates(1, 0, g_date->DayInYear(30, 11));// last possible day of harvest

      m_field->SetMConstants(0, 1);

      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

      //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
      //optimising farms not used for now so most of related code is removed (but not in 'start' case)
      if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

          if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
              if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
              {
                  g_msg->Warn(WARN_BUG, "DK_Carrots_Spring::Do(): ", "Harvest too late for the next crop to start!!!");
                  int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
              }
              // Now fix any late finishing problems
              for (int i = 0; i < noDates; i++) {
                  if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
                      m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
                  }
                  if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
                      m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
                      m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
                  }
              }
          }
          // Now no operations can be timed after the start of the next crop.

          if (!m_ev->m_first_year) {
              // Are we before July 1st?
              d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
              if (g_date->Date() < d1) {
                  // Yes, too early. We assumme this is because the last crop was late
                  printf("Poly: %d\n", m_field->GetPoly());
                  g_msg->Warn(WARN_BUG, "DK_Carrots_Spring::Do(): ", "Crop start attempt between 1st Jan & 1st July");
                  int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
                  g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
                  int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
              }
              else {
                  d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
                  if (g_date->Date() > d1) {
                      // Yes too late - should not happen - raise an error
                      g_msg->Warn(WARN_BUG, "DK_Carrots_Spring::Do(): ", "Crop start attempt after last possible start date");
                      g_msg->Warn(WARN_BUG, "Previous Crop ", "");
                      //m_field->GetOwner()->GetPreviousCrop(m_field->GetRotIndex());
                      int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                      g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
                  }
              }
          }
          else 
          {
              // If this is the first year of running then it is possible to start
              // on day 0, so need this to tell us what to do:
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2), dk_cars_stoneburier, false);
              break;
          }
      }//if

          // End single block date checking code. Please see next line
          // comment as well.
          // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date;
      if (!m_ev->m_first_year) d1 += 365; // Add 365 for spring crop (not 1st yr)
      if (g_date->Date() > d1) {
          d1 = g_date->Date();
      }
      // This is the first real farm operation
      SimpleEvent(d1, dk_cars_stoneburier, false);
  }
      break;
      // LKM: Do stoneburier before 15th of April - if not done, try again +1 day until the the 15th of April when we succeed 
  case dk_cars_stoneburier:
      if (!m_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_cars_stoneburier, true);
          break;
      }
      // LKM: Queue up the next event - Deep harrow done before the 20th of April - if not done, try again +1 day until the 20th of April when we will succeed
      SimpleEvent(g_date->Date(), dk_cars_deep_harrow, false);
      break;
  case dk_cars_deep_harrow:
      if (!m_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_cars_deep_harrow, true);
          break;
      }
      // LKM: Queue up the next event - Bedformer done before the 20th of April - if not done, try again +1 day until the 20th of April when we will succeed
      SimpleEvent(g_date->Date(), dk_cars_bedformer, false);
      break;
  case dk_cars_bedformer:
      if (!m_farm->BedForming(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_cars_bedformer, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow1 (making seedbed) done after the 5th of February and before the 5th of May - if not done, try again +1 day until the 5th of May when we will succeed
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 2) , dk_cars_sharrow1, false);
      break;
  case dk_cars_sharrow1:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_sharrow1, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow2 (making seedbed) done 5 days after, and before the 10th of May - if not done, try again +1 day until the 10th of May when we will succeed
      SimpleEvent(g_date->Date() + 5, dk_cars_sharrow2, false);
      break;
  case dk_cars_sharrow2:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_sharrow2, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow3 (making seedbed) done 5 days after, and before the 15th of May - if not done, try again +1 day until the 15th of May when we will succeed
      SimpleEvent(g_date->Date() + 5, dk_cars_sharrow3, false);
      break;
  case dk_cars_sharrow3:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_sharrow3, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow4 (making seedbed) done 5 days after, and before the 20th of May - if not done, try again +1 day until the 20th of May when we will succeed
      SimpleEvent(g_date->Date() + 5, dk_cars_sharrow4, false);
      break;
  case dk_cars_sharrow4:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_sharrow4, true);
          break;
      }
      // LKM: Queue up the next event - water1 done before the 25th of May - if not done, try again +1 day until the 25th of May when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_water1, false);
      break;
  case dk_cars_water1:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_water1, true);
          break;
      }
      // LKM: Queue up the next event - fungicide1 done  before the 30th of May - if not done, try again +1 day until the 30th of May when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_fungicide1, false);
      break;
  case dk_cars_fungicide1:
      if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_fungicide1, true);
          break;
      }
      // LKM: Queue up the next event - sow done  before the 30th of May - if not done, try again +1 day until the 30th of May when we will succeed
      SimpleEvent(g_date->Date(), dk_cars_sow, false);
      break;
  case dk_cars_sow:
      if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_sow, true);
          break;
      }
      // LKM: Queue up the next event - herbicide2 done  before the 5th of June - if not done, try again + 1 day until the 5th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_herbicide2, false);
      break;
  case dk_cars_herbicide2:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_herbicide2, true);
          break;
      }
      // LKM: Queue up the next event - herbicide3 done  before the 10th of June - if not done, try again + 1 day until the 10th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_herbicide3, false);
      break;
  case dk_cars_herbicide3:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_herbicide3, true);
          break;
      }
      // LKM: Queue up the next event - fertilizer1 done  before the 15th of June - if not done, try again + 1 day until the 15th of June  when we will succeed
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->Date() + 1, dk_cars_ferti_s1, false);
          break;
      }
      else
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_ferti_p1, false);
          break;
      }

  case dk_cars_ferti_s1:
      if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_ferti_s1, true);
          break;
      }// LKM: Queue up the next event - herbicide4 + row cultivation1 at the same time done before the 20th of June - if not done, try again + 1 day until the 20th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_herbicide4, false);
      break;

  case dk_cars_ferti_p1:
      if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_ferti_p1, true);
          break;
      }// LKM: Queue up the next event - herbicide4 + row cultivation1 at the same time done before the 20th of June - if not done, try again + 1 day until the 20th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_herbicide4, false);
      break;
      
  case dk_cars_herbicide4:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_herbicide4, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_cars_row_cultivation1, false);
      break;
  case dk_cars_row_cultivation1:
      if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_row_cultivation1, true);
          break;
      }
      // LKM: Queue up the next event - herbicide5 + row cultivation2 (+8 days after) at the same time done before the 30th of June - if not done, try again + 1 day until the 30th of June  when we will succeed
      SimpleEvent(g_date->Date() + 8, dk_cars_herbicide5, false);
      break;
  case dk_cars_herbicide5:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_herbicide5, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_cars_row_cultivation2, false);
      break;
  case dk_cars_row_cultivation2:
      if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_row_cultivation2, true);
          break;
      }
      // LKM: Queue up the next event - water2 done before the 30th of June - if not done, try again +1 day until the 30th of June when we will succeed
      SimpleEvent(g_date->Date(), dk_cars_water2, false);
      break;
  case dk_cars_water2:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_water2, true);
          break;
      }
      // LKM: Queue up the next event - hilling up1 done before the 5th of July - if not done, try again +1 day until the 5th of July when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_hilling_up1, false);
      break;
  case dk_cars_hilling_up1:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_hilling_up1, true);
          break;
      }
      // LKM: Queue up the next event - herbicide6 done before the 10th of July - if not done, try again + 1 day until the 10th of July  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_herbicide6, false);
      break;
  case dk_cars_herbicide6:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_herbicide6, true);
          break;
      }
      // LKM: Queue up the next event - add boron1 done before the 10th of July - if not done, try again + 1 day until the 10th of July  when we will succeed
      SimpleEvent(g_date->Date(), dk_cars_boron_s1, false);
      break;
  case dk_cars_boron_s1:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(11, 7) - g_date->DayInYear()))
          {
              SimpleEvent(g_date->Date() + 1, dk_cars_boron_s1, true);
              break;
          }
          // LKM: Queue up the next event - water3 done before the 20th of July - if not done, try again +1 day until the 10th of July when we will succeed
          SimpleEvent(g_date->Date() + 1, dk_cars_water3, false);
          break;
      }
      else
      SimpleEvent(g_date->Date(), dk_cars_boron_p1, false);
      break;

  case dk_cars_boron_p1:
      if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(11, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_boron_p1, true);
          break;
      }
      // LKM: Queue up the next event - water3 done before the 20th of July - if not done, try again +1 day until the 10th of July when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_water3, false);
      break;

  case dk_cars_water3:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_water3, true);
          break;
      }
      // LKM: Queue up the next event - hilling up2 done before the 30th of July - if not done, try again +1 day until the 30th of July when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_hilling_up2, false);
      break;
  case dk_cars_hilling_up2:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_hilling_up2, true);
          break;
      }
      // LKM: Queue up the next event - boron2 done after the 1st of May, and before the 30th of August - if not done, try again +1 day until the 30th of August when we will succeed
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) , dk_cars_boron_s2, false);
      break;
  case dk_cars_boron_s2:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(28, 8) - g_date->DayInYear()))
          {
              SimpleEvent(g_date->Date() + 1, dk_cars_boron_s2, true);
              break;
          }
          // LKM: Queue up the next event - hilling up3 done before the 30th of August - if not done, try again +1 day until the 30th of July when we will succeed
          SimpleEvent(g_date->Date(), dk_cars_hilling_up3, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_cars_boron_p2, false);
      break;

  case dk_cars_boron_p2:
      if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(28, 8) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_boron_p2, true);
          break;
      }
      // LKM: Queue up the next event - hilling up3 done before the 30th of August - if not done, try again +1 day until the 30th of July when we will succeed
      SimpleEvent(g_date->Date(), dk_cars_hilling_up3, false);
      break;

  case dk_cars_hilling_up3:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(29, 8) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_hilling_up3, true);
          break;
      }
      // LKM: Queue up the next event - fertilizer2 done after the 10th of June and before the 30th of August - if not done, try again +1 day until the 30th of August when we will succeed
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_cars_ferti_s2, false);
          break;
      }
      else
      {
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_cars_ferti_p2, false);
      break;
      }
      break;

  case dk_cars_ferti_s2:
      if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_ferti_s2, true);
          break;
      }
      // LKM: Here is a fork leading to two parallel events
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_cars_insecticide1, false); // Insecticide thread
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_cars_fungicide2, false); // Fungicide thread
      break;

  case dk_cars_ferti_p2:
      if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_ferti_p2, true);
          break;
      }
      // LKM: Here is a fork leading to two parallel events
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_cars_insecticide1, false); // Insecticide thread
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_cars_fungicide2, false); // Fungicide thread - main thread
      break;
      
  case dk_cars_insecticide1:
      // here we check whether we are using ERA pesticide or not
      d1 = g_date->DayInYear(30, 9) - g_date->DayInYear();
      if (!cfg_pest_carrots_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
      {
          flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
      }
      else {
          flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
      }
      if (!flag) {
          SimpleEvent(g_date->Date() + 1, dk_cars_insecticide1, true);
          break;
      }
      //LKM: Queue up the next event - insecticide2 (+ 7 days after) done before the 7th of October - if not done, try again +1 day until the 7th of October when we will succeed
      SimpleEvent(g_date->Date() + 7, dk_cars_insecticide2, false);
      break;
  case dk_cars_insecticide2:
      // here we check whether we are using ERA pesticide or not
      d1 = g_date->DayInYear(8, 10) - g_date->DayInYear();
      if (!cfg_pest_carrots_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
      {
          flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
      }
      else {
          flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
      }
      if (!flag) {
          SimpleEvent(g_date->Date() + 1, dk_cars_insecticide2, true);
          break;
      }
      //End of thread
      break;
  case dk_cars_fungicide2:
      // Here comes the fungicide thread - done before the 30th of September - if not done, try again +1 day until the 30th of September when we will succeed
      if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_fungicide2, true);
          break;
      }
      // LKM: Queue up the next event - fungicide3 (+ 14 days after) done before the 15th of October - if not done, try again +1 day until the 15th of October when we will succeed
      SimpleEvent(g_date->Date() + 14, dk_cars_fungicide3, false);
      break;
  case dk_cars_fungicide3:
      if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_cars_fungicide3, true);
          break;
      }
   
      // LKM: Queue up the next event - harvest done before the 30th of November - if not done, try again +1 day until the 30th of November when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_cars_harvest, false);
      break;
  case dk_cars_harvest:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_cars_harvest, true);
              break;
          }
          d1 = g_date->Date();
          if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_cars_wait, false);
              // Because we are ending harvest before 1.7 so we need to wait until the 1.7
              break;
          }
          else {
              done = true; // end of plan
          }
      }
  case dk_cars_wait:
      done = true;
      break;
  default:
      g_msg->Warn(WARN_BUG, "DK_Carrots_Spring::Do(): ""Unknown event type! ", "");
      exit(1);
  }

  return done;
}


