/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>ITGrassland.cpp This file contains the source for the ITGrassland class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2022 \n
 \n
*/
//
// ITGrassland.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/ITGrassland.h"


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool ITGrassland::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case it_g_start:
	{
		IT_G_FERTI_S1 = false;
		IT_G_FERTI_P1 = false;
		// Check the next crop for early start, unless it is a spring crop
				// in which case we ASSUME that no checking is necessary!!!!
				// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.
		a_field->ClearManagementActionSum();

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {
			//Checking the future...
			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(1, 7) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "ITGrassland::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "ITGrassland::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "ITGrassland::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				//only for the first year 
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 3), it_g_sleep_all_day, false);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // OK, let's go.
		// Here we queue up the first event
		d1 = g_date->OldDays() + m_first_date;
		if (!m_ev->m_first_year) d1 += 365; // Add 365 for spring crop (not 1st yr)
		if (g_date->Date() > d1) {
			d1 = g_date->Date();
		}
		// OK, let's go. - LKM: Queue first operation 
		SimpleEvent(d1, it_g_sleep_all_day, false);
	}
	break; 

	case it_g_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_g_sleep_all_day, true);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), it_g_manure_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), it_g_manure_p1, false, m_farm, m_field);
		break;

	case it_g_manure_s1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.50)) { // 50% suggestion
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_g_manure_s1, true);
				break;
			}
			else
			//Rest of farmers do manure in autumn so we need to remember who already did it
			IT_G_FERTI_S1 = true;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), it_g_cutting1, false);
		break;

	case it_g_manure_p1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.50)) { // 50% suggestion
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_g_manure_p1, true);
				break;
			}
			else
			//Rest of farmers do manure in autumn so we need to remember who already did it
			IT_G_FERTI_P1 = true;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), it_g_cutting1, false);
		break;

	case it_g_cutting1:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00)) {
			if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_g_cutting1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 30, it_g_cutting2, false); // suggestion of days between cuts
		break;

	case it_g_cutting2:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00)) {
			if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_g_cutting2, true);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 11), it_g_manure_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 11), it_g_manure_p2, false, m_farm, m_field);
		break;

	case it_g_manure_s2:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.0) && IT_G_FERTI_S1 == 0) {
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_g_manure_s2, true);
				break;
			}
		}
		// End of management 
		done = true;
		break;

	case it_g_manure_p2:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.0) && IT_G_FERTI_P1 == 0) {
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_g_manure_p2, true);
				break;
			}
		}
		// End of management 
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "ITGrassland::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}