/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_OWinterRye.cpp This file contains the source for the DE_OWinterRye class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_OWinterRye.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OWinterRye.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_winterrye_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_OWR_InsecticideDay;
extern CfgInt   cfg_OWR_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter Rye.
*/
bool DE_OWinterRye::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
    bool flag = false;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
  switch (m_ev->m_todo)
    {
    case de_owry_start:
    {
        DE_OWRY_PLOUGH_RUNS = false;
        DE_OWRY_HARROW_RUNS = false;
        a_field->ClearManagementActionSum();

        // Set up the date management stuff
        m_last_date = g_date->DayInYear(15, 9);
        // Start and stop dates for all events after harvest
        int noDates = 7;
        m_field->SetMDates(0, 0, g_date->DayInYear(1, 8));
        // Determined by harvest date - used to see if at all possible
        m_field->SetMDates(1, 0, g_date->DayInYear(20, 8));
        m_field->SetMDates(0, 1, g_date->DayInYear(5, 8));
        m_field->SetMDates(1, 1, g_date->DayInYear(5, 9));
        m_field->SetMDates(0, 2, g_date->DayInYear(25, 8));
        m_field->SetMDates(1, 2, g_date->DayInYear(25, 8));
        m_field->SetMDates(0, 3, g_date->DayInYear(25, 8));
        m_field->SetMDates(1, 3, g_date->DayInYear(30, 8));
        m_field->SetMDates(0, 4, g_date->DayInYear(5, 8));
        m_field->SetMDates(1, 4, g_date->DayInYear(5, 8)); // not needed
        m_field->SetMDates(0, 5, g_date->DayInYear(15, 9));
        m_field->SetMDates(1, 5, g_date->DayInYear(15, 9));
        m_field->SetMDates(0, 6, g_date->DayInYear(1, 8));
        m_field->SetMDates(1, 6, g_date->DayInYear(30, 8));
        // Check the next crop for early start, unless it is a spring crop
        // in which case we ASSUME that no checking is necessary!!!!
        // So DO NOT implement a crop that runs over the year boundary
        if (m_ev->m_startday > g_date->DayInYear(1, 7))
        {
            if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
            {
                g_msg->Warn(WARN_BUG, "DE_OWinterRye::Do(): "
                    "Harvest too late for the next crop to start!!!", "");
                exit(1);
            }
            // Now fix any late finishing problems
            for (int i = 0; i < noDates; i++)
            {
                if (m_field->GetMDates(0, i) >= m_ev->m_startday)
                    m_field->SetMDates(0, i, m_ev->m_startday - 1);
                if (m_field->GetMDates(1, i) >= m_ev->m_startday)
                    m_field->SetMDates(1, i, m_ev->m_startday - 1);
            }
        }
        // Now no operations can be timed after the start of the next crop.

        int d1;
        if (!m_ev->m_first_year)
        {
            int today = g_date->Date();
            // Are we before July 1st?
            d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
            if (today < d1)
            {
                // Yes, too early. We assumme this is because the last crop was late
                g_msg->Warn(WARN_BUG, "DE_OWinterRye::Do(): "
                    "Crop start attempt between 1st Jan & 1st July", "");
                exit(1);
            }
            else
            {
                d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
                if (today > d1)
                {
                    // Yes too late - should not happen - raise an error
                    g_msg->Warn(WARN_BUG, "DE_OWinterRye::Do(): "
                        "Crop start attempt after last possible start date", "");
                    exit(1);
                }
            }
        }
        else
        {
            SimpleEvent_(g_date->OldDays() + g_date->DayInYear(28, 8),
                de_owry_harvest, true, m_farm, m_field);
            break;
        }
        // End single block date checking code. Please see next line
        // comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(15, 8);// Add 365 for spring crop
        if (g_date->Date() > d1) {
            d1 = g_date->Date();
        }

        // OK, let's go.
        if (m_farm->IsStockFarmer()) // StockFarmer
        {
            SimpleEvent_(d1, de_owry_ferti_s1, true, m_farm, m_field);
        }
        else SimpleEvent_(d1, de_owry_ferti_p1, true, m_farm, m_field);
    }
    break;

    case de_owry_ferti_p1:
        if (m_ev->m_lock || m_farm->DoIt(10))
        {
            if (!m_farm->FP_Manure(m_field, 0.0,
                g_date->DayInYear(30, 9) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_owry_ferti_p1, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->Date(), de_owry_autumn_plough, true, m_farm, m_field);
        break;

    case de_owry_ferti_s1:
        if (m_ev->m_lock || m_farm->DoIt(40))
        {
            if (!m_farm->FA_Manure(m_field, 0.0,
                g_date->DayInYear(30, 9) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_owry_ferti_s1, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->Date(), de_owry_autumn_plough, true, m_farm, m_field);
        break;

    case de_owry_autumn_plough:
        if (!m_farm->AutumnPlough(m_field, 0.0,
            g_date->DayInYear(30, 9) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_autumn_plough, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date() + 1, de_owry_autumn_harrow, true, m_farm, m_field);
        break;

    case de_owry_autumn_harrow:
        if (!m_farm->AutumnHarrow(m_field, 0.0,
            g_date->DayInYear(30, 9) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_autumn_harrow, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 9),
            de_owry_autumn_sow, true, m_farm, m_field);
        break;

    case de_owry_autumn_sow:
        if (!m_farm->AutumnSow(m_field, 0.0,
            g_date->DayInYear(10, 10) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_autumn_sow, true, m_farm, m_field);
            break;
        }
        {
            long newdate1 = g_date->OldDays() + g_date->DayInYear(10, 9);
            long newdate2 = g_date->Date() + 10;
            if (newdate2 > newdate1)
                newdate1 = newdate2;
            SimpleEvent_(newdate1, de_owry_strigling1, true, m_farm, m_field);
        }
        break;

    case de_owry_strigling1:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(10, 10) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_strigling1, true, m_farm, m_field);
            break;
        }
        // Next year
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 2) + 365,
            de_owry_spring_roll1, true, m_farm, m_field);
        break;

    case de_owry_spring_roll1:
        if (m_ev->m_lock || m_farm->DoIt(5))
        {
            if (!m_farm->SpringRoll(m_field, 0.0,
                g_date->DayInYear(5, 3) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_owry_spring_roll1, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 3),
            de_owry_strigling2, true, m_farm, m_field);
        break;
    case de_owry_strigling2:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(20, 3) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_strigling2, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 3),
            de_owry_strigling3, true, m_farm, m_field);
        break;

    case de_owry_strigling3:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(15, 4) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_strigling3, true, m_farm, m_field);
            break;
        }
        if (m_farm->IsStockFarmer()) // StockFarmer
        {
            SimpleEvent_(g_date->Date() + 1, de_owry_ferti_s2, true, m_farm, m_field);
        }
        else
            SimpleEvent_(g_date->Date() + 1, de_owry_ferti_p2, true, m_farm, m_field);
        break;

    case de_owry_ferti_p2:
        if (m_ev->m_lock || m_farm->DoIt(50))
        {
            if (!m_farm->FP_Manure(m_field, 0.0,
                g_date->DayInYear(30, 4) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_owry_ferti_p2, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_owry_harvest, true, m_farm, m_field);
        break;

    case de_owry_ferti_s2:
        if (!m_farm->FA_Manure(m_field, 0.0,
            g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_ferti_s2, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_owry_ferti_s3, true, m_farm, m_field);
        break;

    case de_owry_ferti_s3:
        if (m_ev->m_lock || m_farm->DoIt(50))
        {
            if (!m_farm->FA_Slurry(m_field, 0.0,
                g_date->DayInYear(30, 5) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_owry_ferti_s3, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7),
            de_owry_harvest, true, m_farm, m_field);
        break;

    case de_owry_harvest:
        if (!m_farm->Harvest(m_field, 0.0,
            m_field->GetMDates(1, 0) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_harvest, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date(), de_owry_straw_chopping, false, m_farm, m_field);
        break;

    case de_owry_straw_chopping:
    {
        if (m_ev->m_lock || m_farm->DoIt(60))
        {
            if (!m_farm->StrawChopping(m_field, 0.0,
                m_field->GetMDates(1, 0) - g_date->DayInYear()))
            {
                SimpleEvent_(g_date->Date() + 1, de_owry_straw_chopping, true, m_farm, m_field);
                break;
            }
            else
            {
                // Did Chopping so test for stubble harrow/deep_plough
                SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8),
                    de_owry_deep_plough, true, m_farm, m_field);
                SimpleEvent_(g_date->OldDays() + m_field->GetMDates(0, 1),
                    de_owry_stubble_harrow1, true, m_farm, m_field);
                break;
            }
        }
        int d1 = g_date->Date() + 3;
        if (d1 > m_field->GetMDates(0, 2)) d1 = m_field->GetMDates(0, 2);
        SimpleEvent_(d1, de_owry_hay_turning, true, m_farm, m_field);
    }
    break;

    case de_owry_hay_turning:
        if (m_ev->m_lock || m_farm->DoIt(20))
        {
            if (!m_farm->HayTurning(m_field, 0.0,
                m_field->GetMDates(1, 2) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_owry_hay_turning, true, m_farm, m_field);
                break;
            }
        }
        {
            long d1 = g_date->OldDays() + g_date->DayInYear(7, 8);
            long d2 = g_date->Date() + 3;
            if (d2 > d1) d1 = d2;
            if (d1 > m_field->GetMDates(0, 3)) d1 = m_field->GetMDates(0, 3);
            SimpleEvent_(d1, de_owry_hay_bailing, true, m_farm, m_field);
        }
        break;

    case de_owry_hay_bailing:
        if (!m_farm->HayBailing(m_field, 0.0,
            m_field->GetMDates(1, 3) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_owry_hay_bailing, true, m_farm, m_field);
            break;
        }
        // These events will almost certainly predate
        // 'today' due to the ending date used above.
        SimpleEvent_(g_date->OldDays() + m_field->GetMDates(0, 6),
            de_owry_deep_plough, true, m_farm, m_field);
        SimpleEvent_(g_date->OldDays() + m_field->GetMDates(0, 4),
            de_owry_stubble_harrow1, true, m_farm, m_field);
        break;

    case de_owry_stubble_harrow1:
        DE_OWRY_HARROW_RUNS = true;
        if (m_ev->m_lock || m_farm->DoIt(20))
        {
            if (!m_farm->StubbleHarrowing(m_field, 0.0,
                m_field->GetMDates(1, 1) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_owry_stubble_harrow1, true, m_farm, m_field);
                break;
            }
            int d1 = g_date->Date() + 10;
            if (d1 > m_field->GetMDates(0, 5)) d1 = m_field->GetMDates(0, 5);
            SimpleEvent_(d1, de_owry_stubble_harrow2, true, m_farm, m_field);
            break;
        }
        DE_OWRY_HARROW_RUNS = false;
        SimpleEvent_(g_date->Date(), de_owry_catch_all, true, m_farm, m_field);
        break;

    case de_owry_stubble_harrow2:
        if (!m_farm->StubbleHarrowing(m_field, 0.0,
            m_field->GetMDates(1, 5) - g_date->DayInYear()))
        {
            SimpleEvent_(g_date->Date() + 1, de_owry_stubble_harrow2, true, m_farm, m_field);
            break;
        }
        DE_OWRY_HARROW_RUNS = false;
        SimpleEvent_(g_date->Date(), de_owry_catch_all, true, m_farm, m_field);
        break;

    case de_owry_deep_plough:
        DE_OWRY_PLOUGH_RUNS = true;
        if (m_ev->m_lock || m_farm->DoIt(5))
        {
            if (!m_farm->DeepPlough(m_field, 0.0,
                m_field->GetMDates(1, 6) - g_date->DayInYear()))
            {
                SimpleEvent_(g_date->Date() + 1, de_owry_deep_plough, true, m_farm, m_field);
                break;
            }
            // if we deep plough then finish plan here
            done = true;
            // END OF MAIN THREAD
            break; //
        }
        DE_OWRY_PLOUGH_RUNS = false;
        SimpleEvent_(g_date->Date(), de_owry_catch_all, true, m_farm, m_field);
        break;

    case de_owry_catch_all:
        if (!DE_OWRY_PLOUGH_RUNS || !DE_OWRY_HARROW_RUNS)
            done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "DE_OWinterRye::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }
    return done;
}