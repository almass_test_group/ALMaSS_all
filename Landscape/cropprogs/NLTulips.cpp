/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CA LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLTulips.cpp This file contains the source for the NLTulips class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLTulips.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLTulips.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_tulips_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_TU_InsecticideDay;
extern CfgInt   cfg_TU_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional tulips.
*/
bool NLTulips::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case nl_tu_start:
	{
		// nl_tu_start just sets up all the starting conditions and reference dates that are needed to start a nl_ca
		NL_TU_AUTUMN_PLOUGH = false;
		NL_TU_FERTI_DONE = false;
		NL_TU_STRAW_REMOVED = false;
		NL_TU_FUNGI_SPRAY_DATE = 0;
		a_field->ClearManagementActionSum();


		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(20, 7)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(20, 7));

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "NLTulips::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "NLTulips::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "NLTulips::Do(): ", "Crop start attempt after last possible start date");
						int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 2), nl_tu_fungicide1, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_tu_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, nl_tu_ferti_p1, false, a_farm, a_field);
	}
	break;

	// This is the first real farm operation
	case nl_tu_ferti_p1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
		{
			if (!a_farm->FP_Manure(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p1, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 9);
		}
		SimpleEvent_(d1, nl_tu_autumn_plough, false, a_farm, a_field);
		break;
	case nl_tu_ferti_s1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
		{
			if (!a_farm->FA_Manure(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s1, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 9);
		}
		SimpleEvent_(d1, nl_tu_autumn_plough, false, a_farm, a_field);
		break;
	case nl_tu_autumn_plough:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_autumn_plough, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 10, nl_tu_bed_forming, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_tu_autumn_heavy_stubble_cultivator, false, a_farm, a_field);
		break;
	case nl_tu_autumn_heavy_stubble_cultivator:
		if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_autumn_heavy_stubble_cultivator, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 10, nl_tu_bed_forming, false, a_farm, a_field);
		break;
	case nl_tu_bed_forming:
		if (!a_farm->BedForming(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_bed_forming, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide0, false, a_farm, a_field);
		break;
	case nl_tu_fungicide0:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide0, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_planting, false, a_farm, a_field);
		break;
	case nl_tu_planting:
		if (!a_farm->AutumnSow(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_planting, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_herbicide1, false, a_farm, a_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, nl_tu_insecticide1, false, a_farm, a_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_tu_fungicide1, false, a_farm, a_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->Date() + 10, nl_tu_straw_covering, false, a_farm, a_field);	// Flower management
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4) + 365, nl_tu_irrigation, false, a_farm, a_field);	// Flower management
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 5, nl_tu_ferti_s2, false, a_farm, a_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->Date() + 5, nl_tu_ferti_p2, false, a_farm, a_field);
		break;
	case nl_tu_ferti_p2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.40))
		{
			if (!a_farm->FP_PK(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p2, true, a_farm, a_field);
				break;
			}
		}
		NL_TU_FERTI_DONE = true;
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_ferti_p3, false, a_farm, a_field);
		break;
	case nl_tu_ferti_s2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.40))
		{
			if (!a_farm->FA_PK(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s2, true, a_farm, a_field);
				break;
			}
		}
		NL_TU_FERTI_DONE = true;
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_ferti_s3, false, a_farm, a_field);
		break;
	case nl_tu_ferti_p3:
		if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p3, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), nl_tu_ferti_p4, false, a_farm, a_field);
		break;
	case nl_tu_ferti_s3:
		if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s3, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), nl_tu_ferti_s4, false, a_farm, a_field);
		break;
	case nl_tu_ferti_p4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_tu_ferti_s4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_tu_straw_covering:
		if (NL_TU_FERTI_DONE == 0) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_straw_covering, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->StrawCovering(a_field, 0.0, g_date->DayInYear(20, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_straw_covering, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_straw_removal, false, a_farm, a_field);
			break;
		}
		break;
	case nl_tu_straw_removal:
		if (!a_farm->StrawRemoval(a_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_straw_removal, true, a_farm, a_field);
			break;
		}
		NL_TU_STRAW_REMOVED = true;
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 4), nl_tu_flower_cutting, false, a_farm, a_field);
		break;
	case nl_tu_flower_cutting:
		if (!a_farm->FlowerCutting(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_flower_cutting, true, a_farm, a_field);
			break;
		}
		// End of thread
		break;
	case nl_tu_herbicide1: // The first of the pesticide managements.
						   // Here comes the herbicide thread
		if (NL_TU_STRAW_REMOVED == 0) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_herbicide1, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_herbicide1, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 7, nl_tu_herbicide2, false, a_farm, a_field);
			break;
		}
		break;
	case nl_tu_herbicide2:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_herbicide2, true, a_farm, a_field);
			break;
		}
		// End of thread
		break;
	case nl_tu_fungicide1:
		// Here comes the fungicide thread
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide1, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide1, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide1:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide1, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide2, false, a_farm, a_field);
		break;
	case nl_tu_fungicide2:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide2, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide2, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide2:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide2, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide3, false, a_farm, a_field);
		break;
	case nl_tu_fungicide3:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide3, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide3, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide3:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide3, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		SimpleEvent_(d1, nl_tu_fungicide4, false, a_farm, a_field);
		break;
	case nl_tu_fungicide4:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide4, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide4, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide4:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide4, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide5, false, a_farm, a_field);
		break;
	case nl_tu_fungicide5:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide5, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide5, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide5:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide5, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide6, false, a_farm, a_field);
		break;
	case nl_tu_fungicide6:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide6, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide6, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide6:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide6, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
		}
		SimpleEvent_(d1, nl_tu_fungicide7, false, a_farm, a_field);
		break;
	case nl_tu_fungicide7:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide7, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide7, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide7:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide7, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide8, false, a_farm, a_field);
		break;
	case nl_tu_fungicide8:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide8, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide8, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide8:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide8, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide9, false, a_farm, a_field);
		break;
	case nl_tu_fungicide9:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide9, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide9, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide9:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide9, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide10, false, a_farm, a_field);
		break;
	case nl_tu_fungicide10:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide10, true, a_farm, a_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide10, false, a_farm, a_field);
		break;
	case nl_tu_added_insecticide10:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide10, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 6);
		}
		SimpleEvent_(d1, nl_tu_harvest, false, a_farm, a_field);		break;

	case nl_tu_insecticide1:
		if (NL_TU_FUNGI_SPRAY_DATE >= g_date->Date() - 2) { // Should by at least 3 days after fungicide + insecticide
			SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide1, false, a_farm, a_field);
		}
		else
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
			{
				// here we check wheter we are using ERA pesticide or not
				if (!cfg_pest_tulips_on.value() ||
					!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide1, true, a_farm, a_field);
						break;
					}
				}
				else {
					a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
			}
			d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
			}
			SimpleEvent_(d1, nl_tu_insecticide2, false, a_farm, a_field);
			break;
		}
		break;
	case nl_tu_insecticide2:
		if (NL_TU_FUNGI_SPRAY_DATE >= g_date->Date() - 2) { // Should by at least 3 days after fungicide + insecticide
			SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide2, false, a_farm, a_field);
		}
		else
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_tulips_on.value() ||
				!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide2, true, a_farm, a_field);
					break;
				}
			}
			else {
				a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
	case nl_tu_irrigation:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.25))
		{
			if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_irrigation, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_tu_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!a_farm->BulbHarvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_harvest, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date();
		if (d1 <= g_date->OldDays() + g_date->DayInYear(1, 7)) {
			SimpleEvent_(d1, nl_tu_sleep_all_day, false, a_farm, a_field);
			break;
		}
		else {
			done = true;
			break;
		}
		break;
	case nl_tu_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_sleep_all_day, true, a_farm, a_field);
			break;
		}
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLTulips::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}