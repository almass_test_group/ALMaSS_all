/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLCatchCropPea.cpp This file contains the source for the NLCatchCropPea class</B> \n
*/
/**
\file
 by Chris J. Topping and Elzbieta Ziolkowska \n
 Version of June 22nd 2020 \n
 All rights reserved. \n
 \n
*/
//
// NLCatchCropPea.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLCatchCropPea.h"

extern CfgBool  g_farm_fixed_crop_enable;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional CatchPeaCrop.
*/
bool NLCatchCropPea::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLCatchCropPea; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case nl_ccp_start:
		/**
		* Catch crops in ALMaSS cannot be part of the normal farm plan but are created as part of another crop management.
		* They are therefore dependend on the other crops to set up the starting conditions, hence AddNewEvent is called by the calling crop to create m_startday etc..
		*/
		/**
		* Date management control is done by providing pairs of dates as start and stop dates for a management. In this case only harvest can be moved
		* but normally there will be post harvest operations too. These dates are accessed/stored using GetMDates and SetMDates and should be in pairs for each operation.
		* The first non-moveable date is always 0,0, then the crop operations are numbered x,1, x,2 etc for each subsequent operation. 0,y is the start date and 1,y is the end date.
		*/
		m_field->ClearManagementActionSum();

		m_field->SetMDates(0, 0, g_date->DayInYear(1, 1)); // This is the end of active plan first date and cannot be moved. NB this is next year at this point.
		m_field->SetMDates(1, 0, g_date->DayInYear(15, 3)); // This is the last active plan event date
		// If the start date is after summer then this is likely a mistake, so warn about this and exit
		if (m_ev->m_startday > g_date->DayInYear(1, 7))
		{
			g_msg->Warn(WARN_BUG, "NLCatchCropPea::Do(): "
				"Autumn crop following catch crop - if this is correct remove this check from the code", "");
			exit(tov_NLCatchCropPea);
		}
		// Now test for fixable late finishing problems
		if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
		{
			g_msg->Warn(WARN_BUG, "NLCatchCropPea::Do(): Harvest too late for the next crop to start!!!", "");
			int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
			g_msg->Warn("Next Crop ", (double)almassnum);
			exit(tov_NLCatchCropPea);
		}
		// Now fix any late finishing problems - for the code below will work with more than one set of MDate pairs but is not entirely necessary for this crop (cannot move MDates[0,0])
		for (int i = 0; i < noDates; i++) {
			if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
				m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
			}
			if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
				m_field->SetMConstants(i, 0); // Indicates that a date change occured if anyone is looking
				m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
			}
		}
		// Now all the date management things are checked we can start with the main code

		/**
		* If this is a normal year do this, but if its the first year special code MAY be needed because we start in the middle of a crop.
		* In this case we don't need special behaviour because our crop start is not affected by the timing of the first year (always follows another crop)
		* but we do need to check if the finishing dates match the next crop and that we have not started this crop too late.
		*/
		//if (!a_ev->m_first_year) {
			// Are we before July 1st?
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
			// We need to skip this test if its a catch crop that preceded
			if (g_date->Date() < d1)
			{
				// Yes, too early. We assumme this is because the last crop was late
				printf("Poly: %d\n", a_field->GetPoly());
				g_msg->Warn(WARN_BUG, "NLCatchCropPea::Do(): ", "Crop start attempt between 1st Jan & 1st July");
				int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
				g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
				int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
				g_msg->Warn("Next Crop ", (double)almassnum);
				exit(l_tov); // returns the tov number on exit
			}
			else
			{
				d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "NLCatchCropPea::Do(): ", "Crop start attempt after last possible start date");
					g_msg->Warn(WARN_BUG, "Previous Crop ", "");
					a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum);
					exit(l_tov); // returns the tov number on exit
				}
			}
		// Normally the date checking if statement would end here }
		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		// OK, let's go.
		SimpleEvent_(d1, nl_ccp_stubble_cultivator, false, a_farm, a_field);
		break;

		// This is the first real farm operation
	case nl_ccp_stubble_cultivator:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_stubble_cultivator, true, a_farm, a_field);
			break;
		}
		// Here we queue up the nextt event - this differs depending on whether we have a
		// stock or arable farmer
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 5, nl_ccp_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 5, nl_ccp_ferti_p1, false, a_farm, a_field);
		break;

	case nl_ccp_ferti_p1:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_p1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ccp_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case nl_ccp_ferti_s1:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_s1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ccp_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case nl_ccp_preseeding_cultivator_with_sow:
		if (!a_farm->PreseedingCultivatorSow(a_field, 0.0, g_date->DayInYear(16, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_preseeding_cultivator_with_sow, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		}
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			if (a_farm->IsStockFarmer()) SimpleEvent_(g_date->OldDays() + m_field->GetMDates(0, 0) + 365, nl_ccp_ferti_s2_sandy, false, a_farm, a_field);
			else  SimpleEvent_(g_date->OldDays() + m_field->GetMDates(0, 0) + 365, nl_ccp_ferti_p2_sandy, false, a_farm, a_field);
		}
		else {
			if (a_farm->IsStockFarmer()) SimpleEvent_(g_date->OldDays() + m_field->GetMDates(0, 0) + 365, nl_ccp_ferti_s2_clay, false, a_farm, a_field);
			else  SimpleEvent_(g_date->OldDays() + m_field->GetMDates(0, 0) + 365, nl_ccp_ferti_p2_clay, false, a_farm, a_field);
		}
		break;
	case nl_ccp_ferti_s2_clay:
			if (!a_farm->FA_Manure(a_field, 0.0, g_date->DayInYear(25, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_s2_clay, true, a_farm, a_field);
				break;
			}
			d1 = g_date->Date() + 40;
			if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
				d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
			}
			SimpleEvent_(d1, nl_ccp_winter_plough_clay, false, a_farm, a_field);
			break;
	case nl_ccp_ferti_p2_clay:
		if (!a_farm->FP_Manure(a_field, 0.0, g_date->DayInYear(25, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_p2_clay, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 40;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		SimpleEvent_(d1, nl_ccp_winter_plough_clay, false, a_farm, a_field);
		break;
	case nl_ccp_winter_plough_clay:
		if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(15, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_winter_plough_clay, true, a_farm, a_field);
			break;
		}
		// The plan is finished on clay soils (catch crop in incorporated into soil on autumn)
		// Calling sleep_all_day to move to the next year 
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, nl_ccp_sleep_all_day, false, a_farm, a_field);
		break;
	case nl_ccp_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_sleep_all_day, true, a_farm, a_field);
			break;
		}
		done = true;
		break;
	case nl_ccp_ferti_s2_sandy:
			if (!a_farm->FA_Manure(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_s2_sandy, true, a_farm, a_field);
				break;
			}
			// The plan is finished on sandy soils; spring plough (to incorporate catch crop into soil) will follow but it is called from the next crop 
			done = true;
			break;
	case nl_ccp_ferti_p2_sandy:
		if (!a_farm->FP_Manure(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_p2_sandy, true, a_farm, a_field);
			break;
		}
		// The plan is finished on sandy soils; spring plough (to incorporate catch crop into soil) will follow but it is called from the next crop 
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLCatchCropPea::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}
