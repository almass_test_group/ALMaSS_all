//
// OLegume_Whole.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Chris J. Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OLegume.h"

extern CfgFloat cfg_strigling_prop;

bool DE_OLegume::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;

    bool done = false;
    int d1 = 0;
    int d2 = 0;

    switch (m_ev->m_todo)
    {
    case de_ol_start:
    {
        a_field->ClearManagementActionSum();

        int noDates = 1;
        m_field->SetMDates(0, 0, g_date->DayInYear(5, 9)); // last possible day of harvest
        // Check the next crop for early start, unless it is a spring crop
        // in which case we ASSUME that no checking is necessary!!!!
        // So DO NOT implement a crop that runs over the year boundary
        if (m_ev->m_startday > g_date->DayInYear(1, 7)) // LKM: to check if spring or winter crop
        {
            if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
            {
                g_msg->Warn(WARN_BUG, "DE_OLegume::Do(): "
                    "Harvest too late for the next crop to start!!!", "");
                exit(1);
            }
        }
        if (!DE_OLEGUME_DO_CATCHCROP)
        {
            // Now fix any late finishing problems
            for (int i = 0; i < noDates; i++)
            {
                if (m_field->GetMDates(0, i) >= m_ev->m_startday)
                    m_field->SetMDates(0, i, m_ev->m_startday - 1);
                if (m_field->GetMDates(1, i) >= m_ev->m_startday)
                    m_field->SetMDates(1, i, m_ev->m_startday - 1);
            }
        }
        // Now no operations can be timed after the start of the next crop.

        int d1;
        int today = g_date->Date();
        d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
        if (today > d1)
        {
            // Yes too late - should not happen - raise an error
            g_msg->Warn(WARN_BUG, "DE_OLegume::Do(): "
                "Crop start attempt after last possible start date", "");
            exit(1);
        }
        // End single block date checking code. Please see next line
        // comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + m_first_date;;
        if (!m_ev->m_first_year) d1 += 365; // Add 365 for spring crop (not 1st yr)
        if (g_date->Date() > d1) {
            d1 = g_date->Date();
        }
        // OK, let's go. - LKM: Queue first operation - start with spring harrow possible from 1st March - 10th April 
        SimpleEvent(d1, de_ol_spring_harrow1, false);
    }
    break;
    // LKM: do spring harrow, do it before the 1st of April - if not done, try again +1 day until the 10th of April when we succeed - 100% of farmers do this
    case de_ol_spring_harrow1:
        if (!m_farm->SpringHarrow(m_field, 0.0,
            g_date->DayInYear(15, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_spring_harrow1, true);
            break;
        }
        // LKM: Queue up the next event - spring plough 
        SimpleEvent(g_date->Date() + 1, de_ol_spring_plough, false);
        break;
        // LKM: do spring plough before the 15th of April - if not done, try again +1 day until the 15th of April when we succeed- 100% of farmers do this
    case de_ol_spring_plough:
        if (!m_farm->SpringPlough(m_field, 0.0,
            g_date->DayInYear(25, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_spring_plough, true);
            break;
        }
        // LKM: Queue up the next event - K and S are added 
        SimpleEvent(g_date->Date() + 1, de_ol_ks_ferti, false);
        break;
        // LKM: add K and S before the 20th of April - if not done, try again +1 day until the 20th of April when we succeed- 100% of farmers do this
    case de_ol_ks_ferti:
        if (!m_farm->FP_SK(m_field, 0.0,
            g_date->DayInYear(30, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_ks_ferti, true);
            break;
        }
        // LKM: Queue up the next event - spring harrow just before sowing
        SimpleEvent(g_date->Date() + 1, de_ol_spring_harrow2, false);
        break;
        // LKM: spring harrow only done if difficult to sow because of heavy rain (assume 10% will do this) before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
    case de_ol_spring_harrow2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
        {
            if (!m_farm->ShallowHarrow(m_field, 0.0,
                g_date->DayInYear(30, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, de_ol_spring_harrow2, true);
                break;
            }
        }
        // LKM: Queue up the next event - spring row sow done before the 30th of April (and after 20th of March) - if not done, try again +1 day until the 30th of April when we will succeed
                SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), de_ol_spring_row_sow, false);
                break;
    case de_ol_spring_row_sow:
        if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_spring_row_sow, true);
            break;
        }
        //Queue up harrow/strigling 3-5 days after sow
            SimpleEvent(g_date->Date() + 3, de_ol_strigling, false);
            break;
 // LKM: strigling before the 5th of May - if not done, try again +3 days until the 5th of May when we succeed
    case de_ol_strigling:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(5, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_strigling, true);
            break;
        } //LKM: Queue up row cultivation 30-45 days after sow to clean field from weeds
        SimpleEvent(g_date->Date() + 30, de_ol_rowcultivation1, false);
        break;
    case de_ol_rowcultivation1:
        if (!m_farm->RowCultivation(m_field, 0.0,
            g_date->DayInYear(5, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_rowcultivation1, true);
            break;
        }
        SimpleEvent(g_date->Date() + 30, de_ol_rowcultivation2, false);
        break;
    case de_ol_rowcultivation2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
        {
            if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, de_ol_rowcultivation2, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8),
            de_ol_harvest, false);
        break;
    case de_ol_harvest:
        // LKM: harvest before the 25th of August - if not done, try again +1 days until the 10th of September when we succeed - 100% of farmers do this
        if (!m_farm->Harvest(m_field, 0.0,
            m_field->GetMDates(0, 0) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_ol_harvest, false, m_farm, m_field);
            break;
        }
        // End Main Thread
        done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "OLegume::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

  return done;
}


