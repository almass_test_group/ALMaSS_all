/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OChristmasTrees_Perm3.cpp This file contains the source for the DK_OChristmasTrees_Perm3 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 \n
*/
//
// DK_OChristmasTrees_Perm3.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OChristmasTrees_Perm3.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_OChristmasTrees_Perm3_SkScrapes("DK_CROP_OCTP3_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_OChristmasTrees_Perm3_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_OCTP3_InsecticideDay;
extern CfgInt   cfg_OCTP3_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DK_OChristmasTrees_Perm3::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKOChristmasTrees_Perm3;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_octp3_start:
	{
		DK_OCTP3_AFTER_EST = 0; //Here flags should get randomly for each field a value
		
		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(16, 8); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 8); // last possible day in this case of sowing catch crop (no harvest in this code) // ferti
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(16, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // catch crop

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(365, flexdates, int(dk_octp3_manure_6_s))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
		// OK, let's go.

		int noDates = 1;
		

		// Check the next crop for early start, unless it is a spring crop
			// in which case we ASSUME that no checking is necessary!!!!
			// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

			//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
			//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {
			//Checking the future...
			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(30, 12) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "DKChristmasTrees_Perm3:Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Here we need to allow a start before 1/7 because of management of christmas trees
				d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "DKChristmasTrees_Perm3::Do(): Crop start attempt after last possible start date", "");
					exit(1);
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				if (a_farm->IsStockFarmer()) {
					SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_octp3_manure_6_s, false);
					break;
				}
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_octp3_manure_6_p, false);
				break;
			}
		}//if
	 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
	//Each field has assign randomly a DK_CTP2_Yx flag value from 1 to 10
	//if 0, then management of year 6 (Y6)
	//if 1 or 2, then same management for those two years (Y7_8)
	//if 3 or 4, then same management for those two years (Y9_10)

		if ((DK_OCTP3_AFTER_EST + g_date->GetYearNumber()) % 5 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			if (a_farm->IsStockFarmer()) {
				SimpleEvent(d1, dk_octp3_manure_6_s, false);
				break;
			}
			else SimpleEvent(d1, dk_octp3_manure_6_p, false);
			break;
		}
		else if ((DK_OCTP3_AFTER_EST + g_date->GetYearNumber()) % 5 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			if (a_farm->IsStockFarmer()) {
				SimpleEvent(d1, dk_octp3_manure_7_8_s, false);
				break;
			}
			else SimpleEvent(d1, dk_octp3_manure_7_8_p, false);
			break;
		}
		else if ((DK_OCTP3_AFTER_EST + g_date->GetYearNumber()) % 5 == 2)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			if (a_farm->IsStockFarmer()) {
				SimpleEvent(d1, dk_octp3_manure_7_8_s, false);
				break;
			}
			else SimpleEvent(d1, dk_octp3_manure_7_8_p, false);
			break;
		}
		else 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			if (a_farm->IsStockFarmer()) {
				SimpleEvent(d1, dk_octp3_npk1_9_10_s, false);
				break;
			}
			else SimpleEvent(d1, dk_octp3_npk1_9_10_p, false);
			break;
		}
		break;
	}
	break; 

		// year 6 (after establishment) 
	case dk_octp3_manure_6_s:
		if (!m_farm->FA_Manure(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manure_6_s, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_6, false); // weeding thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_octp3_grazing_6, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_octp3_sow_catch_crop, false);
		break;

	case dk_octp3_manure_6_p:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manure_6_p, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_6, false); // weeding thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_octp3_grazing_6, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_octp3_sow_catch_crop, false);
		break;

	case dk_octp3_grazing_6:
		if (!m_farm->PigsOut(m_field, 0.0,
			g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_grazing_6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_pig_is_out_6, false);
		break;
	case dk_octp3_pig_is_out_6:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_pig_is_out_6, false);
			break;
		}
		break; // end of grazing thread
		
	case dk_octp3_manual_weeding1_6:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_cutting_6, false);
		break;

	case dk_octp3_manual_cutting_6: // no specific timing for this trimming event
		if (!m_farm->CutOrch(m_field, 0.0,
			g_date->DayInYear(16, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_cutting_6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 30, dk_octp3_manual_weeding2_6, false);
		break;
	case dk_octp3_manual_weeding2_6:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding2_6, true);
			break;
		}
		break;

		// year 7+8 (after establishment) 
	case dk_octp3_manure_7_8_s:
		if (!m_farm->FA_Manure(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manure_7_8_s, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_7_8, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_octp3_grazing_7_8, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_octp3_sow_catch_crop, false);
		break;

	case dk_octp3_manure_7_8_p:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manure_7_8_p, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_7_8, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_octp3_grazing_7_8, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_octp3_sow_catch_crop, false);
		break;

	case dk_octp3_grazing_7_8:
		if (!m_farm->PigsOut(m_field, 0.0,
			g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_grazing_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_pig_is_out_7_8, false);
		break;
	case dk_octp3_pig_is_out_7_8:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_pig_is_out_7_8, false);
			break;
		}
		break; // end of grazing thread

	case dk_octp3_manual_weeding1_7_8:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_cutting_7_8, false);
		break;

	case dk_octp3_manual_cutting_7_8: // no specific timing for this trimming event
		if (!m_farm->CutOrch(m_field, 0.0,
			g_date->DayInYear(10, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_cutting_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_cutting2_7_8, false);
		break;

	case dk_octp3_manual_cutting2_7_8: // no specific timing for this trimming event (removing faulty trees)
		if (!m_farm->CutOrch(m_field, 0.0,
			g_date->DayInYear(13, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_cutting2_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding2_7_8, false);
		break;

	case dk_octp3_manual_weeding2_7_8:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding2_7_8, true);
			break;
		}
		break;

		// year 9+10 (after establishment) 
	case dk_octp3_npk1_9_10_s:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_npk1_9_10_s, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_9_10, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_octp3_npk2_9_10_s, false);
		break;

	case dk_octp3_npk1_9_10_p:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_npk1_9_10_p, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_9_10, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_octp3_npk2_9_10_p, false);
		break;

	case dk_octp3_npk2_9_10_s:
		if (!m_farm->FA_NPK(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_npk2_9_10_s, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_octp3_calcium_9_10_s, false);
		break;

	case dk_octp3_npk2_9_10_p:
		if (!m_farm->FP_NPK(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_npk2_9_10_p, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_octp3_calcium_9_10_p, false);
		break;

	case dk_octp3_calcium_9_10_s:
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FA_Calcium(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_octp3_calcium_9_10_s, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_sow_catch_crop, false);
		break;

	case dk_octp3_calcium_9_10_p:
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_octp3_calcium_9_10_p, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_sow_catch_crop, false);
		break;

	case dk_octp3_sow_catch_crop:
		if (!m_farm->AutumnSow(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_sow_catch_crop, true);
			break;
		} 
		//end of year all years (after establishment)
		done = true;
		break;

	case dk_octp3_manual_weeding1_9_10:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding1_9_10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding2_9_10, false);
		break;

	case dk_octp3_manual_weeding2_9_10:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_octp3_manual_weeding2_9_10, true);
			break;
		}
		// end of weeding thread
		break;

		// So we are done, and somewhere else the farmer will queue up the start event of the next crop - DKOChristmasTrees_Perm4 (1+ years)
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "DK_OChristmasTrees_Perm3::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}