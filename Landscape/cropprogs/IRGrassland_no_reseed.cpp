/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>IRGrassland_no_reseed.cpp This file contains the source for the IRGrassland_no_reseed class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2022 \n
 \n
*/
//
// FI_Grassland_no_reseed.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/IRGrassland_no_reseed.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_IR_Grassland_no_reseed_SkScrapes("IR_CROP_GNRS_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_grassland_n_rs_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_GNRS_InsecticideDay;
extern CfgInt   cfg_GNRS_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool IRGrassland_no_reseed::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case ir_gnrs_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		IR_GNRS_W_SIL = false; // 32% in total do sillage
		IR_GNRS_W_SIL_1 = false; // 22% do with sillage in May
		IR_GNRS_W_SIL_2 = false; // 8% do with sillage in June (and 2% in August)
		IR_GNRS_EARLY = false; //30% graze by 1st of March
		IR_GNRS_MID = false; // 60% graze by 17th of March
		IR_GNRS_LATE = false; // 90% graze by 1st of April
		IR_GNRS_PEST_APP = false; // 8% of all do pesticide appl.
		IR_GNRS_PEST_APP_1 = false; // of the 8% above 80% do pesticide mid April
		IR_GNRS_PEST_APP_2 = false; // of the 8% above 15% do pesticide mid June

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_IR_Grassland_no_reseed_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		int noDates = 1;
		//a_field->SetMDates(0, 0, g_date->DayInYear(15, 8)); // 
		//a_field->SetMDates(1, 0, g_date->DayInYear(15, 8)); //
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "IRGrassland_no_reseed::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "IRGrassland_no_reseed::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "IRGrassland_no_reseed::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start woth the first event on the after winter time) - Date here is a suggestion (not stated in crop scheme!)
				if (a_farm->IsStockFarmer()) {
					SimpleEvent(g_date->OldDays() + g_date->DayInYear(14, 1), ir_gnrs_ferti_s1, false);
					break;
				}
				else SimpleEvent(g_date->OldDays() + g_date->DayInYear(14, 1), ir_gnrs_ferti_p1, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(14, 1);
		if (g_date->Date() >= d1) d1 += 365;
		// OK, let's go. 98% OF ALL GRASS IS NON-RESEED - 68% do grazing and no silage, 32% do grazing with silage
		// LKM: Here we queue up the first event
		//fertilizer - 100% do this in Feb-March, but if heavy covers of grass, grazing is done first (not coded for now)
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(d1, ir_gnrs_ferti_s1, false);
			break;
		}
		else SimpleEvent(d1, ir_gnrs_ferti_p1, false); 
		break;
	}
	break;
	case ir_gnrs_ferti_s1: // here the timing of ferti is moved 14 days earlier to make room for 6 weeks of no grazing after application of slurry (and grazing is done March 1st (30%), March 17th (60%), April 1st (10%))
		if (a_ev->m_lock || a_farm->DoIt_prob(.30)) {
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s1, true);
				break;
			}
			IR_GNRS_EARLY = true; // we need to remember who do early grazing
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.60/.70)) {
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(31, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s1, true);
				break;
			}
			IR_GNRS_MID = true; // we need to remember who do intermediate grazing
		}
		else {
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s1, true);
				break;
			}
			IR_GNRS_LATE = true; // we need to remember who do late grazing
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_s2, false);
		break;
	
	case ir_gnrs_ferti_p1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.30)) {
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(15, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p1, true);
				break;
			}
			IR_GNRS_EARLY = true; // we need to remember who do early grazing
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.60 / .70)) {
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(31, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p1, true);
				break;
			}
			IR_GNRS_MID = true; // we need to remember who do intermediate grazing
		}
		else {
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(15, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p1, true);
				break;
			}
			IR_GNRS_LATE = true; // we need to remember who do late grazing
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_p2, false);
		break;

	case ir_gnrs_ferti_s2:
		if (IR_GNRS_EARLY == true) {
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(22, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s2, true);
				break;
			}
		}
		else if (IR_GNRS_MID == true) {
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(8, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s2, true);
				break;
			}
		}
		else if (IR_GNRS_LATE == true){
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(22, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 35, ir_gnrs_grazing1, false);
		break;

	case ir_gnrs_ferti_p2:
		if (IR_GNRS_EARLY == true) {
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(22, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p2, true);
				break;
			}
		}
		else if (IR_GNRS_MID == true) {
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(8, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p2, true);
				break;
			}
		}
		else if (IR_GNRS_LATE == true) {
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(22, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 35, ir_gnrs_grazing1, false); // main thread
		break;

	case ir_gnrs_grazing1:
		if (!a_farm->CattleOut(a_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_grazing1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out1, false);
		break;
		
	case ir_gnrs_cattle_out1:    // Keep the cattle out there
					   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear(), g_date->DayInYear(1, 4))) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out1, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), ir_gnrs_herbicide1, false);
		break;

	case ir_gnrs_herbicide1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.08)) {
			if (a_ev->m_lock || a_farm->DoIt_prob(.80)) { // 80% of the 8%
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ir_gnrs_herbicide1, true);
					break;
				}
				IR_GNRS_PEST_APP_1 = true; // we need to remember who did the pesticide applications now
			}
			IR_GNRS_PEST_APP = true; // we need to remember who do pesticide applications (8% of all)
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 5), ir_gnrs_cutting1, false); // main thread
		break;

	case ir_gnrs_cutting1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.32)) { // 32% of all (22% for this time, 8% later, 2% later again)
			if (a_ev->m_lock || a_farm->DoIt_prob(.22 / .32)) {
				if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ir_gnrs_cutting1, true);
					break;
				}
				IR_GNRS_W_SIL_1 = true; // we need to remember who did the silage now
			}
			IR_GNRS_W_SIL = true; // we need to remember who do silage in general
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), ir_gnrs_ferti_s3, false);
			break;
		}
		else
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), ir_gnrs_ferti_p3, false);
			break;

	case ir_gnrs_ferti_s3:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(23, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s3, true);
			break;
		}
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) {
			SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_s4_c, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_s4_s, false);
		break;

	case ir_gnrs_ferti_p3:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(23, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p3, true);
			break;
		}
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) {
			SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_p4_c, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_p4_s, false);
		break;

	case ir_gnrs_ferti_s4_c:
		if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s4_c, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), ir_gnrs_herbicide2, false);
		break;

	case ir_gnrs_ferti_p4_c:
		if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p4_c, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), ir_gnrs_herbicide2, false);
		break;

	case ir_gnrs_ferti_s4_s:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s4_s, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), ir_gnrs_herbicide2, false);
		break;

	case ir_gnrs_ferti_p4_s:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p4_s, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), ir_gnrs_herbicide2, false);
		break;

	case ir_gnrs_herbicide2:
		if ((IR_GNRS_PEST_APP == true) && (IR_GNRS_PEST_APP_1 == false)){
			if (a_ev->m_lock || a_farm->DoIt_prob(.15 / .20)) { // 15% of the 20% left (of the 8% that do weed control)
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ir_gnrs_herbicide2, true);
					break;
				}
				IR_GNRS_PEST_APP_2 = true; // we need to remember who did the pesticide applications now - 15%
			}	
		}
		if ((IR_GNRS_W_SIL == true) && (IR_GNRS_W_SIL_1 == false)) {
			if (a_ev->m_lock || a_farm->DoIt_prob(.08 / .10)) {
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(21, 6), ir_gnrs_cutting2, false);
				break;
			}
			else IR_GNRS_W_SIL_2 = true; // we need to remember who did the silage later (2%)
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing2, false);
		break;

	case ir_gnrs_cutting2:
		if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(13, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_cutting2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, ir_gnrs_grazing2, false);
		break;

	case ir_gnrs_grazing2:
		if (!a_farm->CattleOut(a_field, 0.0, g_date->DayInYear(28, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_grazing2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out2, false);
		break;

	case ir_gnrs_cattle_out2:    // Keep the cattle out there
					   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(29, 7) - g_date->DayInYear(), g_date->DayInYear(29, 7))) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out2, false);
			break;
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date()+1, ir_gnrs_ferti_s5, false);
			break;
		}
		else
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p5, false);
		break;
		
	case ir_gnrs_ferti_s5:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s5, true);
			break;
		}
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) {
			SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_s6_c, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_s6_s, false);
		break;

	case ir_gnrs_ferti_p5:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p5, true);
			break;
		}
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) {
			SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_p6_c, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_p6_s, false);
		break;

	case ir_gnrs_ferti_s6_c:
		if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(8, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s6_c, true);
			break;
		}
		if (IR_GNRS_W_SIL_2 == true) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), ir_gnrs_cutting3, false);
			break;
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing3, false); // main thread
		break;

	case ir_gnrs_ferti_p6_c:
		if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(8, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p6_c, true);
			break;
		}
		if (IR_GNRS_W_SIL_2 == true) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), ir_gnrs_cutting3, false);
			break;
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing3, false); // main thread
		break;

	case ir_gnrs_ferti_s6_s:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(8, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s6_s, true);
			break;
		}
		if 	(IR_GNRS_W_SIL_2 == true) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), ir_gnrs_cutting3, false);
			break;
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing3, false); // main thread
		break;

	case ir_gnrs_ferti_p6_s:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(8, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p6_s, true);
			break;
		}
		if (IR_GNRS_W_SIL_2 == true) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), ir_gnrs_cutting3, false);
			break;
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing3, false); // main thread
		break;

	case ir_gnrs_cutting3:
		if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_cutting3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, ir_gnrs_grazing3, false);
		break;

	case ir_gnrs_grazing3:
		if (!a_farm->CattleOut(a_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_grazing3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out3, false);
		break;

	case ir_gnrs_cattle_out3:    // Keep the cattle out there 
					   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear(), g_date->DayInYear(10, 9))) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out3, false);
			break;
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s7, false);
			break;
		}
		else
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p7, false);
		break;

	case ir_gnrs_ferti_s7:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(12, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s7, true);
			break;
		}
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) {
			SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_s8_c, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_s8_s, false);
		break;

	case ir_gnrs_ferti_p7:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(12, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p7, true);
			break;
		}
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6) {
			SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_p8_c, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_ferti_p8_s, false);
		break;

	case ir_gnrs_ferti_s8_c:
		if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s8_c, true);
			break;
		}
		if ((IR_GNRS_PEST_APP == true) && (IR_GNRS_PEST_APP_1 == false) && (IR_GNRS_PEST_APP_2 == false)) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_herbicide3, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing4, false);
		break;

	case ir_gnrs_ferti_p8_c:
		if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p8_c, true);
			break;
		}
		if ((IR_GNRS_PEST_APP == true) && (IR_GNRS_PEST_APP_1 == false) && (IR_GNRS_PEST_APP_2 == false)) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_herbicide3, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing4, false);
		break;

	case ir_gnrs_ferti_s8_s:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_s8_s, true);
			break;
		}
		if ((IR_GNRS_PEST_APP == true) && (IR_GNRS_PEST_APP_1 == false) && (IR_GNRS_PEST_APP_2 == false)) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_herbicide3, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing4, false);
		break;

	case ir_gnrs_ferti_p8_s:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_ferti_p8_s, true);
			break;
		}
		if ((IR_GNRS_PEST_APP == true) && (IR_GNRS_PEST_APP_1 == false) && (IR_GNRS_PEST_APP_2 == false)) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_herbicide3, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing4, false);
		break;

	case ir_gnrs_herbicide3:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(21, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_herbicide3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 7, ir_gnrs_grazing4, false);
		break;

	case ir_gnrs_grazing4:
		if (!a_farm->CattleOut(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_grazing4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out4, false);
		break;

	case ir_gnrs_cattle_out4:    // Keep the cattle out there 
			   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear(), g_date->DayInYear(15, 11))) {
			SimpleEvent(g_date->Date() + 1, ir_gnrs_cattle_out4, false);
			break;
		}

		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "IRGrassland_no_reseed::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}