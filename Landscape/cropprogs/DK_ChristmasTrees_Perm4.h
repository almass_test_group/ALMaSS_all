/**
\file
\brief
<B>DK_ChristmasTrees_Perm4.h This file contains the headers for the DK_ChristmasTrees_Perm4 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_ChristmasTrees_Perm4.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_CHRISTMASTREES_PERM4_H
#define DK_CHRISTMASTREES_PERM4_H

#define DK_CTP4_BASE 63200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DK_CTP4_AFTER_EST	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing DK_ChristmasTrees_Perm4, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ctp4_start = 1, // Compulsory, must always be 1 (one).
	dk_ctp4_sleep_all_day = DK_CTP4_BASE,
	dk_ctp4_npk1_s11,
	dk_ctp4_npk2_s11,
	dk_ctp4_calcium_s11,
	dk_ctp4_npk1_p11,
	dk_ctp4_npk2_p11,
	dk_ctp4_calcium_p11,
	dk_ctp4_manual_weeding_11,
	dk_ctp4_herbicide1,
	dk_ctp4_herbicide2,
	dk_ctp4_herbicide3,
	dk_ctp4_sow_catch_crop_11,
	dk_ctp4_manual_cutting_11,
	dk_ctp4_harvest,
	dk_ctp4_foobar,

} DK_ChristmasTrees_Perm4ToDo;


/**
\brief
DK_ChristmasTrees_Perm4 class
\n
*/
/**
See DK_ChristmasTrees_Perm4.h::DK_ChristmasTrees_Perm4ToDo for a complete list of all possible events triggered codes by the DK_ChristmasTrees_Perm4 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_ChristmasTrees_Perm4 : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_ChristmasTrees_Perm4(TTypesOfVegetation a_tov, TTypesOfCrops a_toc) : Crop(a_tov, a_toc)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 5);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_ctp4_foobar - DK_CTP4_BASE);
		m_base_elements_no = DK_CTP4_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
		fmc_Others,	// zero element unused but must be here	
		fmc_Others,	//	dk_ctp3_start = 1, // Compulsory, must always be 1 (one).
		fmc_Others, // dk_ctp3_sleep_all_day = DK_CTP4_BASE,
		fmc_Fertilizer, //dk_ctp2_npk1_s11,
		fmc_Fertilizer, //dk_ctp2_npk2_s11,
		fmc_Fertilizer, //dk_ctp2_calcium_s11,
		fmc_Fertilizer, //dk_ctp2_npk1_p11,
		fmc_Fertilizer, //dk_ctp2_npk2_p11,
		fmc_Fertilizer, //dk_ctp2_calcium_p11,
		fmc_Cultivation, //dk_ctp2_manual_weeding_11,
		fmc_Herbicide, //dk_ctp4_herbicide1,
		fmc_Herbicide, //dk_ctp4_herbicide2,
		fmc_Herbicide, //dk_ctp4_herbicide3,
		fmc_Others, //dk_ctp2_sow_catch_crop_11,
		fmc_Cutting, //dk_ctp2_manual_cutting_11,
		fmc_Harvest, //dk_ctp2_harvest,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_ChristmasTrees_Perm4_H