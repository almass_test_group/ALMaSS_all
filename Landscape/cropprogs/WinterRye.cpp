//
// WinterRye.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterRye.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool WinterRye::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case wry_start:
    {
		WRY_DECIDE_TO_HERB=1;
        a_field->ClearManagementActionSum();

		// Set up the date management stuff
      m_last_date=g_date->DayInYear(1,9);
      // Start and stop dates for all events after harvest
      int noDates=4;
      m_field->SetMDates(0,0,g_date->DayInYear(15,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(15,8));
      m_field->SetMDates(0,1,g_date->DayInYear(10,8)); // Subbleharrow start
      m_field->SetMDates(1,1,g_date->DayInYear(1,9)); // Stubbleharrow end
      m_field->SetMDates(0,2,g_date->DayInYear(3,8)); // alternative to 1st
      m_field->SetMDates(1,2,g_date->DayInYear(25,8));
      m_field->SetMDates(0,3,g_date->DayInYear(3,8)); // follows last one
      m_field->SetMDates(1,3,g_date->DayInYear(25,8));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	int d1;
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "WinterRye::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
    }
      // Now no operations can be timed after the start of the next crop.

      // CJT note:
      // Start single block date checking code to be cut-'n-pasted...

      if ( ! m_ev->m_first_year )
      {
		// Are we before July 1st?
		d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
		if (g_date->Date() < d1)
		{
		  // Yes, too early. We assumme this is because the last crop was late
			  g_msg->Warn( WARN_BUG, "WinterRye::Do(): "
			 "Crop start attempt between 1st Jan & 1st July", "" );
			  exit( 1 );
		}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "WinterRye::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
             wry_harvest, false );
        break;
      }
	}//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      // OK, let's go.

      if ( m_farm->IsStockFarmer())
      {
			d1 = g_date->OldDays() + g_date->DayInYear( 10,9 );
      }
      else
      {
			d1 = g_date->OldDays() + g_date->DayInYear( 15,9 );
      }
      if ( g_date->Date() > d1 ) // This allows a later start up to 11,10
      {
			d1 = g_date->Date();
      }

      if ( m_farm->IsStockFarmer()) {
	WRY_DID_MANURE = false;
	WRY_DID_SLUDGE = false;
	SimpleEvent( d1, wry_fertmanure_stock, false );
	SimpleEvent( d1, wry_fertsludge_stock, false );
      } else {
	SimpleEvent( d1, wry_autumn_plough, false );
      }
    }
    break;

  case wry_fertmanure_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 11, 10 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_fertmanure_stock, true );
	break;
      }
    }
    WRY_DID_MANURE = true;
    if ( WRY_DID_SLUDGE ) {
      // We are the last thread.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,9 ),
		 wry_autumn_plough, false );
    }
    break;

  case wry_fertsludge_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if (!m_farm->FA_Sludge( m_field, 0.0,
			      g_date->DayInYear( 10, 10 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_fertsludge_stock, true );
	break;
      }
    }
    WRY_DID_SLUDGE = true;
    if ( WRY_DID_MANURE ) {
      // No, *we* are the last thread!
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,9 ),
		   wry_autumn_plough, false );
    }
    break;

  case wry_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 15, 10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wry_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), wry_autumn_harrow, false );
    break;

  case wry_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 15, 10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wry_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,10 ),
		 wry_autumn_sow, false );
    break;

  case wry_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 1, 11 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wry_autumn_sow, true );
      break;
    }
    SimpleEvent( g_date->Date(), wry_autumn_roll, false );
    break;

  case wry_autumn_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if (!m_farm->AutumnRoll( m_field, 0.0,
			       g_date->DayInYear( 1, 11 ) -
			       g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_autumn_roll, true );
	break;
      }
    }

    if ( m_farm->IsStockFarmer()) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   wry_fertslurry_stock, false );
    } else {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 2,10 ),
		   wry_fertmanganese_plant_one, false );
    }
    break;

  case wry_fertmanganese_plant_one:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
					 g_date->DayInYear( 30, 10 ) -
					 g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_fertmanganese_plant_one, true );
	break;
      }
      // Did first application, then queue up second for next year.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   wry_fertmanganese_plant_two, false );
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,3 ) + 365,
		 wry_fertnpk_plant, false );
    break;

  case wry_fertmanganese_plant_two:
    if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
				       g_date->DayInYear( 5, 5 ) -
				       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wry_fertmanganese_plant_two, true );
      break;
    }
    // End of thread.
    break;

  case wry_fertnpk_plant:
    if (!m_farm->FP_NPK( m_field, 0.0,
			 g_date->DayInYear( 30, 4 ) -
			 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wry_fertnpk_plant, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
		 wry_spring_roll, false );
    break;

  case wry_fertslurry_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 65 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 30, 4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_fertslurry_stock, true );
	break;
      }
    }
    SimpleEvent( g_date->Date(), wry_fert_ammonium_stock, false );
    break;

  case wry_fert_ammonium_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
      if (!m_farm->FA_AmmoniumSulphate( m_field, 0.0,
			      g_date->DayInYear( 30, 4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_fert_ammonium_stock, true );
	break;
      }
    }
    SimpleEvent( g_date->Date(), wry_spring_roll, false );
    break;

  case wry_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->SpringRoll( m_field, 0.0,
			       g_date->DayInYear( 25,4 ) -
			       g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_spring_roll, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
                 wry_strigling, false );
    break;

  case wry_strigling:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 1,5 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_strigling, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,4 ),
                 wry_growth_reg_one, false );
    break;

  case wry_growth_reg_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 60*cfg_greg_app_prop.value() ))) {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				   g_date->DayInYear( 2,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wry_growth_reg_one, true );
	break;
      }
      // Did first application of growth regulator, so
      // queue up the second one too.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 11,5 ),
		   wry_growth_reg_two, false );
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 25,4 );
      if ( d1 <= g_date->Date()) {
	d1 = g_date->Date() + 1;
      }
      SimpleEvent( d1, wry_herbicide, false );
    }
    break;

  case wry_growth_reg_two:
    if (!m_farm->GrowthRegulator( m_field, 0.0,
				  g_date->DayInYear( 25,5 ) -
				  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wry_growth_reg_two, true );
      break;
    }
    // End of thread.
    break;

  case wry_herbicide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 80*cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier()))) { //modified probability
      
			if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, wry_herbicide, true );
				break;
		}
    }
	SimpleEvent( g_date->OldDays() + g_date->DayInYear() + 14, //should we change this date?
		 wry_herbicide_two, false );
	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		 wry_fungicide, false );
    break;

  case wry_herbicide_two:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 69*cfg_herbi_app_prop.value() * WRY_DECIDE_TO_HERB * m_farm->Prob_multiplier()))) { //modified probability
		if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wry_herbicide_two, true );
			break;
		}
    }   
    break;

  case wry_fungicide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 75*cfg_fungi_app_prop1.value()  * m_farm->Prob_multiplier()))) { //modified probability
      
			if (!m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 30,5 ) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, wry_fungicide, true );
				break;
		}
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
		 wry_insecticide, false );
    break;

  case wry_insecticide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 16*cfg_ins_app_prop1.value() * m_farm->Prob_multiplier() ))) { //modified probability
      
			if (!m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 10,6 ) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, wry_insecticide, true );
				break;
		}
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 1,6 );
      if ( d1 <= g_date->Date()) {
	d1 = g_date->Date() + 1;
      }
      SimpleEvent( d1, wry_water, false );
    }
    break;

  case wry_water:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 15,6 ) -
			  g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wry_water, true );
        break;
      }
    }
	ChooseNextCrop (4);
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                  wry_harvest, false );
    break;

  case wry_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  g_date->DayInYear( 15,8 ) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wry_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), wry_straw_chopping, false );
    break;

  case wry_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
		if (m_field->GetMConstants(0)==0) {
			if (!m_farm->StrawChopping( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterRye::Do(): failure in 'StrawChopping' execution", "" );
				exit( 1 );
			} 
		}
		else{  
		  if (!m_farm->StrawChopping( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date()+1, wry_straw_chopping, true );
			break;
		  }
		}
      // OK, did chop, so go directly to stubbles.
      SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1), wry_stubble_harrowing, false );
      break;
    }

    // No chopping, so do hay turning and bailing before stubbles.
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2), wry_hay_turning, false );
    break;

  case wry_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 40 ))
    {
		if (m_field->GetMConstants(2)==0) {
			if (!m_farm->HayTurning( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterRye::Do(): failure in 'HayTurning' execution", "" );
				exit( 1 );
			} 
		}
		else{ 
		  if (!m_farm->HayTurning( m_field, 0.0, m_field->GetMDates(1,2) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wry_hay_turning, true );
			break;
		  }
		}
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,3), wry_hay_bailing, false );
    break;

  case wry_hay_bailing:
    if (m_field->GetMConstants(3)==0) {
			if (!m_farm->HayBailing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterRye::Do(): failure in 'HayBailing' execution", "" );
				exit( 1 );
			} 
	}
	else{ 
		if (!m_farm->HayBailing( m_field, 0.0, m_field->GetMDates(1,3) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wry_hay_bailing, true );
			break;
		}
	}
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1), wry_stubble_harrowing, false );
    break;

  case wry_stubble_harrowing:
    if ( m_ev->m_lock || m_farm->DoIt( 30 ))
    {
		if (m_field->GetMConstants(1)==0) {
			if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterRye::Do(): failure in 'StubbleHarrowing' execution", "" );
				exit( 1 );
			} 
		}
		else { 
		  if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wry_stubble_harrowing, true );
			break;
		  }
		}
	}
    // End of main thread.
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "WinterRye::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


