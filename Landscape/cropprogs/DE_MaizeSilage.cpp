/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_MaizeSilage.cpp This file contains the source for the DE_MaizeSilage class</B> \n
*/
/**
\file
 by Chris J. Topping and Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_MaizeSilage.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_MaizeSilage.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_maize_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_M_InsecticideDay;
extern CfgInt   cfg_M_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_MaizeSilage::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
		m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
		m_field = a_field;
		m_ev = a_ev;
		bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
		bool flag = false;
		int d1 = 0;
		int noDates = 1;
		TTypesOfVegetation l_tov = tov_DEMaizeSilage; // The current type - change to match the crop you have
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_ms_start:
	{
		// de_ww_start just sets up all the starting conditions and reference dates that are needed to start a de_ww
		DE_MS_START_FERTI = false;
		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// Below is an example (you need to adjust it) of 2 start and stop dates for all 'movable' events for this crop; just need to adjust that
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(30, 9)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(5, 10)); // last possible day of straw chopping

		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "DE_MaizeSilage::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "DE_MaizeSilage::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "DE_MaizeSilage::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), de_ms_spring_sow, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (m_field->GetSoilType() == 2) { // on sandy soils 
			SimpleEvent_(d1, de_ms_stubble_harrow1, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_ms_stubble_harrow2, false, m_farm, m_field);
	}
	break;

	// This is the first real farm operation
	case de_ms_stubble_harrow1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_stubble_harrow1, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		SimpleEvent_(d1, de_ms_winter_plough1, false, m_farm, m_field);
		break;
	case de_ms_winter_plough1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_winter_plough1, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_p1, false, m_farm, m_field);
		break;
	case de_ms_stubble_harrow2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
		{
			if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_stubble_harrow2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 11)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 11);
		}
		SimpleEvent_(d1, de_ms_winter_plough2, false, m_farm, m_field);
		break;
	case de_ms_winter_plough2:
		if (m_ev->m_lock || m_farm->DoIt_prob(100))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_winter_plough2, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_p1, false, m_farm, m_field);
		break;
	case de_ms_ferti_p1:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_ferti_p1, true, m_farm, m_field);
			break;
		}
		if (m_field->GetSoilType() == 2) { // on sandy soils
			SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough2, false, m_farm, m_field);
		break;
	case de_ms_ferti_s1:
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_ferti_s1, true, m_farm, m_field);
			break;
		}
		if (m_field->GetSoilType() == 2) { // on sandy soils 
			SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough2, false, m_farm, m_field);
		break;
	case de_ms_spring_plough1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
		{
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_ms_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_ms_spring_plough2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0))
		{
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_ms_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_ms_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_ms_spring_sow_with_ferti, false, m_farm, m_field);
		break;
	case de_ms_spring_sow_with_ferti:
		// 75% will do sow with firtilizer 
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_spring_sow_with_ferti, true, m_farm, m_field);
				break;
			}
			else
			{
				// 75% of farmers will do this, but the other 25% won't so we need to remember whether we are in one or the other group
				DE_MS_START_FERTI = true;
				// Here is a fork leading to parallel events
				SimpleEvent_(g_date->Date() + 3, de_ms_harrow, false, m_farm, m_field);
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_ms_herbicide1, false, m_farm, m_field); // Herbidide thread
				if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_ms_ferti_s2, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_ms_ferti_p2, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ms_spring_sow, false, m_farm, m_field);
		break;
	case de_ms_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to parallel events
		SimpleEvent_(g_date->Date() + 3, de_ms_harrow, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_ms_herbicide1, false, m_farm, m_field); // Herbidide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_ms_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_ms_ferti_p2, false, m_farm, m_field);
		break;
	case de_ms_ferti_p2:
		// Here comes N thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60) && (DE_MS_START_FERTI == 0))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), de_ms_harvest, false, m_farm, m_field);
		break;
	case de_ms_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60) && (DE_MS_START_FERTI == 0))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), de_ms_harvest, false, m_farm, m_field);
		break;
	case de_ms_herbicide1:
		// Here comes the herbicide thread
		if (m_field->GetGreenBiomass() <= 0)
		{
			SimpleEvent_(g_date->Date() + 1, de_ms_herbicide1, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_ms_herbicide1, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case de_ms_harrow:
		// Here comes the MAIN thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.15))
		{
			if (m_field->GetGreenBiomass() <= 0)
			{
				if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_ms_harrow, true, m_farm, m_field);
					break;
				}
			}
			else { SimpleEvent_(g_date->Date() + 1, de_ms_harrow, true, m_farm, m_field); }
		}
		// End of thread
		break;
	case de_ms_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_ms_straw_chopping, false, m_farm, m_field);
		break;
	case de_ms_straw_chopping:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->StrawChopping(m_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "DE_MaizeSilage::Do(): failure in 'StrawChopping' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_straw_chopping, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_MaizeSilage::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}
