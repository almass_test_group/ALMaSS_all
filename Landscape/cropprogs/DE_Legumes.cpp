/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_Legumes.cpp This file contains the source for the DE_Legumes class</B> \n
*/
/**
\file
 by Chris J. Topping and Luna Kondrup Marcussen, modified by Susanne Stein \n
 Version of August 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_Legumes.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_Legumes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_peas_on;
extern CfgFloat cfg_pest_product_1_amount;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional legumes.
*/
bool DE_Legumes::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DELegumes; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	switch (m_ev->m_todo)
	{
	case de_leg_start:
	{
		// de_leg_start just sets up all the starting conditions and reference dates that are needed to start a de_leg crop off

		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.


        int noDates = 1;
        m_field->SetMDates(0, 0, g_date->DayInYear(5, 9)); // last possible day of harvest
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.
		if (m_ev->m_startday > g_date->DayInYear(1, 7)) // LKM: to check if spring or winter crop
		{
			if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
			{
				g_msg->Warn(WARN_BUG, "DE_Legumes::Do(): "
					"Harvest too late for the next crop to start!!!", "");
				exit(1);
			}
		}
		if (!DE_LEG_DO_CATCHCROP)
		{
			// Now fix any late finishing problems
			for (int i = 0; i < noDates; i++)
			{
				if (m_field->GetMDates(0, i) >= m_ev->m_startday)
					m_field->SetMDates(0, i, m_ev->m_startday - 1);
				if (m_field->GetMDates(1, i) >= m_ev->m_startday)
					m_field->SetMDates(1, i, m_ev->m_startday - 1);
			}
		}
		// Now no operations can be timed after the start of the next crop.

        int d1;
        int today = g_date->Date();
        d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
        if (today > d1)
        {
            // Yes too late - should not happen - raise an error
            g_msg->Warn(WARN_BUG, "DE_Legumes::Do(): "
                "Crop start attempt after last possible start date", "");
            exit(1);
        }
        // End single block date checking code. Please see next line
        // comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + m_first_date;;
        if (!m_ev->m_first_year) d1 += 365; // Add 365 for spring crop (not 1st yr)
        if (g_date->Date() > d1) {
            d1 = g_date->Date();
        }
        // OK, let's go. - LKM: Queue first operation - start with spring harrow possible from 1st March - 10th April 
        SimpleEvent_(d1, de_leg_spring_harrow, false, m_farm, m_field);
    }
    break;
    //LKM: here begins what is in the guidelines
    SimpleEvent_(g_date->Date() + 1,
        de_leg_spring_harrow, false, m_farm, m_field);
    break;
    case de_leg_spring_harrow:
        if (m_ev->m_lock || m_farm->DoIt(100)) {
            if (!m_farm->SpringHarrow(m_field, 0.0,
                g_date->DayInYear(15, 3) -
                g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_leg_spring_harrow, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->Date() + 1, de_leg_ferti_ks, false, m_farm, m_field);
        break;
        //LKM: in guidelines manure is not always necessary depending on fertilization of previous crops, SST: but sulfur and kalium is important
    case de_leg_ferti_ks:
        if (!m_farm->FP_SK(m_field, 0.0,
            g_date->DayInYear(20, 3) -
            g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_ferti_ks, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date() + 1, de_leg_fungicide1, false, m_farm, m_field);
        break;
    case de_leg_fungicide1:
        if (!m_farm->FungicideTreat(m_field, 0.0,
            g_date->DayInYear(20, 3) -
            g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_fungicide1, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date() + 1, de_leg_spring_sow, false, m_farm, m_field);
        break;
    case de_leg_spring_sow:
        if (!m_farm->SpringSow(m_field, 0.0,
            g_date->DayInYear(31, 3) -
            g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_spring_sow, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date() + 1, de_leg_herbicide1, false, m_farm, m_field);
        break;
        //LKM: next stop herbicide1
    case de_leg_herbicide1:
        if (!m_farm->HerbicideTreat(m_field, 0.0,
            g_date->DayInYear(10, 4) -
            g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_herbicide1, true, m_farm, m_field);
            break;
        }
        //LKM: next stop is herbicide2 
        SimpleEvent_(g_date->Date() + 7, de_leg_herbicide2, false, m_farm, m_field);
        break;
    case de_leg_herbicide2:
        if (!m_farm->HerbicideTreat(m_field, 0.0,
            g_date->DayInYear(17, 4) -
            g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_herbicide2, true, m_farm, m_field);
            break;
        }
        //LKM: next stop is herbicide3 
        SimpleEvent_(g_date->Date() + 7, de_leg_herbicide3, false, m_farm, m_field);
        break;
    case de_leg_herbicide3:
        if (m_ev->m_lock || m_farm->DoIt(0.60)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(24, 4) -
                g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_leg_herbicide3, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), de_leg_insecticide1, false, m_farm, m_field);
        break;
    case de_leg_insecticide1:
        // here we check whether we are using ERA pesticide or not
        d1 = g_date->DayInYear(30, 4) - g_date->DayInYear();
        if (!cfg_pest_peas_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
        {
            flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
        }
        else {
            flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
        }
        if (!flag) {
            SimpleEvent_(g_date->Date() + 1, de_leg_insecticide1, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date() + 14, de_leg_insecticide2, false, m_farm, m_field);
        break;
    case de_leg_insecticide2:
        // here we check whether we are using ERA pesticide or not
        d1 = g_date->DayInYear(14, 5) - g_date->DayInYear();
        if (!cfg_pest_peas_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
        {
            flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
        }
        else {
            flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
        }
        if (!flag) {
            SimpleEvent_(g_date->Date() + 1, de_leg_insecticide2, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date(), de_leg_herbicide4, false, m_farm, m_field);
        break;
    case de_leg_herbicide4:
        if (m_ev->m_lock || m_farm->DoIt(0.80)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(20, 5) -
                g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_leg_herbicide4, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), de_leg_fungicide2, false, m_farm, m_field);
        break;
    case de_leg_fungicide2:
        if (!m_farm->FungicideTreat(m_field, 0.0,
            g_date->DayInYear(31, 5) -
            g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_fungicide2, true, m_farm, m_field);
            break;
        }
        // LKM: Here is a fork leading to two parallel events
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), de_leg_fungicide3, false, m_farm, m_field); // Fungicide thread
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), de_leg_insecticide3, false, m_farm, m_field); // Insecticide thread
        break;
    case de_leg_fungicide3:
        // Here comes the fungicide thread 
        if (m_ev->m_lock || m_farm->DoIt(0.30)) {
            if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear()))
            {
                SimpleEvent_(g_date->Date() + 1, de_leg_fungicide3, true, m_farm, m_field);
                break;
            }
        }
        // LKM: Queue up the next event - fungicide4 (+ 14 days after) 
        SimpleEvent_(g_date->Date() + 14, de_leg_fungicide4, false, m_farm, m_field);
        break;
    case de_leg_fungicide4:
        if (m_ev->m_lock || m_farm->DoIt(0.20)) {
            if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear()))
            {
                SimpleEvent_(g_date->Date() + 1, de_leg_fungicide4, true, m_farm, m_field);
                break;
            }
        }
        //End of thread
        break;
    case de_leg_insecticide3:
        // Here comes the insecticide thread
        if (m_ev->m_lock || m_farm->DoIt(0.30)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(30, 6) - g_date->DayInYear();
            if (!cfg_pest_peas_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent_(g_date->Date() + 1, de_leg_insecticide3, true, m_farm, m_field);
                break;
            }
        }
        // LKM: Queue up the next event - insecticide4 (+ 14 days after)
        SimpleEvent_(g_date->Date() + 14, de_leg_insecticide4, false, m_farm, m_field);
        break;
    case de_leg_insecticide4:
        if (m_ev->m_lock || m_farm->DoIt(0.20)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(14, 7) - g_date->DayInYear();
            if (!cfg_pest_peas_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent_(g_date->Date() + 1, de_leg_insecticide4, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->Date() + 1, de_leg_herbicide5, false, m_farm, m_field);
        break;
    case de_leg_herbicide5:
        // desiccation, 14 days before harvest
        if (m_ev->m_lock || m_farm->DoIt(0.10)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear()))
            {
                SimpleEvent_(g_date->Date() + 1, de_leg_herbicide5, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->Date() + 14, de_leg_harvest, false, m_farm, m_field);
        break;
    case de_leg_harvest:
        if (!m_farm->Harvest(m_field, 0.0,
            m_field->GetMDates(0, 0) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_harvest, false, m_farm, m_field);
            break;
        }
  
        // End Main Thread
        done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "DE_Legumes::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

    return done;
}