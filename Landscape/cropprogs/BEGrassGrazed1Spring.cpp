/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>BEGrassGrazed2.cpp This file contains the source for the BEGrassGrazed2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// BEGrassGrazed2.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/BEGrassGrazed1Spring.h"

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional TemporalGrassGrazed2.
*/
bool BEGrassGrazed1Spring::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case BE_gg1s_start:
	{
		// BE_gg1s_start just sets up all the starting conditions and reference dates that are needed to start a BE_pot
		BE_gg1s_FERTI_DATE = 0;
		BE_gg1s_CUT_DATE = 0;
		BE_gg1s_WATER_DATE = 0;


		// Set up the date management stuff
				// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 1 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(10, 10)); //  last possible day of last cutting
		a_field->SetMDates(1, 0, g_date->DayInYear(10, 10)); //	last possible day of NPK application

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, uBEess it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - ms_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "BEGrassGrazed1Spring::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "BEGrassGrazed1Spring::Do(): ", "Crop start attempt after last possible start date");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), BE_gg1s_preseeding_cultivator, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.

		d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		if (g_date->Date() >= d1) d1 += 365;
		// OK, let's go.
		// Here we queue up the first event
		SimpleEvent_(d1, BE_gg1s_preseeding_cultivator, false, a_farm, a_field);
	
	}
	break;

	// This is the first real farm operation
	case BE_gg1s_preseeding_cultivator:
		if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_preseeding_cultivator, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p1, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_p1:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(22, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_gg1s_spring_sow, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_s1:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(22, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_gg1s_spring_sow, false, a_farm, a_field);
		break;
	case BE_gg1s_spring_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_spring_sow, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s2, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p2, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_p2:
		if (a_farm->DoIt_prob(0.90)) {
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), BE_gg1s_cut_to_silage1, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_s2:
		if (a_farm->DoIt_prob(0.90)) {
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), BE_gg1s_cut_to_silage1, false, a_farm, a_field);
		break;
	case BE_gg1s_cut_to_silage1:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < BE_gg1s_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage1, true, a_farm, a_field);
			break;
		}
		else
		{
			if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage1, true, a_farm, a_field);
				break;
			}
			BE_gg1s_CUT_DATE = g_date->DayInYear();
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s3, false, a_farm, a_field);
			}
			else SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p3, false, a_farm, a_field);
			// Start water thread
			break;
		}
		break;
	case BE_gg1s_ferti_s3:
		if (!a_farm->FA_Slurry(a_field, 0.0, (BE_gg1s_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s3, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s4, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_p3:
		if (!a_farm->FP_Slurry(a_field, 0.0, (BE_gg1s_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p3, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p4, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_s4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s4, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, BE_gg1s_cut_to_silage2, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_p4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p4, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, BE_gg1s_cut_to_silage2, false, a_farm, a_field);
		break;
	case BE_gg1s_cut_to_silage2:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < BE_gg1s_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage2, true, a_farm, a_field);
			break;
		}
		else
		{
			if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage2, true, a_farm, a_field);
				break;
			}
			BE_gg1s_CUT_DATE = g_date->DayInYear();
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s5, false, a_farm, a_field);
			}
			else SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p5, false, a_farm, a_field);
			break;
		}
		break;
	case BE_gg1s_ferti_s5:
		if (!a_farm->FA_Slurry(a_field, 0.0, (BE_gg1s_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s5, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s6, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_p5:
		if (!a_farm->FP_Slurry(a_field, 0.0, (BE_gg1s_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p5, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p6, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_s6:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.9))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s6, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), BE_gg1s_cattle_out, false, a_farm, a_field);
		break;

	case BE_gg1s_ferti_p6:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.9))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p6, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), BE_gg1s_cattle_out, false, a_farm, a_field);
		break;

	case BE_gg1s_cut_to_silage3:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < BE_gg1s_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage3, true, a_farm, a_field);
			break;
		}
		else
		{
			if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage3, true, a_farm, a_field);
				break;
			}
			BE_gg1s_CUT_DATE = g_date->DayInYear();
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 5, BE_gg1s_ferti_s7, false, a_farm, a_field);
			}
			else SimpleEvent_(g_date->Date() + 5, BE_gg1s_ferti_p7, false, a_farm, a_field);
			break;
		}
		break;
	case BE_gg1s_ferti_s7:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.9))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s7, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, BE_gg1s_cut_to_silage4, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_p7:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.9))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p7, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, BE_gg1s_cut_to_silage4, false, a_farm, a_field);
		break;
	case BE_gg1s_cut_to_silage4:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < BE_gg1s_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage4, true, a_farm, a_field);
			break;
		}
		else
		{
			if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage4, true, a_farm, a_field);
				break;
			}
			BE_gg1s_CUT_DATE = g_date->DayInYear();
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 5, BE_gg1s_ferti_s8, false, a_farm, a_field);
			}
			else SimpleEvent_(g_date->Date() + 5, BE_gg1s_ferti_p8, false, a_farm, a_field);
			break;
		}
		break;
	case BE_gg1s_ferti_s8:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_s8, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, BE_gg1s_cut_to_silage5, false, a_farm, a_field);
		break;
	case BE_gg1s_ferti_p8:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_ferti_p8, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 5, BE_gg1s_cut_to_silage5, false, a_farm, a_field);
		break;
	case BE_gg1s_cut_to_silage5:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < BE_gg1s_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage5, true, a_farm, a_field);
			break;
		}
		else
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.40))
			{
				if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(9, 10) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, BE_gg1s_cut_to_silage5, true, a_farm, a_field);
					break;
				}
				done = true;
				break;
			}
		}
		done = true;
		break;
	case BE_gg1s_cattle_out:
		if (a_ev->m_lock || a_farm->DoIt_prob(.5)) {
			if (!a_farm->CattleOut(a_field, 0.0, g_date->DayInYear(26, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_gg1s_cattle_out, true, a_farm, a_field);
				break;
			}
			// Keep them out there
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_cattle_is_out, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 21, BE_gg1s_cut_to_silage3, false, a_farm, a_field);
		break;

	case BE_gg1s_cattle_is_out:    // Keep the cattle out there
								   // CattleIsOut() returns false if it is not time to stop grazing
		if (!a_farm->CattleIsOut(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear(), g_date->DayInYear(31, 8))) {
			SimpleEvent_(g_date->Date() + 1, BE_gg1s_cattle_is_out, false, a_farm, a_field);
			break;
		}
		done = true;
		break;
		// End of thread
	default:
		g_msg->Warn(WARN_BUG, "BEGrassGrazed1Spring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}