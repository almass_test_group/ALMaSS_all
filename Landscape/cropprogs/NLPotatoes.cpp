/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLPotatoes.cpp This file contains the source for the NLPotatoes class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLPotatoes.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLPotatoes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_potatoes_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_POT_InsecticideDay;
extern CfgInt   cfg_POT_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional potatoes.
*/
bool NLPotatoes::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case nl_pot_start:
	{
		// nl_pot_start just sets up all the starting conditions and reference dates that are needed to start a nl_pot
		NL_POT_HERBI = false;
		NL_POT_FUNGI1 = false;
		NL_POT_FUNGI2 = false;
		NL_POT_FUNGI3 = false;
		NL_POT_FUNGI4 = false;
		NL_POT_FUNGI5 = false;
		a_field->ClearManagementActionSum();


		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(30, 10)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(30, 10));


		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - ms_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "NLPotatoes::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "NLPotatoes::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "NLPotatoes::Do(): ", "Crop start attempt after last possible start date");
						int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4), nl_pot_spring_planting, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		SimpleEvent_(d1, nl_pot_stubble_harrow, false, a_farm, a_field);
	}
	break;

	// This is the first real farm operation
	case nl_pot_stubble_harrow:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_stubble_harrow, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, nl_pot_ferti_s1_sandy, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, nl_pot_winter_plough_clay, false, a_farm, a_field);
		break;

	case nl_pot_ferti_p1_sandy:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p1_sandy, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_spring_plough_sandy, false, a_farm, a_field);
		break;
	case nl_pot_ferti_s1_sandy:
		if (a_farm->IsStockFarmer())
		{
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s1_sandy, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, nl_pot_spring_plough_sandy, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p1_sandy, false, a_farm, a_field);
		break;
	case nl_pot_spring_plough_sandy:
		if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_spring_plough_sandy, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 4);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_pot_ferti_s2_sandy, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, nl_pot_ferti_p2_sandy, false, a_farm, a_field);
		break;
	case nl_pot_ferti_p2_sandy:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p2_sandy, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, a_farm, a_field);
		break;
	case nl_pot_ferti_s2_sandy:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s2_sandy, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, a_farm, a_field);
		break;
	case nl_pot_winter_plough_clay:
		if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_winter_plough_clay, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_pot_ferti_s2_clay, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_pot_ferti_p2_clay, false, a_farm, a_field);
		break;
	case nl_pot_ferti_p2_clay:
		if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p2_clay, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, a_farm, a_field);
		break;
	case nl_pot_ferti_s2_clay:
		if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s2_clay, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, a_farm, a_field);
		break;
	case nl_pot_bed_forming:
		if (!a_farm->BedForming(a_field, 0.0, g_date->DayInYear(9, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_spring_planting, false, a_farm, a_field);
		break;
	case nl_pot_spring_planting:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_spring_planting, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 14, nl_pot_hilling1, false, a_farm, a_field); // Hilling + herbicides = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), nl_pot_fungicide1, false, a_farm, a_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), nl_pot_insecticide, false, a_farm, a_field);	// Insecticide thread
		if (a_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s3_clay, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p3_clay, false, a_farm, a_field);
		if (a_farm->IsStockFarmer()) //Stock Farmer					// microelements thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s4, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p4, false, a_farm, a_field);
		break;
	case nl_pot_ferti_p3_clay:
		if (a_field->GetSoilType() != 2 && a_field->GetSoilType() != 6)// on clay soils (NL KLEI & VEEN)
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.25))
			{
				if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p3_clay, true, a_farm, a_field);
					break;
				}
			}
			// End of thread
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s3_sandy, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p3_sandy, false, a_farm, a_field);
		break;
	case nl_pot_ferti_s3_clay:
		if (a_field->GetSoilType() != 2 && a_field->GetSoilType() != 6)// on clay soils (NL KLEI & VEEN)
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.25))
			{
				if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s3_clay, true, a_farm, a_field);
					break;
				}
			}
			// End of thread
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s3_sandy, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p3_sandy, false, a_farm, a_field);
		break;
	case nl_pot_ferti_p3_sandy:
		if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p3_sandy, true, a_farm, a_field);
			break;
		}
		// End of thread
		break;
	
	case nl_pot_ferti_s3_sandy:
		if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s3_sandy, true, a_farm, a_field);
			break;
		}
		// End of thread
		break;

	case nl_pot_ferti_p4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.25))
		{
			if (!a_farm->FP_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_pot_ferti_s4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.25))
		{ 
			if (!a_farm->FA_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_pot_hilling1:
		if (!a_farm->HillingUp(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_hilling1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, nl_pot_herbicide1, false, a_farm, a_field);
		break;
	case nl_pot_herbicide1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_herbicide1, true, a_farm, a_field);
				break;
			}
			NL_POT_HERBI = true;
		}
		SimpleEvent_(g_date->Date() + 14, nl_pot_herbicide2, false, a_farm, a_field);
		break;
	case nl_pot_herbicide2:
		if (a_ev->m_lock || (a_farm->DoIt_prob(0.625) && NL_POT_HERBI == 1)) // 50% of all farmers
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_herbicide2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 9), nl_pot_dessication1, false, a_farm, a_field);
		break;
	case nl_pot_dessication1:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_dessication1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 5, nl_pot_dessication2, false, a_farm, a_field);
		break;

	case nl_pot_dessication2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.75))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_dessication2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, nl_pot_harvest, false, a_farm, a_field);
		break;
	case nl_pot_fungicide1:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide2, false, a_farm, a_field);
		break;
	case nl_pot_fungicide2:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide2, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide3, false, a_farm, a_field);
		break;
	case nl_pot_fungicide3:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide3, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide4, false, a_farm, a_field);
		break;
	case nl_pot_fungicide4:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide4, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide5, false, a_farm, a_field);
		break;
	case nl_pot_fungicide5:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(7, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide5, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide6, false, a_farm, a_field);
		break;
	case nl_pot_fungicide6:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide6, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide7, false, a_farm, a_field);
		break;
	case nl_pot_fungicide7:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide7, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide8, false, a_farm, a_field);
		break;
	case nl_pot_fungicide8:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide8, true, a_farm, a_field);
				break;
			}
			NL_POT_FUNGI1 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide9, false, a_farm, a_field);
		break;
	case nl_pot_fungicide9:
		if (a_ev->m_lock || (a_farm->DoIt_prob(1.00) && NL_POT_FUNGI1 == 1))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(7, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide9, true, a_farm, a_field);
				break;
			}
			NL_POT_FUNGI2 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide10, false, a_farm, a_field);
		break;
	case nl_pot_fungicide10:
		if (a_ev->m_lock || (a_farm->DoIt_prob(0.875) && NL_POT_FUNGI2 == 1)) // 70% of all farmers
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(14, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide10, true, a_farm, a_field);
				break;
			}
			NL_POT_FUNGI3 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide11, false, a_farm, a_field);
		break;
	case nl_pot_fungicide11:
		if (a_ev->m_lock || (a_farm->DoIt_prob(1.00) && NL_POT_FUNGI3 == 1))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(21, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide11, true, a_farm, a_field);
				break;
			}
			NL_POT_FUNGI4 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide12, false, a_farm, a_field);
		break;
	case nl_pot_fungicide12:
		if (a_ev->m_lock || (a_farm->DoIt_prob(0.857) && NL_POT_FUNGI4 == 1)) // 60% of all farmers
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide12, true, a_farm, a_field);
				break;
			}
			NL_POT_FUNGI5 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide13, false, a_farm, a_field);
		break;
	case nl_pot_fungicide13:
		if (a_ev->m_lock || (a_farm->DoIt_prob(1.00) && NL_POT_FUNGI5 == 1))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(7, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide13, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide14, false, a_farm, a_field);
		break;
	case nl_pot_fungicide14:
		if (a_ev->m_lock || (a_farm->DoIt_prob(1.00) && NL_POT_FUNGI5 == 1))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(14, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide14, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide15, false, a_farm, a_field);
		break;
	case nl_pot_fungicide15:
		if (a_ev->m_lock || (a_farm->DoIt_prob(1.00) && NL_POT_FUNGI5 == 1))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(21, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide15, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_pot_insecticide:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_potatoes_on.value() ||
				!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_pot_insecticide, true, a_farm, a_field);
					break;
				}
			}
			else {
				a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
	case nl_pot_harvest:
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_harvest, true, a_farm, a_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLPotatoes::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}