/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------
#ifndef WheatPControlH
#define WheatPControlH
//---------------------------------------------------------------------------

//
// WheatPControl.h
//

#ifndef WHEATPCONTROL_H
#define WHEATPCONTROL_H

#define WHEATPCONTROL_BASE 7500
#define WWP_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWP_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wpc_start = 1, // Compulsory, must always be 1 (one).
  wpc_sleep_all_day = WHEATPCONTROL_BASE,
  wpc_ferti_p1,
  wpc_ferti_s1,
  wpc_ferti_s2,
  wpc_autumn_plough,
  wpc_autumn_harrow,
  wpc_stubble_harrow1,
  wpc_autumn_sow,
  wpc_autumn_roll,
  wpc_ferti_p2,
  wpc_herbicide1,
  wpc_spring_roll,
  wpc_herbicide2,
  wpc_GR,
  wpc_fungicide,
  wpc_fungicide2,
  wpc_insecticide1,
  wpc_insecticide2,
  wpc_insecticide3,
  wpc_strigling1,
  wpc_strigling2,
  wpc_water1,
  wpc_water2,
  wpc_ferti_p3,
  wpc_ferti_p4,
  wpc_ferti_p5,
  wpc_ferti_s3,
  wpc_ferti_s4,
  wpc_ferti_s5,
  wpc_harvest,
  wpc_straw_chopping,
  wpc_hay_turning,
  wpc_hay_baling,
  wpc_stubble_harrow2,
  wpc_grubning,
} WWheatPControlToDo;


class WheatPControl: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
};

#endif // WHEATPCONTROL_H
#endif
 
