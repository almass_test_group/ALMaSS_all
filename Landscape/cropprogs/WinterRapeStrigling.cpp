/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterRapeStrigling.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;


bool WinterRapeStrigling::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int noDates=3;
  int d1=0;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case wrs_start:
    {
      WR_DID_RC_CLEAN=false;
      WR_DID_HERBI_ZERO=false;
      WR_INSECT_DATE=0;
      WR_SWARTH_DATE=-1;
      a_field->ClearManagementActionSum();

      // Start single block date checking code to be cut-'n-pasted...
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(15,10);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(1,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(1,8));
      m_field->SetMDates(0,1,g_date->DayInYear(25,7));
      m_field->SetMDates(1,1,g_date->DayInYear(15,8));
      m_field->SetMDates(0,2,g_date->DayInYear(1,8));
      m_field->SetMDates(1,2,g_date->DayInYear(15,10));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary
    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "WinterRapeStrigling::Do(): "
                 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
        if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                m_field->SetMDates(0,i,m_ev->m_startday-1);
        if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                 m_field->SetMDates(1,i,m_ev->m_startday-1);
      }
    }
      // Now no operations can be timed after the start of the next crop.
      // Start single block date checking code to be cut-'n-pasted...
      if ( ! m_ev->m_first_year )
      {
        // Are we before July 1st?
        d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
        if (g_date->Date() < d1)
        {
          // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "WinterRapeStrigling::Do(): "
                 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
        }
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "WinterRapeStrigling::Do(): "
                 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year so must start in spring like nothing was unusual
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,7 )
                                                          ,wrs_swarth, false );
        break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 21,8 );
      if ( g_date->Date() > d1 ) {
        d1 = g_date->Date();
      }
      // OK, let's go.
      SimpleEvent( d1, wrs_ferti_zero, false );
    }
    break;

  // NB this is only stock farmers
  case wrs_ferti_zero:
    if (( m_ev->m_lock || m_farm->DoIt( 10 ))&& (m_farm->IsStockFarmer()))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 24, 8 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, wrs_ferti_zero, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 8 ),
                 wrs_autumn_plough, false );
    break;
  case wrs_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 95 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_autumn_plough, true );
                                break;
      }
    }
    SimpleEvent( g_date->Date() + 1, wrs_autumn_harrow, false );
    break;
  case wrs_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
         g_date->DayInYear( 25, 8 ) - g_date->DayInYear()))
    {
      SimpleEvent( g_date->Date() + 1, wrs_autumn_harrow, false );
      break;
    }
    {
            long newdate1 = g_date->OldDays() + g_date->DayInYear( 10, 8 );
            long newdate2 = g_date->Date();
            if ( newdate2 > newdate1 ) newdate1 = newdate2;
            SimpleEvent( newdate1, wrs_autumn_sow, false );
    }
    break;

  case wrs_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
         g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrs_autumn_sow, false );
      break;
    }
    d1 = g_date->Date() + 10;
    if ( d1 < g_date->OldDays() + g_date->DayInYear( 20, 8 ))
                           d1 = g_date->OldDays() + g_date->DayInYear( 20, 8 );
    SimpleEvent( d1, wrs_strigling_one, false );
    break;

case wrs_strigling_one:
  if (!m_farm->Strigling( m_field, 0.0,
                       g_date->DayInYear( 20, 9 ) -
                       g_date->DayInYear())) {
    SimpleEvent( g_date->Date() + 1, wrs_strigling_one, true );
    break;
  }
  {
    d1 = g_date->Date() + 7;
    if ( d1 < g_date->OldDays() + g_date->DayInYear( 28, 8 ))
    d1 = g_date->OldDays() + g_date->DayInYear( 28, 8 );
    SimpleEvent( d1, wrs_strigling_two, false );
  }
  break;

case wrs_strigling_two:
  if (!m_farm->RowCultivation( m_field, 0.0,
                       g_date->DayInYear( 30, 9 ) -
                       g_date->DayInYear())) {
    SimpleEvent( g_date->Date() + 1, wrs_strigling_two, true );
    break;
  }
  {
    if (m_farm->IsStockFarmer())
           SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,3 )+365,
                        wrs_ferti_s1, false );
    else SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 )+365,
                        wrs_ferti_p1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 )+365,
                        wrs_insect_one, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,5 )+365,
                        wrs_fungi_one, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 )+365,
                        wrs_strigling_three, false );

    // This is the main thread, that will lead to the end (eventually)...
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,7 )+365,
                        wrs_swarth, false );
  }
  break;

  case wrs_ferti_p1:
    if (!m_farm->FP_NPKS( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrs_ferti_p1, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 4 ),
                 wrs_ferti_p2, false );
    break;

  case wrs_ferti_p2:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if ( !m_farm->FP_NPKS( m_field, 0.0,
            g_date->DayInYear( 1, 5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_ferti_p2, true );
      }
    }
    // Execution in this thread stops here. No new events are queued up.
    break;
case wrs_ferti_s1:
    if (!m_farm->FP_NPKS( m_field, 0.0,
           g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrs_ferti_s1, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
              wrs_ferti_p2, false );
    break;

  case wrs_ferti_s2:
    if ( !m_farm->FP_Slurry( m_field, 0.0,
          g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrs_ferti_s2, true );
    }
    // Execution in this thread stops here. No new events are queued up.
    break;

  case wrs_insect_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int)(60*cfg_ins_app_prop1.value()) ))
    {
      if ( !m_farm->InsecticideTreat( m_field, 0.0,
            g_date->DayInYear( 30, 4 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, wrs_insect_one, true );
        break;
      }
      else
      {
        WR_INSECT_DATE = g_date->Date();
        long newdate1 = g_date->OldDays() + g_date->DayInYear( 1, 5 );
        long newdate2 = g_date->Date() + 7;
        if ( newdate2 > newdate1 )
        {
         newdate1 = newdate2;
        }
        SimpleEvent( newdate1, wrs_insect_one_b, false );
        break;
      }
    }
    break;    // End of thread if gets here

  case wrs_insect_one_b:
    if ( g_date->Date() == WR_FUNGI_DATE )
    {
      // try again tomorrow.
            SimpleEvent( g_date->Date() + 1, wrs_insect_one_b, false );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int)(83*cfg_ins_app_prop1.value()) )) {
      if ( !m_farm->InsecticideTreat( m_field, 0.0,
            g_date->DayInYear( 30, 5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_insect_one_b, true );
        break;
      }
      else
      {
        WR_INSECT_DATE = g_date->Date();
        long newdate1 = g_date->OldDays() + g_date->DayInYear( 20, 6 );
            long newdate2 = g_date->Date() + 7;
        if ( newdate2 > newdate1 ) newdate1 = newdate2;
        SimpleEvent( newdate1, wrs_insect_one_c, false );
        break;
      }
    }
    break;    // End of thread if gets to here

  case wrs_insect_one_c:
    if ( m_ev->m_lock || m_farm->DoIt((int)(40*cfg_ins_app_prop1.value() )))
    {
      if ( g_date->Date() == WR_FUNGI_DATE )
      {
      // try again tomorrow.
            SimpleEvent( g_date->Date() + 1, wrs_insect_one_c, false );
        break;
      }
      if ( !m_farm->InsecticideTreat( m_field, 0.0,
            g_date->DayInYear( 1, 7 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_insect_one_c, true );
      }
      WR_INSECT_DATE = g_date->Date();
    }
                // END_OF_ThREAD
    break;

  case wrs_fungi_one:
   if (m_farm->DoIt((int)(100*cfg_fungi_app_prop1.value()) ))
   {
    if ( g_date->Date() == WR_INSECT_DATE )
    {
      // try again tomorrow.
            SimpleEvent( g_date->Date() + 1, wrs_fungi_one, false );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if ( !m_farm->FungicideTreat( m_field, 0.0,
            g_date->DayInYear( 1, 6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_insect_one_c, true );
      }
      WR_FUNGI_DATE = g_date->Date();
    }
                // END_OF_ThREAD
   }
   break;

  case wrs_strigling_three:
  if ( g_date->Date() == WR_INSECT_DATE )
  {
    SimpleEvent( g_date->Date() + 1, wrs_strigling_three, m_ev->m_lock );
    break;
  }
  if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
    if ( !m_farm->Strigling( m_field, 0.0,
          g_date->DayInYear( 25, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrs_strigling_three, true );
      break;
    }
    else
    {
      long newdate1 = g_date->OldDays() + g_date->DayInYear( 25, 4 );
      long newdate2 = g_date->Date() + 5;
      if ( newdate2 > newdate1 )
      newdate1 = newdate2;
      SimpleEvent( newdate1, wrs_strigling_threeb, false );
    }
  }
  break;

  case wrs_strigling_threeb:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if ( !m_farm->Strigling( m_field, 0.0,
            g_date->DayInYear( 1, 5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_strigling_threeb, true );
      }
    }
          // END_OF_ThREAD
    break;

  case wrs_swarth:
    if (m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if ( !m_farm->Swathing( m_field, 0.0,
            g_date->DayInYear( 25, 7 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_swarth, true );
        break;
      }
      else WR_SWARTH_DATE=g_date->DayInYear();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
                 wrs_harvest, false );
    break;

  case wrs_harvest:
    {
      long EndDate;
      if (WR_SWARTH_DATE!=-1)
      {
        EndDate=WR_SWARTH_DATE+14;
      }
      else EndDate=g_date->DayInYear( 1, 8 );
      if ( !m_farm->Harvest( m_field, 0.0,
          EndDate - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, wrs_harvest, false );
        break;
      }
      if ( m_farm->DoIt( 95 ))
      {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
                   wrs_cuttostraw, false );
      }
      else  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 7 ),
                   wrs_compress, false);
      break;
    }

  case wrs_cuttostraw:
    if ( !m_farm->StrawChopping( m_field, 0.0,
          g_date->DayInYear( 1, 8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrs_cuttostraw, false );
      break;
    }
    if (g_date->DayInYear()<g_date->DayInYear(15,7))
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 7 ),
                 wrs_stub_harrow, false );
    else SimpleEvent( g_date->Date() + 1,wrs_stub_harrow, false );
    break;

  case wrs_compress:
    if ( !m_farm->HayBailing( m_field, 0.0,
          g_date->DayInYear( 1, 8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrs_compress, false );
      break;
    }
    if (g_date->DayInYear()<g_date->DayInYear(15,7))
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 7 ),
                 wrs_stub_harrow, false );
    else
      SimpleEvent( g_date->Date(), wrs_stub_harrow, false );
    break;

  case wrs_stub_harrow:
    if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
      if ( !m_farm->StubbleHarrowing( m_field, 0.0,
            m_field->GetMDates(1,1) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_stub_harrow, true );
        break;
      }
    }
          SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 8 ),
                 wrs_grubbing, false );
    break;

  case wrs_grubbing:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if ( !m_farm->DeepPlough( m_field, 0.0,
            m_field->GetMDates(1,2) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrs_grubbing, true );
      }
    }
    //
    // End of execution tree for this management plan.
    //
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "WinterRapeStrigling::Do(): "
                 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}

