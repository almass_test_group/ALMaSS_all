//
// OTriticale.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OTriticale.h"

/** @todo OTriticale is created from Triticale by removing all inorganic inputs. No other changes are made except to force strigling - this needs to be checked against reality */

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool OTriticale::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1=0;
  int noDates=2;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case otri_start:
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(15,9);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(5,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,8));
      m_field->SetMDates(0,1,g_date->DayInYear(20,8));
      m_field->SetMDates(1,1,g_date->DayInYear(15,9));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){
    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "OTriticale::Do(): "	 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
    }
      // Now no operations can be timed after the start of the next crop.
      // Start single block date checking code to be cut-'n-pasted...
      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "OTriticale::Do(): " "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "OTriticale::Do(): "	 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year so must start in spring like nothing was unusual
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 )
                                                     ,otri_harvest, false );
        break;
      }
	}//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 20,8 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }

      // OK, let's go.
    OTRI_WATER_DATE = 0;

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,8 ),
		 otri_fa_manure, false );
    break;

  case otri_fa_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 40 )) {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 5,10 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, otri_fa_manure, true );
	break;
      }
    }
    {
      d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1,9 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 1,9 );
      SimpleEvent( d1, otri_autumn_plough, false );
    }
    break;

  case otri_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 15,10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, otri_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date() + 1, otri_autumn_harrow, false );
    break;

  case otri_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 15,10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, otri_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->Date(), otri_autumn_sow, false );
    break;

  case otri_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 15,10 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, otri_autumn_sow, true );
      break;
    }
    SimpleEvent( g_date->Date(), otri_autumn_roll, false );
    break;

  case otri_autumn_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) 
	{
      if (!m_farm->AutumnRoll( m_field, 0.0, g_date->DayInYear( 10,10 ) - g_date->DayInYear())) 
	  {
		  SimpleEvent( g_date->Date() + 1, otri_autumn_roll, true );
		  break;
	  }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3) + 365, otri_spring_roll, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365, otri_fa_npk, false );
	if (m_farm->IsStockFarmer()) SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365, otri_fa_slurry, false ); // StockFarmer
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365, otri_strigling, false );
    break;

  case otri_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 15 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
			       g_date->DayInYear( 10,4 ) -
			       g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, otri_spring_roll, true );
        break;
      }
    }
    break;

  case otri_fa_npk:
    if (!m_farm->FA_NPK( m_field, 0.0, g_date->DayInYear( 30, 4 ) -	 g_date->DayInYear())) 
	{
      SimpleEvent( g_date->Date() + 1, otri_fa_npk, true );
      break;
    }
    break;

  case otri_fa_slurry:
    if (!m_farm->FA_Slurry( m_field, 0.0, g_date->DayInYear( 250,4 ) - g_date->DayInYear())) 
	{
        SimpleEvent( g_date->Date() + 1, otri_fa_slurry, true );
        break;
    }
    break;

  case otri_strigling:
    if (!m_farm->Strigling( m_field, 0.0, g_date->DayInYear( 30,4 ) - g_date->DayInYear())) 
	{
		SimpleEvent( g_date->Date() + 1, otri_strigling, true );
		break;
	}
	ChooseNextCrop (2);
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ), otri_harvest, false );
    break;

  case otri_water:

	  if ( m_ev->m_lock || m_farm->DoIt( 15 ))
    {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 30,6 ) -
			  g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, otri_water, true );
        break;
      }
      OTRI_WATER_DATE = g_date->Date();
    }
    break;

  case otri_harvest:
    if (m_field->GetMConstants(0)==0) {
		if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "OTriticale::Do(): failure in 'Harvest' execution", "" );
			exit( 1 );
		} 
	}
	else {  
		if (!m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, otri_harvest, true );
			break;
		}
	}
    if ( m_farm->DoIt( 75 ))
      SimpleEvent( g_date->Date(), otri_chopping, true );
    else
      SimpleEvent( g_date->Date(), otri_hay_turning, true );
    break;

  case otri_chopping:
	if (m_field->GetMConstants(0)==0) {
		if (!m_farm->StrawChopping( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "OTriticale::Do(): failure in 'StrawChopping' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if (!m_farm->StrawChopping( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
		  SimpleEvent( g_date->Date() + 1, otri_chopping, true );
		  break;
		}
	}
    SimpleEvent( g_date->Date(), otri_stubble_harrow, false );
    break;

  case otri_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
		if (m_field->GetMConstants(0)==0) {
			if (!m_farm->HayTurning( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "OTriticale::Do(): failure in 'HayTurning' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if (!m_farm->HayTurning( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, otri_hay_turning, true );
				break;
			}
		}
    }
    SimpleEvent( g_date->Date(), otri_hay_bailing, false );
    break;

  case otri_hay_bailing:
    if (m_field->GetMConstants(0)==0) {
			if (!m_farm->HayBailing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "OTriticale::Do(): failure in 'HayBailing' execution", "" );
				exit( 1 );
			} 
	}
	else { 
		if (!m_farm->HayBailing( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, otri_hay_bailing, true );
			break;
		}
	}
    SimpleEvent( g_date->Date(), otri_stubble_harrow, false );
    break;

  case otri_stubble_harrow:
     if (m_field->GetMConstants(1)==0) {
		if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "OTriticale::Do(): failure in 'StubbleHarrowing' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
		  SimpleEvent( g_date->Date() + 1, otri_stubble_harrow, true );
		  break;
		}
	}
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OTriticale::Do(): "	 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


