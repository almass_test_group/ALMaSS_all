/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_PermanentGrassGrazed.cpp This file contains the source for the DE_PermanentGrassGrazed class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
*/
//
// DE_PermanentGrassGrazed.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_PermanentGrassGrazed.h"


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DE_PermanentGrassGrazed::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DEPermanentGrassGrazed;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_pgg_start:
	{
		// Set up the date management stuff
		m_field->ClearManagementActionSum();

		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(31, 10)); // last possible day of fertilizer application
		m_field->SetMDates(1, 0, g_date->DayInYear(31, 10)); // last possible day of fertilizer application

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "DEPermanentGrassGrazed::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "DEPermanentGrassGrazed::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "DEPermanentGrassGrazed::Do(): ", "Crop start attempt after last possible start date");
						int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), de_pgg_cutting1, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.

		d1 = g_date->OldDays() + g_date->DayInYear(1, 2);
		if (g_date->Date() >= d1) d1 += 365;
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_pgg_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, de_pgg_ferti_p1, false, a_farm, a_field);
		break;
	}
	break;

	// This is the first real farm operation
		// This happens in a non-sowing year
	case de_pgg_ferti_s1:
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pgg_ferti_s1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_pgg_cattle_out1, false, m_farm, m_field);
		break;
	case de_pgg_ferti_p1:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pgg_ferti_p1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_pgg_cattle_out1, false, m_farm, m_field);
		break;
	case de_pgg_cattle_out1:
		if (!m_farm->CattleOut(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pgg_cattle_out1, true, m_farm, m_field);
			break;
		}
		// Keep them out there
		SimpleEvent_(g_date->Date() + 1, de_pgg_cattle_is_out1, false, m_farm, m_field);
		break;
	case de_pgg_cattle_is_out1:    // Keep the cattle out there
		// CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear(), g_date->DayInYear(1, 4))) {
			SimpleEvent_(g_date->Date() + 1, de_pgg_cattle_is_out1, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_pgg_cattle_out2, false, m_farm, m_field);
		break;
	case de_pgg_cattle_out2:
		if (a_ev->m_lock || a_farm->DoIt(20)) {
			if (!m_farm->CattleOut(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pgg_cattle_out2, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, de_pgg_cattle_is_out2, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->Date() + 1, de_pgg_cutting1, false, m_farm, m_field);
		break;
	case de_pgg_cutting1:
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_pgg_cutting1, true);
			break;
		}
		SimpleEvent_(g_date->Date() + 28, de_pgg_cutting2, false, m_farm, m_field);
		break;
	case de_pgg_cutting2:
		if (!m_farm->CutToSilage(m_field, 0.0,
			g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_pgg_cutting2, true);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer					// Manure thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), de_pgg_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), de_pgg_ferti_p2, false, m_farm, m_field);
		break;
	case de_pgg_cattle_is_out2:    // Keep the cattle out there
								   // CattleIsOutLow() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear(), g_date->DayInYear(30, 9))) {
			SimpleEvent_(g_date->Date() + 1, de_pgg_cattle_is_out2, false, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer					// Manure thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), de_pgg_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), de_pgg_ferti_p2, false, m_farm, m_field);
		break;
	case de_pgg_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pgg_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;
	case de_pgg_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pgg_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DE_PermanentGrassGrazed::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}