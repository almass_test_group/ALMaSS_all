/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_BushFruit_Perm2.cpp This file contains the source for the DK_BushFruit_Perm2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of July 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_BushFruit_Perm2.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_BushFruit_Perm2.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_bushfruit_on;
extern CfgFloat cfg_pest_product_1_amount;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_BushFruit_Perm2::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DKBushFruit_Perm2;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_bfp2_start:
	{
		// dk_bfp2_start just sets up all the starting conditions and reference dates that are needed to start a dk_bfp1
		DK_BFP2_EARLY_HARVEST = false;

		m_last_date = g_date->DayInYear(31, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use - NO harvest here - this is herbicide2 instead
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - water2

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_bfp2_cover_on))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + isSpring;
		// OK, let's go.
		// LKM: Here we queue up the first event 
		SimpleEvent(d1, dk_bfp2_cover_on, false);
		break;
	}
	break;

	// LKM: This is the first real farm operation - cover bushes with fiber (suggest 50%)
	case dk_bfp2_cover_on:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->FiberCovering(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp2_cover_on, true);
				break;
			}
			else
			{
				//We need to remember who did cover
				DK_BFP2_EARLY_HARVEST = true;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp2_water1_cover, false);
			break;
		}
		// fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp2_fertilizer1_s, false); // fertilizer thread - main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp2_water1, false); // water thread
		break;
	case dk_bfp2_water1_cover:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_water1_cover, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp2_cover_off, false);
		break;
	case dk_bfp2_cover_off:
		if (!m_farm->FiberRemoval(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_cover_off, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_bfp2_fertilizer1_s, false);
		break;
	case dk_bfp2_water1:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_water1, true);
			break;
		}
		break; // end of water thread
	case dk_bfp2_fertilizer1_s: // only for weak varieties, suggest 50%
		if (m_farm->IsStockFarmer()) {
			if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
			{
				if (!a_farm->FA_N(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_bfp2_fertilizer1_s, true);
					break;
				}
			}
			//fork of parallel events:
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp2_row_cultivation1, false); // weeding thread - main thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp2_insecticide, false); // insecticide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_bfp2_herbicide1, false); // herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp2_fungicide1, false); // fungicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp2_water2, false); // water thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp2_fertilizer1_p, false);
		break;

	case dk_bfp2_fertilizer1_p: // only for weak varieties, suggest 50%
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FP_N(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp2_fertilizer1_p, true);
				break;
			}
		}

		//fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp2_row_cultivation1, false); // weeding thread - main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp2_insecticide, false); // insecticide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_bfp2_herbicide1, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp2_fungicide1, false); // fungicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp2_water2, false); // water thread
		break;
	case dk_bfp2_insecticide:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(31, 5) - g_date->DayInYear();
		if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
		}
		else {
			flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
		}
		if (!flag) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_insecticide, true);
			break;
		}
		break; // end of insecticide thread
	case dk_bfp2_herbicide1:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_herbicide1, true);
			break;
		}
		break; // end of herbicide thread
	case dk_bfp2_fungicide1:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_fungicide1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 20, dk_bfp2_fungicide2, false);
		break;
	case dk_bfp2_fungicide2:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_fungicide2, true);
			break;
		}
		break; // end of fungicide thread
	case dk_bfp2_water2:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_water2, true);
			break;
		}
		break; // end of water thread
	case dk_bfp2_row_cultivation1:
		if (bool(DK_BFP2_EARLY_HARVEST) == false) {
			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp2_row_cultivation1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 10, dk_bfp2_row_cultivation2, false);
			break;
		}
		else if (bool(DK_BFP2_EARLY_HARVEST) == true) {
			SimpleEvent(g_date->Date(), dk_bfp2_row_cultivation2, false);
			break;
		}
	case dk_bfp2_row_cultivation2:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_row_cultivation2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp2_row_cultivation3, false);
		break;
	case dk_bfp2_row_cultivation3:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_row_cultivation3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_bfp2_straw_cover, false); // water thread
		break;
	case dk_bfp2_straw_cover:
		if (!a_farm->StrawCovering(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_straw_cover, true);
			break;
		}

		if (DK_BFP2_EARLY_HARVEST == true) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_bfp2_harvest_early1, false);
			break;
		}
		else if (DK_BFP2_EARLY_HARVEST == false) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 6), dk_bfp2_harvest1, false);
			break;
		}

	case dk_bfp2_harvest_early1:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(11, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest_early1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest_early2, false);
		break;
	case dk_bfp2_harvest_early2:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest_early2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest_early3, false);
		break;
	case dk_bfp2_harvest_early3:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(17, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest_early3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 6), dk_bfp2_harvest1, false);
		break;
	case dk_bfp2_harvest1:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest2, false);
		break;
	case dk_bfp2_harvest2:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(24, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest3, false);
		break;
	case dk_bfp2_harvest3:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(27, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest4, false);
		break;
	case dk_bfp2_harvest4:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest5, false);
		break;
	case dk_bfp2_harvest5:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(3, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest6, false);
		break;
	case dk_bfp2_harvest6:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(6, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest7, false);
		break;
	case dk_bfp2_harvest7:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(9, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest7, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest8, false);
		break;
	case dk_bfp2_harvest8:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(12, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest9, false);
		break;
	case dk_bfp2_harvest9:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest9, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest10, false);
		break;
	case dk_bfp2_harvest10:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(18, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 3, dk_bfp2_harvest11, false);
		break;
	case dk_bfp2_harvest11:
		if (!a_farm->HarvestBushFruit(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_harvest11, true);
			break;
		}
		//fork of parallel events:
		SimpleEvent(g_date->Date(), dk_bfp2_herbicide2, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 7), dk_bfp2_fertilizer2_s, false); // fertilizer thread - main thread
		break;
	case dk_bfp2_herbicide2:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(22, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_herbicide2, true);
			break;
		}
		break; // end of herbicide thread
	case dk_bfp2_fertilizer2_s:
		if (m_farm->IsStockFarmer()) {
			if (!a_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp2_fertilizer2_s, true);
				break;
			}
			//fork of parallel events:
			SimpleEvent(g_date->Date(), dk_bfp2_herbicide3, false); // herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_bfp2_water3, false); // water thread - main thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 7), dk_bfp2_fertilizer2_p, false);
		break;

	case dk_bfp2_fertilizer2_p:
		if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_fertilizer2_p, true);
			break;
		}
		//fork of parallel events:
		SimpleEvent(g_date->Date(), dk_bfp2_herbicide3, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_bfp2_water3, false); // water thread - main thread
		break;


	case dk_bfp2_herbicide3:
		if (!a_farm->HerbicideTreat(m_field, 0.0, m_field->GetMDates(0, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_herbicide3, true);
			break;
		}
		break; // end of herbicide thread
	case dk_bfp2_water3:
		if (!a_farm->Water(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp2_water3, true);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop (DK_BushFruit_Perm2)
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DK_BushFruit_Perm2::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}