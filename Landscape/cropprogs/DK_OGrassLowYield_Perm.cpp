//
// DK_OGrassLowYield_Perm.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OGrassLowYield_Perm.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool DK_OGrassLowYield_Perm::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;

	int d1;

	bool done = false;
	TTypesOfVegetation l_tov = tov_DKOGrassLowYield_Perm;

	switch (m_ev->m_todo) {
	case dk_oglyp_start:
	{
		a_field->ClearManagementActionSum();
		DK_OGLYP_CUT_DATE = 0;
		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(1, 10); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 9); // last possible day of harvest
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(1, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(365, flexdates, int(dk_oglyp_cut_to_hay))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(15, 5);
		// OK, let's go.
		SimpleEvent(d1, dk_oglyp_cut_to_hay, false);

	}
	break;

	case dk_oglyp_cut_to_hay:
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->CutToHay(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oglyp_cut_to_hay, true);
				break;
			}
			// did cut to hay so try turning
			else
			{
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6),
					dk_oglyp_raking1, false);
				DK_OGLYP_CUT_DATE = g_date->Date();
			}
			break;
		}
		SimpleEvent(g_date->OldDays() + m_first_date, dk_oglyp_cattle_out1, false); // non-cutters graze

		break;

	case dk_oglyp_raking1:
		if (DK_OGLYP_CUT_DATE + 20 >= g_date->Date()) m_ev->m_lock = true;
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->HayTurning(m_field, 0.0,
				g_date->DayInYear(25, 6) - g_date->DayInYear()))
			{
				SimpleEvent(g_date->Date() + 1, dk_oglyp_raking1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_oglyp_raking2, false);
			break;
		}
		ChooseNextCrop(1);
		SimpleEvent(DK_OGLYP_CUT_DATE + 21, dk_oglyp_cattle_out1, false);	// 100% of these graze
		break;

	case dk_oglyp_raking2:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->HayTurning(m_field, 0.0,
				g_date->DayInYear(26, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oglyp_raking2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_oglyp_compress_straw, false);
		break;

	case dk_oglyp_compress_straw:
		if (!m_farm->HayBailing(m_field, 0.0,
			g_date->DayInYear(1, 7) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oglyp_compress_straw, true);
			break;
		}
		// Did compress straw

		ChooseNextCrop(1);

		SimpleEvent(DK_OGLYP_CUT_DATE + 21, dk_oglyp_cattle_out1, false);	// 100% of these graze
		break;

	case dk_oglyp_cattle_out1:
		ChooseNextCrop(1);

		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->CattleOut(m_field, 0.0, -1)) { 
				//raise an error
				g_msg->Warn(WARN_BUG, "DK_OGrassLowYield_Perm::Do(): failure in 'CattleOut' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->CattleOut(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oglyp_cattle_out1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + m_field->GetMConstants(0), dk_oglyp_cattle_is_out, false);
		break;

	case dk_oglyp_cattle_out2:
		if ((m_ev->m_lock || m_farm->DoIt(33)))
		{
			if (m_field->GetMConstants(0) == 0) {
				if (!m_farm->CattleOut(m_field, 0.0, -1)) { //raise an error
					g_msg->Warn(WARN_BUG, "DK_OGrassLowYield_Perm::Do(): failure in 'CattleOut' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->CattleOut(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_oglyp_cattle_out2, true);
					break;
				}
			}
			// Success
			SimpleEvent(g_date->Date() + m_field->GetMConstants(0), dk_oglyp_cattle_is_out, false);
			break;
		}
		//Don't graze - but don't end too early;
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(2, 7), dk_oglyp_wait, false);
		break;

	case dk_oglyp_wait:
		done = true;
		break;

	case dk_oglyp_cattle_is_out:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->CattleIsOut(m_field, 0.0, -1, m_field->GetMDates(1, 1))) { //raise an error
				//added 28.08 - issue a warning only if we fail on the last day that this can be done, i.e. MDate
				if (g_date->Date() == m_field->GetMDates(1, 0)){
					g_msg->Warn(WARN_BUG, "DK_OGrassLowYield_Perm::Do(): failure in 'CattleIsOut' execution", "");
					exit(1);
				}
			}
		}
		else {
			if (!m_farm->CattleIsOut(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear(), m_field->GetMDates(1, 1))) {
				SimpleEvent(g_date->Date() + 1, dk_oglyp_cattle_is_out, true);
				break;
			}
		}
		// if they come in then send them out if too early
		if (g_date->DayInYear() < g_date->DayInYear(10, 9))
		{
			SimpleEvent(g_date->Date() + 1, dk_oglyp_cattle_out2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 8),
			dk_oglyp_cut_weeds, false);
		break;

	case dk_oglyp_cut_weeds:
		if ((m_ev->m_lock || m_farm->DoIt(25)) &&
			//--FN--
			(g_date->DayInYear() < g_date->DayInYear(15, 9)))
		{
			if (!m_farm->CutWeeds(m_field, 0.0,
				m_field->GetMDates(0, 1) - g_date->DayInYear())) {
				// --FN--
				SimpleEvent(g_date->Date() + 1, dk_oglyp_cut_weeds, true);
				break;
			}
		}
		// END MAIN THREAD
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DK_OGrassLowYield::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}


