/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Hunters_all.h
Version of  10 October 2013. \n
By Chris J. Topping
*/
/**
* \brief <B>The header file for all hunter types and population manager classes</B>
*/

//---------------------------------------------------------------------------
#ifndef HuntersH
#define HuntersH
//---------------------------------------------------------------------------
#include "../../BatchALMaSS/ALMaSS_Setup.h"
using namespace std;
#include "../../Landscape/ls.h"
#include "../../BatchALMaSS/PopulationManager.h"
#include "../../GooseManagement/Goose_Base.h"
#include "../../GooseManagement/Goose_Population_Manager.h"
class Hunter;
class Hunter_Population_Manager;
class Field;

//------------------------------------------------------------------------------

/**
A list of hunter types
*/
typedef enum
{
      toh_GooseHunter=0,
      toh_foobar
} TTypeOfHunters;

/**
\brief
Hunters like other ALMaSS animals work using a state/transition concept. These are the hunter behavioural states.
*/
enum  TypeOfHunterState
{
      tohts_InitialState=0,
      tohts_Hunting,
	  tohts_OutHunting,
      tohts_Resting,
	  tohts_foobar
};

/**
\brief
Types of message a hunter leader can pass on to team members
*/
enum TypeOfHunterLeaderMessage
{
	hlm_shoot = 0,
	hlm_gohome,
	hlm_foobar
} ;

/**
\brief
Used for creation of a new hunter object
*/
class struct_Hunter
{
 public:
	 int m_ref;
	 int m_HType;
	 APoint m_home;
	 vector <int> m_huntlocrefs;
	 vector <APoint> m_huntlocs;
     unsigned m_no_hunt_locs;
     APoint m_validxy;
	 vector<Farm*> m_farms;
	 int m_huntingdayslimit;
	 int m_weekend;
	 double m_efficiency;
	 double m_goosecountchance;
	 Landscape* m_L;
	 Population_Manager_Base * m_preyPM;
};


/**
* A simple data structure for the hunter allocation algorithm vectors to work with
*/
struct FarmOccupcancyData
{
	int m_FarmRef;
	int m_no_hunters;
};

/**
* A type definition for a list of hunters as a vector
*/
typedef vector < Hunter* > HunterList;

/**
\brief
The base class for hunters encompsassing all their general behaviours
*/
/**
* The hunter class specifies general hunter behaviours that are common to all hunter types. 
**/
class Hunter : public TAnimal
{
protected:
    // Attributes
	/** \brief A reference number unique to this hunter */
	int m_myname{-1};
	/** \brief  The current hunter behavioural state */
    TypeOfHunterState m_CurrentHState{tohts_foobar};
	/** \brief  Pointer to the population manager */
    Hunter_Population_Manager * m_OurPopulationManager;
	/** \brief  Records the time spent hunting per day */
    int m_clock{-1};
	/** \brief  The numbers of game items shot todate this year */
	int m_bag{-1};
	/** \brief  The numbers of shots to-date this year */
	int m_myShots{-1};
	/** \brief  The number of shells in the magazine*/
	int m_myMagazine{-1};
	/** \brief  The numbers of days used for hunting this year */
	int m_huntingdays{-1};
	/** \brief  The date for the last hunt day */
	long int m_lasthuntday{-1};
	/** \brief  Annual self-imposed limit on number shot - unused at present*/
	int m_baglimit{-1};
	/** \brief  Annual self-imposed limit on number of days spent hunting */
	int m_huntlimit{-1};
	/** \brief  Probability of 'hitting' a game item */
	double m_efficiency{0.0};
	/** \brief  Probability of checking for geese in hunting area */
	double m_goosecountchance{0.0};
	/** \brief  Code for weekly hunting activity */
	int m_weekend{-1};
	/** \brief List of pointers to the farmers whose land the hunter hunts  */
	vector<Farm*> m_OurFarmers{nullptr};
	/** /brief The home location for the hunter */
	APoint m_Home;
	/** \brief The polygon reference number to our current hunting field (which is at a hunting location) */
	int m_huntfield_polyref{-1};
	/** /brief The number of farms he can hunt on */
	int m_NoHuntLocs{-1};
	/** \ The hunting location ref numbers */
	vector <int> m_HuntLocRefs;
	/** \ The hunting locations centroid of the farm held in m_OurFarmer */
	vector <APoint> m_HuntLocs;
	
	// Methods
	/** \brief Decide whether to go out hunting on a specific day */
	virtual TypeOfHunterState st_ShouldGoHunting( );

     /** \brief The basic hunting behaviour */
     virtual TypeOfHunterState st_OutHunting( ); // Must be overridden in descendent classes;
	/** \brief Finished hunting and waiting for the next opportunity */
	static TypeOfHunterState st_Resting( );
	/** \brief Initiation of a basic hunter here */
    virtual void Init(struct_Hunter* p_data);
	/** \brief Checks for the hunting day preference (weekday or any day) */
	bool IsTodayAPreferredHuntDay( int a_today );
	/** \brief Uses a probability test to determine whether to go hunting today */
	bool IsTodayAChosenHuntDay( int a_today );
	/** \brief If the hunter checks for game at their hunting locations then this is done here. Must be overridden */
	virtual int CheckForGame( ) {
		return false;
	}
	
public:
	/** \brief The constructor for the Hunter class */
    Hunter(struct_Hunter* p_data, Hunter_Population_Manager* p_PPM);
	/** \brief The destructor for the Hunter class */
    ~Hunter() override;
    /** \brief  Gets the annual hunting attempts count */
	int GetHuntingDays() const {
		return m_huntingdays;
	}
	/** \brief  Gets the annual hunting bag */
	int GetBag() const {
		return m_bag;
	}
	/** \brief  Gets the number of shots this season to-date */
	int GetShots() const {
		return m_myShots;
	}
	/** \brief  Sets the annual hunting bag to zero */
	virtual void ResetBag() {
		m_bag = 0;
		m_myShots = 0;
	}
	/** \brief  Sets the annual hunting attempts count to zero */
	void ResetHuntingOpportunities() {
		m_huntingdays = 0;
	}
	/** \brief  Supplies the clock time */
	int GetClock() const {
		return m_clock;
	}
	/** \brief  Sets the clock back to zero */
	void ResetClock() {
		m_clock = 0;
	}
	/** \brief  Sets the bag and hunting counters to zero */
	void ResetSeasonData() {
		ResetBag();
		ResetHuntingOpportunities();
	}
	/** \brief A debug function, but may be useful in other contexts. Returns true of currently out hunting */
	bool IsOutHunting() {
		if (m_CurrentHState == tohts_OutHunting) return true;
		return false;
	}
    /** \brief  Optimism in the morning, perhaps we should hunt? */
    void OnMorning() { m_CurrentHState = tohts_Hunting; }
	/** \brief  Provide our ref name */
	int GetRef() const { return m_myname; }
	/** \brief  Provide our home coords */
	APoint GetHome() { return m_Home; }
	/** \brief Get the polygon reference number to our current hunting field (which is at a hunting location) */
	int GetHuntField() const {
		return m_huntfield_polyref;
	}
	/** \brief  Provide our hunting location coords */
	APoint GetHuntLoc(unsigned a_ref) { return m_HuntLocs[a_ref]; }
	/** \brief  Provide our ref name */
	Farm* GetFarmerRef(unsigned a_ref) { return m_OurFarmers[a_ref]; }
	/** \brief Is it hunting season? - MUST be overridden in descendent class */
	virtual bool InSeason( int /* day */ ) {
		return false;
	}
	/** \brief Is it the end of the hunting season? - MUST be overridden in descendent class */
	virtual bool IsSeasonEnd( int /* day */ ) {
		return true;
	}
	/** \brief Returns the length of the hunting season in days - MUST be overridden in descendent class */
	virtual int GetSeasonLengthLeft( int /* day */ ) {
		return 0;
	}
	/** \brief Each hunter needs to save different kinds of data, so we use a polymorphic method for this */
	virtual void SaveMyData( ofstream* /* a_ofile */ ) {
		; // Base class does nothing
	}
	/** \brief On shoot message handler - must be overidden in descendent classes */
	virtual void OnShoot() { ; }
	/** \brief On gohome message handler - must be overidden in descendent classes */
	virtual void OnGoHome(){ ; }

};

/**
\brief
The class for goose hunters encompsassing all their specific behaviours
*/
class GooseHunter : public Hunter
{
protected:
// Attributes
	/** \brief Pointer to our game population, the geese */
	Goose_Population_Manager* m_preyPopulationManger;
	/** \brief Our list of possible hunting fields as polygon reference numbers as supplied by Landscape::SupplyPolyRef(int,int); */
	polylist* m_huntfields{nullptr};
	/** \brief Flag to show whether a hunting location has been found */
	bool m_dugin{false};
	/** \brief A special bag data structure so the hunter knows what kind of geese he shot */
	int m_goosebag[ gst_foobar ]{ -1, -1, -1 , -1, -1, -1 };
	/** \brief Bag limit for pinkfoot */
	int m_pinkfootbaglimit;
	/** \brief Bag limit for greylag */
	int m_greylagbaglimit;
	/** \brief When hunting this indicates whether the hunter is a team leader */
	bool m_leader;
// Methods
	/** \brief Initiation of a specific goose hunter here */
    virtual void Init();
	/** \brief Is it goose hunting season? - MUST be overridden in descendent class */
	bool InSeason( int day ) override;
	/** \brief Is it the end of the goose hunting season? - MUST be overridden in descendent class */
	bool IsSeasonEnd( int day ) override;
	/** \brief Returns the length of the hunting season in days - MUST be overridden in descendent class */
	int GetSeasonLengthLeft( int day ) override;
	/** \brief The basic hunting behaviour */
	TypeOfHunterState st_OutHunting (  ) override;
	/** \brief Behavior involved in deciding whether to go hunting */
	TypeOfHunterState st_ShouldGoHunting() override;
	/** \brief Locate the hunt for today */
	//bool FindHuntingLocation( void );
	/** \brief If the hunter checks for game at their hunting locations then this is done here. Must be overridden */
	int CheckForGame( ) override;
	/** \brief On shoot message handler */
	void OnShoot() override;
	/** \brief On gohome message handler */
	void OnGoHome() override;
public:
// Methods
    /** \brief  GooseHunter constructor */
	GooseHunter(struct_Hunter* p_data, Hunter_Population_Manager* p_PPM);
    /** \brief  GooseHunter destructor */
    ~GooseHunter() override;
    /** \brief  GooseHunter Step code */
	void Step() override;
    /** \brief  Message received when a goose is successully shot */
	void OnShotABird( int a_birdtype, int a_poly );
	/** \brief Each hunter needs to save different kinds of data, so we use a polymorphic method for this */
	void SaveMyData( ofstream* a_ofile ) override;
	/** \brief  Sets the annual hunting bag to zero */
	void ResetBag() override{
		Hunter::ResetBag();
		for (int & i : m_goosebag) i = 0;
	}
	/** \brief Returns the leader flag */
	bool IsLeader() const {
		return m_leader;
	}
};
class HunterConfigs{
public:
    HunterConfigs()=default;

/* Here we have the input variable for hunting seasons on our game species */
/** \brief The start of the goose open season */
    CfgInt cfg_gooseopenseasonstart{"GOOSE_OPENSEASON_START", CFG_CUSTOM, 243};
/** \brief The end of the goose open season. Legal values are 0 to 364 */
    CfgInt cfg_gooseopenseasonend{"GOOSE_OPENSEASON_END", CFG_CUSTOM, 364}; // 30
/** \brief The proportion of protected species in a flock above which there is no shooting  */
    CfgFloat cfg_huntermaxprotectedpct{"HUNTER_MAXPROTECTECTEDPCT", CFG_CUSTOM, 0.1};
/** \brief The number of shots a hunter can take per shooting event */
    CfgInt cfg_hunter_magazinecapacity{"HUNTER_MAGAZINECAPACITY", CFG_CUSTOM, 2};
/** \brief The default length of hunting */
    CfgInt cfg_huntlength{"GOOSE_HUNTER_HUNT_LENGTH", CFG_CUSTOM, 180}; // default is 3 hours
/** \brief Should we record the birds shot? */
    CfgBool cfg_Hunters_RecordBag{"HUNTERS_RECORDBAG", CFG_CUSTOM, false};
/** \brief Should we enter hunter distribution mode? */
    CfgBool cfg_Hunters_Distribute{"HUNTERS_DISTRIBUTE", CFG_CUSTOM, false};
/** \brief The rule set to use for distributing hunters */
    CfgInt cfg_Hunters_Distribute_Ruleset{"HUNTERS_DISTRIBUTE_RULESET", CFG_CUSTOM, 0};
/** \brief The rule set to use for distributing non-resident hunters */
    CfgInt cfg_Hunters_Distribute_NonResRuleset{"HUNTERS_DISTRIBUTE_NONRESRULESET", CFG_CUSTOM, 0};
/** \brief Number of hunters living out of area as a percentage of local hunters */
    CfgInt cfg_Hunters_NonresidentPct{"HUNTERS_NONRESIDENTPCT", CFG_CUSTOM, 30};
/** \brief Maximum hunter density per ha */
    CfgFloat cfg_Hunters_MaxDensity{"HUNTERS_MAXDENSITY", CFG_CUSTOM, 0.1};
/** \brief Maximum hunter density per ha - power parameter */
    CfgFloat cfg_Hunters_MaxDensityPower{"HUNTERS_MAXDENSITYPOWER", CFG_CUSTOM, 0.741}; // 1.0 here will give linear, e.g. with 0.2 will allow 1 hunter per 5ha, 0.741 with 0.3 will do the same but only allow 30 hunters in 500 ha (instead of 100).
/** \brief The probability of hunters having one hunter location */
    CfgFloat cfg_hunterlocONE{"HUNTERS_HUNTERLOCPROB_ONE", CFG_CUSTOM, 0.297};
/** \brief The cumulative probability of hunters having up to 2 hunter locations */
    CfgFloat cfg_hunterlocTWO{"HUNTERS_HUNTERLOCPROB_TWO", CFG_CUSTOM, 0.616};
/** \brief The cumulative probability of hunters having up to 3 hunter locations */
    CfgFloat cfg_hunterlocTHREE{"HUNTERS_HUNTERLOCPROB_THREE", CFG_CUSTOM, 0.806};
/** \brief The cumulative probability of hunters having up to 4 hunter locations */
    CfgFloat cfg_hunterlocFOUR{"HUNTERS_HUNTERLOCPROB_FOUR", CFG_CUSTOM, 0.854};
/** \brief Hedging bets scaler to the probability of going hunting on a particular day */
    CfgFloat cfg_hunterhuntdayprobscaler{"HUNTER_HUNTDAYPROBSCALER", CFG_CUSTOM, 2.0};
/** \brief The likelihood that geese are close enough to shoot on large fields  */
    CfgFloat cfg_largefieldgooseproximity{"HUNTER_LARGEFIELDGOOSEPROXIMITYCHANCE", CFG_CUSTOM, 0.5};
/** \brief The size of the field which will trigger the use of cfg_largefieldgooseproximity */
    CfgInt cfg_largefieldgooseproximitysizecutoff{"HUNTER_LARGEFIELDGOOSEPROXIMITYCHANCESIZECUTOFF", CFG_CUSTOM, 20000};
/** \brief The number of days rest between going hunting */
    CfgInt cfg_hunterrefractionperiod{"HUNTER_REFRACTIONPERIOD", CFG_CUSTOM, 7};
/** \brief The pinkfoot hunting bag limit */
    CfgInt cfg_hunter_pinkfootbaglimit{"HUNTER_PINKFOOTBAGLIMIT", CFG_CUSTOM, 9999};
/** \brief The greylag hunting bag limit */
    CfgInt cfg_hunter_greylagbaglimit{"HUNTER_GREYLAGBAGLIMIT", CFG_CUSTOM, 9999};

};
/**
\brief
The class to handle all predator population related matters
*/
class Hunter_Population_Manager : public Population_Manager
{
public:
    HunterConfigs HunterCfg;
	// Methods
	/** \brief Hunter population manager constructor */
	explicit Hunter_Population_Manager(Landscape* p_L);
	/** \brief Hunter population manager destructor */
	~Hunter_Population_Manager () override;
	/** \brief Create the initial hunter population and initializes any output options.*/
	void Init();
	/** \brief Distributes hunters to hunting locations (farms).*/
	void DistributeHunters();
	/** \brief Implements the rule sets to distributes hunters to hunting locations (farms).*/
	void DistributeHuntersByRules( vector<HunterInfo> *a_hunterlist, int a_no_hunters, int a_ruleset );
	/** \brief Used to implement rule sets based on rule set 10.*/
	void RuleSet10Based( int a_no_hunters, vector<int>* a_farmsizes, vector<HunterInfo>* a_hunterlist, vector<APoint>* a_roostlocs, int a_ruleset );
	/** \brief Saves the results of the hunter distribution to an output file */
	static void SaveDistributedHunters( vector<HunterInfo>* a_hunterlist, int a_no_hunters );
	/** \brief Saves the results of the hunter distribution to an output file by farm*/
	void SaveFarmHunters( vector<HunterInfo>* a_hunterlist, int a_no_hunters );
	/** \brief Creates hunter objects and assigns them to the population manager lists */
	void CreateObjects(int ob_type, TAnimal*, struct_Hunter* data,int number);
	/** \brief Hunting bag output */
	void RecordHuntingSuccess(int poly,int birds, int a_hunter);
	/** \brief Returns the hunter home location */
	APoint GetHunterHome(int a_index, int a_list);
	/** \brief Returns the hunter hunting location location */
	APoint GetHunterHuntLoc(int a_index, int a_list, unsigned a_ref);
	/** \brief Calculates the number of hunting locations based on a distribution */
	unsigned GetNoHuntLocs() const;
	/** \brief helper method to reduce code size in hunter rules - checks density rules */
	bool CheckDensity(int a_ref, vector<int> *a_illegalfarms, vector<FarmOccupcancyData>* a_FarmOccupancy);
	/** \brief Adds a hunter hunting, returns true if that hunter is the leader otherwise false*/
	bool AddHunterHunting( int a_polyref, Hunter* a_hunter );
	/** \brief A message system to rely messages from the leader hunter to others in his team */
	void HunterLeaderMessage(TypeOfHunterLeaderMessage a_signal, int a_polyref);
	/** \brief Lists of hunters at all active hunting locations (updated daily)*/
	vector<HunterList*> m_ActiveHuntingLocationsHunters;
	/** \brief  Lists of polygon reference numbers for all active hunting locations (updated daily) */
	vector<int> m_ActiveHuntingLocationsPolyrefs;
	/** \brief  Debugging check method */
	bool IsPolyrefOnActiveList( int a_polyref );
	/** \brief This returns the number of geese which are legal quarry on the polygon the day before. */
	void SetHuntingSeason();
	/** \brief Get the start of the overall hunting season*/
	int GetHuntingSeasonStart() const{ return m_HuntingSeasonStart; }
	/** \brief Get the end of the overall hunting season*/
	int GetHuntingSeasonEnd() const{ return m_HuntingSeasonEnd; }

protected:
	// Attributes
	/** \brief Used to follow the time of day in 10 minute steps */
	unsigned m_daytime{0};
	/** \brief Output file for hunting bag record*/
	ofstream* m_HuntingBagRecord{nullptr};
	/** \brief Start of the overall hunting season*/
	int m_HuntingSeasonStart;
	/** \brief End of the overall hunting season*/
	int m_HuntingSeasonEnd;
    double m_goose_MinForageOpenness;


	// Methods
	bool StepFinished() override{ return true; }
	/** \brief Does general daily tasks e.g. reset time of day, reset bag lists if start of year etc..*/
	void DoFirst() override;
	/** \brief  Available for hunter management - not used currently */
	void DoBefore()override{}
	/** \brief  Available for hunter management - not used currently */
	void DoAfter() override {}
	/** \brief  Available for hunter management - not used currently */
	void DoLast() override {}
};

//---------------------------------------------------------------------------
#endif
