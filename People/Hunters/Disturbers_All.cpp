//
// Created by andrey on 3/9/21.
//
#include <iostream>

#include <utility>
#include <vector>
#include <list>
#include "../Hunters/Disturbers_All.h"


#include "../../People/Hunters/Hunters_all.h"
#include "Disturbers_toletov.h"
#include <algorithm>




//*******************************************************************************************************************/
//                                        Base Class Disturber
//*******************************************************************************************************************/

Disturbers::Disturbers(shared_ptr<Disturber_Population_Manager> DPM, unique_ptr<struct_Disturbers> data){
    CfgFloat cfg_DisturbersBangChance("DISTURBERS_BANGCHANCE", CFG_CUSTOM, 0.001, 0, 1);
    m_x = data -> m_x;
    m_y = data -> m_y;
    m_ScalingFactor = data -> m_ScalingFactor;
    /**We will allocate space for our maps right away*/
    m_Allowed_tiles= make_unique<blitz::Array<bool,2>>(m_x,m_y);
    m_DisturbersMap= make_unique<blitz::Array<bool,2>>(m_x,m_y);
    m_myDPM = std::move(DPM);
    m_DisturbersNo = data->DisturbersNo;
    m_BangChance = cfg_DisturbersBangChance.value();
    //std::default_random_engine generator;
    //std::uniform_real_distribution<double> distribution(0.0,1.0-m_BangChance);

    m_TheLandscape = data -> L;
    m_DisturberProhibitedToles = data -> m_DisturberProhibitedToles;
    /** Populating the allowed map*/
    for (int i1=0; i1<m_x; i1++){
        for (int i2=0; i2<m_y; i2++){
            bool IsAllowed=false;
            for (int ii1 =0;ii1<m_ScalingFactor;ii1++){
                for (int ii2 =0;ii2<m_ScalingFactor;ii2++){
                    //if there is at least one allowed tile, the patch is allowed
                    TTypesOfLandscapeElement tole=m_TheLandscape->SupplyElementType(i1*m_ScalingFactor+ii1,i2*m_ScalingFactor+ii2);
                    bool temp=!(m_DisturberProhibitedToles.count(tole));
                    IsAllowed+=temp;
                }

            }
            (*m_Allowed_tiles)(i1,i2)=IsAllowed;
//            if (i1>10000&&i2>20000&&i1<15000&&i2<25000)
//                cout<<m_Tole_map(blitz::Range(10000,10010),blitz::Range(20000,20020))<<'\n';
        }
    }
    m_NumberAllowed = (int) blitz::sum(*m_Allowed_tiles);
    cout<<"DEBUG: Allowed "<<m_NumberAllowed<<" tiles"<<'\n';
    //m_BangInterval_start=distribution(generator);


}
void Disturbers::Init(){
    ResetMap();
    //*disturbers_map=blitz::where(((random_map<( ((double)m_NumberAllowed/10000)*(m_DisturbersNo)/map_size))&&*m_Allowed_tiles), true, false);
}
bool Disturbers::IsDisturbed(float seed, int X, int Y) {
    /** Coordinates are scaled already*/
  //  cout<<"DEBUG: Seed is: "<< seed<<". Interval is ["<<m_BangInterval_start<<", "<<m_BangInterval_start+m_BangChance<<"]\n";
   // cout<<"DEBUG1: Disturber is "<<(*m_DisturbersMap)(X,Y)<<'\n';
    return (seed>m_BangInterval_start)&&(seed<=(m_BangInterval_start+m_BangChance))&&(*m_DisturbersMap)(X,Y);
}
void Disturbers::ResetMap() {
    /** Reset Disturbers*/
    int map_size = m_x*m_y;
#ifdef __USE_BLITZ_RANDOM
    std::generate(m_DisturbersMap->begin(),m_DisturbersMap->end(),[&]{  return (m_myDPM->m_RNG.random()<( ((double)m_NumberAllowed/10000)*(double)(m_DisturbersNo)/map_size)) ;  } );
#else
    std::generate(m_DisturbersMap->begin(),m_DisturbersMap->end(),[&]{  return (g_rand_uni()<( ((double)m_NumberAllowed/10000)*(double)(m_DisturbersNo)/map_size)) ;  } );
#endif
    (*m_DisturbersMap)*=(*m_Allowed_tiles);
    auto sum_to_show = (int) blitz::sum(*m_DisturbersMap);
    cout<<"DEBUG: There are "<<sum_to_show<<" disturbers"<<'\n';
    cout<<"DEBUG: Disturbers affect "<<sum_to_show*m_ScalingFactor*m_ScalingFactor<<" tiles out of "<<(m_TheLandscape->SupplySimAreaHeight())*(m_TheLandscape->SupplySimAreaWidth())<<'\n';
    ResetInterval();
    //cout<<"DEBUG: The interval is: ["<<m_BangInterval_start<<", "<<m_BangInterval_start+m_BangChance<<" ]"<<'\n';



}
void Disturbers::ResetInterval() {
    /**Resetting the interval where the seed causes the Disturber to Bang*/

    //m_BangInterval_start=(m_myDPM->m_RNG.random())*(1-m_BangChance);
    m_BangInterval_start=(g_rand_uni())*(1-m_BangChance);

}




//*******************************************************************************************************************/
//                                        Class StaticGooseDisturber
//*******************************************************************************************************************/

//*******************************************************************************************************************/
//                                        Class Disturber_Population_Manager
//*******************************************************************************************************************/

Disturber_Population_Manager::Disturber_Population_Manager(int lists_num, Landscape* p_L, TTypesOfPopulation target): Population_Manager( p_L),
                                                                                                                      cfg_DefaultNoDisturbers("DISTURBERS_DEFAULT_NO", CFG_CUSTOM, 5000)
{


    m_daylight=true;
    CfgInt cfg_DisturbersRandSeed("DISTURBERS_SEED", CFG_CUSTOM, 42);
    CfgInt cfg_DisturbersScalingFactor("DISTURBERS_SCALINGFACTOR", CFG_CUSTOM, 10);
   // std::vector<int> temp_vect(lists_num, cfg_DefaultNoDisturbers.value());
    /**variable that defines the number of disturbers of different kinds per 10x10 tiles*/
   // CfgArray_Int cfg_DisturbersNo("DISTURBERS_NO", CFG_CUSTOM, lists_num, temp_vect);
    m_NumOfDisturbers=lists_num;
    m_ScalingFactor=cfg_DisturbersScalingFactor.value();
    m_y=m_TheLandscape->SupplySimAreaHeight()/m_ScalingFactor;
    m_x=m_TheLandscape->SupplySimAreaWidth()/m_ScalingFactor;
    m_Target=target;
    TheArray_new = std::make_unique< smartTListOfDistubers >(m_NumOfDisturbers);
    m_RandomSeedMap = make_unique<blitz::Array<double,2>>(m_x, m_y);
#ifdef __USE_BLITZ_RANDOM
    m_RNG.seed(cfg_DisturbersRandSeed.value()); // we will seed it with the same number as everywhere
#endif
    ResetSeedMap();
}
void Disturber_Population_Manager::ResetSeedMap() {
#ifdef __USE_BLITZ_RANDOM
    std::generate(m_RandomSeedMap->begin(),m_RandomSeedMap->end(),[&]{  return m_RNG.random() ;  } );
#else
    std::generate(m_RandomSeedMap->begin(),m_RandomSeedMap->end(),[&]{  return g_rand_uni() ;  } );
#endif
}
void Disturber_Population_Manager::Init(){
    std::vector<int> temp_vect(m_NumOfDisturbers, cfg_DefaultNoDisturbers.value());
    CfgArray_Int cfg_DisturbersNo("DISTURBERS_NO", CFG_CUSTOM, m_NumOfDisturbers, temp_vect);

    /*
    int map_y=m_TheLandscape->SupplySimAreaHeight();
    int map_x=m_TheLandscape->SupplySimAreaWidth();
     */


    for (int j=0; j<m_NumOfDisturbers; j++){


        std::unique_ptr <struct_Disturbers> aps(new struct_Disturbers);
        aps->L = m_TheLandscape;

        aps->DisturbersNo=cfg_DisturbersNo.value()[j];
        aps->m_x =m_x;
        aps->m_y =m_y;
        aps->m_ScalingFactor=m_ScalingFactor;


        CreateObjects(static_cast<TTypeOfDisturbers>(j), std::move(aps),1);


    }
}
void Disturber_Population_Manager::CreateObjects(TTypeOfDisturbers type, std::unique_ptr<struct_Disturbers> data,
                                                 int number) {



    if (number>1){
        g_msg->Warn("wrong number of objects to be created",0);
        exit(-1);
    }


    m_NumOfDisturbers = number;

        if (type==Disturber_types::StaticGooseDisturber_type0){
            data->m_DisturberProhibitedToles=DisturberProhibitedToles;

            std::unique_ptr <Disturbers> new_Disturber (new StaticGooseDisturber(shared_from_this(), std::move(data)));
            (*TheArray_new)[0] = std::move(new_Disturber) ;
            (*TheArray_new)[0]->Init();
            m_DisturbersResetDate.push_back(364);// we can add a custom date for each type for now--> in the end of the year
    }
  //  (*m_Disturbed).resize(x,y);



}
Disturber_Population_Manager::~Disturber_Population_Manager()  {
    ;
}
void Disturber_Population_Manager::Reset_Disturbers() {
    for (int i=0; i<m_NumOfDisturbers; i++){
        if ((m_DisturbersResetDate[i]==m_TheLandscape->SupplyDayInYear())&&(m_daytime==0)){
            ((*TheArray_new)[i].get())->ResetMap();
        }
    }
    // We will check for each disturber that the reset date is there and if it is we will reset
}
void Disturber_Population_Manager::DoFirst() {
    int dlt = m_TheLandscape->SupplyDaylength();
    int day = m_TheLandscape->SupplyDayInYear();

    m_daytime = ( m_daytime + 10 ) % 1440; // We have 10 minutes timesteps. This resets m_daytime to 0 when it hits 1440.
    if (m_daytime < dlt ) m_daylight = true; else m_daylight = false;
    for (int i=0; i<m_NumOfDisturbers; i++){
        /** Every step we reset the interval */
        ((*TheArray_new)[i].get())->ResetInterval();
    }
    vector<int> reset_days = DisturberCfg.cfg_disturber_reset_days.value();
    if (std::find(reset_days.begin(), reset_days.end(), day) != reset_days.end()){
        Reset_Disturbers();
        ResetSeedMap();
    }
 //   if (m_daytime==0){
 //       cout<<"DEBUG: Disturber Day "<<day<<'\n';
 //   }
}
/** The last thing to do: actual disturbing of the animals*/
void Disturber_Population_Manager::DoLast() {
        PopulationManagerList *PML=m_TheLandscape->SupplyThePopManagerList();
        Population_Manager_Base* PM=PML->GetPopulation(m_Target);
        if(m_Target==TOP_Goose&&m_NumOfDisturbers>0){
            dynamic_cast<Goose_Population_Manager*>(PM)->DisturberBangAtCoords(shared_from_this());
        }

}
/** The main Run-- calculating the disturbers*/
void Disturber_Population_Manager::Run(int NoTSteps) {
    /*
     * int day = m_TheLandscape->SupplyDayInYear();
     * */

    /** DEBUGGING */
    /*if (day>215){
        */
        DoFirst();
        DoLast();
        /*
    }
*/


}
bool Disturber_Population_Manager::IsDisturbed(int X, int Y) {
    // First let's transfer coordinates to the coordinates of the stored maps
    int new_X;
    int new_Y;
    bool result{false};
    int day;
    new_X = X/m_ScalingFactor;
    new_Y = Y/m_ScalingFactor;
    auto seed=(float)(*m_RandomSeedMap)(new_X,new_Y);
    day = m_TheLandscape->SupplyDayInYear();
    for (int i=0; i<m_NumOfDisturbers; i++){
        // check that the disturber is Active
        if(((*TheArray_new)[i].get())->IsActive(SupplyDayTime(), SupplyDayLight(), day)){
            result+=((*TheArray_new)[i].get())->IsDisturbed(seed,new_X, new_Y);
        }
    }
    return result;
}
