/*******************************************************************************************************
Copyright(c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.

Redistributionand use in sourceand binary forms, with or without modification, are permitted provided
that the following conditions are met :

Redistributions of source code must retain the above copyright notice, this list of conditionsand the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditionsand
the following disclaimer in the documentationand /or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*********************************************************************************************************/


#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../../BatchALMaSS/ALMaSS_Setup.h"
#include "../../ALMaSSDefines.h"
#include "../../Landscape/ls.h"
#include "../../BatchALMaSS/PopulationManager.h"
#include "Farmer.h"
#include "SocialNetworks.h"

/** \brief Hold the intensity of the interaction between farmers (1 MAX, 0 no interaction) */
CfgFloat  cfg_FarmerMobility("FARMER_MOBILITY", CFG_CUSTOM, 1.0);

extern CfgBool cfg_UseFarmerNetworks;
extern CfgBool cfg_UseConsumat;
extern CfgBool cfg_UseOnlySpatialNetwork;
extern CfgInt cfg_NumberNeighboursSpatial;
extern CfgInt cfg_NumberNeighboursVirtual;
extern CfgInt cfg_NumberNeighboursAssociative;

//probability_distribution* normalDistributionGenerator = new probability_distribution("NORMAL", "0.5 0.25");
// previous line commented out by Andrey Chuhutin 27/06/22. Reasons:
// - object not used.
// - one should not allocate the object on the heap with static duration: it causes memory leak (the memory is not released upon the program end). If you want the object to be created: allocate the memory inside the class function (e.g. class constructor), or at least inside some routine (function)



// Assign default static member values (these will be changed later).


Farmer::Farmer(int a_x, int a_y, Landscape* a_L, FarmerList* a_farmerlist, Farm* a_farm) :TAnimal(a_x, a_y, a_L) {
	if (cfg_UseFarmerNetworks.value())
	{
		m_farmerlist = a_farmerlist;
		m_spatialNetwork = new SocialNetworkSpatial(a_farmerlist, this);
		m_virtualNetwork = new SocialNetworkVirtual(a_farmerlist, this);
		m_associativeNetwork = new SocialNetworkAssociative(a_farmerlist, this);
		// Assign and evenly distributed value 0-1.0
		//m_riskaversion = g_rand_uni(); // 0-1.0
		// Assign normal distributed value (can be outside 0.0-1.0)
		//m_riskaversion = normalDistributionGenerator->get(); 
		//m_riskspan = g_rand_uni(); // 0-1.0
	}
	InitializeVariables();
	m_annualprofitloss.resize(1);
	m_OurFarm = a_farm;
	m_CurrentFarmerState = tfs_InitialState;
	m_consumatbehave = tcs_InitialState;
	m_MyFarmManager = a_farm->GetFarmManager();
}

Farmer::~Farmer() {
	if (cfg_UseFarmerNetworks.value())
	{
		delete m_spatialNetwork;
		delete m_virtualNetwork;
		delete m_associativeNetwork;
	}
}

void Farmer::UpdateFarmerStyle()
{
	/**
	* Uses the risk aversion and environmental attributes to calculate the farmer style.
	* This version just assumes equal weight of both attributes and adjusts the neutral value of 1 proportionally
	* e.g. riskaversion = 0.75 and envAttitude = 0.5, then the result would be 1.25 (increase probability by 25%)
	* THIS IS A DUMMY, the real way to do this should also include other attributes e.g. if you have a profit-maximiser attribute.
	* Each way of making the style needs considering carefully.
	*/
	double pstyle = (m_riskaversion - m_envAttitude) + 1.0;
	m_MyFarmingStyle.SetPesticideStyle(pstyle);
	double fstyle = (m_riskaversion - m_envAttitude) + 1.0;
	m_MyFarmingStyle.SetPesticideStyle(fstyle);

}

void Farmer::SetPersonalAttributes(FarmerAttributes Attributes)
{
	if (Attributes.Age == -1) {
		// Here we assume if we don't set it explicitly its random 20-69;
		m_Age = int(g_rand_uni() * 50) + 20;
	}
	else m_Age = Attributes.Age;
	if (Attributes.riskA == -1) {
		// Here we assume if we don't set it explicitly its random 0-1.0;
		m_riskaversion = (g_rand_uni());
	}
	else m_riskaversion = Attributes.riskA;
	if (Attributes.envA == -1) {
		// Here we assume if we don't set it explicitly its random 0-1.0;
		m_envAttitude = (g_rand_uni());
	}
	else m_envAttitude = Attributes.envA;
	if (Attributes.riskSpan == -1) {
		m_riskspan = g_rand_uni();
	}
	else m_riskspan = Attributes.riskSpan;	 // 0-1.0
	m_CollectiveRef = Attributes.Cooperative;
}

FarmerAttributes Farmer::GetPersonalAttributes() 
{
	FarmerAttributes Attributes;

	Attributes.Age = m_Age;
	Attributes.Cooperative = m_CollectiveRef;
	Attributes.envA = m_envAttitude;
	Attributes.riskA = m_riskaversion;
	Attributes.riskSpan = m_riskspan;
	return Attributes;

}

void Farmer::Step()
{
	if (m_StepDone) return;
	switch (m_CurrentFarmerState)
		{
		case tfs_InitialState: // Initial state
			m_CurrentFarmerState = tfs_OrdinaryManagement;
			if (cfg_UseFarmerNetworks.value())
			{
				m_spatialNetwork->UpdateNetwork();
			}
			break;
		case tfs_DoAnnualAccounting:
			m_CurrentFarmerState = st_DoAnnualAccounting();
			break;
		case tfs_UpdateSocialBasedAttributes:
			m_CurrentFarmerState = st_UpdateSocialBasedAttributes();
			m_StepDone = true;
			break;
		case tfs_OrdinaryManagement:
			m_CurrentFarmerState = st_OrdinaryManagement();
			break;
		case tfs_UpdateManagement:
			m_CurrentFarmerState = st_UpdateManagement();
			break;
		default:
			g_msg->Warn("Farmer::Step() Unknown state:", int(m_CurrentFarmerState));
			exit(142);
		}
}

void Farmer::EndStep()
{
	if (g_date->DayInYear() == 0) {
		SwapAttitudeTempReal();
	}
}

TypeOfFarmerState Farmer::st_OrdinaryManagement()
{
	if (g_date->DayInYear() == 364) {
		return tfs_DoAnnualAccounting;
	}
	if (g_date->DayInYear() == 0) {
		return tfs_UpdateSocialBasedAttributes;
	}
	m_StepDone = true;
	return tfs_OrdinaryManagement;
}

TypeOfFarmerState Farmer::st_UpdateSocialBasedAttributes()
{
		ChooseNeighbours();
		UpdateRiskaversion();
		//SocialNetworkSpatial::SocialNetworkSpatial(m_);
		return tfs_UpdateManagement;
}

TypeOfFarmerState Farmer::st_DoAnnualAccounting()
{
	// last day of the year so need to do some economics
	double result = m_runningprofit - m_runningloss;
	Set_annualprofitloss(result);
	m_runningloss = 0;
	m_runningprofit = 0;
	// Decide which state to go to
	return tfs_UpdateSocialBasedAttributes;
}

TypeOfFarmerState Farmer::st_UpdateManagement()
{
		if (m_useConsumat) { 
			switch (m_consumatbehave)
			{
			case tcs_InitialState:
				m_consumatbehave = tcs_Optimization;
				break;
			case tcs_Repetition:
				Repeat();
				break;
			case tcs_Imitation:
				Imitate();
				break;
			case tcs_Socialcomparison:
				SocialCompare();
				break;
			case tcs_Optimization:
				Optimize();
				break;
			default:
				g_msg->Warn("Farmer::st_UpdateManagement() Unknown state:", int(m_consumatbehave));
				exit(142);
			}
		} else Optimize();
		// Once decision are taken this may alter the farmer style
		UpdateFarmerStyle();
		return tfs_OrdinaryManagement;
}

void Farmer::InitializeVariables()
{
	m_Age = 0; // Is set by SetPersonalAttributes
	m_runningprofit = 0;
	m_runningloss = 0;
	m_runninglabourusage = 0;
	m_RiskScore = 0;
	m_consumatbehave = tcs_Optimization;
	/** Sets the farming style variables to default (1.0) to avoid issues when the agent farmer is not used */
	m_MyFarmingStyle.SetPesticideStyle(1.0);
	m_MyFarmingStyle.SetFertilizerStyle(1.0);
}

void Farmer::SetStaticParameterValues()
{
	 m_useConsumat = cfg_UseConsumat.value();
	 m_useonlyspatial = cfg_UseOnlySpatialNetwork.value();
	 m_neighbours_spatial = cfg_NumberNeighboursSpatial.value();
	 m_neighbours_virtual = cfg_NumberNeighboursAssociative.value();
	 m_neighbours_virtual = cfg_NumberNeighboursVirtual.value();

}

void Farmer::Imitate()
{
	;
}

void Farmer::SocialCompare()
{
	;
}

void Farmer::Optimize()
{
	;
}

void Farmer::Repeat()
{
	;
}

void Farmer::UpdateSpatialNetwork()
{
	m_spatialNetwork->UpdateNetwork();
}

void Farmer::UpdateAssociativeNetwork()
{
	m_associativeNetwork->UpdateNetwork();
}

void Farmer::UpdateVirtualNetwork()
{
	 m_virtualNetwork->UpdateNetwork(); 
}

void Farmer::UpdateRiskaversion() { 

	m_riskaversion_temp = m_riskaversion;
	for (auto a_neighbour : m_neighbours) {

		double neighbour_risk_aversion = a_neighbour->Get_riskaversion();
		double neighbour_risk_span = a_neighbour->Get_riskspan();

		double neighbour_risk_lowerbound = neighbour_risk_aversion - neighbour_risk_span / 2;
		if (neighbour_risk_lowerbound < 0) { neighbour_risk_lowerbound = 0; }

		double neighbour_risk_upperbound = neighbour_risk_aversion + neighbour_risk_span / 2;
		if (neighbour_risk_upperbound > 1) { neighbour_risk_upperbound = 1; }

		double owner_risk_lowerbound = m_riskaversion_temp - m_riskspan / 2;
		if (owner_risk_lowerbound < 0) { owner_risk_lowerbound = 0; }
		double owner_risk_upperbound = m_riskaversion_temp + m_riskspan / 2;
		if (owner_risk_upperbound > 1) { owner_risk_upperbound = 1; }

		double overlap = min(owner_risk_upperbound, neighbour_risk_upperbound) - max(owner_risk_lowerbound, neighbour_risk_lowerbound);
		
		if (overlap > 0) { 
			double m_overlap = overlap / 2 + max(owner_risk_lowerbound, neighbour_risk_lowerbound);
			double mobility = (m_riskspan / (m_riskspan + neighbour_risk_span))* cfg_FarmerMobility.value();
			m_riskaversion_temp = m_riskaversion_temp * (1 - mobility) + m_overlap * mobility;
		}
	}
}

void Farmer::UpdateRiskaversion(int i) {
	Farmer* a_neighbour;
	a_neighbour = m_neighbours[i];
	double neighbour_risk_aversion = a_neighbour->Get_riskaversion();
	double neighbour_risk_span = a_neighbour->Get_riskspan();

	double neighbour_risk_lowerbound = neighbour_risk_aversion - neighbour_risk_span/2;
	if (neighbour_risk_lowerbound < 0) { neighbour_risk_lowerbound = 0; }

	double neighbour_risk_upperbound = neighbour_risk_aversion + neighbour_risk_span / 2;
	if (neighbour_risk_upperbound > 1) { neighbour_risk_upperbound = 1; }

	double owner_risk_lowerbound = m_riskaversion - m_riskspan / 2;
	if (owner_risk_lowerbound < 0) { owner_risk_lowerbound = 0; }
	double owner_risk_upperbound = m_riskaversion + m_riskspan / 2;
	if (owner_risk_upperbound > 1) { owner_risk_upperbound = 1; }

	double overlap = min(owner_risk_upperbound, neighbour_risk_upperbound) - max(owner_risk_lowerbound, neighbour_risk_lowerbound);
	if (overlap > 0) {
		double m_overlap = overlap / 2 + max(owner_risk_lowerbound, neighbour_risk_lowerbound);
		double mobility = (m_riskspan / (m_riskspan + neighbour_risk_span)) * cfg_FarmerMobility.value();
		m_riskaversion = m_riskaversion * (1 - mobility) + m_overlap * mobility;

	}

}

void Farmer::ChooseNeighbours() {
	
	for (int i = 0; i < m_neighbours_spatial; i++) {
		m_neighbours.push_back(m_spatialNetwork->GetNeighbour(i));
	}
	
	if (!m_useonlyspatial) {
		for (int i = 0; i < m_neighbours_virtual; i++) {
			m_neighbours.push_back(m_virtualNetwork->GetNeighbour(i));
		}
		for (int i = 0; i < m_neighbours_associative; i++) {
			m_neighbours.push_back(m_associativeNetwork->GetNeighbour(i));
		}
	}
}


void Farmer::CheckManagementImpacts(FarmEvent* ev) {
	/**
	Checks performance of the management against expected norms
	*/
	vector<int> totals = ev->m_field->GetManagementTotals();
	int crop = int(ev->m_field->GetCropType());
	// Now the farmer has enough information to evauate his performance.
	int FungicideNorm = m_MyFarmManager->GetManagementNorm(crop, fmc_Fungicide);
	int HerbicideNorm = m_MyFarmManager->GetManagementNorm(crop, fmc_Herbicide);
	int InsecticideNorm = m_MyFarmManager->GetManagementNorm(crop, fmc_Insecticide);
	int FertilizerNorm = m_MyFarmManager->GetManagementNorm(crop, fmc_Fertilizer);
	// The information can be used as you like, but here is a simple relative measure
	double P_relativeF = totals[fmc_Fertilizer] / double(FertilizerNorm);
	double P_relativeH = totals[fmc_Herbicide] / double(HerbicideNorm);
	double P_relativeI = totals[fmc_Insecticide] / double(InsecticideNorm);
	double P_relativeFg = totals[fmc_Fungicide] / double(FungicideNorm);
}
