/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Bombus_Population_Manager.cpp
Version of  10 September 2021 \n
By Jordan Chetcuti \n \n
*/
//---------------------------------------------------------------------------
#include <string.h>
#include <fstream>
#include<vector>
#pragma warning( push )
#pragma warning( disable : 4100)
#pragma warning( disable : 4127)
#pragma warning( disable : 4244)
#pragma warning( disable : 4267)
#include <blitz/array.h>
#pragma warning( pop )
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Bombus/Bombus.h"
#include "../Bombus/Bombus_Population_Manager.h"
//---------------------------------------------------------------------------------------


CfgStr cfg_BombusOutput("BOMBUS_OUTPUT", CFG_CUSTOM, "TIALMaSSConfig.cfg");
CfgInt cfg_BombusRunNo("BOMBUS_RUNNO", CFG_CUSTOM, 0);
CfgStr cfg_ProbeName("PROBE_NAME", CFG_CUSTOM, "%sProbe.res");

CfgInt cfg_BombusNoStartingGyne("BOMBUS_NOSTARTINGGYNE", CFG_CUSTOM, 1);

/** The age of the first gynes. Will be organic for future generations, but here, a figured a third of the year for the first? */
CfgInt cfg_BombusStartGyneAge("BOMBUS_STARTGYNEAGE", CFG_CUSTOM, 144000);
/** \brief These are the mean mass and body fat weight in grams from \cite<Samuelson2018>*/
CfgFloat cfg_BombusStartGyneMass("BOMBUS_STARTGYNEMASS", CFG_CUSTOM, 1325.57);
CfgFloat cfg_BombusStartGyneMassSD("BOMBUS_STARTGYNEMASSSD", CFG_CUSTOM, 20.76);

CfgFloat cfg_BombusStartGyneBodyFat("BOMBUS_STARTGYNEBODYFAT", CFG_CUSTOM, 1056.47);
CfgFloat cfg_BombusStartGyneBodyFatSD("BOMBUS_STARTGYNEBODYFATSD", CFG_CUSTOM, 132.56);

CfgFloat cfg_BombuswaxConcInter("BOMBUS_WAXCONCINTER", CFG_CUSTOM, 0.14);
CfgFloat cfg_BombuswaxConcInterSD("BOMBUS_WAXCONCINTERSD", CFG_CUSTOM, 0.01);


CfgFloat cfg_BombusDaysHibernatedMean("BOMBUS_DAYSHIBERNATEDMEAN", CFG_CUSTOM, 100.0);
CfgFloat cfg_BombusDaysHibernatedSD("BOMBUS_DAYSHIBERNATEDSD", CFG_CUSTOM, 10.0);


CfgFloat cfg_BombusNoStartingColWorkers("BOMBUS_NOSTARTINGCOLWORKERS", CFG_CUSTOM, 2.0);
CfgFloat cfg_BombusNoStartingColWorkersSD("BOMBUS_NOSTARTINGCOLWORKERSSD", CFG_CUSTOM, 0.0);

CfgFloat cfg_BombuswaxConcCoef("BOMBUS_WAXCONCCOEF", CFG_CUSTOM, -0.0004);

CfgFloat cfg_BombusStartGynePropOvaries("BOMBUS_STARTGYNEPROPOVARIES", CFG_CUSTOM, 0.04);

CfgFloat cfg_BombusminsQueenCanReduce("BOMBUS_MINSQUEENCANREDUCE", CFG_CUSTOM, 8.0);
CfgFloat cfg_BombusminsQueenCanReduceSD("BOMBUS_MINSQUEENCANREDUCESD", CFG_CUSTOM, 0.0);
//CfgFloat appears to be returning a int?

//---------------------------------------------------------------------------
//                         BOMBUS CONSTANTS
//---------------------------------------------------------------------------


// Assign default static member values (these will be changed later).
double Bombus_Colony::m_TempToday = -9999.0;
double Bombus_Worker::m_TempToday = -9999;
string Bombus_Recording::m_RunNo = "0";
vector<string> Bombus_Recording::m_VectString{"RunNo", "Year", "Day"};

//---------------------------------------------------------------------------
/**
 * \brief The Bombus population manager needs to release the memory used recording the individual bees. 
 */
Bombus_Population_Manager::~Bombus_Population_Manager()
{
	delete RecordingIndividuals;
	// Should all be done by the Population_Manager destructor
}

Bombus_Recording::Bombus_Recording(string a_FileName, vector<string> a_headings, string a_RunNo)
{
	m_RunNo = a_RunNo;

	m_BombusRecordingFile = new ofstream(a_FileName);


	if (!(*m_BombusRecordingFile).is_open())
	  {
		  g_msg->Warn( WARN_FILE, "Bombus_Recording::Bombus_Recording() Unable to open file for append: ", a_FileName );
		  exit(1);
	  }
	RecordValues(a_headings);
}


Bombus_Recording::~Bombus_Recording()
{
	m_BombusRecordingFile->flush();
	m_BombusRecordingFile->close();
	delete m_BombusRecordingFile;
}


void Bombus_Recording::RecordValues(vector<string> a_values)
{
	ostream_iterator<string> output_iterator((*m_BombusRecordingFile), "\t");
	std::copy(m_VectString.begin(),m_VectString.end(), output_iterator);
    std::copy(a_values.begin(), a_values.end(), output_iterator);
	(*m_BombusRecordingFile) << '\n';
}

void Bombus_Recording::SetDate(int a_year, int a_day)
{
	m_VectString = {m_RunNo, to_string( a_year), to_string(a_day)};

}

//---------------------------------------------------------------------------
Bombus_Population_Manager::Bombus_Population_Manager(Landscape* L) : Population_Manager(L, 9)
{
	/**
	To give each adult Bombus an ID, the population manager counts up from zero.
	*/
	/*to_Colony = 0,
	to_Cluster
	to_Gyne = 2,
	to_Queen,
	to_Egg,
	to_Larva,
	to_Pupa,
	to_Worker,
	to_Male*/
	/** Loads the list of Animal Classes. */
	m_ListNames[0] = "Colony";
	m_ListNames[1] = "Cluster";
	m_ListNames[2] = "Gynes";
	m_ListNames[3] = "Queen";
	m_ListNames[4] = "Eggs";
	m_ListNames[5] = "Larva";
	m_ListNames[6] = "Pupa";
	m_ListNames[7] = "Worker";
	m_ListNames[8] = "Male";
	m_ListNameLength = 9;
	m_population_type = TOP_Bombus;
	//// We need one vector for each life stage
	//TheArray.resize(m_ListNameLength);
	//for (double i = 0; i < m_ListNameLength; i++) {
	//	// Set default BeforeStepActions
	//	BeforeStepActions[i] = 0;
	//	//m_ListNames[i] = "Unknown";
	//	m_LiveArraySize.push_back(0);
	//}
	m_SimulationName = "Bombus Simulation";
	// Set up necessary conditions for mid-lifecycle start
	m_PreWinteringEndFlag = true;
	/**
	* A few objects are created here just to set the species specific parameter values.
	* It is preferred that this is done here rather than the constructure for parameters that are since this only needs doing once.
	*/

	Nectar bonectar;
	bonectar.SetStatic();

	Pollen bopollen;
	bopollen.SetStatic();

	Food bofood;
	bofood.SetStatic();

	Stomach bostomach;
	bostomach.SetStatic();
	

	Bombus_Colony bocolony(0, 0, m_TheLandscape, this, 0, toColonyLoc_BoxLab);
	bocolony.SetLabTemp();
	bocolony.SetStatic();

	//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberYoung, Bombus_Colony* a_Colony = 0
	Bombus_Cluster bocluster(0, 0, m_TheLandscape, this, 0, &bocolony);
	bocluster.SetStatic();

	Mass bomass;
	bomass.SetStatic();

	Bombus_Base bobase(0, 0, m_TheLandscape, this, &bocolony);
	bobase.SetStatic();

	Bombus_Egg boegg(0, 0, m_TheLandscape, this, 0, 0, 0, 0, &bocolony, nullptr);
	boegg.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Colony, a_Cluster
	Bombus_Larva bolarva(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, &bocolony, nullptr);
	bolarva.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_Colony, a_Cluster 
	Bombus_Pupa bopupa(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 0, 0, &bocolony, nullptr, false, 0.0);
	bopupa.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id
	Bombus_Worker boWorker(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 200.0, 0.0, 0, &bocolony, 0);
	boWorker.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id, a_SpermID
	Bombus_Gyne boGyne(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 0, 600.0, 0, 0, &bocolony, 0, 0);
	boGyne.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id, a_SpermID
	Bombus_Queen boQueen(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 0, 800.0, 0, 0, &bocolony, 0, 0);
	boQueen.SetStatic();


	Bombus_Male boMale(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 400.0, 0, &bocolony, 0);
	boMale.SetStatic();



	// Create some animals
	auto StartGyneMass = double(cfg_BombusStartGyneMass.value());
	auto StartGyneMassSD = double(cfg_BombusStartGyneMassSD.value());

	string Variables = to_string(StartGyneMass) + " " + to_string(StartGyneMassSD);
	probability_distribution StartGyneMassDistribution{"NORMAL", Variables};


	auto StartGyneBodyFat = double(cfg_BombusStartGyneBodyFat.value());
	auto StartGyneBodyFatSD = double(cfg_BombusStartGyneBodyFatSD.value());
	string Variables2 = to_string(StartGyneBodyFat) + " " + to_string(StartGyneBodyFatSD);
	probability_distribution StartGyneBodyFatDistribution{"NORMAL", Variables2};


	auto waxConcInter = double(cfg_BombuswaxConcInter.value());
	auto waxConcInterSD = double(cfg_BombuswaxConcInterSD.value());
	string Variables3 = to_string(waxConcInter) + " " + to_string(waxConcInterSD);
	probability_distribution waxConcInterDistribution{"NORMAL", Variables3};

	auto DaysHibernatedMean = double(cfg_BombusDaysHibernatedMean.value());
	auto DaysHibernatedSD = double(cfg_BombusDaysHibernatedSD.value());
	string Variables4 = to_string(DaysHibernatedMean) + " " + to_string(DaysHibernatedSD);
	probability_distribution DaysHibernatedDistribution{"NORMAL", Variables4};


	auto minsQueenCanReduceMean = double(cfg_BombusminsQueenCanReduce.value());
	auto minsQueenCanReduceSD = double(cfg_BombusminsQueenCanReduceSD.value());
	string Variables5 = to_string(minsQueenCanReduceMean) + " " + to_string(minsQueenCanReduceSD);
	probability_distribution minsQueenCanReduceDistribution{"NORMAL", Variables5};

	auto StartGynePropOvaries = double(cfg_BombusStartGynePropOvaries.value());
	struct_Bombus* sp;
	sp = new struct_Bombus;
	//(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID, unsigned long a_FatherID, double a_age, double a_Mass, double a_BodyFat)
	sp->BPM = this;
	sp->L = m_TheLandscape;


	auto StartingColWorkers = double(cfg_BombusNoStartingColWorkers.value());
	auto StartingColWorkersSD = double(cfg_BombusNoStartingColWorkersSD.value());

	string Variables6 = to_string(StartingColWorkers) + " " + to_string(StartingColWorkersSD);
	probability_distribution StartingColWorkersDistribution{"NORMAL", Variables6};


	CfgInt cfg_BombusSexGenes("BOMBUS_SEX_GENES", CFG_CUSTOM, 24);

	int NoStartingGyne = cfg_BombusNoStartingGyne.value();
	for (int i = 0; i < NoStartingGyne; i++) // This will need to be an input variable (config)
	{
		sp->x = random(SimW);
		sp->y = random(SimH);
		/** \brief Set a random ID for the mother and father*/
		sp->MotherID = nextID();
		sp->FatherID = nextID();
		/** The age of the gyne. Will be orgnanic for future generations, but here, a figured a third of the year? */
		sp->age = cfg_BombusStartGyneAge.value();
		/** \brief These are the mean mass and body fat weight in grams from \cite<Samuelson2018>*/
		sp->Mass = StartGyneMassDistribution.get();
		sp->BodyFat = StartGyneBodyFatDistribution.get();
		sp->Callow = false;

		sp->DaysHibernated = int(DaysHibernatedDistribution.get());

		double ConcValue = cfg_BombuswaxConcCoef.value() * sp->DaysHibernated + waxConcInterDistribution.get();
		sp->waxConcInter = ConcValue;


		sp->PropOvaries = StartGynePropOvaries;
		sp->Colony = nullptr;
		sp->m_id = nextID();
		sp->SpermID = nextID();
		sp->minsQueenCanReduce = minsQueenCanReduceDistribution.get();
		sp->Callow = false;
		sp->StartingColWorkers = int(round(StartingColWorkersDistribution.get()));
		sp->StartingIndiv = true;


		sp->SexLocus_1 = random(cfg_BombusSexGenes.value()) + 1;
		int tempRandomSexLocus = random(cfg_BombusSexGenes.value()) + 1;
		while (sp->SexLocus_1 == tempRandomSexLocus)
		{
			tempRandomSexLocus = random(cfg_BombusSexGenes.value()) + 1;
		}
		sp->SexLocus_2 = tempRandomSexLocus;
		sp->MatesSexLocus = random(cfg_BombusSexGenes.value()) + 1;

		if(NoStartingGyne == 1)
		{	cout << "Single colony run: \n";
			cout << "Colony x: " << sp->x << "\n";
			cout << "Colony y: " << sp->y << "\n";
			cout << "Gynes sex locus 1: " << sp->SexLocus_1 << "\n";
			cout << "Gynes sex locus 2: " << sp->SexLocus_2 << "\n";
			cout << "Mates sex locus: " << sp->MatesSexLocus << "\n";
		}


		//It would be good to have a way to set the gynes as hibernating or not. I was thinking we don't want the queen to have this as an input variable, but this could
		//be used to chill a queen either if temperatures suddenly drop, or in a commercial colony. I don't know if the queen would then diapause still.
		CreateObjects(to_Gyne, nullptr, sp, 1);
	}
	// Init performs initialization of the data to run the Bombus model
	//Init();
	delete sp;

	RecordingIndividuals = new Bombus_Recording(cfg_BombusOutput.value(), {
		                                           "Colony",
		                                           "ID",
		                                           "MothersID",
		                                           "FathersID",
		                                           "Identity",
		                                           "Age",
		                                           "StageAge",
		                                           "Mass",
		                                           "Starting",
		                                           "LabTemp",
		                                           "ColonyTemp"
	                                           }, to_string(cfg_BombusRunNo.value()));

	

}

//---------------------------------------------------------------------------
void Bombus_Population_Manager::CreateObjects(int ob_type, TAnimal* /*pvo*/, struct_Bombus* data, int number)
{
	Bombus_Egg* new_Bombus_Egg;
	Bombus_Larva* new_Bombus_Larva;
	Bombus_Pupa* new_Bombus_Pupa;
	Bombus_Worker* new_Bombus_Worker;
	Bombus_Gyne* new_Bombus_Gyne;
	Bombus_Queen* new_Bombus_Queen;
	Bombus_Male* new_Bombus_Male;
	for (int i = 0; i < number; i++)
	{
		/** As the workers have their own enumerated list, the need their own switch*/
		switch (ob_type)
		{
		case to_Egg:
			//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID, unsigned long a_FatherID
			if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Egg = dynamic_cast<Bombus_Egg*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Bombus_Egg->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                       data->SexLocus_1, data->SexLocus_2, data->Colony, data->Cluster);
				new_Bombus_Egg->SetMotherID(data->MotherID);
				new_Bombus_Egg->SetFatherID(data->FatherID);
				new_Bombus_Egg->SetColony(data->Colony);
				new_Bombus_Egg->SetCluster(data->Cluster);
				new_Bombus_Egg->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Egg->SetSexLocus_2(data->SexLocus_2);

				data->Cluster->AddEgg(new_Bombus_Egg);
			}
			else
			{
				new_Bombus_Egg = new Bombus_Egg(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                data->SexLocus_1, data->SexLocus_2, data->Colony, data->Cluster);
				TheArray[ob_type].push_back(new_Bombus_Egg);
				new_Bombus_Egg->SetMotherID(data->MotherID);
				new_Bombus_Egg->SetFatherID(data->FatherID);
				new_Bombus_Egg->SetColony(data->Colony);
				new_Bombus_Egg->SetCluster(data->Cluster);
				new_Bombus_Egg->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Egg->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddEgg(new_Bombus_Egg);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Larva:
			//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID, unsigned long a_FatherID, double a_age
			if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Larva = dynamic_cast<Bombus_Larva*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Bombus_Larva->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                         data->SexLocus_1, data->SexLocus_2, data->age, data->Colony, data->Cluster);
				new_Bombus_Larva->SetMotherID(data->MotherID);
				new_Bombus_Larva->SetFatherID(data->FatherID);
				new_Bombus_Larva->SetAge(data->age);
				new_Bombus_Larva->SetColony(data->Colony);
				new_Bombus_Larva->SetCluster(data->Cluster);
				new_Bombus_Larva->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Larva->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddLarva(new_Bombus_Larva);
				///** Larva knowing which larva it is in a set or list of larva*/
				//new_Bombus_Larva->SetNoLarva(data->Cluster->GetNumberLarva()-1);
			}
			else
			{
				new_Bombus_Larva = new Bombus_Larva(data->x, data->y, data->L, data->BPM, data->MotherID,
				                                    data->FatherID, data->SexLocus_1, data->SexLocus_2, data->age,
				                                    data->Colony, data->Cluster);
				TheArray[ob_type].push_back(new_Bombus_Larva);
				new_Bombus_Larva->SetMotherID(data->MotherID);
				new_Bombus_Larva->SetFatherID(data->FatherID);
				new_Bombus_Larva->SetAge(data->age);
				new_Bombus_Larva->SetColony(data->Colony);
				new_Bombus_Larva->SetCluster(data->Cluster);
				new_Bombus_Larva->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Larva->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddLarva(new_Bombus_Larva);
				///** Larva knowing which larva it is in a set or list of larva*/
				//new_Bombus_Larva->SetNoLarva(data->Cluster->GetNumberLarva() - 1);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Pupa:
			//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID, unsigned long a_FatherID, double a_age, double a_Mass, double a_BodyFat
			if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Pupa = dynamic_cast<Bombus_Pupa*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Bombus_Pupa->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                        data->SexLocus_1, data->SexLocus_2, data->age, data->Mass, data->BodyFat,
				                        data->Colony, data->Cluster, data->IsGyne, data->PupaDM);
				new_Bombus_Pupa->SetMotherID(data->MotherID);
				new_Bombus_Pupa->SetFatherID(data->FatherID);
				new_Bombus_Pupa->SetAge(data->age);
				new_Bombus_Pupa->SetMass(data->Mass);
				new_Bombus_Pupa->SetColony(data->Colony);
				new_Bombus_Pupa->SetCluster(data->Cluster);
				new_Bombus_Pupa->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Pupa->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddPupa(new_Bombus_Pupa);
			}
			else
			{
				new_Bombus_Pupa = new Bombus_Pupa(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                  data->SexLocus_1, data->SexLocus_2, data->age, data->Mass,
				                                  data->BodyFat, data->Colony, data->Cluster, data->IsGyne,
				                                  data->PupaDM);
				TheArray[ob_type].push_back(new_Bombus_Pupa);
				new_Bombus_Pupa->SetMotherID(data->MotherID);
				new_Bombus_Pupa->SetFatherID(data->FatherID);
				new_Bombus_Pupa->SetAge(data->age);
				new_Bombus_Pupa->SetMass(data->Mass);
				new_Bombus_Pupa->SetColony(data->Colony);
				new_Bombus_Pupa->SetCluster(data->Cluster);
				new_Bombus_Pupa->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Pupa->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddPupa(new_Bombus_Pupa);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Worker:
			if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Worker = dynamic_cast<Bombus_Worker*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Bombus_Worker->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                          data->SexLocus_1, data->SexLocus_2, data->age, data->Mass, data->BodyFat,
				                          data->PropOvaries, data->Colony, data->m_id);
				new_Bombus_Worker->SetMotherID(data->MotherID);
				new_Bombus_Worker->SetFatherID(data->FatherID);
				new_Bombus_Worker->SetAge(data->age);
				new_Bombus_Worker->SetMass(data->Mass);
				new_Bombus_Worker->SetColony(data->Colony);
				new_Bombus_Worker->SetMyID(data->m_id);
				new_Bombus_Worker->SetAgeDegrees(data->AgeDM);
				new_Bombus_Worker->SetCallow(data->Callow);
				new_Bombus_Worker->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Worker->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Worker->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Worker->AddToWarmAccount();
			}
			else
			{
				new_Bombus_Worker = new Bombus_Worker(data->x, data->y, data->L, data->BPM, data->MotherID,
				                                      data->FatherID, data->SexLocus_1, data->SexLocus_2, data->age,
				                                      data->Mass, data->BodyFat, data->PropOvaries, data->Colony,
				                                      data->m_id);
				TheArray[ob_type].push_back(new_Bombus_Worker);
				new_Bombus_Worker->SetMotherID(data->MotherID);
				new_Bombus_Worker->SetFatherID(data->FatherID);
				new_Bombus_Worker->SetAge(data->age);
				new_Bombus_Worker->SetMass(data->Mass);
				new_Bombus_Worker->SetColony(data->Colony);
				new_Bombus_Worker->SetMyID(data->m_id);
				new_Bombus_Worker->SetAgeDegrees(data->AgeDM);
				new_Bombus_Worker->SetCallow(data->Callow);
				new_Bombus_Worker->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Worker->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Worker->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Worker->AddToWarmAccount();
			}

			if (data->Callow == false)
			{
				data->Colony->m_ActiveColonyWorkers.push_back(new_Bombus_Worker);
			}


			IncLiveArraySize(ob_type);
			break;
		case to_Gyne:
			if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Gyne = dynamic_cast<Bombus_Gyne*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Bombus_Gyne->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                        data->SexLocus_1, data->SexLocus_2, data->MatesSexLocus, data->age, data->Mass,
				                        data->BodyFat, data->PropOvaries, data->Colony, data->m_id, data->SpermID);
				new_Bombus_Gyne->SetMotherID(data->MotherID);
				new_Bombus_Gyne->SetFatherID(data->FatherID);
				new_Bombus_Gyne->SetAge(data->age);
				new_Bombus_Gyne->SetMass(data->Mass);
				new_Bombus_Gyne->SetBodyFat(data->BodyFat);
				new_Bombus_Gyne->SetColony(data->Colony);
				new_Bombus_Gyne->SetMyID(data->m_id);
				new_Bombus_Gyne->SetSpermID(data->SpermID);
				new_Bombus_Gyne->SetDaysHibernated(data->DaysHibernated);
				new_Bombus_Gyne->SetChemicalConc(data->waxConcInter);
				new_Bombus_Gyne->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Gyne->SetCallow(data->Callow);
				new_Bombus_Gyne->SetStartingColWorkers(data->StartingColWorkers);
				new_Bombus_Gyne->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Gyne->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Gyne->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Gyne->SetMateSexAllele(data->MatesSexLocus);
			}
			else
			{
				new_Bombus_Gyne = new Bombus_Gyne(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                  data->SexLocus_1, data->SexLocus_2, data->MatesSexLocus, data->age,
				                                  data->Mass, data->BodyFat, data->PropOvaries, data->Colony,
				                                  data->m_id, data->SpermID);
				TheArray[ob_type].push_back(new_Bombus_Gyne);
				new_Bombus_Gyne->SetMotherID(data->MotherID);
				new_Bombus_Gyne->SetFatherID(data->FatherID);
				new_Bombus_Gyne->SetAge(data->age);
				new_Bombus_Gyne->SetMass(data->Mass);
				new_Bombus_Gyne->SetBodyFat(data->BodyFat);
				new_Bombus_Gyne->SetColony(data->Colony);
				new_Bombus_Gyne->SetMyID(data->m_id);
				new_Bombus_Gyne->SetSpermID(data->SpermID);
				new_Bombus_Gyne->SetDaysHibernated(data->DaysHibernated);
				new_Bombus_Gyne->SetChemicalConc(data->waxConcInter);
				new_Bombus_Gyne->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Gyne->SetCallow(data->Callow);
				new_Bombus_Gyne->SetStartingColWorkers(data->StartingColWorkers);
				new_Bombus_Gyne->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Gyne->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Gyne->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Gyne->SetMateSexAllele(data->MatesSexLocus);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Queen:
			if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Queen = dynamic_cast<Bombus_Queen*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Bombus_Queen->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                         data->SexLocus_1, data->SexLocus_2, data->MatesSexLocus, data->age, data->Mass,
				                         data->BodyFat, data->PropOvaries, data->Colony, data->m_id, data->SpermID);
				new_Bombus_Queen->SetMotherID(data->MotherID);
				new_Bombus_Queen->SetFatherID(data->FatherID);
				new_Bombus_Queen->SetAge(data->age);
				new_Bombus_Queen->SetMass(data->Mass);
				new_Bombus_Queen->SetBodyFat(data->BodyFat);
				new_Bombus_Queen->SetColony(data->Colony);
				new_Bombus_Queen->SetMyID(data->m_id);
				new_Bombus_Queen->SetSpermID(data->SpermID);
				new_Bombus_Queen->SetDaysHibernated(data->DaysHibernated);
				new_Bombus_Queen->SetChemicalConc(data->waxConcInter);
				new_Bombus_Queen->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Queen->SetCallow(false);
				new_Bombus_Queen->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Queen->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Queen->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Queen->SetMateSexAllele(data->MatesSexLocus);
				new_Bombus_Queen->AddToWarmAccount();
				data->Colony->SetColoniesQueen(new_Bombus_Queen);
			}
			else
			{
				new_Bombus_Queen = new Bombus_Queen(data->x, data->y, data->L, data->BPM, data->MotherID,
				                                    data->FatherID, data->SexLocus_1, data->SexLocus_2,
				                                    data->MatesSexLocus, data->age, data->Mass, data->BodyFat,
				                                    data->PropOvaries, data->Colony, data->m_id, data->SpermID);
				TheArray[ob_type].push_back(new_Bombus_Queen);
				new_Bombus_Queen->SetMotherID(data->MotherID);
				new_Bombus_Queen->SetFatherID(data->FatherID);
				new_Bombus_Queen->SetAge(data->age);
				new_Bombus_Queen->SetMass(data->Mass);
				new_Bombus_Queen->SetBodyFat(data->BodyFat);
				new_Bombus_Queen->SetColony(data->Colony);
				new_Bombus_Queen->SetMyID(data->m_id);
				new_Bombus_Queen->SetSpermID(data->SpermID);
				new_Bombus_Queen->SetDaysHibernated(data->DaysHibernated);
				new_Bombus_Queen->SetChemicalConc(data->waxConcInter);
				new_Bombus_Queen->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Queen->SetCallow(false);
				new_Bombus_Queen->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Queen->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Queen->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Queen->SetMateSexAllele(data->MatesSexLocus);
				new_Bombus_Queen->AddToWarmAccount();
				data->Colony->SetColoniesQueen(new_Bombus_Queen);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Male:
			if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Male = dynamic_cast<Bombus_Male*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Bombus_Male->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                        data->SexLocus_1, data->SexLocus_2, data->age, data->Mass, data->BodyFat,
				                        data->Colony, data->m_id);
				new_Bombus_Male->SetMotherID(data->MotherID);
				new_Bombus_Male->SetFatherID(data->FatherID);
				new_Bombus_Male->SetAge(data->age);
				new_Bombus_Male->SetColony(data->Colony);
				new_Bombus_Male->SetMyID(data->m_id);
				new_Bombus_Male->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Male->SetSexLocus_2(data->SexLocus_2);
			}
			else
			{
				new_Bombus_Male = new Bombus_Male(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                  data->SexLocus_1, data->SexLocus_2, data->age, data->Mass,
				                                  data->BodyFat, data->Colony, data->m_id);
				TheArray[ob_type].push_back(new_Bombus_Male);
				new_Bombus_Male->SetMotherID(data->MotherID);
				new_Bombus_Male->SetFatherID(data->FatherID);
				new_Bombus_Male->SetAge(data->age);
				new_Bombus_Male->SetColony(data->Colony);
				new_Bombus_Male->SetMyID(data->m_id);
				new_Bombus_Male->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Male->SetSexLocus_2(data->SexLocus_2);
			}
			IncLiveArraySize(ob_type);
			break;
		default:
			m_TheLandscape->Warn("Bombus_Population_Manager::CreateObjects() unknown object type - ",
			                     to_string(ob_type));
			exit(1);  // NOLINT(concurrency-mt-unsafe)
		}
	}
}

//---------------------------------------------------------------------------
Bombus_Colony* Bombus_Population_Manager::CreateColony(int ob_type, struct_Bombus* data, int number)
{
	Bombus_Colony* new_Bombus_Colony;
	if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
	{
		// We need to reuse an object
		new_Bombus_Colony = dynamic_cast<Bombus_Colony*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
		new_Bombus_Colony->ReInit(data->x, data->y, data->L, data->BPM, data->m_id, data->ColonyLocation);
		new_Bombus_Colony->SetColonyID(data->m_id);
	}
	else
	{
		new_Bombus_Colony = new Bombus_Colony(data->x, data->y, data->L, data->BPM, data->m_id, data->ColonyLocation);
		TheArray[ob_type].push_back(new_Bombus_Colony);
		new_Bombus_Colony->SetColonyID(data->m_id);
	}
	IncLiveArraySize(ob_type);
	return new_Bombus_Colony;
}

//--------------------------------------------------------------------------------------------------------------------------------
Bombus_Cluster* Bombus_Population_Manager::CreateCluster(int ob_type, struct_Bombus* data, int NumberYoung)
{
	Bombus_Cluster* new_Bombus_Cluster;
	if (static_cast<unsigned>(TheArray[ob_type].size()) > GetLiveArraySize(ob_type))
	{
		// We need to reuse an object
		new_Bombus_Cluster = dynamic_cast<Bombus_Cluster*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
		new_Bombus_Cluster->ReInit(data->x, data->y, data->L, data->BPM, NumberYoung, data->Colony);
	}
	else
	{
		new_Bombus_Cluster = new Bombus_Cluster(data->x, data->y, data->L, data->BPM, NumberYoung, data->Colony);
		TheArray[ob_type].push_back(new_Bombus_Cluster);
	}
	IncLiveArraySize(ob_type);
	return new_Bombus_Cluster;
}

unsigned long Bombus_Population_Manager::nextID()
{
	//return 1;
	return currentID++;
}

void Bombus_Population_Manager::DoFirst()
{
	double temp = m_TheLandscape->SupplyTemp();
	double SoilTemp = m_TheLandscape->SupplySoilTemp();
	//double DayProp = m_TheLandscape->SupplyDaylightProp();
	//double NightProp = m_TheLandscape->SupplyNightProp();
	int Time = g_date->GetHour() * 60 + g_date->GetMinute();
	if (Time >= g_date->SunRiseTime() && Time < g_date->SunSetTime())
	{
		IsDaylight = true;
	}
	else
	{
		IsDaylight = false;
	}
	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Colony
	Bombus_Worker BoW(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 200.0, 0, 0, nullptr);
	BoW.SetTemp(temp); // Sets the static variable for temperature for all Bombus (speed optimization)
	Bombus_Colony BoC(0, 0, m_TheLandscape, this, 0, toColonyLoc_Underground);
	BoC.SetOutsideTemp(temp);
	BoC.SetSoilTemp(SoilTemp);
	if (Time == 0)
	{
		BoC.SetLabTemp();
	}


	RecordingIndividuals->SetDate(m_TheLandscape->SupplyYearNumber(), m_TheLandscape->SupplyDayInYear());



}

void Bombus_Population_Manager::DoAfter()
{
}

void Bombus_Population_Manager::DoLast()
{
}

GetProbeName::GetProbeName()
{
	value = cfg_ProbeName.value();
}
