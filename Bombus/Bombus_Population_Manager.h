/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Bombus_Population_Manager.h
Version of  10 September 2021 \n
By Jordan Chetcuti \n \n
*/
//---------------------------------------------------------------------------
#ifndef Bombus_Population_ManagerH
#define Bombus_Population_ManagerH
#include <unordered_map>
#include <utility>
//---------------------------------------------------------------------------

class GetProbeName
{
public:
	string value;
	GetProbeName();
};

//---------------------------------------------------------------------------
class Bombus;
/**
\brief
Used for creation of a new Bombus object
*/
class struct_Bombus
{
public:
	/** \brief x-coord */
	int x = 0;
	/** \brief y-coord */
	int y = 0;
	/** \brief Landscape pointer */
	Landscape* L = nullptr;
	/** \brief Bombus_Population_Manager pointer */
	Bombus_Population_Manager* BPM = nullptr;
	/** \brief age */
	int age = 0;
	/** \brief colony or adults id */
	unsigned long m_id = 0;
	/** \bried MotherID */
	unsigned long MotherID = 0;
	/** \bried FatherID */
	unsigned long FatherID = 0;
	double Mass = 0;
	/** \brief this contains the mass of the body fat in mg. */
	double BodyFat = 0;
	/** \brief this contains the proportion of the individuals mass that is ovaries. */
	double PropOvaries = 0;
	/** \brief a pointer to a colony */
	Bombus_Colony* Colony = nullptr;
	/** \brief The gyne gets sperm from males and stores it overwinter */
	unsigned long SpermID = 0;
	/** \brief The juviniles are part of a cluster */
	Bombus_Cluster* Cluster = nullptr;
	/** \brief The location of the colony created */
	TTypeOfColonyLocation ColonyLocation = toColonyLoc_Foo;
	//int Time = 0;
	double PupaDM = 0.0;
	bool IsGyne = false;
	double AgeDM = 0.0;
	double waxConcInter = 0.0;
	int DaysHibernated = 0;

	double minsQueenCanReduce = 0.0;

	bool Callow = true;

	int StartingColWorkers = 0;

	bool StartingIndiv = false;

	/** \brief Individuals get given either one or two sex allele. Haploid males only one, diploid two, but they are the same one, all other two different ones.*/
	int SexLocus_1 = 0;
	int SexLocus_2 = 0;

	/** \brief The gyne needs to pass on the information of the sperms sex allele to her queen self. */
	int MatesSexLocus = 0;
};


/**
 * \brief Creates a text file with defined headings that can then have data saved to. 
 */
class Bombus_Recording
{
protected:
	ofstream* m_BombusRecordingFile;
	static string m_RunNo;

	static vector<string> m_VectString;

public:
	Bombus_Recording(string a_FileName, vector<string> a_headings, string a_RunNo);


	~Bombus_Recording();

	void RecordValues(vector<string> a_values);

	void SetDate(int a_year, int a_day);
};



/**
\brief
The class to handle all Bombus population related matters
*/
class Bombus_Population_Manager final : public Population_Manager
{
protected:
	// Attributes
	/**
	To give each adult Bombus an ID, the population manager counts up from zero.
	*/
	unsigned long currentID = 1;
	/** A flag to signal the prewinter phase is over */
	bool m_PreWinteringEndFlag;
public:
	// Methods
	/** \brief Bombus_Population_Manager Constructor */
	Bombus_Population_Manager(Landscape* L);

	/** \brief Bombus_Population_Manager Destructor */
	~Bombus_Population_Manager() override;
	/** \brief Method for creating a new individual Bombus */
	void CreateObjects(int ob_type, TAnimal* pvo, struct_Bombus* data, int number);
	/** \bried Like the more general create object, but specific for colony creation*/
	Bombus_Colony* CreateColony(int ob_type, struct_Bombus* data, int number);
	/** \bried Like the more general create object, but specific for cluster creation*/
	Bombus_Cluster* CreateCluster(int ob_type, struct_Bombus* data, int NumberYoung);
	vector<Bombus_Colony*> Colonies;
	vector<pair<int, int>> Colonies_coords;
	/** \brief Whether it is daylight or not.*/
	bool IsDaylight{};

	unsigned long nextID();

	/** \brief "Day", "Colony", "ID", "Identity", "Age", "Mass"*/
	Bombus_Recording* RecordingIndividuals;

protected:
	// Methods
	/** \brief  Things to do before anything else at the start of a timestep  */
	void DoFirst() override;

	/** \brief Things to do before the Step */
	//virtual void DoBefore(){
	// m_ThePollenMap->RefillPollenMap();
	//}
	/** \brief Things to do before the EndStep */
	void DoAfter() override;

	/** \brief Things to do after the EndStep */
	void DoLast() override;
};
#endif
