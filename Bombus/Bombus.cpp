/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Bombus.cpp
Version of  10 September 2021 \n
By Jordan Chetcuti \n \n
*/
// ReSharper disable CppClangTidyClangDiagnosticSwitchEnum
#include<vector>
#include <random>
#pragma warning( push )
#pragma warning( disable : 4100)
#pragma warning( disable : 4127)
#pragma warning( disable : 4244)
#pragma warning( disable : 4267)
#pragma warning( disable : 26812)
#include <blitz/array.h>
#pragma warning( pop )
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Bombus/Bombus.h"
#include "../Bombus/Bombus_Population_Manager.h"
//---------------------------------------------------------------------------
using namespace std;
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//                         BOMBUS CONSTANTS
//---------------------------------------------------------------------------

CfgFloat cfg_BombusEggBelowTempCoef("BOMBUS_EGGBELOWTEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusEggAboveTempCoef("BOMBUS_EGGABOVETEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusLarvaBelowTempCoef("BOMBUS_LARVABELOWTEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusLarvaAboveTempCoef("BOMBUS_LARVAABOVETEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusPupaBelowTempCoef("BOMBUS_PUPABELOWTEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusPupaAboveTempCoef("BOMBUS_PUPAABOVETEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusWorkerBelowTempCoef("BOMBUS_WORKERBELOWTEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusWorkerAboveTempCoef("BOMBUS_WORKERABOVETEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusGyneBelowTempCoef("BOMBUS_GYNEBELOWTEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusGyneAboveTempCoef("BOMBUS_GYNEABOVETEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusQueenBelowTempCoef("BOMBUS_QUEENBELOWTEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusQueenAboveTempCoef("BOMBUS_QUEENABOVETEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusMaleBelowTempCoef("BOMBUS_MALEBELOWTEMPCOEF", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusMaleAboveTempCoef("BOMBUS_MALEABOVETEMPCOEF", CFG_CUSTOM, 0.0);
CfgStr cfg_BombusColonyLocation("BOMBUS_COLONYLOCATION", CFG_CUSTOM, "toColonyLoc_BoxLab");
CfgInt cfg_BombusColonyDeathdays("BOMBUS_COLONYDEATHDAYS", CFG_CUSTOM, 365);
CfgInt cfg_BombusBroodStage("BOMBUS_BROODSTAGE", CFG_CUSTOM, 0);
CfgFloat cfg_BombusStartWorkerMassAlpha("BOMBUS_STARTWORKERMASSALPHA", CFG_CUSTOM, 5.42);
CfgFloat cfg_BombusStartWorkerMassBeta("BOMBUS_STARTWORKERMASSBETA", CFG_CUSTOM, 40.75284);
CfgBool cfg_BombusAccessForaging("BOMBUS_ACCESSFORAGING", CFG_CUSTOM, false);
CfgFloat cfg_BombusFedNectarPerDay("BOMBUS_FEDNECTARPERDAY", CFG_CUSTOM, 1000.0);
CfgFloat cfg_BombusFedSugarProp("BOMBUS_FEDSUGARPROP", CFG_CUSTOM, 0.45);
CfgFloat cfg_BombusStepEnergyInspection("BOMBUS_STEPENERGYINSPECTION", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusFedPollenPerDay("BOMBUS_FEDPOLLENPERDAY", CFG_CUSTOM, 10000000);
CfgFloat cfg_BombusPropBroodCare("BOMBUS_PROPBROODCARE", CFG_CUSTOM, 1.0);
CfgFloat cfg_BombusMinColonyTempToLay("BOMBUS_MINCOLONYTEMPTOLAY", CFG_CUSTOM, 28.0);
CfgFloat cfg_BombusOptimumDevTemp("BOMBUS_OPTIMUMDEVTEMP", CFG_CUSTOM, 32.0);
CfgFloat cfg_BombusOptimumColTemp("BOMBUS_OPTIMUMCOLTEMP", CFG_CUSTOM, 30.0);
CfgFloat cfg_BombusTempCoef("BOMBUS_TEMPCOEF", CFG_CUSTOM, -0.009);
CfgFloat cfg_BombusEnergyDensityEgg("BOMBUS_ENERGYDENSITYEGG", CFG_CUSTOM, 7.0);
CfgInt cfg_BombusEggMax("BOMBUS_EGGMAX", CFG_CUSTOM, 16);
CfgInt cfg_BombusEggMin("BOMBUS_EGGMIN", CFG_CUSTOM, 6);
CfgFloat cfg_BombusMinJuvDevTemp("BOMBUS_MINJUVDEVTEMP", CFG_CUSTOM, 25.0);
CfgFloat cfg_BombusB1EggCellPollen("BOMBUS_B1EGGCELLPOLLEN", CFG_CUSTOM, 10.0);
CfgFloat cfg_BombusB2EggCellPollen("BOMBUS_B2EGGCELLPOLLEN", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusB3EggCellPollen("BOMBUS_B3EGGCELLPOLLEN", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusWorkersEggCellPollen("BOMBUS_WORKERSEGGCELLPOLLEN", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusRootIncubatingProb("BOMBUS_ROOTINCUBATINGPROB", CFG_CUSTOM, 2.0);
CfgFloat cfg_BombusIncubatingDenominator("BOMBUSINCUBATINGDENOMINATOR", CFG_CUSTOM, 100.0);
CfgFloat cfg_BombusCallowForDM("BOMBUS_CALLOWFORDM", CFG_CUSTOM, 4320.0);
CfgFloat cfg_BombusCallowMinTemp("BOMBUS_CALLOWMINTEMP", CFG_CUSTOM, 25.0);
CfgFloat cfg_BombusPropDMmaxIsGyne("BOMBUS_PROPDMMAXISGYNE", CFG_CUSTOM, 0.95);
CfgFloat cfg_BombusLarvalDMmin("BOMBUS_LARVALDMMIN", CFG_CUSTOM, 43200.0);
CfgFloat cfg_BombusLarvalDMmax("BOMBUS_LARVALDMMAX", CFG_CUSTOM, 136800.0);
CfgFloat cfg_BombusLarvalMaleDM("BOMBUS_LARVALMALEDM", CFG_CUSTOM, 100800.0);
CfgFloat cfg_BombusLarvalC("BOMBUS_LARVALC", CFG_CUSTOM, 1.0);
CfgFloat cfg_BombusLarvalQ("BOMBUS_LARVALQ", CFG_CUSTOM, 100.0);
CfgFloat cfg_BombusLarvalB("BOMBUS_LARVALB", CFG_CUSTOM, 0.4);
CfgFloat cfg_BombusLarvalv("BOMBUS_LARVALV", CFG_CUSTOM, 1.0);
CfgFloat cfg_BombusLarvalA("BOMBUS_LARVALA", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusLarvalW("BOMBUS_LARVALW", CFG_CUSTOM, 2.7);
CfgFloat cfg_BombusLarvalBWax("BOMBUS_LARVALBWAX", CFG_CUSTOM, 4000.0);
CfgFloat cfg_BombusLarvalvWax("BOMBUS_LARVALVWAX", CFG_CUSTOM, 1.0);
CfgFloat cfg_BombusLarvaGyneDMcutoff("BOMBUS_LARVAGYNEDMCUTOFF", CFG_CUSTOM, 25.0);
CfgFloat cfg_BombusMaxMass("BOMBUS_MAXMASS", CFG_CUSTOM, 1400.0);
CfgFloat cfg_BombusMinMass("BOMBUS_MINMASS", CFG_CUSTOM, 1.5);
CfgFloat cfg_BombusGrowthG("BOMBUS_GROWTHG", CFG_CUSTOM, 1.5);
CfgInt cfg_BombusMaxAgeWorkerMakeWax("BOMBUS_MAXAGEWORKERMAKEWAX", CFG_CUSTOM, 7);
CfgFloat cfg_BombusInsulation("BOMBUS_INSULATION", CFG_CUSTOM, 1.0);
CfgFloat cfg_BombusAdultColWarmCoef("BOMBUS_ADULTCOLWARMCOEF", CFG_CUSTOM, 0.26928);
CfgBool cfg_BombusFeedLargestFirst("BOMBUS_FEEDLARGESTFIRST", CFG_CUSTOM, true);
CfgInt cfg_BombusWorkerEncounteredInStep("BOMBUS_WORKERENCOUNTEREDINSTEP", CFG_CUSTOM, 1);
CfgInt cfg_BombusDominationThreshold("BOMBUS_DOMINATIONTHRESHOLD", CFG_CUSTOM, 1);
CfgFloat cfg_BombusOvaryShrinkage("BOMBUS_OVARYSHRINKAGE", CFG_CUSTOM, 0.00005);
CfgFloat cfg_BombusMaxPropOvaries("BOMBUS_MAXPROPOVARIES", CFG_CUSTOM, 0.04);
CfgFloat cfg_BombusMinAgeWokersCompete("BOMBUS_MINAGEWOKERSCOMPETE", CFG_CUSTOM, 14400.0);
CfgFloat cfg_BombusDigestionRate("BOMBUS_DIGESTIONRATE", CFG_CUSTOM, 0.05);
CfgFloat cfg_BombusSugarDigestionRate("BOMBUS_SUGARDIGESTIONRATE", CFG_CUSTOM, 0.0082);

CfgFloat cfg_BombusInitialEggTemp("BOMBUS_INITIALEGGTEMP", CFG_CUSTOM, 32.0);
CfgStr cfg_BombusLarvaDevControl("BOMBUS_LARVADEVCONTROL", CFG_CUSTOM, "toLarvaDev_WorkerNo");
CfgFloat cfg_BombusAgeMortalityMultiQueen("BOMBUS_AGEMORTALITYMULTIQUEEN", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusAgeMortalityMultiGyne("BOMBUS_AGEMORTALITYMULTIGYNE", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusAgeMortalityMultiWorker("BOMBUS_AGEMORTALITYMULTIWORKER", CFG_CUSTOM, 0.0);
CfgFloat cfg_BombusAgeMortalityMultiMales("BOMBUS_AGEMORTALITYMULTIMALE", CFG_CUSTOM, 0.0);

/**
Based on the idea that Bombus eggs take between 3 and 4 days to hatch \cite <Smith2020>
which 20DD or 28800DM and at perfect developement temperature of 30C is approximatly 3.1 days*/
static CfgFloat cfg_BombusEggDevelTotalDM("BOMBUS_EGGDM", CFG_CUSTOM, 28800.0);

/**
Haploid males hang around their birth colony for a bit, before heading off. I need to look this up.
*/
static CfgFloat cfg_BombusMaleLeaving("BOMBUS_HAPMALELEAVING", CFG_CUSTOM, 0.5);

/**
The default value is based on 71% of eggs surviving from \cite <Smith2020> and \cite <Brian1951>
*/
/** \brief About 71% of eggs develope \cite <Smith2020>  \cite <Brian1951> */
static CfgFloat cfg_BombusEggMortality("BOMBUS_EGGSTEPMORT", CFG_CUSTOM, 0.000594424);
/**
This default mortality value, is derived from ~75% of larvae surviving to pupate \cite <Smith2020> and \cite <Brian1951>.
The number of days larva take to develope varies. They spending 2/3rds of larva + pupa time as a larva \cite <> so very approximatly 15 days.
Using 1-survival^(1/(days * 144)) = 0.000249693
I have read in /cite <free1959bumblebees> that mortality may be higher for larva at early stages, so maybe this should be adjusted.
*/
static CfgFloat cfg_BombusLarvaMortalitySlope("BOMBUS_LARVASTEPMORTSLOPE", CFG_CUSTOM, 0.0);
static CfgFloat cfg_BombusLarvaMortalityIntercept("BOMBUS_LARVASTEPMORTINTERCEPT", CFG_CUSTOM, 0.000249693);
/**
This default mortality value, is derived from ~90% of pupa surviving to adulthood \cite <Smith2020> and \cite <Brian1951>.
The number of days pupa take to develope varies. They spend 1/3rds of larva + pupa time as a pupa \cite <> so very approximatly 7 days.
Using 1-survival^(1/(days * 144)) = 0.00009145459872617060
*/
static CfgFloat cfg_BombusPupaMortality("BOMBUS_PUPASTEPMORT", CFG_CUSTOM, 0.00009145459872617060);
/** \brief For B.polaris & B. hyperboreus 57.1% and 37.2% and  62.5% and 41.0% mortality
*over I'm guessing 30 days \cite<Richard1973>.*/
static CfgFloat cfg_BombusWorkerMortality("BOMBUS_WORKERSTEPMORT", CFG_CUSTOM, 0.000163178);
static CfgFloat cfg_BombusGyneMortality("BOMBUS_GYNESTEPMORT", CFG_CUSTOM, 0.0001);
/** \brief For B.pascuorum 12% mortality over 7 days \cite<Carnell2020>.*/
static CfgFloat cfg_BombusQueenMortality("BOMBUS_QUEENSTEPMORT", CFG_CUSTOM, 0.00011096);
static CfgFloat cfg_BombusMaleMortality("BOMBUS_MALESTEPMORT", CFG_CUSTOM, 0.0001);

/**\brief Overly simplistic, but giving each adult lifestage an amount their mortality is affected by dead adults in the colony. */
static CfgFloat cfg_BombusWorkerDeadEffected("BOMBUS_WORKERDEADEFFECTED", CFG_CUSTOM, 0.000163178);
static CfgFloat cfg_BombusGyneDeadEffected("BOMBUS_GYNEDEADEFFECTED", CFG_CUSTOM, 0.0001);
static CfgFloat cfg_BombusQueenDeadEffected("BOMBUS_QUEENDEADEFFECTED", CFG_CUSTOM, 0.00011096);
static CfgFloat cfg_BombusMaleDeadEffected("BOMBUS_MALEDEADEFFECTED", CFG_CUSTOM, 0.0001);


/**
Colonies have "mortality" they are destroyed by mice and other forms of predation. I don't have a gigure on this yet, so I have added a small mortality.

*/
static CfgFloat cfg_BombusColonyMortality("BOMBUS_COLONYSTEPMORT", CFG_CUSTOM, 0.0);


double Bombus_Colony::FedNectarPerDay = 0.0;
double Bombus_Colony::FedSugarProp = 0.0;
double Bombus_Colony::FedPollenPerDay = 0.0;
TTypeOfLarvaDevControl Bombus_Colony::LarvaDevControl = toLarvaDev_Foo;
double Bombus_Colony::LarvalDMmin = 0.0;
double Bombus_Colony::LarvalDMmax = 0.0;
double Bombus_Colony::LarvalC = 0.0;
double Bombus_Colony::LarvalQ = 0.0;
double Bombus_Colony::LarvalB = 0.0;
double Bombus_Colony::Larvalv = 0.0;
double Bombus_Colony::LarvalA = 0.0;
double Bombus_Colony::LarvalW = 0.0;
double Bombus_Colony::LarvalBWax = 0.0;
double Bombus_Colony::LarvalvWax = 0.0;
double Bombus_Colony::ConstantTemp = 0.0;
vector<int> Bombus_Colony::m_FanningAtTemp = {};
vector<double> Bombus_Colony::m_MultipleDailyColonyTemps(1, 0.0);
double Bombus_Colony::ColonyInsulationCoef = 0.0;
double Bombus_Colony::AdultColWarmCoef = 0.0;
double Bombus_Colony::OptimumColTemp = 0.0;
double Bombus_Cluster::InitialEggTemp = 0.0;
double Bombus_Cluster::MinMass = 0.0;
double Bombus_Cluster::OptimumDevTemp = 0.0;
double Bombus_Base::B1EggCellPollen = 0.0;
double Bombus_Base::B2EggCellPollen = 0.0;
double Bombus_Base::B3EggCellPollen = 0.0;
double Bombus_Base::CallowForDM = 0.0;
double Bombus_Base::CallowMinTemp = 0.0;
double Bombus_Base::DigestionRate = 0.0;
double Bombus_Base::DominationThreshold = 0.0;
int Bombus_Base::EggMax = 0;
double Bombus_Base::EggMin = 0.0;
double Bombus_Base::EnergyDensityEgg = 0.0;
bool Bombus_Base::FeedLargestFirst = true;
double Bombus_Base::GrowthG = 0.0;
double Bombus_Base::LarvaGyneDMcutoff = 0.0;
double Bombus_Base::LarvalDMmax = 0.0;
double Bombus_Base::LarvalDMmin = 0.0;
double Bombus_Base::MaxMass = 0.0;
double Bombus_Base::MaxPropOvaries = 0.0;
double Bombus_Base::MinAgeWokersCompete = 0.0;
double Bombus_Base::MinColonyTempToLay = 0.0;
double Bombus_Base::MinJuvDevTemp = 0.0;
double Bombus_Base::MinMass = 0.0;
double Bombus_Base::OptimumColTemp = 0.0;
double Bombus_Base::OptimumDevTemp = 0.0;
double Bombus_Base::OvaryShrinkage = 0.0;
double Bombus_Base::PropDMmaxIsGyne = 0.0;
double Bombus_Base::RootIncubatingProb = 0.0;
double Bombus_Base::StartWorkerMass = 0.0;
double Bombus_Base::SugarDigestionRate = 0.0;

int Bombus_Base::AdultsEncounteredInStep = 0;
double Bombus_Base::WorkersEggCellPollen = 0.0;
bool Bombus_Base::AccessForaging = false;

TTypeOfColonyLocation Bombus_Base::ColonyLocation = toColonyLoc_Foo;
TTypeOfLarvaDevControl Bombus_Base::LarvaDevControl = toLarvaDev_Foo;
double Bombus_Base::LarvalMaleDM = 0.0;
int Bombus_Base::MaxAgeWorkerMakeWax = 0;
double Bombus_Base::PropBroodCare = 0.0;
double Bombus_Base::StepEnergyInspection = 0.0;
double Bombus_Base::waxConcCoef = 0.0;
double Bombus_Base::denominator = 1.0;
bool Bombus_Base::AdultsHomeotherms = false;
double Bombus_Base::AdultBodyDensity = 0.0;
double Bombus_Base::AdultHeatCapacityJperg = 0.0;
double Bombus_Base::AlphaOfAir = 0.0;
double Bombus_Base::BodyTempAdults = 0.0;

double Bombus_Egg::m_EggDM = 0.0;
double Bombus_Worker::m_EggDM = 0.0;
double Mass::MaxPropOvaries = 0.0;
int Mass::DominationThreshold = 0;
double Mass::OvaryShrinkage = 0.0;
double Mass::m_minMass = 0.0;
double Mass::m_maxMass;
vector<double> Mass::m_HomeothermEnergyCurve(0);
vector<double> Mass::m_poikilothermyEnergyCurve(0);

double Stomach::SugarDigestionRate = 0.0;
double Bombus_Colony::m_mortality = 0.0;
double Bombus_Male::m_ProbMaleLeaving = 0;
double Bombus_Colony::m_SoilTempToday = 0;
double Bombus_Colony::m_NightProp = 0;
double Bombus_Colony::m_DaylightPropToday = 0;
bool Mass::JuvenilesHomeotherms = false;
bool Mass::AdultsHomeotherms = false;
double Mass::m_EnergyOvaryMass = 0.0;
double Nectar::SugarDensity_mg_per_ul = 0.0;
double Nectar::EnergyPer_mgSugar = 0.0;
double Pollen::EnergyPer_mgProtein = 0.0;
double Pollen::EnergyPer_mgSugar = 0.0;
double Pollen::EnergyPer_mgFat = 0.0;
double Pollen::ProteinDensity_mg_per_ul = 0.0;
double Pollen::SugarDensity_mg_per_ul = 0.0;
double Pollen::FatDensity_mg_per_ul = 0.0;
double Pollen::AshDensity_mg_per_ml = 0.0;
double Food::EnergyPer_mgProtein = 0.0;
double Food::ProteinDensity_mg_per_ul = 0.0;
double Stomach::ConcentrationAdultAddToNectar = 0.0;
double Stomach::EnergyPer_mgProtein = 0.0;
double Stomach::MassPollenConcInNectar = 0.0;
double Stomach::ProteinDensity_mg_per_ul = 0.0;
double Stomach::EnergyDensityFlesh = 0.0;
double Bombus_Colony::PropProtein = 0.0;
double Bombus_Colony::PropSugar = 0.0;
double Bombus_Colony::PropFat = 0.0;
double Bombus_Cluster::ClusterLengthPerEgg = 0.0;
double Bombus_Cluster::ClusterWidthPerEgg = 0.0;
double Bombus_Cluster::ClusterHieght = 0.0;
double Bombus_Cluster::ClusterHeatCapacityJperg = 0.0;
double Bombus_Cluster::AlphaOfAir = 0.0;
double Bombus_Base::FanningEnergyPerMin = 0.0;
double Bombus_Larva::StomachCapacityPermgBodyWeight = 0.0;
double Bombus_Larva::EnergyDensityFlesh = 0.0;
double Bombus_Larva::PupaDMDivisionOfLarval = 0.0;
int Bombus_Larva::m_LarvaStageHardLimit = 0;

int Bombus_Worker::MaxSleep = 0;
int Bombus_Worker::MinSleep = 0;
double Bombus_Worker::StomachCapacityPermgBodyWeight = 0.0;




double Bombus_Worker::MaxCocoonMass = 0.0;
double Bombus_Worker::WaxEnergyDensity = 0.0;
double Bombus_Worker::DivisionEggAtDangerOfEaten = 0.0;
double Bombus_Worker::PropEggProtein = 0.0;
double Bombus_Worker::VolumeLarvalFeeding = 0.0;
int Bombus_Worker::m_WorkerNoLayDamageThreshold = 0;
double Bombus_Gyne::MinHibernationDuration = 0.0;
double Bombus_Gyne::HibernationWakeUpTemp = 0.0;
double Bombus_Queen::MassWaxFirstNectarPot = 0.0;
int Bombus_Queen::m_QueenNoLayDamageThreshold = 0;


double Bombus_Male::StomachCapacityPermgBodyWeight = 0.0;





vector<double> Bombus_Worker::m_AgeMortMultiVectWorker = {};
vector<double> Bombus_Gyne::m_AgeMortMultiVectGyne= {};
vector<double> Bombus_Queen::m_AgeMortMultiVectQueen = {};
vector<double> Bombus_Male::m_AgeMortMultiVectMale = {};


//********************************************************************************************************************************
//** Nectar class                                         
//********************************************************************************************************************************
void Nectar::AddNectarProp(double a_NectarVolume, double a_NectarSugarProp, double a_SugarDensity, double a_EnergyDensity)
{
	m_Volume.DepositQuantity(a_NectarVolume);
	double VolSugar = a_NectarVolume * a_NectarSugarProp;
	double MassSugar = VolSugar * a_SugarDensity;
	m_MassSugar.DepositQuantity(MassSugar);
	m_Energy.DepositQuantity(MassSugar * a_EnergyDensity);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Nectar::AddNectarQuantities(double a_NectarVolume, double a_MassSugar, double a_Energy)
{
	m_Volume.DepositQuantity(a_NectarVolume);
	m_MassSugar.DepositQuantity(a_MassSugar);
	m_Energy.DepositQuantity(a_Energy);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Nectar::AddNectarPacket(Nectar a_Packet)
{
	m_Volume.DepositQuantity(a_Packet.m_Volume.TakeAll());
	m_MassSugar.DepositQuantity(a_Packet.m_MassSugar.TakeAll());
	m_Energy.DepositQuantity(a_Packet.m_Energy.TakeAll());
}

//--------------------------------------------------------------------------------------------------------------------------------
Nectar Nectar::TakeNectar(double a_NectarVolume)
{
	double TotalNectar = m_Volume.ReportQuantity();
	double AvailableNect = m_Volume.TakeQuantity(a_NectarVolume);
	double Prop;
	if (TotalNectar > 0.0)
	{
		Prop = AvailableNect / TotalNectar;
	}
	else
	{
		Prop = 0.0;
	}
	double AvailableMassSugar = m_MassSugar.TakeProportion(Prop);
	double AvailableEnergy = m_Energy.TakeProportion(Prop);
	Nectar packet;
	packet.AddNectarQuantities(AvailableNect, AvailableMassSugar, AvailableEnergy);
	return packet;
}

//--------------------------------------------------------------------------------------------------------------------------------
Nectar Nectar::TakeNectarEqualsToSugar(double a_MassSugar)
{
	double TotalSugar = m_MassSugar.ReportQuantity();
	double AvailableMassSugar = m_MassSugar.TakeQuantity(a_MassSugar);
	double Prop = 0.0;
	if (TotalSugar > 0)
	{
		Prop = AvailableMassSugar / TotalSugar;
	}
	double AvailableNect = m_Volume.TakeProportion(Prop);
	double AvailableEnergy = m_Energy.TakeProportion(Prop);
	Nectar packet;
	packet.AddNectarQuantities(AvailableNect, AvailableMassSugar, AvailableEnergy);
	return packet;
}

//--------------------------------------------------------------------------------------------------------------------------------
Nectar Nectar::TakeNectarEqualsToEnergy(double a_Energy)
{
	double TotalEnergy = m_Energy.ReportQuantity();
	double AvailableEnergy = m_Energy.TakeQuantity(a_Energy);
	double Prop = 0.0;
	if (TotalEnergy > 0)
	{
		Prop = AvailableEnergy / TotalEnergy;
	}
	double AvailableMassSugar = m_MassSugar.TakeProportion(Prop);
	double AvailableNect = m_Volume.TakeProportion(Prop);
	Nectar packet;
	packet.AddNectarQuantities(AvailableNect, AvailableMassSugar, AvailableEnergy);
	return packet;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Nectar::SetStatic()
{
	SugarDensity_mg_per_ul = cfg_BombusSugarDensitymgperul.value();
	EnergyPer_mgSugar = cfg_BombusEnergyPermgSugar.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//** Pollen class                                         
//********************************************************************************************************************************


void Pollen::SetStatic()
{
	EnergyPer_mgProtein = cfg_BombusEnergyPermgProtein.value();
	EnergyPer_mgSugar = cfg_BombusEnergyPermgSugar.value();
	EnergyPer_mgFat = cfg_BombusEnergyPermgFat.value();
	ProteinDensity_mg_per_ul = cfg_BombusProteinDensitymgperul.value();
	SugarDensity_mg_per_ul = cfg_BombusSugarDensitymgperul.value();
	FatDensity_mg_per_ul = cfg_BombusFatDenstymgperul.value();
	AshDensity_mg_per_ml = cfg_BombusAshDensitymgperml.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//** Food class                                         
//********************************************************************************************************************************
void Food::AddNectarPacket(Nectar a_Packet)
{
	m_Volume.DepositQuantity(a_Packet.ReportVolume());
	m_MassSugar.DepositQuantity(a_Packet.ReportMassSugar());
	m_Energy.DepositQuantity(a_Packet.ReportEnergy());
}

//--------------------------------------------------------------------------------------------------------------------------------
void Food::SetStatic()
{
	EnergyPer_mgProtein = cfg_BombusEnergyPermgProtein.value();
	ProteinDensity_mg_per_ul = cfg_BombusProteinDensitymgperul.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//** Mass class                                         
//********************************************************************************************************************************
double Mass::CalculateMaintenanceEnergy()
{
	/** Alternativly to what is typically fed to larva from \cite<Ribeiro1999>, we can work out the energy budget*/

	bool homeotherm = false;
	switch (m_owner->ReportIdentity())
	{
	case to_Larva:
		if (JuvenilesHomeotherms)
		{
			homeotherm = true;
		}
		break;
	default:
		if (AdultsHomeotherms)
		{
			homeotherm = true;
		}
		break;
	}

	return GetEnergyNeeded(ReportLeanMass(), homeotherm);
}

void Mass::HardSetLeanMass(double a_Mass)
{
	m_LeanMass.ResetQuantity(a_Mass);
	m_MaxLeanMass.ResetQuantity(a_Mass);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Mass::DigestedFoodEffect(Food a_FoodPacket)
{
	double EnergyInFood = a_FoodPacket.ReportEnergy();
	double MaintenanceEnergy = CalculateMaintenanceEnergy();
	if (EnergyInFood <= MaintenanceEnergy)
	{
		//This can now be negative an denotes a deficit.
		EnergyInFood -= MaintenanceEnergy;
	}
	else
	{
		/** Pollen is important for health as well as growth. I have assumed some pollen
		will be used for maintenance and to give that health benefit. I may need to find
		a way to add in poor health with lack of protein. For now I wil add a counter of
		protein consumed.
		Add MaintenacePacket protein to tally of that used in life.
		*/
		m_ProteinEatenInLife += a_FoodPacket.TakeFoodEnergy(MaintenanceEnergy).ReportProteinMass();
		if (int(1000 * m_LeanMass.ReportQuantity()) == int(1000 * m_MaxLeanMass.
				ReportQuantity()) &&
			m_HasOvaries == true &&
			ReportPropOvaries() < MaxPropOvaries &&
			m_owner->ReportTimesDominated() < DominationThreshold &&
			!m_owner->GetCompetitionPoint()		
			)
		{
			/** Deposit FoodForOvaries = ProteinMassForOvaries within the mass of the ovaries */
			m_OvaryMass.DepositQuantity(a_FoodPacket.PacketAllProtein().ReportProteinMass());
		}
		EnergyInFood = a_FoodPacket.ReportEnergy();
	}
	/** Larva use additional energy to grow lean mass. Adults store fat.*/
	if (m_CapableStoreFat == false)
	{
		m_LeanMass.DepositQuantity(EnergyInFood / m_EnergyLeanMass);
		if (m_LeanMass.ReportQuantity() > m_MaxLeanMass.ReportQuantity())
		{
			m_MaxLeanMass.ResetQuantity(m_LeanMass.ReportQuantity());
			m_Developing = true;
		}
		else
		{
			/** This assumes that regaining condition having been food deprived is not developing.*/
			m_Developing = false;
		}
	}
	else
	{
		/** If the worker has encountered competition, its ovaries will shrink and premable the energy used.*/
		if (m_owner->ReportTimesDominated() >= DominationThreshold && m_owner->ReportIdentity() == to_Worker)
		{
			EnergyInFood += m_OvaryMass.TakeProportion(OvaryShrinkage) * m_EnergyOvaryMass;
		}
		/** If surplus energy store.
		Else remove from store the deficit.*/
		if (EnergyInFood >= 0)
		{
			double FoodEqualLeanMass = EnergyInFood / m_EnergyLeanMass;
			double LeanMassDeficit = m_MaxLeanMass.ReportQuantity() - m_LeanMass.ReportQuantity();
			/** If there is less energy left in the food than the deficit in lean mass, put all energy into lean mass.
			Otherwise restore the lean mass, and put the rest into fat.*/
			if (FoodEqualLeanMass <= LeanMassDeficit)
			{
				m_LeanMass.DepositQuantity(FoodEqualLeanMass);
			}
			else
			{
				EnergyInFood -= LeanMassDeficit * m_EnergyLeanMass;
				m_LeanMass.ResetQuantity(m_MaxLeanMass.ReportQuantity());
				m_FatMass.DepositQuantity(EnergyInFood / m_EnergyFatMass);
			}
		}
		else
		{
			EnergyInFood = EnergyInFood * -1.0;
			double EnergyFromFat = m_FatMass.TakeQuantity(EnergyInFood / m_EnergyFatMass) * m_EnergyFatMass;
			EnergyInFood -= EnergyFromFat;
			double EnergyFromOvaries = m_OvaryMass.TakeQuantity(EnergyInFood / m_EnergyOvaryMass) *
				m_EnergyOvaryMass;
			EnergyInFood -= EnergyFromOvaries;
			m_LeanMass.TakeQuantity(EnergyInFood / m_EnergyLeanMass);  
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Mass::HardSetOvaryProp(double a_OvaryPropBody)
{
	if (m_HasOvaries == true)
	{
		if (a_OvaryPropBody > MaxPropOvaries)
		{
			a_OvaryPropBody = MaxPropOvaries;
		}
		m_OvaryMass.ResetQuantity(m_LeanMass.ReportQuantity() * (a_OvaryPropBody / (1 - a_OvaryPropBody)));
	}
}


double Mass::GetEnergyNeeded(double a_mass, bool a_homeotherm)
{
	if(a_mass < m_minMass)
	{
		a_mass = m_minMass;
	}

	double precision = 1000.0;
	int intMass = int((a_mass - m_minMass) * precision);
	
	if(a_homeotherm)
	{
		if(intMass >  int(m_HomeothermEnergyCurve.size()))
		{
			g_msg->Warn("Mass::GetEnergyNeeded - Individual with mass larger than maximum trying to calc energy need. ", a_mass);
			exit(1); // NOLINT(concurrency-mt-unsafe)
		}

		return m_HomeothermEnergyCurve[intMass];
	}

	if(intMass >  int(m_poikilothermyEnergyCurve.size()))
		{
			g_msg->Warn("Mass::GetEnergyNeeded - Individual with mass larger than maximum trying to calc energy need. ", a_mass);
			exit(1); // NOLINT(concurrency-mt-unsafe)
		}

	return m_poikilothermyEnergyCurve[intMass];
}

//--------------------------------------------------------------------------------------------------------------------------------
void Mass::SetStatic()
{
	MaxPropOvaries = cfg_BombusMaxPropOvaries.value();
	DominationThreshold = cfg_BombusDominationThreshold.value();
	OvaryShrinkage = cfg_BombusOvaryShrinkage.value();
	m_EnergyOvaryMass = cfg_BombusEnergyDensityOvaries.value();
	AdultsHomeotherms = cfg_BombusAdultsHomeotherms.value();
	JuvenilesHomeotherms = cfg_BombusJuvinilessHomeotherms.value();
	m_maxMass = cfg_BombusMaxMass.value();
	m_minMass = cfg_BombusMinMass.value();


	if(AdultsHomeotherms || JuvenilesHomeotherms)
	{
		m_HomeothermEnergyCurve = PreCalculateEnergyPowCurve(m_minMass, m_maxMass, true);
	}

	if(!AdultsHomeotherms || !JuvenilesHomeotherms)
	{
		m_poikilothermyEnergyCurve = PreCalculateEnergyPowCurve(m_minMass, m_maxMass, false);
	}


	
	

}

//--------------------------------------------------------------------------------------------------------------------------------

//********************************************************************************************************************************
//** Other functions                                     
//********************************************************************************************************************************

bool CompareMass(Bombus_Worker* a_Female1, Bombus_Worker* a_Female2)
{
	return (a_Female1->GetTotalMass() > a_Female2->GetTotalMass());
}

//--------------------------------------------------------------------------------------------------------------------------------
bool CompareMassLarva(Bombus_Larva* a_Larva1, Bombus_Larva* a_Larva2)
{
	return (a_Larva1->GetTotalMass() > a_Larva2->GetTotalMass());
}

vector<double> CalculateExpCurve(double a_a, double a_b, int a_Length)
{
	vector<double> vector_to_return;
	vector_to_return.reserve(a_Length);
	for(int x = 0; x < a_Length; x++)
	{
		vector_to_return.push_back(a_a * exp(a_b * x));
	}
	return vector_to_return;

}

vector<int> CalculateExpCurveInt(double a_a, double a_b, int a_Length)
{
	vector<double> doubleVector = CalculateExpCurve(a_a, a_b, a_Length);
	vector<int> vector_to_return;
	vector_to_return.reserve(a_Length);
	for (auto& a_value : doubleVector )
	{
		vector_to_return.push_back(int(round(a_value)));
	}
	return vector_to_return;
}

vector<double> PreCalculateEnergyPowCurve(double a_min, double a_max, bool a_homeotherm)
{

	double precision = 1000.0;

	int minMass = int(a_min * precision);
	int maxMass = int(a_max * precision);

	int VectLength = maxMass - minMass;

	vector<double> vector_to_return;
	vector_to_return.reserve(VectLength);

	if(a_homeotherm)
	{
		for (int x = 0; x <= VectLength; x++)
			{
		
			vector_to_return.push_back(600 * 4.1 * pow(((x + minMass) / (1000000.0 * precision)), 0.751));

			
		}
	}
	else
	{
		for (int x = 0; x <= VectLength; x++)
		{
		
			vector_to_return.push_back(600 * 0.14 * pow(((x + minMass) / (1000000.0 * precision)), 0.751));
			
		}
		
	}


	return vector_to_return;
}


//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfColonyLocation StringToLocation(string a_str)
{
	//map<string, TTypeOfColonyLocation> table = map_list_of("toColonyLoc_BoxLab", TTypeOfColonyLocation::toColonyLoc_BoxLab)("toColonyLoc_Underground", TTypeOfColonyLocation::toColonyLoc_Underground);
	unordered_map<string, TTypeOfColonyLocation> table = {
		{"toColonyLoc_BoxLab", toColonyLoc_BoxLab},
		{"toColonyLoc_Underground", toColonyLoc_Underground}
	};
	auto it = table.find(a_str);
	if (it != table.end())
	{
		return it->second;
	}
	return toColonyLoc_Foo;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfLarvaDevControl StringToControl(string a_str)
{
	//map<string, TTypeOfColonyLocation> table = map_list_of("toColonyLoc_BoxLab", TTypeOfColonyLocation::toColonyLoc_BoxLab)("toColonyLoc_Underground", TTypeOfColonyLocation::toColonyLoc_Underground);
	unordered_map<string, TTypeOfLarvaDevControl> table = {
		{"toLarvaDev_QueenStep", toLarvaDev_QueenStep},
		{"toLarvaDev_WaxSignal", toLarvaDev_WaxSignal},
		{"toLarvaDev_WorkerNo", toLarvaDev_WorkerNo}
	};
	auto it = table.find(a_str);
	if (it != table.end())
	{
		return it->second;
	}
	return toLarvaDev_Foo;
}

//--------------------------------------------------------------------------------------------------------------------------------


//********************************************************************************************************************************
//*
//**                                          Bombus_Colony
//**
//********************************************************************************************************************************
/**
 * \brief The Bombus colony has been created as TAnimal to allow colony wide action to be taken. 
 * \param a_x Horizontal location of colony in landscape. 
 * \param a_y Vertical location of colony in landscape. 
 * \param p_L Landscape manager.
 * \param p_BPM Bombus population manager.
 * \param a_ColonyID Colony ID, set the same as Queen. 
 * \param a_location Location of colony; lab, underground, on surface, above ground. 
 */
Bombus_Colony::Bombus_Colony(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM,// NOLINT(cppcoreguidelines-pro-type-member-init)
                             unsigned long a_ColonyID, TTypeOfColonyLocation a_location) : TAnimal(a_x, a_y, p_L)
{
	Init(a_x, a_y, p_L, p_BPM, a_ColonyID, a_location);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Colony::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_ColonyID,
                           TTypeOfColonyLocation a_location)
{
	TAnimal::ReinitialiseObject(a_x, a_y, p_L);
	Init(a_x, a_y, p_L, p_BPM, a_ColonyID, a_location);
}
//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Colony::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_ColonyID,
                         TTypeOfColonyLocation a_location)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = p_BPM;
	m_OurLandscape = p_L;
	m_CurrentBoState = toBombusS_InitialState;
	m_ColonyLoc = a_location;
	/**
	* Constructor needs to initiate starting values. This is done by Init - which is shared with ReiInit
	*/
	m_ColonyTemp = BackgroundTemp();
	m_Nectar = Nectar();
	/** Add the amount of nectar for the day.*/
	m_Nectar.AddNectarProp(FedNectarPerDay, FedSugarProp);
	m_Pollen.AddPollenWithProp(FedPollenPerDay, PropProtein, PropSugar, PropFat);



	/** The larval degree days increases with the "age" of the colony. Age of colony is represented here by number of workers*/
	if (LarvaDevControl == toLarvaDev_WorkerNo)
	{
		LarvalDM = LarvalDMmin + (
			(LarvalDMmax - LarvalDMmin) /
			(
				pow((LarvalC + LarvalQ * exp(-LarvalB * NoAdults)), (1.0 / Larvalv))
			)
		);
	}
	else if (LarvaDevControl == toLarvaDev_WaxSignal)
	{
		LarvalDM = LarvalDMmin + (
			(LarvalDMmax - LarvalDMmin) /
			(pow(
					(LarvalA + pow(LarvalW, LarvalBWax * ReportChemConc())),
					(1.0 / LarvalvWax))
			)
		);
	}
	m_wax = m_QWax = m_WWax = m_MassColony = 0.0;
	m_ColonyID = a_ColonyID;

	m_WarmBodies = 0;
	m_SumWarmBodies = 0.0;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Colony::SetStatic()
{
	/**The next three lines set the temperature in the lab colonies for a number of days (after which the temperature remains the last temperature supplied).*/
	int DaysOfTemp = cfg_BombusDaysOfTemp.value();
	CfgArray_Double cfg_BombusDayTemps("BOMBUS_DAYTEMPS", CFG_CUSTOM, DaysOfTemp, vector<double>{28.0});
	m_MultipleDailyColonyTemps = cfg_BombusDayTemps.value();


	CfgFloat cfg_BombusFanningExp_a("BOMBUS_FANNINGEXP_A", CFG_CUSTOM, 0.000003);
	CfgFloat cfg_BombusFanningExp_b("BOMBUS_FANNINGEXP_B", CFG_CUSTOM, 0.4529);

	/** \brief Calculate the number that would be needed to fan the colony at different temperatures. b is divided by 10, because temperature is multiplied by 10 to
	 * remove decimal place. 
	 */
	m_FanningAtTemp = CalculateExpCurveInt(cfg_BombusFanningExp_a.value(), cfg_BombusFanningExp_b.value() / 10.0, 600);


	FedNectarPerDay = cfg_BombusFedNectarPerDay.value();
	FedSugarProp = cfg_BombusFedSugarProp.value();
	FedPollenPerDay = cfg_BombusFedPollenPerDay.value();
	LarvaDevControl = StringToControl(cfg_BombusLarvaDevControl.value());
	LarvalDMmin = cfg_BombusLarvalDMmin.value();
	LarvalDMmax = cfg_BombusLarvalDMmax.value();
	LarvalC = cfg_BombusLarvalC.value();
	LarvalQ = cfg_BombusLarvalQ.value();
	LarvalB = cfg_BombusLarvalB.value();
	Larvalv = cfg_BombusLarvalv.value();
	LarvalA = cfg_BombusLarvalA.value();
	LarvalW = cfg_BombusLarvalW.value();
	LarvalBWax = cfg_BombusLarvalBWax.value();
	LarvalvWax = cfg_BombusLarvalvWax.value();
	ColonyInsulationCoef = cfg_BombusInsulation.value();
	AdultColWarmCoef = cfg_BombusAdultColWarmCoef.value();
	OptimumColTemp = cfg_BombusOptimumColTemp.value();
	PropProtein = cfg_BombusPropProtein.value();
	PropSugar = cfg_BombusPropSugar.value();
	PropFat = cfg_BombusPropFat.value();
	m_mortality = cfg_BombusColonyMortality.value();

	/** Error checking on cfg input values.*/
	if (LarvaDevControl == toLarvaDev_WorkerNo && Larvalv == 0.0)
	{
		m_OurLandscape->Warn("Bombus_Colony::Init()", "Larvalv cannot be zero.");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}

	if (LarvaDevControl == toLarvaDev_WorkerNo && Larvalv == 0.0)
	{
		m_OurLandscape->Warn("Bombus_Colony::Init()", "Larvalv cannot be zero.");
		std::exit(1); // NOLINT(concurrency-mt-unsafe) // NOLINT(concurrency-mt-unsafe)
	}
}

bool Bombus_Colony::FirstNectarCellComplete()
{
	if(NectarCell >= 1)
	{
		return true;
	}
	return false;
}


//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Colony::BeginStep()
{
	if (NoAdults == 0 || g_rand_uni() < m_mortality || m_AgeColony >= cfg_BombusColonyDeathdays.value())
	{
		m_StepDone = true;
		SetCurrentStateNo(-1);
		return;
	}

	m_ColonyTemp = CalculateColonyTemp();
	if (ColonyTimeStep % 144 == 0)
	{
		/** The larval degree days increases with the "age" of the colony. Age of colony is represented here by number of workers*/
		//Moved into once a day, as they won't change that quickly.
		if (LarvaDevControl == toLarvaDev_WorkerNo)
		{
			LarvalDM = LarvalDMmin + (
				(LarvalDMmax - LarvalDMmin) /
				(
					pow((LarvalC + LarvalQ * exp(-LarvalB * NoAdults)), (1.0 / Larvalv))
				)
			);
		}
		else if (LarvaDevControl == toLarvaDev_WaxSignal)
		{
			LarvalDM = LarvalDMmin + (
				(LarvalDMmax - LarvalDMmin) /
				(pow(
						(LarvalA + pow(LarvalW, LarvalBWax * ReportChemConc())),
						(1.0 / LarvalvWax))
				)
			);
		}

		/** Remove any old nectar.*/
		m_Nectar.TakeNectar(FedNectarPerDay);
		/** Add the amount of nectar for the day.*/
		m_Nectar.AddNectarProp(FedNectarPerDay, FedSugarProp);
		m_Pollen.TakePollenMass(FedPollenPerDay);
		m_Pollen.AddPollenWithProp(FedPollenPerDay, PropProtein, PropSugar, PropFat);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Colony::Step()
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState:
		m_CurrentBoState = toBombusS_InitialState;
		m_StepDone = true;
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Colony::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Colony::EndStep()
{
	/** Increases a colony step counter*/
	ColonyTimeStep++;
	/** Increase colony age by a day, once a day. */
	if (ColonyTimeStep % 144 == 0)
	{
		m_AgeColony += 1;
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
double Bombus_Colony::BackgroundTemp()
{
	double BackGroundTemp;
	switch (m_ColonyLoc)
	{
	case toColonyLoc_Underground:
		BackGroundTemp = GetSoilTemp();
		break;
	case toColonyLoc_Surface:
		BackGroundTemp = (GetSoilTemp() + GetOutsideTemp()) / 2.0;
		break;
	case toColonyLoc_AboveGround:
	/** These may actually be cooler than air temp depending on where the bumblebee has chosen as home. It could also be 2C warmer \cite<Odonnell2001>*/
	case toColonyLoc_BoxField:
		/** These may actually be warmer than air temp depending on where they have been stuck.*/
		BackGroundTemp = m_TempToday;
		break;
	case toColonyLoc_BoxLab:
		/** \cite<Beheshti2013> but I think I have read 27C and 30C, would be good if this could be set with a constant.*/
		BackGroundTemp = ConstantTemp;
		break;
	default:
		g_msg->Warn("Incorrect location for colony specified. ", m_ColonyLoc);
		exit(1); // NOLINT(concurrency-mt-unsafe)
	}
	return BackGroundTemp;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Colony::st_Dying()
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
	return toBombusS_Die;
}

void Bombus_Colony::SetLabTemp()
{
	int day = m_OurLandscape->SupplyDayInYear();
	if (day < int(m_MultipleDailyColonyTemps.size()))
	{
		ConstantTemp = m_MultipleDailyColonyTemps[day];
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
double Bombus_Colony::CalculateColonyTemp()
{
	/** The temperature of the colony is not only influenced by the background temperature, if the colony has been warmed up then it should be warmer
	than the background*/
	double YesterdaysColonyTemp = m_ColonyTemp;

	//***
	//This determines the ability to HEAT the colony.
	//***


	//double NewTemp = slopeNumber * double(NoAdults) + InsulatedColonyTemp;

	/** Using supplementary data from \cite<Wynants2021> phen12350-sup-0004-supinfos4.xlsx ran glm analysis in R

	The temperature in the colony should be something like:

	ColonyTempToday = ColonyTempYesterday + (AmbientTemp - ColonyTempYesterday)*exp( -c * surfaceAreaColony / MassColony) + m*NoAdults

	That is, the temperature is changed by heat gained or lost to the ambient temperature outside of the colony in relation to its surface area and mass with
	c = AlphaOfSurrounding / heatCapacityColony and the heat is also changed by the adults body temperature (lost to colony and in incubating).

	There is also amount lost by fanning adults (exchanging the air in the colony with that out). But here I will ignore this when the colony is below optimum.

	Unfortunatly in the \cite<Wynants2021>  data we do not know the area or mass of the colony. Area is constant, but mass will increase cubically with the number of
	juviniles and adults in the colony increases.

	Therefore:
	ColonyTempToday = ColonyTempYesterday + (AmbientTemp - ColonyTempYesterday)*exp( -c / NoAdults^3) + m*NoAdults

	This is fine, until adults start dying. The mass added in their accumulation (wax) will not be lost, and unless they are thrown out their mass is not lost either.

	The mass of the colony is not currently calculated.
	ColonyTempToday = ColonyTempYesterday + (AmbientTemp - ColonyTempYesterday) * exp( -c / ColonyMass) + m * NoAdults


	We could assume that no matter the mass of the colony, over a whole day the colony would end up at ambient without the workers. Therefore:

	ColonyTempToday = AmbientTemp + m * WarmingNoAdults - n * FanningAdults

	using Wynants data this is something like:

								Estimate Std. Error t value Pr(>|t|)
	MinusFan                      0.2534     0.0276   9.181   <2e-16 ***
	`Number of fanning workers`   1.8621     1.1415   1.631    0.105

	ColonyTempToday = AmbientTemp +  0.2534  * WarmingNoAdults + 1.8621 * FanningAdults

	With the fanning making no sense. Without fanning:

	ColonyTempToday = AmbientTemp +  0.26928 * NoAdults

	In addition adding (AmbientTemp - ColonyTempYesterday), this term is also insignificant.

	In all cases their is a clearly linear relationship of the residuals with temperature. This means a non-linear relationship is more appropriate.
	This is probably related to adults switching from warming to fanning as the colony nears optimum.


	To allow insulation to be taken into account later (queens in the wild must be able to insulate the colony or the species wouldn't persist).



	YesterdaysColonyTemp + ColonyInsulationCoef * (BackgroundTemp() - YesterdaysColonyTemp)  + AdultColWarmCoef * NoAdults;

	From the \cite<Wynants2021> data:
	ColonyInsulationCoef ~ 0.07271
	AdultColWarmCoef ~ 0.27676

	but setting ColonyInsulationCoef = 1, is no insulation and becomes the one above.
	ColonyInsulationCoef = 1
	AdultColWarmCoef ~ 0.26928
	*/

	double NewTemp = YesterdaysColonyTemp + ColonyInsulationCoef * (BackgroundTemp() - YesterdaysColonyTemp);


	double MeanAdultTemp =  m_SumWarmBodies / double(m_WarmBodies);

	/** Adults can not raise the temperature about their maximum.*/
	if(NewTemp < MeanAdultTemp)
	{
		NewTemp += AdultColWarmCoef * m_SumWarmBodies / cfg_BombusBodyTempAdults.value();
		if(NewTemp > MeanAdultTemp)
		{
			NewTemp = MeanAdultTemp;
		}
	}

	m_SumWarmBodies = 0;
	m_WarmBodies = 0;


	double InsulatedColonyTemp = NewTemp;

	//***
	//This determines the ability to COOL the colony.
	//***
	m_AdultsFanning = m_FanningAtTemp[int(round(InsulatedColonyTemp * 10.0))];
	

	/** The number of females currently assigned to fan. This will be increased*/
	int FanningAdults = 0;
	/** Set the largest workers in the colony to fanning.*/
	if (m_AdultsFanning > 0)
	{

		sort(m_ActiveColonyWorkers.begin(), m_ActiveColonyWorkers.end(), CompareMass);
		int NeededToFan = m_AdultsFanning;
		/**
		Loop through the workers, and set them to fanning if they are alive.
		Ask if they start fanning and if this is true record that they are fanning.
		If we have all the workers fanning we need break the while statement.
		*/
		auto w = m_ActiveColonyWorkers.begin();
		while (w < m_ActiveColonyWorkers.end())
		{
			if ((*(w))->SetFanning())
			{
				NeededToFan--;
				FanningAdults++;
			}
			++w;

			if (NeededToFan == 0)
			{
				break;
			}
		}

		if(NeededToFan > 0){

			auto g = m_ActiveColonyGynes.begin();
			while (g < m_ActiveColonyGynes.end())
			{
				if ((*(g))->SetFanning())
				{
					NeededToFan--;
					FanningAdults++;
				}
				++g;

				if (NeededToFan == 0)
				{
					break;
				}
			}
		}


		if(NeededToFan > 0){

			auto m = m_ActiveColonyMales.begin();
			while (m < m_ActiveColonyMales.end())
			{
				if ((*(m))->SetFanning())
				{
					NeededToFan--;
					FanningAdults++;
				}
				++m;

				if (NeededToFan == 0)
				{
					break;
				}
			}
		}
		//if(NeededToFan > 0){
		//	m_ColonyQueen->SetFanning();
		//	NeededToFan--;
		//	FanningAdults++;
		//}

	}

	/** NoAdults is probably not correct, this should actually be the number of workers available to cool the colony. */
	if (FanningAdults == 0)
	{
		return NewTemp;
	}
	if (FanningAdults == m_AdultsFanning && NewTemp >= OptimumColTemp)
	{
		return OptimumColTemp;
	}

	if (NewTemp > OptimumColTemp)
	{
		NewTemp -= ((NewTemp - OptimumColTemp) * FanningAdults / m_AdultsFanning);
	}

	return NewTemp;
}

//--------------------------------------------------------------------------------------------------------------------------------
/** \brief Removes a bee object from the cluster */
bool Bombus_Colony::RemoveCluster(Bombus_Cluster* a_cluster)
{
	/**
	* Returns true if successful and false if the object is not found
	*/
	auto end = m_ColonyClusters.end();
	for (auto i = m_ColonyClusters.begin(); i < end; ++i)
	{
		if (*(i) == a_cluster)
		{
			m_ColonyClusters.erase(i);
			return true;
		}
	}
	return false; // An error because the a_bee pointer was not found
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Colony::RemoveAdult(Bombus_Base* a_Adult)
{

	return a_Adult->RemoveSelfFromColony();

}

//********************************************************************************************************************************
//**
//********************************************Bombus_Cluster Definition **********************************************************
//**
//********************************************************************************************************************************
void Bombus_Cluster::AddPollenLump(double a_MassPollen)
{
	Pollen SomePollen = m_OurColony->m_Pollen.TakePollenMass(a_MassPollen);
	m_Food.AddFood(
		SomePollen.ReportMassSugar(),
		SomePollen.ReportMassFattyAcids(),
		SomePollen.ReportProteinMass(),
		SomePollen.ReportEnergy(),
		SomePollen.ReportVolume()
	);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Cluster::EatAllClusterEggs()
{
	if (!m_ClusterEggs.empty())
	{
		int i = 0;
		while (i != 1000)
		{
			auto iegg = m_ClusterEggs.begin();
			(*iegg)->Eaten();
			if (m_ClusterEggs.empty())
			{
				break;
			}
			i++;
		}
	}
}

void Bombus_Cluster::KillThis()
{
	if (!m_ClusterEggs.empty())
	{
		auto end = m_ClusterEggs.end();
		for (auto i = m_ClusterEggs.begin(); i != end; ++i)
		{
			(*i)->KillThis();
		}
	}
	if (!m_ClusterLarva.empty())
	{
		auto end = m_ClusterLarva.end();
		for (auto i = m_ClusterLarva.begin(); i != end; ++i)
		{
			(*i)->KillThis();
		}
	}
	if (!m_ClusterPupa.empty())
	{
		auto end = m_ClusterPupa.end();
		for (auto i = m_ClusterPupa.begin(); i != end; ++i)
		{
			(*i)->KillThis();
		}
	}

	m_CurrentStateNo = -1;
	m_StepDone = true;
}

bool Bombus_Cluster::IsDead()
{
	if (m_CurrentStateNo == -1)
	{
		return true;
	}
	return false;
}

bool Bombus_Cluster::GetIfNotFocusedOn()
{
	if(m_FocusedOnBy == nullptr)
	{
		return true;
	}
	return false;

	//return !static_cast<bool>(m_FocusedOnBy);
}

bool Bombus_Cluster::GetIfFocusedOn()
{
	if(m_FocusedOnBy == nullptr)
	{
		return false;
	}
	return true;

	//return static_cast<bool>(m_FocusedOnBy);
}

//--------------------------------------------------------------------------------------------------------------------------------
/**
 * \brief Clusters are specified as TAnimal to allow clusters of juveniles to be managed collectively. 
 * \param a_x Horizontal location of colony in landscape. 
 * \param a_y Vertical location of colony in landscape. 
 * \param p_L Landscape manager.
 * \param p_BPM Bombus population manager.
 * \param a_NumberEggs Number of eggs added to cluster.
 * \param a_Colony A pointer to the colony the cluster is in.
 */
Bombus_Cluster::Bombus_Cluster(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberEggs,  // NOLINT(cppcoreguidelines-pro-type-member-init)
                               Bombus_Colony* a_Colony) : TAnimal(a_x, a_y, p_L)
{
	Init(a_x, a_y, p_L, p_BPM, a_NumberEggs, a_Colony);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Cluster::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberEggs,
                            Bombus_Colony* a_Colony)
{
	TAnimal::ReinitialiseObject(a_x, a_y, p_L);
	Init(a_x, a_y, p_L, p_BPM, a_NumberEggs, a_Colony);
}
//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Cluster::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberEggs,
                          Bombus_Colony* a_Colony)
{
	m_CurrentBoState = toBombusS_InitialState;
	m_Food.TakeFoodVol(m_Food.ReportVolume());
	m_ClusterEggs.resize(0);
	m_ClusterLarva.resize(0);
	m_ClusterEggs.resize(0);
	m_Age = 0;
	m_OurColony = a_Colony;
	/** Set the temperature of the new cluster to that of the queens internal temp. This seems to make most sense, as the queen
	has just made the cluster with wax from her body, or bitten of and recycled and then laid body temp eggs in the cell. */
	m_temp = InitialEggTemp;
	m_OurColony->m_ColonyClusters.push_back(this);
	m_EggMass = a_NumberEggs * MinMass;
	timestep = 0;
	m_wax = 0.0;
	m_JuvMass = 0.0;
	m_PupaMass = 0.0;

	m_ClusterLengthmm = ClusterLengthPerEgg * a_NumberEggs;
	m_ClusterWidthmm = ClusterWidthPerEgg * a_NumberEggs;
	m_ClusterHeighthmm = ClusterHieght;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Cluster::st_Dying()
{
	/** Removes cluster from its colony*/
	m_OurColony->RemoveCluster(this);
	m_OurColony = nullptr;
	m_ClusterEggs.clear();
	m_ClusterLarva.clear();
	m_ClusterPupa.clear();
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Cluster::st_Warm()
{
	return toBombusS_Warm;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Cluster::st_Cool()
{
	/** T(t) = Ts + (T0 - Ts)*e^-kt; Ts = ambient (colony temp); T0 = starting temp (optimal?)
	*/
	double Ts = m_OurColony->GetColonyTemp();
	m_temp = Ts + (m_temp - Ts) * exp(-Get_k() * 10.0 / 60.0);
	return toBombusS_Cool;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Cluster::SetStatic()
{
	InitialEggTemp = cfg_BombusInitialEggTemp.value();
	MinMass = cfg_BombusMinMass.value();
	OptimumDevTemp = cfg_BombusOptimumDevTemp.value();
	ClusterLengthPerEgg = cfg_BombusClusterLengthPerEgg.value();
	ClusterWidthPerEgg = cfg_BombusClusterWidthPerEgg.value();
	ClusterHieght = cfg_BombusClusterHieght.value();
	ClusterHeatCapacityJperg = cfg_BombusClusterHeatCapacityJperg.value();
	AlphaOfAir = cfg_BombusAlphaOfAir.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Cluster::Step()
{
	//Not sure what the cluster needs to do, but it needs to get out of the way at the moment!
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState: // The cluster is cooling unless it is being incubated.
		m_CurrentBoState = toBombusS_Cool;
		break;
	case toBombusS_Cool:
		m_CurrentBoState = st_Cool();
		m_StepDone = true;
		break;
	case toBombusS_Warm:
		m_CurrentBoState = st_Warm();
		m_StepDone = true;
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Cluster::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Cluster::EndStep()
{


	if (m_OurColony->GetCurrentStateNo() == -1 || GetNumberJuv() == 0)
	{
		m_StepDone = true;
		SetCurrentStateNo(-1);
		st_Dying();
		return;
	}
	/** I have added this before feeding as presumably this is another way workers end
	up staying in the colony when they are young.*/
	double TotalMass = m_EggMass + m_PupaMass;
	if (GetNumberLarva() > 0)
	{

		auto i = GetLarva()->begin();
		while (i != GetLarva()->end())
		{
			TotalMass += (*i)->GetMass();
			++i;
		}
	}
	SetJuvMass(TotalMass);
	m_Age += 10;
	timestep++;
}

//--------------------------------------------------------------------------------------------------------------------------------
double Bombus_Cluster::Get_k()
{
	/**
	k = αA / C; A = surface area; C = heat capacity; α = heat transfer corefficient
	I assume as the cluster grows A and C would increase.
	\cite<Putra2016> gives the heat capacity of wax as 2.040 kJ / kgC I think that is the same as J / gC
	The queen is about 20mm longand 8mm wide(thorax width), so I assume that is somewhat similar
	to the size of the first couple of clusters combined.
	α = heat transfer corefficient https ://www.thermopedia.com/content/841/ something like 5-37 W/m2K
	*/
	//assuming cluster is similar in size to the queen and initially 5mm high. 
	double top = (m_ClusterLengthmm / 1000.0) * (m_ClusterWidthmm / 1000.0);
	double side = (m_ClusterLengthmm / 1000.0) * (m_ClusterHeighthmm / 1000.0);
	double end = (m_ClusterHeighthmm / 1000.0) * (m_ClusterWidthmm / 1000.0);
	double A = 0.0;
	if (m_CurrentBoState == toBombusS_Warm)
	{
		A = top;
	}
	else if (m_CurrentBoState == toBombusS_Cool)
	{
		A = (top + side + end) * 2.0;
	}
	double heat_capacity = ClusterHeatCapacityJperg; //kJ/kgK or J/gK
	double C = heat_capacity * (m_JuvMass + m_wax) / 1000.0;
	double alpha = AlphaOfAir;
	//this is freely moving air 5 - 37W/m2K, it might be lower if they air is still and in a small area. 
	if (C <= 0.0)
	{
		m_OurLandscape->Warn("Bombus_Cluster::Get_k()", "Impossible cluster with zero juveniles and wax.");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
	double k = alpha * A / C;
	return k;
}

//--------------------------------------------------------------------------------------------------------------------------------
double Bombus_Cluster::CalculateDMDeveloped()
{
	/**
	The temperature difference between optimum and the actual temperature of the cluster,
	multiplied by the 10 mins of the time step.
	*/
	double DM = 10.0 * (OptimumDevTemp - m_temp);
	return DM;
}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//**
//**                                            Bees
//**
//********************************************************************************************************************************
//********************************************************************************************************************************
//**************************************** Bombus_Base Definition ******************************************************************
//*******************************************************************************************************************************/
/**
 * \brief The base class for all bumble bees containing common attributes and behaviour for descendent types 
 * \param a_x Horizontal location of colony in landscape. 
 * \param a_y Vertical location of colony in landscape. 
 * \param p_L Landscape manager.
 * \param p_BPM Bombus population manager.
 * \param a_Colony A pointer to the colony the cluster is in.
 */
Bombus_Base::Bombus_Base(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM,  // NOLINT(cppcoreguidelines-pro-type-member-init)
                         // NOLINT(cppcoreguidelines-pro-type-member-init)
                         Bombus_Colony* a_Colony) : TAnimal(a_x, a_y, p_L)
{
	Bombus_Base::Init(a_x, a_y, p_L, p_BPM, a_Colony);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, Bombus_Colony* a_Colony)
{
	TAnimal::ReinitialiseObject(a_x, a_y, p_L);
	Init(a_x, a_y, p_L, p_BPM, a_Colony);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, Bombus_Colony* a_Colony)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = p_BPM;
	m_OurLandscape = p_L;
	m_CurrentBoState = toBombusS_InitialState;
	m_OurColony = a_Colony;
	timeInState = 0;
	timestep = 0;
	m_AgeDegrees = 0;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::Dominate()
{
	m_TimesDominated++;

}

void Bombus_Base::Damaged(bool a_CompetitionAggression)
{
	if(a_CompetitionAggression)
	{
		m_Damage++;
	}

}

void Bombus_Base::AddToWarmAccount()
{
	if (m_Callow == false && m_OurColony != nullptr)
	{
		m_OurColony->m_WarmBodies++;
		m_OurColony->m_SumWarmBodies += m_Temp;
	}
	
}



//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::SetStatic()
{
	MinJuvDevTemp = cfg_BombusMinJuvDevTemp.value();
	B1EggCellPollen = cfg_BombusB1EggCellPollen.value();
	B2EggCellPollen = cfg_BombusB2EggCellPollen.value();
	B3EggCellPollen = cfg_BombusB3EggCellPollen.value();
	WorkersEggCellPollen = cfg_BombusWorkersEggCellPollen.value();
	RootIncubatingProb = cfg_BombusRootIncubatingProb.value();
	CallowForDM = cfg_BombusCallowForDM.value();
	CallowMinTemp = cfg_BombusCallowMinTemp.value();
	SugarDigestionRate = cfg_BombusSugarDigestionRate.value();
	MinAgeWokersCompete = cfg_BombusMinAgeWokersCompete.value();
	AdultsEncounteredInStep = cfg_BombusWorkerEncounteredInStep.value();
	EggMax = cfg_BombusEggMax.value();
	EggMin = cfg_BombusEggMin.value();
	EnergyDensityEgg = cfg_BombusEnergyDensityEgg.value();

	MinMass = cfg_BombusMinMass.value();
	LarvalDMmin = cfg_BombusLarvalDMmin.value();
	LarvalDMmax = cfg_BombusLarvalDMmax.value();
	LarvaGyneDMcutoff = cfg_BombusLarvaGyneDMcutoff.value();
	OvaryShrinkage = cfg_BombusOvaryShrinkage.value();
	MaxPropOvaries = cfg_BombusMaxPropOvaries.value();
	MaxMass = cfg_BombusMaxMass.value();
	PropDMmaxIsGyne = cfg_BombusPropDMmaxIsGyne.value();
	OptimumDevTemp = cfg_BombusOptimumDevTemp.value();
	DominationThreshold = cfg_BombusDominationThreshold.value();
	FanningEnergyPerMin = cfg_BombusFanningEnergyPerMin.value();
	DigestionRate = cfg_BombusDigestionRate.value();
	OptimumColTemp = cfg_BombusOptimumColTemp.value();
	GrowthG = cfg_BombusGrowthG.value();
	FeedLargestFirst = cfg_BombusFeedLargestFirst.value();
	MinColonyTempToLay = cfg_BombusMinColonyTempToLay.value();
	MaxPropOvaries = cfg_BombusMaxPropOvaries.value();

	ColonyLocation = StringToLocation(cfg_BombusColonyLocation.value());
	LarvaDevControl = StringToControl(cfg_BombusLarvaDevControl.value());
	LarvalMaleDM = cfg_BombusLarvalMaleDM.value();
	MaxAgeWorkerMakeWax = cfg_BombusMaxAgeWorkerMakeWax.value();
	PropBroodCare = cfg_BombusPropBroodCare.value();
	StepEnergyInspection = cfg_BombusStepEnergyInspection.value();
	AccessForaging = cfg_BombusAccessForaging.value();
	denominator = cfg_BombusIncubatingDenominator.value();

	AdultsHomeotherms = cfg_BombusAdultsHomeotherms.value();
	AdultBodyDensity = cfg_BombusAdultBodyDensity.value();
	AdultHeatCapacityJperg = cfg_BombusAdultHeatCapacityJperg.value();
	AlphaOfAir = cfg_BombusAlphaOfAir.value();
	BodyTempAdults = cfg_BombusBodyTempAdults.value();

	if (MinMass <= 0.0 && EnergyDensityEgg <= 0)
	{
		m_OurLandscape->Warn("Bombus_Base::SetStatic", "MinMass and EnergyDensityEgg must be greater than zero.");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
	if ((OptimumDevTemp - MinJuvDevTemp) <= 0.0)
	{
		m_OurLandscape->Warn("Bombus_Base::SetStatic", "MinJuvDevTemp should be less than the OptimumDevTemp.");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
	if (MaxMass <= 0.0)
	{
		m_OurLandscape->Warn("Bombus_Larva::CalculatePotentialGrowth()", "MaxMass must be greater than zero.");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
	if (MaxPropOvaries <= 0.0)
	{
		m_OurLandscape->Warn("Bombus_Worker::st_Decide", "MaxPropOvaries must be greater than zero.");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
double Bombus_Base::ProbV1(double a_temp)
{
	return pow(OptimumDevTemp - a_temp, RootIncubatingProb) / denominator;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::RecordVitals()
{
	string ColonyID = "0";
	if (m_OurColony != nullptr)
	{
		ColonyID = to_string(GetColony()->GetColonyID());
	}
	string LabTemp;
	string ColonyTemp;
	if (m_OurColony == nullptr)
	{
		int day = m_OurLandscape->SupplyDayInYear();

		LabTemp = to_string(m_OurColony->GetMultipleDailyColonyTemps()[day]);

		ColonyTemp = "NA";
	}
	else
	{
		LabTemp = to_string(m_OurColony->GetLabTemp());
		ColonyTemp = to_string(m_OurColony->GetColonyTemp());
	}

	string MothersID = to_string(GetMotherID());
	string FathersID = to_string(GetFatherID());

	vector<string> Output = {
		ColonyID,
		to_string(m_id),
		MothersID,
		FathersID,
		m_MyIdentityString,
		to_string(int(double(m_Age) / 1440.0)),
		to_string(int(double(m_StageAge) / 1440.0)),
		to_string(m_Mass.ReportMass()),
		to_string(m_StartingIndiv),
		LabTemp,
		ColonyTemp
	};
	m_OurPopulationManager->RecordingIndividuals->RecordValues(Output);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::MortalityStressors()
{
	/**
	Mortality stressor for adult bumblebees.
	*/
	m_mortalityToday = m_mortality;
	double temp;

	if (!GetExpMort()->empty())
	{


		if (GetColony() != nullptr)
		{
			m_mortalityToday = m_mortalityToday * (*GetExpMort())[m_StageAgeDay] * (1.0 + double(GetColony()->NoAdultCorpses)
				* m_DeadEffected);
		}
		else
		{
			m_mortalityToday = m_mortalityToday * (*GetExpMort())[m_StageAgeDay];
		}
	}
	if (m_OurColony != nullptr)
	{
		temp = m_OurColony->GetColonyTemp();
	}
	else if (AccessForaging == false)
	{
		temp = m_OurColony->GetLabTemp();
	}
	else
	{
		temp = m_OurLandscape->SupplyTemp();
	}

	if (m_BelowTempCoef != 0.0 && temp < OptimumColTemp)
	{
		m_mortalityToday = m_mortalityToday * exp((OptimumColTemp - temp) * m_BelowTempCoef);
	}
	else if (m_AboveTempCoef != 0.0 && temp > OptimumColTemp)
	{
		m_mortalityToday = m_mortalityToday * exp((temp - OptimumColTemp) * m_AboveTempCoef);
	}


	if(m_Senesce) {
		m_mortalityToday = 10.0 * m_mortalityToday;
	}

}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::Grow()
{
	/**
	When the individual took on food, they filled their stomach. Based on the data from \cite<Muth2018> I set this as
	25ul/149mg mass. I think the data suggests they can digest that food at 0.72ul/min (for a ~149mg B. impatience).
	Presumably this also scales with mass (volume of the bee). ~0.005ul/min mg. So in 10mins they would absorb 0.05ul/mg.
	Emptying their stomach in a little over 30mins. Of course if they are feeding larva they could empty quicker, but they
	will still be absorbing some of the food.
	*/
	Food Digested = m_Stomach.Digest(DigestionRate * m_Mass.ReportLeanMass());
	m_Mass.DigestedFoodEffect(Digested);
	/** If the mass drops by half from an all-time max, then the larva starves to death.*/
	if (m_Mass.Starvation() == true)
	{
		st_Dying();
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Base::st_Fan()
{
	/** Assumed to be similar to flight, but less dependent on mass. Have rounded up \cite<Heinrich2014>
	to give 6j/Min for fanning.*/
	m_Mass.TakeEnergyFromFat(FanningEnergyPerMin * 10.0);
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------

void Bombus_Base::EatNectar()
{
	Nectar SomeNectar = GetColony()->m_Nectar.TakeNectar(
		m_Stomach.GetMaxVolumeCapacity() - m_Stomach.ReportVolumeFood());
	m_Stomach.PointToFood()->AddNectarPacket(SomeNectar);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::EatPollen()
{
	/** \cite<Pereboom2000> states that workers drink nectar and then eat pollen. Then containing 7.5 +/- 3.4 ug
		protien per ul of stomach contents. This is up from 0.3+/-0.3 ug/ul of nectar alone.
		This is 0.0003mg/ul. The energy density of protein is ~17.0J/mg.
		Do we take pollen up to 7.5 ug/ul, do we assume the individual tried to take 7.5 - 0.3 +/- 3.4 or the upper 0.95 confidence ?
		The individual could be eating a quantity of pollen, and depending on the quality, they are getting more or less protein.
		Or do they consume more of the duff pollen? If the 20-60% of pollen is protein are values with a somewhat arbitrary cut off,
		(through trial and error a bit more than 87% confidence), then the bee would take ~18.3 ug of pollen/ul. This is a total
		fudge, as it assumes that the bees eat the same amount proportional to their size, regardless of the content of the pollen.
		This can be refined, but for now I am going to go with 20.0ug/ul (rounding the uncertain 18.3 ug/ul). Or 0.02mg/ul
		*/
	double EnergyFromFat = PointToMass()->TakeEnergyFromFat(
		m_Stomach.ReportVolumeFood() * m_Stomach.ReturnConcentrationAdultAddToNectar() *
		m_Stomach.ReturnEnergyPer_mgProtein());


	double MassProtein = EnergyFromFat / m_Stomach.ReturnEnergyPer_mgProtein();
	double MassPollenWanted = (m_Stomach.ReportVolumeFood() * m_Stomach.ReturnMassPollenConcInNectar()) -
		MassProtein;
	Pollen MassPollenTaken = GetColony()->m_Pollen.TakePollenMass(MassPollenWanted);
	/** Protein excreted by adult into stomach*/
	m_Stomach.PointToFood()->AddFood(0, 0, MassProtein, EnergyFromFat,
	                                 MassProtein / m_Stomach.ReturnProteinDensity_mg_per_ul());
	/** Pollen eaten*/
	m_Stomach.PointToFood()->AddFood(MassPollenTaken.ReportMassSugar(), MassPollenTaken.ReportMassFattyAcids(),
	                                 MassPollenTaken.ReportProteinMass(), MassPollenTaken.ReportEnergy(),
	                                 MassPollenTaken.ReportVolume());
	if (MassPollenTaken.ReportProteinMass() > 0)
	{
		m_Stomach.SetPollenOnBoard(true);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
bool Bombus_Base::InColony()
{
	if (m_Location_x == GetColony()->Supply_m_Location_x() &&
		m_Location_y == GetColony()->Supply_m_Location_y())
	{
		return true;
	}
	return false;
}


void Bombus_Base::CalculateMyBodyTemp()
{
	if (AdultsHomeotherms == false)
	{
		double Mass = m_Mass.ReportMass();
		double A = 5.2 * pow(AdultBodyDensity * Mass / 1000.0, (2 / 3));
		double heat_capacity = AdultHeatCapacityJperg; //kJ/kgK or J/gK
		double C = heat_capacity * m_Mass.ReportMass() / 1000;
		double alpha = AlphaOfAir;
		//this is freely moving air 5 - 37W/m2K, it might be lower if they air is still and in a small area. 
		if (C <= 0.0)
		{
			m_OurLandscape->Warn("Bombus_Cluster::Get_k()", "Impossible adult with zero mass.");
			std::exit(1); // NOLINT(concurrency-mt-unsafe)
		}
		double k = alpha * A / C;

		/**Can hold temperature while in the colony or without one (but will be need to be changed for having one, but not being in it in V2.*/
		double temp;
		if (m_OurColony != nullptr)
		{
			temp = m_OurColony->GetColonyTemp();
		}
		else if (AccessForaging == false)
		{


			temp = GetColony()->GetMultipleDailyColonyTemps()[0];
		}
		else
		{
			temp = m_OurLandscape->SupplyTemp();
		}
		/** Bumble bees unlike truely endotherms appear to be able to drop their core temperature when the colony is too warm, or when there
		 * is not a need to warm it. 
		 */
		double targetBodyTemp;
		if(temp < 25.0)
		{
			targetBodyTemp = BodyTempAdults;
		}
		else if(temp < OptimumColTemp)
		{
			targetBodyTemp = BodyTempAdults - 1.0;
		}else
		{
			targetBodyTemp = -0.5 * (temp - OptimumColTemp) + BodyTempAdults - 1.0;
		}

		if(targetBodyTemp < temp)
		{
			targetBodyTemp = temp;
		}


		/** If the individual is warming its self, it needs energy to do this. It can't cool its self except by urination,
		 * and that probably isn't going to work inside the colony. 
		 */
		double TempWithoutWarming = temp + (m_Temp - temp) * exp(-k * 600.0);

		if(m_Callow ||TempWithoutWarming >= targetBodyTemp)
		{
			m_Temp = TempWithoutWarming;
		}else
		{
			double EnergyWanted = C * (targetBodyTemp - TempWithoutWarming);
			double EnergyGotten = m_Mass.TakeEnergyFromFat(EnergyWanted);

			m_Temp = TempWithoutWarming + (EnergyGotten / C);
		}



	}
	else
	{
		m_Temp = BodyTempAdults;
	}





}
//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Base::RemoveSelfFromColony()
{
	
}
//--------------------------------------------------------------------------------------------------------------------------------
bool Bombus_Base::SetFanning()
{
	if (m_StepDone || m_CurrentStateNo == -1) return false;
	m_CurrentBoState = toBombusS_Fan;
	return true;
}
//--------------------------------------------------------------------------------------------------------------------------------
void Stomach::Init(Bombus_Base* a_bee, double a_MaxVolumeCapacity)
{
	m_owner = a_bee;
	m_MaxVolumeCapacity = a_MaxVolumeCapacity;
	SugarDigestionRate = cfg_BombusSugarDigestionRate.value();
	EnergyDensityFlesh = cfg_BombusEnergyDensityFlesh.value();
}
//--------------------------------------------------------------------------------------------------------------------------------
void Stomach::SetStatic()
{
	ConcentrationAdultAddToNectar = cfg_BombusConcentrationAdultAddToNectar.value();
	EnergyPer_mgProtein = cfg_BombusEnergyPermgProtein.value();
	MassPollenConcInNectar = cfg_BombusMassPollenConcInNectar.value();
	ProteinDensity_mg_per_ul = cfg_BombusProteinDensitymgperul.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
Food Stomach::Digest(double a_FoodVolume)
{
	double SugarInStomach = m_FoodInStomach.ReportSugarMass();
	double VolumeWanted;
	if (SugarInStomach > 0)
	{
		double SugarNeeded = SugarDigestionRate * m_owner->PointToMass()->ReportLeanMass();
		double VolumeInStomach = m_FoodInStomach.ReportVolume();
		VolumeWanted = VolumeInStomach * SugarNeeded / SugarInStomach;
	}
	else
	{
		VolumeWanted = 0.0;
		m_PollenOnBoard = false;
	}
	/** Limits consumption to sugar needed or the maximum throughput of fluid. All based on Muth et al.*/
	double VolumeAchievable = min(VolumeWanted, a_FoodVolume);
	/** An individual can only digest what is actually in the stomach or some portion of it..*/
	Food SomeFood = m_FoodInStomach.TakeFoodVol(VolumeAchievable);
	return SomeFood;
}

//--------------------------------------------------------------------------------------------------------------------------------
Food Stomach::Regurgitate(double a_FoodVolume)
{
	/** When an adult feeds a larva or returns nectar to the colony they regurtiate food.*/
	Food SomeFood = m_FoodInStomach.TakeFoodVol(a_FoodVolume);
	if (m_FoodInStomach.ReportVolume() == 0.0)
	{
		m_PollenOnBoard = false;
	}
	return SomeFood;
}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//**************************************** Bombus_Egg Definition ******************************************************************
//*******************************************************************************************************************************/
Bombus_Egg::Bombus_Egg(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                       unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, Bombus_Colony* a_Colony,
                       Bombus_Cluster* a_Cluster) : Bombus_Base(a_x, a_y, p_L, p_BPM, a_Colony)
{
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_Colony, a_Cluster);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Egg::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                        unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, Bombus_Colony* a_Colony,
                        Bombus_Cluster* a_Cluster)
{
	Bombus_Base::ReInit(a_x, a_y, p_L, p_BPM, a_Colony);
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_Colony, a_Cluster);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Egg::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                      unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, Bombus_Colony* a_Colony,
                      Bombus_Cluster* a_Cluster)
{
	/** \brief Label of identity.*/
	m_MyIdentity = to_Egg;
	m_MyIdentityString = "Egg";
	m_OurCluster = a_Cluster;
	m_mortality = cfg_BombusEggMortality.value();
	m_BelowTempCoef = cfg_BombusEggBelowTempCoef.value();
	m_AboveTempCoef = cfg_BombusEggAboveTempCoef.value();
	MortalityStressors();
	/**
	Bombus can lay diploid and haploid eggs, the diploid eggs have a mother and father and develope into workers, gynes and inbreeding aberant diploid males.
	Diploid eggs and the proceeding larva, pupa and females and aparant males may want to include information on the mother and father.
	*/

	SetMotherID(a_MotherID);
	SetFatherID(a_FatherID);
	if (a_FatherID == 0)
	{
		m_isHaploid = true;
	}
	m_Age = 0;
	m_StageAge = 0;
	m_StageAgeDay = 0;
}

void Bombus_Egg::MortalityStressors()
{
	/**
	Mortality stressor for juvinile bumblebees.
	*/
	m_mortalityToday = m_mortality;
	if (m_OurCluster != nullptr)
	{
		double ClusterTemp = m_OurCluster->GetTemp();
		if (m_BelowTempCoef != 0.0 && ClusterTemp < OptimumDevTemp)
		{
			m_mortalityToday = m_mortalityToday * exp((OptimumDevTemp - ClusterTemp) * m_BelowTempCoef);
		}
		else if (m_AboveTempCoef != 0.0 && ClusterTemp > OptimumDevTemp)
		{
			m_mortalityToday = m_mortalityToday * exp((ClusterTemp - OptimumDevTemp) * m_AboveTempCoef);
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Egg::Step()
{
	/**
	* Bombus terrestris egg behaviour is simple. It calls develop until the egg hatches or dies.
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState: // Initial state always starts with develop
		m_CurrentBoState = toBombusS_Develop;
		break;
	case toBombusS_Develop:
		m_CurrentBoState = st_Develop();
		m_StepDone = true;
		break;
	case toBombusS_NextStage:
		m_CurrentBoState = st_Hatch();
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Egg::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Egg::EndStep()
{
	if (timestep % 144 == 0)
	{
		m_StageAgeDay += 1;
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Egg::SetStatic()
{
	m_EggDM = cfg_BombusEggDevelTotalDM.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Egg::st_Develop()
{
	/*
	* Development is preceded by a mortality test, then a day degree calculation is made to determine the development that occurred in the last 24 hours.
	* When enough day degrees are achieved the egg hatches.If it does not hatch then the development behaviour is queued up for the next day.
	*
	* We will have to calculate how much time the queen is away from her brood and therefore how much cooling can take place
	*/
	MortalityStressors();

	if (g_rand_uni() < m_mortalityToday || GetColony()->GetCurrentStateNo() == -1)
		return toBombusS_Die;
	/*
		* DD will be something like, Temp-6, but Temp will be proportional to the time the queen spends on the eggs, we don't know how quickly the eggs cool k in equation
		T(t)=Ta + (T0 - Ta) e(-kt). T0 would be ~30C, Ta would be the temperature in the colony, which would be its self proportional to background temperature and how many bees are
		in the colony. I think the queen can probably heat the eggs quicker than they cool due to a temperature higher than 30C
		*/
	if (m_AgeDegrees >= m_EggDM)
	{
		return toBombusS_NextStage;
	}



	if(GetColony()->GetColonyTemp() >= MinJuvDevTemp)
	{
		m_AgeDegrees += 10.0 * (m_OurCluster->GetTemp() - MinJuvDevTemp);
	}

	m_Age += 10;
	m_StageAge += 10;

	timestep++;
	return toBombusS_Develop;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Egg::st_Hatch()
{
	/**
	* Creates a new larva object and passes the data from the egg to it, then signals young object removal.
	*/
	struct_Bombus sBo;
	//This structure for Bombus is in its population manager. Each transition, or line of transition, may need a different struct
	sBo.BPM = m_OurPopulationManager;
	sBo.L = m_OurLandscape;
	sBo.age = m_Age;
	sBo.x = m_Location_x;
	sBo.y = m_Location_y;
	sBo.MotherID = GetMotherID();
	sBo.FatherID = GetFatherID();
	/** Add new larva self to the NoLarva*/
	GetColony()->NoLarva++;
	sBo.Colony = GetColony();
	sBo.Cluster = m_OurCluster;
	sBo.Cluster->RemoveEggMass(MinMass);
	sBo.SexLocus_1 = GetSexLocus_1();
	sBo.SexLocus_2 = GetSexLocus_2();
	m_OurPopulationManager->CreateObjects(to_Larva, nullptr, &sBo, 1);
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Egg::RemoveEgg()
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	/** Remove self from the cluster of juviniles*/
	m_OurCluster->RemoveEgg(this);
	/** Remove self from the number of male eggs*/
	if (m_isHaploid || m_sexLocus_1 == m_sexLocus_2)
	{
		if (GetColony()->GetColonyID() == m_MotherID)
		{
			GetColony()->NoQueensMaleEggs--;
		}
		else
		{
			GetColony()->NoWorkerMaleEggs--;
		}
	}
	GetColony()->NoEggs--;

	SetCluster(nullptr);
	SetColony(nullptr);
	SetSexLocus_1(0);
	SetSexLocus_2(0);
	m_StepDone = true;
}

TTypeOfBombusState Bombus_Egg::st_Dying()
{
	GetColony()->NoDeadEggs++;
	RemoveEgg();
	return toBombusS_Die;
}

void Bombus_Egg::Eaten()
{
	RemoveEgg();
}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//**************************************** Bombus_Larva Definition ************************************************
//*******************************************************************************************************************************/
Bombus_Larva::Bombus_Larva(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                           unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age,
                           Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster) : Bombus_Egg(
	a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_Colony, a_Cluster)
{
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Colony, a_Cluster);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                          unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age,
                          Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster)
{
	Bombus_Egg::ReInit(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_Colony, a_Cluster);
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Colony, a_Cluster);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                        unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age,
                        Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster)
{
	/** \brief Label of identity.*/
	m_MyIdentity = to_Larva;
	m_MyIdentityString = "Larva";
	m_StageAge = 0;
	m_StageAgeDay = 0;
	SetMotherID(a_MotherID);
	SetFatherID(a_FatherID);
	LarvaMortalitySlope = cfg_BombusLarvaMortalitySlope.value();
	LarvaMortalityIntercept = cfg_BombusLarvaMortalityIntercept.value();
	m_BelowTempCoef = cfg_BombusLarvaBelowTempCoef.value();
	m_AboveTempCoef = cfg_BombusLarvaAboveTempCoef.value();
	MortalityStressors();
	m_sexLocus_1 = a_SexLocus_1;
	m_sexLocus_2 = a_SexLocus_2;
	m_Mass.Init(this);
	m_Mass.HardSetLeanMass(MinMass);
	//RandomPercFeedings = p1.get();
	double VolumeCapacity = m_Mass.ReportLeanMass() * StomachCapacityPermgBodyWeight;
	m_Stomach.Init(this, VolumeCapacity);
	if (m_OurCluster != nullptr)
	{
		TempOverMin = (m_OurCluster->GetTemp() - MinJuvDevTemp);
		SetNextMass(CalculatePotentialGrowth());
	}
	if (a_FatherID == 0)
	{
		m_isHaploid = true;
		m_DM = LarvalMaleDM;
		m_IAmMale = true;
	}
	else if (m_sexLocus_1 == m_sexLocus_2)
	{
		m_isHaploid = false;
		m_DM = LarvalMaleDM;
		m_IAmMale = true;
	}
	else
	{
		m_isHaploid = false;
		if (LarvaDevControl == toLarvaDev_QueenStep)
		{
			m_DM = LarvalDMmax;
		}
		else
		{
			m_DM = GetColony()->LarvalDM;
		}
	}
	m_NumberQInteractions = 0;
	m_NumberWInteractions = 0;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::SetStatic()
{
	StomachCapacityPermgBodyWeight = cfg_BombusStomachCapacityPermgBodyWeight.value();
	EnergyDensityFlesh = cfg_BombusEnergyDensityFlesh.value();
	PupaDMDivisionOfLarval = cfg_BombusPupaDMDivisionOfLarval.value();

	CfgInt cfg_BombusLarvaStageHardLimit("BOMBUS_LARVASTAGEHARDLIMIT", CFG_CUSTOM, 100);
	m_LarvaStageHardLimit = cfg_BombusLarvaStageHardLimit.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::BeginStep()
{
	/** The gyne juvinile hormone can be detected in the first 3.5 to 5 days. I think this is
	likely due to developement at slightly different teperatures. If the min dev temp is 25C
	then at 32C and 3.5 DD is 24.5, at 30C DD is 25 therefore I will put a threshold of 25DD or 36000DM.
	Slightly lazily I have said if the DD is the max possible, then it is a gyne. This is likely more size based.
	But this should be good enough.*/
	if (m_FatherID != 0 && m_AgeDegrees >= 36000 && m_DM >= LarvalDMmax * PropDMmaxIsGyne)
	{
		m_IAmGyne = true;
	}
	//RecordVitals();
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::Step()
//It is possible this should be moved up to an earlier base. If everything ends up stepping in the same way, it would be repetitive to have it repeatedly.
{
	/**
	* Bombus terrestris egg behaviour is simple. It calls develop until the egg hatches or dies.
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState:
		//m_StepDone = true;
		m_CurrentBoState = toBombusS_InitialState;
		m_StepDone = true;
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Larva::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::EndStep()
{
	/** To develop and grow the larva needs to be kept over a minimum.*/
	TempOverMin = (m_OurCluster->GetTemp() - MinJuvDevTemp);
	if (m_OurCluster->PointToPollenLump()->ReportVolume() > 0.0 && int(1000 * m_HeldFood.ReportVolume()) ==
		0)
	{
		PointToFoodSource(m_OurCluster->PointToPollenLump());
	}
	else
	{
		PointToFoodSource(&m_HeldFood);
	}
	st_Consume();
	/** Develop a little in each ten min time step.*/
	st_Develop();
	SetNextMass(CalculatePotentialGrowth());
	/** As the larva grows its stomach grows and it can consume more food quicker.*/
	double VolumeCapacity = m_Mass.ReportLeanMass() * StomachCapacityPermgBodyWeight;
	m_Stomach.UpdateMaxCapacity(VolumeCapacity);
	/** The queens percentage of interactions in the step reduces the degree minutes
	and leads to supression of size and becoming a gyne.
	This must happen in the first 3.5, to 5 days. At least for the gyne. I suppose
	workers could continue to reduce.
	*/
	if (LarvaDevControl == toLarvaDev_QueenStep && m_NumberQInteractions > 0 && m_FatherID != 0)
	{
		double PropQueenInteracts = double(m_NumberQInteractions) /
			(double(m_NumberQInteractions) + double(m_NumberWInteractions));

		double MaxReduction = 10.0 * (LarvalDMmax + LarvalDMmin) / m_minsQueenCanReduce;
		m_DM -= PropQueenInteracts * MaxReduction;
	}

	if (timestep % 144 == 0)
	{
		m_timesFed = 0;
		m_StageAgeDay += 1;
	}
	m_NumberQInteractions = 0;
	m_NumberWInteractions = 0;
}

//--------------------------------------------------------------------------------------------------------------------------------
double Bombus_Larva::CalculatePotentialGrowth()
{
	if (GetColony() != nullptr && GetColony()->GetColonyTemp() >= MinJuvDevTemp) {
		double Mass = m_Mass.ReportLeanMass();
		double MassMulti = GrowthG * Mass * TempOverMin * (1.0 - (Mass / MaxMass));
		double PossiblenextMass = MassMulti * Mass + Mass;
		if(PossiblenextMass >= MaxMass)
		{
			PossiblenextMass = MaxMass;
		}
		return PossiblenextMass;
	}
	return m_Mass.ReportLeanMass();
}

//--------------------------------------------------------------------------------------------------------------------------------
double Bombus_Larva::CalculateGrowthEnergy()
{
	double MassChange = m_PottentialNextMass - m_Mass.ReportLeanMass();
	/** Fresh mass 7 × 10^6 J/kg (7 J/mg) is stored, Fat – 4 × 106 J kg-1  /cite<Peters1983>.  */
	return EnergyDensityFlesh * MassChange;
}

//--------------------------------------------------------------------------------------------------------------------------------
bool Bombus_Larva::Hungry()
{
	//Maybe this should be and not or?
	if (int(1000 * m_Stomach.ReportVolumeFood()) == 0 || m_Mass.ReportEnergyDeficit() > 0)
	{
		return true;
	}
	return false;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::Feed(Food a_FoodPacket)
{
	m_HeldFood.AddFoodPacket(a_FoodPacket);
	m_timesFed++;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Larva::st_Develop()
{
	/*
	* Development is preceded by a mortality test.
	* When enough day degrees are achieved the larva pupates.
	*
	* The amount fed dictates mass gain or loss.
	*
	* When not fed enough to grow, larva can holt development.
	*/
	m_mortality = LarvaMortalitySlope * m_Mass.ReportLeanMass() + LarvaMortalityIntercept;
	MortalityStressors();

	double rand = g_rand_uni();
	if (m_StageAgeDay >= m_LarvaStageHardLimit ||  rand < m_mortalityToday || GetColony()->GetCurrentStateNo() == -1)
	{
		st_Dying();
		return toBombusS_Die;
	}
	if (m_AgeDegrees >= m_DM)
	{
		Pupate();
		st_Dying();
		return toBombusS_Die;
	}
	Grow();
	if (m_CurrentStateNo == -1)
	{
		return toBombusS_Die;
	}

	/** Ask if the larva are developing.*/
	if (m_Mass.AskIfDeveloping() == true && GetColony()->GetColonyTemp() >= MinJuvDevTemp)
	{
		m_AgeDegrees += TempOverMin * 10.0;
	}
	m_Age += 10;
	m_StageAge += 10;

	timestep++;
	m_StepDone = true;
	return toBombusS_Develop;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Larva::st_Dying()
{
	m_CurrentStateNo = -1;
	/** Removes larva from its cluster*/
	m_OurCluster->RemoveLarva(this);
	SetCluster(nullptr);
	/** remove  larva self from the NoLarva*/
	GetColony()->NoLarva--;
	GetColony()->NoLarvaCorpses++;
	SetColony(nullptr);
	SetSexLocus_1(0);
	SetSexLocus_2(0);
	m_StepDone = true;
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Larva::Pupate()
{
	/**
	* Creates a new pupa object and passes the data from the larva to it.
	*/
	struct_Bombus sBo;
	//This structure for Bombus is in its population manager. Each transition, or line of transition, may need a different struct
	sBo.BPM = m_OurPopulationManager;
	sBo.L = m_OurLandscape;
	sBo.age = m_Age;
	sBo.x = m_Location_x;
	sBo.y = m_Location_y;
	sBo.MotherID = GetMotherID();
	sBo.FatherID = GetFatherID();
	sBo.Mass = m_Mass.ReportLeanMass();
	sBo.IsGyne = m_IAmGyne;
	sBo.PupaDM = m_DM / PupaDMDivisionOfLarval;
	sBo.Colony = GetColony();
	sBo.Cluster = m_OurCluster;
	sBo.SexLocus_1 = GetSexLocus_1();
	sBo.SexLocus_2 = GetSexLocus_2();
	m_OurCluster->AddPupaMass(m_Mass.ReportLeanMass());
	m_OurPopulationManager->CreateObjects(to_Pupa, nullptr, &sBo, 1);
}

TTypeOfBombusState Bombus_Larva::st_Consume()
{
	/** Larva were eating to much and getting huge. This gives them an apetite upto a max for there size if
			the food is of very low quality they will consume the max they can, and pottentially still starve.*/

	double HeldEnergyFood = m_FoodSource->ReportEnergy();
	double ratio;
	if (HeldEnergyFood > 0.0)
	{
		double HeldVolFood = m_FoodSource->ReportVolume();
		ratio = HeldVolFood / HeldEnergyFood;
	}
	else
	{
		ratio = 0.0;
	}


	double EnergyNeeded = (m_PottentialNextMass - m_Mass.ReportLeanMass()) / EnergyDensityFlesh +
		m_Mass.CalculateMaintenanceEnergy();

	double VolNeeded = EnergyNeeded * ratio;
	// double VolWanted = min(m_MaxVolumeCapacity - ReportVolumeFood(), VolNeeded - ReportVolumeFood());
	double VolWanted = VolNeeded - m_Stomach.ReportVolumeFood();

	if (VolWanted < 0.0)
	{
		VolWanted = 0.0;
	}
	Food SomeFood = m_FoodSource->TakeFoodVol(VolWanted);

	/** The individual consumes what they can from a food packet. This is usually a larva.*/
	m_Stomach.PointToFood()->AddFoodPacket(SomeFood);

	return toBombusS_Consume;
}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//**************************************** Bombus_Pupa Definition ************************************************
//*******************************************************************************************************************************/
Bombus_Pupa::Bombus_Pupa(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                         unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
                         double a_BodyFat, Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster, bool a_isGyne,
                         double a_DM) : Bombus_Larva(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1,
                                                     a_SexLocus_2, a_age, a_Colony, a_Cluster)
{
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Mass, a_BodyFat, a_Colony,
	     a_Cluster, a_isGyne, a_DM);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Pupa::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                         unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
                         double a_BodyFat, Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster, bool a_isGyne,
                         double a_DM)
{
	Bombus_Larva::ReInit(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Colony,
	                     a_Cluster);
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Mass, a_BodyFat, a_Colony,
	     a_Cluster, a_isGyne, a_DM);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Pupa::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                       unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
                       double a_BodyFat, Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster, bool a_isGyne, double a_DM)
{
	/** \brief Label of identity.*/
	m_MyIdentity = to_Pupa;
	m_MyIdentityString = "Pupa";
	m_StageAge = 0;
	m_StageAgeDay = 0;
	m_mortality = cfg_BombusPupaMortality.value();
	SetMotherID(a_MotherID);
	SetFatherID(a_FatherID);
	m_BelowTempCoef = cfg_BombusPupaBelowTempCoef.value();
	m_AboveTempCoef = cfg_BombusPupaAboveTempCoef.value();
	MortalityStressors();
	if (a_FatherID == 0)
	{
		m_isHaploid = true;
	}
	else
	{
		m_isHaploid = false;
	}
	m_DM = a_DM;
	m_IAmGyne = a_isGyne;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Pupa::BeginStep()
{
	//RecordVitals();
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Pupa::Step()
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState: // Initial state always starts with develop
		m_CurrentBoState = toBombusS_Develop;
		break;
	case toBombusS_Develop:
		m_CurrentBoState = st_Develop();
		m_StepDone = true;
		break;
	case toBombusS_NextStage:
		m_CurrentBoState = st_Emerge();
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Pupa::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Pupa::EndStep()
{
	if (timestep % 144 == 0)
	{
		m_StageAgeDay += 1;
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Pupa::st_Develop()
{
	/*
	* Development is preceded by a mortality test, then a day degree calculation is made to determine the development that occurred in the last 24 hours.
	* When enough day degrees are achieved the larva pupates.If it does not pupate then the development behaviour is queued up for the next day.
	*
	* We will have to calculate how much time the queen is away from her brood and therefore how much cooling can take place. Also larvae on the edge
	* of the brood will be a little cooler.
	*
	*The larvae compete for resources and those at the edge of the colony get fed less. The first workers also get fed less. Amount of food dictates
	* how large workers will get and I assume large workers will take longer to develop.
	*/
	MortalityStressors();

	if (g_rand_uni() < m_mortalityToday || GetColony()->GetCurrentStateNo() == -1)
	{
		return toBombusS_Die;
	}
	/*
		* DD will be something like, Temp-6, but Temp will be proportional to the time the queen spends on the eggs, we don't know how quickly the eggs cool k in equation
		T(t)=Ta + (T0 - Ta) e(-kt). T0 would be ~30C, Ta would be the temperature in the colony, which would be its self proportional to background temperature and how many bees are
		in the colony. I think the queen can probably heat the eggs quicker than they cool due to a temperature higher than 30C
		*/
	/** Pupa take about half the time the larva took to develop*/
	if (m_AgeDegrees >= m_DM)
	{
		return toBombusS_NextStage;
	}

		/** Ask if the larva are developing.*/
	if (GetColony()->GetColonyTemp() >= MinJuvDevTemp)
	{
		TempOverMin = m_OurCluster->GetTemp() - MinJuvDevTemp;
		m_AgeDegrees += TempOverMin * 10.0;
	}

	m_Age += 10;
	m_StageAge += 10;
	timestep++;
	return toBombusS_Develop;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Pupa::st_Emerge()
{
	/**
	* Creates a new larva object and passes the data from the egg to it, then signals young object removal.
	*/
	struct_Bombus sBo;
	//This structure for Bombus is in its population manager. Each transition, or line of transition, may need a different struct
	sBo.BPM = m_OurPopulationManager;
	sBo.L = m_OurLandscape;
	sBo.age = m_Age;
	sBo.x = m_Location_x;
	sBo.y = m_Location_y;
	sBo.MotherID = GetMotherID();
	sBo.FatherID = GetFatherID();
	sBo.Mass = m_Mass.ReportLeanMass();
	sBo.BodyFat = 0;
	sBo.PropOvaries = 0;
	sBo.Colony = GetColony();
	sBo.SexLocus_1 = GetSexLocus_1();
	sBo.SexLocus_2 = GetSexLocus_2();

	GetColony()->EmptyCell++;
	/** Add new worker self to the NoWorkers*/
	GetColony()->NoAdults++;
	m_OurCluster->RemovePupaMass(sBo.Mass);
	GetColony()->m_TotalAdultMass += sBo.Mass;
	sBo.m_id = m_OurPopulationManager->nextID();

	if (m_sexLocus_2 == 0 || m_sexLocus_1 == m_sexLocus_2)
	{
		//haploid and diploid males
		GetColony()->NoMales++;
		m_OurPopulationManager->CreateObjects(to_Male, nullptr, &sBo, 1);
	}
	else if (m_IAmGyne == false)
	{
		//Worker
		GetColony()->NoWorkers++;
		m_OurPopulationManager->CreateObjects(to_Worker, nullptr, &sBo, 1);
	}
	else
	{
		// gyne
		GetColony()->NoGyne++;
		m_OurPopulationManager->CreateObjects(to_Gyne, nullptr, &sBo, 1);
	}

	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Pupa::st_Dying()
{
	m_CurrentStateNo = -1;
	/** Removes larva from its cluster*/
	m_OurCluster->RemovePupa(this);
	GetColony()->NoDeadPupa++;
	SetCluster(nullptr);
	SetColony(nullptr);
	SetSexLocus_1(0);
	SetSexLocus_2(0);
	m_StepDone = true;
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Pupa::SetStatic()
{
}

//********************************************************************************************************************************
//**************************************** Bombus_Worker Definition ************************************************
//*******************************************************************************************************************************/
Bombus_Worker::Bombus_Worker(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM,  // NOLINT(cppcoreguidelines-pro-type-member-init)
                             unsigned long a_MotherID, unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2,
                             int a_age, double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony,
                             unsigned long a_id) :
	Bombus_Base(a_x, a_y, p_L, p_BPM, a_Colony)
{
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Mass, a_BodyFat,
	     a_OvaryProp, a_Colony, a_id);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                           unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
                           double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id)
{
	Bombus_Base::ReInit(a_x, a_y, p_L, p_BPM, a_Colony);
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Mass, a_BodyFat,
	     a_OvaryProp, a_Colony, a_id);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                         unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
                         double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id)
{
	/** \brief Label of identity.*/
	m_MyIdentity = to_Worker;
	m_MyIdentityString = "Worker";
	m_Age = a_age;
	m_StageAge = 0;
	m_StageAgeDay = 0;
	m_id = a_id;
	m_ForagingTime = 0;
	SetMotherID(a_MotherID);
	SetFatherID(a_FatherID);
	m_Temp = BodyTempAdults;
	m_mortality = cfg_BombusWorkerMortality.value();
	/** This value is being set the same for all workers, but could be reduced if a worker is born into a colony that already has a high number of dead.*/
	m_DeadEffected = cfg_BombusWorkerDeadEffected.value();


	m_BelowTempCoef = cfg_BombusWorkerBelowTempCoef.value();
	m_AboveTempCoef = cfg_BombusWorkerAboveTempCoef.value();



	/** Based on the data from \cite<Muth2018>, when trying to get as much food as possible, B. impatience took on:
	volumeSucrose(ul) = 0.7175minute + 12.315
	With 30% w/w sucrose they consumed:
	volumeSucrose(ul) = -0.0011minute^2 + 0.6291x + 24.429
	In 10mins this is ~20 and 31 ul of solution in 10mins. Presumably they can consume much faster than 10mins.
	So for a ~149mg B. impatience I'd be tempted to go with them consuming 25ul.
	*/
	m_Mass.Init(this);
	m_Mass.HardSetLeanMass(a_Mass);
	m_Mass.SetCapableStoreFat(true);
	m_Mass.HardSetBodyFat(a_BodyFat);
	m_Mass.SetHasOvaries(true);
	m_Mass.HardSetOvaryProp(a_OvaryProp);



	CalculateMyBodyTemp();


	//m_AgeMortMultiVect = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiWorker.value(), 365 );

	MortalityStressors();
	m_SleepNeeded.ResetQuantity(MinSleep);
	m_SleepWanted.ResetQuantity(MaxSleep);


	double VolumeCapacity = m_Mass.ReportLeanMass() * StomachCapacityPermgBodyWeight;
	m_Stomach.Init(this, VolumeCapacity);
	/** "ln (mass (g))=0.552 (thorax width (mm))-4.09" \cite<Goulson2002a>p127 rearanged as 5/276(100*ln(Mass)-409)
	This was for workers, but I have pottentially falsly applied this to queens.
	*/
	m_ThoraxWidth = 5 * (100 * log(m_Mass.ReportLeanMass()) - 409) / 276;
	if (GetColony() != nullptr)
	{
		GetColony()->m_ColonyFemales.push_back(this);
	}

	m_BroodsLaid = 0;
    m_Senesce = false;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::BeginStep()
{





	CalculateMyBodyTemp();
	MortalityStressors();
	if (g_rand_uni() < m_mortalityToday)
	{
		m_CurrentBoState = st_Dying();
		return;
	}

	AddToWarmAccount();


	if (timestep % 144 == 0)
	{
		/** Making this additional sleepneeded allows a sleep deficit to recovered.*/
		m_SleepNeeded.IncreaseQuantity(MinSleep);
		m_SleepWanted.IncreaseQuantity(MaxSleep);
		/** If every day they count any gynes they detect, then when they don't detect any, rightly
		or wrongly they could assume there are no more.*/
		if (m_SwitchingPointDetected == true)
		{
			if (m_numberGyneLarvaToday == 0 && GetIfOvaryMax())
			{
				m_CompetitionPossible = true;
			}
			m_numberGyneLarvaToday = 0;
		}
		/** If competition is possible and the worker has developed ovaries they can lay eggs.
		For B. fervidus \cite<Hobbs1966> states an worker egg cells contian 3 or 4 eggs compared
		to 2-14 for the queen. Presumably B. terrestris would be similar. But maybe even lower
		(similar to the queens brood 1.
		\cite<Bloch1999a> gives the length of worker the oocyte as 2.5mm and \cite<Simons2018> the
		width as 0.6mm. A better option is likely \cite<Vogt1994> & \cite<Vogt1998> who give the
		mass of the ovaries for the queen as a proportion of their overal mass. Growing from
		1.4% to 4 - 4.5% of their mass. Workers will not be carrying as much fat, but given they
		are late in producing the juvinile hormone as adults as opposed to gynes having this hormone
		from the time of being a larva, and they lay far fewer eggs, their ovaries are likely much
		smaller. Therefore using 4% for both is probably fine.
		*/
		if ((m_CompetitionPossible == true || GetColony()->NoQueens == 0) && m_StageAge >= MinAgeWokersCompete && m_Mass
			.ReportPropOvaries() >= MaxPropOvaries)
		{
			m_NumberEggsPerCell.ResetQuantity(4);
		}
		ZeroEggsLaidToday();
		RecordVitals();
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::Step()
/**
Adult life stages have many more options of activities than juvininiles. They need to first briefly
develop and then for the rest of the step decide what to do.
*/
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState: // Initial state always starts with develop
		m_CurrentBoState = toBombusS_Decide;
		break;
	case toBombusS_Decide:
		m_CurrentBoState = st_Decide();
		break;
	//case toBombusS_RandomMove:
	//	m_CurrentBoState = st_RandomMove();
	//	break;
	case toBombusS_Sleep:
		m_CurrentBoState = st_Sleep();
		m_StepDone = true;
		break;
	//case toBombusS_DirectMove:
	//	m_CurrentBoState = st_DirectMove();
	//	break;
	//case toBombusS_Forage:
	//	m_CurrentBoState = st_Forage();
	//	break;
	case toBombusS_Consume:
		m_CurrentBoState = st_Consume();
		m_StepDone = true;
		break;
	case toBombusS_Inspect:
		m_CurrentBoState = st_Inspect();
		break;
	case toBombusS_AddWax:
		m_CurrentBoState = st_AddWax();
		m_StepDone = true;
		break;
	case toBombusS_Incubate:
		m_CurrentBoState = st_Incubate();
		m_StepDone = true;
		break;
	case toBombusS_Feed:
		m_CurrentBoState = st_Feed();
		m_StepDone = true;
		break;
	case toBombusS_Fan:
		m_CurrentBoState = st_Fan();
		m_StepDone = true;
		break;
	case toBombusS_LayEgg:
		m_CurrentBoState = st_LayEgg();
		m_StepDone = true;
		break;
	case toBombusS_Other:
		m_CurrentBoState = st_Other();
		m_StepDone = true;
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Worker::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::EndStep()
{

	/** In the first day assuming that the mother of the cluster "guards" the egg cell. */
	if(m_FocusCluster != nullptr && (m_FocusCluster->GetMinutesAge() >= 10 || m_FocusCluster->IsDead())){

		if(m_FocusCluster->GetIfFocusedOn())
		{
			m_FocusCluster->SetFocusedOnBy(nullptr);
		}
		m_FocusCluster = nullptr;

	}

	/** Develop every 10mins*/
	st_Develop();
	if (timestep % 144 == 0)
	{
		m_StageAgeDay += 1;
	}
	

	ResetTimesDominated();
}

void Bombus_Worker::IncrementColonyMaleEggs(int a_NumberOfEggs)
{
	GetColony()->NoWorkerMaleEggs += a_NumberOfEggs;
}

void Bombus_Worker::IncrementEggsLaidToday(int a_NumberOfEggs)
{
	m_EggsLaidToday.IncreaseQuantity(a_NumberOfEggs);
}


void Bombus_Worker::RemoveFemaleSelfFromColony()
{
	/**
	* Returns true if successful and false if the object is not found
	*/
	auto end = m_OurColony->m_ColonyFemales.end();
	for (auto i = m_OurColony->m_ColonyFemales.begin(); i < end; ++i)
	{
		if (*(i) == this)
		{
			m_OurColony->m_ColonyFemales.erase(i);
			return;
		}
	}
}

void Bombus_Worker::RemoveSelfFromColony()
{

	RemoveFemaleSelfFromColony();

	if (!m_Callow) {
		auto end2 = m_OurColony->m_ActiveColonyWorkers.end();
		for (auto ifemale = m_OurColony->m_ActiveColonyWorkers.begin(); ifemale < end2; ++ifemale)
		{
			if (*(ifemale) == this)
			{
				m_OurColony->m_ActiveColonyWorkers.erase(ifemale);
				break;
			}


		}
	}
}


bool Bombus_Worker::GetIfOvaryMax()
{
	if(GetOvaryProp() >= MaxPropOvaries)
	{
		return true;
	}
	return false;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::ChooseRandomCluster()
{
	int NumberClusters = int(GetColony()->m_ColonyClusters.size());
	if (NumberClusters > 0)
	{
		int randomIndex = random(NumberClusters);
		auto setIt = GetColony()->m_ColonyClusters.begin();
		setIt += randomIndex;

		if((*setIt)->GetIfNotFocusedOn())
		{
			
			m_FocusCluster =  (*setIt);
			(*setIt)->SetFocusedOnBy(this);
			
		}
		else{
			m_FocusCluster = nullptr;
		}
	}
	else{
		m_FocusCluster = nullptr;
	}
	
}



//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::SetStatic()
{
	m_EggDM = cfg_BombusEggDevelTotalDM.value();

	MaxSleep = cfg_BombusMaxSleep.value();
	MinSleep = cfg_BombusMinSleep.value();
	StomachCapacityPermgBodyWeight = cfg_BombusStomachCapacityPermgBodyWeight.value();




	MaxCocoonMass = cfg_BombusMaxCocoonMass.value();
	WaxEnergyDensity = cfg_BombusWaxEnergyDensity.value();
	DivisionEggAtDangerOfEaten = cfg_BombusDivisionEggAtDangerOfEaten.value();
	PropEggProtein = cfg_BombusPropEggProtein.value();
	VolumeLarvalFeeding = cfg_BombusVolumeLarvalFeeding.value();
	m_AgeMortMultiVectWorker = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiWorker.value(), 365 );

	CfgInt cfg_BombusWorkerNoLayDamageThreshold("BOMBUS_WORKERNOLAYDAMAGETHRESHOLD", CFG_CUSTOM, 100);
	m_WorkerNoLayDamageThreshold = cfg_BombusWorkerNoLayDamageThreshold.value();

}

//--------------------------------------------------------------------------------------------------------------------------------
int Bombus_Worker::GetMatesSexLocus()
{
	return 0;
}




//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Dying()
{
	/** In the first day assuming that the mother of the cluster "guards" the egg cell. */
	if(m_FocusCluster != nullptr){

		if(m_FocusCluster->GetIfFocusedOn())
		{
			m_FocusCluster->SetFocusedOnBy(nullptr);
		}
		m_FocusCluster = nullptr;

	}



	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
	/** remove worker self from the NoWorkers*/
	GetColony()->NoWorkers--;
	GetColony()->NoAdults--;
	GetColony()->RemoveAdult(this);
	if (InColony())
	{
		GetColony()->NoAdultCorpses++;
	}

	SetSexLocus_1(0);
	SetSexLocus_2(0);
	SetColony(nullptr);
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::CheckClusterEggs(Bombus_Cluster* a_FocusCluster)
{
	/** The worker can eat eggs that aren't their own if they know the competition point has passed.
	However if they encounter an egg which isn't the queen of the colonies egg, then they would know that the competition point has passed.
	This is also true of the queen.
	*/

	//Presumably all eggs in a cell will have the same mother. 
	auto i = a_FocusCluster->GetEggs()->begin();
	if (!a_FocusCluster->GetEggs()->empty() && (*i)->GetAgeDegrees() < m_EggDM / DivisionEggAtDangerOfEaten)
	{
		//if ((*i)->GetMotherID() != GetColony()->GetColonyID())
		//{
		//	m_CompetitionPossible = true;
		//}
		//Hardwired version where workers eat any young egg that isn't theirs. 
		if (g_rand_uni() < 1 &&  m_CompetitionPossible == true &&
			m_StageAge >= MinAgeWokersCompete &&
			m_Mass.ReportPropOvaries() >= MaxPropOvaries &&
			(*i)->GetMotherID() != GetMyID())
		{
			EatAllEggs();
		}
	}

}

bool Bombus_Worker::CanIAddWax()
{
	if (m_StageAge <= MaxAgeWorkerMakeWax * 1440.0)
	{
		return true;
	}
	return false;
}

bool Bombus_Worker::CanLayEggs ()
{
	
	if(m_Damage > m_WorkerNoLayDamageThreshold) {
			m_Senesce = true;
			return false;
	}


	if (m_NumberEggsPerCell.ReportQuantity() > 0 &&
			GetColony()->m_Pollen.ReportPollenMass() >= WorkersEggCellPollen &&
			(GetColony()->NoQueens == 0 || m_CompetitionPossible == true))
	{
		return true;
	}


	return false;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_AddWax()
{
	/** Workers may only produce wax between 2-7 days of age. After this presumably they would take it from
	elsewhere. This doesn't seem to hold with \cite<Beheshti2013> who mentions laying workers secreeting wax.
	Egg laying workers are usually older. \cite<Beheshti2013> mentions that worker egg cells are smaller than
	those of the queen (4-5mm high).
	I have no idea about the queen.
	The weight of empty cocoons increases with tempertature \cite<Zaragoza-Trello2020>.
	The cocoons weight 0.40 +/-0.11 grams. If we assume the mass scales with size of larva. Then for the mass of larva
	the cluster needs 0.40/MaxMass or possibly, 0.62/MaxMass (if we assume two standard deviations more is the max size).
	This does of course ignore temperature. There is still a lot of varience once temperature is accounted for.


	Based on a slightly tenuous source (https://www.beesource.com/threads/weird-questions-about-beeswax.213090/),
	beeswax is 12.7kcal/g or 53136.8j/g so 53j/mg
	*/

	double waxNeeded = (m_FocusCluster->ReportJuvMass() * MaxCocoonMass / MaxMass) - m_FocusCluster->ReportWax();
	//converted to mg
	double EnergyNeeded = waxNeeded * WaxEnergyDensity;
	EnergyNeeded = m_Mass.TakeEnergyFromFat(EnergyNeeded);
	//This should be added to the energy needed as well so they can consume what was lost. 
	//m_ActivityEnergy += EnergyNeeded; Scratch that, it would lead to double accounting.
	double WaxAvailable = EnergyNeeded / WaxEnergyDensity;
	/** This adds wax to the cluster, and updates how much is in the colony. */
	m_FocusCluster->AddWax(WaxAvailable);
	GetColony()->AddWax(WaxAvailable);

	AddTypeWax(WaxAvailable);

	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Dominate()
{
	/** The queen and worker isn't going to do anything to male if it encounters one, but it will know the switch must have happened. Therefore
	as a cheat instead of actually encountering a male, the probability of encountering one is used instead. If encountering one they can't use
	the same action to encounter an adult female and they set the switching point flag to true and return. */

	double ProbabilityMaleEncounter = double(GetColony()->NoMales) / double(GetColony()->
		NoAdults);
	if (g_rand_uni() <= ProbabilityMaleEncounter)
	{
		m_SwitchingPointDetected = true;
		return toBombusS_Dominate;
	}


	int NumberFemales = int(GetColony()->m_ColonyFemales.size());
	if (NumberFemales > 1)
	{
		int randomIndex = random(NumberFemales);
		auto setIt = GetColony()->m_ColonyFemales.begin();
		setIt += randomIndex;
		if (*(setIt) == this)
		{
			if (randomIndex < (NumberFemales - 1))
			{
				setIt += 1;
			}
			else
			{
				setIt = GetColony()->m_ColonyFemales.begin();
			}
		}
		double targetWorkerOvariesProp = (*setIt)->GetOvaryProp();
		double MyOvariesProp = m_Mass.ReportPropOvaries();
		/** Presumably when two workers encounter each other the
		less aggressive is dominated. If they are the same I assume
		they avoid conflict unless past the competition point. Unless
		they are overtly agressive I don't think we can add a particular
		energy demand.*/
		if (targetWorkerOvariesProp < MyOvariesProp)
		{
			(*setIt)->Dominate();
			(*setIt)->Damaged(m_CompetitionPossible);
		}
		else if (targetWorkerOvariesProp > MyOvariesProp)
		{
			Dominate();
			Damaged(m_CompetitionPossible);
		}
		else if (int(1000 * targetWorkerOvariesProp) == int(1000 * MyOvariesProp))
		{
			(*setIt)->Dominate();
			(*setIt)->Damaged(m_CompetitionPossible);
			Dominate();
			Damaged(m_CompetitionPossible);
		}
	}
	return toBombusS_Dominate;
}

TTypeOfBombusState Bombus_Worker::st_Consume()
{
	EatNectar();

	if ((m_TimesDominated <= DominationThreshold || m_AgeDegrees <= CallowForDM) && IsForager() == false)
	{
		EatPollen();
	}
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Develop()
{
	if (GetColony() != nullptr && GetColony()->GetCurrentStateNo() == -1)
	{
		m_StepDone = true;
		SetCurrentStateNo(-1);
		return toBombusS_Die;
	}
	Grow();
	if (GetColony() != nullptr && GetColony()->GetColonyTemp() >= CallowMinTemp)
	{
		AddAgeDegrees(10.0 * (GetColony()->GetColonyTemp() - CallowMinTemp));
	}
	m_Age += 10;
	m_StageAge += 10;
	timestep++;
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Decide()
{



	/**
	Workers incubate juviniles when they themselves feel cold, they fan the collony when they feel hot. I am assuming that they forage for food when they think there is not
	enough food.
	Size is a factor in all of these perceptions, cold, hot or a lack of food.
	Small bees feel the cold, are more resistent to heat and need less food.
	Large bees don't feel the cold, but don't like the heat and they notice a lack of food sooner.
	Having emerged as full adults, the workers take on brood care.
	*/
	/** Which comes first, inspecting the larva to see who needs feeding or going to collect food?
		Maybe the proceedure should be:
			- Collect food
			- if enough food
				-# feed juviniles
			-else
				-# Become forager
		Not sure how I would then have workers do other tasks.
	*/
	/**
	Throughout the day, worker need to feed themselves with the energy needed.
	If they have less energy then they "should" at that time of day, then they are hungry.
	If they aren't then they can do other things.
	But they can only do this if they are in the colony.
	*/

	/** Callows adults rest and eat while they dry. This is approximatly one day, I have set this as one day at 28C
	Warmer and they will dry out faster, .*/
	if (m_Callow == true && GetAgeDegrees() < CallowForDM)
	{
		if (int(1000 * m_Stomach.ReportVolumeFood()) == 0 || g_rand_uni() <
			MyProbabilityPrioritisingFood())
		{
			return toBombusS_Consume;
		}
		if (m_OurPopulationManager->IsDaylight == false || m_SleepWanted.ReportQuantity() > 0)
		{
			//What if not getting enough sleep causes a switch back to colony?
			return toBombusS_Sleep;
		}
	}
	/** Will only be entered once and will negate the code before if it has been entered.*/
	if (m_Callow == true && GetAgeDegrees() >= CallowForDM)
	{
		GetColony()->m_ActiveColonyWorkers.push_back(this);
		m_Callow = false;
		CalculateMyBodyTemp();
		AddToWarmAccount();
	}


	/** The workers will encounter other females. Inspecting she will choose a random cluster to inspect.
	We could assume that the worker as she moves between tasks encounters others. This means that this isn't
	necessarily a seperate task for the worker.
	She is unlikely to need 10mins for the task. Therefore she could encounter multiple workers.
	Less likely to try it on when her ovaries are small. As they mature she becomes more aggressive.
	#V2 #V3 improvement would be to adapt this to invading queen deffence. Possibly cuckoos as well.

	Initially we ignored adult males, but they are still adults the worker will encounter. This may be important
	as a trigger for switching and competition points. It will also dilute any domination of other workers as she
	proportionally fewer females.
	*/
	if (GetColony()->NoAdults > 1)
	{
		for (int i = 0; i < AdultsEncounteredInStep; i++)
		{
			st_Dominate();
		}
	}


	if(m_FocusCluster != nullptr)
	{
		return toBombusS_Incubate;
	}


	/** I think this should mean non-foragers should sleep on and off throughout the day and night.
	If they get within 4 hours of the end of the day (how much sleep they need), they will sleep, but
	they will still wake up. But the probability of sleeping will still be 1. This isn't perfect, as
	I am sure they could run a deficit. There might be a way to allow a deficit. I have made the sleep
	needed additional, so if they don't get the sleep, they'll need it tommorow. Could weight the probability,
	0.02 times the m_SleepNeeded would reduce the probability of just before 1440 of only being 0.96 (if they can't sleep).
	Not sure its worth it.
	*/
	if ((m_Forager == true && m_OurPopulationManager->IsDaylight == false && m_SleepWanted.ReportQuantity() > 0) ||
		(m_Forager == false && g_rand_uni() < MyProbabilitySleeping()))
	{
		//What if not getting enough sleep causes a switch back to colony?
		return toBombusS_Sleep;
	}
	if (InColony() && (int(1000 * m_Stomach.ReportVolumeFood()) == 0 || g_rand_uni() <
			MyProbabilityPrioritisingFood())
	)
	{
		return toBombusS_Consume;
	}

	if (m_Forager == false)
	{
		/** If the worker is not a forager, then they can do multiple things. Incubate juviniles, fan the colony or feed the larva for example.
		Workers inspect clusters of juviniles. If they are larva, they feed them.
		*/
		/** I have set the eggs to appear at the begining of the day based on conditions.
		Therefore if the worker hasn't been busy doing all of these other tasks and has
		eggs then why not give it a go. */

		/** I don't know if a worker can lay every day after the competition point.
		Might make more sense for a worker to look after own cluster exclusivly*/

		/** If the worker can physically lay an egg,
		there are resources, and
		either the competition point has passed or there is no queen, then
		the individual can lay eggs.*/
		if (CanLayEggs())
		{
			return toBombusS_LayEgg;
		}


		if (!GetColony()->m_ColonyClusters.empty() && g_rand_uni() <= PropBroodCare)
		{
			return toBombusS_Inspect;
		}

		/** With nothing else to do, they grab some extra shut eye.*/
		if (m_SleepWanted.ReportQuantity() > 0)
		{
			return toBombusS_Sleep;
		}
	} //else they are a forager.
	//else {
	//	if (AccessForaging == true && GetColony()->IsLight() == true ) {
	//		return toBombusS_Forage;
	//	}
	//	else if (GetColony()->IsDark() == true) {
	//		/** Foragers sleep something like 8 hours a night. Therefore if not enough daylight I'll assume they sleep.*/
	//		return toBombusS_Sleep;
	//	}
	//	else if (g_rand_uni() > MyProbabilitySleeping()) {
	//		return toBombusS_Sleep;
	//	}
	//	else {
	//		m_StepDone = true;
	//	}
	//}
	m_StepDone = true;
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Inspect()
{
	/** Inspecting will involve walking over to a cluster, opening the cluster
	and inserting antena to assess the individuals inside. Presumably this
	will require energy above maintenance, but I don't know what it is.*/

	if (StepEnergyInspection > 0)
	{
		double EnergyNeeded = StepEnergyInspection * m_Mass.ReportMass();
		if (m_Mass.ReportEnergyInFat() < EnergyNeeded)
		{
			return toBombusS_Decide;
		}
		if (EnergyNeeded > 0.0)
		{
			m_Mass.TakeEnergyFromFat(EnergyNeeded);
		}
	}


	if (int(GetColony()->m_ColonyClusters.size()) > 0)
	{
		ChooseRandomCluster();
		if(m_FocusCluster == nullptr)
		{
			m_StepDone = true;
			return toBombusS_Decide;
		}
		


		//**************
		//Check the eggs
		//**************
		CheckClusterEggs(m_FocusCluster);
		if (m_FocusCluster == nullptr)
		{
			m_StepDone = true;
			return toBombusS_Decide;
		}

		//**************
		//Decide whether to incubate eggs.
		//**************

		/** Not sure what is the priority, raising the temperature, or feeding larva. I have programmed a probability of incubation
		based on how much higher than the minimum the temperature is. This might be backwards and should probably be based on temp
		less than optimal.*/
		double temp = m_FocusCluster->GetTemp();
		double probIncub = 0.0;
		if (temp < OptimumDevTemp)
		{
			probIncub = ProbV1(temp);
			//probIncub2 = ProbV2(temp);
		}
		if (g_rand_uni() <= probIncub)
		{
			return toBombusS_Incubate;
		}

		//**************
		//Decide whether to add wax
		//**************

		if (CanIAddWax() && m_FocusCluster->ReportWax() < m_FocusCluster->ReportJuvMass() * 0.40 / MaxMass)
		{
			return toBombusS_AddWax;
		}


		//**************
		//Check the larva
		//**************

		int HungryLarva = 0;


		auto end = m_FocusCluster->GetLarva()->end();
		for (auto i = m_FocusCluster->GetLarva()->begin(); i != end; ++i)
		{
			/** In the first days as larva, determining developement time based on queen interaction.*/
			if ((*i)->GetAgeDegrees() < LarvaGyneDMcutoff)
			{
				IncrementLarvaeInteractions(*i);
			}
			/** Larva emit a hunger signal \cite<Smeets2017>*/
			if ((*i)->Hungry() == true)
			{
				HungryLarva++;
			}
			/** While inspecting if they detect a gyne then they know the switch has happened*/
			if ((*i)->m_IAmGyne == true)
			{
				m_SwitchingPointDetected = true;
				m_numberGyneLarvaToday += 1;
			}

			/** If a worker detects a male larva (or egg?) wouldn't they know the switch had also taken place? If they then don't encounter any gynes
			then they would know the competition point has been reached.*/
			if ((*i)->m_IAmMale == true)
			{
				m_SwitchingPointDetected = true;
			}
		}

		//**************
		//Decide if need to feed larva. 
		//**************

		if (HungryLarva > 0)
		{
			st_Feed();
		}
		//ClustersToInspect--;
	}




	/** If the inspect is returning to decide, then the step is done.*/
	m_StepDone = true;
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_LayEgg()
{
	/**
	* Creates a new egg object and passes the data from the queen to it.
	*/
	struct_Bombus sBo;
	//This structure for Bombus is in its population manager. Each transition, or line of transition, may need a different struct
	sBo.BPM = m_OurPopulationManager;
	sBo.L = m_OurLandscape;
	sBo.x = m_Location_x;
	sBo.y = m_Location_y;
	sBo.MotherID = m_id;

	/** If the FatherID is Null then this is a haploid egg, otherwise diploid.*/
	if (m_SwitchingPointDetected == true)
	{
		sBo.FatherID = 0;
		sBo.SexLocus_2 = 0;
	}
	else
	{
		sBo.FatherID = m_SpermID;

		sBo.SexLocus_2 = GetMatesSexLocus();
	}
	sBo.Colony = GetColony();
	int NumberOfEggs = m_NumberEggsPerCell.ReportQuantity();
	/** An egg cell takes energy to create. Presumably similar to flesh (see grow). So the mass and 7j/mg*/
	double TotalMass = NumberOfEggs * MinMass;
	double eggEnergy = TotalMass * EnergyDensityEgg;

	/** Take this energy if it is available from fat reserves.*/
	if (m_Mass.ReportFatMass() >= eggEnergy)
	{
		m_Mass.TakeEnergyFromFat(eggEnergy);
	}
	else
	{
		NumberOfEggs = int(floor(m_Mass.ReportFatMass() / (MinMass * EnergyDensityEgg)));
		TotalMass = NumberOfEggs * MinMass;
		eggEnergy = TotalMass * EnergyDensityEgg;
		m_Mass.TakeEnergyFromFat(eggEnergy);
	}
	if (NumberOfEggs > 0)
	{
		/** The queen creates a cluster with a number of eggs. */
		m_FocusCluster = sBo.Cluster = m_OurPopulationManager->CreateCluster(to_Cluster, &sBo, NumberOfEggs);
		m_FocusCluster->SetFocusedOnBy(this);

		//CreateCluster(NumberEggs, m_OurColony);
		m_FocusCluster->SetJuvMass(TotalMass);
		m_FocusCluster->AddPollenLump(PollenLumpToAdd());
		st_AddWax();
		/** Instead of laying a number of eggs in one go, having the queen iterate here instead of in CreateObject allows
		the queen to impart different "gentics" to each egg. This is only one of her two sex allels at the moment.*/
		for (int i = 0; i < NumberOfEggs; i++)
		{
			if (g_rand_uni() <= 0.5)
			{
				sBo.SexLocus_1 = GetSexLocus_1();
			}
			else
			{
				sBo.SexLocus_1 = GetSexLocus_2();
			}
			m_OurPopulationManager->CreateObjects(to_Egg, nullptr, &sBo, 1);
			if (sBo.FatherID == 0 || sBo.SexLocus_1 == sBo.SexLocus_2)
			{
				IncrementColonyMaleEggs(1);
			}
			GetColony()->NoEggs++;


		}
		IncrementEggsLaidToday(NumberOfEggs);
		m_NumberEggsPerCell.ResetQuantity(0);
		
		
	}
	return toBombusS_Decide;
}

////--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Worker::st_RandomMove()
//{
//
//	/** 
//	I have based this movement on \cite<Sprayberry2018> who had bumblebees turning as fast as they can (0.1seconds) traveling at 3m/s and using a CRW based on \cite<Heinrich1979>.
//	I generated the paths of these movements and then sampled at 1 minute intervals and determined the distributions of turning angles and movement over those minutes. 
//
//	However, \cite<Osborne1999> seem to show bumblebees moving between 3m/s and 15.6m/s in much straighter paths.  \cite<Riley1999> showed average speeds of 6.2-7.3 m/s (depending on the month).
//	It is possible these paths are once bumblebees had established the location of resources as their return paths were similar to their outgoing path. 
//	
//	*/
//
//	//I have the bee jumping from here to there without passing through the space in between. I think in the Osmia and honey bee model might look at things along the line. 
//	//Given that we are simplifying a more tortuous path it might be good if they checked out what was either side of the line as well. Need to be careful I don't needlessly increase
//	//computation time. 
//	std::random_device r;
//	std::default_random_engine generator(r());
//	std::cauchy_distribution<double> distribution(0.0, 2.0);
//	double turn = distribution(generator) * PI / 180;
//
//	std::default_random_engine generator2(r());
//	std::gamma_distribution<float> GammaDistribution(5, 3);
//	double move = GammaDistribution(generator2);
//
//	Heading = fmod(Heading + turn + 2 * PI, 2 * PI);
//
//	int x = int(round(double(m_Location_x) + sin(Heading) * move));
//	int y = int(round(double(m_Location_y) + cos(Heading) * move));
//	m_OurLandscape->CorrectCoords(x, y); // For wrap around
//
//	m_Location_x = x;
//	m_Location_y = y;
//
//
//
//	double EnergyUsed = 20; //1.2 KJ/h or 20 j/min This will likely vary based on the size of the bee, I need to check. But I think flight it quite efficient so it might not vary enough to worry about. 
//	//16.7432 J per g of sugar?? Not sure that is right
//	// 17 kilojoules per gram of sucrose (wikipedia
//	// 15.4 KJ/g fructose
//	// 16.2 kilojoules per gram or 15.7 kJ/g glucose
//	// 12.7 KJ/g of Honey
//	//if a bee forages for 57 to 75 mins (check this as this is from memory), then they use 1.14 - 1.5 Kj 
//
//	//Based on one single graph in Ellinton et al. 1990 I recon that resting the use about 0.83 j/min and walking about 1.73 j/min but that there is also a recovery period from flying where they use more 
//	//~8.9 j/min which takes about half a minute (maybe as high as 13 j/min).
//
//	return toBombusS_Decide;
//}
//--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Worker::st_DirectMove(APoint Target, APoint Origin)
//{		
//	int TargetX = Target.m_x;
//	int TargetY = Target.m_y;
//	int dx = TargetX - Origin.m_x;
//	int dy = TargetY - Origin.m_y;
//	double distance = pow((pow(double(dx), 2.0) + pow(double(dy), 2.0)), (0.5));
//
//	if (dy == 0) {
//		if (dx >= 0) {
//			Heading = PI * 90.0 / 180.0;
//		}
//		else if (dx < 0) {
//			Heading = PI * 270.0 / 180.0;
//		}
//	}
//	else if (dx >= 0 && dy > 0) {
//		Heading = atan(dx / dy);
//	}
//	else if (dy < 0) {
//		Heading = PI + atan(dx / dy);
//	}
//	else if (dx < 0 && dy > 0) {
//		Heading = (2 * PI) + atan(dx / dy);
//	}
//	else {
//		cout << "Heading error.";
//	}
//	/** \cite<Riley1999> showed average speeds of 6.2 -7.3 m/s (6.2 in two months and 7.3 in a third, therefore ~6.5m/s or 394m/min)*/
//	travelTime = int(round(distance / 394.0)); //394 m/min
//	m_ForagingTime -= travelTime;
//	double EnergyUsed = 20.0 * double(travelTime); //supposed 20 joules per minute flying.
//	m_Location_x = TargetX;
//	m_Location_y = TargetY;
//	return toBombusS_Decide;
//}
//--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Worker::st_Forage()
//{
//	/** How long they will spend foraging*/
//	m_ForagingTime = random(75);
//	/** The location of the colony*/
//	APoint ColonyPoint = GetColony()->SupplyPoint();
//	/**
//	Based on cite/<Goulson2002a> most B. terrestris foragers return with nectar alone (~53%), followed by nectar and pollen (~42%) and Pollen alone (~5%).
//	"47.90+/-1.42 mg of forage"
//	"Both 59.6 +/- 2.37 mg"
//	"Nectar 47.1 +/- 1.80 mg"
//	"Pollen 26.3 +- 3.47mg"
//	Mass of forage (mg)=19.1 (thorax width (mm))?65
//	"The 433 foragers carried a mean+=/-SE mass of pollen and/or nectar equivalent to 23.10+0.01% of their unladen body mass. The heaviest load carried amounted to 77.1% of the bee’s body mass."
//	Based on this, the Pollen
//	*/
//	double Chance;
//	/** Queen forages when there are no workers, otherwise it is the workers who are foraging*/
//	if (GetColony()->NoWorkers == 0) {
//		/** When their are larva, the queen will need to forage for pollen if they need it or nectar alone for her self*/
//		if (GetColony()->NoLarva > 0) {
//			double AgeLarvaCluster =0;
//			vector<Bombus_Cluster*>::iterator setIt;
//			for (setIt = GetColony()->m_ColonyClusters.begin(); setIt != GetColony()->m_ColonyClusters.end(); ++setIt) {
//				if ((*setIt)->GetNumberLarva() > 0) {
//					AgeLarvaCluster = -999;
//				}
//			}
//			/** If the larva need pollen, then the queen should forage for both pollen and nectar, but if they have enough pollen, she only needs to fill up her reserve of nectar*/
//			double LarvaPollen;
//			if (AgeLarvaCluster == 0) {
//				LarvaPollen = 0;
//			}
//			else {
//				LarvaPollen = GetColony()->NoLarva * 0.002 * pow(AgeLarvaCluster, 4.3719);
//			}
//			if (LarvaPollen > PollenFedToLarva) {
//				Chance = 0.6;
//			}
//			else {
//				Chance = 0.1;
//			}
//		}/** Before the eggs are laid some pollen is also needed*/
//		else if (GetColony()->m_Pollen.ReportQuantity() < EggCellPollen &&  (GetColony()->Brood1EggsLayed == false || (GetColony()->Brood1LarvaPupated && GetColony()->Brood2EggsLayed == false))) {
//			Chance = 0.6;
//		}
//		else {
//			Chance = 0.1;
//		}
//	}
//	else {
//		Chance = 0.6;// g_rand_uni();
//	}
//	if (Chance <= 0.53) {
//		//Nectar only
//
//		///** Based on 23.1% of body mass equaling the 47.9mg mean forage, then just nectar is 22.7% of body mass*/
//		//m_Nectar_capacity = 0.227 * m_Mass * 1000;
//		/** 
//		Volume of nectar per body size Nectar_mL= 0.343862 (±0.040680 SE)×W_g-0.046322 (±0.046322 SE). The maximum capacity would likely be the upper limit (3 times the SE)
//		m_Nectar_capacity = 0.465902 * m_Mass/1000 + 0.067475
//		But the individual will likely most often turn around well before this.
//		*/
//		m_Nectar_capacity = 0.465902 * m_Mass.ReportLeanMass() / 1000 + 0.067475; //mL or cm3
//		m_Pollen_capacity = 0;
//	}
//	else if (Chance <= (0.53 + 0.42) ){
//		//Nectar and pollen
//
//		m_Pollen_capacity = 15;//This is a guesstimate based on both being 59.6mg combined nectar and pollen and nectar alone being 47.1mg. Pollen was less or not dependent on size of bee in cite/<Goulson2002a>
//		//15mg reduces the nectar a bit, but not entirely
//
//		//m_Nectar_capacity = (0.227 * m_Mass * 1000);
//		m_Nectar_capacity = 0.465902 * m_Mass.ReportLeanMass() / 1000 + 0.067475; //mL or cm3
//		/** This is an entirely made up, it is simply assuming the full load of nectar with the rest made up of pollen (from the 59.6 mg of both from \cite<Goulson2002a> p.126*/	
//	}
//	else {
//		//Pollen only
//		/** Pollen is less size dependent, should probably pull from a distribution, but we may work this out from the landscape.*/
//		m_Pollen_capacity = 26.3;
//		m_Nectar_capacity = 0;
//	}
//	//Workers scout for somewhere to forage themselves and move on with a little nectar only if the nectar isn't great quality. Once they have found a good source though they will return straight to the location. 
//	//Not sure what search behaviour looks like. Gynes searching for hibernation and colony sites search with a zig zag
//	/** My current location*/
//	APoint MyPoint;
//	MyPoint = SupplyPoint();
//	double distance;
//	//There has to be a better way to do this. 
//	while ((m_Nectar_capacity > m_Stomach.ReportQuantity() || m_Pollen_capacity > m_PollenLoad.ReportQuantity()) &&  m_OurPopulationManager->IsDaylight == true) {
//		if (m_OurColony != 0) {
//			double TargetX = ColonyPoint.m_x;
//			double TargetY = ColonyPoint.m_y;
//			double dx = TargetX - m_Location_x;
//			double dy = TargetY - m_Location_y;
//			distance = pow((pow(dx, 2) + pow(dy, 2)) ,(0.5));
//			if (distance < 0) {
//				cout << "";
//			}
//			travelTime = int(round(distance / 394.0));
//			if ((m_ForagingTime - travelTime) <= 0) {
//				break;
//			}
//		}
//		PollenNectarQuality Nectar;
//		/** If I don't have a destination in mind I will CRW */
//		while (m_GoodForage.m_x == -1 && m_GoodForage.m_y == -1 ) {
//			st_RandomMove();
//			/** Having randomly moved I'll check if there is nectar*/
//			MyPoint = SupplyPoint();
//			//cout << "Landscape element: " <<  m_OurLandscape->SupplyElementType(MyPoint.m_x, MyPoint.m_y)<< "\n";
//			//cout << "vegetation: " << m_OurLandscape->SupplyVegType(MyPoint.m_x, MyPoint.m_y) << "\n";
//
//			//cout << "quality: " << m_OurLandscape->SupplyNectar(MyPoint.m_x, MyPoint.m_y).m_quality << "\n";
//			//cout << "quantity: " << m_OurLandscape->SupplyNectar(MyPoint.m_x, MyPoint.m_y).m_quantity << "\n";
//			//cout << "nectar: " << m_OurLandscape->SupplyTotalNectar(MyPoint.m_x, MyPoint.m_y) << "\n";
//			Nectar = m_OurLandscape->SupplyNectar(MyPoint.m_x, MyPoint.m_y);
//			if (Nectar.m_quantity > 0 && Nectar.m_quality > 0) { //maybe this should be multiple choice, if nectar but low quality, suck up a bit and keep searching for better. 
//				/** If there is nectar I'll set the forageing location, otherwise it stays as -1,-1*/
//				m_GoodForage = MyPoint;
//			}
//		}
//		/** If the while loop is bypassed because I have a destination in mind, I'll go to that location*/
//		if (m_OurColony != 0 && ( m_GoodForage.m_x != -1 || m_GoodForage.m_y != -1 )&& (m_GoodForage.m_x != MyPoint.m_x || m_GoodForage.m_y != MyPoint.m_y)) {
//			st_DirectMove(m_GoodForage, MyPoint);
//		}
//		/** If there is no nectar here I'll delete this destination, else I'll consume some nectar*/
//		MyPoint = SupplyPoint();
//		Nectar = m_OurLandscape->SupplyNectar(MyPoint.m_x, MyPoint.m_y);
//		if (Nectar.m_quantity == 0 || Nectar.m_quality == 0) {
//			m_GoodForage.m_x = -1;
//			m_GoodForage.m_y = -1;
//		}
//		else {
//			//suck up nectar
//			//m_OurLandscape->SupplyNectar(MyPoint.m_x, MyPoint.m_y);
//			/** \cite<Muth2015> looks at nectar handling time of simple and complex flowers. This time seems to cary from a couple of seconds upto 60s*/
//			//These are temporary nonsence numbers. 
//			//if a foraging trip takes 45mins, is that 30mins traveling and then 15mins foraging? Maybe if moving 3m/s and searching, but if moving at 16m/s then they would max distance in 2 or 3 mins. 
//			//This would leave ~40 mins to forage. And they would collect 0.002632876 mL per min
//			//I don't think this is fast enough, the queen is struggling to feed herself using 0.002ml per min. I think it should be 0.002ml per second. But I think this might be ~0.004ml for a queen. 
//			double UptakeRate_s = (0.0025 * m_Mass.ReportLeanMass() + 0.0016);
//			//The bee won't be feeding continually. If they take 2 or 3 seconds beween each flower, then a minute would contain 15-20s
//			double UptakeRate_m = UptakeRate_s * 15;
//			double RemainingNectarCapacity = m_Nectar_capacity - m_Stomach.ReportQuantity();
//			double PercNect = m_OurLandscape->SupplyNectar(MyPoint.m_x, MyPoint.m_y).m_quality / 3;
//			if (RemainingNectarCapacity >= UptakeRate_m) {
//				m_Stomach.DepositQuantity(UptakeRate_m);//ml
//
//				m_SugarLoad.DepositQuantity(1000 * 1.587 * UptakeRate_m * PercNect);//mg //I don't know what the quality values are. I also can't be bothered looking at the max value at the mo have assumed 6.
//
//				//If nectar quality isn't great, then the individual should move on. Kind of want them, to random move, and see if nectar is better, but if not move back. 
//			}
//			else {
//				m_Stomach.DepositQuantity(RemainingNectarCapacity);
//				m_SugarLoad.DepositQuantity(1000 * 1.587 * RemainingNectarCapacity * m_OurLandscape->SupplyNectar(MyPoint.m_x, MyPoint.m_y).m_quality / 3.0);//mg //I don't know what the quality values are. I also can't be bothered looking at the max value at the mo have assumed 6.
//			}
//			//Pollen gathering is harder so I have added an aditional time penalty. There should possibly be an aditional mortality risk as well. 
//			double RemainingPollenCapacity = m_Pollen_capacity - m_PollenLoad.ReportQuantity();
//			if (RemainingPollenCapacity >= 1) {
//				m_PollenLoad.DepositQuantity(1);
//				m_ForagingTime += -1;
//			}
//			else if(RemainingPollenCapacity > 0){
//				m_PollenLoad.DepositQuantity(RemainingPollenCapacity);
//				m_ForagingTime += -1;
//			}
//			m_ForagingTime += -1;
//			double EnergyUsed = 20;
//		}
//	}
//	/** Those with a colony will return to it and deposit their foraged food.*/
//	if (m_OurColony != 0) {
//		if (ColonyPoint.m_x != MyPoint.m_x || ColonyPoint.m_y != MyPoint.m_y) {
//			st_DirectMove(ColonyPoint, MyPoint);
//		}
//		if (GetColony()->NoLarva > 0) {
//			cout << "";
//		}
//		if (GetColony()->NoWorkers > 0 || GetColony()->NoLarva == 0) {
//			//Not sure how we are going to queue bees. Or maybe just assume that they are and just increase the time that storing nectar and pollen takes with number of foragers divided by number of pots. 
//			GetColony()->m_Nectar.DepositQuantity(m_Stomach.TakeAll());//ml or cm3
//			GetColony()->m_Sugar.DepositQuantity(m_SugarLoad.TakeAll());//mg
//			if (GetColony()->m_Nectar.ReportQuantity() <= 0) {
//				GetColony()->m_NectarSugarProp = 0;
//			}
//			else {
//				GetColony()->m_NectarSugarProp = GetColony()->m_Sugar.ReportQuantity() / (1000 * GetColony()->m_Nectar.ReportQuantity() * 1.587);//% //the mg of sugar to grams divided by 1.587 gives the volume and then volume of sugar divided by volume of nectar gives prcentage.
//			}
//			GetColony()->m_Pollen.DepositQuantity(m_PollenLoad.TakeAll());
//		}
//		else {
//			vector<Bombus_Cluster*>::iterator setIt;
//			for (setIt = GetColony()->m_ColonyClusters.begin(); setIt != GetColony()->m_ColonyClusters.end(); ++setIt) {
//				double NLarva = (*setIt)->GetNumberLarva();
//				if (NLarva > 0) {
//					double Sugar;
//					if (m_PollenLoad.ReportQuantity() > 0) {
//						Sugar = 0.3 * m_PollenLoad.ReportQuantity() / 0.7;
//					}
//					else {
//						Sugar = 0;
//					}
//					if (m_SugarLoad.ReportQuantity() >= Sugar) {
//						double Perc;
//						if (m_SugarLoad.ReportQuantity() == 0) {
//							Perc = 0;
//						}
//						else {
//							Perc = Sugar / m_SugarLoad.ReportQuantity();
//						}
//						double Nectar = Perc * m_Stomach.ReportQuantity();
//						//ACluster->AddFood(m_PollenLoad + Sugar);
//						PollenFedToLarva += m_PollenLoad.TakeAll();	
//						m_Stomach.TakeQuantity(Nectar);
//						m_SugarLoad.TakeQuantity(Sugar);
//					}else{
//						double ColonySugar = Sugar - m_SugarLoad.ReportQuantity();
//						ColonySugar = GetColony()->m_Sugar.TakeQuantity(ColonySugar);
//						double AllSugar = m_SugarLoad.TakeAll() + ColonySugar;
//						GetColony()->m_Nectar.TakeQuantity(ColonySugar / (1.587 * 1000 * GetColony()->m_NectarSugarProp));
//						m_Stomach.ResetQuantity(0);
//						double Pollen = m_PollenLoad.ReportQuantity() * AllSugar / Sugar;
//						//ACluster->Food += Pollen + AllSugar;
//						m_PollenLoad.TakeQuantity(Pollen);
//						PollenFedToLarva += Pollen;
//					}
//					GetColony()->m_Nectar.DepositQuantity(m_Stomach.TakeAll());
//					GetColony()->m_Sugar.DepositQuantity(m_SugarLoad.TakeAll());
//					//Could have queen eat pollen, or just leave it on herself until she finds nectar. 
//					//std::set<TAnimal*>::iterator setItLarva = ACluster->Larva.begin();
//					//double n = random(NLarva);
//					//std::advance(setItLarva, n);
//					//TAnimal* a = *setItLarva;
//					////Bombus_Larva* b = a;
//					////b->Fed(1.0);
//					//Bombus_Larva *b = [a];
//					//cout << a;
//				}
//
//			}
//
//		}
//	}
//	// I could have the time used worked out towards the end. Or work out amounts from flours and times of each plus travel time and turn back when we get to that time.
//	///** time=?0.28 (thorax width)+2.16* \cite<Goulson2002a>p.127 */
//	//double timeUsed = ceil(60 * (-0.28 * m_ThoraxWidth + 2.16));
//	//m_MinsLeft += -timeUsed;
//	//m_DaylightLeft += -timeUsed;
//	return toBombusS_Decide;
//}
//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::EatAnEgg(Bombus_Egg* a_AnEgg)
{
	/** If we assume the eggs are ~70% protein \cite<Kouřimská2016> and 30% fat.*/
	double MassEggs = MinMass;
	double MassProtein = PropEggProtein * MassEggs;
	double MassFat = MassEggs - MassProtein;
	double Energy = MassEggs * EnergyDensityEgg; //The same as when laying an egg based on flesh. 
	//I don't know how to get volume.
	Food SomeFood;
	SomeFood.AddFood(0, MassFat, MassProtein, Energy, 0);
	m_Stomach.PointToFood()->AddFoodPacket(SomeFood);
	a_AnEgg->Eaten();
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Worker::EatAllEggs()
{
	int NoEggs = m_FocusCluster->GetNumberEggs();
	/** If we assume the eggs are ~70% protein \cite<Kouřimská2016> and 30% fat.*/
	double MassEggs = MinMass * NoEggs;
	double MassProtein = PropEggProtein * MassEggs;
	double MassFat = MassEggs - MassProtein;
	double Energy = MassEggs * EnergyDensityEgg; //The same as when laying an egg based on flesh. 
	//I don't know how to get volume.
	Food SomeFood;
	SomeFood.AddFood(0, MassFat, MassProtein, Energy, 0);


	m_Stomach.PointToFood()->AddFoodPacket(SomeFood);

	m_FocusCluster->EatAllClusterEggs();
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Incubate()
{
	/**
	"Silvola (1984) estimated that a B. terrestris queen uses about 600 mg of sugar per day at temperatures typical for central Europe, and that
	to obtain this she may visit up to 6,000 flowers" \cite<Goulson2010>
	*/
	/**
	The cluster needs to be heated. We can again use Newtons law of cooling (or warming in this case).
	T(t) = Ts + (T0 -Ts)e^-kt
	T(t) in this case is the optimal.
	T0 the clusters starting temp
	Ts the workers temp (~37C)
	k can be got from the cluster now with Get_k();
	We can rearange to work out how much time is needed to heat the cluster to optimal. Or we could interupt the process and have workers heat
	them a bit (how much time they have less than what would be needed) and the therefore work out what there new temp would be.
	*/
	double ClusterTemp = m_FocusCluster->GetTemp();
	if (ClusterTemp >= OptimumDevTemp)
	{
		return toBombusS_Decide;
	}
	m_FocusCluster->SetState(toBombusS_Warm);
	double k = m_FocusCluster->Get_k();
	double t = log((OptimumDevTemp - m_Temp) / (ClusterTemp - m_Temp)) / (-k);
	if (t > 600.0)
	{
		//10mins in seconds)

		/** Incubation should take approximatly (-0.0005Airtemp(C) + 0.0201)*mass j/min
		from the energy stores of the adult incubating.*/

		double NewTemp = m_Temp + ( ClusterTemp - m_Temp) * exp(-k * (600.0));
		m_FocusCluster->SetTemp(NewTemp);

		//return toBombusS_Incubate;
	}else
	{
		m_FocusCluster->SetTemp(OptimumDevTemp);
	}
	
	m_FocusCluster->SetState(toBombusS_Cool);
	//Assume that they can sleep longer than incubation would be needed?
	timeInState += 10;
	m_SleepNeeded.DecreaseQuantity(10);
	m_SleepWanted.DecreaseQuantity(10);

	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Feed()
{
	/** If the worker alread has some food resources on board it should feed the larva, otherwise it needs
	to go and get some more.
	Presumably the amount of energy they use is tiny compare to what is absorbed when carrying food.
	*/
	//if (int(1000.0 * m_Stomach.ReportVolumeFood()) == 0 || m_Stomach.ReportPollenOnBoard() == false)
	//{
		/** if there is enough food, the worker should take some of the resources onboard.*/
		//I couldn't think of a better way to do this. But there has to be. 
		EatNectar();
		EatPollen();
		/** \cite<Pereboom2000> states that workers drink nectar and then eat pollen. Then containing 7.5 +/- 3.4 ug
		protien per ul of stomach contents. This is up from 0.3+/-0.3 ug/ul of nectar alone.*/
	//}
	/**
	How much energy is in the food? Sugar contains 15.76 J/mg of table sugar; sucrose is 16.2 J/mg, honey is 12.72 J/mg.
	Energy in pollen: 412.07 Kcal/100g /cite<Dranca2020> which is 1724.10088 Kj/100 which is 17.24 J/mg
	For now I can do a proportional average of honey and pollen. Say 30:70 s0 15.88 J/mg
	*/
	/** The number of feedings change for the cluster as the cluster ages. If time was finctioning in this model continuosly, then I wouldn't
	need to fudge this*/
	//double TotalNumberFeedings = int(100 * 0.1 * exp(0.372 * a_Cluster->Age) * a_Cluster->GetNumberLarva());
	//TotalNumberFeedings -= a_Cluster->Feedings;

	//while (ClustersToInspect > 0) {

	if (FeedLargestFirst == true)
	{
		sort((*m_FocusCluster->GetLarva()).begin(), (*m_FocusCluster->GetLarva()).end(), CompareMassLarva);
	}
	auto i =  m_FocusCluster->GetLarva()->begin();
	while (m_Stomach.ReportVolumeFood() > 0 && i !=  m_FocusCluster->GetLarva()->end())
	{
		/** 5.6 mg is the mean amount of food given to queen larva in /cite<Ribeiro1999> but the amount varied between 0.6 and 40.2 mg.
		Could this be due to variation in food quality? /cite<Ribeiro1999> doesn't say what they are fed, but references
		/cite<Duchateau1988> who mention not feeding once the workers forage. Therefore food could fluctate. Volume
		could be constant, but the amount of sugar vary. Although /cite<Ribeiro1999> do state that these numbers were
		obtained when the worker had access to as much food as they need. They suggest workers may vary this based
		on the need of the individual.
		\cite<Pereboom2000> retrieved 0.88ul (=/- 0.47) from the larva, with only a slight correlation with the age/size of
		the larvae. They suggest this could partly be because it is easier to collect from a larger larvae. They suggest they
		are all fed the same amount per feeding. Therefore I am going to feed them 0.88ul.
		Looking at /cite<Duchateau1988> I can't work out the mg amounts of food. I suppose this includes pollen density.
		*/
		if (int(1000.0 * (*i)->FoodHeld()) == 0)
		{
			(*i)->Feed(m_Stomach.Regurgitate(VolumeLarvalFeeding));
		}
		++i;
	}

	/** I don't know if workers keep coming back to a cluster until they have fed everyone, but i have assumed here
	that if they came back to feed some larva, they fed until they run out of food, and then forget the specific cluster.
	Workers have been observed feeding some of a cluster, this could be because some aren't hungry (already fed) and/or
	they run out of food.
	*/


	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Worker::st_Sleep()
{
	/** Workers and queens likely sleep a different amount when looking after larva,
	Possibly not in one go. Looking at \cite<Nagari2019> Figure 2, the bees appear to sleep in 20-25min bouts.
	For ease of fitting into hours, I am going with 20mins. Foragers will sleep the full amount, but they will just return here.
	Energy usage is now maintenance only.
	*/
	/** Presumably if there are clusters which are not being incubated (are cooling) a queen or worker will go and incubate while
	they sleep?*/
	int NumberClusters = int(GetColony()->m_ColonyClusters.size());
	if (NumberClusters > 0)
	{
		auto setIt = GetColony()->m_ColonyClusters.begin();
		auto end = GetColony()->m_ColonyClusters.end();
		while (m_FocusCluster == nullptr && setIt != end)
		{
			double probIncub = 0.0;
			//double probIncub2 = 0.0;
			double temp = (*setIt)->GetTemp();
			if (temp < MinJuvDevTemp)
			{
				probIncub = 1.0;
			}
			else if (temp < OptimumDevTemp)
			{
				probIncub = ProbV1(temp);
				//probIncub2 = ProbV2(temp);
			}
			if ((*setIt)->GetCurrentStateNo() == toBombusS_Cool && g_rand_uni() <= probIncub)
			{
				m_FocusCluster = *setIt;
				m_FocusCluster->SetFocusedOnBy(this);
				//break;
			}
			setIt += 1;
		}
	}
	if (m_FocusCluster != nullptr)
	{
		st_Incubate();
	}
	else
	{
		/** When incubating I am assuming they sleep. So if they have gone to incubate,
		sleep is removed from that needed there. If they incubate I assume they would
		move off(wake up) when the cluster is warm enought therefore they won't come
		back here to wake up.*/
		m_SleepNeeded.DecreaseQuantity(10);
		m_SleepWanted.DecreaseQuantity(10);
		timeInState += 10;
	}
	//What is the probability of waking up?
	if ((m_Forager == true || m_MyIdentity == to_Gyne) && m_SleepWanted.ReportQuantity() == 0)
	{
		timeInState = 0;
		return toBombusS_Decide;
	}
	/** I have assumed the probability of waking up is the opposite of falling asleep (g_rand_uni() < MyProbabilitySleeping())*/
	if ((m_Forager == false || m_MyIdentity == to_Queen) && g_rand_uni() >= MyProbabilitySleeping())
	{
		timeInState = 0;
		return toBombusS_Decide;
	}
	return toBombusS_Sleep;
}

//--------------------------------------------------------------------------------------------------------------------------------
//*****************************************************************************************************************************************
//***                                                                                                                                  ****
//***                                                           Gynes                                                                  ****
//***                                                                                                                                  ****
//****************************************************************************************************************************************/
//********************************************************************************************************************************
//**************************************** Bombus_Gynes Definition ************************************************
//*******************************************************************************************************************************/
Bombus_Gyne::Bombus_Gyne(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                         unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
                         double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony,
                         unsigned long a_id, unsigned long a_SpermID) : Bombus_Worker(
	a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Mass, a_BodyFat, a_OvaryProp,
	a_Colony, a_id)
{
	Init();
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Gyne::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                         unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
                         double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony,
                         unsigned long a_id, unsigned long a_SpermID)
{
	Bombus_Worker::ReInit(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_age, a_Mass,
	                      a_BodyFat, a_OvaryProp, a_Colony, a_id);
	Init();
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Gyne::Init()
{
	/** \brief Label of identity.*/
	m_MyIdentity = to_Gyne;
	m_MyIdentityString = "Gyne";
	m_StageAge = 0;
	m_StageAgeDay = 0;
	///** Maintenence Energy is reused*/
	//m_MaintenceEnergy = CalculateMaintenanceEnergy();
	//m_EnergyNeeded = m_MaintenceEnergy;
	m_SleepNeeded.ResetQuantity(MaxSleep);
	m_SleepWanted.ResetQuantity(MaxSleep);
	m_mortality = cfg_BombusGyneMortality.value();

	/** This value is being set the same for all gynes, but could be reduced if a worker is born into a colony that already has a high number of dead.*/
	m_DeadEffected = cfg_BombusGyneDeadEffected.value();

	m_BelowTempCoef = cfg_BombusGyneBelowTempCoef.value();
	m_AboveTempCoef = cfg_BombusGyneAboveTempCoef.value();


	//m_AgeMortMultiVect = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiGyne.value(), 365 );
	MortalityStressors();
	m_Mass.SetHasOvaries(false);
}

//--------------------------------------------------------------------------------------------------------------------------------
/**
Adult bumblebees do not need to continuously call develop. They age a day and then enter a decision loop until the day is over.
*/
void Bombus_Gyne::BeginStep()
{


	CalculateMyBodyTemp();
	MortalityStressors();
	if (g_rand_uni() < m_mortalityToday)
	{
		m_CurrentBoState = st_Dying();
		return;
	}

	AddToWarmAccount();

	if (timestep % 144 == 0)
	{
		m_SleepNeeded.IncreaseQuantity(MaxSleep);
		m_SleepWanted.IncreaseQuantity(MaxSleep);
		RecordVitals();
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Gyne::Step()
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState:
		m_CurrentBoState = toBombusS_Decide;
		break;
	case toBombusS_Decide:
		m_CurrentBoState = st_Decide();
		break;
	//case toBombusS_RandomMove:
	//	m_CurrentBoState = st_RandomMove();
	//	break;
	case toBombusS_Consume:
		m_CurrentBoState = st_Consume();
		m_StepDone = true;
		break;
	case toBombusS_Sleep:
		m_CurrentBoState = st_Sleep();
		m_StepDone = true;
		break;
	case toBombusS_Inspect:
		m_CurrentBoState = st_Inspect();
		break;
	case toBombusS_AddWax:
		m_CurrentBoState = st_AddWax();
		m_StepDone = true;
		break;
	case toBombusS_Incubate:
		m_CurrentBoState = st_Incubate();
		m_StepDone = true;
		break;
	case toBombusS_Feed:
		m_CurrentBoState = st_Feed();
		m_StepDone = true;
		break;
	case toBombusS_Fan:
		m_CurrentBoState = st_Fan();
		m_StepDone = true;
		break;
	//case toBombusS_Forage:
	//	m_CurrentBoState = st_Forage();
	//	m_StepDone = true;
	//	break;
	//case toBombusS_SearchMate:
	//	m_CurrentBoState = st_SearchMate();
	//	m_StepDone = true;
	//	break;
	//case toBombusS_Mate:
	//	m_CurrentBoState = st_Mate();
	//	m_StepDone = true;
	//	break;
	//case toBombusS_SearchHibernation:
	//	m_CurrentBoState = st_SearchHibernation();
	//	m_StepDone = true;
	//	break;
	//case toBombusS_Hibernate:
	//	m_CurrentBoState = st_Hibernate();
	//	m_StepDone = true;
	//	break;
	//case toBombusS_SearchColony:
	//	m_CurrentBoState = st_SearchColony();
	//	m_StepDone = true;
	//	break;
	case toBombusS_FoundColony:
		m_CurrentBoState = st_FoundColony();
		break;
	//case toBombusS_StealColony:
	//	m_CurrentBoState = st_StealColony();
	//	break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Gyne::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Gyne::st_Decide()
{
	/**Key events in a gynes life :
			* Leaving maternal colony
			* m_OurColony = 0
			* Mating
			* GetSpermID() != 0
			* Finding hibernation site
			* Hibernated
			* m_hibernated == true
			* Finding and founding a colony.
	*/
	if (GetColony() != nullptr)
	{
		/** Will only be entered once and will negate the code before if it has been entered.*/
		if (m_Callow == true && GetAgeDegrees() >= CallowForDM)
		{
			GetColony()->m_ActiveColonyGynes.push_back(this);
			m_Callow = false;
			CalculateMyBodyTemp();
			AddToWarmAccount();
		}

		/**
		Gyne is in the colonyand hasn't left permenantly. At this stage in their life they should primarily be concerned with foraging for themselves and eating the stocks of the maternal colony.
		It has been suggested that they may act like workers if there is a need in the colony.
		Because the founding gynes needed to be mid hibernation, I have set the default for gynes as 90 days and then any who belong to a colony (only the young) are set to zero.
		*/
		m_DaysHibernated = 0;
		if (m_OurPopulationManager->IsDaylight == false && m_SleepNeeded.ReportQuantity() > 0)
		{
			return toBombusS_Sleep;
		}
		if (InColony())
		{
			if (int(1000 * m_Stomach.ReportVolumeFood()) == 0)
			{
				return toBombusS_Consume;
			}
		}

		//if (AccessForaging == true && GetAgeDegrees() >= CallowForDM && g_rand_uni() < (1 / (1 + exp(-50 * ((m_Mass.ReportFatMass() / m_Mass.ReportLeanMass()) - 0.15))))) {
		//	return toBombusS_Leave;
		//}





		/** Gynes can end up acting like workers or the queen. They get broody and start helping out if feeding or incubation
		need to be done.*/
		if (!m_Callow  && !GetColony()->m_ColonyClusters.empty() && GetAgeDegrees() >= CallowForDM)
		{

			return toBombusS_Inspect;
		}
	}
	//if(m_OurColony == 0 && m_SpermID == 0) {
	//	//Find a mate
	//	return toBombusS_SearchMate;
	//}
	//if (GetColony() == 0 && m_SpermID != 0 && m_DaysHibernated == 0) {
	//	//Search for a place to hibernate - We could also have the individual get food, possibly returning to maternal nest, but if so will have to change loosing their colony id. Feeding
	//	//at their maternal colony might suggest that they don't disperse like the males do. 
	//	return toBombusS_SearchHibernation;
	//}
	if (ColonyLocation == toColonyLoc_BoxLab && m_DaysHibernated > 0)
	{
		return toBombusS_FoundColony;
	}
	if (GetColony() == nullptr && m_DaysHibernated > 0 && m_DaysHibernated <= MinHibernationDuration)
	{
		/**
		\cite<Prys-Jones2011> in Fig. 3 shows data from from their 1982 PhD thesis showing Max soil temp when different Bombus sp. wake up. Soild temp 6.5 - 6.6C for B. terrestris.
		There is a three month period when hibernating gynes are insensative to temperature waking them \cite<Larrere 1993>.
		*/
		return toBombusS_Hibernate;
	}
	if (GetColony() == nullptr && m_OurLandscape->SupplySoilTemp() >= HibernationWakeUpTemp && m_DaysHibernated >
		MinHibernationDuration)
	{
		m_hibernated = true;
	}
	if (GetColony() == nullptr && m_hibernated == false && m_OurLandscape->SupplySoilTemp() <= HibernationWakeUpTemp)
	{
		return toBombusS_Hibernate;
	}
	///** The gyne will not regain the mass she lost while hibernating by foraging. But she does regain some.*/
	//if(AccessForaging == true && m_Mass.ReportEnergyDeficit() > 0) {
	//	// gynes regain fat lost during hibernation, those less than 0.6g don't survive hibernation, therefore those exiting hibernation should regain this fat, 
	//	//we could record their pre-hibernation weight and have them get back to this. They should forage.
	//	return toBombusS_Forage;
	//}
	/** Once the gyne has regained the mass she lost while hibernating she will search for a colony. If this takes too long, she will loose
	weight and return to foraging*/
	if (GetColony() == nullptr && int(1000 * m_Mass.ReportEnergyDeficit()) == 0)
	{
		//double FoodClock = 1440.0 * m_EnergyFed / m_EnergyNeeded;
		//if (AccessForaging == true && FoodClock <  && FoodClock < GetColony()->GetTime()) {
		//	return toBombusS_Forage;
		//}
		//Search for a colony location
		return toBombusS_SearchColony;
	}
	m_StepDone = true;
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Gyne::st_SearchMate()
//{
//	return toBombusS_Decide;
//}
////--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Gyne::st_Mate()
//{
//	/** 
//	Mating in bumblebees can take upto 30mins. They may even move while still attached to each other (on foot). Gynes almost always only mate once. Males may mate more than once.
//	*/
//	
//	/** When the male mates with the gyne he will pass on his id. I just need to work out how.*/
//	SetSpermID(0);
//	//Because time is frozen for each, I think the males will need to set up their paths forst and then gynes will have to get the information from this path.
//
//	return toBombusS_Decide;
//}
////--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Gyne::st_SearchHibernation()
//{
//
//
//	return toBombusS_Decide;
//}
////--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Gyne::st_Hibernate()
//{
//	/** 
//	\cite<Prys-Jones2011> in Fig. 3 shows data from from their 1982 PhD thesis showing Max soil temp when different Bombus sp. wake up. Soild temp 6.5 - 6.6C for B. terrestris. 
//	*/
//
//	//I could have them add up the days they have hibernated instead of m_hibernated and once over three months they can be awoken by temp. 
//
//	++m_DaysHibernated;
//
//	m_StepDone = true;
//
//
//	return toBombusS_Decide;
//}
////--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Gyne::st_SearchColony()
//{
//	/**
//	The gyne searches for a site for a colony. If she finds a suitable site, she st_FoundColony(). She can also st_StealColony(), for this i'll need something here like
//	*/
//	st_RandomMove();
//		
//	int pindex = m_OurLandscape->SupplyPolyRefIndex(m_Location_x, m_Location_y);
//
//	bool found = false;
//	bool ExistingColony = false;
//
//	switch (m_OurLandscape->SupplyElementType(pindex)) {
//		case tole_Hedges:
//		case tole_RoadsideVerge:
//		case tole_Railway:
//		case tole_FieldBoundary:
//		case tole_Scrub:
//		case tole_PermPastureTussocky:
//		case tole_PermanentSetaside:
//		case tole_NaturalGrassDry:
//		case tole_RiversidePlants:
//		case tole_PitDisused:
//		case tole_RiversideTrees:
//		case tole_DeciduousForest:
//		case tole_MixedForest:
//		case tole_ConiferousForest:
//		case tole_YoungForest:
//		case tole_StoneWall:
//		case tole_Fence:
//		case tole_Garden:
//		case tole_Building:
//		case tole_HedgeBank:
//		case tole_BeetleBank:
//		case tole_Heath:
//		case tole_Orchard:
//		case tole_UnsprayedFieldMargin:
//		case tole_OrchardBand:
//		case tole_Parkland:
//		case tole_UrbanPark:
//		case tole_BuiltUpWithParkland:
//		case tole_Copse:
//		case tole_Churchyard:
//		case tole_NaturalGrassWet:
//		case tole_HeritageSite:
//		case tole_Wasteland:
//		case tole_IndividualTree:
//		case tole_WoodlandMargin:
//		case tole_PermPastureTussockyWet:
//		case tole_UrbanVeg:
//		case tole_DrainageDitch:
//		case tole_RoadsideSlope:
//
//			if (m_OurPopulationManager->Colonies_coords.size() > 0) {
//				/** Store the coordinates as a pair of values. {x,y} */
//				pair<int, int> coords;
//				coords.first = m_Location_x;
//				coords.second = m_Location_y;
//				//Count could be used here, but find is more efficient. We could use count if we want a density of colonies in a meter squared. 
//				if (std::find(m_OurPopulationManager->Colonies_coords.begin(), m_OurPopulationManager->Colonies_coords.end(), coords) != m_OurPopulationManager->Colonies_coords.end()) {
//					cout << "Another colony" << "\n";
//					return st_StealColony();
//				}
//			}
//			
//			//This wind speed of 2 is entirely arbitrary. Bumblbee nests tend to be in sheltered spots, but I don't know how sheltered
//			//or even what these units are currently. 
//			if (m_OurLandscape->SupplyWind(pindex) < 5) {
//				return st_FoundColony();
//			}
//	}
//	return toBombusS_Decide;
//}
//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Gyne::st_FoundColony()
{
	/** Creates a new colony and queen object and passes the data from the gyne to it, then signals gyne object removal.*/
	struct_Bombus sColony;
	struct_Bombus sBo;
	//This structure for Bombus is in its population manager. Each transition, or line of transition, may need a different struct
	sColony.BPM = sBo.BPM = m_OurPopulationManager;
	sColony.L = sBo.L = m_OurLandscape;
	sBo.age = m_Age;
	sBo.DaysHibernated = m_DaysHibernated;
	sBo.waxConcInter = m_ChemicalConc;
	sColony.x = sBo.x = m_Location_x;
	sColony.y = sBo.y = m_Location_y;
	if (ColonyLocation != toColonyLoc_GyneChoose)
	{
		sColony.ColonyLocation = ColonyLocation;
	}
	else
	{
		// Code can be added here to read in the proportion of colonies found in different locations for different species.
		sColony.ColonyLocation = toColonyLoc_Foo; //Foo is nothing. Needs to be changed.
	}
	sColony.m_id = sBo.m_id = m_id;
	sBo.MotherID = GetMotherID();
	sBo.FatherID = GetFatherID();
	sBo.BodyFat = m_Mass.ReportFatMass();
	sBo.Mass = m_Mass.ReportLeanMass();

	sBo.Callow = false;

	sBo.PropOvaries = m_Mass.ReportPropOvaries();
	sBo.SpermID = m_SpermID;
	sColony.ColonyLocation = ColonyLocation;
	sBo.Colony = m_OurPopulationManager->CreateColony(to_Colony, &sColony, 1);
	pair<int, int> coords;
	coords.first = m_Location_x;
	coords.second = m_Location_y;
	m_OurPopulationManager->Colonies_coords.push_back(coords);
	m_OurPopulationManager->Colonies.push_back(sBo.Colony);
	sBo.minsQueenCanReduce = m_minsQueenCanReduce;
	sBo.StartingIndiv = m_StartingIndiv;

	/** The gyne needs to pass on her own sex alleles, but also that of the male she mated with. */
	sBo.SexLocus_1 = GetSexLocus_1();
	sBo.SexLocus_2 = GetSexLocus_2();
	sBo.MatesSexLocus = m_MatesSexLocus;


	m_OurPopulationManager->CreateObjects(to_Queen, nullptr, &sBo, 1);
	sBo.Colony->m_TotalAdultMass += m_Mass.ReportMass();
	sBo.Colony->NoQueens++;
	sBo.Colony->NoAdults++;
	if (ColonyLocation == toColonyLoc_BoxLab && m_StartingColWorkers > 0)
	{
		sBo.DaysHibernated = 0;
		sBo.waxConcInter = 0.0;
		sBo.PropOvaries = 0.0;
		/**If the colony is started in the lab with workers this should add workers to the colony*/
		sBo.MotherID = m_id;
		sBo.FatherID = m_SpermID;
		//The next three possibly shouldn't be hardwired but set if they are known.
		sBo.age = 1440;
		sBo.AgeDM = CallowForDM;
		sBo.BodyFat = 10.0;

		/** Here assuming the workers that found the colony are the queens daughters. They may not always be if only one or two are used
		to stimulate the queen into laying. But if recieving a mature colony they should be. */
		if (g_rand_uni() <= 0.5)
		{
			sBo.SexLocus_1 = GetSexLocus_1();
		}
		else
		{
			sBo.SexLocus_1 = GetSexLocus_2();
		}
		sBo.SexLocus_2 = m_MatesSexLocus;


		double StartWorkerMassAlpha = cfg_BombusStartWorkerMassAlpha.value();
		double StartWorkerMassBeta = cfg_BombusStartWorkerMassBeta.value();
		string StartWorkerMassString = to_string(StartWorkerMassAlpha) + " " + to_string(StartWorkerMassBeta);
		probability_distribution StartWorkerMassDistribution{"GAMMA", StartWorkerMassString};

		for (int i = 0; i < m_StartingColWorkers; i++)
		{
			sBo.Mass = StartWorkerMassDistribution.get();
			/** Add new worker self to the NoWorkers*/
			sBo.Colony->NoWorkers++;
			sBo.Colony->NoAdults++;
			sBo.Colony->m_TotalAdultMass += sBo.Mass;
			sBo.m_id = m_OurPopulationManager->nextID();
			sBo.Callow = false;
			m_OurPopulationManager->CreateObjects(to_Worker, nullptr, &sBo, 1);
		}
	}
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Gyne::st_Dying()
{

	if(m_FocusCluster != nullptr)
	{
		m_FocusCluster->SetFocusedOnBy(nullptr);
		m_FocusCluster = nullptr;
	}


	SetSexLocus_1(0);
	SetSexLocus_2(0);
	SetMateSexAllele(0);

	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
	if (GetColony() != nullptr)
	{
		GetColony()->NoGyne--;
		GetColony()->NoAdults--;
		GetColony()->RemoveAdult(this);
		if (InColony())
		{
			GetColony()->NoAdultCorpses++;
		}

		SetColony(nullptr);
	}
	return toBombusS_Die;
}

TTypeOfBombusState Bombus_Gyne::st_Consume()
{
	EatNectar();
	EatPollen();
	return toBombusS_Decide;
}

void Bombus_Gyne::RemoveSelfFromColony()
{
	RemoveFemaleSelfFromColony();

	if (!m_Callow) {
	auto end2 = m_OurColony->m_ActiveColonyGynes.end();
	for (auto ifemale = m_OurColony->m_ActiveColonyGynes.begin(); ifemale < end2; ++ifemale)
	{
		if (*(ifemale) == this)
		{
			m_OurColony->m_ActiveColonyGynes.erase(ifemale);
			break;
		}
	}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Gyne::SetDaysHibernated(int a_DaysHibernated)
{
	m_DaysHibernated = a_DaysHibernated;
	if (a_DaysHibernated > 30)
	{
		m_Mass.SetHasOvaries(true);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Gyne::SetStatic()
{
	MinHibernationDuration = cfg_BombusMinHibernationDuration.value();
	HibernationWakeUpTemp = cfg_BombusHibernationWakeUpTemp.value();

	m_AgeMortMultiVectGyne = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiGyne.value(), 365 );

}



//--------------------------------------------------------------------------------------------------------------------------------
//TTypeOfBombusState Bombus_Gyne::st_StealColony()
//{
//	/**
//	As well as founding their own colonies, gynes can also try and steal another queen. I have programmed this as a fight to the death. With only one surviving. I need to check this
//
//	Success of introducng a new queen varies with the number of workers. Without a queen, the sucess drops from 60% to 10% between just the first brood workers, and 
//	~40 workers \cite<Cilavdaroglu2020>
//	*/
//	
//	/** Store the coordinates as a pair of values. {x,y} */
//	pair<int, int> coords;
//	coords.first = m_Location_x;
//	coords.second = m_Location_y;
//
//	/** Find the index for the colony from when its coordinates were added to the coordinates vector and then the colony was added to the colonies vector*/
//	std::vector<pair<int, int>>::iterator itr = std::find(m_OurPopulationManager->Colonies_coords.begin(), m_OurPopulationManager->Colonies_coords.end(), coords);
//	int index = int(std::distance(m_OurPopulationManager->Colonies_coords.begin(), itr));
//
//	/** The colony the gyne has found*/
//	Bombus_Colony* ColonyFound = m_OurPopulationManager->Colonies[index];
//
//	//This value should vary with the age/number of workers in the colony. Maybe the 60-10% of Cilavdaroglu2020 and then a 50:50 between the queens. Something like 0.5e^-0.05workers
//	/** This is the successrate of the new gyne stealing the colony. 0.5 is probably far too high*/
//	if (g_rand_uni() <= 0.5) {
//
//		/**
//		* Creates a new Queen object and passes the data from the gyne to it, then signals gyne object removal.
//		*/
//		struct_Bombus sBo; //This structure for Bombus is in its population manager. Each transition, or line of transition, may need a different struct
//		sBo.BPM = m_OurPopulationManager;
//		sBo.L = m_OurLandscape;
//		sBo.age = m_Age;
//		sBo.x = m_Location_x;
//		sBo.y = m_Location_y;
//		sBo.MotherID = GetMotherID();
//		sBo.FatherID = GetFatherID();
//		sBo.BodyFat = m_Mass.ReportFatMass();
//		sBo.Mass = m_Mass.ReportLeanMass();
//		sBo.SpermID = m_SpermID;
//		sBo.m_id = m_id;
//
//		/** The colony found is my colony*/
//		sBo.Colony = ColonyFound;
//		/** I have changed the ID of my colony to my ID*/
//		sBo.Colony->SetColonyID(m_id);
//		/** I am now a queen */
//		m_OurPopulationManager->CreateObjects(to_Queen, 0, &sBo, 1);
//		GetColony()->NoQueens++;
//		GetColony()->NoAdults++;
//		GetColony()->m_TotalAdultMass += m_Mass.ReportMass();
//		 
//	}
//	else {
//		cout << "I failed to steal the colony and I die. \n";
//	}
//	/** If I fail to steal the colony I die, if I steal the colony I am now a queen and the gyne I was dies.*/
//	return toBombusS_Die;
//}
////--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//**************************************** Bombus_Queen Definition ************************************************
//*******************************************************************************************************************************/
Bombus_Queen::Bombus_Queen(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                           unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
                           double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony,
                           unsigned long a_id, unsigned long a_SpermID) : Bombus_Gyne(
	a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_MatesSexLocus, a_age, a_Mass, a_BodyFat,
	a_OvaryProp, a_Colony, a_id, a_SpermID)
{
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_MatesSexLocus, a_age, a_Mass,
	     a_BodyFat, a_OvaryProp, a_Colony, a_id, a_SpermID);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Queen::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                          unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
                          double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony,
                          unsigned long a_id, unsigned long a_SpermID)
{
	Bombus_Gyne::ReInit(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_MatesSexLocus,
	                    a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id, a_SpermID);
	Init(a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_SexLocus_1, a_SexLocus_2, a_MatesSexLocus, a_age, a_Mass,
	     a_BodyFat, a_OvaryProp, a_Colony, a_id, a_SpermID);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Queen::Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                        unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
                        double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony,
                        unsigned long a_id, unsigned long a_SpermID)
{
	m_Mass.SetHasOvaries(true);
	/** \brief Label of identity.*/
	m_MyIdentity = to_Queen;
	m_MyIdentityString = "Queen";
	m_StageAge = 0;
	m_StageAgeDay = 0;

	m_mortality = cfg_BombusQueenMortality.value();

	/** This value is being set the same for all queens, but could be changed based on their mate, size, fat reserves....*/
	m_DeadEffected = cfg_BombusQueenDeadEffected.value();


	m_BelowTempCoef = cfg_BombusQueenBelowTempCoef.value();
	m_AboveTempCoef = cfg_BombusQueenAboveTempCoef.value();
	/** Queen has an age multiplier to their mortality*/


	//m_AgeMortMultiVect = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiQueen.value(), 365 );
	CalculateMyBodyTemp();
	MortalityStressors();

	/** When workers were looking after larva on their own they slept half as much \cite<Nagari2019>.*/
	m_SleepNeeded.ResetQuantity(MinSleep);

	m_SleepWanted.ResetQuantity(MaxSleep);
	/** Based on \cite<Duchateau1988> this is approximatly how many eggs are laid (based on a number of egg cells and number of eggs per cell. But each queen must lay a certain number based on
	something. At the moment I have it as random. But I don't think it should be. Could it be the queens personal fat reserve (I'm thinking the amount of resources available).
	*/
	double ColonyTemp = GetColony()->GetColonyTemp();
	if (ColonyTemp >= OptimumColTemp)
	{
		m_NumberEggsPerCell.ResetQuantity(EggMax);
	}
	else if (ColonyTemp < MinColonyTempToLay)
	{
		m_NumberEggsPerCell.ResetQuantity(0);
	}
	else
	{
		double NumberEggs = EggMin + (ColonyTemp - MinColonyTempToLay) * (double(EggMax) - EggMin) /
			(OptimumColTemp - MinColonyTempToLay);
		m_NumberEggsPerCell.ResetQuantity(int(NumberEggs));
	}
	//EggCellsB2Day = 1;
	//EggCellsB3Day = 1;



		/** This switch allows the colonies to be started at later brood stages. Not super effective because although
	you can set the number of starting workers in the colony, you can't set the number of each type of juvinile.
	May be useful if we create microcolonies. */
	m_BroodsLaid = cfg_BombusBroodStage.value();

	m_Senesce = false;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Queen::BeginStep()
{


	CalculateMyBodyTemp();
	MortalityStressors();
	if (g_rand_uni() < m_mortalityToday)
	{
		m_CurrentBoState = st_Dying();
		return;
	}

	AddToWarmAccount();

	if (timestep % 144 == 0)
	{
		/** When workers were looking after larva on their own they slept half as much \cite<Nagari2019>.*/
		m_SleepNeeded.IncreaseQuantity(MinSleep);
		m_SleepWanted.IncreaseQuantity(MaxSleep);
		/** Based on \cite<Duchateau1988> this is approximatly how many eggs are laid (based on a number of egg cells and number of eggs per cell. But each queen must lay a certain number based on
		something. At the moment I have it as random. But I don't think it should be. Could it be the queens personal fat reserve (I'm thinking the amount of resources available).
		*/
		double ColonyTemp = GetColony()->GetColonyTemp();
		if (ColonyTemp >= OptimumColTemp)
		{
			m_NumberEggsPerCell.ResetQuantity(EggMax);
		}
		else if (ColonyTemp < MinColonyTempToLay)
		{
			m_NumberEggsPerCell.ResetQuantity(0);
		}
		else
		{
			int NumberEggs = int(double(EggMin) + (ColonyTemp - MinColonyTempToLay) * (
				double(EggMax) - double(EggMin)) / (
				OptimumColTemp - MinColonyTempToLay));
			m_NumberEggsPerCell.ResetQuantity(NumberEggs);
		}

		ZeroEggsLaidToday();

		RecordVitals();
	}
	if (GetColony()->NoWorkers > 0 || AccessForaging == false)
	{
		m_Forager = false;
	}
	else
	{
		m_Forager = true;
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Queen::Step()
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState: // Initial state always starts with develop
		m_CurrentBoState = toBombusS_Decide;
		break;
	case toBombusS_Decide:
		m_CurrentBoState = st_Decide();
		break;
	//case toBombusS_RandomMove:
	//	m_CurrentBoState = st_RandomMove();
	//	break;
	case toBombusS_Sleep:
		m_CurrentBoState = st_Sleep();
		m_StepDone = true;
		break;
	//case toBombusS_DirectMove:
	//	m_CurrentBoState = st_DirectMove();
	//	break;
	//case toBombusS_Forage:
	//	m_CurrentBoState = st_Forage();
	//	break;
	case toBombusS_Consume:
		m_CurrentBoState = st_Consume();
		m_StepDone = true;
		break;
	case toBombusS_Inspect:
		m_CurrentBoState = st_Inspect();
		break;
	case toBombusS_AddWax:
		m_CurrentBoState = st_AddWax();
		m_StepDone = true;
		break;
	case toBombusS_Incubate:
		m_CurrentBoState = st_Incubate();
		m_StepDone = true;
		break;
	case toBombusS_Feed:
		m_CurrentBoState = st_Feed();
		m_StepDone = true;
		break;
	case toBombusS_Fan:
		m_CurrentBoState = st_Fan();
		m_StepDone = true;
		break;
	case toBombusS_Other:
		m_CurrentBoState = st_Other();
		m_StepDone = true;
		break;
	//case toBombusS_ColonyStolen:
	//	m_CurrentBoState = st_ColonyStolen();
	//	break;
	case toBombusS_LayEgg:
		m_CurrentBoState = st_LayEgg();
		m_StepDone = true;
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Queen::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Queen::st_Decide()
{
	/** Within decide all actions that kill the queen should be first.*/
	if (GetColony()->GetColonyID() != m_id)
	{
		return toBombusS_Die;
	}


	if(m_FocusCluster != nullptr)
	{
		return toBombusS_Incubate;
	}

	/**
	The queen is governed by whether certain events have happened or not.
		- Before the queen lays any eggs
			-# Warming and drying the new nest
			-# moving bedding around to make a cosy hollow
			-# building nectar cup, depositing pollen clump and egg cell and then laying eggs (it is possible the order of this is not fixed and queens will deposit nectar on the floor.
		- Brood one eggs layed
			-# incubate
			-# gather nectar for the day
		- Brood one larva
			-# incubate
			-# gather nectar and pollen for the day
		- Brood one stops feeding before pupating
			-# lay brood two
			-# incubate
		- Brood two larva
			-# incubate
			-# gather nectar and pollen for the day
		- Workers emerge
			-# queen no longer forages
			-# police
			-# lay eggs
			-# incubate
			-# feed larva
		- switching point
			-# gradually lay nothing but male eggs
		- competition point
			-# eat eggs of workers
	*/
	/** Deciding if there is time left in the day should come next.
	I have set this at 15, but it should be what ever the minimum time needed for activities are.
	*/
	//if ( < 1440) {
	/** The queen needs to inspect or police. Inspecting she will choose a random cluster to inspect.
	We could assume that the queen as she moves between tasks encounters workers. This means that this isn't
	necessarily a seperate task for the queen.
	She is unlikely to need 10mins for the task. Therefore she could encounter multiple workers.
	#V2 #V3 improvement would be to adapt this to invading queen deffence. Possibly cuckoos as well.
	*/
	if (GetColony()->NoAdults > 1)
	{
		for (int i = 0; i < AdultsEncounteredInStep; i++)
		{
			st_Dominate();
		}
	}
	/**Presumably a queen need some sleep. Lets assume the minimum is 1/6 of a day (Eban-Rothschild et al. 2011, Nagari et al. 2019)
	So in deciding to sleep, some sleep is a priority and some is a luxuery. Therefore sleeping could be devided into two places.
	Here priority sleep and at the end of decide, luxery sleep. */
	if (g_rand_uni() < MyProbabilitySleeping())
	{
		return toBombusS_Sleep;
	}
	/**
	Throughout the day, the queen need to feed herself with the energy needed.
	If they have less energy then they "should" at that time of day, then they are hungry.
	If they aren't then they can do other things.
	*/
	if (InColony())
	{
		if (int(1000 * m_Stomach.ReportVolumeFood()) == 0)
		{
			return toBombusS_Consume;
		}
	}

	/**If the colony does not have a complete nectar pot, build one. */
	if(!GetColony()->FirstNectarCellComplete())		
	{
		BuildFirstNectarPot();
		m_StepDone = true;
		return toBombusS_Decide;
	}



	SetCurrentBrood();

	/** If the queen has build a nectar cell, the colony is warm enough and she physically and in colony timing can lay an egg, she does */
	if(GetColony()->FirstNectarCellComplete() && CanLayEggs())
	{
		return toBombusS_LayEgg;
	}
	
	
	if (!GetColony()->m_ColonyClusters.empty())
	{
		return toBombusS_Inspect;
	}
	if (m_SleepWanted.ReportQuantity() > 0)
	{
		return toBombusS_Sleep;
	}
	m_StepDone = true;
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Queen::st_Dying()
{
	if(m_FocusCluster != nullptr)
	{
		m_FocusCluster->SetFocusedOnBy(nullptr);
		m_FocusCluster = nullptr;
	}



	SetSexLocus_1(0);
	SetSexLocus_2(0);
	SetMateSexAllele(0);
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
	/** remove worker self from the NoQueens*/
	GetColony()->NoQueens--;
	GetColony()->NoAdults--;
	GetColony()->RemoveAdult(this);
	if (InColony())
	{
		GetColony()->NoAdultCorpses++;
	}
	SetColony(nullptr);
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Queen::CheckClusterEggs(Bombus_Cluster* a_FocusCluster)
{


	auto i = a_FocusCluster->GetEggs()->begin();
	if (!a_FocusCluster->GetEggs()->empty() && (*i)->GetAgeDegrees() < m_EggDM / DivisionEggAtDangerOfEaten)
	{
		/**Here the queen is checking if a worker is laying eggs. Presumably all eggs in a cell will have the same mother. Therefore if one egg in a cluster
		is not hers, then the competition phase has started and she eats all the eggs.*/
		if ((*i)->GetMotherID() != GetMyID())
		{
			m_CompetitionPossible = true;
			EatAllEggs();
			m_FocusCluster->SetFocusedOnBy(nullptr);
			m_FocusCluster = nullptr;
		}


		/** If the queen has not already eaten all the eggs, then she will also check each one individually to see if it is a diploid male and eat it.
		* She may not check the later broods, only the first, and when she finds a male she eats or kills it \cite<DiPietro2022>. I have assumed this only takes place
		* in approximatly the first day like it does for recognising the eggs of others. If it was a protracted period as \cite<DiPietro2022> suggest, it would be too
		* easy for the queen and or workers to remove these males throughout the colony.
		*/

		if (m_BroodsLaid == 1)
		{
			int counter = 0;
			int numbereggs = int(a_FocusCluster->GetEggs()->size());
			while (counter != numbereggs)
			{
				i = a_FocusCluster->GetEggs()->begin();
				i += counter;
				if (int(a_FocusCluster->GetEggs()->size()) == 0 || i == a_FocusCluster->GetEggs()->end()) break;
				if ((*i)->GetSexLocus_1() == (*i)->GetSexLocus_2())
				{
					EatAnEgg(*i);
					continue;
				}
				counter++;
			}

			/** In the unlikely event that every egg in a cell was a diploid male (50:50 chance of each egg being diploid in an inbred colony) then the cluster is dead.*/
			if (a_FocusCluster->GetNumberJuv() == 0)
			{
				a_FocusCluster->KillThis();
				m_FocusCluster->SetFocusedOnBy(nullptr);
				m_FocusCluster = nullptr;
			}
		}
	}


}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Queen::IncrementLarvaeInteractions(Bombus_Larva* a_Larva)
{
	a_Larva->InteractQueen();
	a_Larva->SetminsQueenCanReduce(m_minsQueenCanReduce);
}

void Bombus_Queen::IncrementColonyMaleEggs(int a_NumberOfEggs)
{
	GetColony()->NoQueensMaleEggs += a_NumberOfEggs;
}

void Bombus_Queen::SetCurrentBrood() 
{
	switch (m_BroodsLaid)
	{
	case 0:
		if(GetColony()->EggsInColony())
		{
			m_BroodsLaid = 1;	
		}
		break;
	case 1:
	case 2:
		if(!GetColony()->EggsInColony() && !GetColony()->LarvaInColony())
		{
			m_BroodsLaid++;
		}
		break;
	case 3:
		m_BroodsLaid = 3;
		break;
	default: 
		m_OurLandscape->Warn("Bombus_Queen::SetCurrentBrood()", "Unrecognized brood stage");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}




}

void Bombus_Queen::RemoveSelfFromColony()
{
	RemoveFemaleSelfFromColony();
	m_OurColony->SetColoniesQueen(nullptr);
}


//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Queen::SetStatic()
{
	m_EggDM = cfg_BombusEggDevelTotalDM.value();
	MassWaxFirstNectarPot = cfg_BombusMassWaxFirstNectarPot.value();
	m_AgeMortMultiVectQueen = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiQueen.value(), 365 );

	CfgInt cfg_BombusQueenNoLayDamageThreshold("BOMBUS_QUEENNOLAYDAMAGETHRESHOLD", CFG_CUSTOM, 100);
	m_QueenNoLayDamageThreshold = cfg_BombusQueenNoLayDamageThreshold.value();
}

//--------------------------------------------------------------------------------------------------------------------------------
int Bombus_Queen::GetMatesSexLocus()
{
	return m_MatesSexLocus;
}



double Bombus_Queen::PollenLumpToAdd()
{
	switch (m_BroodsLaid)
	{
	case 0:
		return B1EggCellPollen;
	case 1:
		return B2EggCellPollen;
	default:
		return B3EggCellPollen;
	}

}

void Bombus_Queen::BuildFirstNectarPot()
{
		/** Queen needs to create a honeypot. The smallest queen cocoon
			was 0.22g, but I assume the initial honey pot is half this (at least) as the queen feed from it while
			incubating. But maybe it is stouter. */
		double waxNeeded = MassWaxFirstNectarPot - GetColony()->ReportWax(); //converted to mg
		double EnergyNeeded = waxNeeded * WaxEnergyDensity;
		if (GetColony()->ReportWax() < MassWaxFirstNectarPot)
		{
			EnergyNeeded = m_Mass.TakeEnergyFromFat(EnergyNeeded);
			double WaxAvailable = EnergyNeeded / WaxEnergyDensity;
			GetColony()->AddWax(WaxAvailable);
			GetColony()->AddQWax(WaxAvailable, m_ChemicalConc);
		}

		if(GetColony()->ReportWax() >= MassWaxFirstNectarPot) GetColony()->AddNectarPot();
}

bool Bombus_Queen::CanLayEggs()
{

	/** The queen can't lay more eggs in brood one or two until there are no eggs or larva that need care. But she can in brood 3+. */
	switch(m_BroodsLaid)
	{
	case 1:
	case 2:
		if(GetColony()->EggsInColony() || GetColony()->LarvaInColony()) {
			return false;
		}

	default:  // NOLINT(clang-diagnostic-implicit-fallthrough)
		if(m_Damage > m_QueenNoLayDamageThreshold) {
			m_Senesce = true;
			return false;
		}

		if(m_NumberEggsPerCell.ReportQuantity() > 0) {
			return true;
		}
	}
	return false;

}

//--------------------------------------------------------------------------------------------------------------------------------
//********************************************************************************************************************************
//**************************************** Bombus_Male Definition ************************************************
//*******************************************************************************************************************************/
Bombus_Male::Bombus_Male(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                         unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
                         double a_BodyFat, Bombus_Colony* a_Colony, unsigned long a_id) : Bombus_Base(
	a_x, a_y, p_L, p_BPM, a_Colony)
{
	Init(a_MotherID, a_age, a_Mass, a_BodyFat);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Male::ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
                         unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
                         double a_BodyFat, Bombus_Colony* a_Colony, unsigned long a_id)
{
	Bombus_Base::ReInit(a_x, a_y, p_L, p_BPM, a_Colony);
	Init(a_MotherID, a_age, a_Mass, a_BodyFat);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Male::Init(unsigned long a_MotherID, int a_age, double a_Mass, double a_BodyFat)
{
	/** \brief Label of identity.*/
	m_MyIdentity = to_Male;
	m_MyIdentityString = "Male";
	m_StageAge = 0;
	m_StageAgeDay = 0;
	m_Temp = BodyTempAdults;
	m_mortality = cfg_BombusMaleMortality.value();

	/** This value is being set the same for all males, workers are fairly similar, and not all that important within this simulation. But when they are born could have an effect.*/
	m_DeadEffected = cfg_BombusMaleDeadEffected.value();


	m_BelowTempCoef = cfg_BombusMaleBelowTempCoef.value();
	m_AboveTempCoef = cfg_BombusMaleAboveTempCoef.value();


	SetMotherID(a_MotherID);
	//SetFatherID(a_FatherID);
	SetAge(a_age);
	m_Mass.Init(this);
	m_Mass.HardSetLeanMass(a_Mass);
	m_Mass.SetCapableStoreFat(true);
	m_Mass.HardSetBodyFat(a_BodyFat);


	//m_AgeMortMultiVect = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiMales.value(), 365 );
	CalculateMyBodyTemp();
	MortalityStressors();


	double VolumeCapacity = m_Mass.ReportLeanMass() * StomachCapacityPermgBodyWeight;
	m_Stomach.Init(this, VolumeCapacity);
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Male::BeginStep()
{


	CalculateMyBodyTemp();
	MortalityStressors();
	if (g_rand_uni() < m_mortalityToday)
	{
		m_CurrentBoState = st_Dying();
		return;
	}

	AddToWarmAccount();

	if (timestep % 144 == 0)
	{
		RecordVitals();
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Male::Step()
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentBoState)
	{
	case toBombusS_InitialState: // Initial state always starts with develop
		m_CurrentBoState = toBombusS_Decide;
		break;
	case toBombusS_Decide:
		m_CurrentBoState = st_Decide();
		m_StepDone = true;
		break;
	case toBombusS_Consume:
		m_CurrentBoState = st_Consume();
		break;
	case toBombusS_Sleep:
		m_CurrentBoState = st_Sleep();
		m_StepDone = true;
		break;
	case toBombusS_Fan:
		m_CurrentBoState = st_Fan();
		m_StepDone = true;
		break;
	case toBombusS_Leave:
		m_CurrentBoState = st_Leave();
		break;
	case toBombusS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Bombus_Male::Step()", "unknown state - default");
		std::exit(1); // NOLINT(concurrency-mt-unsafe)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Male::EndStep()
{
	/**
	Adult bumblebees do not need to continuously call develop.
	*/
	st_Develop();
	if (timestep % 144 == 0)
	{
		m_StageAgeDay += 1;
	}
	
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Male::st_Dying()
{

	SetSexLocus_1(0);
	SetSexLocus_2(0);
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
	if (GetColony() != nullptr)
	{
		GetColony()->NoMales--;
		GetColony()->NoAdults--;
		if (InColony())
		{
			GetColony()->NoAdultCorpses++;
		}
		SetColony(nullptr);
	}
	return toBombusS_Die;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Male::SetStatic()
{
	m_ProbMaleLeaving = cfg_BombusMaleLeaving.value();

	StomachCapacityPermgBodyWeight = cfg_BombusStomachCapacityPermgBodyWeight.value();




	m_AgeMortMultiVectMale = CalculateExpCurve(1, cfg_BombusAgeMortalityMultiMales.value(), 365 );

}


//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Male::st_Develop()
{
	if ((GetColony() != nullptr && GetColony()->GetCurrentStateNo() == -1) || m_CurrentStateNo == -1)
	{
		m_StepDone = true;
		SetCurrentStateNo(-1);
		return toBombusS_Die;
	}

	if (AccessForaging == true && GetColony() != nullptr && GetAgeDegrees() >= CallowForDM && g_rand_uni() <
		m_ProbMaleLeaving)
	{
		GetColony()->NoMales--;
		SetColony(nullptr);
	}
	Grow();
	if (GetColony() != nullptr && GetColony()->GetColonyTemp() >= CallowMinTemp)
	{
		AddAgeDegrees(10.0 * (GetColony()->GetColonyTemp() - CallowMinTemp));
	}
	m_Age += 10;
	m_StageAge += 10;
	timestep++;
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Male::st_Sleep()
{
	m_SleepNeeded.DecreaseQuantity(10);
	m_SleepWanted.DecreaseQuantity(10);
	timeInState += 10;
	//What is the probability of waking up?
	if (m_SleepWanted.ReportQuantity() == 0 || m_OurPopulationManager->IsDaylight == true)
	{
		timeInState = 0;
		return toBombusS_Decide;
	}
	return toBombusS_Sleep;
}

TTypeOfBombusState Bombus_Male::st_Consume()
{
	EatNectar();
	EatPollen();
	return toBombusS_Decide;
}

void Bombus_Male::RemoveSelfFromColony()
{
	if (!m_Callow) {
		auto end2 = m_OurColony->m_ActiveColonyMales.end();
		for (auto male = m_OurColony->m_ActiveColonyMales.begin(); male < end2; ++male)
		{
			if (*(male) == this)
			{
				m_OurColony->m_ActiveColonyMales.erase(male);
				break;
			}
		}
	}
}


//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Male::st_Decide()
{
	/** Will only be entered once and will negate the code before if it has been entered.*/
	if (m_Callow == true && GetAgeDegrees() >= CallowForDM)
	{
		GetColony()->m_ActiveColonyMales.push_back(this);
		m_Callow = false;
		CalculateMyBodyTemp();
		AddToWarmAccount();
	}


	if (m_OurPopulationManager->IsDaylight == false && m_SleepNeeded.ReportQuantity() > 0)
	{
		return toBombusS_Sleep;
	}
	if (InColony())
	{
		if (m_Stomach.ReportVolumeFood() == 0.0)
		{
			return toBombusS_Consume;
		}
	}
	m_StepDone = true;
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Male::st_Leave()
{
	/*Fly away from colony*/
	if (GetColony() != nullptr)
	{
		/*Return to the colony*/
	}
	return toBombusS_Decide;
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Male::st_MatePatrol()
{
	return TTypeOfBombusState();
}

//--------------------------------------------------------------------------------------------------------------------------------
TTypeOfBombusState Bombus_Male::st_Mate()
{
	return TTypeOfBombusState();
}

//--------------------------------------------------------------------------------------------------------------------------------
