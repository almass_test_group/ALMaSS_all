/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Bombus.h
\brief <B>The main source code for all Bombus lifestages and population manager classes</B>
*/
/**  \file Bombus.h
Version of  10 September 2021 \n
By Jordan Chetcuti \n \n
*/
//---------------------------------------------------------------------------
// ReSharper disable CppClangTidyReadabilityInconsistentDeclarationParameterName
#ifndef BombusH
#define BombusH
#define PI 3.14159265

//---------------------------------------------------------------------------
// Forwards
class Bombus_Population_Manager;
class probability_distribution;
class Bombus_Cluster;
class Bombus_Egg;
class Bombus_Larva;
class Bombus_Pupa;
class Bombus_Base;
class Bombus_Worker;
class Bombus_Gyne;
class Bombus_Male;
class Bombus_Queen;

class Food;
class Pollen;
//------------------------------------------------------------------------------

static CfgInt cfg_BombusDaysOfTemp("BOMBUS_DAYSOFTEMP", CFG_CUSTOM, 1);
static CfgFloat cfg_BombusEnergyPermgSugar("BOMBUS_ENERGYPERMGSUGAR", CFG_CUSTOM, 15.48);
static CfgFloat cfg_BombusEnergyPermgProtein("BOMBUS_ENERGYPERMGPROTEIN", CFG_CUSTOM, 17.0);
static CfgFloat cfg_BombusEnergyPermgFat("BOMBUS_ENERGYPERMGFAT", CFG_CUSTOM, 37.0);
static CfgFloat cfg_BombusSugarDensitymgperul("BOMBUS_SUGARDENSITYMGPERUL", CFG_CUSTOM, 1.587);
static CfgFloat cfg_BombusProteinDensitymgperul("BOMBUS_PROTEINDENSITYMGPERUL", CFG_CUSTOM, 1.35);
static CfgFloat cfg_BombusFatDenstymgperul("BOMBUS_FATDENSTYMGPERUL", CFG_CUSTOM, 0.9232);
static CfgFloat cfg_BombusAshDensitymgperml("BOMBUS_ASHDENSITYMGPERML", CFG_CUSTOM, 1.5);

static CfgFloat cfg_BombusClusterLengthPerEgg("BOMBUS_CLUSTERLENGTHPEREGGMM", CFG_CUSTOM, 3.3);
static CfgFloat cfg_BombusClusterWidthPerEgg("BOMBUS_CLUSTERWIDTHPEREGGMM", CFG_CUSTOM, 1.3);
static CfgFloat cfg_BombusClusterHieght("BOMBUS_CLUSTERHEIGHT", CFG_CUSTOM, 5.0);

static CfgFloat cfg_BombusClusterHeatCapacityJperg("BOMBUS_CLUSTERHEATCAPACITYJPERG", CFG_CUSTOM, 2.04);
static CfgFloat cfg_BombusAlphaOfAir("BOMBUS_ALPHAOFAIR", CFG_CUSTOM, 5.0);

static CfgFloat cfg_BombusFanningEnergyPerMin("BOMBUS_FANNINGENERGYPERMIN", CFG_CUSTOM, 6.0);
static CfgFloat cfg_BombusConcentrationAdultAddToNectar("BOMBUS_CONCENTRATIONADULTADDTONECTAR", CFG_CUSTOM, 0.00030);
static CfgFloat cfg_BombusMassPollenConcInNectar("BOMBUS_MASSPOLLENCONCINNECTAR", CFG_CUSTOM, 0.020);
static CfgFloat cfg_BombusEnergyDensityFlesh("BOMBUS_ENERGYDENSITYFLESH", CFG_CUSTOM, 7.0);
static CfgFloat cfg_BombusStomachCapacityPermgBodyWeight("BOMBUS_STOMACHCAPACITYPERMGBODYWEIGHT", CFG_CUSTOM, 0.170);
static CfgFloat cfg_BombusPupaDMDivisionOfLarval("BOMBUS_PUPADMDIVISIONOFLARVAL", CFG_CUSTOM, 2.0);
static CfgInt cfg_BombusMaxSleep("BOMBUS_MAXSLEEP", CFG_CUSTOM, 480);
static CfgInt cfg_BombusMinSleep("BOMBUS_MINSLEEP", CFG_CUSTOM, 240);
static CfgFloat cfg_BombusMaxCocoonMass("BOMBUS_MAXCOCOONMASS", CFG_CUSTOM, 400.0);
static CfgFloat cfg_BombusWaxEnergyDensity("BOMBUS_WAXENERGYDENSITY", CFG_CUSTOM, 53.0);
static CfgFloat cfg_BombusMassWaxFirstNectarPot("BOMBUS_MASSWAXFIRSTNECTARPOT", CFG_CUSTOM, 11.0);
static CfgFloat cfg_BombusDivisionEggAtDangerOfEaten("BOMBUS_DIVISIONEGGATDANGEROFEATEN", CFG_CUSTOM, 4.0);
static CfgFloat cfg_BombusPropEggProtein("BOMBUS_PROPEGGPROTEIN", CFG_CUSTOM, 0.70);
static CfgFloat cfg_BombusBodyTempAdults("BOMBUS_BODYTEMPADULTS", CFG_CUSTOM, 37.0);

static CfgFloat cfg_BombusVolumeLarvalFeeding("BOMBUS_VOLUMELARVALFEEDING", CFG_CUSTOM, 0.880);
static CfgFloat cfg_BombusMinHibernationDuration("BOMBUS_MINHIBERNATIONDURATION", CFG_CUSTOM, 90.0);
static CfgFloat cfg_BombusHibernationWakeUpTemp("BOMBUS_HIBERNATIONWAKEUPTEMP", CFG_CUSTOM, 6.60);
static CfgFloat cfg_BombusEnergyDensityOvaries("BOMBUS_ENERGYDENSITYOVARIES", CFG_CUSTOM, 17.0);
static CfgFloat cfg_BombusPropProtein("BOMBUS_PROPPROTEIN", CFG_CUSTOM, 0.310);
static CfgFloat cfg_BombusPropSugar("BOMBUS_PROPSUGAR", CFG_CUSTOM, 0.480);
static CfgFloat cfg_BombusPropFat("BOMBUS_PROPFAT", CFG_CUSTOM, 0.120);

static CfgFloat cfg_BombusAdultBodyDensity("BOMBUS_ADULTBODYDENSITY", CFG_CUSTOM, 1.010);
static CfgFloat cfg_BombusAdultHeatCapacityJperg("BOMBUS_ADULTHEATCAPACITYJPERG", CFG_CUSTOM, 3.60);
static CfgBool cfg_BombusAdultsHomeotherms("BOMBUS_ADULTSHOMEOTHERMS", CFG_CUSTOM, false);
static CfgBool cfg_BombusJuvinilessHomeotherms("BOMBUS_JUVINILESSHOMEOTHERMS", CFG_CUSTOM, false);


/**
Used for the population manager's list of Bombus
*/
/**
Bombus like other ALMaSS animals work using a state/transition concept.
These are the Bombus behavioural states, these need to be altered, but some are here just to show how they should look.
*/
using TTypeOfBombusState = enum TTypeOfBombusState
{
	toBombusS_InitialState = 0,
	toBombusS_Develop,
	toBombusS_Decide,
	toBombusS_Inspect,
	toBombusS_NextStage,
	toBombusS_Feed,
	//toBombusS_RandomMove,
	//toBombusS_Forage,
	toBombusS_Consume,
	toBombusS_Sleep,
	toBombusS_EatEgg,
	toBombusS_LayEgg,
	toBombusS_Incubate,
	toBombusS_AddWax,
	toBombusS_Fan,
	toBombusS_Leave,
	toBombusS_SearchMate,
	toBombusS_Mate,
	toBombusS_SearchHibernation,
	toBombusS_Hibernate,
	toBombusS_SearchColony,
	toBombusS_FoundColony,
	toBombusS_StealColony,
	toBombusS_ColonyStolen,
	toBombusS_Die,
	toBombusS_Warm,
	toBombusS_Cool,
	toBombusS_Dominate,
	toBombusS_Other,
	toBombusS_Foo
};
//--------------------------------------------------------------------------------------------------------------------------------
/**
Bombus Colony and cluster life stages modelled
*/
using TTypeOfBombusColonyLifeStages = enum TTypeOfBombusColonyLifeStages
{
	to_Colony = 0,
	to_Cluster
};
//--------------------------------------------------------------------------------------------------------------------------------
/**
Bombus life stages modelled
*/
using TTypeOfBombusLifeStages = enum TTypeOfBombusLifeStages
{
	to_Gyne = 2,
	to_Queen,
	to_Egg,
	to_Larva,
	to_Pupa,
	to_Worker,
	to_Male
};
//--------------------------------------------------------------------------------------------------------------------------------
/**
Colonies can be in a number of locations. See \cite<Liczner2019>;
*/
using TTypeOfColonyLocation = enum TTypeOfColonyLocation
{
	toColonyLoc_Underground = 0,
	toColonyLoc_Surface,
	toColonyLoc_AboveGround,
	toColonyLoc_BoxField,
	toColonyLoc_BoxLab,
	toColonyLoc_GyneChoose,
	toColonyLoc_Foo
};
//--------------------------------------------------------------------------------------------------------------------------------
/**
*Workers get bigger as the colony gets older, due to a combination of an increase in the number of workers and loss of
* influence of the queen.
*/
using TTypeOfLarvaDevControl = enum TTypeOfLarvaDevControl
{
	toLarvaDev_WorkerNo = 0,
	toLarvaDev_WaxSignal,
	toLarvaDev_QueenStep,
	toLarvaDev_Foo
};
//--------------------------------------------------------------------------------------------------------------------------------
/**
\brief
Function to compare to female size
*/
bool CompareMass(Bombus_Worker* a_Female1, Bombus_Worker* a_Female2);
bool CompareMassLarva(Bombus_Larva* a_Larva1, Bombus_Larva* a_Larva2);

/**
 * \brief Calculates all the f(x) values for ae^bx where e, also known as Euler's number approximately equal to 2.71828,
 * a and b are constants and x is the index of the vector.
 * \param a_a Constant before the e.
 * \param a_b Constant raising the e to a level multiplied by the vector index, x. 
 * \param a_Length int length of the vector.
 * \return a vector of length a_Length containing double.
 */
vector<double> CalculateExpCurve(double a_a, double a_b, int a_Length);
/**
 * \brief Does the same as @ref CalculateExpCurve(), but returning rounded integer values instead. ;
 * */
vector<int> CalculateExpCurveInt(double a_a, double a_b, int a_Length);



vector<double>  PreCalculateEnergyPowCurve(double a_min, double a_max, bool a_homeotherm);

/**
Class representing double values that represent physical quantity, never less than zero.
*/
class Quantity
{
protected:
	/** \brief A double quantity*/
	double m_quantity{};
public:
	Quantity()
	{
		Init();
	}

	void Init()
	{
		m_quantity = 0.0;
	}

	/** \brief Take an amount or what is available from the quantity.*/
	double TakeQuantity(double a_amount)
	{
		if (a_amount <= m_quantity)
		{
			m_quantity -= a_amount;
			return a_amount;
		}
		double available = m_quantity;
		m_quantity = 0.0;
		return available;
	}

	/** \brief Take a proportion of the total.*/
	double TakeProportion(double a_Prop)
	{
		double anAmount = m_quantity * a_Prop;
		m_quantity -= anAmount;
		return anAmount;
	}

	/** \brief Take all that is available.*/
	double TakeAll()
	{
		double available = m_quantity;
		m_quantity = 0.0;
		return available;
	}

	/** \brief Add an amount to the quantity.*/
	void DepositQuantity(double a_amount)
	{
		m_quantity += a_amount;
	}

	/** \brief Set the quantity to a particular value.*/
	void ResetQuantity(double a_amount)
	{
		m_quantity = a_amount;
	}

	/** \brief Report how much there is.*/
	double ReportQuantity()
	{
		return m_quantity;
	}
};

//--------------------------------------------------------------------------------------------------------------------------------
/**
Class representing integer values that represent physical number of things, never less than zero.
*/
class IntQuantity
{
protected:
	/** \brief An integer quantity*/
	int m_quantity = 0;
public:
	/** \brief Constructor always initialises the value to zero.*/
	IntQuantity()
	{
		m_quantity = 0;
	}

	/** \brief Take an integer amount or what is available from the quantity.*/
	int DecreaseQuantity(int a_amount)
	{
		if (a_amount <= m_quantity)
		{
			m_quantity -= a_amount;
			return a_amount;
		}
		int available = m_quantity;
		m_quantity = 0;
		return available;
	}

	/** \brief Add an amount to the quantity.*/
	void IncreaseQuantity(int a_amount)
	{
		m_quantity += a_amount;
	}

	/** \brief Set the quantity to a particular value.*/
	void ResetQuantity(int a_amount)
	{
		m_quantity = a_amount;
		if (a_amount < 0)
		{
			m_quantity = 0;
		}
	}

	/** \brief Report how much there is.*/
	int ReportQuantity()
	{
		return m_quantity;
	}
};

//--------------------------------------------------------------------------------------------------------------------------------
/**
*Class representing the mass of the bees. Collection of \ref<quantity> representing lean,
fat and ovary mass. This allows the growth or loss of mass by consuming food.
*/
class Mass
{
protected:
	/** \brief The owner of the Mass*/
	Bombus_Base* m_owner;
	/** \brief \ref<quantity> class of the lean mass, without fat or ovaries.*/
	Quantity m_LeanMass;
	/** \brief \ref<quantity> class of the maximum lean mass achieved, used to work out when the individual
	starves to death and is necessary to regain before storing fat.*/
	Quantity m_MaxLeanMass;
	/** \brief The energy in a mg of lean mass. */
	double m_EnergyLeanMass = 7.0; //j/mg
	/** \brief Record of if a larva is developing and adding degree minutes or not.*/
	bool m_Developing = false;
	/** \brief In general adults can store fat, juveniles for the purposes of the model cannot.*/
	bool m_CapableStoreFat = false;
	/** \brief \ref<quantity> class of the mass of fat.*/
	Quantity m_FatMass;
	/** \brief The energy in a mg of fat. */
	double m_EnergyFatMass = 37.0; //j/mg
	/** \brief Adult females have ovaries and can grow (or shrink) them.*/
	bool m_HasOvaries = false;

	static double MaxPropOvaries;
	static int DominationThreshold;
	static double OvaryShrinkage;
	static bool JuvenilesHomeotherms;
	static bool AdultsHomeotherms;


	/** \brief \ref<quantity> class of the mass of the ovaries.*/
	Quantity m_OvaryMass;
	/** I am assuming that the ovaries are almost pure protein and hence
	the reason they grow much faster when consuming nectar and pollen.
	I am assuming if fed only pollen they have to convert a substantial amount to
	energy to maintain themselves. But it is the pollen that causes the ovaries to
	grow. With only nectar they can do it, but they would need to synthesize all
	of the proteins themselves which is costly.*/
	static double m_EnergyOvaryMass; ////Energy in protein 17 kJ/g
	/** \brief Pollen or something in it (I have assumed protein) is important for longevity.
	I have added this so we could add that.*/
	double m_ProteinEatenInLife = 0.0;

	static double m_minMass;
	static double m_maxMass;
	static vector<double> m_HomeothermEnergyCurve;
	static vector<double> m_poikilothermyEnergyCurve;


public:
	/** \brief Initialization of the mass variable to link the owner of the mass.*/
	void Init(Bombus_Base* a_bee) { m_owner = a_bee; }
	/** \brief Mass constructor. */
	Mass() { m_owner = nullptr; }

	/** \brief A typical interface function - calculates the energy that is needed to maintain current mass. */
	double CalculateMaintenanceEnergy();
	/** \brief A typical interface function - this one sets the lean mass of the individual used when creating an individual. */
	void HardSetLeanMass(double a_Mass);
	/** \brief A typical interface function - returns if the individual is starving based on if they have lost
	* half their lean mass. */
	bool Starvation()
	{
		if (m_LeanMass.ReportQuantity() <= 0.5 * m_MaxLeanMass.ReportQuantity())
		{
			return true;
		}
		return false;
	}

	/** \brief A typical interface function - reports the lean mass. */
	double ReportLeanMass() { return m_LeanMass.ReportQuantity(); }
	/** \brief A typical interface function - reports if the individual (larva) is developing. */
	bool AskIfDeveloping() { return m_Developing; }
	/** \brief A typical interface function - reports if the individual has a deficit in energy
	*that has caused a loss of lean mass.
	*/
	double ReportEnergyDeficit()
	{
		return (m_MaxLeanMass.ReportQuantity() - m_LeanMass.ReportQuantity()) * m_EnergyLeanMass;
	}

	/** \brief A typical interface function - sets the body fat (used when a gyne turns into a queen) */
	void HardSetBodyFat(double a_BodyFat)
	{
		if (m_CapableStoreFat == true)
		{
			m_FatMass.ResetQuantity(a_BodyFat);
		}
	}

	/** \brief Takes energy only from fat reserves. Performing optional tasks that can be refused.*/
	double TakeEnergyFromFat(double a_Energy)
	{
		if (m_CapableStoreFat == false || m_EnergyFatMass <= 0.0)
		{
			return 0.0;
		}
		return m_FatMass.TakeQuantity(a_Energy / m_EnergyFatMass) * m_EnergyFatMass;
	}

	/** \brief A typical interface function - reports energy stored in fat. */
	double ReportEnergyInFat()
	{
		if (m_CapableStoreFat == false)
		{
			return 0.0;
		}
		return m_EnergyFatMass * m_FatMass.ReportQuantity();
	}

	/** \brief Takes the food that has been digested and partitions it into growth or shrinking of fat, ovaries and lean mass. */
	void DigestedFoodEffect(Food a_FoodPacket);
	/** \brief A typical interface function - reports the mass of fat. */
	double ReportFatMass() { return m_FatMass.ReportQuantity(); }
	/** \brief A typical interface function - reports total mass. */
	double ReportMass()
	{
		double LeanMass = m_LeanMass.ReportQuantity();
		if (m_CapableStoreFat == false)
		{
			return LeanMass;
		}
		return LeanMass + m_FatMass.ReportQuantity() + m_OvaryMass.ReportQuantity();
	}

	/** \brief A typical interface function - sets the ability to store fat. */
	void SetCapableStoreFat(bool a_CapableStoreFat) { m_CapableStoreFat = a_CapableStoreFat; }
	/** \brief Reports the proportion of the total mass that is ovaries. Important to determine competition. */
	double ReportPropOvaries()
	{
		double OvaryMass = m_OvaryMass.ReportQuantity();
		double PropOvaries = OvaryMass / (OvaryMass + m_LeanMass.ReportQuantity());
		return PropOvaries;
	}

	void SetHasOvaries(bool a_HasOvaries) { m_HasOvaries = a_HasOvaries; }
	/** \brief A typical interface function - sets the body fat (used when a gyne turns into a queen) */
	void HardSetOvaryProp(double a_OvaryPropBody);
	double ReportMaxMass() { return m_MaxLeanMass.ReportQuantity(); }


	double GetEnergyNeeded(double a_mass, bool a_homeotherm);

	void SetStatic();
};

//--------------------------------------------------------------------------------------------------------------------------------
/**
*Class representing nectar as an object with properties. Collection of \ref<quantity> volume, mass of sugar and
the energy in that volume of nectar.
*/
class Nectar
{
	/** \brief An object with all the properties of nectar. Means all the conversion are in one place between volume, mass and energy.*/
protected:
	/** \brief Quantity of nectar volume in ul */
	Quantity m_Volume;
	/** \brief Quantity of sugar mass in mg */
	Quantity m_MassSugar; //mg
	/** \brief Quantity of energy in joules (j) */
	Quantity m_Energy;

	static double SugarDensity_mg_per_ul;
	static double EnergyPer_mgSugar;

public:
	/** \brief Nectar contains sugar of a certain density, sucrose for example is 1.587g/cm3 which is 1.587 mg/ul and
	16.3J/mg or KJ/g. \cite<Heinrich2014> suggest that a bee gets ~15.48j/mg of sugar. Although they later suggest 16.21J/mg.*/
	void AddNectarProp(double a_NectarVolume, double a_NectarSugarProp, double a_SugarDensity = SugarDensity_mg_per_ul,
	                   double a_EnergyDensity = EnergyPer_mgSugar);
	/** \brief Add amounts to a packet of nectar.*/
	void AddNectarQuantities(double a_NectarVolume, double a_MassSugar, double a_Energy);
	/** \brief Add everything in another packet of nectar to this packet of nectar.*/
	void AddNectarPacket(Nectar a_Packet);
	/** \brief Report how much sugar is in the nectar.*/
	double ReportMassSugar() { return m_MassSugar.ReportQuantity(); }
	/** \brief Report how much energy is in the nectar.*/
	double ReportEnergy() { return m_Energy.ReportQuantity(); }
	double ReportVolume() { return m_Volume.ReportQuantity(); }
	/** \brief Take a packet of nectar equal to a volume.*/
	Nectar TakeNectar(double a_NectarVolume);
	/** \brief Take a packet of nectar equal to a mass of sugar.*/
	Nectar TakeNectarEqualsToSugar(double a_MassSugar);
	/** \brief Take a packet of nectar with a certain amount of energy in it.*/
	Nectar TakeNectarEqualsToEnergy(double a_Energy);

	void SetStatic();
};

//--------------------------------------------------------------------------------------------------------------------------------
/**
*Class representing pollen as an object with properties. Collection of \ref<quantity> of
* pollen mass, mass of sugar, energy, mass protein, mass of fatty acids and total volume.
*Energy in protein 17 kJ/g
*Energy in sugar ~15.48 kJ/g
* Energy in fatty acids 37 kJ/g
*/
class Pollen
{
protected:
	/** \brief mg of pollen*/
	Quantity m_PollenMass;
	/** \brief mg of sugar*/
	Quantity m_MassSugar;
	/** \brief Quantity of energy in joules (j) */
	Quantity m_Energy;
	/** \brief mg of protein*/
	Quantity m_ProteinMass;
	/** \brief mg of fatty acids*/
	Quantity m_MassFattyAcids;
	/** vegetable fat is something like 912mg/ml or 0.9232mg/ul \cite<Awogbemi2019>
	* sugar 1.59 mg / ul
	* protein 1.35 mg/ul
	* There is also a proportion of pollen that is vitamins, minerals, "ash" or waste.
	*/
	Quantity m_Volume;

	static double EnergyPer_mgProtein;
	static double EnergyPer_mgSugar;
	static double EnergyPer_mgFat;
	static double ProteinDensity_mg_per_ul;
	static double SugarDensity_mg_per_ul;
	static double FatDensity_mg_per_ul;
	static double AshDensity_mg_per_ml;


public:
	/** Add protein amount in mg to class. https://en.wikipedia.org/wiki/Bee_pollen gives bee pollen as
	"40�60% simple sugars (fructose and glucose), 20�60% proteins, 3% minerals and vitamins, 1�32% fatty
	acids, and 5% diverse other components." citing \cite<Mohammad2020>. I iterated though the possible
	combinations, and have set as default the combined averages.
	This should probably be changed to fit with the data in \cite<Vaudo2020>*/
	void AddPollenWithProp(double a_PollenMass, double a_PropProtein, double a_PropSugar, double a_PropFat)
	{
		m_PollenMass.DepositQuantity(a_PollenMass);
		m_ProteinMass.DepositQuantity(a_PollenMass * a_PropProtein);
		double StoredProtein = m_ProteinMass.ReportQuantity();
		m_MassSugar.DepositQuantity(a_PollenMass * a_PropSugar);
		double StoredSugar = m_MassSugar.ReportQuantity();
		m_MassFattyAcids.DepositQuantity(a_PollenMass * a_PropFat);
		double StoredFattyAcids = m_MassFattyAcids.ReportQuantity();
		double StoredEnergy = StoredProtein * EnergyPer_mgProtein + StoredSugar * EnergyPer_mgSugar +
			StoredFattyAcids * EnergyPer_mgFat;
		m_Energy.ResetQuantity(StoredEnergy);
		/** vegetable fat is something like 912mg/ml or 0.9232mg/ul \cite<Awogbemi2019>
		* sugar 1.59 mg / ul
		* protein 1.35 mg/ul
		* There is also a proportion of pollen that is vitimins, minerals, "ash" or waste.
		* By mass, using the defaults its about 9%. for ease, could just add on 10% to the volume.
		* If we assume this is cellulose, this is 1.5 mg/ul.
		*/
		m_Volume.DepositQuantity(
			StoredProtein / ProteinDensity_mg_per_ul + StoredSugar / SugarDensity_mg_per_ul + StoredFattyAcids /
			FatDensity_mg_per_ul + a_PollenMass * (1 - (a_PropProtein + a_PropSugar + a_PropFat)) / AshDensity_mg_per_ml);
	}

	void AddPollenWithMasses(double a_PollenMass, double a_ProteinMass, double a_SugarMass, double a_FattyMass)
	{
		if (a_PollenMass <= 0.0)
		{
			return;
		}
		m_PollenMass.DepositQuantity(a_PollenMass);
		m_ProteinMass.DepositQuantity(a_ProteinMass);
		double StoredProtein = m_ProteinMass.ReportQuantity();
		m_MassSugar.DepositQuantity(a_SugarMass);
		double StoredSugar = m_MassSugar.ReportQuantity();
		m_MassFattyAcids.DepositQuantity(a_FattyMass);
		double StoredFattyAcids = m_MassFattyAcids.ReportQuantity();
		double StoredEnergy = StoredProtein * EnergyPer_mgProtein + StoredSugar * EnergyPer_mgSugar +
			StoredFattyAcids * EnergyPer_mgFat;
		m_Energy.ResetQuantity(StoredEnergy);
		/** vegetable fat is something like 912mg/ml or 0.9232mg/ul \cite<Awogbemi2019>
		* sugar 1.59 mg / ul
		* protein 1.35 mg/ul
		* There is also a proportion of pollen that is vitimins, minerals, "ash" or waste.
		* By mass, using the defaults its about 9%. for ease, could just add on 10% to the volume.
		* If we assume this is cellulose, this is 1.5 mg/ul.
		*/
		m_Volume.DepositQuantity(
			StoredProtein / ProteinDensity_mg_per_ul + StoredSugar / SugarDensity_mg_per_ul + StoredFattyAcids /
			FatDensity_mg_per_ul + a_PollenMass * (1 - ((a_ProteinMass + a_SugarMass + a_FattyMass) / a_PollenMass)) /
			AshDensity_mg_per_ml);
	}

	/** Take a packet of pollen with a particular mass of pollen.*/
	Pollen TakePollenMass(double a_MassPollen)
	{
		double TotalPollen = m_PollenMass.ReportQuantity();
		double AvailablePollen = m_PollenMass.TakeQuantity(a_MassPollen);
		double Prop = 0.0;
		if (TotalPollen > 0.0)
		{
			Prop = AvailablePollen / TotalPollen;
		}
		double AvailableProtein = m_ProteinMass.TakeProportion(Prop);
		double AvailableSugar = m_MassSugar.TakeProportion(Prop);
		double AvailableFatty = m_MassFattyAcids.TakeProportion(Prop);
		m_Energy.TakeProportion(Prop);
		m_Volume.TakeProportion(Prop);
		Pollen packet;
		packet.AddPollenWithMasses(AvailablePollen, AvailableProtein, AvailableSugar, AvailableFatty);
		return packet;
	}

	/** Take a packet of pollen with a particular mass of protein in it.*/
	Pollen TakePollenWithProtein(double a_MassProtein)
	{
		double TotalProtein = m_ProteinMass.ReportQuantity();
		double AvailableProtein = m_ProteinMass.TakeQuantity(a_MassProtein);
		double Prop = 0.0;
		if (TotalProtein > 0.0)
		{
			Prop = AvailableProtein / TotalProtein;
		}
		double AvailablePollen = m_PollenMass.TakeProportion(Prop);
		double AvailableSugar = m_MassSugar.TakeProportion(Prop);
		double AvailableFatty = m_MassFattyAcids.TakeProportion(Prop);
		m_Energy.TakeProportion(Prop);
		m_Volume.TakeProportion(Prop);
		Pollen packet;
		packet.AddPollenWithMasses(AvailablePollen, AvailableProtein, AvailableSugar, AvailableFatty);
		return packet;
	}

	/** Take a packet of pollen with a particular mass of fatty acids in it.*/
	Pollen TakePollenWithFattyAcids(double a_MassFattyAcids)
	{
		double TotalFatty = m_MassFattyAcids.ReportQuantity();
		double AvailableFatty = m_MassFattyAcids.TakeQuantity(a_MassFattyAcids);
		double Prop = 0.0;
		if (TotalFatty > 0.0)
		{
			Prop = AvailableFatty / TotalFatty;
		}
		double AvailablePollen = m_PollenMass.TakeProportion(Prop);
		double AvailableProtein = m_ProteinMass.TakeProportion(Prop);
		double AvailableSugar = m_MassSugar.TakeProportion(Prop);
		m_Energy.TakeProportion(Prop);
		m_Volume.TakeProportion(Prop);
		Pollen packet;
		packet.AddPollenWithMasses(AvailablePollen, AvailableProtein, AvailableSugar, AvailableFatty);
		return packet;
	}

	/** Take a packet of pollen with a particular mass of sugar in it.*/
	Pollen TakePollenWithSugar(double a_MassSugar)
	{
		double TotalSugar = m_MassSugar.ReportQuantity();
		double AvailableSugar = m_MassSugar.TakeQuantity(a_MassSugar);
		double Prop = 0.0;
		if (TotalSugar > 0.0)
		{
			Prop = AvailableSugar / TotalSugar;
		}
		double AvailablePollen = m_PollenMass.TakeProportion(Prop);
		double AvailableProtein = m_ProteinMass.TakeProportion(Prop);
		double AvailableFatty = m_MassFattyAcids.TakeProportion(Prop);
		m_Energy.TakeProportion(Prop);
		m_Volume.TakeProportion(Prop);
		Pollen packet;
		packet.AddPollenWithMasses(AvailablePollen, AvailableProtein, AvailableSugar, AvailableFatty);
		return packet;
	}

	double ReportPollenMass() { return m_PollenMass.ReportQuantity(); }
	double ReportProteinMass() { return m_ProteinMass.ReportQuantity(); }
	double ReportMassSugar() { return m_MassSugar.ReportQuantity(); }
	double ReportMassFattyAcids() { return m_MassFattyAcids.ReportQuantity(); }
	double ReportEnergy() { return m_Energy.ReportQuantity(); }
	double ReportVolume() { return m_Volume.ReportQuantity(); }

	void SetStatic();
};

class Food //: public Nectar, public Pollen
{
protected:
	/** \brief mg of protein*/
	Quantity m_ProteinMass;
	/** \brief mg of fatty acids*/
	Quantity m_MassFattyAcids;
	Quantity m_Energy;
	Quantity m_MassSugar;
	Quantity m_Volume;

	static double EnergyPer_mgProtein;
	static double ProteinDensity_mg_per_ul;


public:
	void AddFood(double a_MassSugar, double a_MassFattyAcids, double a_ProteinMass, double a_Energy, double a_Volume)
	{
		m_Energy.DepositQuantity(a_Energy);
		m_MassFattyAcids.DepositQuantity(a_MassFattyAcids);
		m_MassSugar.DepositQuantity(a_MassSugar);
		m_ProteinMass.DepositQuantity(a_ProteinMass);
		m_Volume.DepositQuantity(a_Volume);
	}

	void AddFoodPacket(Food a_FoodPacket)
	{
		AddFood(
			a_FoodPacket.m_MassSugar.ReportQuantity(),
			a_FoodPacket.m_MassFattyAcids.ReportQuantity(),
			a_FoodPacket.m_ProteinMass.ReportQuantity(),
			a_FoodPacket.m_Energy.ReportQuantity(),
			a_FoodPacket.m_Volume.ReportQuantity()
		);
	}

	/** \brief Add everything in another packet of nectar to this packet of nectar.*/
	void AddNectarPacket(Nectar a_Packet);

	Food TakeFoodVol(double a_Volume)
	{
		double TotalVol = m_Volume.ReportQuantity();
		double AvailableVol = m_Volume.TakeQuantity(a_Volume);
		double Prop;
		if (TotalVol > 0)
		{
			Prop = AvailableVol / TotalVol;
		}
		else
		{
			Prop = 0.0;
		}
		double AvailableSugar = m_MassSugar.TakeProportion(Prop);
		double AvailableProtein = m_ProteinMass.TakeProportion(Prop);
		double AvailableFatty = m_MassFattyAcids.TakeProportion(Prop);
		double AvailableEnergy = m_Energy.TakeProportion(Prop);
		Food packet;
		packet.AddFood(AvailableSugar,
			AvailableFatty,
			AvailableProtein,
			AvailableEnergy,
			AvailableVol);
		return packet;
	}

	Food TakeFoodEnergy(double a_Energy)
	{
		double TotalEnergy = m_Energy.ReportQuantity();
		double AvailableEnergy = m_Energy.TakeQuantity(a_Energy);
		double Prop = 0.0;
		if (TotalEnergy > 0.0)
		{
			Prop = AvailableEnergy / TotalEnergy;
		}
		double AvailableSugar = m_MassSugar.TakeProportion(Prop);
		double AvailableProtein = m_ProteinMass.TakeProportion(Prop);
		double AvailableFatty = m_MassFattyAcids.TakeProportion(Prop);
		double AvailableVol = m_Volume.TakeProportion(Prop);
		Food packet;
		packet.AddFood(AvailableSugar, AvailableFatty, AvailableProtein, AvailableEnergy, AvailableVol);
		return packet;
	}

	Food PacketAllProtein()
	{
		double AllProtein = m_ProteinMass.TakeAll();
		double EnergyInProtein = m_Energy.TakeQuantity(AllProtein * EnergyPer_mgProtein);
		double VolumeProtein = m_Volume.TakeQuantity(AllProtein / ProteinDensity_mg_per_ul); //mg / ul
		Food NewPacket;
		NewPacket.AddFood(0, 0, AllProtein, EnergyInProtein, VolumeProtein);
		return NewPacket;
	}

	double ReportEnergy() { return m_Energy.ReportQuantity(); }
	double ReportVolume() { return m_Volume.ReportQuantity(); }
	double ReportProteinMass() { return m_ProteinMass.ReportQuantity(); }
	double ReportSugarMass() { return m_MassSugar.ReportQuantity(); }

	void SetStatic();
};

class Stomach
	/**\brief Bees can take food into their stomach. Adults can then give this to larva. */
{
protected:
	/** \brief The maximum volume of the stomach.*/
	double m_MaxVolumeCapacity; //ul
	Food m_FoodInStomach;
	/** \brief The owner of the stomach*/
	Bombus_Base* m_owner;
	bool m_PollenOnBoard = false;
	// Mass m_Mass;
	//Mass* m_BeeMass;

	static double SugarDigestionRate;
	static double ConcentrationAdultAddToNectar;
	static double EnergyPer_mgProtein;
	static double MassPollenConcInNectar;
	static double ProteinDensity_mg_per_ul;
	static double EnergyDensityFlesh;


public:
	/** \brief Initialization of the nectar variable with a large default capacity. As a default I have assumed larva only
	have through put and no stomach as such. Using \cite<Muth2014> initial larval size = 1.5 * 0.71 + 12.135. ul*/
	void Init(Bombus_Base* a_bee, double a_MaxVolumeCapacity);

	Stomach()
	{
		m_owner = nullptr;
		m_MaxVolumeCapacity = 0.0;
	}

	/** \brief If an individual grows presumably the stomach grows. This is only applicable for larva
	and they may not have "stomachs" per se.*/
	void UpdateMaxCapacity(double a_MaxVolumeCapacity) { m_MaxVolumeCapacity = a_MaxVolumeCapacity; }
	/** \brief Report the volume of food in the stomach.*/
	double ReportVolumeFood() { return m_FoodInStomach.ReportVolume(); }
	/** \brief Over time I have assumed some of the food in the stomach is digested.
	This may not be the case if adults have a nectar stomach.*/
	Food Digest(double a_FoodVolume);
	/** \brief Adults can regurgitate food to feed to larva or store in nectar cells.*/
	Food Regurgitate(double a_FoodVolume);
	bool ReportPollenOnBoard() { return m_PollenOnBoard; }
	void SetStatic();

	double GetMaxVolumeCapacity() { return m_MaxVolumeCapacity; }

	Food* PointToFood() { return &m_FoodInStomach; }

	double ReturnConcentrationAdultAddToNectar() { return ConcentrationAdultAddToNectar; }
	double ReturnEnergyPer_mgProtein() { return EnergyPer_mgProtein; }
	double ReturnMassPollenConcInNectar() { return MassPollenConcInNectar; }
	double ReturnProteinDensity_mg_per_ul() { return ProteinDensity_mg_per_ul; }
	void SetPollenOnBoard(bool a_PollenOnBoard) { m_PollenOnBoard = a_PollenOnBoard; }
};


class Bombus_Colony : public TAnimal
{
	/**
	Bumblebee gynes found colonies and become queens.
	Temperature,
	how much food is stored,
	the ratio of larva to workers,
	the switching point past,
	and competition point past
	are important determinants of the behaviour of bumblebees associated with the colony.
	Instead of bumblebees querying the landscape, for some things, each bee should query the colony.
	*/
protected:
	/** \brief Variable to record current behavioural state */
	TTypeOfBombusState m_CurrentBoState;
	/** \brief This is a time saving pointer to the correct population manager object */
	Bombus_Population_Manager* m_OurPopulationManager{};
	/** \brief The temperature of the the colony will depend on the temperature either outside, or of the soil */
	static double m_TempToday;
	static double m_SoilTempToday;
	/** \brief The temperature of the colony */
	double m_ColonyTemp{};



	/** \brief Static containing how many adults are needed to cool the colony at a particular temperature.*/
	static vector<int> m_FanningAtTemp;

	int m_AdultsFanning = 0;
	/** \brief Amount of wax */
	double m_wax = 0.0;
	/** \brief Queens wax*/
	double m_QWax = 0.0;
	/** \brief Workers wax*/
	double m_WWax = 0.0;
	/** \brief Queen chemical in wax. */
	double m_ChemSignal = 0;
	/** \brief Mass of colony*/
	double m_MassColony{};
	/** \brief The colony ID, so the bees know which colony they belong to. */
	unsigned long m_ColonyID{};
	/** \brief A typical member variable - this one is the age in days */
	int m_AgeColony = 0;

	/** \brief Current time-step in this animals life */
	int ColonyTimeStep = 0;
	/** \brief Colony location */
	TTypeOfColonyLocation m_ColonyLoc;
	/** \breif The day begins in twilight so it isn't yet fully light*/
	bool m_daylight = false;
	/** \breif The day begins in twilight so it isn't yet fully dark anymore*/
	bool m_dark = false;
	/** \brief The proportion of daylight today from the landscape */
	static double m_DaylightPropToday;
	static double m_NightProp;
	double BackgroundTemp();


	static double FedNectarPerDay;
	static double FedSugarProp;
	static double FedPollenPerDay;
	static TTypeOfLarvaDevControl LarvaDevControl;
	static double Larvalv;
	static double LarvalDMmin;
	static double LarvalDMmax;
	static double LarvalC;
	static double LarvalQ;
	static double LarvalB;
	static double LarvalA;
	static double LarvalW;
	static double LarvalBWax;
	static double LarvalvWax;

	static vector<double> m_MultipleDailyColonyTemps;
	static double ConstantTemp;

	static double ColonyInsulationCoef;

	static double AdultColWarmCoef;


	static double OptimumColTemp;

	static double PropProtein;
	static double PropSugar;
	static double PropFat;


public:
	long m_WarmBodies = 0;
	double m_SumWarmBodies = 0.0;
	double m_TotalAdultMass = 0;
	double m_TotalClusterMass = 0;
	double m_TotalClusterTemp = 0;
	/** \brief Switching point passed */
	bool m_SwitchingPointPast = false;

	/** \brief The colony (really the queen and workers) need to know how many workers and larva are in the colony*/
	unsigned long NoEggs = 0;
	unsigned long NoQueensMaleEggs = 0;
	unsigned long NoWorkerMaleEggs = 0;

	unsigned long NoLarva = 0;
	unsigned long NoLarvaToFeed = 0;

	/** \brief The colony needs to keep track of the adults (for temperature), the colony is created by a gyne/queen, so must start with one adult in it. */
	unsigned long NoWorkers = 0;
	unsigned long NoGyne = 0;
	unsigned long NoMales = 0;
	unsigned long NoQueens = 0;
	unsigned long NoAdults = 0;

	unsigned long NoAdultCorpses = 0;
	unsigned long NoDeadEggs = 0;
	unsigned long NoLarvaCorpses = 0;
	unsigned long NoDeadPupa = 0;

	/** \brief Old pupal cells can be used for nectar or pollen*/
	unsigned long EmptyCell = 0;
	/** \brief Queens build an initial NectarCell when they found the colony (either before or maybe after laying eggs) */
	unsigned long NectarCell = 0;
	/** \brief The Queen does not appear to build a pollen cell in the beginning?*/
	unsigned long PollenCell = 0;

	/** \brief Larval DD varying with colony age, workers or wax. Running the model at 10mins, degree minutes makes more sense than degree days,*/
	double LarvalDM = 0.0;
	/** \brief Clusters within the colony*/
	vector<Bombus_Cluster*> m_ColonyClusters;
	/** \brief A vector of all of the females in the colony.*/
	vector<Bombus_Worker*> m_ColonyFemales;
	vector<Bombus_Worker*> m_ActiveColonyWorkers;
	vector<Bombus_Gyne*> m_ActiveColonyGynes;
	vector<Bombus_Male*> m_ActiveColonyMales;
	Bombus_Queen* m_ColonyQueen;
	//vector<Bombus_Worker*>* GetWorkers() { return &m_ColonyFemales; }
	/** Pollen pots may act as a time bottleneck-
	"Observations made during the terrestris experiment indicated that a queuing
	of workers occurred at the pollen pots. Workers often spent over 5 min feeding
	at these pots, during which time other individuals were denied ac- cess, suggesting
	that there may be an upper limit to the number of workers simultaneously engaged in
	feeding larvae." \cite<Pendrel1981> p.74 This limit may play a part in colony homeostasis (p.75).
	Full nectar pots cause foragers to forage less \cite<Goulson2010>
	*/
	/** \brief Amount of nectar */
	Nectar m_Nectar;
	/** \brief Amount of pollen */
	Pollen m_Pollen;
	/** \brief Static variable giving baseline daily mortality */
	static double m_mortality;
	/** \brief Colony constructor */
	Bombus_Colony(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_ColonyID,
	              TTypeOfColonyLocation a_location);
	/** \brief Colony reinitialise object methods */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_ColonyID,
	                    TTypeOfColonyLocation a_location);
	void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_ColonyID,
	          TTypeOfColonyLocation a_location);
	int GetAgeColony() { return m_AgeColony; }
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override;
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void EndStep() override;

	/** \brief A typical interface function - this one returns the colony ID */
	unsigned long GetColonyID() { return m_ColonyID; }
	/** \brief Set the colony ID*/
	void SetColonyID(unsigned long a_ColonyID) { m_ColonyID = a_ColonyID; }
	//void SetTemp(int a_temperature) { m_TempToday = int(floor(a_temperature + 0.5)); } for some reason the temp was modified for osmia
	/** \brief Set the temperature outside  */
	void SetOutsideTemp(double a_temperature) { m_TempToday = a_temperature; }
	/** \brief Get the temperature outside */
	double GetOutsideTemp() { return m_TempToday; }
	/** \brief Set the soil temperature */
	void SetSoilTemp(double a_SoilTempToday) { m_SoilTempToday = a_SoilTempToday; }
	/** Get the soil temperature today */
	double GetSoilTemp() { return m_SoilTempToday; }

	/** \brief Set the lab temperature */
	void SetLabTemp();
	/** Get the lab temperature today */
	double GetLabTemp() { return ConstantTemp; }

	/** Setting the temp is protected, but individuals need to be able to get the temp in the colony.*/
	double GetColonyTemp() { return m_ColonyTemp; }
	void SetColonyTemp(double a_Temp) { m_ColonyTemp = a_Temp; }


	int ReportAdultsFanning() { return m_AdultsFanning; }
	/** \brief Add wax to colony.*/
	void AddWax(double a_wax) { m_wax += a_wax; }

	void AddQWax(double a_wax, double a_ChemConc)
	{
		m_QWax += a_wax;
		m_ChemSignal += a_wax * a_ChemConc;
	}

	void AddWWax(double a_wax) { m_WWax += a_wax; }
	/** \brief Report wax in colony*/
	double ReportWax() { return m_wax; }
	/** \brief Report concentration of chemical queen adds to wax in wax.*/
	double ReportChemConc()
	{
		if (m_wax <= 0.0)
		{
			return 0.0;
		}
		return m_ChemSignal / m_wax;
	}

	bool RemoveCluster(Bombus_Cluster* a_cluster);
	void RemoveAdult(Bombus_Base* a_Adult);

	void SetStatic();

	void AddNectarPot(){NectarCell++;}
	bool FirstNectarCellComplete();

	/** \brief Asks colony if it has eggs. */
	bool EggsInColony() {return NoEggs > 0;}

	bool LarvaInColony(){return NoLarva > 0;}

	void SetColoniesQueen(Bombus_Queen* a_Queen){m_ColonyQueen = a_Queen;}

	vector<double> GetMultipleDailyColonyTemps(){return m_MultipleDailyColonyTemps;}

	

protected:
	/** \brief "Behavioural state" here means just updating what is going on in the colony. */
	virtual TTypeOfBombusState st_Dying();
	double CalculateColonyTemp();
};

class Bombus_Cluster : public TAnimal
	/**
	When laying eggs the queen builds an egg cell to lay eggs into. Over time this develops into a cluster of larva and then pupa.
	The queen and workers investigate a cluster to decide wether to incubate or feed lara. The larva cools when it isn't being incubated.
	*/
{
protected:
	/** \brief Variable to record current behavioural state */
	TTypeOfBombusState m_CurrentBoState;
	/** \brief This is a time saving pointer to the correct population manager object */
	Bombus_Population_Manager* m_OurPopulationManager{};
	/** \brief Pointer to the colony */
	Bombus_Colony* m_OurColony{};
	IntQuantity Remaining;
	IntQuantity m_TotalTime;
	double m_temp{};
	int m_Age = 0;
	vector<Bombus_Egg*> m_ClusterEggs;
	vector<Bombus_Larva*> m_ClusterLarva;
	vector<Bombus_Pupa*> m_ClusterPupa;
	IntQuantity m_Feedings;
	Food m_Food;
	//double m_DM;
	/** \brief Current timestep in this cluster */
	int timestep{};
	/** \brief Amount of wax */
	double m_wax = 0.0;
	/** \brief Total juvinile mass*/
	double m_JuvMass = 0.0;
	double m_EggMass = 0.0;
	double m_PupaMass = 0.0;


	static double InitialEggTemp;
	static double MinMass;
	static double OptimumDevTemp;


	static double ClusterLengthPerEgg;
	static double ClusterWidthPerEgg;
	static double ClusterHieght;

	static double ClusterHeatCapacityJperg;
	static double AlphaOfAir;


	double m_ClusterLengthmm{};
	double m_ClusterWidthmm{};
	double m_ClusterHeighthmm{};

	Bombus_Worker* m_FocusedOnBy = nullptr;


public:
	///** \brief Returns how many degreee days inhabitants of the cluster will have gained in a day.*/
	//double GetDM() {
	//	return m_DM;
	//}
	/** \brief Returns the temperature of the cluster.*/
	double GetTemp()
	{
		return m_temp;
	}

	/** \brief Set the temperature (by the worker or queen). */
	void SetTemp(double a_temp) { m_temp = a_temp; }
	/** \brief calculate k value for use when heating and cooling the cluster.*/
	double Get_k();
	/** \brief Juviniles can as how many degree days they have got today*/
	double CalculateDMDeveloped();
	/** \brief Report how long the cluster has been incubated */
	int GetTimeIncubated()
	{
		return m_TotalTime.ReportQuantity();
	}

	/** \brief This adds a new bee object to the cluster */
	void AddEgg(Bombus_Egg* a_egg)
	{
		m_ClusterEggs.push_back(a_egg);
	}

	/** \brief Removes a bee object from the cluster */
	bool RemoveEgg(Bombus_Egg* a_egg)
	{
		/**
		* Returns true if successful and false if the object is not found
		*/
		for (auto i = m_ClusterEggs.begin(); i < m_ClusterEggs.end(); ++i)
		{
			if (*(i) == a_egg)
			{
				m_ClusterEggs.erase(i);
				return true;
			}
		}
		return false; // An error because the a_bee pointer was not found
	}

	/** \brief This adds a new bee object to the cluster */
	void AddLarva(Bombus_Larva* a_Larva)
	{
		m_ClusterLarva.push_back(a_Larva);
	}

	/** \brief Removes a bee object from the cluster */
	bool RemoveLarva(Bombus_Larva* a_Larva)
	{
		/**
		* Returns true if successful and false if the object is not found
		*/
		for (auto i = m_ClusterLarva.begin(); i < m_ClusterLarva.end(); ++i)
		{
			if (*(i) == a_Larva)
			{
				m_ClusterLarva.erase(i);
				return true;
			}
		}
		return false; // An error because the a_bee pointer was not found
	}

	void AddPollenLump(double a_MassPollen);
	void EatAllClusterEggs();
	Food* PointToPollenLump() { return &this->m_Food; }
	void AddEggMass(double a_mass) { m_EggMass += a_mass; }
	void RemoveEggMass(double a_mass) { m_EggMass -= a_mass; }
	void AddPupaMass(double a_mass) { m_PupaMass += a_mass; }
	void RemovePupaMass(double a_mass) { m_PupaMass -= a_mass; }
	vector<Bombus_Larva*>* GetLarva() { return &m_ClusterLarva; }
	vector<Bombus_Egg*>* GetEggs() { return &m_ClusterEggs; }
	vector<Bombus_Pupa*>* GetPuppa() { return &m_ClusterPupa; }
	/** \brief This adds a new pupa object to the cluster */
	void AddPupa(Bombus_Pupa* a_Pupa)
	{
		m_ClusterPupa.push_back(a_Pupa);
	}

	/** \brief Removes a pupa object from the cluster */
	bool RemovePupa(Bombus_Pupa* a_Pupa)
	{
		/**
		* Returns true if successful and false if the object is not found
		*/
		for (auto i = m_ClusterPupa.begin(); i < m_ClusterPupa.end(); ++i)
		{
			if (*(i) == a_Pupa)
			{
				m_ClusterPupa.erase(i);
				return true;
			}
		}
		return false; // An error because the a_bee pointer was not found
	}

	/** \brief Returns the number of juvinile bees in the cluster */
	int GetNumberJuv()
	{
		return int(m_ClusterEggs.size() + m_ClusterLarva.size() + m_ClusterPupa.size());
	}

	/** \brief Returns the number of eggs in the cluster*/
	int GetNumberEggs() { return int(m_ClusterEggs.size()); }
	/** \brief Returns the number of larva in the cluster*/
	int GetNumberLarva() { return int(m_ClusterLarva.size()); }
	/** \brief Add wax to cluster.*/
	void AddWax(double a_wax) { m_wax += a_wax; }
	/** \brief Report wax*/
	double ReportWax() { return m_wax; }
	/** \brief Set the mass of the juviniles in the cluster */
	void SetJuvMass(double a_JuvMass) { m_JuvMass = a_JuvMass; }
	/** \brief Report juvinile mass*/
	double ReportJuvMass() { return m_JuvMass; }
	/** \brief An external entity (bee, parasite, disease) can kill the inhabitants and therefore cluster*/
	void KillThis() override;

	void SetState(TTypeOfBombusState a_state) { m_CurrentBoState = a_state; }
	bool IsDead();


	/** \brief Static variable giving baseline daily mortality */
	static double m_mortality;
	/** \brief Cluster constructor */
	Bombus_Cluster(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberYoung,
	               Bombus_Colony* a_Colony = nullptr);
	/** \brief Cluster reinitialise object methods */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberYoung,
	                    Bombus_Colony* a_Colony = nullptr);

	void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberYoung,
	          Bombus_Colony* a_Colony);
	/** \brief "Behavioural state" here means just updating what is going on in the clucter. */
	virtual TTypeOfBombusState st_Dying();
	virtual TTypeOfBombusState st_Warm();
	virtual TTypeOfBombusState st_Cool();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void EndStep() override;


	void SetStatic();

	void SetFocusedOnBy(Bombus_Worker* a_Female)
	{
		m_FocusedOnBy = a_Female;
	}


	/** \brief Check if another is interacting with the cluster in this timestep	 */
	bool GetIfNotFocusedOn();
	bool GetIfFocusedOn();

	int GetMinutesAge() {return m_Age;}

};

class Bombus_Base : public TAnimal
{
	/**
	A Bombus must have some simple functionality:
	Inititation and development
	And some simple characteristics, herein age.
	Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
	NB All areas are squares of size length X length
	This class should never actually be used to create an object, it is a base class from which all other bee types are defined.
	*/
protected:
	/** \brief The thorax width in mm */
	double m_ThoraxWidth = 0;
	/** \brief Variable giving baseline daily mortality */
	double m_mortality{};
	/** \brief Variable giving daily mortality, set as m_mortality and then modified for a specific day..*/
	double m_mortalityToday{};

	mutable bool m_Senesce = false;

	/** \brief Adults are being affected by the dirty colony. \cite<moret2009> suggests this will likely differ based on when an individual is born. But can be simple for now.*/
	double m_DeadEffected = 0.0;

	double m_BelowTempCoef = 0.0;
	double m_AboveTempCoef = 0.0;

	bool m_CompetitionPossible = false;
	int m_Damage = 0;

	/** \breif The temperature of the adult.*/
	double m_Temp{};

	bool m_Callow = true;

	/** \brief Juviniles belong to a cluster.*/
	Bombus_Cluster* m_OurCluster{};

	/** \brief Pointer to the colony */
	Bombus_Colony* m_OurColony{};
	/** \breif The identity of the individual.*/
	TTypeOfBombusLifeStages m_MyIdentity;
	string m_MyIdentityString;
	/** \brief Variable to record current behavioural state */
	TTypeOfBombusState m_CurrentBoState;
	/** \brief This is a time saving pointer to the correct population manager object */
	Bombus_Population_Manager* m_OurPopulationManager{};
	/** \brief A typical member variable - this one is the age in minutes */
	int m_Age = 0;
	Mass m_Mass;
	/** \brief id from parent one, the mother */
	unsigned long m_MotherID = 0;
	/** \brief id from parent the additional parent, the father */
	unsigned long m_FatherID{}; //Left for future functionality
	/** \breif At the moment the age of a stage might be useful*/
	int m_StageAge = 0;
	int m_StageAgeDay = 0;
	double m_AgeDegrees = 0.0;
	/** \brief I needed a way to keep track of haploid eggs */
	bool m_isHaploid = false;
	///** \brief Energy they would need to maintain mass. */
	//double m_MaintenceEnergy = 0;
	///** \brief Energy they would need to grow in mass. */
	//double m_GrowthEnergy = 0;
	///** \brief Energy they would need to do tasks. */
	//double m_ActivityEnergy = 0;
	///** \brief Energy they would need to maintain and grow in mass. */
	//double m_EnergyNeeded = 0;
	IntQuantity m_SleepNeeded;
	IntQuantity m_SleepWanted;
	/** \brief Current timestep in this animals life */
	int timestep = 0;
	/** \brief The time that has been spent in current state*/
	int timeInState{};

	/** \brief This allows the target for food to be switched from that held to the cluster food.*/
	Food* m_FoodSource{};
	/** \brief Food currently being carried*/
	Stomach m_Stomach;
	int m_TimesDominated = 0;
	//double m_MaxComfortTemp = 32;
	/** \brief adults id */
	unsigned long m_id = 0;
	/** \brief If given all the food they want, this is the mass the larva could achieve next.*/
	double m_PottentialNextMass = 0.0;

	/** \brief Bumblebee sex is determined by having two or only one copy of a sex allele \cite<>. */
	int m_sexLocus_1 = 0;
	int m_sexLocus_2 = 0;
	static double AdultBodyDensity;
	static double AdultHeatCapacityJperg;
	static double AlphaOfAir;
	static double BodyTempAdults;


	static bool AccessForaging;
	static double B1EggCellPollen;
	static double B2EggCellPollen;
	static double B3EggCellPollen;
	static double CallowForDM;
	static double CallowMinTemp;

	static TTypeOfColonyLocation ColonyLocation;
	static double DigestionRate;
	static double DominationThreshold;

	static int EggMax;
	static double EggMin;
	static double EnergyDensityEgg;
	static bool FeedLargestFirst;
	static double GrowthG;
	static TTypeOfLarvaDevControl LarvaDevControl;
	static double LarvaGyneDMcutoff;
	static double LarvalDMmax;
	static double LarvalDMmin;
	static double LarvalMaleDM;
	static int MaxAgeWorkerMakeWax;
	static double MaxMass;
	static double MaxPropOvaries;
	static double MinAgeWokersCompete;
	static double MinJuvDevTemp;
	static double MinMass;


	static double OptimumColTemp;
	static double OptimumDevTemp;
	static double OvaryShrinkage;
	static double PropBroodCare;
	static double PropDMmaxIsGyne;
	static double RootIncubatingProb;
	static double StartWorkerMass;
	static double StepEnergyInspection;
	static double SugarDigestionRate;

	static double waxConcCoef;
	static int AdultsEncounteredInStep;
	static double WorkersEggCellPollen;
	static double waxConcInter;
	static double MinColonyTempToLay;
	static double QueenTempIncr;
	//static double denominator;

	static double FanningEnergyPerMin;
	static double denominator;

	static bool AdultsHomeotherms;


	/** \breif The workers can have multiple "jobs", but the biggest distinction is if foraging or not.*/
	bool m_Forager = false;

	bool m_StartingIndiv = false;

	//double m_AgeMortalityMulti = 1.0;
	vector<double> m_AgeMortMultiVect{};

public:
	/** \brief Bombus constructor */
	Bombus_Base(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, Bombus_Colony* a_Colony);
	/** \brief Bombus reinitialise object methods */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, Bombus_Colony* a_Colony);
	/** \brief Initialisation code for Constructor and ReInit */
	virtual void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, Bombus_Colony* a_Colony);

	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override
	{
	} // NB this is not used in the Bombus_Base code
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override
	{
	} // NB this is not used in the Bombus_Base code
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void EndStep() override
	{
	} // NB this is not used in the Bombus_Base code
	/** \brief A typical interface function - this one sets the colony of the individual - it can be 0 */
	void SetColony(Bombus_Colony* a_Colony)
	{
		m_OurColony = a_Colony;
	}

	/** \brief A typical interface function - this one returns the colony of the individual */
	Bombus_Colony* GetColony() { return m_OurColony; }
	/** \brief A typical interface function - this one sets the MotherID of the individual  */
	void SetMotherID(unsigned long a_MotherID) { m_MotherID = a_MotherID; }
	/** \brief A typical interface function - this one returns the MotherID of the individual */
	unsigned long GetMotherID() { return m_MotherID; }
	/** \brief Set ID of the father*/
	void SetFatherID(unsigned long a_FatherID) { m_FatherID = a_FatherID; }
	/** \brief A typical interface function - this one returns parent two */
	unsigned long GetFatherID() { return m_FatherID; }
	/** \brief Return a pointer to mass*/
	Mass* PointToMass()
	{
		return &this->m_Mass;
	}

	void Dominate();
	void Damaged(bool a_CompetitionAggression);
	void AddToWarmAccount();

	int ReportTimesDominated() { return m_TimesDominated; }
	void ResetTimesDominated() { m_TimesDominated = 0; }
	/** \breif Returns the identity of the individual.*/
	TTypeOfBombusLifeStages ReportIdentity()
	{
		return m_MyIdentity;
	}

	void SetNextMass(double a_NextMass) { m_PottentialNextMass = a_NextMass; }
	double ReportPotentialNextMass() { return m_PottentialNextMass; }
	/** \brief Set my individual ID */
	void SetMyID(unsigned long a_id) { m_id = a_id; }
	/** \brief Get my individual ID */
	unsigned long GetMyID() { return m_id; }
	/** \brief A typical interface function - this one returns the cumulative AgeDegrees of the eggs */
	double GetAgeDegrees() { return m_AgeDegrees; }
	/** \brief A typical interface function - this one sets the cumulative AgeDegrees of the eggs */
	void SetAgeDegrees(double a_AgeDegrees) { m_AgeDegrees = a_AgeDegrees; }
	void AddAgeDegrees(double a_AgeDegrees) { m_AgeDegrees += a_AgeDegrees; }
	void PointToFoodSource(Food* a_FoodSource) { m_FoodSource = a_FoodSource; }
	double GetTotalMass() { return m_Mass.ReportMass(); }

	/** \brief This allows the individuals used to start the simulation to be identified by setting them as starting. */
	void SetStartingIndiv(bool a_StartingIndiv) { m_StartingIndiv = a_StartingIndiv; }

	virtual void SetStatic();

	double ProbV1(double a_temp);

	//double ProbV2(double a_temp);

	virtual bool IsForager() { return m_Forager; }


	void RecordVitals();

	/** \brief Workers, gynes, queens and males calculate mortality stressors the same way and inherit from base. */
	virtual void MortalityStressors();

	void SetSexLocus_1(int a_SexLocus_1) { m_sexLocus_1 = a_SexLocus_1; }
	void SetSexLocus_2(int a_SexLocus_2) { m_sexLocus_2 = a_SexLocus_2; }

	int GetSexLocus_1() { return m_sexLocus_1; }
	int GetSexLocus_2() { return m_sexLocus_2; }


	virtual vector<double>* GetExpMort(){return &m_AgeMortMultiVect; }

	virtual bool GetCompetitionPoint() {return false;}

	virtual void RemoveSelfFromColony();

	bool SetFanning();

protected:
	/** \brief The individual determines how much food it has and grows or shrinks (uses fat or wastes away) */
	void Grow();
	/** \brief Behavioural state dying */
	virtual TTypeOfBombusState st_Dying() { return toBombusS_Foo; }
	virtual TTypeOfBombusState st_Fan();


	void SetMortality(double a_mortality) { m_mortality = a_mortality; }


	void EatNectar();
	void EatPollen();

	/** \breif Individuals can check they are in the colony. */
	bool InColony();
	void CalculateMyBodyTemp();
};

class Bombus_Egg : public Bombus_Base
{
	/**
   Extends the Bombus_Base for specialisms related to being an egg.
   This class should never actually be used to create an object, it is a egg class from which all other bee egg types are defined.
	*/
protected:
	/** \brief all bombus eggs will have the same day degrees threshold */
	double m_DM = 0.0;

	static double m_EggDM;
public:
	/** \brief This Bombus_Egg constructor */
	Bombus_Egg(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	           unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, Bombus_Colony* a_Colony,
	           Bombus_Cluster* a_Cluster = nullptr);
	/** \brief Bombus_Egg ReInit for object pool */
	void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,  // NOLINT(clang-diagnostic-overloaded-virtual)
	            // NOLINT(clang-diagnostic-overloaded-virtual)
	            unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, Bombus_Colony* a_Colony,
	            Bombus_Cluster* a_Cluster = nullptr);
	/** \brief The Step is the second 'part' of the time-step that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the time-step that an animal can behave in. It is called once per time-step. */
	void EndStep() override;
	/** \brief Initialization code for Constructor and ReInit */
	void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,  // NOLINT(clang-diagnostic-overloaded-virtual)
	          // NOLINT(clang-diagnostic-overloaded-virtual)
	          unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, Bombus_Colony* a_Colony,
	          Bombus_Cluster* a_Cluster);

	/** \brief Eggs, larva and pupa calculate mortality stressors the same way and inherit from eggs. */
	void MortalityStressors() override;

	/** \brief Juveniles having their cluster assigned*/
	void SetCluster(Bombus_Cluster* a_Cluster) { m_OurCluster = a_Cluster; }
	/** \brief Inquiry into what cluster this juvinile belongs to*/
	Bombus_Cluster* GetCluster() { return m_OurCluster; }


	void SetDM(double a_DM) { m_DM = a_DM; }

	void SetStatic() override;
	/** \brief If an egg is eaten it still dies, but it is rather imediate and doesn't leave a dead egg.*/
	virtual void Eaten();

protected:
	/** \brief Behavioural state development */
	virtual TTypeOfBombusState st_Develop();
	/** \brief Behavioural state hatch */
	virtual TTypeOfBombusState st_Hatch();
	/**\brief Two ways an egg can die, which are similar. This is the similar code. */
	void RemoveEgg();
	/** \brief Behabiour state die */
	TTypeOfBombusState st_Dying() override;
};

class Bombus_Larva : public Bombus_Egg
{
protected:
	/** \brief This is the temperature over the minimum developement temperature*/
	double TempOverMin = 0.0;
	int m_NumberQInteractions = 0;
	int m_NumberWInteractions = 0;
	/** \brief Static variable giving baseline daily mortality */
	//static double m_larvamortality;
	double LarvaMortalitySlope{};
	double LarvaMortalityIntercept{};

	static int m_LarvaStageHardLimit;

	int m_timesFed = 0;
	double m_minsQueenCanReduce = 0.0;

	static double StomachCapacityPermgBodyWeight;
	static double EnergyDensityFlesh;
	static double PupaDMDivisionOfLarval;

	/** \brief The larva can hold food that it is given and eat from this. */
	Food m_HeldFood;
public:
	/**\brief Flag larva sets to state it is a gyne if conditions are met. Proxy for JH. */
	bool m_IAmGyne = false;
	/**\brief Flag larva sets to state it is a male if conditions are met. I have assumed that males are detectable.*/
	bool m_IAmMale = false;

	/** \brief Bombus_Larva constructor */
	Bombus_Larva(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	             unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, Bombus_Colony* a_Colony,
	             Bombus_Cluster* a_Cluster);
	/** \brief Bombus_WorkerLarvaReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	                    unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age,
	                    Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster);
	/** \brief Initialisation code for Constructor and ReInit */
	void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	          unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, Bombus_Colony* a_Colony,
	          Bombus_Cluster* a_Cluster);
	/** \brief A typical interface function - this one returns the age */
	int GetAge() { return m_Age; }
	/** \brief A typical interface function - this one returns the age */
	void SetAge(int a_age) { m_Age = a_age; }
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override;
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void EndStep() override;
	/** \brief Calculate how much the individual could grow if given necessary food*/
	double CalculatePotentialGrowth();
	/** \brief Calculate how much energy would be needed to grow to full pottential*/
	double CalculateGrowthEnergy();
	/** \brief Ask the larva if it is hungry*/
	bool Hungry();
	double FoodHeld() { return m_HeldFood.ReportVolume(); }
	/** \brief Feed a_food (as an amount of energy) to the larva*/
	void Feed(Food a_Volume);
	double GetMass() { return m_Mass.ReportMass(); }

	void InteractQueen()
	{
		m_NumberQInteractions++;
	}

	void InteractWorker()
	{
		m_NumberWInteractions++;
	}

	void SetminsQueenCanReduce(double a_minsQueenCanReduce) { m_minsQueenCanReduce = a_minsQueenCanReduce; }

	void SetStatic() override;
protected:
	/** \brief Behavioural state development */
	TTypeOfBombusState st_Develop() override;
	/** \brief Behavioural state Dying */
	TTypeOfBombusState st_Dying() override;
	/** \brief Behavioural state pupate */
	virtual void Pupate();
	/** \brief Behavioural state of feeding self*/
	virtual TTypeOfBombusState st_Consume();
};

class Bombus_Pupa : public Bombus_Larva
{
public:
	/** \brief Bombus_Pupa constructor */
	Bombus_Pupa(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	            unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
	            double a_BodyFat, Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster, bool a_isGyne, double a_DM);
	/** \brief Bombus_Pupa ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,  // NOLINT(clang-diagnostic-overloaded-virtual)
	                    // NOLINT(clang-diagnostic-overloaded-virtual)
	                    unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
	                    double a_BodyFat, Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster, bool a_isGyne,
	                    double a_DM);
	/** \brief Initialisation code for Constructor and ReInit */
	void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	          unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass, double a_BodyFat,
	          Bombus_Colony* a_Colony, Bombus_Cluster* a_Cluster, bool a_isGyne, double a_DM);
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override;
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void EndStep() override;
	/** \brief A typical interface function - this one returns the Mass */
	void SetMass(double a_Mass) { m_Mass.HardSetLeanMass(a_Mass); }

	void SetStatic() override;
protected:
	/** \brief Behavioural state development */
	TTypeOfBombusState st_Develop() override;
	/** \brief Behavioural state for emerging from the pupa */
	virtual TTypeOfBombusState st_Emerge();
	/** \brief Behavioural state dying */
	TTypeOfBombusState st_Dying() override;
};

class Bombus_Worker : public Bombus_Base
{
	/**
	Bombus females have overies, and if they manage to get enough nutrients the overies develop. The more developed the overies are the more aggressive the worker is to intruders,
	to the queen and other workers. Workers can lay haploid eggs when developed and will attempt to steal the queens eggs and eat them. Workers vary in size and larger workers forage
	outside of the nest. Workers become habituated to a task, but can switch if the colony needs them to.
	*/
protected:
	///** \brief Pollen baskets have a limit in how much pollen they can store. But workers seem to bring back similar amounts indipendent of size */
	//double m_Pollen_capacity;
	///** \brief Nectar is stored in a nectar stomach and larger individuals can bring back more. */
	//double m_Nectar_capacity;
	/** \brief Pollen currently being carried*/
	Quantity m_PollenLoad;
	/** \brief Energy onboard */
	//quantity m_EnergyLoad;
	/** \brief This is the cluster the worker or queen is focused on*/
	Bombus_Cluster* m_FocusCluster = nullptr;
	/** \brief The number of eggs laid per cell*/
	IntQuantity m_NumberEggsPerCell;
	/** \brief Number of cells per day*/
	int m_NumberCellsPerDay = 0;

	static int m_WorkerNoLayDamageThreshold;

	int m_BroodsLaid;

	/** \brief Gynes get sperm from males which they use once they are queens. This information is passed onto diploid individuals.*/
	unsigned long m_SpermID = 0;

	/** \brief Foraging bees will return to the same resource location if successful*/
	APoint m_GoodForage;
	/** \brief Foraging time - If a bee didn't find food or if they were taking too long to find more there must be a point they would return to the colony. This might be energy vs reward.*/
	int m_ForagingTime{};
	/** \brief Direction facing */
	double Heading = g_rand_uni() * 2.0 * PI;
	/** \brief pointer to the pollen map object */
	//static BombusPollenMap* m_ThePollenMap;
	/** \brief The temperature outside the colony. Workers will only fly if they like the temperature */
	static double m_TempToday;




	/** \brief The proportion of daylight today from the landscape */
	static double m_DaylightPropToday;


	bool m_SwitchingPointDetected = false;
	int m_numberGyneLarvaToday = 0;



	IntQuantity m_EggsLaidToday;
	/** \brief Behavioural states that are inherited by workers and gynes. */
	virtual TTypeOfBombusState st_Develop();
	virtual TTypeOfBombusState st_Decide();
	virtual TTypeOfBombusState st_Inspect();
	virtual TTypeOfBombusState st_LayEgg();
	//virtual TTypeOfBombusState st_RandomMove();
	//virtual TTypeOfBombusState st_DirectMove(APoint Target, APoint Origin);
	//virtual TTypeOfBombusState st_Forage();
	virtual void EatAnEgg(Bombus_Egg* a_AnEgg);
	virtual void EatAllEggs();
	virtual TTypeOfBombusState st_Incubate();
	virtual TTypeOfBombusState st_Feed();
	virtual TTypeOfBombusState st_Sleep();
	virtual TTypeOfBombusState st_AddWax();
	virtual TTypeOfBombusState st_Dominate();
	/** \brief Behavioural state of feeding self*/
	virtual TTypeOfBombusState st_Consume();
	virtual TTypeOfBombusState st_Other() { return toBombusS_Decide; }
	/** \brief "Behavioural state" here means just updating what is going on in the colony. */
	TTypeOfBombusState st_Dying() override;

	double MyProbabilitySleeping()
	{
		return double(m_SleepNeeded.ReportQuantity()) / (10.0 * (144.0 - double(timestep)));
	}

	double MyProbabilityPrioritisingFood() { return (2 - (2 * m_Mass.ReportLeanMass() / m_Mass.ReportMaxMass())); }


	/** \brief Workers check eggs and at certain times of the colony life gain information or eat eggs.*/
	virtual void CheckClusterEggs(Bombus_Cluster* a_FocusCluster);

	/** \brief Supposedly workers can only add wax during the first week or so of adulthood.*/
	virtual bool CanIAddWax();

	virtual bool CanLayEggs();

	/** \brief Larva are informaed they have interacted with a worker, queen or gyne. */
	virtual void IncrementLarvaeInteractions(Bombus_Larva* a_Larva) { a_Larva->InteractWorker(); }


	

	double m_minsQueenCanReduce{};

	static double m_EggDM;
	static int MaxSleep;
	static int MinSleep;
	static double StomachCapacityPermgBodyWeight;

	
	static double MaxCocoonMass;
	static double WaxEnergyDensity;
	static double DivisionEggAtDangerOfEaten;
	static double PropEggProtein;
	static double VolumeLarvalFeeding;

	static vector<double> m_AgeMortMultiVectWorker;

	virtual void AddTypeWax(double a_WaxAmount) { GetColony()->AddWWax(a_WaxAmount); }

public:
	/** \brief Bombus_Worker constructor a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id*/
	Bombus_Worker(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	              unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
	              double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id = 0);
	/** \brief Bombus_Worker ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,  // NOLINT(clang-diagnostic-overloaded-virtual)
	                    // NOLINT(clang-diagnostic-overloaded-virtual)
	                    unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
	                    double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id = 0);
	/** \brief Initialisation code for Constructor and ReInit */
	void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,  // NOLINT(clang-diagnostic-overloaded-virtual)
	          // NOLINT(clang-diagnostic-overloaded-virtual)
	          unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass, double a_BodyFat,
	          double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id);
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override;
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void EndStep() override;
	

	/** \brief The temperature outside which Bombus sp. determine by sticking there rear end out the door  */
	void SetTemp(double a_temperature) { m_TempToday = a_temperature; }

	void SetBodyTemp(double a_temperature) { m_Temp = a_temperature; }

	/** \brief The proportion of daylight outside*/
	void SetDaylightProp(double a_DaylightPropToday) { m_DaylightPropToday = a_DaylightPropToday; }
	/** \brief Record of amount fed to larva in a day.*/
	double PollenFedToLarva = 0;
	/** The worker or queen knows how long they took the get somewhere and therefore how long back, but this might be too perfect a knowledge*/
	int travelTime = 0;
	/** \brief Set my age*/
	void SetAge(int a_age) { m_Age = a_age; }
	/** \brief Set my mass*/
	void SetMass(double a_Mass) { m_Mass.HardSetLeanMass(a_Mass); }
	void SetBodyFat(double a_BodyFat) { m_Mass.HardSetBodyFat(a_BodyFat); }
	double GetOvaryProp() { return m_Mass.ReportPropOvaries(); }
	bool GetIfOvaryMax();

	bool GetCompetitionPoint() override {return m_CompetitionPossible;}


	void ZeroEggsLaidToday() { m_EggsLaidToday.ResetQuantity(0); }
	int ReportEggsLaidToday() { return m_EggsLaidToday.ReportQuantity(); }
	void ChooseRandomCluster();
	bool IsForager() override { return m_Forager; }
	
	void SetminsQueenCanReduce(double a_minsQueenCanReduce) { m_minsQueenCanReduce = a_minsQueenCanReduce; }

	void SetStatic() override;

	void SetCallow(bool a_callow) { m_Callow = a_callow; }

	virtual int GetMatesSexLocus();


	virtual double PollenLumpToAdd() { return WorkersEggCellPollen; }


	void SetFocusOn(Bombus_Cluster* a_Cluster){m_FocusCluster = a_Cluster;}

	void RemoveFemaleSelfFromColony();
	void RemoveSelfFromColony() override;
protected:
	virtual void IncrementColonyMaleEggs(int a_NumberOfEggs);

	void IncrementEggsLaidToday(int a_NumberOfEggs);
	

	virtual void SetCurrentBrood(){}

	vector<double>* GetExpMort() override {return &m_AgeMortMultiVectWorker;}



};

class Bombus_Gyne : public Bombus_Worker
{
	/**
	Gynes primary purpose is to gain body fat and mate before finding a site to hibernate. Although gynes sometimes act like workers for a short time if their birth colony
	has a need of them, foraging and performing nest duties including defence. After hibernating the gyne feeds and developes their overies and then founds a colony at which
	stage the gyne becomes a queen.
	*/
protected:
	/** \brief Gynes need to know if they have hibernated - although this may differ if second summer colony*/
	int m_DaysHibernated = 0;
	bool m_hibernated = false;

	int m_StartingColWorkers{};

	double m_ChemicalConc = 0;

	/** \brief Gynes get sperm from males, this represents the sex allele.*/
	int m_MatesSexLocus = 0;

	static double MinHibernationDuration;
	static double HibernationWakeUpTemp;

	static vector<double> m_AgeMortMultiVectGyne;

public:
	/** \brief Bombus_Gyne  constructor */
	Bombus_Gyne(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	            unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
	            double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id,
	            unsigned long a_SpermID = 0);
	/** \brief Bombus_Gyne  ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,  // NOLINT(clang-diagnostic-overloaded-virtual)
	                    unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
	                    double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony,
	                    unsigned long a_id, unsigned long a_SpermID = 0);
	/** \brief Initialisation code for Constructor and ReInit */
	void Init();
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override;
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	//virtual void EndStep(); //This is the same as for the worker.


	unsigned long GetSpermID() { return m_SpermID; }

	void SetChemicalConc(double a_ChemicalConc) { m_ChemicalConc = a_ChemicalConc; }
	void SetDaysHibernated(int a_DaysHibernated);
	void SetStatic() override;
	void SetStartingColWorkers(int a_StartingColWorkers) { m_StartingColWorkers = a_StartingColWorkers; }

	/** \brief Gyne gets the ID of the male she mates with.*/
	void SetSpermID(unsigned long a_SpermID) { m_SpermID = a_SpermID; }
	/** \brief Gyne gets the sex allele that the male sperm contains.*/
	void SetMateSexAllele(int a_MateSexLocus) { m_MatesSexLocus = a_MateSexLocus; }

	int GetMatesSexLocus() override { return m_MatesSexLocus; }

	void RemoveSelfFromColony() override;

protected:
	/** \brief Behavioural states specific to gynes */
	TTypeOfBombusState st_Decide() override;
	//virtual TTypeOfBombusState st_SearchMate();
	//virtual TTypeOfBombusState st_Mate();
	//virtual TTypeOfBombusState st_SearchHibernation();
	//virtual TTypeOfBombusState st_Hibernate();
	//virtual TTypeOfBombusState st_SearchColony();
	virtual TTypeOfBombusState st_FoundColony();
	//virtual TTypeOfBombusState st_StealColony();
	TTypeOfBombusState st_Dying() override;
	/** \brief Behavioural state of feeding self*/
	TTypeOfBombusState st_Consume() override;

	/** \brief I am not having gynes do this, while I am having workers and queens do this.*/
	void CheckClusterEggs(Bombus_Cluster* a_FocusCluster) override{
	}

	/** \brief Supposedly workers can only add wax during the first week or so of adulthood. Presumably gynes don't until they are queens.*/
	bool CanIAddWax() override { return false; }

	/** \brief Larva are informaed they have interacted with a worker, queen or gyne. Assuming gynes act like workers in this case.*/
	void IncrementLarvaeInteractions(Bombus_Larva* a_Larva) override { a_Larva->InteractWorker(); }

	vector<double>* GetExpMort() override {return &m_AgeMortMultiVectGyne;}
};

class Bombus_Queen : public Bombus_Gyne
{
	/**
	Queens are gynes that have established a colony or stollen one, and start laying eggs. For our purposes a gyne becomes a queen
	on establishing a colony (choosing a location). Queens can still do everything a worker can, although they usually stop
	foraging once their are workers. Unlike honey bees they don't just lay eggs. The queen builds and caps egg cells in which she
	has laid eggs, possibly as a defence against workers eating her eggs.
	*/
protected:
	/** According to \cite<Duchateau1988> brood 1: contains 3 - 8 cells, each with 2 eggs in, brood 2: 6-13 cells each with
	* 5 eggs in and brood 3+ 2 cells and 8 in each. All of this varying by temperature. From the perspective of incubation
	* I think the whole of brood1 or brood2 will be incubated together as 1. The egg cells will affect initial feeding,
	* and the series of Hobbs papers suggest this is a major difference between species. This is only relevant until the
	* individual larva can hold there own food (10mg) or possibly until the seperate themselves. For simplicity I am going
	* to make the whole cluster "one cell" and vary the number of eggs in it. Assuming that competition will happen only
	* until they are 10mg in mass.
	*
	* B2 doesn't make a lot of sense. This is the same as four days of laying. I wonder if the second brood is layed
	* over successive days.
	*
	*/
	//int EggCellsB1 = 1;
	//int EggCellsB2Day = 1;
	//int EggCellsB3Day = 1;
	static double MassWaxFirstNectarPot;

	static vector<double> m_AgeMortMultiVectQueen;

	static int m_QueenNoLayDamageThreshold;

public:
	/** \brief Bombus_Queen  constructor */
	Bombus_Queen(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	             unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
	             double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id,
	             unsigned long a_SpermID = 0);
	/** \brief Bombus_Queen  ReInit for object pool */
	void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	            unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
	            double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id,
	            unsigned long a_SpermID = 0) override;
	/** \brief Initialisation code for Constructor and ReInit */
	void Init(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	          unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_MatesSexLocus, int a_age,
	          double a_Mass, double a_BodyFat, double a_OvaryProp, Bombus_Colony* a_Colony, unsigned long a_id,
	          unsigned long a_SpermID);
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override;
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	//virtual void EndStep();// This is the same as for the workers.


	void SetStatic() override;

	int GetMatesSexLocus() override;



	double PollenLumpToAdd() override;

	void BuildFirstNectarPot();

	bool CanLayEggs() override;

	void RemoveSelfFromColony() override;

	void SetCurrentBrood() override;

protected:
	/** \brief Behavioural states of queen and not gynes. */
	TTypeOfBombusState st_Decide() override;
	/** \brief "Behavioural state" here means just updating what is going on in the colony. */
	TTypeOfBombusState st_Dying() override;


	/** \brief The queen check eggs and eats any diploid eggs or any not her own.*/
	void CheckClusterEggs(Bombus_Cluster* a_FocusCluster) override;
	/** \brief Supposedly workers can only add wax during the first week or so of adulthood. Presumably a queen can always, or at least for much longer (B2 or B3). */
	bool CanIAddWax() override { return true; }
	void AddTypeWax(double a_WaxAmount) override { GetColony()->AddQWax(a_WaxAmount, m_ChemicalConc); }

	/** \brief Larva are informed they have interacted with a worker, queen or gyne. */
	void IncrementLarvaeInteractions(Bombus_Larva* a_Larva) override;

	void IncrementColonyMaleEggs(int a_NumberOfEggs) override;

	vector<double>* GetExpMort() override {return &m_AgeMortMultiVectQueen;}
	

};

class Bombus_Male : public Bombus_Base
{
protected:
	/** \brief The location of the current nest, holds -1 in m_x when no nest */
	APoint m_CurrentColonyLoc;



	static double StomachCapacityPermgBodyWeight;




	static vector<double> m_AgeMortMultiVectMale;

public:
	static double m_ProbMaleLeaving;
	/** \brief Bombus_Male constructor */
	Bombus_Male(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,
	            unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
	            double a_BodyFat, Bombus_Colony* a_Colony, unsigned long a_id = 0);
	/** \brief Bombus_Male ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID,  // NOLINT(clang-diagnostic-overloaded-virtual)
	                    unsigned long a_FatherID, int a_SexLocus_1, int a_SexLocus_2, int a_age, double a_Mass,
	                    double a_BodyFat, Bombus_Colony* a_Colony, unsigned long a_id = 0);

	/** \brief Initialisation code for Constructor and ReInit */
	void Init(unsigned long a_MotherID, int a_age, double a_Mass, double a_BodyFat);  // NOLINT(clang-diagnostic-overloaded-virtual)
	// NOLINT(clang-diagnostic-overloaded-virtual)
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void BeginStep() override;
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	void Step() override;
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	void EndStep() override;
	/** \brief "Behavioural state" here means just updating what is going on in the colony. */
	TTypeOfBombusState st_Dying() override;
	/** \brief Behavioural states specific to males */


	/** \brief A typical interface function - this one returns the age */
	int GetAge() { return m_Age; }
	/** \brief A typical interface function - this one returns the age */
	void SetAge(int a_age) { m_Age = a_age; }

	void SetStatic() override;


	void RemoveSelfFromColony() override;

protected:
	/** \brief Behavioural state development */
	virtual TTypeOfBombusState st_Develop();
	/** \brief Behavioural state for emerging from the pupa */
	virtual TTypeOfBombusState st_Decide();
	virtual TTypeOfBombusState st_Leave();
	virtual TTypeOfBombusState st_MatePatrol();
	virtual TTypeOfBombusState st_Mate();
	virtual TTypeOfBombusState st_Sleep();
	/** \brief Behavioural state of feeding self*/
	virtual TTypeOfBombusState st_Consume();

	vector<double>* GetExpMort() override {return &m_AgeMortMultiVectMale;}

};

TTypeOfColonyLocation StringToLocation(string a_str);
TTypeOfLarvaDevControl StringToControl(string a_str);

#endif
