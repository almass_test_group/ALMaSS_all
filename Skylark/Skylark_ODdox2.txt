/**
\page Skylark_page ALMaSS Skylark ODdox Documentation

\htmlonly 
<style type="text/css">
body {
    color: #000000
    background: white;
  }
h2  {
    color: #0022aa
    background: white;
  }
</style>
  <h1>
<small>
<small>
<small>
Version 1.0<p>
<small>
Created by<br>
</small>
</small>
Chris J. Topping<br>
<small>Department of Wildlife Ecology<br>
National Environmental Research Institute, Aarhus University<br>
Grenaavej 14<br>
DK-8410 Roende<br>
Denmark<br>
<br>
19th March 2011<br>
</small>
</small>
</small>
</h1>
<br>
\endhtmlonly
\n
<HR> 
<br>
\htmlonly
<h2><big>The skylark model description following ODdox protocol</big></h2>
\endhtmlonly
<br>
\htmlonly
<h2><big>Overview</big></h2>
\endhtmlonly
<br>
<h2>  1. Purpose </h2>
<br>
The skylark model simulates the ecology and behaviour of the skylark (<I>Alauda arvensis</I>) in Denmark. 
The skylark is a passerine bird, originating in steppes and thus preferring an open habitat for breeding. 
It is a common bird in agricultural areas in Denmark. Its food is both plant and arthropods, but it relies on 
arthropods during the breeding season, and for feeding the young.<br>
<br>
The skylark model was developed to enable impacts of landscape, management, climate and policy changes on skylark 
populations in Denmark. A key element in its development, along with the other ALMaSS models, was to integrate 
population modelling with a high resulution dynamic environment. Initially, the skylark model was developed for 
use in pesticide risk assessment, but subsequently has been utilised in many other forms of impact assessment. 
A primary design aim was therefore flexibility to cope with a range of inputs and provision of many output signals. 
This is achieved primarily by the use of mechanistic modelling processes and high level of realism. <br>
<br>
<br>
<h2>  2. State variables and scales </h2>
<br>
Skylarks are modelled as individuals belonging to one of five classes: <br>
1) Skylark_Clutch - containing a number of eggs. Exisiting between the first day of laying and hatch (inclusive). <br>
2) Skylark_Nestling - existing from hatch until nest-leaving. <br>
3) Skylark_PreFledgeling - existing from nest-leaving until maturity.<br>
4) Skylark_Male - existing from maturity to death.<br>
5) Skylark_Female - existing from maturity to death.<br>
<br>
<br>
<h2>  3. Process Overview and Scheduling</h2>
<br>
<br>
\image html MaleSkylarkStateTransitionDiagram.jpg 
\htmlonly <h1> <small> <small>
Male Skylark state-transition diagram. Red arrows indicate external events causing transition to this state.
</small> </small> </h1> \endhtmlonly
<br>
<br>
\image html FemaleSkylarkStateTransitionDiagram.jpg 
\htmlonly <h1> <small> <small>
Female Skylark state-transition diagram. Red arrows indicate external events causing transition to this state. Hatched arrow is a crossing arrow that does not connect to those it crosses.
</small> </small> </h1> \endhtmlonly
<br>
<br>

<h2>States</h2>

<br>
<B>Initiation</B>
The skylark enters the simulation as an adult. Immediate transition to floating.<br>
<br>
<B>Floating</B>Skylark_Male::st_Floating  Skylark_Female::st_Floating<br>
The bird moves around the simulation area continually trying to find vacant territories of suitable quality via regular transition to Find Territory. If the end of the breeding season is reached, then there is a transition to flocking.<br>
<br>
<B>Find Territory</B> Skylark_Male::st_FindingTerritory<br>
The determination of the territory positions and sizes is complicated. The model achieves this through a pre-processing of the landscape on the day in the year that the first bird returns. This pre-processing determines where in the landscape territories can physically fit, and places them ready for the returning birds to select from. Pre-processing is based upon habitat quality criteria related to the height of vegetation and habitat types, and results in a density of skylark territories per 100Ha. Vegetation height in potentially suitable habitats was evaluated based on nesting acceptance criteria (VQ in Table 1). Selection of these territories is based upon the nesting suitability at the time of evaluation; hence, what was a good territory early in the year may be poor later due to vegetation growth (e.g. if planted with winter rape). The result of pre-processing is that territories are therefore fixed each year, and unlike real territories cannot vary in size or shape throughout the season, but may continually change their occupancy status as the season progresses. The male bird therefore assess territories for acceptance as he moves through the landscape. When he finds a suitable territory he will discontinue searching and make a transition to attract mate. An older male skylark can remove a younger male from his territory if that male is not already paired.<br>
<br>
<B>Find Territory </B> Skylark_Female::st_Finding_Territory<br>
This behaviour is the same as Find Territory - Male, except that the female looks for territories containing males trying to attract a mate. If her previous mate is present, she will assess his territory first.<br>
<br>
<B>Emigration</B>Skylark_Male::st_Emigrating Skylark_Female::st_Emigrating<br>
The birds are assumed to be in their over-wintering areas. Date and weather conditions are checked until they are suitable for immigration, at which time there is a transition to Immigration.<br>
<br>
<B>Immigration</B> Skylark_Male::st_Immigrating Skylark_Female::st_Immigrating<br>
On entry to immigration there is a probability test for over-wintering mortality. This test is sex and age dependent. Afterwards there is an immediate transition to Arrive in Simulation Area.<br>
<br>
<B>Arrive in Simulation Area</B> Skylark_Male::st_Arriving Skylark_Female::st_Arriving<br>
A position in randomly chosen for the bird, after which it makes a transition to flocking.<br>
<br>
<B>Flocking Skylark_Male::st_Flocking  Skylark_Female::st_Flocking</B><br>
The bird is assumed to move around the area waiting until weather conditions are suitable to begin Find Territory or Emigration, depending upon the time of year. If before breeding, and weather conditions deteriorate (heavy wind or rain, or snow cover), then there is a transition to Temporarily Leave Area. Otherwise if conditions are good (low ind, low rainfall, and temperature >5�C) There will be a transition to Find Territory. If after breeding, then poor conditions will trigger a transition to Emigration. A transition to Emigration will also occur if the bird has not emigrated until October.<br>
<br>
<B>Temporarily Leave Area</B> Skylark_Male::st_TempLeavingArea Skylark_Female::st_TempLeavingArea<br>
The bird is removed from the simulation area until there have been three consecutive days of temperatures above freezing, after which there is a transition to Arrive in Simulation Area.<br>
<br>
<B>Scare off Chicks</B> Skylark_Male::st_ScaringOffChicks<br>
When the chicks reach 30 days old then they are scared out of the territory by the male.<br>
<br>
<B>Follow Mate (Male)</B> Skylark_Male::st_FollowingMate<br>
The male occupies the same location as the female. When the female's eggs hatch there is a transition to Caring for Young. If the female dies or abandons the territory, there is a transition to Attract a Mate.<br>
<br>
<B>Attract a Mate Skylark_Male::st_AttractingAMate </B><br>
The male waits for a female to select and accept his territory. If the breeding season comes to an end, there is a transition to flocking. If a female arrives, there is a transition to follow mate.<br>
<br>
<B>Attract a Mate Skylark_Female::st_AttractingMate </B><br>
This is a rare event, but can occur if the male dies early in the season. The result is that the female waits until the territory is claimed by another male, then pairs with him.<br>
<br>
<B>Die</B>Skylark_Male::st_Dying Skylark_Female::st_Dying<br>
Remove the skylark from the simulation. Ensure any dependent skylarks are told that the bird has dies (mate, and/or young).<br>
<br>
<B>Establish a Territory </B>Skylark_Male::st_EstablishingATerritory<br>
The male claims the territory, primarily a programming construct, but resulting in the male being registered as occupying the territory.<br>
<br>
<B>Caring for young </B>Skylark_Male::st_CaringForYoung Skylark_Female::st_CaringForYoung<br>
Males and females use the time available to them for foraging to obtain food resources from their home range. These resources are then reduced by the existence metabolism of the bird, the remainder is then fed to the chicks in lots corresponding to hourly feedings. Feeding is even between chicks if there is sufficient food to satiate all chicks - if less food is available, the largest chick will obtain the food. Males stay in this state until the chicks are 30 days old, when there is a transition of Scare Off Chicks. Females stay in this state until the chicks are 18 days old, when they either make a transition to Start a New Brood, or if this is their fourth breeding attempt will wait until the chicks are 30 days old and then do Stopping Breeding.<br>
<br>
<B>Egg Hatch</B> Skylark_Female::st_EggHatching<br>
A transition is made to Care for Young, and a message is sent to the male to do the same.<br>
<br>
<B>Incubation</B> Skylark_Female::st_Incubating<br>
Incubation occurs as described in development above. The female spends that time off the nest required to find energy to cover her basal metabolic requirements, plus that energy required to warm the eggs. Incubation continues until the eggs hatch and there is a transition to Care For Young, or the incubation period (MID) is exceeded, at which point the female will Start New Brood.<br>
<br>
<B>Egg Laying</B> Skylark_Female::st_Laying<br>
The female produces one egg per day, if she has built up sufficient energy reserves to acomplish this.<br>
<br>
<B>Stopping Breeding</B> Skylark_Female::st_StoppingBreeding<br>
The female gives up the territory for this year, informs here mate, and makes a transition to Flocking.<br>
<br>
<B>Building up Resources</B> Skylark_Female::st_BuildingUpResources<br>
The female forages from her home range each day. The food resources she collects are reduced by her existence metabolism, the remaining is then converted to stored energy. When this energy exceeds ECEL, she will make a transition to preparing for breeding Skylark_Female::st_PreparingForBreeding which determines the time needed for egg production and nest building.<br>
a<br>
<B>Make Nest</B> Skylark_Female::st_MakingNest<br>
When weather conditions are better than TTB the female begins construction of a nest in the best nesting habitat in her territory. Construction takes NB days, after which there is a transition to Egg Laying. Energetic resources are continually built up during this period (see Building Up Resources).<br>
<br>
<B>Give Up Territory</B> Skylark_Female::st_GivingUpTerritory<br>
The female gives up the territory for this year, informs here mate, and makes a transition to Floating.<br>
<br>
<B>Start New Brood</B> Skylark_Female::st_StartingNewBrood<br>
The female assesses the habitat quality of the territory. If still suitable (i.e. above MTQ) she will make a transition to Make Nest, otherwise she will go to Give Up Territory.<br>
<br>
<br>


<h2>Development of nestlings and eggs</h2>

The model parameters related to reproduction and development are shown in Table 1. The egg-cooling rate (ECR) during 
periods of inattentiveness is for isolated eggs in still air and includes heat loss due to radiation, convection and 
evaporation of moisture through eggshell. Development rate of the eggs and young is controlled by the rate of feeding
by the adults. In the eggs this is determined by the amount of time incubated, which is related to the length of 
time required for the female to obtain enough food to cover her existence metabolism requirements (EMA). During 
periods of inattentiveness the eggs cool, and are then warmed up again on the bird's return. The impact on 
development of the eggs is thus assumed to be:<br>

\image html SkylarkDevEqn1.jpg

where md is minute degrees, IT is the incubation temperature, Tb is the lowest temperature reached during cooling, 
I is the period of inattentiveness in minutes. Eggs sum minute degrees until the development total (DI) is reached. 
Nestling growth is determined by the rate of feeding of the adults up to a maximum growth rate determined by the 
maximum growth rate found in field data (MGR) [1]. Conversion efficiencies of dry-weight of insects to bird body 
weight were obtained from data from the house sparrow (<I>Passer domesticus</I>) [2]. This data was scaled proportionally 
for the differences in skylark weight and time taken for growth until adult weight. Bad weather such as cold, wet or
windy days (CD, WD, & WID) result in the female skylark spending time keeping the nestlings warm. Since 'Bad weather' 
is a categorical variable, the time used was assumed to be half the day. This was deducted from the time available 
for foraging. The extraction rate (ER), of food from habitats is a scaling parameter that was used to adjust the 
rates of development such that all eggs, nestlings and fledglings developed at the correct rate according to field 
data [1]. This same field study provided data on overall mortality rates of eggs and nestlings, which was used to 
adjust daily predation rates of eggs and nestlings (EMP, NMP) in order to obtain these overall figures. Starvation 
mortality was assumed when the nestlings did not obtain their existence metabolism (EMJ) for two days running. 
Mortality of eggs was assumed if development took longer than 14 days (field data observed maximum before desertion 
of the female).<br>

\image html SkylarkParameterTable.jpg
\htmlonly <h1> <small> <small>
Table 1: Skylark parameter inputs and sources (see references below)<br>
</h1> </small> </small> \endhtmlonly 
<br>
<br>
\htmlonly
<h2><big>Design</big></h2>
\endhtmlonly
<br>
<h2>  4. Design Concepts</h2>
<br>
<br>
<h3>  4.a Emergence</h3>
All aspects of breeding are emergent properties of the energetic relationships resulting from food availability and accessibility. 
These in turn a function of weather, vegetation type and farmer management behaviour. Territory occupancy is a function of territory 
selection behaviour and changes in vegetation structure. Hence, a female will abandon a territory in mid-season if the vegetation 
grows too tall.<br>
<br>
<h3>  4.b. Adaptation</h3>
Adaptation is not used in the model.<br>
<br>
<h3>  4.c Fitness</h3>
Fitness is determined by age of skylark in any territorial disputes. Otherwise fitness is a question of energy dependent survival 
as nestlings or pre-fledgelings.<br> In a sense fitness is determined by the quality of territory chosen by the male.<br> 
<br>
<h3>  4.d Prediction</h3>
The male predicts the quality of territory based on physical ques when selecting a territory. At the time he selects this territory 
it is not possible to know how the vegetation will develop, hence prediction is based on location (if he has a past breeding history),
and the absence of undesriable structures.<br>
<br>
<h3>  4.e Sensing</h3>
Skylarks can sense the food levels and accessibility levels in habitats within their home range. They can also sense other skylark 
territories when searching for a territory.<br>
<br>
<br>
<h3>  4.f Interaction</h3>
Interactions occur between adult skylarks via mate selection, territory competition and caring for young.<br>
<br>
<br>
<h3> 4.g Stochasticity</h3>
Stochasticity is used estensively as the basis for probabilistic decsions, typically associated with mortality events ( e.g. see 
Skylark_Clutch::OnFarmEvent ), but also with other distributions, e.g. clutch size with clutch attempt. Typically these are used 
where there is no mechanical direct way to calculate the effect of an event.<br>
<br>
<h3>  4.h Collectives</h3>
The Skylark_Clutch object is a collective for the eggs in a single clutch.<br>
<br>
<br>
<h3>  4.i Observation</h3>
As with all ALMaSS models there are standard outputs in the form of object-class specific probes (see e.g. probe_data). The skylark 
model also has a number of specific output forms relating to breeding success, and pattern oriented modelling data output.
<br>
<br>
<br>
<h2> 5. Initialisation</h2>
On starting the model the population is initialised with a number of male and female skylarks (configurable). These birds live 
one year before immigrating and becoming breeding birds in the simulation.<br>
<br>
<br>
<h2> 6. Inputs </h2>
Many of the parameters of the skylark model are configurable (e.g. migration mortality cfg_femreturnmortmax() ), although default 
values are those fitted in a pattern oriented exercise (publication in prep). <br>
<br>
<br>
<br>
<h2> 7. Interconnections</h2>
Primary connections are between the skylark classes and Skylark_Population_Manager and the Landscape classes.<br>
<br>
Key ways in which the Skylarks interact with the Landscape class are:<br>
- Finding territory: The model determines territory size and position through a pre-processing of the landscape on the day in the
 year that the first male bird returns. This pre-processing determines where in the landscape territories can physically fit, and 
 places them ready for the returning male birds to select from.  Pre-processing is based upon habitat quality criteria related to 
 the height of vegetation and habitat types, and results in a density of skylark territories per 100Ha. Vegetation height in 
 potentially suitable habitats was evaluated based on nesting acceptance criteria. Selection of these territories is based upon 
 the nesting suitability at the time of evaluation; hence, what was a good territory early in the year may be poor later due to 
 vegetation growth (e.g. if planted with winter rape). The result of pre-processing is that territories are therefore fixed each 
 year, and unlike real territories cannot vary in size or shape throughout the season, but may continually change their occupancy 
 status as the season progresses. The male bird therefore assesses territories for acceptance as he moves through the landscape. 
 When he finds a suitable territory he will discontinue searching and make a transition to attract mate. An older male skylark can 
 remove a younger male from his territory if that male is not already paired.<br>
- Foraging: Skylarks build up resources and care for their young.  Skylarks can sense the food levels and accessibility levels 
in habitats within their home range in the model.  Foraging success is determined by the insect biomass and habitat area for each 
habitat type in the skylark's home range.  Insect biomass is in turn dependent upon management and weather.  <br>
- Development: all skylark stages are affected by the ambient temperature which determines egg development rate and basal 
metabolic energetic requirements.  Inclement weather during incubation or nestling rearing therefore not only affects the length
 of time the parents need for foraging but also increases energetic requirements resulting in a positive feedback situation. <br>
- Preparing for breeding/starting new brood: All aspects of breeding are emergent properties of the energetic relationships 
resulting from food availability and accessibility. These in turn a function of weather, vegetation type and farmer management 
behaviour. Territory occupancy is a function of territory selection behaviour and changes in vegetation structure. Hence, a female 
will abandon a territory in mid-season if the vegetation grows too tall.<br>
.
<br>
Skylark_Population_Manager is responsible for managing lists of the extant birds and carrying out input/output methods. This class is
descended from Population_Manager, which has generally applicable ALMaSS I/O functions, but the Skylark_Population_Manager extends 
these functions and adds a number that are specific to the skylark. For example, Skylark_Population_Manager::ProbePOM which provides the basic
data required for pattern-oriented modelling tests.<br>
<br>
<h2> 8. References</h2>
1  Odderskaer P, Prang A, Poulsen JG, Elmegaard N, Andersen PN.1997. Skylark reproduction in pesticide treated and 
untreated fields. Pesticide Research No 32. Ministy of Environment and Energy, Denmark.<br>
2  Kendeigh SC, Dol'nik VR, Gavrilov VM. 1977. Avian energetics. In Pinowski J, Kendeigh SC, eds, Granivorous Birds 
in Ecosystems. Cambridge University Press, UK, pp 129-197.<br>
3 Drent R. 1975. Incubation. In Farner DS, King JR, Parkes KC, eds, Avian Biology, Vol 5. Academic Press, New York, pp 333-407.<br>
4 Poulsen JG. 1996. Behaviour and parantal care of Skylark Alauda arvensis chicks. Ibis 138:525-531.<br>
5 Delius JD.1965. A population study of Skylarks Alauda arvensis.  Ibis 107:466-491.<br>
6 Haffer J. 1985. Alauda. In Glutz von Blotzheim UN, Bauer KM, eds, Der Vogel Mitteleuropa, Vol 10. AULA-Verlag, Wiesbaden, Germany, pp 229-281.<br> 
7 Castro G, Stoyan N, Myers JP. 1989. Assimilation efficiency in birds: A function of taxon or food type. Comp Biochem Physiol 92A:271-278.<br>
8 Schlapfer A. 1988. Populationsokologie der Feldlerche Alauda arvensis in der intensiv genutzten Agrarlandschaft. Ornithol Beob 85:309-371. <br>
9 Jenny M. 1990. Populationsdynamic der Feldlerche Alauda arvensis in einer intensiv genutzten Agrarlandschaft des Sweizerischen Mittellandes. Ornithol Beob 87:153-163.<br>
10 Willson JD, Evans  J, Browne SJ, King JR.1997. Territory distribution and breeding success of Skylarks Alauda arvensis on organic and intensive farmland in southern England. J Appl Ecol 34:1462-1478.<br>
11 Daunicht WD.1998. Zum ienfluss der Feinstruktur in der Vegatation auf die Habitatwahl, Habitatnutzung, Siedlungsdichte und populationsdynamik von Feldlerchen (<i>Alauda arvensis</i>) in grossparzelligem Ackerland. University of Bern, Germany. <br>
12 Odderskaer P, Prang A, Poulsen JG, Andersen PN, Elmegaard N.1997. Skylark (<i>Alauda arvensis</i>) utilization of micro-habitats in spring barley fields. Agr, Ecosyst Enviro 62:21-29.<br>
<br>
*/
