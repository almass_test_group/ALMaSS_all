/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_Covey.h This file contains the headers for the partridge covey class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of 15th October 2008 \n
 \n
 Doxygen formatted comments in October 2008 \n
 Code by Frank Nikolaisen & Chris Topping\n
*/
//---------------------------------------------------------------------------
#ifndef Partridge_CoveyH
#define Partridge_CoveyH
//---------------------------------------------------------------------------

extern class CoveyManager * g_covey_manager;
class Partridge_Base;
class Partridge_Male;
class Partridge_Female;

  /**  \brief Iterator class for wrap around landscape  */
class ForIterator {
  /**  \brief List of points  */
  int * m_list;
  /**  \brief How many points  */
  int m_limit;

public:
  /**  \brief Get pointer to list of points  */
  int * GetList( void ) {
    return m_list;
  }

  /**  \brief Get how many points  */
  int GetLimit( void ) {
    return m_limit;
  }

  /**  \brief Constructor  */
  ForIterator( int a_min_incl, int a_max_excl, int a_step_size, int a_norm_min_incl, int a_norm_max_excl );
  /**  \brief Destructor  */
  ~ForIterator( void );
};


/**  \brief The collective for a family of partridges  */
/**  The partridge covey is contains all behaviour that is performed as a group of birds. In fact this group can be as small as 1 bird, 
but if the behaviour is the same as a family group it will appear here.  \n
Note that the covey is treated just like a life-stage, and is handled by the Partridge_Population_Manager.\n
\n
 Each covey has a 'peg' position, which starts out as the
 center of a newly established territory.
 It also has a covey location, which may or may not coincide
 with the peg position. The covey can move some distance
 away from the peg, given by an 'elastic' function of distance
 between covey (physical) and the virtual territory center 'peg'.
 If a partridge would like to assess the quality of a hypothetial
 territory centeret at its current (covey) position, it will
 call Partridge_Covey::AssessHabitatQuality and make decisions on suitability depending on the value returned.
 The size of the potential territory evaluated is the minimum of
 the distance to the closest neighbour peg and a configurable
 maximum territory radius, Partridge_All.cpp::cfg_par_terr_max_width.\n
*/
class Partridge_Covey : public TAnimal {
protected:
// Attributes
	/** \brief costly intermediate variable */
	int m_flyto_steps;
	/** \brief costly intermediate variable */
	int m_flyto_dist;
	/** \brief costly intermediate variable */
	double m_flyto_ts;
	/** \brief flag for excess density - unused */
	bool m_essence_low;
	/** \brief flag for fixed homerange */
	bool m_peg_is_fixed;
	/**  \brief  Unused  */
	bool m_permanent_marker;
	/**  \brief  Unique ID no-  */
	unsigned int m_id;
	/**  \brief  List of covey members  */
	Partridge_Base * m_members[ 100 ];
	/**  \brief  Size of covey  */
	unsigned int m_members_size;
	/**  \brief  Signals Step is done for today  */
	bool m_step_done;
	/**  \brief  Signals distance updating is done for today  */
	bool m_dist_done;
	/**  \brief  Closest covey distance  */
	double m_nearest_covey_dist;
	/**  \brief  Signals homerange evaluation done  */
	bool m_terr_done;
	/**  \brief  Homerange evaluation score  */
	double m_terr_qual;
	/**  \brief  unused  */
	bool m_force_low;
	/** \brief The current behavioural state */
	Partridge_State m_state;
	/** \brief A pointer to the landscape */
	Landscape * m_map;
	/** \brief Landscape width */
	int m_width;
	/** \brief Landscape height */
	int m_height;
	/** \brief Covey's population manager */
	Partridge_Population_Manager * m_manager;
	/** \brief  Covey centre as float */
	double m_center_x_float;
	/** \brief  Covey centre as float */
	double m_center_y_float;
	/** \brief  Covey centre as float */
	double m_new_center_x_float;
	/** \brief  Covey centre as float */
	double m_new_center_y_float;
	/** \brief  Covey centre */
	int m_center_x;
	/** \brief  Covey centre */
	int m_center_y;
	/** \brief  Covey x-coord */
	int m_covey_x;
	/** \brief  Covey y-coord */
	int m_covey_y;
	/** \brief  Homerange width */
	int m_terr_size;
	/** \brief Nesting flag */
	bool m_nest_on_nest;
	/** \brief  Nest coord-x */
	int m_nest_x;
	/** \brief  Nest coord-y */
	int m_nest_y;
	/**	\brief Have assessed habitat quality? */
	bool m_assess_done;
	/**	\brief Habitat quality */
	double m_assess_qual;
	/**	\brief Have moved? */
	bool m_move_done;
	/**	\brief Distance moved */
	double m_dist_moved;
	/**	\brief Food collected */
	double m_food_today;

// Methods
	/** \brief  Update all covey members x,y coords */
	void CoveyUpdateMemberPositions( int a_x, int a_y );
	/** \brief  Evaluate x,y as a nest location */
	bool NestGoodSpot( int a_x, int a_y );
	/** \brief  Find nest location*/
	bool NestFindFast( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl );
	/** \brief  Wrap-around utility function */
	int Norm( int a_coord, int a_size );
	/** \brief  Wrap-around utility function */
	int NormInc( int a_coord, int a_size );
	/** \brief  Wrap-around utility function */
	int NormDec( int a_coord, int a_size );
	/** \brief  Test for proximity to unacceptable areas */
	bool NestNearBadAreas( int a_x, int a__y );
	/** \brief  Nest location evaluation function */
	bool NestBadArea( int a_x, int a__y );
	/** \brief  Nest location evaluation function */
	bool NestNearNests( int a_x, int a_y, int a_min_dist_sq );
	/** \brief  Nest location evaluation function */
	bool NestBadAreasScanSlow( int a_min_x, int a_min_y, int a_length, int a_x, int a_y );
	/** \brief  Nest location evaluation function */
	bool NestBadAreasScanFast( int a_min_x, int a_min_y, int a_length, int a_x, int a_y );
	/** \brief Move a_distance from current covey position */
	bool FlyToFast( int a_distance );
	/** \brief Move a_distance from current covey position (wraparound)*/
	bool FlyToSlow( int a_distance );
	/** \brief Get the pressure force */
	double Pressure( double a_distance );
	/** \brief Test for exceedence of pressure limit */
	bool PressureLimitExceeded( double a_pressure );
	/** \brief Updates distance to nearest covey */
	void DistanceUpdate( void );
	/** \brief Updates distance to fixed nearest covey */
	void DistanceUpdate2( void );
	/** \brief Get distance too covey */
	double DistanceToCovey( Partridge_Covey * a_covey );
	/** \brief  Entry point for habitat evaluation */
  double HabitatEvaluate( int a_center_x, int a_center_y, int * a_size_used, bool a_rethink_size );
	/** \brief Evaluate habitat fast */
  double HabitatEvaluateFaster( int a_center_x, int a_center_y );
	/** \brief Evaluate habitat (wrap around) */
  double HabitatEvaluateFast( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl );
	/** \brief  */
  double HabitatEvaluateSlow( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl );
	/** \brief  */
  double HabitatEvalPoly( TTypesOfLandscapeElement a_cet, int a_poly, Landscape * landscape );
  /** \brief Evaluate a field's habitat quality */
  int HabitatEvalPolyField( int a_field );

	/** \brief Uses the peg force to exclude possible movement directions */
	/**
	MoveTryExclude() should return a boolean distribution
	depending on the squared distance from the current location
	and the covey 'peg'. It is used when determining which directions
	we are allowed in, given the current location, and is called
	on for quadrants 'perpendicular' to the direction of the peg.
	Ie. moves toward the peg are always allowed, perpendicular is
	determined by this method, and those away by Partridge_Covey::MoveTryExcludeHarder().\n
	If both methods always return true for distances below a certain
	threshold, the covey is allowed to roam freely within a circle
	of sqrt(threshold).\n
	Note that in these methods a_dist_sq is treated linearly, meaning that 
	the 'force' against moves away from the peg will automatically
	be a second order polynomial.\n
	The implementation of these methods are currently quite rudimentary.
	One might want to incorporate such variables as the age of the chicks,
	size of the territory etc.\n
	*/
	bool MoveTryExclude( int a_dist_sq );
	/** \brief Uses the peg force to exclude possible movement directions */
	/**
	Called for quadrants facing away from the peg - see Partridge_Covey::MoveTryExclude*/
	bool MoveTryExcludeHarder( int a_dist_sq );

	/** \brief Test possible movement directions */
	/*
	Builds an array, m_move_allowed[8], from the current covey
	location and the MoveTryExclude*() methods, mapping
	from directions and into booleans. We calculate this array
	*before* evaluating the actual moves, because it would be pointless
	to make an expensive loop in MoveOptimalDirection*() below if
	we already know we will never use said value calculated. \n
	*/
	void MoveDirectionsAllowed( int a_x, int a_y );

	/** \brief Choose best foraging direction */
	/*
	Given the array of possible directions we might want to move in,
	calculate a rough evaluation [0:3] of the possible directions
	in the array m_move_list[8]. The actual food quality summed
	up during this evaluation is collected in m_move_dir_qual[8].
	The result of the values in m_move_list[] and m_move_dir_qual[]
	are assigned the values 0 and 0.0 respectively for those
	directions, which are blocked by the entries in m_move_allowed[]. \n
	*/
	void MoveOptimalDirectionSlow( int a_x, int a_y );
	/** \brief Choose best foraging direction (wrap around)*/
	/** See MoveOptimalDirectionSlow for details */
	void MoveOptimalDirectionFast( int a_x, int a_y );

	/** \brief Weighted foraging walk */
	/**
	Given our current location and m_move_list[], decide the direction to go next.\n
	Do this by:\n
	A) Remove directions from whence we came by calling Partridge_Coveh::MoveExcludedByPast()\n
	B) Build list of possible move directions at a given limit (3 == best moves etc.)\n
	C) Of those directions in the temporary select list, choose the previous direction we went to, or, if not present, a random direction.\n
	D) If no valid directions could be found even at limit 1, try backtracking down the patch from where we came.\n 
	E) Try to fuzzy move from the choosen path (if not blocked).\n
	*/
	int MoveWeighDirection( int a_x, int a_y );


public:
	/** \brief List of coveys close to this covey */
	Partridge_Covey* m_neighbourlist[50];
	/** \brief Number of coveys close to this covey */
	int m_neighbourlist_size;
	/** \brief  Find nest location (wrap around) */
	bool NestFindSlow( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl );
/*	double TerrEvalaPoly( TTypesOfLandscapeElement a_cet, int a_poly )  {
		return TerrEvalPoly( a_cet, a_poly );
	}
*/

protected:
	/** \brief Current homerange assessment radius */
	int m_assess_size;
/*
	// Optional reporting variables - look for m_move_get_enable in comments
	bool m_move_get_enable;
	vector < int > m_move_get_x;
	vector < int > m_move_get_y;
*/
	/** \brief Internal variable */
	int m_move_get_index;
	/** \brief Internal variable */
	int m_move_get_size;
	/** \brief Steps remembered */
	int m_move_qual_memory[ 4 ];
	/** \brief Internal variable */
	int m_move_qual_hist[ 4 ] [ MOVE_QUAL_HIST_SIZE ];
	/** \brief Make a move based on memory of quality */
	void MoveQualMemory( int a_qual );

  /** The direction we came from in the previous move. Remembered across daily steps. */
	/** \brief Internal variable */
  int m_move_whence_we_came;
	/** \brief Internal variable */
  int m_move_step_size;
	/** \brief Internal variable */
  double m_move_step_size_inv;

  /** \brief Test if we can go there */
  int MoveCanMove(int polyref);
  
  /** \brief We know where we want to go. Now do it! */
  double MoveDoIt( int * a_x, int * a_y, int a_dir, int a_step_size );
  
  /** \brief Move optimising food intake */
  double MoveMagicVegToFood( int a_polygon );
  /** \brief Move evaluation taking edges into account */
  int MoveEvalEdgesAndQual( int a_edges, double a_qual );
  /** \brief Fuzzdy movement selection */
  int MoveSelectFuzzy( void );

  /** \brief Part of movement evaluation */
  /**
  These methods:\n
    bool MoveSelectLimit( int a_limit, int a_x, int a_y );\n
  int MoveSelect( void );\n
  void MoveVegConstInit( void );\n
build a list of possible directions we can
  move to from our present location, given we are only interested
  in directions with a quality evaluation equal to a_limit. \n
  */
  bool MoveSelectLimit( int a_limit, int a_x, int a_y );
  /** \brief Part of movement evaluation */
  /**
  These methods:\n
    bool MoveSelectLimit( int a_limit, int a_x, int a_y );\n
  int MoveSelect( void );\n
  void MoveVegConstInit( void );\n
build a list of possible directions we can
  move to from our present location, given we are only interested
  in directions with a quality evaluation equal to a_limit. \n
  */
  int MoveSelect( void );
  /** \brief Part of movement evaluation */
  /**
  These methods:\n
    bool MoveSelectLimit( int a_limit, int a_x, int a_y );\n
  int MoveSelect( void );\n
  void MoveVegConstInit( void );\n
build a list of possible directions we can
  move to from our present location, given we are only interested
  in directions with a quality evaluation equal to a_limit. \n
  */
  void MoveVegConstInit( void );
  
  // Misc and tuning functions
	/** \brief Behaviour when alpha male is killed */
	void OntheDadDead();
	/** \brief Behaviour when alpha female is killed */
	void OntheMumDead();
	/** \brief Kill a proportion of the chicks */
	void KillChicks( int percent );

	/** \brief Pointer to the alpha male */
	Partridge_Female * m_theMum;
	/** \brief Pointer to the alpha female */
	Partridge_Male * m_theDad;
	/** \brief No chicks in covey */
  int m_ourChicks;
	/** \brief Date for covey dissolve */
  int m_CoveyDissolveDate;
	/** \brief Returns the age of the chicks */
  int SupplyChickAge( void );
  /** \brief Max move distance today*/
  /** 
  Determined at the end of the begin step and depending on the age of the chicks. If MoveDistance is called in the BeginStep then it will use yesterdays values.
  */
  int m_maxAllowedMove;
  /** \brief Food needed today*/
  /** 
  Determined at the end of the begin step and depending on the age of the chicks. If MoveDistance is called in the BeginStep then it will use yesterdays values.
  */
  double m_maxFoodNeedToday;
  /** \brief Age of chicks */
  int m_ChickAge;

public:
  /** \brief Test for covey emigration */
  bool CoveyEmigrate();
  /** \brief Test for individuals from covey emigrating. */
  void CoveyIndividualEmigrate();
  /** \brief  Test for another covey too close*/
  bool TooClose();
  /** \brief Check if covey contains chicks */
  bool HaveWeChicks( void );
  /** \brief Returns the number of uncles */
  int GetUncleNumber(); // {  return m_UncleNumber; }
  /** \brief Return the max possible move distance today */
  int GetmaxAllowedMove() {
    return m_maxAllowedMove;
  }

  /** \brief Return the maximum food need today */
  double GetmaxFoodNeedToday() {
    return m_maxFoodNeedToday;
  }

  /** \brief Add a chick */
  void OnAddChick( Partridge_Female * a_pf );
  /** \brief Remove a dead chick */
  void OnChickDeath();
  /** \brief Remove a matured chick */
  void OnChickMature();
  /** \brief Apply extra chick mortality */
  void ChickExtraMortality();

  /** \brief Set the dissolve date */
  void SetCoveyDissolveDate( int date ) {
    m_CoveyDissolveDate = date;
  }

  /** \brief Is their a suitable female mate in the covey? */
  Partridge_Female * FindMeAMateInCovey( int a_family_counter );
  /** \brief Finds the first unpaired female in the covey */
  Partridge_Female * GetUnpairedFemale();
  /** \brief Finds the first never paired female in the covey */
  Partridge_Female * GetUnpairedFemale_virgin();
  /** \brief Find a male in the covey */
  Partridge_Male * GetMaleInCovey();
  /** \brief Returns pointer to a_member */
  Partridge_Base * GetMember(int a_member);
  /** \brief  Returns a random member */
  Partridge_Base * GetAMember();
  /** \brief  Finds the closest other covey*/
  Partridge_Covey* FindNeighbour();
  /** \brief  Dissolve message handler*/
  void OnDissolve( int date );
  /** \brief Debug method */
  bool AllFlocking();
  /** \brief Debug method */
  bool AllFlocking2();
  /** \brief Debug method */
  bool ArePaired();
  /** \brief Debug method */
  void SanityCheck();
  /** \brief Debug method */
  void SanityCheck3();
  /** \brief Debug method */
  void SanityCheck4();
  /** \brief Debug method */
  void SanityCheck2( int no_chicks );

  /** \brief Decrement the chick number */
  void RemoveExtraChick() {
    m_ourChicks--;
  }

  /** \brief Return the number of chicks */
  int GetOurChicks() {
    return m_ourChicks;
  }

  /** \brief Return the covey size */
  unsigned int GetCoveySize() {
    return m_members_size;
  }

  /** \brief Set chick age */
  void SetChickAge( int age ) {
    m_ChickAge = age;
  }

  /** \brief Return chick age */
  int GetChickAge() {
    return m_ChickAge;
  }

  /** \brief Merging test */
  bool CanWeMerge(int a_NoUncles);
  /** \brief Merging test for uncle number */
  bool ManagerCheckMerge( int a_noOfUncles );
  /** \brief Swap uncle to alhpha */
  void SetUncle();
  /** \brief Kill extra chicks */
  void KillExcessChicks(int remaining);
  /**
  \brief
  Determine the chick mortality on parent death
  */
  void ActOnParentDeath();
  

  // End CJT section.

  /** \brief Try to locate a suitable nesting location */
  bool NestFindLocation( void );

  /** \brief Are we nesting? */
  bool NestOnNest( void ) {
    return m_nest_on_nest;
  }

  /** \brief Leave the nest  */
  void NestLeave( void ) {
    m_nest_on_nest = false;
  }

  /** \brief Return nest x-coord */
  int NestGetX( void ) {
    return m_nest_x;
  }

  /** \brief Return nest y-coord */
  int NestGetY( void ) {
    return m_nest_y;
  }

  /** \brief Return the food obtained today */
  double SupplyFoodToday( void );
  /** \brief Return the distance moved today */
  double SupplyDistanceMoved( void );


  // *********
  // CJT note: Using references in parameters to return
  // values is a *very bad* coding habit due to what is commonly
  // known as 'side effects', a really nasty class of 'invisible'
  // bugs. Using pointers to the same effect is OK, as
  // you can see in the call that something might be happening
  // to your parameter variable.
  /** \brief Assess habitat quality (with checks) */
  double AssessHabitatQuality( int & a_sz );

  // If we only care about elements or we have managed an update to the
  // TerrQualMap in PartridgePopulationManager, then we can use this much
  // faster method instead with a constant size of 50m (need to replace this
  // with a config variable - ***CJT***
  /** \brief Assess habitat quality (efficient) */
  double AssessHabitatQualityFaster( );

  /** \brief Get the size ('radius') used during territory evaluation. */
  int AssessGetSize( void ) {
    return m_assess_size;
  }

  /** \brief Accept this breeding location */
  /** 
  If the quality is found to be good enough, the animal can decide to use it for its own by calling this method.\n
  This updates the peg position to that of the animal, and updates
  internal state in accordance with it. After FixTerritory() any
  subsequent calls to Partridge_Covey::SupplyHabitatQuality() will give the quality on the basis of the same size but possibly drifted
  pushed peg location from the values used in the recent call to Partridge_Covey::AssessTerritoryQuality.\n
  */
  void FixHabitat( void );
  /** \brief Return habitat quality at our location */
  double SupplyHabitatQuality();


  /** \brief Prevent/enable peg drift */
  void SetFixed( bool a_is_fixed ) {
    m_peg_is_fixed = a_is_fixed;
  }

  /** \brief Wonder whether the peg is fixed? */
  bool IsFixed( void ) {
    return m_peg_is_fixed;
  }

  /** \brief Return object type */
  Partridge_Object GetObjectType( void ) {
    return pob_Covey;
  }


	// Remember to set locations in members through SetX(),
	// SetY() upon moving the covey, including FlyTo().
	/**
		\brief Entry point for movement 
	*/
	void MoveDistance( int a_max_distance, int a_step_size, double a_food_density_needed );
	
  /* 
  // Optional methods used for inspecting the covey's daily movements
  // due to MoveDistance(). Intended for demonstration/plotting/debugging
  // purposes. Can be safely ignored if not needed.

	// Enable/disable the movement collection engine in MoveDistance().
	  void MoveGetEnable( bool a_state );

	// Report number of steps taken today.
	int MoveGetSteps( void );

	// Prepare for daily loop of fetching movement data.
	void MoveGetPrepare( void );

	// Fetch the data in tight loop.
	bool MoveGet( int * a_x, int * a_y );

  // Calling MoveGetPrepare(), MoveGetSteps() or MoveGet() with the
  // movement collecting engine disabled is an error and will result
  // in a bug being reported, and the program execution will
  // be terminated.

  // // Example usage:
  //
  // l_covey->MoveGetEnable( true );
  //
  // ...
  //
  // l_covey->MoveDistance( 200, 10, 1234.0 );
  // printf( "Steps today: %d\n",
  //         l_covey->MoveGetSteps() );
  //
  // l_covey->MoveGetPrepare();
  //
  // int x, y;
  // while ( l_covey->MoveGet( &x, &y )) {
  //   printf( "X: %4d Y: %4d\n", x, y );
  // }

  */

	/**	\brief Fly to	*/
	/** 
		Fly to a random, new location a_distance away. Only allowed if the size of the covey is 1.
	*/
	bool FlyTo( int a_distance );

	/** \brief unused */
	void MoveTo( int a_x, int a_y );
	

	/**	\brief Find mate in covey */
	/** Find mate within the covey (if possible). Return NULL if not. */
	Partridge_Male * FindMeAHusband( Partridge_Female * a_female );
	/**	\brief Find mate in covey */
	/** Find mate within the covey (if possible). Return NULL if not. */
	Partridge_Female * FindMeAWife( Partridge_Male * a_male );
	/**	\brief Find uncle in covey */
	/** 
		Searches the covey for any bird with the UncleStatus set as true. Or returns NULL if none. 
	*/
	Partridge_Base * GetUncle();

	/**	\brief Find mate in area */
	/** 
		Loops through the males in the simulation, find first one in state pars_FAttractingMate and within distance a_max_distance, NULL if none.
	*/
	Partridge_Female * FindMateInArea( int a_max_distance );

	/**	\brief Covey BeginStep */
	void BeginStep( void );
	/**	\brief Covey Step */
	void Step( void );
	/**	\brief Covey EndStep */
	void EndStep( void );

	/**	\brief  */
  void AddMember( Partridge_Base * a_new_member );
	/**	\brief  */
  bool IsMember( Partridge_Base * a_possible_member );

	/**	\brief  Remove a member from the covey */
	/** 
	RemoveMember() causes the current Covey instance
	to be deleted! if a_former_member was the last one.\n
	*/
	int RemoveMember( Partridge_Base * a_former_member );

	/**	\brief Get covey size */
	unsigned int  Memberships( void ) {
		return m_members_size;
	}

  // Methods associated with the territory functionality of a covey.

	/**	\brief  */
  bool LifeEssenceLow( void ) {
    return m_essence_low;
  }

	/**	\brief  */
bool ForceLow( void ) {
	return m_force_low;
}

	/**	\brief  */
  unsigned int X( void ) {
    return m_covey_x;
  }

	/**	\brief  */
  unsigned int Y( void ) {
    return m_covey_y;
  }

	/**	\brief  */
  unsigned int XCenter( void ) {
    return m_center_x;
  }

	/**	\brief  */
  unsigned int YCenter( void ) {
    return m_center_y;
  }

	/**	\brief  */
  double XCenterF( void ) {
    return m_center_x_float;
  }

	/**	\brief  */
  double YCenterF( void ) {
    return m_center_y_float;
  }

	/**	\brief  */
  unsigned int ID( void ) {
    return m_id;
  }

  // The following methods exists solely for the benefit of the manager
  // class and its friends, eyes only. That includes the con/destructor
  // pair.
	/**	\brief  */
  void ManagerCheckMerge( void );
	/**	\brief  */
  void ManagerRethinkPos( void );
	/**	\brief  */
  void ManagerDriftPos( void );
	/**	\brief  */
  void ManagerUpdatePos( void );

  /**  \brief  Unused  */
  /** This peg is not associated with a family entity and can *never* be be moved, nor deleted. It is, well, permanent.\n
  Not used at present
  */
  void ManagerSetPermanent( void )
  {
    m_permanent_marker = true;
  }

  /**  \brief  Unused  */
  /** Is peg permanent\n
  Not used at present\n
  */
  bool ManagerIsPermanant( void ) {
    return m_permanent_marker;
  }

	/**	\brief Constructor */
  Partridge_Covey( Partridge_Base * a_first_member, Partridge_Population_Manager * a_manager, unsigned int a_center_x,
       unsigned int a_center_y, Landscape * a_map );
	/**	\brief  Destructor */
  virtual ~Partridge_Covey( void );

};



// For the benefit of the Covey_Manager.
static vector < Partridge_Covey * > g_covey_list;


/**
\brief The covey manager class
*/
/**
The covey manaer takes over the role of the Population_Manager for animal models. This is a design fault of historical origin and should be rectified one day with the 
Partridge_Population_Manager taking over this role.\n
*/
class CoveyManager {
	/**	\brief  Counter to keep track of covey ids*/
	int m_id_counter;
	/**	\brief  Landscape width */
	int m_world_width;
	/**	\brief  Landscape height */
	int m_world_height;
	/**	\brief  Half landscape width*/
	int m_world_width_div2;
	/**	\brief  Half landscape height*/
	int m_world_height_div2;
	/**	\brief Pointer to the landscape */
	Landscape * m_map;
	/**	\brief  Minimum interesting force */
	double m_par_force_ignore_below;
public:
	/**	\brief A debug function */
	void SanityCheck1();

	/**	\brief Add a new covey */
	void AddCovey( Partridge_Covey * a_covey );
	/**	\brief  Remove a covey */
	bool DelCovey( Partridge_Covey * a_covey );
	/**	\brief  Unused */
	int CoveyDensity(int x, int y, int radius);
	/**	\brief Calculate the geometric mean of no of chicks */
	double BroodGeoMean();
	/**	\brief Set the minimum interesting force */
	void Setpar_force_ignore_below(double ff);
	/**	\brief Get the minimum interesting force */
	double Getpar_force_ignore_below() {
		return m_par_force_ignore_below;
	}
	/**	\brief Do the covey management for the time-step */
	void Tick( void );

	/**	\brief  Constructor */
	CoveyManager( unsigned int a_world_width, unsigned int a_world_height, Landscape * a_map );
	/**	\brief Destructor */
	~CoveyManager( void );
};
#endif

