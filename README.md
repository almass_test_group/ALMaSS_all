This repository contains all the code for both the command line and GUI versions of ALMaSS

The CropBaseNumbers.xls file holds a reference list for all crop base numbers. Please ensure any new crops added appear in a sensible place in this list.