//---------------------------------------------------------------------------
#ifndef Roe_allH
#define Roe_allH

//---------------------------------------------------------------------------
#include <math.h>

#include <vector>
#include <algorithm>
#include <string>


//---------------------------------------------------------------------------

using namespace std;

#define __DEBUG_CJT1
#define __DEBUG_CJT2




//---------------------------------------------------------------------------

class Roe_Base;
class Roe_Male;
class Roe_Female;
class Roe_Fawn;
class Roe_Adult_Base;
class RoeDeer_Population_Manager;
class TTypesOfElement;
class Grid;
class GridCell;
class MovementMap;
class probe_data;

//---------------------------------------------------------------------------

typedef vector<Roe_Fawn *> Roe_FawnList;
typedef vector<Roe_Male *> Roe_MaleList;
typedef vector<Roe_Female *> Roe_FemaleList;
// the arrays
typedef vector< Roe_Male *>    ListOfMales;
typedef vector< Roe_Female *>  ListOfFemales;
typedef vector< Roe_Base *>    ListOfDeer;

class RoeDeerGroup
{
protected:
    ListOfDeer m_DeerList;
    int m_reference;
    Roe_Base* m_oldest;
public:
    RoeDeerGroup(int a_ref) {
        m_reference = a_ref;
        m_DeerList.clear();
        m_oldest = NULL;
    }
    int GetIndex() { return m_reference; }
    void AddDeer(Roe_Base* a_deer);
    bool RemoveDeer(Roe_Base* a_deer);
    int GetGroupSize() { return int(m_DeerList.size()); }
    void ResetOldest();
    APoint GetGroupCentre();
    void Update();
    void EndGroup();
};
//---------------------------------------------------------------------------

enum TTypesOfRoeDeer
{
    trd_Roe_Fawn = 0,
    trd_Roe_Male,
    trd_Roe_Female,
    trd_Roe_Foobar
};

enum TRoeDeerStates {
    rds_Initialise = 0,
    rds_FOnMature,
    rds_MOnMature,
    rds_FUpdateGestation,
    rds_FUpdateEnergy,
    rds_MUpdateEnergy,
    rds_FAUpdateEnergy,
    rds_FEstablishRange,
    rds_MEstablishRange,
    rds_MEstablishTerritory,
    rds_FDisperse,
    rds_MDisperse,
    rds_FOnNewDay,
    rds_MOnNewDay,
    rds_FAOnNewDay,
    rds_FFormGroup,
    rds_FInHeat,
    rds_FGiveBirth,
    rds_FCareForYoung,
    rds_FRun,
    rds_MRun,
    rds_FARunToMother,
    rds_FRecover,
    rds_MRecover,
    rds_FARecover,
    rds_FEvade,
    rds_MEvade,
    rds_FAHide,
    rds_MAttendFemale,
    rds_FRuminate,
    rds_MRuminate,
    rds_FARuminate,
    rds_FFeed,
    rds_MFeed,
    rds_FAFeed,
    rds_FASuckle,
    rds_FMate,
    rds_MMate,
    rds_FDie,
    rds_MDie,
    rds_FADie,
    rds_FDeathState,
    rds_MDeathState,
    rds_FADeathState,
    rds_FAInit,
    rds_FAMature,
    rds_NewBehaviour,
    rds_RUBBISH
};
//---------------------------------------------------------------------------

enum roeStatus
{
    rdstatus_territorial,
    rdstatus_satellite,
    rdstatus_peripheral
};

enum roeMortality
{
    rm_random = 0,
    rm_starve,
    rm_dispersal,
    rm_running,
    rm_seekcover,
    rm_feed,
    rm_road,
    rm_fight,
    rm_abandoned,
    rm_motherdead,
    rm_foobar
};

enum roeEnergyUse
{
    reu_Disperse = 0,
    reu_InHeatAttend,
    reu_GiveBirth, 
    reu_Care,
    reu_Run,
    reu_Recover, 
    reu_SeekCover,
    reu_Evade,
    reu_Rumi,
    reu_Feed, 
    reu_Mate, 
    reu_EstablRangeFight
    // //[0]=Run,[1]=Recov,[2]=Hide,[3]=Rumi,[4]=Feed,[5]=Suckle
};
class RoeMaleFights
{
public:
    Roe_Male* m_Opponent;
    bool m_Result;
    bool m_AsTerritoryOwner;
};

class RoeDeerThreatProbability
{
public:    // Generates a probability of threats for a roe deer per day and time of day across a landscape.
    RoeDeerThreatProbability(Landscape* a_land, int a_gridsize, double a_baseprobability);
    virtual ~RoeDeerThreatProbability() {
        ;
    }
    double GetThreatProbability(int a_x, int a_y) { 
        int index = (a_x / m_cellsize) + ((a_y / m_cellsize) * m_max_x);
        return m_ThreatProbability[index];
    }
protected:
    vector<double>m_ThreatProbability;
    /** \brief This just records the total number of cells */
    int m_cellnumber;
    /** \brief This just records the max x-range of cells */
    int m_max_x;
    /** \brief This just records the max y-range of cells */
    int m_max_y;
    /** \brief This is the cell size in m wide */
    int m_cellsize;


};
class RoeDeerMortalityRecording
{
    ofstream RoeMortFile;
    int runningmortcount[trd_Roe_Foobar][rm_foobar];
    string morts[rm_foobar] = { "random ", "starve ","dispersal ","rm_running ", "seekcover ","feed ", "road", "fight", "abandoned ", "motherdead" };
public:
    RoeDeerMortalityRecording()
    {
    	zeromortcount();
        RoeMortFile.open("RoeDeerMortalityRecord.txt");
        RoeMortFile << "Year" << '\t' << "Day" << '\t' << "LifeStage";
        for (int i = 0; i < (int)rm_foobar; i++) RoeMortFile << '\t' << morts[i];
        RoeMortFile << '\n';

    }
    ~RoeDeerMortalityRecording()
    {
        RoeMortFile.close();
    }
    void zeromortcount() {
        for (int r = trd_Roe_Fawn; r < trd_Roe_Foobar; r++) {
            for (int i = 0; i < (int)rm_foobar; i++) runningmortcount[r][i] = 0;
        }
    }
    void outputroemort(int a_year, int a_day) {
        for (int r = trd_Roe_Fawn; r < trd_Roe_Foobar; r++) {
            RoeMortFile << a_year << '\t' << a_day << '\t' << r ;
            for (int i = 0; i < (int)rm_foobar; i++) RoeMortFile << '\t' << runningmortcount[r][i];
            RoeMortFile << '\n';
        }
        RoeMortFile.flush();
        zeromortcount();
    }
    void AddToMort(TTypesOfRoeDeer a_class, roeMortality a_type) { runningmortcount[(int)a_class][(int)a_type]++; }
};
//---------------------------------------------------------------------------

class Deer_struct
{
public:
    int x;
    int y;
    bool sex;
    Landscape* L;
    RoeDeer_Population_Manager* Pop;
    Roe_Female* mum;
    int group;
    double size;
    int age;
    int ID;
};
//---------------------------------------------------------------------------

class Roe_Base : public TAnimal
{
public:
    TRoeDeerStates CurrentRState;
    RoeDeer_Population_Manager* m_OurPopulation;
    int m_Age; //in days
    double m_Size;   //body weight in kg
    APoint m_RangeCentre;
    int m_Reserves;
    APoint m_OldRange;    //stores roes own rangecentre while in group or dispersing
    int m_SearchRange;
    bool m_float;
    bool m_Disperse;
    bool m_HaveRange;
    Roe_Female* Mum;
    int m_ID; //unique ID number
    bool m_Sex;   //true=male
    bool m_IsDead;
    unsigned Alive;
    unsigned IsAlive() { return Alive; }
    double m_RMR;
    int SupplyReserves() { return m_Reserves; }
private:
protected:
    double m_HomeRange;
    int timestep;  //current timestep in this animals life

    int m_Cross_x;
    int m_Cross_y;
    double m_EnergyGained;    //counter that keeps track of energy gained from feeding
    int m_RecovCount;  //counts time spend in recov. Set=0 when state is left
    int m_RumiCount;   //counts time spend ruminating. Set=0 when state is left
    int m_FeedCount;  //counts time spend feeding since end of last ruminate period
    int m_LengthFeedPeriod;//calculated daily in UpdateEnergy, depends on date and body weight
    int m_LengthRuminate;
    /** \brief Flag for looking for cover */
    bool m_seekingcover;
    TRoeDeerStates m_LastState;  //holds the last state in before disturbance or NewDay
    int m_danger_x; // co-ordinates to a source of danger - set by an
    int m_danger_y; // ApproachOfDanger message
    //for obstacle avoidance during very directed movement:
    int m_distrc; //stores the last dist to rc
    int m_weightedstep; //this many steps with weight during last hour
    int SimW;
    int SimH;
    int m_MinInState[12]; //for energy calc., minutes spent in different states
    /** \brief Records the location of a favourite cover spot */
    APoint m_fav_cover;
    bool Running(int x, int y);
    bool RunSteps(int x, int y, int a_steps, int a_rightway);
    bool RunStepsCC(int x, int y, int a_steps, int a_rightway);

    int DistanceTo(int p_x, int p_y, int p_To_x, int p_To_y);
    int DistanceToCC(int p_x, int p_y, int p_To_x, int p_To_y)
    {
        m_OurLandscape->CorrectCoords(p_x, p_y);
        m_OurLandscape->CorrectCoords(p_To_x, p_To_y);
        return DistanceTo(p_x, p_y, p_To_x, p_To_y);
    }

    int DirectionTo(int p_x, int p_y, int p_To_x, int p_To_y);
    int DirectionToCC(int p_x, int p_y, int p_To_x, int p_To_y)
    {
        m_OurLandscape->CorrectCoords(p_x, p_y);
        return DirectionTo(p_x, p_y, p_To_x, p_To_y);
    }

    int NextStep(int weight, int dir, int recurselevel);
    int AssessHabitat(int polyref);
    int AssessHabitat(int p_x, int p_y);

    int LegalHabitat(int p_x, int p_y);
    /**
    LegalHabitatCC -calls LegalHabitat witth coordinates corrected for wrap-around
    */
    int LegalHabitatCC(int p_x, int p_y)
    {
        m_OurLandscape->CorrectCoords(p_x, p_y);
        return LegalHabitat(p_x, p_y);
    }


    /** \brief Calls the cover calculation for a given polyref */
    int Cover(int polyref);
    /** \brief Calls the cover calculation for a given x,y location */
    int Cover(int p_x, int p_y);
    /** \brief The cover calculation for a given element type */
    int CalcCover(TTypesOfLandscapeElement a_ele, int a_poly);
    /**
    corrects coordinates (for cover) for wrap around landscape
    */
    int CoverCC(int p_x, int p_y) // Corrects coordinates, inline. **FN**
    {
        m_OurLandscape->CorrectCoords(p_x, p_y);
        return Cover(p_x, p_y);
    }

    int NutriValue(int polyref);
    int NutriValue(int p_x, int p_y);
    int NutriValueCC(int p_x, int p_y)
    {
        m_OurLandscape->CorrectCoords(p_x, p_y);
        return NutriValue(p_x, p_y);
    }

    int Feeding(bool p_Disperse);
    void SeekCover(int threshold);
    void SeekNutri(int p_threshold);
    double ProbRoadCross(int p_x, int p_y, int p_width);
    double CalculateRoadMortality(int p_x, int p_y, int p_width);

    void WalkTo(int pos_x, int pos_y);
    void WalkToCC(int pos_x, int pos_y)
    {
        m_OurLandscape->CorrectCoords(pos_x, pos_y);
        WalkTo(pos_x, pos_y);
    }

public:
    virtual void On_MumAbandon(Roe_Female* /* mum */) {}
    virtual void On_IsDead(Roe_Base* /* base */, int /* whos_dead */, bool /* fawn */) {}
    virtual void On_EndGroup() {}
    virtual void Send_IsDead() {}
    virtual void On_ChangeGroup(int /* newgroup */) {}
    virtual void On_UpdateGroup(APoint /* p_group */) {}
    RoeDeerInfo SupplyInfo();
    int GetAge() { return m_Age; }
    virtual void BeginStep(void);
    virtual void Step(void);
    virtual void EndStep(void);
    virtual int WhatState() { return (int)CurrentRState; }
    Roe_Base(int x, int y, Landscape* L, RoeDeer_Population_Manager* RPM);
    virtual void ReInit(int x, int y, Landscape* L, RoeDeer_Population_Manager* RPM);
    virtual void Init(int x, int y, Landscape* L, RoeDeer_Population_Manager* RPM);
    virtual ~Roe_Base();
    double GetHomeRange() { return m_HomeRange; }
    int* GetEnergyUse() { return m_MinInState; }
};
//---------------------------------------------------------------------------

class Roe_Adult_Base : public Roe_Base
{
public:

protected:
TRoeDeerStates m_NewState;    //go to this state after OnNewDay
 int m_DispCount;
 int m_float_x;
 int m_float_y;
 vector<APoint> m_FixedLocationRecord;
 long m_totalarea;
 bool  m_skipstep;
 
public:
 virtual void BeginStep (void);
 virtual void Step      (void);
 virtual void EndStep   (void);
 void AddFix() { m_FixedLocationRecord.push_back(SupplyPoint()); }
 void ClearFixes() { m_FixedLocationRecord.clear(); }
 APoint GetFixNo(int a_i) { return m_FixedLocationRecord[a_i]; }
 int GetFixListSize() { return int(m_FixedLocationRecord.size()); }

 Roe_Adult_Base(int x, int y, double size, int age, Landscape* L, RoeDeer_Population_Manager* RPM);
 virtual void ReInit(int x, int y, double size, int age, Landscape* L, RoeDeer_Population_Manager* RPM);
 virtual ~Roe_Adult_Base();
 void CalculateArea();
 void Divide1(int p_a, int p_b);
 void Divide2(int p_a, int p_b, int p_c, bool p_upper, vector<APoint>* p_upperlist, vector<APoint>* p_lowerlist);
};
//---------------------------------------------------------------------------

class Roe_Fawn : public Roe_Base
{
private:
    //states
    TRoeDeerStates FAInit(void);
    TRoeDeerStates FASuckle(void);
    TRoeDeerStates FAFeed(void);
    TRoeDeerStates FARuminate(void);
    TRoeDeerStates FAHide(void);
    TRoeDeerStates FARecover(void);
    TRoeDeerStates FARunToMother(void);
    TRoeDeerStates FAOnNewDay(void);
    TRoeDeerStates FAUpdateEnergy(void);
    TRoeDeerStates FADie(void);

protected:
    Roe_Base* m_MySiblings[3];  //same age siblings
    int BedSiteList[15][2];
    int m_Agegroup;
    bool m_Mature;
    int m_MinHourlyCare;
    bool m_Orphan;
    bool m_InHide;
    int m_Bedsite_x;
    int m_BedSite_y;
    unsigned m_Count; //counts every timestep for the first 60 days of fawns life (8640 timesteps)
    int m_CareLastHour[6];
    //dynamic list that counts last 6 timesteps as '1' if
                           //fawn has received the required care and '0' if not.

    int m_memorypointer;  //no. of latest bedsite on bedsite list
    void CareCounter(int state); //counts hourly care
    void SelectBedSite();
    int RunTo(int mum_x, int mum_y, bool p_IsSafe);
    void Mature();
    void Send_InitCare();
    void On_UpdateGroup(APoint a_centre);
    void On_ApproachOfDanger(int p_To_x, int p_To_y);
public:
    int m_MyGroup;

public:
    virtual void On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn);
    virtual void On_ChangeGroup(int newgroup);
    virtual void On_MumAbandon(Roe_Female* mum);
    virtual void BeginStep(void);
    virtual void Step(void);
    virtual void EndStep(void);
    virtual void On_EndGroup();
    void On_AddSibling(Roe_Fawn* a_sib) {}
    Roe_Fawn(Deer_struct* p_data);
    virtual void ReInit(Deer_struct* p_data);
    virtual void Init(Deer_struct* p_data);
    virtual ~Roe_Fawn();
};
//---------------------------------------------------------------------------

class Roe_Female : public Roe_Adult_Base
{
public:
    vector<Roe_Base*> m_MyYoung;  //an array of pointers to all offspring
    int m_MyGroup;     //number of mygroup in grouplist
    int SupplyIsPregnant() { if (m_Pregnant) return 1; else return 0; }
    int SupplyYoung() { return m_NoOfYoung; }

    // STATES
protected:
    TRoeDeerStates FOnMature(void);
    TRoeDeerStates FDie(void);
    TRoeDeerStates FDisperse(void);
    TRoeDeerStates FEstablishRange(void);
    TRoeDeerStates FOnNewDay(void);
    TRoeDeerStates FUpdateGestation(void);
    TRoeDeerStates FGiveBirth(void);
    TRoeDeerStates FFormGroup(void);
    TRoeDeerStates FInHeat(void);
    TRoeDeerStates FFeed(void);
    TRoeDeerStates FRun();
    TRoeDeerStates FRecover(void);
    TRoeDeerStates FEvade();
    TRoeDeerStates FRuminate(void);
    TRoeDeerStates FCareForYoung(void);
    TRoeDeerStates FMate(void);
    TRoeDeerStates FUpdateEnergy(void);
    TRoeDeerStates NewBehaviour(void);
    void InHeat(void);
    bool m_Care;
    int m_Gestationdays;
    bool m_Pregnant;
    Roe_Male* m_My_Mate;
    int m_NoOfYoung; //this years fawns
    int m_DaysSinceParturition;
    bool m_WantGroup;
    bool m_NatalGroup;
    int m_OptGroupSize;
    bool m_GroupUpdated;
    int m_HeatCount;
    bool m_WasInHeat;

    //FUNCTIONS
    int  On_CanJoinGroup(Roe_Female* Female);
    void On_UpdateGroup(APoint a_centre);
    void On_ApproachOfDanger(int p_To_x, int p_To_y);
    virtual void On_EndGroup();

public:
    void         Init(void);
    virtual void BeginStep(void);
    virtual void Step(void);
    virtual void EndStep(void);
    virtual void On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn);
    bool On_InitCare(Roe_Fawn* Fawn);
    void On_EndCare(Roe_Fawn* Fawn);
    int SupplyGroupNo() { return m_MyGroup; }
    void AddYoungToList(Roe_Base* fawn);

    Roe_Female(Deer_struct* p_data);
    virtual void ReInit(Deer_struct* p_data);
    virtual void Init(Deer_struct* p_data);
    virtual ~Roe_Female();
};
//---------------------------------------------------------------------------

class Roe_Male : public Roe_Adult_Base
{
private: //male states

protected:
 Roe_Female* m_My_Mate;   //points to the female he is attending

 roeStatus m_Status;
 int m_NoOfMatings;
 vector<RoeMaleFights> m_MyFightList;
 TRoeDeerStates MOnMature(void);
 TRoeDeerStates MDie(void);
  TRoeDeerStates MDisperse(void);
  TRoeDeerStates MEstablishRange(void);
  TRoeDeerStates MEstablishTerritory(void);
  TRoeDeerStates MAttendFemale(void);
  TRoeDeerStates MFeed(void);
  TRoeDeerStates MRun();
  TRoeDeerStates MRecover(void);
  TRoeDeerStates MEvade();
  TRoeDeerStates MRuminate(void);
  TRoeDeerStates MMate(void);
  void MFight(void);
  void MUpdateEnergy(void);
  void MOnNewDay(void);
  /** \brief checks for a previous match with this opponent */
  bool FightResult(Roe_Male* a_male, RoeMaleFights& a_fightresult);
  /** \brief Test for mortality resulting from a fight */
  bool FightMortality(bool a_win_loss);


public:
 void On_Expelled(Roe_Male* rival);
 void On_ApproachOfDanger(int p_To_x,int p_To_y);
 bool On_InHeat (Roe_Female* female,int p_x,int p_y);
 /** \brief Empties the fight list */
 void ClearFightlist() {  m_MyFightList.clear(); }
 roeStatus GetStatus() { return m_Status; }
 bool On_Rank(Roe_Male* rival,double size,int age,int matings,bool Where);
 virtual void On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn);
 void         Init      (void);
 virtual void BeginStep (void);
 virtual void Step      (void);
 virtual void EndStep   (void);

 Roe_Male(Deer_struct* p_data);
 virtual void ReInit(Deer_struct* p_data);
 virtual void Init(Deer_struct* p_data);
 virtual ~Roe_Male();

};
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class TRoeGroup
{
   public:
   Roe_Base* GroupList[30];
   int Size;
   unsigned group_x;
   unsigned group_y;
};

//----------------------------------------------------------------------------

class Roe_Grid
{
   public:
   Roe_Grid(Landscape* L);
   ~Roe_Grid();
   bool AddGridValue(int x,int y, int value);
   int Supply_GridValue(int g_x,int g_y);
   int Supply_GridCoord(int p_coor);
   protected:
   Landscape* m_LS;
   int m_SizeOfCells;
   int m_extent_x;
   int m_extent_y;
   int m_maxcells;
   int* m_grid;  //List of all cells in grid, min. cell size = 20 m
};
//----------------------------------------------------------------------------

class RoeDeer_Population_Manager : public Population_Manager
{
    // Attributes. 
 protected:
    Roe_Grid* m_TheGrid;
    vector<RoeDeerGroup> m_GroupList;//list of all groups in area
    double m_AverageRangeQuality;
    int Turnover;
	/** \brief The number of cells wide in the qual grid */
	int m_gridextentx;
	/** \brief The number of cells tall in the qual grid */
	int m_gridextenty;
    /** \brief The number fawns born this year */
    int m_BornThisYear;
    /** \brief The number of fauns that matured this year */
    int m_MaturedThisYear;
    /** \brief The number of fauns that die this year */
    int m_DiedYoungThisYear;
    /** \brief The number of fauns that die this year */
    int m_DiedAdultsThisYear;
    /** \brief EventThreat probability calculator per location */
    RoeDeerThreatProbability* m_ThreatEventsCalc;
    /** \brief flag for whether its daytime or not */
    bool m_isdaylight;
    /** \brief Incrementally altered number to generate group reference numbers */
    unsigned m_lastgrouprefnum;
public:
	RoeDeerMortalityRecording g_RoeMortRec;
	 /**   \brief Map of suitability for movement */
   MovementMap* m_MoveMap; //**ljk inserted on 25/7-2012
   vector<int> m_DeadListMales; //animals that died within this year.
   vector<int> m_DeadListFemales; //animals that died within this year.
   long StepCounter;  //timestep in this simulation
   int m_AverageMaleSize;
   int m_FemaleDispThreshold;
   int m_MinWeight_Males;
   int m_MinWeight_Females;
   int m_CriticalWeight_Males;
   int m_CriticalWeight_Females;
   double Supply_AverageRangeQuality() {return m_AverageRangeQuality;}
   double RangeQuality(int p_x,int p_y,int p_range);
   double PercentForest(APoint a_Location,int p_range);
   int GetGroupIndex(int a_group) {
       for (int nth = 0; nth< m_GroupList.size(); ++nth)
       {
           if (m_GroupList[nth].GetIndex() == a_group) return nth;
       }
       // Not found raise an error
       g_msg->Warn("Roe deer GetGroupIndex index not found", a_group);
       exit(0);
   }
   int CreateNewGroup(Roe_Base* p_deer);
   APoint GetGroupCentre(int a_TheGroup);
   void UpdateGroup(int a_TheGroup);
   int Supply_GroupSize(int a_group) { return m_GroupList[GetGroupIndex(a_group)].GetGroupSize(); }
   void AddToGroup(Roe_Base* p_deer,int TheGroup);
   void RemoveFromGroup(Roe_Base* p_deer, int TheGroup);
   void DissolveGroup(int TheGroup);
   void GroupUpdates();
   bool InSquare(int p_x, int p_y,int p_sqx,int p_sqy, int p_range);
   //void CalculateArea(int p_a, int p_b,int ID);
   int ScanGrid(int p_x, int p_y, bool avq);
   int AddToArea(int g_x,int g_y, int p_range);
   int AddToAreaCC(int g_x,int g_y, int p_range);
   //void Sort(int ID, int p_a, int p_b);
   //void Divide1(int ID, int p_a, int p_b);
   //void Divide2(int p_a, int p_b, int p_c,bool p_upper);
   int SimW;
   int SimH;
   //void DeleteFightlist(int TheList);
   void PreProcessLandscape();
   void UpdateRanges();  
	// Other interface functions  //ljk added 22/8-2012
    virtual void TheAOROutputProbe();
    virtual void TheRipleysOutputProbe(FILE* a_prb);	
    void AddToDiedAdultsThisYear() {
        m_DiedAdultsThisYear ++;
    }
    void AddToDiedYoungThisYear() {
        m_DiedYoungThisYear++;
    }
    bool GetThreat(int a_x, int a_y) {
        if (g_rand_uni() < m_ThreatEventsCalc->GetThreatProbability(a_x, a_y)) return true;
        return false;
    }
    bool GetIsDayLight() { return m_isdaylight; }
 protected:
    // Methods
     virtual void DoFirst();
     virtual void DoLast() {
         int today = m_TheLandscape->SupplyDayInYear();
         int year = m_TheLandscape->SupplyYearNumber();
         if (StepCounter % 144 == 0)  WriteToEnergyBudgetFile(today, year);
         if (StepCounter % 144 == 143)  GroupUpdates(); // Once per day we need to ensure that all deer in groups are syncronised
     }

 public:
    RoeDeer_Population_Manager(Landscape* L);
    ~RoeDeer_Population_Manager();
   virtual void Init (void);
   void CreateObjects(int, TAnimal *pvo,void* null ,
                                         Deer_struct * data,int number);
   ListOfMales* SupplyMales(int p_x,int p_y,int SearchRange);
   ListOfMales* SupplyTMales(int p_x,int p_y,int SearchRange);
   ListOfMales* SupplyMaleRC(int p_x,int p_y,int SearchRange);
   ListOfFemales* SupplyFemales(int p_x,int p_y,int SearchRange);
   ListOfFemales* SupplyFemaleRC(APoint a_Loc,int SearchRange);
   bool SupplyIsFemale(int p_x, int p_y, int SearchRange);
   void WriteToHRFile(std::string name, int month, int year);
   //void WriteRandHR(std::string name, int total);
   void WriteToFixFile(Roe_Adult_Base*deer);
   void WriteToEnergyBudgetFile(int a_day, int a_year);
   //void WriteFloatFile(std::string name, int male_f, int female_f);
   void WriteIDFile(std::string name, int p_ID, bool p_sex);
   void WriteDeadFile(std::string name, int p_year);
   void WriteAgeStructFile(std::string name, int year);
   //void WriteReproFile(std::string name, int id, int young);    //for Lene
   void WriteSizeFile(std::string name, int year);


};

#endif
