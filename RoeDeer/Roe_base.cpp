//
// roe_base.cpp
//

#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"

#include "../BatchALMaSS/PopulationManager.h"

#include "Roe_all.h"
#include "Roe_constants.h"

extern int FastModulus( int a_val, int a_modulus );


void Roe_Base::BeginStep (void)
{
 if (m_CurrentStateNo==-1) return;
}



//---------------------------------------------------------------------------
void Roe_Base::Step (void)
{
 if (m_StepDone || (m_CurrentStateNo==-1)) return;

}
//---------------------------------------------------------------------------
void Roe_Base::EndStep (void)
{
  if (m_StepDone || (m_CurrentStateNo==-1)) return;
}

//---------------------------------------------------------------------------
/**
Roe_Base::SeekCover - Method for locating good spot based on cover. Used to find place 
to ruminate and fawn bedsites. Threshold is the minimum acceptable cover value
Calls CoverCC(), WalkToCC(), Cover(), WalkTo().
*/
void Roe_Base::SeekCover(int threshold)
{
    //Use remembered good cover if possible
    //locates good spot based on cover. Used to find place to ruminate and fawn
    //bedsites. Threshold is the minimum acceptable cover value

    // Check for close enough rememberd place
    if (m_fav_cover.m_x != -1)
    {
        if (m_OurPopulation->InSquare(m_fav_cover.m_x, m_fav_cover.m_y, m_RangeCentre.m_x, m_RangeCentre.m_y, MinRange))
        {
            if (Cover(m_fav_cover.m_x, m_fav_cover.m_y) > threshold)
            {
                // We assume this is a minor movement and done safely
                m_Location_x = m_fav_cover.m_x;
                m_Location_y = m_fav_cover.m_y;
                m_seekingcover = false;
                return;
            }
            // Rare but cover is gone, so forget the fav_cover
            m_fav_cover.m_x = -1;
        }
    }
    // Nowhere remembered so look around
    m_seekingcover = true;
    int t[8];
    int q[8];
    int dir = random(8);   //start looking in any direction
    bool found = false;
    bool correct = false;
    t[0] = dir & 7;
    t[1] = (dir + 1) & 7;
    t[2] = (dir + 7) & 7;
    t[3] = (dir + 10) & 7;
    t[4] = (dir + 6) & 7;
    t[5] = (dir + 11) & 7;
    t[6] = (dir + 5) & 7;
    t[7] = (dir + 12) & 7;
    int x = m_Location_x;
    int y = m_Location_y;

    //need to correct coordinates
    if ((x <= 100) || (x >= SimW - 100) || (y <= 100) || (y >= SimH - 100))//could step out of area
    {
        correct = true;
        x += SimW;
        y += SimH;
        for (int i = 0; i < 100; i++)       //up to 100 steps at a time
        {
            q[0] = CoverCC(x + (Vector_x[t[0]] * i), y + (Vector_y[t[0]] * i));
            q[1] = CoverCC(x + (Vector_x[t[1]] * i), y + (Vector_y[t[1]] * i));
            q[2] = CoverCC(x + (Vector_x[t[2]] * i), y + (Vector_y[t[2]] * i));
            q[3] = CoverCC(x + (Vector_x[t[3]] * i), y + (Vector_y[t[3]] * i));
            q[4] = CoverCC(x + (Vector_x[t[4]] * i), y + (Vector_y[t[4]] * i));
            q[5] = CoverCC(x + (Vector_x[t[5]] * i), y + (Vector_y[t[5]] * i));
            q[6] = CoverCC(x + (Vector_x[t[6]] * i), y + (Vector_y[t[6]] * i));
            q[7] = CoverCC(x + (Vector_x[t[7]] * i), y + (Vector_y[t[7]] * i));
            if ((q[0] >= threshold) || (q[1] >= threshold) || (q[2] >= threshold) ||
                (q[3] >= threshold) || (q[4] >= threshold) || (q[5] >= threshold) ||
                (q[6] >= threshold) || (q[7] >= threshold))
            {
                //which direction is it?
                int thisway;
                int score = -1;
                int index[8];
                for (int j = 0; j < 8; j++)
                {
                    if (q[j] >= threshold)
                    {
                        score++;  //this many suitable
                        index[score] = j;
                    }
                }
                int rand = random(score + 1);
                thisway = t[index[rand]];

                //walk to that place
                WalkToCC(x + (Vector_x[thisway] * i), y + (Vector_y[thisway] * i));
                found = true;
                break;   //don't look any further
            }
        }
    }
    else //no need to correct area
    {
        for (int i = 2; i < 100; i += 2)       //up to 100 steps at a time
        {
            q[0] = Cover(x + (Vector_x[t[0]] * i), y + (Vector_y[t[0]] * i));
            q[1] = Cover(x + (Vector_x[t[1]] * i), y + (Vector_y[t[1]] * i));
            q[2] = Cover(x + (Vector_x[t[2]] * i), y + (Vector_y[t[2]] * i));
            q[3] = Cover(x + (Vector_x[t[3]] * i), y + (Vector_y[t[3]] * i));
            q[4] = Cover(x + (Vector_x[t[4]] * i), y + (Vector_y[t[4]] * i));
            q[5] = Cover(x + (Vector_x[t[5]] * i), y + (Vector_y[t[5]] * i));
            q[6] = Cover(x + (Vector_x[t[6]] * i), y + (Vector_y[t[6]] * i));
            q[7] = Cover(x + (Vector_x[t[7]] * i), y + (Vector_y[t[7]] * i));
            if ((q[0] >= threshold) || (q[1] >= threshold) || (q[2] >= threshold) ||
                (q[3] >= threshold) || (q[4] >= threshold) || (q[5] >= threshold) ||
                (q[6] >= threshold) || (q[7] >= threshold))
            {
                //which direction is it?
                int thisway;
                int score = -1;
                int index[8];
                for (int j = 0; j < 8; j++)
                {
                    if (q[j] >= threshold)
                    {
                        score++;  //this many suitable
                        index[score] = j;
                    }
                }
                int rand = random(score + 1);
                thisway = t[index[rand]];
                //walk to that place
                WalkTo(x + (Vector_x[thisway] * i), y + (Vector_y[thisway] * i));
                found = true;
                break;   //don't look any further
            }
        }
    }
    if ((!found) && (!correct))//always need to correct now for this last bit
    {
        x += SimW;
        y += SimH;
    }
    int i = 100;
    while (!found)
    {
        q[0] = CoverCC(x + (Vector_x[t[0]] * i), y + (Vector_y[t[0]] * i));
        q[1] = CoverCC(x + (Vector_x[t[1]] * i), y + (Vector_y[t[1]] * i));
        q[2] = CoverCC(x + (Vector_x[t[2]] * i), y + (Vector_y[t[2]] * i));
        q[3] = CoverCC(x + (Vector_x[t[3]] * i), y + (Vector_y[t[3]] * i));
        q[4] = CoverCC(x + (Vector_x[t[4]] * i), y + (Vector_y[t[4]] * i));
        q[5] = CoverCC(x + (Vector_x[t[5]] * i), y + (Vector_y[t[5]] * i));
        q[6] = CoverCC(x + (Vector_x[t[6]] * i), y + (Vector_y[t[6]] * i));
        q[7] = CoverCC(x + (Vector_x[t[7]] * i), y + (Vector_y[t[7]] * i));
        if ((q[0] >= threshold) || (q[1] >= threshold) || (q[2] >= threshold) ||
            (q[3] >= threshold) || (q[4] >= threshold) || (q[5] >= threshold) ||
            (q[6] >= threshold) || (q[7] >= threshold))
        {
            //which direction is it?
            int thisway;
            int score = -1;
            int index[8];
            for (int j = 0; j < 8; j++)
            {
                if (q[j] >= threshold)
                {
                    score++;  //this many suitable
                    index[score] = j;
                }
            }
            int rand = random(score + 1);
            thisway = t[index[rand]];

            //walk to that place
            WalkToCC(x + (Vector_x[thisway] * i), y + (Vector_y[thisway] * i));
            found = true;
            break;   //don't look any further
        }
        i += 2;
        if (i > SimH)
        {
            m_OurLandscape->Warn("Roe_Base:SeekCover():Variable out of range, i", "");
            exit(1);
        }
    }
    if (Cover(m_Location_x, m_Location_y) > threshold)
    {
        m_fav_cover.m_x = m_Location_x;
        m_fav_cover.m_y = m_Location_y;
        m_seekingcover = false;
        return;
    }
    m_seekingcover = true;
}
//--------------------------------------------------------------------------

/**
Roe_Base::SeekNutri - locates good spot to feed based on Digestable energy content (DEC). 
Searches outwards in all directions for better quality. Equals 10 minutes of search activity.
Calls NutriValueCC(), WalkToCC(), WalkTo(), NutriValue().
*/
void Roe_Base::SeekNutri(int threshold)
//locates good spot to feed. Searches outwards in all directions for better quality
//equals 10 minutes of search activity
{
  int dir=random(8);   //start looking in any direction
  int t[8];
  int q[8];
  t[0]=dir & 7;
  t[1]=(dir+1) & 7;
  t[2]=(dir+7) & 7;
  t[3]=(dir+10) & 7;
  t[4]=(dir+6) & 7;
  t[5]=(dir+11) & 7;
  t[6]=(dir+5) & 7;
  t[7]=(dir+12) & 7;
  //need to correct coordinates
  int x=m_Location_x;
  int y=m_Location_y;
  bool found=false;
  bool correct=false;

  if((x <= 100)||(x >= SimW-100)||(y <= 100)||(y >= SimH-100))//could step out of area
  {
    correct=true;
    x += SimW;
    y += SimH;
    for (int i=2; i< 100; i+=2)       //check up till 100 steps away
    {
      q[0]=NutriValueCC( x+(Vector_x[t[0]]*i), y+(Vector_y[t[0]]*i) );
      q[1]=NutriValueCC( x+(Vector_x[t[1]]*i), y+(Vector_y[t[1]]*i) );
      q[2]=NutriValueCC( x+(Vector_x[t[2]]*i), y+(Vector_y[t[2]]*i) );
      q[3]=NutriValueCC( x+(Vector_x[t[3]]*i), y+(Vector_y[t[3]]*i) );
      q[4]=NutriValueCC( x+(Vector_x[t[4]]*i), y+(Vector_y[t[4]]*i) );
      q[5]=NutriValueCC( x+(Vector_x[t[5]]*i), y+(Vector_y[t[5]]*i) );
      q[6]=NutriValueCC( x+(Vector_x[t[6]]*i), y+(Vector_y[t[6]]*i) );
      q[7]=NutriValueCC( x+(Vector_x[t[7]]*i), y+(Vector_y[t[7]]*i) );
      if ((q[0]>=threshold)||(q[1]>=threshold)||(q[2]>=threshold)||
            (q[3]>=threshold)||(q[4]>=threshold)||(q[5]>=threshold)||
               (q[6]>=threshold)||(q[7]>=threshold))
      {
        //which direction is it?
        int thisway;
        int score=-1;
        int index[8];
        int best = q[0];
        for (int j=0; j<8; j++)
        {
          if (q[j]>best)  //only 1 best choice
          {
            best = q[j];
            score = 0;
            index[score]=j;
          }
          else if (q[j]==best)
          {
            score++;  //this many suitable
            index[score]=j;
          }
        }
        if (score==0)
        {
          thisway = t[index[score]];
        }
        else if (score >0)
        {
          int rand = random(score+1);
          thisway=t[index[rand]];
        }
        //walk to that place
        WalkToCC( x+(Vector_x[thisway]*i), y+(Vector_y[thisway]*i) );
        found=true;
        break;   //don't look any further
      }
    }
  }
  else //no need to correct area
  {
    for (int i=2; i< 100; i+=2)       //up till 200 steps at a time
    {
      q[0]=NutriValue(x+(Vector_x[t[0]]*i),y+(Vector_y[t[0]]*i));
      q[1]=NutriValue(x+(Vector_x[t[1]]*i),y+(Vector_y[t[1]]*i));
      q[2]=NutriValue(x+(Vector_x[t[2]]*i),y+(Vector_y[t[2]]*i));
      q[3]=NutriValue(x+(Vector_x[t[3]]*i),y+(Vector_y[t[3]]*i));
      q[4]=NutriValue(x+(Vector_x[t[4]]*i),y+(Vector_y[t[4]]*i));
      q[5]=NutriValue(x+(Vector_x[t[5]]*i),y+(Vector_y[t[5]]*i));
      q[6]=NutriValue(x+(Vector_x[t[6]]*i),y+(Vector_y[t[6]]*i));
      q[7]=NutriValue(x+(Vector_x[t[7]]*i),y+(Vector_y[t[7]]*i));
      if ((q[0]>=threshold)||(q[1]>=threshold)||(q[2]>=threshold)||
            (q[3]>=threshold)||(q[4]>=threshold)||(q[5]>=threshold)||
               (q[6]>=threshold)||(q[7]>=threshold))
      {
        //which direction is it?
        int thisway;
        int score=-1;
        int index[8];
        int best = q[0];
        for (int j=0; j<8; j++)
        {
          if (q[j]>best)  //only 1 best choice
          {
            best = q[j];
            score = 0;
            index[score]=j;
          }
          else if (q[j]==best)
          {
            score++;  //this many suitable
            index[score]=j;
          }
        }
        if (score==0)
        {
          thisway = t[index[score]];
        }
        else if (score >0)
        {
          int rand = random(score+1);
          thisway=t[index[rand]];
        }
        //walk to that place
        WalkTo( x+(Vector_x[thisway]*i), y+(Vector_y[thisway]*i) );
        found=true;
        break;   //don't look any further
      }
    }
  }
  if((!found)&&(!correct))//always need to correct now for this last bit
  {
    x+=SimW;
    y+=SimH;
  }
  int i=100;
  while(!found)
  {
      q[0]=NutriValueCC( x+(Vector_x[t[0]]*i), y+(Vector_y[t[0]]*i) );
      q[1]=NutriValueCC( x+(Vector_x[t[1]]*i), y+(Vector_y[t[1]]*i) );
      q[2]=NutriValueCC( x+(Vector_x[t[2]]*i), y+(Vector_y[t[2]]*i) );
      q[3]=NutriValueCC( x+(Vector_x[t[3]]*i), y+(Vector_y[t[3]]*i) );
      q[4]=NutriValueCC( x+(Vector_x[t[4]]*i), y+(Vector_y[t[4]]*i) );
      q[5]=NutriValueCC( x+(Vector_x[t[5]]*i), y+(Vector_y[t[5]]*i) );
      q[6]=NutriValueCC( x+(Vector_x[t[6]]*i), y+(Vector_y[t[6]]*i) );
      q[7]=NutriValueCC( x+(Vector_x[t[7]]*i), y+(Vector_y[t[7]]*i) );
      if ((q[0]>=threshold)||(q[1]>=threshold)||(q[2]>=threshold)||
            (q[3]>=threshold)||(q[4]>=threshold)||(q[5]>=threshold)||
               (q[6]>=threshold)||(q[7]>=threshold))
      {
        //which direction is it?
        int thisway;
        int score=-1;
        int index[8];
        for (int j=0; j<8; j++)
        {
          if (q[j]>=threshold)
          {
            score++;  //this many suitable
            index[score]=j;
          }
        }
        int rand = random(score+1);
        thisway=t[index[rand]];
        //walk to that place
        WalkToCC( x+(Vector_x[thisway]*i), y+(Vector_y[thisway]*i) );
        found=true;
        break;   //don't look any further
      }
      i+=2;
      if(i>SimH)
      {
        m_OurLandscape ->Warn("Roe_Base:SeekCover():Variable out of range, i","");
        exit( 1 );
      }
  }

}
//---------------------------------------------------------------------------
/**
Roe_Base::Running - straight line movement away from danger. Avoid water, buildings and gardens,
but not roads. The Direction with highest total cover value is chosen.
After 150 steps, the deer checks if this habitat is safe, and if cover is not good, the deer will keep 
running and check if safe every 25 m.
Calls DirectionTo(), Cover(), SupplyRoadWidth(), CalculateRoadMortality().
 
*/
bool Roe_Base::Running(int x, int y)   //running from danger
{
    //straight line movement away from danger. Avoid water, buildings and gardens,
    //but not roads.

    int t[8];
    int q[8];
    q[5] = 0;
    q[6] = 0;
    q[7] = 0;
    bool IsSafe = false;
    //get direction to threat
    int dir = DirectionTo(m_Location_x, m_Location_y, x, y);
    t[5] = dir + 3;
    t[5] = (t[5] + 8) & 7;
    t[6] = dir - 3;
    t[6] = (t[6] + 8) & 7;
    t[7] = dir + 4;
    t[7] = (t[7] + 8) & 7;
    bool nearedge;
    if ((m_Location_x < 200) || (m_Location_y < 200) || (m_Location_x >= SimW - 200) || (m_Location_y >= SimH - 200)) nearedge = true; else nearedge = false;
    //get cover in 3 directions from 0-50 m.
    if (!nearedge)
    {
        for (int i = 0; i < 50; i++)
        {
            q[5] += Cover((m_Location_x + Vector_x[t[5]]), (m_Location_y + Vector_y[t[5]])); //sums cover for each dir
            q[6] += Cover((m_Location_x + Vector_x[t[6]]), (m_Location_y + Vector_y[t[5]]));
            q[7] += Cover((m_Location_x + Vector_x[t[7]]), (m_Location_y + Vector_y[t[5]]));
        }
    }
    else
        // Need correct coords
        for (int i = 0; i < 50; i++)
        {
            for (int j = 5; j < 8; j++)
            {
                APoint pt = m_OurLandscape->CorrectCoordsPt(m_Location_x + Vector_x[t[j]], m_Location_y + Vector_y[t[j]]);
                q[j] += Cover(pt.m_x, pt.m_y); //sums cover for each dir
            }
        }
    //get direction with highest total cover value = int thisway
    int max = q[5];
    int thisway;

    if (q[6] > max) thisway = t[6];
    else if (q[7] > max) thisway = t[7];
    else thisway = t[5];

    if (!nearedge)
    {
        if (RunSteps(m_Location_x, m_Location_y, 200, thisway)) return true; // true means must be dead, so stop
    }
    else
    {
        // Need correct coordinates
        if (RunStepsCC(m_Location_x, m_Location_y, 200, thisway)) return true; // true means must be dead, so stop
    }
    //200 steps are taken, now check if this habitat is safe (cover>=2)
    if (Cover(m_Location_x, m_Location_y) > 2)
    {
        IsSafe = true;
    }
    else  //cover not good, so keep running and check if safe every 10 m
    {
        int loop = 0; //counts how many times through the for-loop, time's up after 15 (total 450 steps)
        while ((IsSafe == false) && (loop <= 15))
        {
            if ((m_Location_x < 10) || (m_Location_y < 10) || (m_Location_x >= SimW - 10) || (m_Location_y >= SimH - 10)) nearedge = true; else nearedge = false;
            if (nearedge) RunStepsCC(m_Location_x, m_Location_y, 10, thisway);
            else RunSteps(m_Location_x, m_Location_y, 10, thisway);
            if (Cover(m_Location_x, m_Location_y) > 2)
            {
                IsSafe = true;
            }
            else IsSafe = false;  //keep running in next time step
            loop++;
        }
    }
    return IsSafe;
}
//---------------------------------------------------------------------------

bool Roe_Base::RunSteps(int x, int y, int a_steps, int a_rightway)
{
    //running from danger
    for (int i = 0; i < a_steps; i++)   //take 200 steps in that direction
    {
        int pick = random(2); // Choose general right or left preference 
        int stuck = 0;
        while ((Cover(m_Location_x + Vector_x[a_rightway], m_Location_y + Vector_y[a_rightway]) == -1))  //thisway not possible, check +/- 1
        {
            //random pick between the other two directions
            if (pick == 0) a_rightway = (a_rightway + 1) & 0x07; else a_rightway = (a_rightway + 7) & 0x07;
            if (++stuck > 8) {
                m_IsDead = true;
                return true;
            }
        }
        //check if rightway is road
        if (Cover(m_Location_x + Vector_x[a_rightway], m_Location_y + Vector_y[a_rightway]) == 1)
        {
            //get width of road
            int width = m_OurLandscape->SupplyRoadWidth(m_Location_x + Vector_x[a_rightway], m_Location_y + Vector_y[a_rightway]);
            double mort = CalculateRoadMortality(m_Location_x + Vector_x[a_rightway], m_Location_y + Vector_y[a_rightway], width);
            if (g_rand_uni() < mort) //roe dies
            {
                m_IsDead = true;
                return false;
            }
        }
        //change coordinates
        m_Location_x += Vector_x[a_rightway];
        m_Location_y += Vector_y[a_rightway];
    }
    return false;
}
//----------------------------------------------------------------------------
bool Roe_Base::RunStepsCC(int x, int y, int a_steps, int a_rightway)
{
    //running from danger
    int pick = random(2); // Choose general right or left preference 
    for (int i = 0; i < a_steps; i++)   //take 200 steps in that direction
    {
        int stuck = 0;
        APoint pt = m_OurLandscape->CorrectCoordsPt(m_Location_x + Vector_x[a_rightway], m_Location_y + Vector_y[a_rightway]);
        while ((Cover(pt.m_x, pt.m_y) == -1))  //thisway not possible, check +/- 1
        {
            //random pick between the other two directions
            if (pick == 0) a_rightway = (a_rightway + 1) & 0x07; else a_rightway = (a_rightway + 7) & 0x07;
            if (++stuck > 8) {
                m_IsDead = true;
                return true;
            }
        }
        //check if rightway is road
        if (Cover(pt.m_x, pt.m_y) == 1)
        {
            //get width of road
            int width = m_OurLandscape->SupplyRoadWidth(pt.m_x, pt.m_y);
            double mort = CalculateRoadMortality(pt.m_x, pt.m_y, width);
            if (g_rand_uni() < mort) //roe dies
            {
                m_IsDead = true;
                return false;
            }
        }
        //change coordinates
        m_Location_x = pt.m_x;
        m_Location_y = pt.m_y;
    }
    return false;
}
//----------------------------------------------------------------------------

/**
Roe_Base::NutriValue - returns the amount of energy gained per 10 minutes in a particular polygon.
Two switch statements; 1 for element type and 1 for crop type if field.
Digestable energy content (DEC) is scaled to a monthly estimate of intake rate. Returns DEC value.
calls functions SupplyElementType(), SupplyVegType(), SupplyVegBiomass().
*/
int Roe_Base::NutriValue(int p_x, int p_y)
{
	//Nutrivalue returns the amount of energy gained per 10 minutes in this polygon.
	//Two switch statements; 1 for element typse and 1 for crop type if field.
	//Digestable energy content (DEC) is scaled to a monthly estimate of intake rate.

	int DEC = 0; //digestable energy content
	double nutri = 0;   //energy gain per minute
	int day = m_OurLandscape->SupplyDayInYear();
	TTypesOfLandscapeElement Elem = m_OurLandscape->SupplyElementType(p_x, p_y);

	int month = m_OurLandscape->SupplyMonth();
	if (Elem != tole_Field)
	{
		switch (Elem)//all but field returns value in this switch
		{
		case tole_Building:
		case tole_Garden:
		case tole_StoneWall:
		case tole_Fence:
		case tole_Pond:
		case tole_Freshwater:
		case tole_Saltwater:
		case tole_River:
		case tole_Coast:
		case tole_ActivePit:
		case tole_Railway:
		case tole_LargeRoad:
		case tole_SmallRoad:
		case tole_Track:
		case tole_Carpark:
		case tole_UrbanNoVeg:
		case tole_Stream:
		case tole_AmenityGrass:
		case tole_Saltmarsh:
		case tole_Churchyard:
		case tole_BareRock:
		case tole_SandDune:
		case tole_PlantNursery:
		case tole_WindTurbine:
		case tole_Pylon:
        case tole_UrbanVeg:

			DEC = 0;     //non habitats return 0
			break;

		case tole_Marsh:
		case tole_ConiferousForest:
			DEC = 950;
			break;

        case tole_Wasteland:
        case tole_WoodlandMargin:
        case tole_Vildtager:
        case tole_Heath:
		case tole_RoadsideVerge:
		case tole_RoadsideSlope:
		case tole_PermPasture:
		case tole_PermPastureLowYield:
		case tole_PermPastureTussocky:
		case tole_PermanentSetaside:
		case tole_PitDisused:
		case tole_NaturalGrassDry:
		case tole_NaturalGrassWet:
		case tole_RiversidePlants:
		case tole_FieldBoundary:
		case tole_HeritageSite:
        case tole_UnknownGrass:
			DEC = 1200;
			break;

        case tole_WoodyEnergyCrop:
        case tole_IndividualTree:
        case tole_Orchard:
		case tole_DeciduousForest:
		case tole_MixedForest:
		case tole_Scrub:
		case tole_Hedges:
		case tole_HedgeBank: // added by ljk on 13/7/2012
		case tole_RiversideTrees:
		case tole_Copse:
			if ((day > 100) && (day < 270)) DEC = 1400;
			else DEC = 950;
			break;

		case tole_YoungForest:   //new class 55
			if ((day > 100) && (day < 270)) DEC = 1500;
			else DEC = 950;
			break;

		case tole_Foobar:
			m_OurLandscape->Warn("Roe_Base::NutriValue(x,y): Unknown element type!", "tole_Foobar");
			exit(1);
			break;
		default:
			//throw an error
			m_OurLandscape->Warn("Roe_Base::NutriValue(x,y): Unknown element type!", m_OurLandscape->PolytypeToString(Elem));
			exit(1);
			break;
		}
	}
	else //is field
	{
		//get crop type
		TTypesOfVegetation VegType = m_OurLandscape->SupplyVegType(p_x, p_y);
		double Biomass = ((double)m_OurLandscape->SupplyVegBiomass(p_x, p_y));
		if (Biomass > 00) //160 ljk 31/7-2012, 160 - is this an arbitrary value?? Where does it come from? Is it just calibrated this way?
		{
			switch (VegType)  //veg. or croptype as applicable
			{
			case tov_SpringBarley:
			case tov_SpringBarleySpr:
			case tov_Oats:
			case tov_OOats:
			case tov_SpringBarleySilage:
			case tov_OSpringBarley:
			case tov_WinterBarley:
			case tov_WinterWheat:
			case tov_WinterRye:
			case tov_OWinterBarley:
			case tov_OWinterRye:
			case tov_Triticale:		
			case tov_OTriticale:
			case tov_OWinterWheat:

			case tov_Maize:
			case tov_OMaizeSilage:
			case tov_MaizeSilage:
			case tov_MaizeStrigling:
				DEC = 1200;
				break;

				//seed- and permanent grass, same as natural grassy habitats
			case tov_SetAside:
			case tov_PermanentGrassGrazed:
			case tov_OPermanentGrassGrazed:
			case tov_SeedGrass1:
			case tov_SeedGrass2:
			case tov_PermanentSetAside:
			case tov_Heath:
				DEC = 1100;
				break;

				//undersown crops and clover grass
			case tov_OWinterWheatUndersown:
			case tov_SpringBarleyCloverGrass:
			case tov_SpringBarleyGrass:
			case tov_OBarleyPeaCloverGrass:
			case tov_OSBarleySilage:
			case tov_CloverGrassGrazed1:
			case tov_CloverGrassGrazed2:
			case tov_OCloverGrassGrazed1:
			case tov_OCloverGrassGrazed2:
			case tov_OCloverGrassSilage1:
            case tov_OSeedGrass2:
            case tov_OSeedGrass1:
				DEC = 1300;
				break;

				//high quality crops
			case tov_FieldPeas:
			case tov_BroadBeans:
			case tov_FodderBeet:
			case tov_SugarBeet:
			case tov_OFodderBeet:
			case tov_OFieldPeas:
			case tov_OFieldPeasSilage:
				DEC = 1500;
				break;

				//no-good crops
			case tov_SpringRape:
			case tov_WinterRape:
			case tov_OWinterRape:
			case tov_OGrazingPigs:
			case tov_OSpringBarleyPigs:
			case tov_OCarrots:
			case tov_Potatoes:
			case tov_PotatoesIndustry:
			case tov_OPotatoes:
				DEC = 100;
				break;

				//others
			case tov_None:
			case tov_NoGrowth:
			case tov_OFirstYearDanger:
				break;

			default:
				m_OurLandscape->Warn("Roe_Base::NutriValue(x,y): Unknown vegetation type!", m_OurLandscape->VegtypeToString(VegType));
				exit(1);
				break;
			}
		}
		else DEC = 100;
	}

	//DEC is multiplied by monthly estimate of intake rate
	nutri = DEC * IntakeRate[month - 1];
	return (int)nutri;
}
//---------------------------------------------------------------------------

/**
Roe_Base::Feeding - When habitat has been deemed suitable for feeding, next step is to
evaluate whether steps should be weighted towards range centre or not. To avoid that animals 
gets trapped when using very directed walk, their progress during the last hour (6 timesteps)
is monitored.
Calls DistanceTo(), NextStep().
*/
int Roe_Base::Feeding(bool p_Disperse)
{
  //Habitat is suitable for feeding. Now evaluate whether steps should be
  //weighted towards range centre or not

    int weight = 0;
    int dir;
    int distrc;

    //first get distance and direction back to range centre
    distrc=DistanceTo(m_Location_x, m_Location_y, m_RangeCentre.m_x, m_RangeCentre.m_y);
    dir=DirectionTo(m_Location_x, m_Location_y, m_RangeCentre.m_x, m_RangeCentre.m_y);

    //to avoid that animals gets trapped when using very directed walk, keep track
    //of their progress during the last hour (6 timesteps)
    if ((m_weightedstep>5)&&(m_distrc-distrc<50))
    { //must be stuck so take the next steps without weight
       weight=0;
    }
    else
    {
      if ((p_Disperse==true)||(distrc<=m_SearchRange))  //don't worry about rangecentre
      {
        weight=0;
        m_weightedstep=0;
      }
      else
      {
        if(distrc<=m_SearchRange+100) weight=1;
         else if ((distrc>m_SearchRange+100)&&(distrc<=m_SearchRange+200)) weight=2;
          else if (distrc>m_SearchRange+200)   weight=3;
        m_weightedstep++;
        m_distrc = distrc;
      }
    }

    //take the steps
    if (NextStep(weight,dir,0)==-1) // roe has taken a step and died doing it
    {
       m_IsDead=true;
    }
    return 0;
}
//---------------------------------------------------------------------------
/**
Roe_Base::DistanceTo - calculates distance from 2 coordinates to other 2 coordinates.
*/
int Roe_Base::DistanceTo(int p_x, int p_y, int p_To_x, int p_To_y)
{
    int x = (p_x - p_To_x);
    int y = (p_y - p_To_y);
    if (x < 0) x = x * -1;
    if (y < 0) y = y * -1;
    if (x > (SimW / 2))  x = SimW - x;
    if (y > (SimH / 2))  y = SimH - y;
    if ((x != 0) || (y != 0))
    {
        return (int)(sqrt(double((x * x) + (y * y))));
    }
    else return 0; // in the case where roe is standing on p_To_x,p_To_y
}
//---------------------------------------------------------------------------

/**
Roe_Base::DirectionTo - calculates direction to a specific location
*/
int Roe_Base::DirectionTo(int p_x,int p_y, int p_To_x, int p_To_y)
 //calculates direction to a location
{
  double a = double(p_x-p_To_x);
  double b = double(p_y-p_To_y);
  int thisway=0;

  if ((b>SimH/2)||(b*-1>SimH/2))
  {
    b=b*-1;
  }
  if((a>SimW/2)||(a*-1>SimW/2))
  {
    a=a*-1;
  }
  if (a > 0)
  {
    if (b>0) thisway=7;
    else if (b<0)
    {
      thisway=5;
    }
    else thisway=6; //b=0
  }
  else if (a < 0)
  {
    if (b>0) thisway=1;
    else if (b<0) thisway=3;
    else thisway=2;
  }
  else if (a==0)
  {
    if (b>0) thisway=0;
    else thisway=4;
  }
  return thisway;
}


//---------------------------------------------------------------------------'
/**
 Roe_Base::LegalHabitat - returns a value indicating whether element type can be crossed or not.
 -1: lethal, cannot be crossed, 0: roads, needs evaluation, 1: can be crossed

*/
int Roe_Base::LegalHabitat(int p_x,int p_y)
{
  //returns a value indicating whether element type can be crossed or not.
  //-1: lethal, cannot be crossed, 0: roads, needs evaluation, 1: can be crossed
  TTypesOfLandscapeElement Elem=m_OurLandscape->SupplyElementType(p_x,p_y);
  int value;
  switch(Elem)
  {
     case tole_Building:
     case tole_Garden:
     case tole_StoneWall:
	 case tole_Fence:
     case tole_Saltwater:
	 case tole_Pond:
	 case tole_Freshwater:
     case tole_River:
     case tole_ActivePit:
	 case tole_UrbanNoVeg:
	 case tole_Churchyard:  //204
	 case tole_BuiltUpWithParkland:
	 case tole_Carpark:
	 case tole_RoadsideSlope:
	 case tole_BareRock:
     case tole_WindTurbine:
     case tole_Pylon:
     case tole_PlantNursery:
     case tole_UrbanVeg:
         value = -1;
       break;

     case tole_Field:
     case tole_RoadsideVerge:
     case tole_PermPasture:
     case tole_PermPastureLowYield:
	 case tole_PermPastureTussocky:
     case tole_DeciduousForest:
     case tole_ConiferousForest:
     case tole_MixedForest:
     case tole_YoungForest:   //new class 55
	 case tole_Copse:
     case tole_Scrub:
     case tole_PitDisused:
     case tole_RiversideTrees:
     case tole_RiversidePlants:
	 case tole_NaturalGrassDry:
	 case tole_NaturalGrassWet:
	 case tole_Coast:
     case tole_PermanentSetaside:
     case tole_Railway:
     case tole_Track:
     case tole_Hedges:
	 case tole_HedgeBank: 
     case tole_FieldBoundary:
     case tole_Marsh:
	 case tole_MownGrass: // 58
	 case tole_OrchardBand: // 57
	 case tole_Orchard: //56
	 case tole_Saltmarsh: // 95
	 case tole_Heath:
	 case tole_AmenityGrass:
	 case tole_UrbanPark:
	 case tole_SandDune:
	 case tole_HeritageSite:
     case tole_Wasteland:
     case tole_UnknownGrass:
     case tole_Vildtager:
     case tole_WoodlandMargin:
     case tole_WoodyEnergyCrop:
     case tole_IndividualTree:
    value = 1;
       break;

     case tole_LargeRoad:
     case tole_SmallRoad:
     case tole_Stream:
         value=0;
       break;

     case tole_Foobar:
       m_OurLandscape->Warn("Roe_Base::LegalHabitat(): Unknown element type!", m_OurLandscape->PolytypeToString(Elem));
       exit( 1 );
       break;
	 default:
      //throw an error
	 m_OurLandscape->Warn("Roe_Base::LegalHabitat(): Unknown element type!", m_OurLandscape->PolytypeToString(Elem));
     exit( 1 );
    break;
  }
  return value;
}

//--------------------------------------------------------------------------
/**
Roe_Base::Cover - Input coordinates. Each type of habitat is given a relative cover value for an adult roe deer from
0-4. Non-habitat is given a value of -1. Open land with very low veg = 0. Related to VegHeight
and type of vegetation. In open land all that matters is VegHeight. Returns cover value.
Calls functions SupplyElementType(), SupplyVegHeight() and SupplyTreeHeight().
*/
int Roe_Base::Cover(int a_polyref)
{
	return CalcCover(m_OurLandscape->SupplyElementType(a_polyref), a_polyref);
}
//--------------------------------------------------------------------------

int Roe_Base::Cover(int p_x, int p_y)
{
	return CalcCover(m_OurLandscape->SupplyElementType(p_x, p_y), m_OurLandscape->SupplyPolyRef(p_x, p_y));
}
//--------------------------------------------------------------------------

int Roe_Base::CalcCover(TTypesOfLandscapeElement a_ele, int a_poly)
{
	/**
	* Each type of habitat is given a relative cover value for an adult roe from
	* 0-4. Non-habitat is given a value of -1. Openland with very low
	* veg = 0. Related to VegHeight and type of veg.
	* In open land all that matters is VegHeight
	*/

  int cov;
  int height;
  char errornum[20];
  switch(a_ele)
  {
	 case tole_BareRock:
	 case tole_UrbanNoVeg:
	 case tole_UrbanPark:
	 case tole_SandDune:
	 case tole_Churchyard:  //204
	 case tole_BuiltUpWithParkland:
	 case tole_Carpark:
	 case tole_Building:
     case tole_Garden:
     case tole_Saltwater:
	 case tole_Pond:
	 case tole_Freshwater:
     case tole_River:
     case tole_Coast:
     case tole_ActivePit:
     case tole_Railway:
	 case tole_HeritageSite:
	 case tole_Stream:
     case tole_PlantNursery:
     case tole_UrbanVeg:                    
         cov=-2;
       break;

     case tole_RoadsideVerge:
     case tole_StoneWall:
	 case tole_Fence:
	 case tole_RoadsideSlope:
     case tole_Pylon:
     case tole_WindTurbine:
         cov=0;
       break;

     case tole_UnknownGrass:
     case tole_Wasteland:
     case tole_Vildtager:
     case tole_NaturalGrassDry: // 110
	 case tole_NaturalGrassWet: // 110
	 case tole_MownGrass: // 58
	 case tole_OrchardBand: // 57
	 case tole_Saltmarsh: // 95
	 case tole_Heath:
	 case tole_AmenityGrass:
	 case tole_PermPasture:
     case tole_PermPastureLowYield:
	 case tole_PermPastureTussocky:
     case tole_Marsh:
     case tole_Field:
	 case tole_PermanentSetaside:
		 cov = 1 + (int)floor(m_OurLandscape->SupplyVegHeight(a_poly)*0.02);
       if ( cov > 4 )
	     cov = 4;
       break;
     case tole_DeciduousForest:
     case tole_ConiferousForest:
     case tole_MixedForest:
	 case tole_Copse:
	 case tole_IndividualTree:
       height = m_OurLandscape->SupplyTreeHeight(a_poly);
       if(height<=10) cov = 3;
       else cov = 2;
       break;

     case tole_YoungForest:  //new class 55
     case tole_Scrub:
     case tole_PitDisused:
     case tole_RiversideTrees:
	 case tole_HedgeBank: // added by ljk 13/7/2012
     case tole_Hedges:
     case tole_WoodyEnergyCrop:
     case tole_WoodlandMargin:
     case tole_Orchard:
       cov=3;
       break;

     case tole_RiversidePlants:
     case tole_FieldBoundary:
		 if (m_OurLandscape->SupplyVegHeight(a_poly)<100) cov = 2;
       else cov=3;
       break;

     case tole_LargeRoad:
     case tole_SmallRoad:
     case tole_Track:
       cov=-1;           //need to distingues roads from other non-habitats
       break;

     case tole_Foobar:
       sprintf(errornum, "%d", a_ele);
       m_OurLandscape->Warn("Roe_Base::Cover(): Unknown element type!",
                       errornum);
       exit( 1 );
       break;

	 default:
      //throw an error
      sprintf(errornum, "%d", a_ele);
      m_OurLandscape->Warn("Roe_Base::Cover(): Unknown element type!",
                       errornum);
      exit( 1 );
      break;
  }
  return cov;
}
//-------------------------------------------------------------------------

/**
Roe_Base::ProbRoadCross - returns the probability of crossing road, depending on traffic load
and road size (width). Calls function SupplyTrafficLoad().
*/
double Roe_Base::ProbRoadCross (int p_x,int p_y, int p_width)
{
   double WidthIndex;
   double TrafficLoadIndex;
   double traffic=(double) m_OurLandscape->SupplyTrafficLoad(p_x,p_y);//absolute no. of cars/hr
   //scale width to largest roadtype to get value between 0 and 1
   WidthIndex=(double)p_width/MaxRoadWidth;
   //scale traffic to national max to get index between 0 and 1
   TrafficLoadIndex=(double)traffic/MaxTraffic;
   return  (2+(((TrafficLoadIndex+1)+(WidthIndex+1))/-2));
   //returns between 0 and 1.00
}
//---------------------------------------------------------------------------
/**
Roe_Base::CalculateRoadMortality - calculates the mortality probability of crossing a road, depending
on traffic load, probability of car passing as roe deer is passing, given this - probability of roe deer 
getting hit. Calls function SupplyTrafficLoad().
*/
double Roe_Base::CalculateRoadMortality (int p_x,int p_y, int p_width )
{
   //get trafficload
   double traffic=(double) m_OurLandscape->SupplyTrafficLoad(p_x,p_y); //cars/hr
   //prob of a car passing while roe is crossing
   double cars =((double)traffic/1200);  //cars/3 sec
   if (g_rand_uni()<((double)cars))  //a car is passing, so calculate roes prob of getting hit (assume the care is 2m wide)
   {
      //The prob. of getting killed is thus cars*p_width/2m
       return (cars / (p_width / 2)) * RoeRoadMortalityScaler;
   }
   else return 0.0;  //no car passing
}
//--------------------------------------------------------------------------
/**
Roe_Base::WalkTo - directed movement towards a position, all habitat except lethal
habitat is used. Roads can be crossed, but mortality probability due to road crossing is 
calculated.
Calls DirectionTo(), LegalHabitatCC(), CorrectCoords(), SupplyRoadWidth(), CalculateRoadMortality(),
DirectionToCC().
*/
void Roe_Base::WalkTo(int pos_x, int pos_y)
{
  //directed movement towards a position, all habitat except lethal
  //habitat is used. Roads can be crossed
  int t[8];
  int q[8];
  bool found=false;
  int x=m_Location_x+SimW;
  int y=m_Location_y+SimH;
  int step=0;
  //First get direction and distance to the position
  int dir=DirectionTo(m_Location_x,m_Location_y,pos_x,pos_y);
  int dist=DistanceTo(m_Location_x,m_Location_y,pos_x,pos_y);
  int range=4*dist;
  //Is it possible to go in preferred direction?
  for (int i=0; i<range; i++)
  {
    t[0]=(dir+8) & 7;
    q[0]=LegalHabitatCC( x+Vector_x[t[0]], y+Vector_y[t[0]] );
    if(q[0]>0)  //good habitat
    {
       found=true;
       step=1;
    }
    else if(q[0]==0) //road, check if possible to cross
    {
      int check; //check habitat on other side of road
      //get width of road
      int xi = x+Vector_x[t[0]];
      int yi = y+Vector_y[t[0]];
      m_OurLandscape->CorrectCoords( xi, yi );
      int width=m_OurLandscape->SupplyRoadWidth( xi, yi );
      double prob=ProbRoadCross( xi, yi, width );
      if(g_rand_uni()< prob) //yes roe will cross
      {
        double mort=CalculateRoadMortality( xi, yi, width );
        if (g_rand_uni()<mort)  //roe dies
        {
          m_IsDead=true;
        }
        else  //survives
        {  //locate the other side of road
          for(int j=1;j<20;j++) {
            int road=LegalHabitatCC( x+Vector_x[t[0]]*j, y+Vector_y[t[0]]*j);
            if(road!= 0) {
	      //not road anymore
              //check 3 steps further on other side of road.
	      //If road or urban you can't cross
              check = LegalHabitatCC( x+(Vector_x[t[0]]*(j+3)),
				      y+(Vector_y[t[0]]*(j+3)) );
              if(check>1) //OK
              {
                found=true;
                step=j;   //need to walk this many steps to get across
              }
              break;
            }
          }
        }
      }
    }
    if(found)
    {
        //change coordinates
        x +=(Vector_x[t[0]]*step);
        y +=(Vector_y[t[0]]*step);
    }
    else //unsuitable habitat or road that cannot be crossed
    {
      //check the remaining directions
      t[1]=(dir+7) & 7;
      t[2]=(dir+9) & 7;
      t[3]=(dir+6) & 7;
      t[4]=(dir+10) & 7;
      t[5]=(dir+11) & 7;
      t[6]=(dir+5) & 7;
      t[7]=(dir+12) & 7;
      q[1]=LegalHabitatCC( x+Vector_x[t[1]], y+Vector_y[t[1]] );
      q[2]=LegalHabitatCC( x+Vector_x[t[2]], y+Vector_y[t[2]] );
      q[3]=LegalHabitatCC( x+Vector_x[t[3]], y+Vector_y[t[3]] );
      q[4]=LegalHabitatCC( x+Vector_x[t[4]], y+Vector_y[t[4]] );
      q[5]=LegalHabitatCC( x+Vector_x[t[5]], y+Vector_y[t[5]] );
      q[6]=LegalHabitatCC( x+Vector_x[t[6]], y+Vector_y[t[6]] );
      q[7]=LegalHabitatCC( x+Vector_x[t[7]], y+Vector_y[t[7]] );
      //evaluate all directions
      for (int j=1; j<8;j++)
      {
        if(q[j]>0)  //only evaluate better than road
        {
          found=true;
          step=1;
          //change coordinates
          x +=Vector_x[t[j]]*step;
          y +=Vector_y[t[j]]*step;
          break;
        }
      }
    }
    //check distance and direction now
    dir=DirectionToCC( x, y, pos_x, pos_y );
    dist=DistanceToCC( x, y, pos_x, pos_y );
    if (dist==0) break;
  }
  if(found)
  {
    //change roes position
    m_Location_x=x;
    m_Location_y=y;
    m_OurLandscape->CorrectCoords( m_Location_x, m_Location_y );
  }
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/**
Roe_Base::NextStep - This function makes the animal object perform a semirandom walk choosing
best feeding habitat based on cover and nutri.value one step at a time. Equals 10 minutes of activity.
Calls functions LegalHabitatCC(), SupplyElementType(), SupplyRoadWidth(), ProbRoadCross(), CalculateRoadMortality(),
LegalHabitat(), SupplyPolyRef(),CorrectWidth(), CorrectHeight(), SuplyPolyRefCC().
*/
int Roe_Base::NextStep (int weight,int dir, int recurselevel)
{
  // **FN** Tunet 15/6-2001.

  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Base:NextStep():Deadcode warning","");
    exit( 1 );
  }
  #endif
  //semirandom walk choosing best feeding habitat based on cover and nutri.value
  //One step at a time. Equals 10 minutes of activity
  if(recurselevel>2)
  {
    m_OurLandscape ->Warn("Roe_Base:NextStep():Variable out of range, recurselevel","");
    exit( 1 );
  }

  int t[8];
  t[0] = dir;
  t[1] = (dir+ 9) & 7;
  t[2] = (dir+ 7) & 7;
  t[3] = (dir+ 10) & 7;
  t[4] = (dir + 6) & 7;
  t[5] = (dir+ 11) & 7;
  t[6] = (dir + 5) & 7;
  t[7] = (dir + 12) & 7;
  int q[8]= {0};
  int x = m_Location_x;
  int y = m_Location_y;
  bool found=false;
  bool correct=false;
  int step=0;
  int check=-2;
  int index[8]= {0};
  int best=-1;
  int score = -1;
  int thissquare;
  int d[5]={8,7,5,3,1};

  //check whether you step out of area. If so use % Sim, else not
  if((x <= 200)||(x >= SimW-200)||(y <= 200)||(y >= SimH-200)) //next step could take you out of area
  {
    x += SimW;
    y += SimH;
    correct=true;
  }
  if(correct==true)
  {
    //take 50 steps
    for(int i=0;i<50;i++)
    {
      q[0] = LegalHabitatCC( x+Vector_x[t[0]], y+Vector_y[t[0]] );
      q[1] = LegalHabitatCC( x+Vector_x[t[1]], y+Vector_y[t[1]] );
      q[2] = LegalHabitatCC( x+Vector_x[t[2]], y+Vector_y[t[2]] );
      q[3] = LegalHabitatCC( x+Vector_x[t[3]], y+Vector_y[t[3]] );
      q[4] = LegalHabitatCC( x+Vector_x[t[4]], y+Vector_y[t[4]] );
      q[5] = LegalHabitatCC( x+Vector_x[t[5]], y+Vector_y[t[5]] );
      q[6] = LegalHabitatCC( x+Vector_x[t[6]], y+Vector_y[t[6]] );
      q[7] = LegalHabitatCC( x+Vector_x[t[7]], y+Vector_y[t[7]] );
       //q[i]=-1,non-habitat, q[i]=0,road, q[i]=1 suitable

      for (int j=0; j<d[weight]; j++)     //score as many squares as needed
      {
        if (q[j]>=0)  //scores all suitable and finds the best square
        {
          score++;
          index[score]=j;
        }
      }
      if (score==-1) //no suitable options,
      {
         if((recurselevel==2)||(NextStep(0,dir,++recurselevel)==-1))
         {
           return -1;
         }
         else
         {
           return 0;
         }
      }
      else //pick random
      {
        //pick random from those available
        thissquare = random(score+1);
        best=q[index[thissquare]];
      }
      if (best == 0)  //roads or rivers/streams
      {
          //rivers are crossed 50:50. Roads depend on width and traffic load
          int hab = -2;
          double prob = 0.50;
          int width = 0;
          double mort = 0;
          int xi = x + Vector_x[t[index[thissquare]]];
          int yi = y + Vector_y[t[index[thissquare]]];
          m_OurLandscape->CorrectCoords(xi, yi);

          if (m_OurLandscape->SupplyElementType(xi, yi) != tole_Stream)  //road
          {
              //get width of road
              int width = m_OurLandscape->SupplyRoadWidth(xi, yi);
              prob = ProbRoadCross(xi, yi, width);
              //take mortality test
              mort = CalculateRoadMortality(xi, yi, width);
          }
          if (g_rand_uni() < prob) //yes, roe will cross
          {
              //does it survive?
              if (g_rand_uni() < mort)
              {
                  return -1;  //roe is dead
              }
              //Locate other side of the road. First in preferred direction and if
              //not found continue clockwise in all other directions
              int preferred = index[thissquare]; //preferred direction
              for (int k = 0; k < 8; k++) //in each direction
              {
                  for (int j = 1; j < 20; j++)
                  {
                      hab = LegalHabitatCC(x + (Vector_x[t[preferred & 7]] * j),
                          y + (Vector_y[t[preferred & 7]] * j));
                      if (hab > 0) //other side found
                      {
                          //check 3 steps further on other side of road or stream.
                          //If road or urban (check = -1) you can't cross
                          check = LegalHabitatCC(x + (Vector_x[t[preferred & 7]] * (j + 3)),
                              y + (Vector_y[t[preferred & 7]] * (j + 3)));
                          if (check > 0) //OK
                          {
                              found = true;
                              step = j;   //need to walk this many steps to get across
                              index[thissquare] = preferred & 7;  //this direction
                          }
                          break;
                      }
                  }
                  if (!found) preferred++;

                  else break;
              }
          }
          if (!found) //would or could not cross
          { //score again, but don't include road or river
              score = -1;
              for (int j = 0; j < d[weight]; j++)
              {
                  if (q[j] > 0)
                  {
                      score++;
                      index[score] = j;
                  }
              }
              //pick random from those available
              thissquare = random(score + 1);
              best = q[index[thissquare]];
          }

          if (score == -1) //no suitable options,
          {
              if ((recurselevel == 2) || (NextStep(0, dir, ++recurselevel) == -1))  //recursive function
              {
                  return -1;
              }
              else
              {
                  return 0;
              }
          }
      }

    // best way is now found, so check if you are crossing
    // into a new polygon?
    if (m_OurLandscape->
	SupplyPolyRefCC( x+Vector_x[t[index[thissquare]]],
			 y+Vector_y[t[index[thissquare]]])
	!= m_OurLandscape->SupplyPolyRefCC( x, y )) {
      m_Cross_x=m_OurLandscape->CorrectWidth( x );
      m_Cross_y=m_OurLandscape->CorrectHeight( y );
    }
    //change coordinates
    if(found)  //roe is crossing a road or river
      {
         x+=(Vector_x[t[index[thissquare]]]*step);
         y+=(Vector_y[t[index[thissquare]]]*step);
         step=0;
      }
      else
      {
         x+=Vector_x[t[index[thissquare]]];
         y+=Vector_y[t[index[thissquare]]];
      }
      score=-1;
      found=false;
    }
  }

  //PART 2 don't need area correction

  else
  {
    //take 50 steps
    for(int i=0;i<50;i++)
    {
      q[0] = LegalHabitat(x+Vector_x[t[0]],y+Vector_y[t[0]]);
      q[1] = LegalHabitat (x+Vector_x[t[1]],y+Vector_y[t[1]]);
      q[2] = LegalHabitat (x+Vector_x[t[2]],y+Vector_y[t[2]]);
      q[3] = LegalHabitat (x+Vector_x[t[3]],y+Vector_y[t[3]]);
      q[4] = LegalHabitat (x+Vector_x[t[4]],y+Vector_y[t[4]]);
      q[5] = LegalHabitat (x+Vector_x[t[5]],y+Vector_y[t[5]]);
      q[6] = LegalHabitat (x+Vector_x[t[6]],y+Vector_y[t[6]]);
      q[7] = LegalHabitat (x+Vector_x[t[7]],y+Vector_y[t[7]]);
       //q[i]=-1,non-habitat, q[i]=0,road, q[i]=1 suitable

        for (int j=0; j<d[weight]; j++)     //score as many squares as needed
        {
          if (q[j]>=0)  //score only road or better
          {
            score++;
            index[score]=j;
          }
        }
        if (score==-1) //no suitable options,
        {
           if((recurselevel==2)||(NextStep(0,dir,++recurselevel)==-1))
           {
             return -1;
           }
           else
           {
             return 0;
           }
        }
        else
        {
          //pick random from those available
          thissquare = random(score+1);
          best=q[index[thissquare]];
        }
        if (best==0)  //roads or rivers/streams
        {
        //rivers are crossed 50:50. Roads depend on width and traffic load
         
         int hab=-2;
         double prob=0.50;
         int width=0;
         double mort=0;
	 int xi = x+Vector_x[t[index[thissquare]]];
	 int yi = y+Vector_y[t[index[thissquare]]];
     TTypesOfLandscapeElement tole = m_OurLandscape->SupplyElementType(xi, yi);
         if( (tole !=tole_River )&& (tole !=tole_Stream))  //road
         { 
             //get width of road
      int width=m_OurLandscape->SupplyRoadWidth( xi, yi );
             prob=ProbRoadCross( xi, yi, width );
            //take mortality test
            mort=CalculateRoadMortality( xi, yi, width );
         }
         if (g_rand_uni()<prob) //yes, roe will cross
         {
           //does it survive?
            if (g_rand_uni()<mort)
            {
              return -1;  //roe is dead
            }
           //Locate other side of the road. First in preferred direction and if
           //not found continue clockwise in all other directions
           int preferred=index[thissquare]; //preferred direction
           for (int k=0;k<8;k++) //in each direction
           {
             #ifdef JUJ__Debug2
               if(preferred>14)
               {
                 m_OurLandscape ->Warn("Roe_Base:NextStep():Variable out of range, preferred","");
                 exit( 1 );
               }
             #endif
             for(int j=1;j<20;j++)
             {
               hab=LegalHabitat((x+(Vector_x[t[preferred&7]]*j)),
				(y+(Vector_y[t[preferred&7]]*j)));
               if(hab>0) //other side found
               {
                 //check 3 steps further on other side of road or stream.
                 //If road or urban you can't cross
                 check = LegalHabitat( x+(Vector_x[t[preferred&7]]*(j+3)),
				       y+(Vector_y[t[preferred&7]]*(j+3)) );

                 if(check>0) //OK
                 {
                   found=true;
                   step=j;   //need to walk this many steps to get across
                   index[thissquare] = preferred & 7;  //this direction
                 }
                 break;
               }
             }
             if(!found) preferred++;
             else break;
           }
         }
         if(!found) //would or could not cross
         { //score again, but don't include road or river
           score=-1;
           for (int j=0;j<d[weight];j++)
           {
             if (q[j]>0)
             {
               score++;
               index[score]=j;
             }
           }
           //pick random from those available
           thissquare = random(score+1);
           best=q[index[thissquare]];
         }

         if (score==-1) //no suitable options,
         {
           if((recurselevel==2)||(NextStep(0,dir,++recurselevel)==-1))  //recursive function
           {
             return -1;
           }
           else
           {
             return 0;
           }
         }
       }
		 
        // best way is now found, so check if you are crossing
	// into a new polygon?
        if (m_OurLandscape->
	    SupplyPolyRef(x+Vector_x[t[index[thissquare]]],
			  y+Vector_y[t[index[thissquare]]])
	    != m_OurLandscape->SupplyPolyRef(x,y))
        {
          m_Cross_x=x;
          m_Cross_y=y;
        }
        //change coordinates
        if(found==true)  //roe is crossing a road
        {
          x+=(Vector_x[t[index[thissquare]]]*step);
          y+=(Vector_y[t[index[thissquare]]]*step);
        }
        else
        {
          x+=Vector_x[t[index[thissquare]]];
          y+=Vector_y[t[index[thissquare]]];
        }
        score=-1;
        found=false;
    } //end of 50 steps
  }

  //update roes location
  if(correct==true)
  {
    m_Location_x = m_OurLandscape->CorrectWidth( x );
    m_Location_y = m_OurLandscape->CorrectHeight( y );
  }
  else
  {
    m_Location_x = x;
    m_Location_y = y;
  }
  return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/**
Roe_Base::Roe_Base - constructor for Roe base. Sets all attributes via call to Init
*/
Roe_Base::Roe_Base(int x, int y, Landscape* L, RoeDeer_Population_Manager* RPM)
    :TAnimal(x, y, L)
{
    Init(x, y, L, RPM);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/**
Roe_Base::Roe_Base - constructor for Roe base. Sets all attributes via call to Init
*/
void Roe_Base::ReInit(int x, int y, Landscape* L, RoeDeer_Population_Manager* RPM)
{
    TAnimal::ReinitialiseObject(x, y, L);
    Init(x, y, L, RPM);
}
//---------------------------------------------------------------------------

void Roe_Base::Init(int x, int y, Landscape * L, RoeDeer_Population_Manager * RPM)
{
  m_RangeCentre.m_x=x;
  m_RangeCentre.m_y=y;
  m_HomeRange=0;
  m_HaveRange=false;
  m_OldRange.m_x=x;
  m_OldRange.m_y=y;
  m_danger_x=0;
  m_danger_y=0;
  m_Disperse = false;
  m_float=false;
  m_OurPopulation=RPM;
  SimW = m_OurPopulation->SimW;
  SimH = m_OurPopulation->SimH;
  m_Cross_x=x;
  m_Cross_y=y;
  CurrentRState=rds_Initialise;
  timestep=0;
  m_RumiCount=0;
  m_FeedCount=0;
  m_RecovCount=0;
  m_weightedstep=0;
  m_EnergyGained=0;
  m_LengthFeedPeriod = 3;  //in timesteps, just the first day, updated daily in UpdateEnergy
  m_LengthRuminate = 0;  //set every time roe finishes a feeding period
  Alive=0x0DEADC0DE;
}
//---------------------------------------------------------------------------
Roe_Base::~Roe_Base ()
{
  Alive=0;
}
//--------------------------------------------------------------------------
/**
Roe_Base::SupplyInfo - returns info on age, size, range center.
*/

RoeDeerInfo Roe_Base::SupplyInfo()
{
   RoeDeerInfo RDI;
   RDI.m_Age = m_Age;
   RDI.m_Size = m_Size;
   RDI.m_Range = m_RangeCentre;
   RDI.m_OldRange = m_OldRange;
   return RDI;
}
//----------------------------------------------------------------------------





