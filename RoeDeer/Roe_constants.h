//
// Roe_constants.h
//

#ifndef ROE_CONSTANTS_H
#define ROE_CONSTANTS_H




//extern const char* SimulationName;
extern const int Cell_Size;
extern const int Cell_HalfSize;
extern const double InvCell_Size;   //1/Cell_Size
extern const int MinRange;
extern const int MaxRange;
extern const int Vector_x[8];
extern const int Vector_y[8];
extern const int MaxDistToMum;
extern const int MinDispDistance;
extern const double ThreatBaseProbability;

extern const unsigned MaleDensityThreshold;

extern const unsigned FemaleDensityThreshold;

extern const int NoRandomAreas;

extern const double DivideRandomAreas;
extern const int MinRangeQuality;
extern const int AverageRecoveryTime;
extern const int MaxRecoveryTime;
extern const int CoverThreshold1;
extern const int CoverThreshold2;

extern const double IntakeRate[12];

extern const int MinNutri;

extern const int MaxRoadWidth;
extern const int MaxTraffic;
extern const double RoeRoadMortalityScaler;

extern const int Inact_Periods_Males[12];
extern const int Inact_Periods_Fawns[12];

extern const double FemaleRMR[7];
extern const double FawnRMR;
extern const double MaleRMR;

extern const double MinWeightFawn[5];

extern const double prop_femalefawns;

extern const int Male_FeedBouts[12];
extern const int Female_FeedBouts[12];

extern const int CostFemaleAct[12];
extern const int CostMaleAct[12];
extern const int CostFawnAct[12];

extern const double Anabolic;
extern const double Anabolic_Inv;

extern const double Catabolic;
extern const double Catabolic_Inv;

extern const int FloatMort;
extern const double FemaleDMR[4];
extern const double MaleDMR[4];

extern const double FawnDMR[2];

extern const int Minhrcare[2];
extern const int Flush_dist[2];
extern const int MalePrime;

extern const int GestationPeriod;

extern const double FightMortalityWinLoss[2];
extern const int CriticalWeightMales;
extern const int MinWeightMales;
extern const int MaxRoeFemaleDistance;

#endif // ROE_CONSTANTS_H
