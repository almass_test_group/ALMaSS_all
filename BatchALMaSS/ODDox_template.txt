/**
#\mainpage TITLE ODDox Documentation
#\page page4 TITLE ODDox Documentation

\htmlonly 
<style type="text/css">
body {
    color: #000000
    background: white;
  }
h2  {
    color: #0022aa
    background: white;
  }
</style>
<h1>
<small>
<small>
<small>
<small>
Created by<br>
<br>
</small>
</small>
Chris J. Topping<br>
<small>Department of Wildlife Ecology<br>
National Environmental Research Institute, University of Aarhus<br>
Grenaavej 14<br>
DK-8410 Roende<br>
Denmark<br>
<br>
1st September 2008<br>
</small>
</small>
</small>
</h1>
<br>
\endhtmlonly
\n
<HR> 
\n
\htmlonly
<h3>A note about ODDox</h3>
\endhtmlonly
This model description follows broadly the ODD protocol for describing individual- and agent-based models (<a href="page0.html#ref4">
Grimm et al. 2006</a>) and consists of three main elements. 
The first provides an (O)verview, the second explains general concepts underlying 
the models (D)esign, the last, (D)etail, provides the information about the various model constructs in detail. The Detail section 
presented has been implemented by using the Doxygen program on the commented source code, creating and html-based documentation. 
In this way the source can be viewed and navigated through in an efficient and manageable manner. This has the advantage that all links 
between objects, methods and functions are highlighted and can be followed easily together with the source code, even without more than 
a very basic understanding of programming.\n
\n
In addtion to the change of Detail to using Doxygen, the section originally entitled 'Submodels' in ODD has been alterned to 'Interconnections'.
The reason for this is that submodels will be described under the classes that are covered in the doxygen part of the ODDox and that classes
with connectsion to the classes described are not submodels but entities existing at the same hierarchical level. The section 'Interconnections'
is therefore used to highlight important linkages to the main classes described in any one section of the ODDox. Note also that the ODDox for 
ALMaSS consists of many individual documentation sections.  Interconnections allows you to navigate to those descriptions that are of primary 
relevance.\n
\n
A note about code structure: This program was written in C++ code which is an object-oriented language. Objects (e.g. a male hare) are 
termed 'instances' of a 'class' (e.g. Hare_Male). Classes are arranged in a hierarchy whereby information contained in a base class is 
available to descendent classes. For an example see the class diagram at the top of TALMaSSObject. This diagram shows how TAnimal is 
descended from TALMaSSObject, and subsequently THare is descended from TAnimal, and Hare_Male from THare. Understanding this basic 
concept and terminology will be an advantage when traversing through the code using the hyperlinks since methods (functions, behaviours) 
and attributes that are not modified in the descendent class need to be viewed in the base class.\n
This method of programming reduces code size and eases code development. An example of this approach can be seen by comparing Hare_Juvenile::st_Dispersal  with Hare_Female::st_Dispersal. In this case the female dispersal uses the juvenile code, then adds special female behaviour afterwards.\n
\n
<HR> 
\n
\htmlonly 
<h2><big>Overview</big></h2>
\endhtmlonly

XXXXXXX description following ODD protocol
TEXT \n
\n

\htmlonly <h2>  1. Purpose </h2>
\endhtmlonly
TEXT\n
\n
\htmlonly <h2>  2. State variables and scales </h2>
\endhtmlonly

\n
\htmlonly <h3>  2.a. Process Overview and Scheduling</h3>
\endhtmlonly

\n
<b>2.a.i	Subhead </b> <br>
\n
TEXT\n
\n
<b>2.a.ii	Subhead </b><br>
<br>
TEXT \n
\n
\htmlonly <h3>  2.b	Process overview</h3>
\endhtmlonly
TEXT \n
\n
TEXT \n
\n
\htmlonly <h3>  2.d		Scheduling</h3>
\endhtmlonly
TEXT \n
\n
<HR> 
\htmlonly 
<h2><big>Design</big></h2>
\endhtmlonly
\htmlonly <h2> 3. Design Concepts</h2>
\endhtmlonly
\htmlonly <h3>  3.a Emergence</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3>  3.b. Adaptation</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3> 3.c Fitness</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3>  3.d Prediction</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3>  3.e Sensing</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3>  3.f Interaction</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3>   3.f.i Subhead if necessary </h3> \endhtmlonly
TEXT \n
\htmlonly <h3> 3.g Stochasticity</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3>  3.h Collectives</h3>
\endhtmlonly
TEXT \n
\htmlonly <h3>  3.i Observation</h3>
\endhtmlonly
TEXT \n
\htmlonly <h2> 4. Initialisation</h2>
\endhtmlonly
TEXT \n
\htmlonly <h2> 6. Interconnections</h2>
\endhtmlonly
TEXT \n
\n
*/