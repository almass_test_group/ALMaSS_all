/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Movementmap.cpp This file contains the source for the MovementMap class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of June 2003 \n
 All rights reserved. \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//---------------------------------------------------------------------------

#define _CRT_SECURE_NO_DEPRECATE
#include "ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../Landscape/MapErrorMsg.h"
#include "../Spiders/Spider_toletov.h"
#include "../Beetles/Beetle_toletov.h"
#include "../Partridge/Partridge_toletov.h"

MovementMapUnsigned::MovementMapUnsigned(Landscape* L, int spref)
{
	m_ALandscape = L;
	// must make sure that we have whole words
	unsigned maxx = unsigned(m_ALandscape->SupplySimAreaWidth());
	unsigned maxy = unsigned(m_ALandscape->SupplySimAreaHeight());
	m_TheMap.resize(maxx * maxy);
	Init(spref);
}

MovementMapUnsigned::~MovementMapUnsigned()
{
	;
}
//-----------------------------------------------------------------------------

void MovementMapUnsigned::Init(int spref)
{
	for (int y = 0; y < m_ALandscape->SupplySimAreaHeight(); y++)
	{
		for (int x = 0; x < m_ALandscape->SupplySimAreaWidth(); x++)
		{
			int colour = 0;
			if (spref == 2) // erigone 
			{  
				colour = spider_tole_movemap_init(m_ALandscape, x, y);
			}
			SetMapValue(x, y, colour);
		}
	}
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

MovementMap::MovementMap(Landscape * L, int spref)
{
  m_ALandscape = L;
  // must make sure that we have whole words
  maxx =(m_ALandscape->SupplySimAreaWidth()+15)/16;
  maxy=m_ALandscape->SupplySimAreaHeight();
  m_TheMap = new uint32[ maxx*maxy ];

  Init(spref);
}
//-----------------------------------------------------------------------------

MovementMap::~MovementMap()
{
  delete[] m_TheMap;
}
//-----------------------------------------------------------------------------


void MovementMap::Init(int spref)
{

  for (int y=0; y<m_ALandscape->SupplySimAreaHeight(); y++)
  {
    for (int x=0; x<m_ALandscape->SupplySimAreaWidth(); x++)
    {
      uint32 colour=0;
	  if (spref==0)
	  {
		 // Beetle 
		 colour = beetle_tole_movemap_init(m_ALandscape, x, y);
	  }
      else if (spref==1) 
	  {  
		 // Partridge
		 colour = partridge_tole_movemap_init(m_ALandscape, x, y);
	  } 
	  else if (spref==2)
	  {  
		 // Erigone
		 colour = spider_tole_movemap_init(m_ALandscape, x, y);
	  }
	  SetMapValue(x,y,colour);
	}
  }
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue(unsigned x, unsigned y, unsigned value)
{

  uint32 theBit= x & 15;  // Identify the bit within 16 possibilities

  // Multiply by two
  theBit=theBit<<1;       // Double it because we use two bits per location

  // divide by 16
  // Calculate index:
  uint32 index= (x>>4)+(y*maxx); // maxx is the number of uints per row + x/16 because there are 16 locations per uint

  value &= 0x03;
  uint32 NewVal = m_TheMap[index] & ~( 0x03 << theBit ); // Make a hole two bits wide at the right location
  m_TheMap[index] = NewVal | (value << theBit); // Fill the hole

}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue0(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  theBit=theBit<<1; // Multiply by two
  // divide by 16
  uint32 index= (x>>4)+(y*maxx);
  m_TheMap[index]  &= ~(0x03<< theBit);
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue1(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  theBit=theBit<<1; // Multiply by two
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  m_TheMap[index]  &= ~(0x03 << theBit);
  m_TheMap[index]  |=  (0x01 << theBit);
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue2(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  // Multiply by two
  theBit=theBit<<1;
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  m_TheMap[index] &= ~(0x03 << theBit);
  m_TheMap[index]  |= (0x02 << theBit);
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue3(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  // Multiply by two
  theBit=theBit<<1;
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  m_TheMap[index]  |= (0x03 << theBit);
}
//-----------------------------------------------------------------------------


int MovementMap::GetMapValue(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  // Multiply by two
  theBit=theBit<<1;
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  return ((m_TheMap[index] >> theBit) & 0x03); // 0-3
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------

MovementMap16::MovementMap16(Landscape * L)
{
  m_ALandscape = L;
  // must make sure that we have whole words
  maxx=(m_ALandscape->SupplySimAreaWidth()+1)/2;
  maxy=m_ALandscape->SupplySimAreaHeight();
  m_TheMap = new uint32[ maxx*maxy ];

  Init();
}
//-----------------------------------------------------------------------------

MovementMap16::~MovementMap16()
{
  delete m_TheMap;
}
//-----------------------------------------------------------------------------


void MovementMap16::Init()
{

  for (int y=0; y<m_ALandscape->SupplySimAreaHeight(); y++)
  {
    for (int x=0; x<m_ALandscape->SupplySimAreaWidth(); x++)
    {
      SetMapValue(x,y,0);
    }
  }
}
//-----------------------------------------------------------------------------


void MovementMap16::SetMapValue(unsigned x, unsigned y, unsigned value)
{

  uint32 theWord= x & 1; // is the last bit set?

  // Multiply by 16o
  theWord=theWord<<4;

  // divide by 16
  // Calculate index:
  uint32 index= (x>>1)+(y*maxx);

  value &= 0xFF;
  uint32 NewVal = m_TheMap[index] & ~( 0xFFFF << theWord );
  m_TheMap[index] = NewVal | (value << theWord);

}
//-----------------------------------------------------------------------------


void MovementMap16::ClearMapValue(unsigned x, unsigned y)
{
  uint32 theWord=x & 1; // is the last bit set?
  theWord=theWord<<4; // Multiply by 16
  // divide by 2
  uint32 index= (x>>1)+(y*maxx); // y and x/2
  m_TheMap[index]  &= ~(0xFFFF<< theWord);
}
//-----------------------------------------------------------------------------

int MovementMap16::GetMapValue(unsigned x, unsigned y)
{
  uint32 theWord=x & 1;
  // Multiply by 16
  theWord=theWord<<4;
  // divide by 16
  uint32 index= (x>>1)+(maxx*y);
  return ((m_TheMap[index] >> theWord) & 0xFFFF); //
}
//-----------------------------------------------------------------------------

IDMapScaled::IDMapScaled(Landscape * L, int a_gridsize) : IDMap<TAnimal*>(L)
{
	m_scale = a_gridsize;
	maxx = (L->SupplySimAreaWidth() / m_scale) + 1;
	maxy = (L->SupplySimAreaHeight() / m_scale) + 1;
	m_TheMap.resize(maxx*maxy);
	for (int y = 0; y<maxy; y++)
	{
		for (int x = 0; x<maxx; x++)
		{
			SetMapValue(x, y, NULL);
		}
	}
}
//-----------------------------------------------------------------------------

IDMapScaled::~IDMapScaled()
{
	;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

/*

MovementMapBlitz::MovementMapBlitz(Landscape* L, int spref)
{
	m_ALandscape = L;
	Init(spref);
}

void MovementMapBlitz::Init(int spref)
{
	m_TheMap.resize(m_ALandscape->SupplySimAreaWidth(), m_ALandscape->SupplySimAreaHeight());
	for (int y = 0; y < m_ALandscape->SupplySimAreaHeight(); y++)
	{
		for (int x = 0; x < m_ALandscape->SupplySimAreaWidth(); x++)
		{
			short colour = 0;
			if (spref == 2)
			{  
				// Erigone
				colour = spider_tole_movemap_init(m_ALandscape, x, y);
			}
			SetMapValue(x, y, colour);
		}
	}
}
//-----------------------------------------------------------------------------
*/