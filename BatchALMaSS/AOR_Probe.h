#pragma once

typedef vector < int > ListOfCells;

class AOR_Probe
{
protected:
	ofstream m_ProbeFile;
	int m_gridcountwidth[4];
	int m_gridcountheight[4];
	int m_gridcountsize[4];
	int m_totalcells[4];
	ListOfCells m_gridcount[4];

	Landscape* m_TheLandscape;
	Population_Manager* m_owner;
public:
	AOR_Probe(Population_Manager* a_owner,Landscape* a_TheLandscape, string a_filename);
	virtual ~AOR_Probe() {
		CloseFile();   
	}

	virtual void CloseFile() {
		m_ProbeFile.close();
	}
	void WriteData();
	virtual void DoProbe(int a_lifestage);
};

class AOR_Probe_Goose : public AOR_Probe
{
protected:
public:
	AOR_Probe_Goose(Population_Manager* a_owner, Landscape* a_TheLandscape, string a_filename);
	void DoProbe(int a_lifestage) override;
};
class AOR_Probe_Beetle : public AOR_Probe
{
protected:
public:
    AOR_Probe_Beetle(Population_Manager* a_owner, Landscape* a_TheLandscape, string a_filename);
    void DoProbe(int a_lifestage) override;
};