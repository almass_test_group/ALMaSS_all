#include "clickablelabel.h"


ClickableLabel::ClickableLabel(QWidget* parent, Qt::WindowFlags f)
    : QLabel(parent) {
    
}

ClickableLabel::~ClickableLabel() {}

void ClickableLabel::mousePressEvent(QMouseEvent* event) {
    emit clicked(event);
}

void ClickableLabel::resizeEvent(QResizeEvent* event){
    Q_UNUSED(event);
    int w = (event->size()).width();
    int h = (event->size()).height();
    int min_size = std::min(w,h);
    resize(min_size, min_size);
}