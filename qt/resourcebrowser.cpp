#include "resourcebrowser.h"
#include "ui_resourcebrowser.h"

ResourceBrowser::ResourceBrowser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ResourceBrowser)
{
    ui->setupUi(this);
}

ResourceBrowser::~ResourceBrowser()
{
    delete ui;
}
