/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulationPopulationManager.cpp
\brief <B>The main source code for subpopulation population manager class</B>
*/
/**  \file SubPopulationPopulationManager.cpp
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"
#include "../OliveMoth/OliveMoth.h"
#include "../OliveMoth/OliveMoth_Population_Manager.h"

using namespace std;
using namespace blitz;


CfgStr cfg_OlivemothDevelopmentFile("OLIVEMOTH_DEVELOPMENT_FILE", CFG_CUSTOM, "Subpopulation/olivefly_info.txt");


//---------------------------------------------------------------------------

OliveMoth_Population_Manager::OliveMoth_Population_Manager(Landscape* L, int a_sub_w, int a_sub_h, int a_num_life_stage) : SubPopulation_Population_Manager(L, cfg_OlivemothDevelopmentFile.value(), a_sub_w, a_sub_h, a_num_life_stage)
{
	m_ListNames[0] = "Egg";
    m_ListNames[1] = "Larvae";
    m_ListNames[2] = "Pupae";
	m_ListNames[3] = "Female";
    m_ListNameLength = 4;
    m_SimulationName = "Olive Moth";
}