/* 
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#ifndef Goose_Barnacle_Base_BaseH
#define Goose_Barnacle_Base_BaseH

/**
\file
\brief
<B>BarnacleGoose_All.h This is the header file for the greylag goose classes</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of 8th February 2013 \n
*/
//---------------------------------------------------------------------------

class Goose_Base;
class Landscape;
class Goose_Population_Manager;

/** \brief
* A class to describe the Barnacle goose base
*/
class Goose_Barnacle_Base : public Goose_Base
{
public:
	/** \brief Goose_Barnacle_Base constructor */
	Goose_Barnacle_Base(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief Intitialise object */
	void Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief ReInit for object pool */
	void ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief Goose_Barnacle_Base destructor */
	virtual ~Goose_Barnacle_Base();
protected:
    /** \brief 
    Return to roost and assess whether to forage again that day 
    */
	TTypeOfGoose_BaseState st_ToRoost();
    /** \brief 
    Find the closest roost
    */
    virtual void ChangeRoost() { m_OurPopulationManager->FindClosestRoost(m_Location_x, m_Location_y, (unsigned) gs_Barnacle); }
	/** \brief
	Pick a hop location point within a_dist meters - must be overridden by descendent classes
	*/
	virtual APoint ChooseHopLoc();
};

/** \brief
* A class to describe the Barnacle family group
*/
class Goose_Barnacle_FamilyGroup : public Goose_Barnacle_Base
{
public:
   /** \brief Goose_Barnacle_FamilyGroup constructor */
	Goose_Barnacle_FamilyGroup(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost);
	/** \brief Intitialise object */
	void Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost);
	/** \brief ReInit for object pool */
	void ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost);
	/** \brief Goose_Barnacle_FamilyGroup destructor */
	~Goose_Barnacle_FamilyGroup() override;
   /** \brief The Goose_Barnacle_FamilyGroup Step*/
   void Step( ) override;
   /** \brief The FamilyGroup KillThis must be overridden for families */
   void KillThis() override;
};

/** \brief
* A class to describe the Barnacle non-breeder
*/
class Goose_Barnacle_NonBreeder : public Goose_Barnacle_Base
{
public:
   /** \brief Goose_Barnacle_NonBreeder constructor */
	Goose_Barnacle_NonBreeder(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief Intitialise object */
	void Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) override;
	/** \brief ReInit for object pool */
	void ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) override;
	/** \brief Goose_Barnacle_NonBreeder destructor */
	~Goose_Barnacle_NonBreeder() override;
   /** \brief The Goose_Barnacle_NonBreeder Step*/
   void Step( ) override;
};
#endif
