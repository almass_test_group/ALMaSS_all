/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulationAnimal.h
\brief <B>The main header code for base class of animal using sub-population method.</B>
*/
/**  \file SubPopulationAnimal.h
Version of  Feb. 2021 \n
By Xiaodong \n \n
*/

//---------------------------------------------------------------------------
#ifndef SubPopulationAnimalH
#define SubPopulationAnimalH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class SubPopulation;
class SubPopulation_Population_Manager;
//------------------------------------------------------------------------------

 enum SubPopulation_Object : int{
   tos_Egg=0,
   tos_Larva,
	tos_Pupa,
	tos_Female,
   tos_Male,
	count
} ;


/**
\brief
The class for base animal using subpopulation method.
*/
class SubPopulation : public TAnimal
{
private:
   /** \brief Flag to show whether it is an empty subpopulation cell. */
   bool m_empty_cell;
   /** \brief A unique ID number for this species */
   unsigned m_SpeciesID;
   /** \brief The array used for mortality calculation. */
   blitz::Array<double, 2> temp_mortality_array;
   

protected:
   /** \brief Variable to store the index of x in the population manager. */
   int m_index_x;
   /** \brief Variable to store the index of y in the population manager. */
   int m_index_y;
   /** \brief This is a time saving pointer to the correct population manager object */
   SubPopulation_Population_Manager*  m_OurPopulationManager;
   /** \brief Variable to store the weighted population density pointer*/
   double *m_popu_density;
   /** \brief Variable to store the suitability pointer*/
   double *m_suitability;
   /** \brief Variable to track the whole population in the current cell. */
   double m_whole_population_in_cell;
   /** \brief Array to hold the population number for each life stage. */
   blitz::Array<double, 1> m_population_each_life_stage;
   /** \brief Variables to record numbers of the animals at different life stages and age in canlander days */
   blitz::Array<double, 2> m_animal_num_array;
   /** \brief Tempraral variables for manipulting the animal number array. */
   blitz::Array<double, 2> m_temp_animal_num_array;

   /** \brief Array used for droping wings. */
   blitz::Array<double, 1> m_temp_droping_wings_array;
   /** \brief Array used for mortality. */
   blitz::Array<double, 1> m_died_number_each_life_stage;

public:
   /** \brief Subpopulation constructor */
   SubPopulation(int p_x, int p_y, Landscape* p_L, SubPopulation_Population_Manager* p_NPM, bool a_empty_flag, int a_index_x, int a_index_y, double* p_suitability=NULL, double* p_weight_density=NULL, int number=-1, int a_SpeciesID=999);
   virtual ~SubPopulation()=default;
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   unsigned getSpeciesID() { return m_SpeciesID; }
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   void setSpeciesID(unsigned a_spID) { m_SpeciesID = a_spID; }
   /** \brief Return the number of animals in a specific life stage in the cell */
   double getNumforOneLifeStage(int source_type);
   /** \brief Return totalpopulation in the cell */
   double getTotalSubpopulation(void) {return m_whole_population_in_cell;}
   /** \brief Add animal number for the given life stage at specific column (age) in the animal number array. */
   void addAnimalNumGivenStageColumn(int source_type, int a_column, double a_num);
   /** \brief Add animal number for the given life stage. */
   void addAnimalNumGivenStage(int source_type, blitz::Array<double, 1>* a_animal_num_array);
   /** \brief Get the numbers of the given life stage at different ages. */
   blitz::Array<double, 1> getArrayForOneLifeStage(int life_stage);
   /** \brief Function to calculate the weighted population density for the cell. */
   virtual void calPopuDensity(void){};
   /** \brief Function to calculate the suitability for the cell. */
   virtual void calSuitability(void){};
   virtual void Step();
   virtual void doDevelopment();
   virtual void doMovement() {};
   virtual void doReproduction();
   /** \brief For some specices, they drop the wings at some situation. */
   virtual void doDropingWings() {};
   virtual void doMortality();
   /** \brief This function is used to calculate the cell realted weight to multiply with the temperature and age based mortality rate. In this base class, it always return 1.*/
   virtual double calCellRelatedMortalityWeight() {return 1.0;};
};

#endif