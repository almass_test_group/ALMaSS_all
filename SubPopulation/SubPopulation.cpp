/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulation.cpp
\brief <B>The main source code for subpopulation base class</B>
*/
/**  \file SubPopulation.cpp
Version of  Feb 2021 \n
By Xiaodong Duan \n \n
*/

#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"
#include "../BatchALMaSS/ALMaSS_Random.h"

extern MapErrorMsg *g_msg;
using namespace std;
using namespace blitz;

SubPopulation::SubPopulation(int p_x, int p_y, Landscape* p_L, SubPopulation_Population_Manager* p_NPM, bool a_empty_flag, int a_index_x, int a_index_y, double* p_suitability, double* p_weight_density, int number, int a_SpeciesID) : TAnimal(p_x,p_y,p_L)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = p_NPM;
	m_SpeciesID = a_SpeciesID; // This can be any number since its just here to demonstrate how you might add some information
	m_animal_num_array.resize(m_OurPopulationManager->supplyLifeStageNum(),m_OurPopulationManager->supplyMaxColNum());
	m_animal_num_array = 0; //set it to be zero
	m_temp_animal_num_array.resize(m_OurPopulationManager->supplyLifeStageNum(),m_OurPopulationManager->supplyMaxColNum());
	temp_mortality_array.resize(m_OurPopulationManager->supplyLifeStageNum(),m_OurPopulationManager->supplyMaxColNum());
	m_temp_droping_wings_array.resize(m_OurPopulationManager->supplyMaxColNum());
	m_temp_droping_wings_array = 0.0;
	m_temp_animal_num_array = 0;
	m_empty_cell = a_empty_flag;
	m_popu_density = p_suitability;
	m_index_x = a_index_x;
	m_index_y = a_index_y;
	m_whole_population_in_cell = 0;
	m_population_each_life_stage.resize(m_OurPopulationManager->supplyLifeStageNum());
	m_population_each_life_stage = 0;
	m_died_number_each_life_stage.resize(m_OurPopulationManager->supplyLifeStageNum());
	m_died_number_each_life_stage = 0;
	//by default (-1), there is nothing to create
	if (number >0){
		addAnimalNumGivenStageColumn(0,0,number);
	}	
}

double SubPopulation::getNumforOneLifeStage(int source_type)
{
	return m_population_each_life_stage(source_type);
}

void SubPopulation::addAnimalNumGivenStageColumn(int source_type, int a_column, double a_num){
	m_animal_num_array(source_type, a_column) += a_num;
	if (m_animal_num_array(source_type, a_column)<0){
		m_animal_num_array(source_type, a_column) = 0;
	}
	//update the whole population in the cell
	m_whole_population_in_cell += a_num;
	if(m_whole_population_in_cell<0){
		m_whole_population_in_cell = 0;
	}
	//update the population for each life stage
	m_population_each_life_stage(source_type) += a_num;
	if(m_population_each_life_stage(source_type)<0){
		m_population_each_life_stage(source_type) = 0;
	}

	m_OurPopulationManager->updateWholePopulationArray(source_type, a_num);

}

void SubPopulation::addAnimalNumGivenStage(int source_type, blitz::Array<double, 1>* a_aphid_num_array){
	m_animal_num_array(source_type, Range::all())+=(*a_aphid_num_array)(Range::all());
	double temp_count = sum((*a_aphid_num_array)(Range::all()));
	m_whole_population_in_cell += temp_count;
	if(m_whole_population_in_cell < 0){
		m_whole_population_in_cell = 0.0;
	}
	m_OurPopulationManager->updateWholePopulationArray(source_type, temp_count);
}

void SubPopulation::Step(){
	// There is nothing, so do nothing.
	calPopuDensity();
	calSuitability();
	if (getTotalSubpopulation() <= 0) {
		m_empty_cell = true;
		return;
	}

	//update the suitability of the current cell
	//calPopuDensity();
	//calSuitability();
	doMortality();
	doDevelopment();
	doMovement();
	doReproduction();
	doDropingWings();
}

void SubPopulation::doDevelopment() {
	//grow up from yesterday or old enough to die
	for (int i = 0; i < m_OurPopulationManager->supplyLifeStageNum(); i++){
		//decide which life path should follow, if there is more than one
		int life_circle = 0;
		
		int old_enough_index = m_OurPopulationManager->supplyOldEnoughIndex(i);
		//check if old enough ones exit
		if (old_enough_index >= 0){
			int next_life_stage_index = m_OurPopulationManager->calNextStage(i);
			//check if they are old enough to the next stage or they are old enough to die
			if (next_life_stage_index > 0){							
				//m_animal_num_array(next_life_stage_index, m_OurPopulationManager->supplyNewestIndex(next_life_stage_index)) += m_animal_num_array(i, old_enough_index);
				addAnimalNumGivenStageColumn(next_life_stage_index, m_OurPopulationManager->supplyNewestIndex(next_life_stage_index), m_animal_num_array(i, old_enough_index));
				//check if it is the first time
				if (m_OurPopulationManager->supplyFirstFlagLifeStage(next_life_stage_index)){
					m_OurPopulationManager->setFirstFlagLifeStage(next_life_stage_index, false);
					m_OurPopulationManager->setOldIndex(next_life_stage_index, m_OurPopulationManager->supplyNewestIndex(next_life_stage_index));
				}

				//m_OurPopulationManager->updateWholePopulationArray(next_life_stage_index, m_animal_num_array(i, old_enough_index));
			}
			//m_OurPopulationManager->updateWholePopulationArray(i, -1*m_animal_num_array(i, old_enough_index));
			addAnimalNumGivenStageColumn(i, old_enough_index, -1*m_animal_num_array(i, old_enough_index));
			//they are too old, all die
			m_animal_num_array(i, old_enough_index) = 0;
		}
	}
	//mortality rate
}

blitz::Array<double, 1> SubPopulation:: getArrayForOneLifeStage(int life_stage){
	return m_animal_num_array(life_stage, blitz::Range::all());
}

void SubPopulation :: doMortality(){
	/*
	if (*m_popu_density < 1){
		double current_rand = g_rand_uni();
		double current_mortality = current_rand < *m_popu_density ? *m_popu_density : current_rand;
	if (*m_suitability < 1){
		double current_rand = g_rand_uni();
		double current_mortality = current_rand < *m_suitability ? *m_suitability : current_rand;
		m_animal_num_array *= current_mortality;
		m_died_number_each_life_stage = m_population_each_life_stage* (1-current_mortality);
		m_OurPopulationManager->updateWholePopulationArray(m_died_number_each_life_stage);
		m_population_each_life_stage -= m_died_number_each_life_stage;
		m_whole_population_in_cell *= current_mortality;		
	}
	*/

	/*
	//We generate a random number for each life stage at each age
	for (int i=0; i<m_OurPopulationManager->supplyLifeStageNum();i++){
		int j_end = m_OurPopulationManager->supplyMaxColNum();
		if (i==0) {
			j_end = 1;
		}
		for (int j=0; j<j_end;j++){
			// get the mortality rate
			double current_rand = g_rand_uni();
			double temp_mortality = m_OurPopulationManager->supplyMortality(i,j);
			double current_mortality = current_rand < temp_mortality ? current_rand : temp_mortality;

			//make them die
			int died_number = m_animal_num_array(i,j) * current_mortality;
			m_animal_num_array(i, j) -= died_number;
			m_OurPopulationManager->updateWholePopulationArray(i, died_number);
			m_whole_population_in_cell -= died_number;
		}
	}
	*/

	//first, we generate a random number
	//double current_rand = g_rand_uni();
	//get the whole mortality array and multiply with the cell realted weight
	temp_mortality_array = (m_OurPopulationManager->supplyMortalityWholeArray());
	//std::cout<<(m_OurPopulationManager->supplyMortalityWholeArray());
	//std::cout<<temp_mortality_array;
	temp_mortality_array *= calCellRelatedMortalityWeight();
	for (int i=0; i<m_OurPopulationManager->supplyLifeStageNum();i++){
		for (int j=0; j<m_OurPopulationManager->supplyMaxColNum();j++){
			// get the mortality rate
			double current_rand = g_rand_uni();
			double temp_mortality = temp_mortality_array(i,j);
			double current_mortality = current_rand < temp_mortality ? current_rand : temp_mortality;

			//make them die, note it is negative
			int died_number = -1*m_animal_num_array(i,j) * current_mortality;
			addAnimalNumGivenStageColumn(i,j, died_number);
			//m_animal_num_array(i, j) += died_number;
			//m_OurPopulationManager->updateWholePopulationArray(i, died_number);
			//m_whole_population_in_cell += died_number;
		}
	}
	//blitz::Array<double, 2> temp_mortality_vs_random_array = temp_mortality_array * blitz::cast(temp_mortality_array >= current_rand, double());
	//when the mortality rate is smaller than the random number, they will survive
	//blitz::firstIndex temp_i;
	//blitz::secondIndex temp_j;
	//temp_base_mortality_array(blitz::Range(temp_base_mortality_array<=current_rand))=0;
}


//In the base subpopulation class, only one way of reproduction: females -> 3 eggs per day. This function need rewriting for all the derived classes
void SubPopulation::doReproduction()
{
	//only do this when there is female
	if(m_population_each_life_stage(3)>0)
	{
		double temp_birth_egg_num = 3*m_population_each_life_stage(3);
		addAnimalNumGivenStageColumn(0, m_OurPopulationManager->supplyNewestIndex(0), temp_birth_egg_num);
	}
}