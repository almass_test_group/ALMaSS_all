#ifndef HBINCLUDES_H
#define HBINCLUDES_H


enum class WorkerTasks
{
    Move=0,
    CleanCells,
    CapBrood,
    TendBrood,
    TendQueen,
    ReceiveNectar,
    DepositNectar,
    RipenNectar,
    CapHoney,
    PackPollen,
    BuildComb,
    Ventillate,
    Guarding,
    CleaningHive,
    TestFlight,
    ForageNectar,
    ForagePollen
};


#endif // HBINCLUDES_H

