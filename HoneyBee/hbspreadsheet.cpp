#include "hbspreadsheet.h"

#include <iostream>

void Spreadsheet::load()
{
  _workerPriority=blitzLoadCSVint("ApisRAMstates_workerpriority.csv");
  _colonyPriority=blitzLoadCSVint("ApisRAMstates_colonypriority.csv");
  _constants=blitzLoadCSVdouble("ApisRAMstates_constants.csv");
}

IArray2D Spreadsheet::workerPriority()
{
  return * _workerPriority;
}

IArray2D Spreadsheet::colonyPriority()
{
  return * _colonyPriority;
}

IArray1D Spreadsheet::getWorkerPriorityLine(int age)
{
  return (*_workerPriority)(age,blitz::Range::all());
}

IArray1D Spreadsheet::getColonyPriorityLine(int l)
{
  return (*_colonyPriority)(l,blitz::Range::all());
}

double Spreadsheet::getConstant(Constants constant)
{
  return (*_constants)(constant+1,1);
}

void Spreadsheet::setGlandularFlags(IArray1D& glandularFlags, const JobData& jobdata)
{
  glandularFlags=1;
  glandularFlags(FEEDBROOD) = jobdata.beeHPGAvailable > getConstant(HPG_PER_FEED);
  bool gotWax = jobdata.beeWaxAvailable > getConstant(WAX_PER_JOB);
  glandularFlags(BUILDCOMB) = gotWax;
  glandularFlags(CAPBROOD) = gotWax;
  glandularFlags(CAPHONEY) = gotWax;
}

void Spreadsheet::setColonyFlags(IArray1D& colonyFlags, const JobData& jobdata)
{
  colonyFlags=1;
  /* outside temp must be warm enough and must be daytime */
  bool canFly = !((jobdata.ambientTemp < getConstant(LOWEST_TEMP_FOR_FLYING)) | !jobdata.isDaytime);
  colonyFlags(TESTFLIGHTS) = canFly;
  colonyFlags(FORAGENECTAR) = canFly;
  colonyFlags(FORAGEPOLLEN) = canFly;
}
  
void Spreadsheet::setLocalFlags(IArray1D& localFlags, const JobData& jobdata)
{
  localFlags=0;
  localFlags(jobdata.localFlag)=true;
}

int Spreadsheet::getJobNumber(const JobData& jobdata)
{
  IArray1D glandularFlags(NUMBER_OF_JOBS);
  IArray1D colonyFlags(NUMBER_OF_JOBS); 
  IArray1D localFlags(NUMBER_OF_JOBS);
  IArray1D jobMask(NUMBER_OF_JOBS);
  IArray1D jobPriorities(NUMBER_OF_JOBS);
  
  setGlandularFlags(glandularFlags, jobdata);
  setColonyFlags(colonyFlags, jobdata);
  setLocalFlags(localFlags, jobdata);
  
  jobMask = (localFlags && glandularFlags) || colonyFlags;
  return 1;
}


#ifdef HBSPREADSHEET_UNITTEST

int main()
{
  Spreadsheet ss = Spreadsheet();
  ss.load();

  JobData jd;
  jd.age=1;
  jd.isSummer=true;
  jd.isDaytime=true;
  jd.ambientTemp=9.0;
  jd.numberOfBees=5000;
  jd.sizeOfBrood=1000;
  jd.sugarPoolVolume=30.0;
  jd.nHoneyCells=5000;
  jd.nPollenCells=5000;
  jd.nBeesExternal=2000;
  jd.nFreeCells=40;
  jd.beeHPGAvailable=0.001;
  jd.beeWaxAvailable=0.001;
  jd.localFlag=FEEDBROOD;
    
  ss.getJobNumber(jd);
}
#endif
