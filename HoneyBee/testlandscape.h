#ifndef TESTLANDSCAPE_H
#define TESTLANDSCAPE_H

#include <blitz/array.h>

class TestLandscape
{
public:
    TestLandscape();
    blitz::Array<float,1> honeyA, pollenA;
    float decHoney, decPollen;

    float honey(int i)
    {
        float toReturn=honeyA(i);
        honeyA(i)-=decHoney;
        if (honeyA(i)<0)
        {
            honeyA(i)=0;
        }
        return toReturn;
    }

    float pollen(int i)
    {
        float toReturn=pollenA(i);
        pollenA(i)-=decPollen;
        if (pollenA(i) < 0)
        {
            pollenA(i)=0;
        }
        return toReturn;
    }

    int waitLength()
    {
        return rand() % 12;
    }
};


#endif // TESTLANDSCAPE_H
