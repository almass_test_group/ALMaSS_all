#include "waggle.h"

int main()
{
   HONEYBEE::Waggle w;
   std::cout << "hasDance() " << w.hasDance() << std::endl;
   w.addDance(5,250,0.3,10);
   std::cout << "hasDance() " << w.hasDance() << std::endl;
   w.addDance(6,260,0.4,10);
   std::cout << "hasDance() " << w.hasDance() << std::endl;
   w.addDance(7,270,0.9,11);
   w.addDance(15,650,0.2,23);
   w.addDance(6,260,0.99,10);
   w.addDance(6,310,0.1,15);
   for (auto d : w.dances)
   {
      std::cout << d << std::endl;
   }
   std::cout << "Have 15,65: " << w.haveDance(15,65) << std::endl;
   std::cout << "Have 5,65: " << w.haveDance(5,65) << std::endl;
   std::cout << "Dance Index 15,65: " << w.danceIndex(15,65) << std::endl;
   std::cout << "Fail: " << w.danceIndex(22,33) << std::endl;
   HONEYBEE::Waggle::Dance d = {15,65,0.5f,1};
   std::cout << "Dance Index 15,65: " << w.danceIndex(d) << std::endl;
   std::cout << "Best Reward: " << w.bestReward() << std::endl;
   std::cout << "Select Dance:\n" << w.selectDance()  << std::endl;

   w.tick();
   for (auto d : w.dances)
   {
      std::cout << d << std::endl;
   }
   w.tick();
   for (auto d : w.dances)
   {
      std::cout << d << std::endl;
   }
   std::cout << "Purging\n";
   w.purge();
   for (auto d : w.dances)
   {
      std::cout << d << std::endl;
   }
   
   return 0;
}
