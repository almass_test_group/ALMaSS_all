#include "waggle.h"

namespace HONEYBEE
{
   std::ostream& operator<<(std::ostream& os, const Waggle::Dance& d)
   {
      os << "Direction:            " << d.direction << std::endl;
      os << "Distance:             " << d.distance << std::endl;
      os << "Quality:              " << d.quality << std::endl;
      os << "Travel Time:          " << d.travelTime << std::endl;
      os << "Nectar Concentration: " << d.nectarConcentration << std::endl;
      os << "Pollen Variation:     " << d.pollenVariation << std::endl;
      os << "Number Of Dances:     " << d.ndances << std::endl;
      os << "Age:                  " << d.age << std::endl;
      return os;
   }
} // Namespace HONEYBEE

