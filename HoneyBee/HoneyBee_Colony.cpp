/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee_Colony.cpp
Version of  10th May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../HoneyBee/BeeVirusInfectionLevels.h"
#include "../HoneyBee/HoneyBee.h"
#include "../HoneyBee/HoneyBee_Colony.h"
#include "blitz/array.h"

//---------------------------------------------------------------------------------------
// Externals
extern MapErrorMsg *g_msg;

//---------------------------------------------------------------------------------------

// Configuration inputs - parameter default values
/** \brief The minimum time step considered for any bees */
CfgInt cfg_BeeMinimumTimeStep("HONEYBEE_MINTIMESTEP", CFG_CUSTOM, 30);
/** \brief The minimum time step considered for any bees */
CfgFloat cfg_EggDailyMortality("HONEYBEE_EGGDAILYMORT", CFG_CUSTOM, 0.001);
/** \brief The minimum time step considered for any bees */
CfgFloat cfg_LarvaDailyMortality("HONEYBEE_LARVADAILYMORT", CFG_CUSTOM, 0.001);
/** \brief The maximum sugar concentration */
CfgFloat cfg_MaxSugar("HONEYBEE_MAXSUGAR", CFG_CUSTOM, 40);
/** \brief The maximum pollen concentration */
CfgFloat cfg_MaxPollen("HONEYBEE_MAXPOLLEN", CFG_CUSTOM, 40);
/** \brief The basic needing of sugar per day */
CfgFloat cfg_BasicSugarDay("HONEYBEE_BASICSUGARDAY", CFG_CUSTOM, 3);
/** \brief The basic needing of pollen per day */
CfgFloat cfg_BasicPollenDay("HONEYBEE_BASICPOLLENDAY", CFG_CUSTOM, 1);

static CfgInt cfg_stepsize( "STEPSIZE", CFG_CUSTOM, 10 ); //step size in mins
static CfgInt cfg_eggsperstep( "EGGSPERSTEP", CFG_CUSTOM, 0); //step size in mins

//---------------------------------------------------------------------------------------
// Assign default static member values (these will be changed later).

unsigned HoneyBee_Base::m_ColonyTime = 0;
//double HoneyBee_WorkerEgg::m_EggDailyMortality = 0;
//double HoneyBee_WorkerEgg::m_LarvaDailyMortality = 0;

//---------------------------------------------------------------------------

HoneyBee_Colony::~HoneyBee_Colony (void)
{
    delete hive;
   // Should all be done by the Population_Manager destructor
}
//---------------------------------------------------------------------------

HoneyBee_Colony::HoneyBee_Colony(Landscape* L) : Population_Manager(L, 9)
{

    m_stepsize = cfg_stepsize.value();
    m_eggsperstep = cfg_eggsperstep.value();
    // Load List of Animal Classes
    m_ListNames[0] = "Worker Egg";
    m_ListNames[1] = "Worker Larva";
    m_ListNames[2] = "Worker Pupa";
    m_ListNames[3] = "Worker Adult";
    m_ListNames[4] = "Drone Egg";
    m_ListNames[5] = "Drone Larva";
    m_ListNames[6] = "Drone Pupa";
    m_ListNames[7] = "Drone Adult";
    m_ListNames[8] = "Queen";
    m_ListNameLength = 9;
    m_SimulationName = "HoneyBee";
	
    currentID=0;
    hive = new Hive(80,25,10,5.0,20.0,3.0);

    blitz::Array<float,3> xx = getHive()->getX();
    int xrange=xx.shape()[0];
    int yrange=xx.shape()[1];
    int zrange=xx.shape()[2];
    HoneyBee_Worker bee(0,0,0, m_TheLandscape,this);
    bee.m_pollen_concentration = cfg_MaxPollen.value();
    bee.m_sugar_concentration = cfg_MaxSugar.value();

//    waggleDance.addDance(5,25,.3);

    for (int i=0; i< 1000; i++) // This will need to be an input variable (config)
	{
        int temp_x = random(xrange);
        int temp_y = random(yrange);
        int temp_z = random(zrange);
        bee.SetX(temp_x);
        bee.SetY(temp_y);
        bee.SetZ(temp_z);
        bee.SetAge(23);
        bee.inHive = true;
        std::cout << bee.getX() << " " << bee.getY() << " " << bee.getZ() << std::endl;
        //hive->incBeeCount(temp_x, temp_y, temp_z);
        add<HoneyBee_Worker,HoneyBee_Worker>(bee);
    }


    HoneyBee_Queen bee_q(0,0,0,m_TheLandscape,this);
    bee_q.SetX(random(xrange));
    bee_q.SetY(random(yrange));
    bee_q.SetZ(random(zrange));
    bee_q.SetAge(23);
    bee_q.inHive = true;
    add<HoneyBee_Queen,HoneyBee_Queen>(bee_q);
    spreadsheet.load();
}


void HoneyBee_Colony::DoFirst()
{
	/**
	* This method does all housekeeping required by the population manager before the start of the next time-step.
	*
	* Currently this is:\n
	* - Updating colony time
	*/
    //dynamic_cast<HoneyBee_WorkerEgg*>(TheArray[ttohb_Worker][0])->AddColonyTime(cfg_BeeMinimumTimeStep.value());
    //layEggs();
    getHive()->step();
    //std::cout << getHive()->dayOfYear() << std::endl;
}

void HoneyBee_Colony::layEggs()
{
    blitz::Array<float,3> temp;
    temp.resize(hive->getTemperatureArray().shape());
    temp=blitz::where(hive->getCellTypeArray()==(int)CellType::Empty, hive->getTemperatureArray(), 0);

    int N=2;
    int x,y,z;

    struct_HoneyBee sp;
    sp.BPM = this;
    sp.L = m_TheLandscape;

    //sp->BPM = this;
    //sp->L = m_TheLandscape;

    HoneyBee_WorkerEgg egg(0,0,0,m_TheLandscape,this);
    for (int i=0,j=0; (i < N) && (j < 1000);j++)
    {
        blitz::TinyVector<int,3> inds;
        inds=blitz::maxIndex(temp);
        x=inds[0];
        y=inds[1];
        z=inds[2];
        //std::cout << "i,j: " << i << ", " << j << ", (x,y,z) = " << "(" << x << "," << y << "," << z << ")" << std::endl;


        if (hive->isFree(x,y,z))
        {
            egg.SetX(x);
            egg.SetY(y);
            egg.SetZ(z);
            //std::cout << "i,j: " << i << ", " << j << ", (x,y,z) = " << "(" << x << "," << y << "," << z << ") *being made*" << std::endl;
            //CreateObjects(ttohb_WorkerEgg,NULL,&sp,1);

            add<HoneyBee_WorkerEgg, HoneyBee_WorkerEgg>(egg);
            temp(x,y,z)=0.0;
            ++i;
        }
    }
    //std::cout << std::endl;
}

/*
blitz::TinyVector<int,3> HoneyBee_Colony::warmest(CellType celltype)
{
    blitz::Array<float,3> temp;
    temp.resize(hive->getTemperatureArray().shape());
    temp=blitz::where(hive->getCellTypeArray()==(int)celltype, hive->getTemperatureArray(), 0);
    blitz::TinyVector<int,3> inds;
    inds=blitz::maxIndex(temp);
    return inds;
}

*/

void HoneyBee_Colony :: AddAgesAll(){
    unsigned size2;
    unsigned size1 =  (unsigned) TheArray.size();
    for ( unsigned listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++) {
        dynamic_cast<HoneyBee_Base*>(TheArray[listindex][j])->AddAge();
        }
    }
}

void HoneyBee_Colony :: NextStageAll(){
    unsigned size2;
    unsigned size1 =  (unsigned) TheArray.size();
    for ( unsigned listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++) {
        HoneyBee_Base* current_bee = dynamic_cast<HoneyBee_Base*>(TheArray[listindex][j]);            
        if (current_bee->m_CurrentHBState != toHoneyBeeS_NextStage && current_bee->m_CurrentHBState !=toHoneyBeeS_Die)
        {
            if (g_rand_uni() < current_bee->mortality()){
                current_bee->m_CurrentHBState = toHoneyBeeS_Die;
                current_bee->st_Dying();
                int x = current_bee->Supply_m_Location_x();
                int y = current_bee->Supply_m_Location_y();
                int z = current_bee->Supply_m_Location_z();
                hive->decBeeCount(x,y,z);
                }
            if (current_bee->GetAge()>=current_bee->developmentTime)
            {
                current_bee->nextStage();
                current_bee->m_CurrentHBState = toHoneyBeeS_NextStage;
            }
        }
        }
    }
}

void HoneyBee_Colony :: DayStep(){
    AddAgesAll();
    NextStageAll();
}
