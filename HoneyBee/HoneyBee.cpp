/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee.cpp
    \brief <B>The main source code for all HoneyBee lifestage classes</B>
    Version of  10th May 2017 \n
    By Chris J. Topping \n \n
*/

#include <iostream>
#include <fstream>
#include <vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../HoneyBee/BeeVirusInfectionLevels.h"
#include "../HoneyBee/HoneyBee.h"
#include "../HoneyBee/HoneyBee_Colony.h"
#include "honeybeeconfigs.h"
#include "hbincludes.h"
#include "hbmath.h"
extern MapErrorMsg *g_msg;
extern Landscape* g_landscape_p;

//using namespace std;
#define M_PI 3.14159265358979323846264338327950288 

// Set to 144 for real simulation.
// Set smaller to make stage progression faster for testing
const int TEST_STEP_MULT=144;

inline void HoneyBee_Base::FillHBdata(struct_HoneyBee* a_data) {
   a_data->BPM = m_OurPopulationManager;
   a_data->L = m_OurLandscape;
   a_data->age = m_Age;
   a_data->x = m_Location_x;
   a_data->y = m_Location_y;
   a_data->z = m_Location_z;
   // a_data->BVI = &m_OurVirusInfection;
}

//******************************************************************************************************
//******************************* START HoneyBee_Base Class ********************************************
//******************************************************************************************************

HoneyBee_Base::HoneyBee_Base(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : TAnimal( a_x ,  a_y , p_L)
{
   // Assign the pointer to the population manager
   SetZ(a_z);
   developmentTime=999;
   id= m_OurPopulationManager->nextID();
   m_OurPopulationManager = p_BPM;
   m_CurrentHBState = toHoneyBeeS_InitialState;
   getHive()->incBeeCount(a_x,a_y,a_z);
   inHive=true;
}


void HoneyBee_Base::ReInit(const int  a_x, const int  a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   TAnimal::ReinitialiseObject(a_x,a_y,p_L);
   SetZ(a_z);
   m_Age = 0;
   m_CurrentStateNo=toHoneyBeeS_InitialState;
   m_CurrentHBState=toHoneyBeeS_InitialState;
   id = m_OurPopulationManager->nextID();
}

HoneyBee_Base::~HoneyBee_Base(void)
{
   getHive()->decBeeCount(getX(),getY(),getZ());
}

void HoneyBee_Base::Step(void)
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   switch (m_CurrentHBState)
   {
   case toHoneyBeeS_InitialState: // Initial state always starts with develop
      m_CurrentHBState = toHoneyBeeS_Develop;
      break;        
   case toHoneyBeeS_Develop:
      m_CurrentHBState=st_Develop(); // Will return movement or die
      m_StepDone=true;
      break;
   case toHoneyBeeS_Progress:
      m_CurrentHBState = st_Progress();
      m_StepDone = true;
      break;
   case toHoneyBeeS_Die:
      st_Dying(); // No return value - no behaviour after this
      m_StepDone = true;
      break;
   default:
      m_OurLandscape->Warn("HoneyBee_Base::Step()", "unknown state - default");
      exit(1);
   }
}

TTypeOfHoneyBeeState HoneyBee_Base::st_Develop()
{

   if (g_rand_uni() < mortality())
      return toHoneyBeeS_Die;
   else
   {
      //m_Age++;
      if (m_Age >= developmentTime*TEST_STEP_MULT) // Note: just for debug
	 return toHoneyBeeS_Progress;
      else
	 return toHoneyBeeS_Develop;
   }
   return toHoneyBeeS_Develop;
}

TTypeOfHoneyBeeState HoneyBee_Base::st_Progress(void)
{
   /**
      This creates a data object to pass on to the population manager to create a worker larva, then signals that this object should be destroyed.
   */
   nextStage();
   m_CurrentStateNo = -1;
   m_StepDone = true;
   return toHoneyBeeS_Die; // Not necessary, but for neatness
}

void HoneyBee_Base::st_Dying(void)
{
   m_CurrentStateNo = -1; // this will kill the animal object and free up space
   getHive()->setCellType(getX(),getY(),getZ(),(int)CellType::Empty);
}

Hive * HoneyBee_Base::getHive()
{
   return m_OurPopulationManager->getHive();
}

//******************************************************************************************************
//***************************** START HoneyBee_WorkerEgg Class *****************************************
//******************************************************************************************************

HoneyBee_WorkerEgg::HoneyBee_WorkerEgg(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_Base(a_x, a_y, a_z, p_L, p_BPM)
{
   developmentTime=3;
}

void HoneyBee_WorkerEgg::Step()
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   m_StepDone = true;
}



//******************************************************************************************************
//*************************** START HoneyBee_WorkerLarva Class *****************************************
//******************************************************************************************************
HoneyBee_WorkerLarva::HoneyBee_WorkerLarva(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerEgg(a_x, a_y, a_z, p_L, p_BPM)
{
   developmentTime=9;
}

void HoneyBee_WorkerLarva::ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}
void HoneyBee_WorkerLarva::Step()
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   m_StepDone = true;
}

/**
 * This is the development code for all honey bee worker larvae.
 * It has to:
 * - take care of daily mortality rates
 * - increase the physiological age based on hive temperature and humidity and food supply
 * - if large enough it must pupate
 *
 * \param[in] nothing
 * \return either a signal to pupate or to continue developing, or to die
 */


/////////////////////////////////////START HoneyBee_WorkerPupa////////////////////////////
HoneyBee_WorkerPupa::HoneyBee_WorkerPupa(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerLarva(a_x, a_y, a_z, p_L, p_BPM)
{
   developmentTime=21;
}

void HoneyBee_WorkerPupa::ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_WorkerPupa::Step()
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   m_StepDone = true;
}


HoneyBee_Worker::HoneyBee_Worker(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerPupa(a_x, a_y, a_z, p_L, p_BPM),
     moveSteps(10),stepCount(0),forageCounter{0}

{
   developmentTime=999999;
}

void HoneyBee_Worker::ReInit(const int  a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_MoveRandom()
{    
   //return toHoneyBeeS_Decide;
   std::array<int,3> p = getHive()->randomNeighbour(getX(),getY(),getZ());
   getHive()->decBeeCount(getX(),getY(),getZ());
   SetX(p[0]);
   SetY(p[1]);
   SetZ(p[2]);
   getHive()->incBeeCount(p[0],p[1],p[2]);
   stepCount++;
   if (stepCount >= moveSteps)
   {
      m_StepDone=true;
   }
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_Decide()
{
   m_StepDone=true;
   return toHoneyBeeS_ForagePollen;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ForagePollen()
{
//    TestLandscape& tl=m_OurPopulationManager->testlandscape;
   if (forageCounter==0)
   {
      forageCounter=10;
      inHive=false;
   }

   forageCounter--;
   m_StepDone=true;

   if (forageCounter==0)
   {
      //int day=m_OurLandscape->SupplyDayInYear();
      //forageAmount=tl.decPollen;
      //forageAmount=10;
      return toHoneyBeeS_StorePollen;
   }
   else
   {
      return toHoneyBeeS_ForagePollen;
   }
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ScoutPollen()
{
   std::cout << "Scout pollen\n";
   return toHoneyBeeS_ScoutPollen;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_StorePollen()
{
   const int forageAmount=10;
   getHive()->incSugarPool(forageAmount);
   m_StepDone=true;
   inHive=true;
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ForageNectar()
{
   // We are not out foraging
   if (forageCounter==0)
   {
      std::cout << "Send off foraging\n";
      auto wag = getHive()->waggleDance;

      if (wag.hasDance())
      {
	 inHive=false;
	 std::cout << "Send off foraging from dance\n";
	 int best = wag.bestReward();
	 dance = wag.dances[best];
	 std::cout << dance << std::endl;

	 float th = dance.direction/(360.0)*2.0*M_PI;
	 int x = dance.distance*cos(th);
	 int y = dance.distance*sin(th); 
	 SetX(getHive()->hivex + x);
	 SetY(getHive()->hivey + y);	 
	 m_StepDone=true;
	 constexpr int flightSpeed = 25; // kmh
	 constexpr float stepSpeed = flightSpeed/60.0*10.0;
	 forageCounter=dance.distance*stepSpeed/1000.0; // Model steps
	 std::cout << "N Steps to forage: " << forageCounter << std::endl;
      }
      else
      {
	 inHive=false;
	 const int segs = 5; // degrees
	 scoutDirection=rand()%(360/segs)*segs;
	 SetX(getHive()->hivex);
	 SetY(getHive()->hivey);
	 std::cout << "Send off scouting\n";
	 return toHoneyBeeS_ScoutNectar;
      }
   }

   // We are out foraging
   forageCounter--;
   m_StepDone=true;
   
   // We've got back from foraging
   if (forageCounter==0)
   {
      std::cout << "Honey I'm home" << std::endl;
      const double forageAmount=10;
      getHive()->incSugarPool(forageAmount,dance.nectarConcentration);
      inHive=true;
      return toHoneyBeeS_Decide;
   }
   return toHoneyBeeS_ForageNectar;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ScoutNectar()
{

   int hivex = getHive()->hivex;
   int hivey = getHive()->hivey;
   const int segs = 5; // degrees
   float scoutDirection=rand()%(360/segs)*segs;
   float th = scoutDirection/360.0f * 2.0 * M_PI;
   float scoutRange = 40.0; // meters (i.e. 4 km)
   int edgex = hivex + scoutRange * cos(th);
   int edgey = hivey + scoutRange * sin(th);
   auto line = generateLine(hivex,hivey,edgex,edgey);
   for (auto xy : line)
   {
      auto nectar = m_OurLandscape->SupplyNectar(xy.first, xy.second);
      double quality = nectar.m_quality;
      double quantity = nectar.m_quantity;
      std::cout << "Nectar: " << xy.first << " " << xy.second
		<< " " << quality << " " << quantity << std::endl;
   }
  
   //  std::cout << "Scout nectar: " << getX() << " " << getY() << std::endl;
   SetX(getX()+1);
   SetY(getY()+1);
   //std::cout << "Scout nectar: " << getX() << " " << getY() << std::endl;
   m_StepDone=true;
   return toHoneyBeeS_ScoutNectar;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_StoreNectar()
{
   const double forageAmount=10;
   getHive()->decSugarPool(forageAmount);
   m_StepDone=true;
   inHive=true;
   return toHoneyBeeS_Decide;
}

void HoneyBee_Worker::Step(void)
{
   // consuming sugar and pollen
   m_sugar_concentration -= 0.01;
   m_pollen_concentration -= 0.01;
   if (m_StepDone || m_CurrentStateNo == -1) return;
   switch (m_CurrentHBState)
   {
   case toHoneyBeeS_InitialState: // Initial state always starts with develop
      //m_CurrentHBState = toHoneyBeeS_ForageNectar;
//        m_CurrentHBState = toHoneyBeeS_Move;
      m_StepDone = true;
      break;
   case toHoneyBeeS_Move:
      m_CurrentHBState=st_MoveRandom(); // Will return movement or die
      break;
   case toHoneyBeeS_Decide:
      m_CurrentHBState=st_Decide();
      break;
   case toHoneyBeeS_ForageNectar:
      m_CurrentHBState=st_ForageNectar();
      break;
   case toHoneyBeeS_ScoutNectar:
      m_CurrentHBState=st_ScoutNectar();
      break;
   case toHoneyBeeS_StoreNectar:
      m_CurrentHBState=st_StoreNectar();
      break;
   case toHoneyBeeS_ForagePollen:
      m_CurrentHBState=st_ForagePollen();
      break;
   case toHoneyBeeS_StorePollen:
      m_CurrentHBState=st_StorePollen();
      break;
   default:
      m_OurLandscape->Warn("HoneyBee_Worker::Step()", "unknown state - default");
      exit(1);
   }
}

void HoneyBee_Worker::BeginStep()
{
   metaboliseAndEat();
}

void HoneyBee_Worker::metaboliseAndEat()
{

   // Consume sugar
   double sugarConsumed=metabolicRate*cfg_hb_sugar_per_metabolic_rate.value();
   sugar-=sugarConsumed;
   // Eat sugar from pool
   getHive()->decSugarPool(sugarConsumed);

   //protein-=currentProteinUsage;
   // getHive()->dec

}


//******************************************************************************************************
//***************************** START HoneyBee_DroneEgg Class *****************************************
//******************************************************************************************************

HoneyBee_DroneEgg::HoneyBee_DroneEgg(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_Base(a_x, a_y, a_z, p_L, p_BPM)
{
   developmentTime=3;
}

void HoneyBee_DroneEgg::Step()
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   m_StepDone = true;
}

//******************************************************************************************************
//*************************** START HoneyBee_DroneLarva Class *****************************************
//******************************************************************************************************
HoneyBee_DroneLarva::HoneyBee_DroneLarva(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_DroneEgg(a_x, a_y, a_z, p_L, p_BPM)
{
   developmentTime=9;
}

void HoneyBee_DroneLarva::ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}
void HoneyBee_DroneLarva::Step()
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   m_StepDone = true;
}

//******************************************************************************************************
//*************************** START HoneyBee_DronePupa Class *****************************************
//******************************************************************************************************

HoneyBee_DronePupa::HoneyBee_DronePupa(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_DroneLarva(a_x, a_y, a_z, p_L, p_BPM)
{
   developmentTime=21;
}

void HoneyBee_DronePupa::ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_DronePupa::Step()
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   m_StepDone = true;
}

//******************************************************************************************************
//*************************** START HoneyBee_Drone Class *****************************************
//******************************************************************************************************

HoneyBee_Drone::HoneyBee_Drone(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_DronePupa(a_x, a_y, a_z, p_L, p_BPM)
{
   developmentTime=999999;
}

void HoneyBee_Drone::ReInit(const int  a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_Drone::Step()
{
   if (m_StepDone || m_CurrentStateNo == -1) return;
   m_StepDone = true;
}

/////////////////////////Queen//////////////////////

HoneyBee_Queen::HoneyBee_Queen(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerPupa(a_x, a_y, a_z, p_L, p_BPM),
     moveSteps(10),stepCount(0),forageCounter{0}

{
   developmentTime = 999999;
}

void HoneyBee_Queen::ReInit(const int  a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);

}

void HoneyBee_Queen::LayEggs(){
   blitz::Array<float,3> xx = m_OurPopulationManager->getHive()->getX();
   int xrange=xx.shape()[0];
   int yrange=xx.shape()[1];
   int zrange=xx.shape()[2];
   HoneyBee_WorkerEgg egg(0,0,0, g_landscape_p,m_OurPopulationManager);
   HoneyBee_DroneEgg egg_d(0,0,0, g_landscape_p,m_OurPopulationManager);

   for (int i=0; i< m_OurPopulationManager->m_eggsperstep; i++) // This will need to be an input variable (config)
   {
      if (g_rand_uni()>0.05 ){
      egg.SetX(random(xrange));
      egg.SetY(random(yrange));
      egg.SetZ(random(zrange));
      egg.SetAge(0);
      while(!(m_OurPopulationManager->hive->isFree(egg.getX(),egg.getY(),egg.getZ()))){
         egg.SetX(random(xrange));
         egg.SetY(random(yrange));
         egg.SetZ(random(zrange));
         }  
      m_OurPopulationManager->add<HoneyBee_WorkerEgg, HoneyBee_WorkerEgg>(egg);}
      else{
         egg_d.SetX(random(xrange));
         egg_d.SetY(random(yrange));
         egg_d.SetZ(random(zrange));
         egg_d.SetAge(0);
         while(!(m_OurPopulationManager->hive->isFree(egg_d.getX(),egg_d.getY(),egg_d.getZ()))){
            egg_d.SetX(random(xrange));
            egg_d.SetY(random(yrange));
            egg_d.SetZ(random(zrange));
            }  
         m_OurPopulationManager->add<HoneyBee_DroneEgg, HoneyBee_DroneEgg>(egg_d);
      }
     }

}

void HoneyBee_Queen::Step(){
   std::cout << "Queen step\n";
   if (m_StepDone || m_CurrentStateNo == -1) return;
   bool should_lay = false;

   //need to add calculation whether laying egg or not

   if (should_lay) LayEggs();
   
   m_StepDone = true;
}
