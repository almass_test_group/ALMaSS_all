#ifndef HONEYBEETYPES

//------------------------------------------------------------------------------
/**
Used for the population manager's list of honey bee life types
*/
enum TTypesOfHoneyBee
{
    ttohb_WorkerEgg = 0,
    ttohb_WorkerLarva,
    ttohb_WorkerPupa,
    ttohb_DroneEgg,
    ttohb_DroneLarva,
    ttohb_DronePupa,
    ttohb_Worker,
    ttohb_Forager,
    ttohb_Scout,
    ttohb_Drone,
    ttohb_Queen
};
//---------------------------------------------------------------------------

#define HONEYBEETYPES

#endif // HONEYBEETYPES

