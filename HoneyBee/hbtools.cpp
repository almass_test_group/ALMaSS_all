#include "hbtools.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <blitz/array.h>
#include <memory>
#include "../Landscape/MapErrorMsg.h"

#define STANDALONE

using Array2D = blitz::Array<double,2>;

//From: https://stackoverflow.com/questions/447206/c-isfloat-function
bool isFloat(std::string myString)
{
    std::istringstream iss(myString);
    float f;
    iss >> std::noskipws >> f; // noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail();
}

//From: https://stackoverflow.com/questions/447206/c-isfloat-function
bool isInt(std::string myString)
{
    std::istringstream iss(myString);
    int i;
    iss >> std::noskipws >> i; // noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail();
}

std::vector<std::string> split(std::string input, char delimiter)
{
    std::vector<std::string> r;
    std::istringstream ss(input);
    std::string token;

    while(std::getline(ss,token,delimiter))
    {
        r.push_back(token);
    }
    return r;
}

std::vector<std::vector<std::string> > loadCSV(std::string filename)
{
    std::vector<std::vector<std::string> > r;
    std::ifstream f(filename);
    if (!f)
    {
#ifndef STANDALONE
      g_msg->Warn("ApisRAM spreadsheet not found.",0);
      exit(1);
#endif
    }
    std::string line;
    while (std::getline(f,line))
    {
        r.push_back(split(line,','));
    }
    return r;
}

blitz::Array<int,2> * blitzLoadCSVint(std::string filename, int nodataValue)
{
    std::vector<std::vector<std::string> > csv;
    csv=loadCSV(filename);
    int X,Y;
    X=csv.size();
    Y=csv[0].size();
    blitz::Array<int,2> * r = new blitz::Array<int,2>;
    r->resize(X,Y);

    // Note: There may be a bug here if the spreadsheet has blank lines.
    // Needs fixing.
    for (int x=0; x < X; ++x)
    {
        for (int y=0; y < Y; ++y)
        {
            std::string s=csv[x][y];
            if (isInt(s))
            {
                (*r)(x,y)=atoi(s.c_str());
            }
            else
            {
                (*r)(x,y)=nodataValue;
            }
        }
    }
    return r;
}

blitz::Array<double,2> * blitzLoadCSVdouble(std::string filename, double nodataValue)
{
    std::vector<std::vector<std::string> > csv;
    csv=loadCSV(filename);
    int X,Y;
    X=csv.size();
    Y=csv[0].size();
    blitz::Array<double,2> * r = new blitz::Array<double,2>;
    r->resize(X,Y);

    // Note: There may be a bug here if the spreadsheet has blank lines.
    // Needs fixing.
    for (int x=0; x < X; ++x)
    {
        for (int y=0; y < Y; ++y)
        {
            std::string s=csv[x][y];
            if (isFloat(s))
            {
                (*r)(x,y)=atof(s.c_str());
            }
            else
            {
                (*r)(x,y)=nodataValue;
            }
        }
    }
    return r;
}

//#define __TESTMAIN
#if defined __TESTMAIN
int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cout << "Usage: csv2blitz filename" << std::endl;
        exit(1);
    }

    char * filename = argv[1];
    double nodata=atof(argv[2]);

    std::vector<std::vector<std::string>> csv = loadCSV(filename);

    for (int i=0; i< (int)csv.size();++i)
    {
        for (int j = 0; j < (int)csv[i].size(); ++j)
        {
            std::cout << csv[i][j] << ", ";
        }
        std::cout << std::endl;
    }

    std::unique_ptr<blitz::Array<double,2> > csvb(blitzLoadCSV(filename));
    std::cout << *csvb;
}
#endif


