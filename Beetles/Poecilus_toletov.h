//
// Created by andrey on 2/5/21.
//

#ifndef ALMASS_POECILUS_TOLETOV_H
#define ALMASS_POECILUS_TOLETOV_H
#include <Landscape/ls.h>
# include <unordered_set>
#include <utility>
using namespace std;
using TToleList=std::unordered_set<TTypesOfLandscapeElement>;
using TTovList=std::unordered_set<TTypesOfVegetation> ;
class PoecilusTovParams{
public:
    TTovList getList();
    explicit PoecilusTovParams(TTovList);
private:
    TTovList PoecilusTovList;
};
class PoecilusToleParams{
public:
    TToleList getList();
    explicit PoecilusToleParams(TToleList);
private:
    TToleList PoecilusToleList;
};
typedef struct PoecilusToleTovs{
    PoecilusToleParams PoecilusStartHabitats{TToleList{
            tole_Field,
            tole_Orchard,
            tole_Vineyard,
            tole_PermPasture,
            tole_PermPastureLowYield,
            tole_PermPastureTussocky,
            tole_RoadsideVerge,
            tole_NaturalGrassDry,
            tole_NaturalGrassWet,
            tole_FieldBoundary,
            tole_UnsprayedFieldMargin,
            tole_YoungForest,
            tole_WaterBufferZone
    }};
    PoecilusTovParams PoecilusSuitableForHibernation{TTovList{
            tov_Wasteland,
            tov_PermanentGrassGrazed,
            tov_PermanentGrassLowYield, // 35
            tov_PermanentGrassTussocky, // 35
            tov_OPermanentGrassGrazed, // 35
            tov_NaturalGrass, // 110
            tov_Heath,
            tov_PermanentSetAside,
            tov_OSetAside,
            tov_WaterBufferZone,
            tov_NLPermanentGrassGrazed
    }};
    PoecilusToleParams PoecilusReproductionLandscape{TToleList{
            tole_Field,
            tole_Orchard,
            tole_Vineyard,
            tole_UnsprayedFieldMargin
    }};
    PoecilusToleParams PoecilusHalfReproductionLandscape{TToleList{
            tole_PermPasture,
            tole_PermPastureLowYield,
            tole_PermPastureTussocky,
            tole_RoadsideVerge,
            tole_NaturalGrassDry, // 110
            tole_NaturalGrassWet,
            tole_YoungForest,
            tole_MownGrass,
            tole_BeetleBank,
            tole_WaterBufferZone,
            tole_FieldBoundary
    }};
} TPoecilusToleTovs;

int poecilus_tole_movemap_init(Landscape* m_OurLandscape, int x, int y);

#endif //ALMASS_POECILUS_TOLETOV_H
