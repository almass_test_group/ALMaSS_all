//
// Version of May, 31, 2020
// Last modified by Andrey Chuhutin on 12/02/2020
/*
*******************************************************************************************************
Copyright (c) 2020, Andrey Chuhutin, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************
*/
//----------------------------------------------------------------------
// Created by Andrey Chuhutin on 29/09/2020.
//
//---------------------------------------------------------------------------

#ifndef Ladybird_allH
#define Ladybird_allH

//---------------------------------------------------------------------------
#include <blitz/array.h>
#include <blitz/numinquire.h>
#include "Beetle_BaseClasses.h"
#include "Aphids_debug/AphidsD_all.h"
#include <bitset>
#include "BatchALMaSS/ALMaSS_Random.h"
/** The size of the "memory" of hibernaculae in years*/
#define HIBERNACULA_MEMORY 5
extern const char* SimulationName;


//------------------------------------------------------------------------------
// Forward Declarations

class Ladybird_Population_Manager;
class Landscape;
class MovementMap;
class SimplePositionMap;

class probability_distribution;


//------------------------------------------------------------------------------
/* Not used
class ListOfPositions
{
 public:
  int locs[2][30];
  int NoLocs;
};
//------------------------------------------------------------------------------
*/

//------------------------------------------------------------------------------

/** \brief A data class for Ladybird data */

 class  struct_Ladybird : public struct_Beetle
{
 public:
  bool can_Reproduce{false};
  int DateMade{0};
  int eggs_developed{0};
  struct_Ladybird()= default;
};
const int LadybirdStagesNumberInDensityMap{8};
//------------------------------------------------------------------------------
/**
\brief The class describing the constants specific to Ladybird

 Some of these constants will be moved to use the configurator. When this happens both the definition and the initialisation will change.
*/
class LadybirdConstantClass{
public:
    /**
    * The map that defines which types of Ladybird can be cannibalised
    * Eggs and larva can be cannibalised, but not pupa or adult
    * */
    std::map<int, bool> CanBeCannibalised { {0, true}, {1, true}, {2, false}, {3, false}};
    /** number of stages */
    const int LadybirdLarvalStagesNum;
    /** the length of the earliest day of emergence*/
    const int LadybirdEmergingDayLength;
    /** chance of emergence in particular day after the earliest date*/
    const int LadybirdEmergingChance;
    /** maximum size of overwintering clump which is allowed */
    const int LadybirdMaxClumpSize;
    /** minimum size of overwintering clump which is allowed */
    //todo: check if it shouldn't be used: delete if not useful
    const int LadybirdMinClumpSize;
    /** Ladybird's Lower Developmental Thresholds*/
    const std::array<double,7> LadybirdLDTs{
            11.5, /** Egg */
            13.8, /** 1st instar */
            13.6, /** 2nd instar */
            13.6, /** 3rd instar */
            13.9, /** 4th instar */
            12.9, /** Pupa */
            12.4 /** Adult */

    };
    /**Prey density factors The variable that connects with prey density and amount of maximum eggs developed */
    const std::array<double,6> LadybirdPDf{
            0, /** Prey level 1*/
            0.5,
            0.82,
            0.89,
            0.98,
            1 /** Prey level 6 */
    };
    /** Temperature-related eggs per day */
    const std::array<double,7> LadybirdEPD{
        0, /** T<12.5 */
        7, /** 12.5<T<17.5 */
        21, /** 17.5<T<22.5 */
        39, /** 22.5<T<27.5 */
        28, /** 27.5<T<32.5 */
        20, /** 32.5<T<37.5 */
        0 /** T> 37.5 */
    };
    /** The minimum number of Aphids ranges for development length factorisation */
    const std::array<int, 4> LadybirdInstarMinAphidFactor{
        10, /**1st instar*/
        15,
        20,
        40 /**4th instar*/
    };
    const std::array<int, 4> LadybirdInstarMaxAphidFactor{
            35, /**1st instar*/
            40,
            70, // originally in paper 80
            140 /**4th instar*/ // originally in paper 150
    };
    const std::array<std::array <double, 6>,4> LadybirdInstarAphidDevelFactor{
            {{1.61538461538462, 1.46153846153846, 1.26923076923077, 1.11538461538462, 1.03846153846154, 1}, // 1st instar
             {1.77777777777778, 1.5, 1.33333333333333, 1.22222222222222, 1.11111111111111, 1}, // 2 instar
             {1.8, 1.55, 1.3, 1.1, 1.05, 1},
             {1.66666666666667, 1.42857142857143, 1.28571428571429, 1.0952380952381, 1.04761904761905, 1}} // 4th instar
    };

    const std::array<double,6> LadybirdAphidNumberEggFactor{
        0, 0.5, 0.82, 0.89, 0.98, 1
    };
    const int LadybirdAphidDensityToStartReproduction{335};/** the density of aphids that when first encountered will trigger moving to the reproduction state
                                                        * NOTE: the density is not absolute but in sq cm of leaf cover per aphid  */
    const int LadybirdAphidDensityToStartForaging{2550};/** the density of aphids that when first encountered will trigger moving to the reproduction state
                                                        * NOTE: the density is not absolute but in sq cm of leaf cover per aphid  */
    const double LadybirdLarvaMovementThreshold{-10};
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyEggMortality;

    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyLarvaeMortality;

    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyPupaeMortality;

    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyAdultMortality;

    const std::array<double,7> LadybirdMaxTemps{ /** Ladybird's Maximum Temperatures that each stage can survive*/
            40., /** Egg */
            40., /** 1st instar */
            40., /** 2nd instar */
            40., /** 3rd instar */
            40., /** 4th instar */
            40., /** Pupa */
            40. /** Adult */

    };
    /** Ladybird's Minimum Temperatures that each stage can survive*/
    const std::array<double,7> LadybirdMinTemps{
            -27., /** Egg */
            -20., /** 1st instar */
            -20., /** 2nd instar */
            -18., /** 3rd instar */
            -9., /** 4th instar */
            -19., /** Pupa */
            -20. /** Adult */

    };

    const std::array<double, 7> LadybirdThresholdDD{
        42,  /** Egg */
        22.8,  /** 1st instar */
        20.4, /** 2nd instar */
        23.1, /** 3rd instar */
        38, /** 4th instar */
        63.6, /** Pupa */
        151.9 /** Adult */
    };
    /** The currency conversion: cannibalism in aphids */
    const std::array<double, 7>LadybirdCannibalismCost{
        10, /** Egg price in Aphids */
        20, /** L1 price in Aphids */
        30, /** L2 price in Aphids */
        40, /** L3 price in Aphids */
        50, /** L4 price in Aphids */
        60, /** Pupa price in Aphids: never used-- pupae are edible */
        70  /** Adult price in Aphids: never used-- Adults are not edible */
    };
    /** The base daily appetite factor for different stages in aphids
     * to get the number of aphids it should be multiplied by age in days
     * */
    const std::array<double, 7>LadybirdAppetiteFactor{
            0, /** Egg does not eat */
            5, /** L1 appetite factor in Aphids */
            5, /** L2 appetite factor in Aphids */
            7, /** L3 appetite factor in Aphids */
            7, /** L4 appetite factor in Aphids */
            0, /** Pupa does not eat */
            8  /** Adult appetite factor  */
    };
    const std::array<double, 12> Ladybird_SCP{
        -15, // January
        -15,
        -10,
        -9,
        -5,
        -5,
        -5,
        -5,
        -5,
        -9,
        -12,
        -17 // December
    };
    /** Threshold temperatures for mortalities: Min **/
    const double LadybirdMortalityTempsMin{10};
    /** Threshold temperatures for mortalities: Max **/
    const double LadybirdMortalityTempsMax{35};
    /** Threshold temperatures for mortalities: Step **/
    const double LadybirdMortalityTempsStep{5.0};
    const double LadybirdFlyingThreshTemp{1.0}; //todo: remove after debugging 7 Celsius?
    const double LadybirdFlyingThreshWind{20.0};
    const int InitialHibernaculaeNumber{500};
    const std::array<double, 7>LadybirdEggTMortality{0.034231321839081,0.030215053763441,0.029655172413793,0.039,0.06304347826087, 0.158125};
    const std::array<double, 7>LadybirdPupaTMortality{0.018996015936255, 0.018292682926829, 0.014158415841584, 0.02859649122807, 0.058888888888889, 0.085263157894737};
    std::unique_ptr<const CfgBool> p_cfg_KillOnNyEve;
    //todo: check if it shouldn't be used: delete if not useful
    /** the map that defines whether the step is finished
     * after we changed state into a specific beetle state */
    std::unique_ptr<std::map<TTypesOfBeetleState, bool>> LadybirdMapOfStepDone;
    std::unique_ptr<const CfgInt> p_cfg_ladybirdstartnos;
    std::unique_ptr<const CfgInt> p_cfg_ladybirdstarteggsno;
    std::unique_ptr<const CfgInt> p_cfg_LadybirdOvipositionPeriodStart;
    std::unique_ptr<const CfgInt> p_cfg_LadybirdOvipositionPeriodEnd;
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdAphidToEggRatio;
    std::unique_ptr<const CfgInt> p_cfg_LadybirdDailyEggMax;
    /**\brief Max distance to travel in one day (in short jumps)*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxDistance;
    /**\brief Max length of a long range flight*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxLongRangeFlight;
    /**Max distance to travel in one day for larva (in moves)*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxLarvaDistance;
    std::unique_ptr<const CfgInt> p_cfg_LadybirdAphidDemand;
    /** distance at which a ladybird senses aphids
    * this value is used to guide the movement of the ladybird
    * from the low concentration to the high concentration */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdAphidSensingDistance;
    /** distance at which a ladybird larva senses aphids*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaAphidSensingDistance;
    /** Max distance of a single hop*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxHoppingDistance;
    /** Mean distance of a single hop*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMeanHoppingDistance;
    /** Std distance of a single hop*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdStdHoppingDistance;
    /** Mean distance of a single hop: Larva*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaMeanHoppingDistance;
    /** Std distance of a single hop: Larva*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaStdHoppingDistance;
    /** Max distance of a single hop*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaMaxHoppingDistance;
    /**Max number of attempts to hop short distance before hopping long*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxShortRangeAttempts;
    /**Max number of attempts to hop long distance before dying*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxLongRangeAttempts;
    /**Max Total number of laid eggs*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdTotalEggsMax;
    /** defines whether the ladybird is uniseasonal*/
    std::unique_ptr<const CfgBool> p_cfg_isLadybirdUniSeasonal;
    /** for non-uniseasonal the chance to die each year */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdYearlyMortality; //todo: remove me
    /** min Extreme temperature. Min temperature below that causes mass mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdExtremeTempMin;
    /** min Extreme temperature. Min temperature below that causes mass mortality while hibernating (factor on top of a regular SCP) */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdExtremeTempMinHibernatingFactor;
    /** max Extreme temperature. Max temperture above that causes mass mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdExtremeTempMax;
    /** Maximum number of short range moves in an attempt to hibernate*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxHibernationTries;
    /** Maturation period: minimum period between the emergence and oviposition (in days)*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMinMaturation;
    /** Max Aphids number that the Ladybird tolerates without fleeing*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxAphids;
    /** Max Aphids number that the Ladybird tolerates without fleeing*/
    std::unique_ptr<const CfgBool> p_cfg_LadybirdDebugging;
    /** Max number of tries to search for LongRangeDirection */
    int LongRangeMaxTries{50};
    /** \brief The numbers (TOPs) of the aphid species that the ladybird can interact with */
    std::unique_ptr<const CfgArray_Int> p_cfg_LadybirdAphidSpecies;
    /** A number of ladybirds that need to overwinter in a particular polygon for it to be regarded as a hibernacula*/
    int HibernaculaThreshold{10};
    TTovList LadybirdProhibitedTovsForMovement;
    TToleList  LadybirdProhibitedTolesForMovement;
    TTovList LadybirdProhibitedTovsForMovementLarva;
    TToleList  LadybirdProhibitedTolesForMovementLarva;
    TTovList LadybirdLongRangeTovs;
    TToleList LadybirdLongRangeToles;
    TTovList LadybirdAggregationTovs;
    TToleList LadybirdAggregationToles;
    LadybirdConstantClass();
protected:
    ;
private:

};
//------------------------------------------------------------------------------
/**

There is no need in Ladybird base class, let's move to the specific stage classes
*/
class Ladybird_Base : virtual public Beetle_Base{
public:
    Ladybird_Base(int, int, Landscape *, Ladybird_Population_Manager*);
    static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;
    int m_DateMade{0};
    int eatenToday{0};
    int m_AphidsAppetite{0};
    int m_MoveCounter{0};

    virtual int EatAphids(int);
    void incEatenToday(int add){eatenToday+=add;};
    virtual int getEatenToday() const{return eatenToday;};
    virtual void setEatenToday(int a){eatenToday=a;};
    inline int getAphidsToEat(){return getAphidsAppetite()-getEatenToday();};
    /**\brief The function returns true if there are too much aphids in the location*/
    static bool IsTooMuchAphids(long num);
    virtual inline int getAphidsAppetite(){return 0;};
    int Cannibalise(int appetite);

    virtual int getMaxCannibalise();
    virtual double getShortRangeDistance(){return 0.;};
    void getHopProbabilities(float (& p)[8], int);
    static int ShortHopDirection(float (&probabilities) [8]);
    static int LongHopDirection(std::array<float,4> (&probabilities));
    /** The function takes the direction in 0-3
     * and gives out the random direction in degrees inside this sector*/
    static int PreciseLongHopDirection(int inputdir);
    virtual bool IsLocationAllowed(int x, int y){
      return true;
    };
    inline int getMoveCounter() const{return m_MoveCounter;};
    inline void resetMoveCounter(){m_MoveCounter=0;};
    inline void incMoveCounter(){m_MoveCounter++;};
    virtual int getMaxDailyDistance();
    virtual inline void calcAphidsAppetite(){m_AphidsAppetite=0;};
    inline bool CanMove(){
        return (getMoveCounter() < getMaxDailyDistance());
    };
    // todo: check if it shouldn't be used
    virtual int getMaxHopDistance(){return 0;};
    virtual int getMaxSensingDistance(){return 0;};


    static int getMaxSAttempts();

    virtual void Move();

    static int getLongRangeDistance(double wind_speed, double travel_angle_rad, double wind_rad);

    static int getLongRangeDistance();

    static int getLongRangeSearchLength();

    static int getLongRangeDirection(int wind_direction, double wind_speed);
    bool IsLongRangeTarget(int x, int y);
    void setDateMade(int date){
        m_DateMade=date;
    };

    bool ExtremeTempIsOn() const override;
    void setTotalEggsToLay(int no){m_TotalEggsToLay=no;};
private:
    bool IsLocationSamePoly(int x, int y);
protected:
    double getExtremeTempMin() const override;

    double getExtremeTempMax() const override;
    double getMinTemp() override;

    double getMaxTemp() override;
    virtual int PreyDensityLevel(long AphidsNum)=0;
    virtual void UpdateEggsToLay(int preydensitylevel, double temp);
    /** the number of eggs to lay in the lifetime: updated until the maturity*/
    int m_TotalEggsToLay{0};

//    virtual double TempRelatedMortality(double temp, double MaxTemp, double MinTemp);


    int GetTempRange(double temp);
};
//------------------------------------------------------------------------------
/**
*\brief
*The class describing the beetle Egg_List objects
 *
 * In general case this class should have only a constructor
*/
class Ladybird_Egg_List : public  Beetle_Egg_List, public Ladybird_Base
{

/** The egg list is an optimisation to reduce memory and time to run
   it means that there are only 365 of them possible and that each on contains
   all the eggs laid on that day. The only real problem is that there is no longer a link from parent to
   offspring - so this version cannot be used with genetics without adding the genes to the APoint struct.\n


*/
public:
	/** \brief Egg_List class constructor */
    Ladybird_Egg_List(int today,Ladybird_Population_Manager* BPM, Landscape* L);

   // static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;

    double getEggDevelConst2() override;

    bool Cannibalisation() override;

    double getSoilCultivationMortality() const override;

    TTypesOfBeetleState st_Develop() override;

    TTypesOfBeetleState st_Hatch() override;
    void ClearFromMap(int X, int Y) override;
    void BeginStep() override;
protected:
    //bool DailyMortality() override;
    int PreyDensityLevel(long AphidsNum) override{return 5;};
    double getDailyEggMortality() const override;
    double TempRelatedMortality (double temp, double maxtemp, double mintemp) override;

};

/** \brief The population manager class for Ladybird
 *
 * This should only include methods and attributes that are different from the generic beetle
 * */

class Ladybird_Larvae : public Beetle_Larvae, public Ladybird_Base{
public:
    Ladybird_Larvae( int x, int y, Landscape* L, Ladybird_Population_Manager* BPM );
    ~Ladybird_Larvae()override=default;

   // static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;
protected:
    // member functions
    //TTypesOfBeetleState TempRelatedMortality() override;
    double getLarvaDevelFactor(long AphidsNum);
    int getLarvalStagesNum() const override;
    void ClearFromMap(int X, int Y) override;
    bool AddToMap(int X, int Y) override;
    inline int getAphidsAppetite() override{ return m_AphidsAppetite;   };
    void calcAphidsAppetite() override;
    double getLDevelConst2(int i) const override;

    bool Cannibalisation() override;

    double getSoilCultivationMortality() const override;

    void Step() override;

    double getShortRangeDistance() override;

    TTypesOfBeetleState st_Develop() override;



    void BeginStep() override;

   // bool TempRelatedLarvalMortality(int temp2) override;

    //bool TempRelatedLarvalMortality();
    static inline double getMovementThreshold();
    int getMaxCannibalise() override;
    bool IsLocationAllowed(int x, int y) override;
    int getMaxDailyDistance() override;
    int getMaxHopDistance() override;
    int getMaxSensingDistance() override;
    int PreyDensityLevel(long AphidsNum) override;

    // member variables
    /* brief # Units eaten today
     * units can be aphids, larva, eggs: maybe there will be some conversion ratio
     * */
    //int eatenToday{0};





    TTypesOfBeetleState st_Pupate() override;

    double getDailyMortalityRate() const  override;

    double getLarvaTDailyMortality(int st, int t) override;

    double TempRelatedMortality(double temp, double maxtemp, double mintemp) override;
};
class Ladybird_Pupae : public Beetle_Pupae, public Ladybird_Base{
public:
    Ladybird_Pupae( int x, int y, Landscape* L, Ladybird_Population_Manager* BPM );
    ~Ladybird_Pupae()override =default;
    void ClearFromMap(int X, int Y) override;
   // static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;

    double getDevelConst2() const override;

    double getSoilCultivationMortality() const override;

    TTypesOfBeetleState st_Emerge() override;
    void BeginStep () override;
protected:
    int PreyDensityLevel(long AphidsNum) override;
    double getDailyMortalityRate() const  override;

    double TempRelatedMortality(double temp, double maxtemp, double mintemp) override;
};
class Ladybird_Adult : public Beetle_Adult, public Ladybird_Base{
public:
    // member functions
    Ladybird_Adult( int x, int y, bool can_Reproduce, Landscape* L, Ladybird_Population_Manager* BPM );
    Ladybird_Adult( int x, int y, int DateMade, bool can_Reproduce, Landscape* L, Ladybird_Population_Manager* BPM );
    ~Ladybird_Adult()override =default;
    bool IsLocationAllowed(int x, int y) override;
    void setOverwintered(bool value) {hasOverwintered=value;};

    inline void setCanReproduce(bool param){m_CanReproduce=param;};
    void setIsHibernating(bool val){
        IsHibernating = val;
    }
    void ResetAll(){
        DailyEggs=0;
        m_EmergenceDay=0;
        m_UnsuccessfulAphidSearch=0;
        m_LongFlights=0;
    }
    // member variables
   // static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;
protected:
    // member functions
    int PreyDensityLevel(long AphidsNum) override;
    /** supplies the daydegrees collected */
    double SupplyADayDeg()  { return m_ADayDeg;}
    /**The function that transports the ladybird to a new position it returns true if a new position was found*/
    int LongRangeFind(float, int, bool&);
    void calcAphidsAppetite() override;
    void Reproduce(int p_x, int p_y) override;
    bool ShouldStartAggregating(int day);
    inline int getAphidsAppetite() override{  return m_AphidsAppetite;   };
    double getDailyMortalityRate() const  override;
    /**
 * \brief Hibernate function for the Adult is used only in the extreme case: 1st year
 *
 * In the rest of the code the Ladybird will turn into Immobile_Adult during hibernation
 * */
    TTypesOfBeetleState st_Hibernate() override;
    TTypesOfBeetleState st_Forage() override;
    void Step() override;
    static int getClutchSize();

    int EatAphids(int) override;
    //int Cannibalise(int);
    //todo: check if the method is needed
    int getEggsLayedToday();
    //todo: check if the method is needed
    int getEggsLayedTotal();
    int getTodaysEggProduction();
    bool getCanReproduce(){return m_CanReproduce;};
    bool getOverwintered() const{return hasOverwintered;};

    static bool getOviPositionPeriod();

    void ClearFromMap(int X, int Y) override;
    bool AddToMap(int X, int Y) override;

    /** The functions that read and write the number of Eggs per day and in total
 * */

    int getMaxHopDistance() override;
    int getMaxSensingDistance() override;
    void setEggCounter(int num){m_EggCounter=num;};
    void decEggCounter(int num){m_EggCounter-=num;};
    int getEggCounter(){return m_EggCounter;};
    void setDailyEggs(int num){DailyEggs=num;};
    void decDailyEggs(int num){DailyEggs-=num;};
    int getDailyEggs() const{return DailyEggs;};

    double getShortRangeDistance() override;

    void RecordUnsuccesfulAphidSearch();
    void RecordSuccesfulAphidSearch();

    int getUnsuccesfulAphidSearchNumber();
    static int getLongRangeFlightThresh();
    static int getMaxLongRangeFlights();

    void RecordLongFlight();
    void RecordNoLongFlight();
    int getLongFlightsNumber();
    TTypesOfBeetleState st_Dispersal() override;
    TTypesOfBeetleState st_Aggregate() override;
    TTypesOfBeetleState st_Aging() override;
    /**Looking for clump to hibernate*/
    bool FindClump();
    /**Checking if ready to hibernate alone*/
    bool IsHibernatingAlone();
    /**Register the beetle as being in hibernation: true on success*/
    bool RegisterHibernation();
    /**Deregister the beetle as being in hibernation: true on success*/
    bool DeregisterHibernation();
    /**Returns the number of cells to check before giving up the idea to hibernate today*/
    static int getMaxHibernationTries();
    /** Check if the clump is not too big to hibernate*/
    bool IsSpaceToHibernate();
    /** Check if someone hibernating in the point */
    bool IsSomeoneHibernating();
    /** Check if the landscape is fit for hibernation*/
    bool IsFitForHibernation();
    /** Depending on the date returns the chance to hibernate alone */
    double getChanceToHibernateAlone();
    /** Returns the first day of aggregation */
    //TODO: Move the parameters to config variables, translate to the day length: 263
    static int getFirstDayOfAggregation(){return 263;};
    //TODO: Move the parameters to config variables, translate to the day length: 293
    /** Returns the last day of aggregation */
    static int getLastDayOfAggregation(){return 293;};
    //TODO: Move the parameters to config variables
    /** Returns the number of aphids needed so the Ladybird won't start hibernation */
    static int getAphidsToStayAwake(){return 1000;};
    //TODO: Move the parameters to config variables
    /** Returns the number of aphids needed so the Ladybird will start hibernation */
    static int getAphidsToHibernate(){return 10;};
    //TODO: Move the parameters to config variables
    /** returns the daily chance to hibernate */
    static double getChanceToHibernate(){return 0.1;};
    //TODO: Move the parameters to config variables
    /** Get the maximum size of the overwintering clump allowed*/
    static int getMaxClumpSize();
    void CanReproduce() override;


    bool IsReproductionReady();
    //todo: check if the method is needed
    static double getAdultDevelConst();

    void BeginStep() override;
    /**The function that implements the short range movement
     *
     * the analogue of Beetle's MoveTo*/
    void ShortRangeMovement();
    /**The function that implements the long range movement*/
    bool LongRangeMovement();

    double getSoilCultivationMortality() const override;

    int getMaxCannibalise() override;

    void RandomMovement();


    // member variables
    /**The day of teh year when this particular individual emerged,
     * to be used to track minimum delay time between the emergence and oviposition*/
    unsigned short int m_EmergenceDay{0};
    bool IsHibernating{false};
    bool hasOverwintered{false};
    //todo: check if the method is needed
    short m_DailyDistance{0};
    /** The variables defining the amount of eggs laid for each animal
     * after checking we can move it to a separate vector (eigen/blitz)
     * */
    int DailyEggs{0};
    /** day degrees sum for an individual*/
    double m_ADayDeg{0};


    //int eatenToday{0};

    /** \brief The variable that holds the unsuccesfull searches for aphids
     *
     * It remembers 32 last days. Therefore the threshold for a long run would be maximum 32 (around a month)
     *
     * */
    std::bitset<32> m_UnsuccessfulAphidSearch {0};
    std::bitset<32> m_LongFlights {0};
    /**Number of Aphids to eat today: Updated before the step*/
    //todo: check if the method is needed
    int m_AphidsToEat{0};


    double getExtremeTempMin() const override;
    

    double getMinTemp() override;

    double getMaxTemp() override;
    static double getDevelConst2() ;


    int CalcMaxEggs();
};

class Ladybird_Population_Manager: public Beetle_Population_Manager, public std::enable_shared_from_this<Ladybird_Population_Manager>
{
 public:
    // member functions
    explicit Ladybird_Population_Manager(Landscape *p_L);
    /**\brief Method to check that the location is allowed for ladybird*/
    bool IsLocationAllowedForAdult(int x, int y);
    bool IsLocationAllowedForLarva(int x, int y);

/**   \brief Method to add beetles to the population */
   virtual void CreateObjects(int ob_type, TAnimal *pvo,void* null ,
                              std::unique_ptr<struct_Ladybird> data,int number);


   /** \brief Destructor: the same as in base class */
   ~Ladybird_Population_Manager() override= default;
    //virtual void Init(void) override;
    void Init() override;

    inline bool isEmergenceDay();
    int getLadybirdEmergenceDayLength() const;
    int getLadybirdEmergenceChance() const;
    /**\brief the function returns true if the weather is suitable for flying */
    bool getFlyingWeather();
    inline int getOviPositionPeriodStart() const{return ladybirdconstantslist->p_cfg_LadybirdOvipositionPeriodStart->value();};
    inline int getOviPositionPeriodEnd() const{return ladybirdconstantslist->p_cfg_LadybirdOvipositionPeriodEnd->value();};
    inline int getMaxHopDistance() const{return ladybirdconstantslist->p_cfg_LadybirdMaxHoppingDistance->value();};
    //todo: check if the method is needed
    inline int getMeanHopDistance() const{return ladybirdconstantslist->p_cfg_LadybirdMeanHoppingDistance->value();};
    //todo: check if the method is needed
    inline int getStdHopDistance() const {return ladybirdconstantslist->p_cfg_LadybirdStdHoppingDistance->value();};
    inline int getMaxLarvaHopDistance() const{return ladybirdconstantslist->p_cfg_LadybirdLarvaMaxHoppingDistance->value();};
    inline int getMaxShortRangeAttempts() const{return ladybirdconstantslist->p_cfg_LadybirdMaxShortRangeAttempts->value();};
    inline int getAphidDensityToReproduce() const{return ladybirdconstantslist->LadybirdAphidDensityToStartReproduction;};
    inline int getAphidDensityToForage() const{return ladybirdconstantslist->LadybirdAphidDensityToStartForaging;};
    inline int getMaxAphids() const{return ladybirdconstantslist->p_cfg_LadybirdMaxAphids->value();};
    bool getOvipositionPeriod();
    bool IsUniSeasonal() const;
    void decAphids(int x, int y, int num);
    int getCannibalisationValue(int x, int y, int type){return (*m_LadybirdCannibalismMap)(x, y, type);};
    void decCannibalisationValue(int x, int y, int type){(*m_LadybirdCannibalismMap)(x, y, type)--;};
   // member variables
    std::unique_ptr<LadybirdConstantClass> ladybirdconstantslist{nullptr};
    probability_distribution m_LongHopRandom{"UNIINT", "0 89"};
    //todo: check if the method is needed
    probability_distribution m_RegularRandom{"UNIINT", "0 2147483647"};
    probability_distribution m_ClutchSizeRandom{"NORMAL", "36.0 10.0"};
    std::unique_ptr<probability_distribution> m_ShortRangeDistanceAdult;
    std::unique_ptr<probability_distribution> m_ShortRangeDistanceLarva;
    double SupplyEDayDeg(int day) override { return (*m_DayDeg)(day,0);}
    /** \brief Get larval day degress for a specific dayand instar */
    double SupplyLDayDeg(int L, int day) override { return (*m_DayDeg)(day,1+L);}
    /** \brief Get pupal day degress for a specific day */
    double SupplyPDayDeg(int day) override{ return (*m_DayDeg)(day,5);}
    unsigned SupplyTilePopulation(unsigned x, unsigned y, int lifestage) override;
    int getLadybirdEggProductionXY(int x, int y);
    long getAphidsNumber(int x, int y);
    /**
     * returns the number of eggs/larva/pupa/adult
     * type is:m_LadybirdDensityMap
     * 0: eggs
     * 1: L1
     * 2: L2
     * 3: L3
     * 4: L4
     * 5: Pu
     * 6: Ad
     * * */
    int getLadybirdDensity(int x, int y, int type);
    bool decLadybirdDensity(int x, int y, int type){
        if ((*m_LadybirdDensityMap)(x,y,type)==blitz::tiny((*m_LadybirdDensityMap)(x,y,type))){
            return false;
        }
        else{
            (*m_LadybirdDensityMap)(x,y,type)--;
            return true;
        }

    };
    bool incLadybirdDensity(int x, int y, int type){
        if ((*m_LadybirdDensityMap)(x,y,type)==blitz::huge((*m_LadybirdDensityMap)(x,y,type))){
            return false;
        }
        else{
            (*m_LadybirdDensityMap)(x,y,type)++;
            return true;
        }

    };
    void setCannibalism(int x, int y, int type, int num);
    /** \brief The function that for each polyref returns whether this polygon was a hibernacula or not
 * **/
    bool IsHibernacula(int a_polyref);
    int getTotalNumberEggsMax() const;
   //Ladybird_Egg_List* m_EList[365];
private:
    /*************************/
    /** variables */
    /*************************/
    /** \brief
    Pointer to an output file for ladybird x y data
    */
    std::shared_ptr<ofstream> m_LadybirdXYDumpFile;
    std::vector<std::shared_ptr<AphidsD_Population_Manager>> Aphids_PM{};
    /** I am not sure about the implementation of teh next ones. */
    /** Maybe it would be better to have a proper vector here (from blitz)
     * With blitz (or better even Eigen) you can do Vector+=scalar instead of loops
     */
    /**   \brief  Storage for daily day degrees for eggs implemented in base class*/
  //  std::array<double,365> m_EDayDeg;
    /**   \brief  Storage for daily day degrees for larvae implemented in base class */
   // std::array<std::array<double,365>,4> m_LDayDeg;
    /**   \brief  Storage for daily day degrees for pupae implemented in base class */
   // std::array<double, 365> m_PDayDeg;
    /**   \brief  Storage for daily day degrees for adults implemented in base class */
   // std::array<double, 365> m_ADayDeg;
    /** \brief Storage for daily day degrees */
    std::unique_ptr<blitz::Array<double, 2>> m_DayDeg;
    /** \brief the dependence between temperature and the clutch size: updated daily */
    double TodaysEggProductionTempFactor{0.0};
    /** \brief the density of the Aphids: received daily from the Aphid model */
    std::unique_ptr<blitz::Array<long, 3>> m_LadybirdAphidsDensityMap;
    /** \brief the density of ladybird lifeforms max 255 elements*/
    std::unique_ptr<blitz::Array<unsigned char, 3>> m_LadybirdDensityMap;
    /** \brief the map of Cannibalism-related adjustments */
    std::unique_ptr<blitz::Array<unsigned char, 3>> m_LadybirdCannibalismMap;
    /** The list of all the polygons where for each polygon the bits correspond to whether it was used for hibernation in a particular year
    * The bitset is initialised randomly on the first year and then runs itself over. The size of the bitset corresponds to how long we are going to
    * "remember" the hibernaculae.
    * */
    std::vector<std::bitset<HIBERNACULA_MEMORY>> m_ListOfHibernaculae;
    /** \brief The number of different aphid species on the landscape: to be updated on the start and to reflect to the
     * size of m_LadybirdAphidsDensityMap*/
    int m_AphidSpeciesNum{-1};
    /*************************/
    /**  methods */
    /*************************/
    /** the function that calculates total egg temperature related factor called daily in DoFirst()*/
    static double LadybirdTotalEggTempFactor(double temp);
    void DebugOutputDoFirst();
    inline double getTodaysEggProductionTempFactor() const{return TodaysEggProductionTempFactor;};
    void RunStepMethods() override;
    /**\brief Function that updates daydegrees vector */
    void LadybirdUpdateDayDegrees(double temptoday, int today);
    /**\brief Function that calculates daily temp-related factor of Egg production */
    static double LadybirdCalcDailyTempRelatedEggFactor(double temp);
    void DoBefore () override;
    void DoFirst() override;
    void KillOnNYEve();

    void ClearOnNYDay();




    void LadybirdUpdateAphidsDensityMap();

    void AphidsUpdateAphidsDensityMap();

    void DoLast() override;



    bool StepFinished() override;

    static void WriteHeaders(const std::shared_ptr<ofstream>& a_file, vector<string> a_headers);

    void XYDump();
    /** \brief The function that initialises hibernaculae
 *
 * It runs only one in the simulation on the first day of the first year
 * */
    void InitialiseHibernaculae();
    /** \brief The function that updates the list of hibernaculae
     *
     * Runs each year Jan 1st*/
    void UpdateHibernaculae();
    /** \brief the amount of objects of specific type according to the density map
     * (FOR DEBUGGING
    * */

    int GetPopulationSizeFromDensityMap(int spec);

    void DebugOutputDoFirstAdult();
};



#endif

