/*
*******************************************************************************************************
Copyright (c) 2011, Andrey Chuhutin, Aarhus, 2020
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------

#ifndef Poecilus_allH
#define Poecilus_allH

//---------------------------------------------------------------------------

#include "Beetle_BaseClasses.h"

extern const char* SimulationName;

// #define __BEETLEPESTICIDE1

//------------------------------------------------------------------------------
// Forward Declarations

class Poecilus_Population_Manager;
class Landscape;
class MovementMap;
class SimplePositionMap;

class probability_distribution;


//------------------------------------------------------------------------------
/* Not used
class ListOfPositions
{
 public:
  int locs[2][30];
  int NoLocs;
};
//------------------------------------------------------------------------------
*/

//------------------------------------------------------------------------------

/** \brief A data class for Poecilus data */
/*class struct_Poecilus
{
 public:
  int DayDegrees;
  int x;
  int y;
  Landscape* L;
  Poecilus_Population_Manager * BPM;
  int HowMany;
};
*/
//------------------------------------------------------------------------------
/**
\brief
The class describing the constants specific to Poecilus
*/
class PoecilusConstantClass{
public:
    ;
protected:
    ;
private:

};
//------------------------------------------------------------------------------
/**

There is no need in Poecilus base class, let's move to the specific stage classes
*/

//------------------------------------------------------------------------------
/**
*\brief
*The class describing the beetle Egg_List objects
 *
 * In general case this class should have only a constructor
*/
class Poecilus_Egg_List : public  Beetle_Egg_List
{

/** The egg list is an optimisation to reduce memory and time to run
   it means that there are only 365 of them possible and that each on contains
   all the eggs laid on that day. The only real problem is that there is no longer a link from parent to
   offspring - so this version cannot be used with genetics without adding the genes to the APoint struct.\n


*/
public:
    /** \brief Egg_List class constructor */
    Poecilus_Egg_List(int today,Poecilus_Population_Manager* BPM, Landscape* L);

};

/** \brief The population manager class for Poecilus
 *
 * This should only include methods and attributes that are different from the generic beetle: e.g. move
 * */

class Poecilus_Larvae : public Beetle_Larvae{
public:
    Poecilus_Larvae( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM );
    ~Poecilus_Larvae()override=default;
};
class Poecilus_Pupae : public Beetle_Pupae{
public:
    Poecilus_Pupae( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM );
    ~Poecilus_Pupae()override =default;
};
class Poecilus_Adult : public Beetle_Adult{
public:
    Poecilus_Adult( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM );
    ~Poecilus_Adult()override =default;
};
class Poecilus_Population_Manager: public Beetle_Population_Manager
{
public:
    /**   \brief Method to add beetles to the population */
    void CreateObjects(int ob_type, TAnimal *pvo,void* null ,
                       std::unique_ptr<struct_Beetle>,int number) override;

    /** \brief Constructor */
    explicit Poecilus_Population_Manager(Landscape* p_L);
    /** \brief Destructor: the same as in base class */
    ~Poecilus_Population_Manager() override= default;
    /** \brief Get adult population size */
    std::unique_ptr<PoecilusConstantClass> poecilusconstantslist{nullptr};
    //Poecilus_Egg_List* m_EList[365];

    void Init() override;
};



#endif

