/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file OliveFly_Population_Manager.h 
\brief <B>The main header code for Olive Fly population manager</B>
*/
/**  \file OliveFly_Population_Manager.h
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------
#ifndef OliveFly_Population_ManagerH
#define OliveFly_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


class OliveFly;
class OliveFly_Population_Manager;

/**
\brief
The class to handle all Olive Fly population related matters in the whole landscape
*/
class OliveFly_Population_Manager : public SubPopulation_Population_Manager
{
public:
   OliveFly_Population_Manager(Landscape* L, int a_sub_w=10, int a_sub_h=10, int a_num_life_stage=4);
   virtual ~OliveFly_Population_Manager(){};
   //OliveFly* CreateObjects(TAnimal *pvo, struct_OliveFly* data, int number){};
};

#endif
