// Version of 5th May 2020
/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
// Version of 7th January 2020

#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/PositionMap.h"
#include "../Spiders/Spider_BaseClasses.h"
#include "../Spiders/Spider_toletov.h"
#include "../Spiders/Erigone.h"
#include "../Spiders/Erigone_Population_Manager.h"


static CfgFloat cfg_NoFoodLevel("SPID_NOFOODLEVEL", CFG_CUSTOM, 2.5); //2.5 because that will give low food after harvest, but no food after ploughing
static CfgFloat cfg_LowFoodLevel("SPID_LOWFOODLEVEL", CFG_CUSTOM, 10.58);
static CfgFloat cfg_HighFoodLevel("SPID_HIGHFOODLEVEL", CFG_CUSTOM, 18.515);

static CfgInt cfg_BalDensityDepMortConst("SPID_BALDENSITYDEPMORTCONST", CFG_CUSTOM, 5); //  Was 5
static CfgInt cfg_WalDensityDepMortConst("SPID_WALDENSITYDEPMORTCONST", CFG_CUSTOM, 5); //  Was 5
static CfgInt cfg_ExtraCompetionMortality("SPID_EXTRACOMPETITIONMORTALITY", CFG_CUSTOM, 10); // was 10

std::array<int,31> Spider_Population_Manager::m_DispDistances =
{
    3,   11,   29,   71,  136,  235,  405,  673, 1054, 1567,
 2185, 2906, 3748, 4693, 5528, 6423, 7226, 7926, 8543, 8990,
 9360, 9614, 9782, 9882, 9950, 9979, 9991, 9997, 9999, 9999,
 10000
};

const double BallooningHours[52] = {
3.67,3.77,3.92,4.08,4.30,4.52,4.77,5.00,5.27,5.53,5.78,6.05,6.32,6.57,6.83,7.08,
7.33,7.58,7.80,8.03,8.22,8.40,8.53,8.62,8.67,8.67,8.60,8.52,8.37,8.20,8.02,7.78,
7.55,7.32,7.07,6.82,6.55,6.32,6.05,5.78,5.53,5.27,5.03,4.78,4.53,4.32,4.12,3.93,
3.77,3.67,3.62,3.60
};

// Initialise any static member attributes (these will normally be parameters specific to species)
int Spider_Base::m_DenDependenceConst0 = 0;        // Initialize static variable
int Spider_Base::m_SimW = 0;                       // Initialize static variable
int Spider_Base::m_SimH = 0;                       // Initialize static variable
int Spider_Egg::m_DailyEggMortConst = 0;           // Initialize static variable
int Spider_Egg::m_HatDensityDepMortConst = 0;      // Initialize static variable
double Spider_Egg::m_EggDevelConst = 0;            // Initialize static variable
double Spider_Juvenile::m_JuvDevelConst = 0;       // Initialize static variable
int Spider_Juvenile::m_JuvDensityDepMortConst = 0; // Initialize static variable
std::array<int, 40> Spider_Juvenile::m_DispersalChance = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }; // Initialize static array variable
double Spider_Female::m_EggProducConst = 0;        // Initialize static variable
int Spider_Female::m_Max_Egg_Production = 0;       // Initialize static variable
int Spider_Female::m_DailyFemaleMort = 0;          // Initialize static variable

//---------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------START class Spider_Base-----------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
bool Spider_Base::EggPosValid(unsigned a_x, unsigned a_y)
{
    return spider_tole_egss_position_valid(m_OurLandscape, a_x, a_y);
}
//---------------------------------------------------------------------------------------------------------------------------------------


bool Spider_Base::HatchDensityMort(int a_x, int a_y, int a_range) {
    int x1, y1, x2, y2;
    // Define extent
    x1 = a_x - a_range;
    y1 = a_y - a_range;
    //range=range<<1;
    x2 = a_x + a_range;
    y2 = a_y + a_range;
    if (x1 < 0) x1 = 0;
    if (y1 < 0) y1 = 0;
    if (x2 > m_SimW) x2 = m_SimW;
    if (y2 > m_SimH) y2 = m_SimH;
    int sum = 0;
    for (int i = x1; i < x2; i++) {
        for (int j = y1; j < y2; j++) {
            sum += CheckPosMap(i, j);
        }
    }
    if (sum > m_DenDependenceConst0) {
        return true; // Den Dep Killed it
    }
    return false; // all OK
}
//---------------------------------------------------------------------------------------------------------------------------------------

Spider_Base::Spider_Base(int x, int y, Landscape* L, Spider_Population_Manager* p_spMan) : TAnimal(x, y, L) {
    Init(p_spMan);
}

void Spider_Base::ReInit(int x, int y, Landscape* L, Spider_Population_Manager* p_spMan) {
    ReinitialiseObject(x, y, L);
    Init(p_spMan);
}

void Spider_Base::Init(Spider_Population_Manager* p_spMan) {
    m_CurrentSpState = TTypesOfSpiderState::tosps_Initiation;
    m_AgeDegrees = 0;
    m_Age = 0;
    m_OurPopulationManager = p_spMan;
    m_pesticide_accum = 0.0;
}
//---------------------------------------------------------------------------------------------------------------------------------------

inline int Spider_Base::CheckPosMap(unsigned x, unsigned y) {
    return m_OurPosMap->GetMapValue(x, y);
}
//---------------------------------------------------------------------------------------------------------------------------------------

inline bool Spider_Base::GetPosMapPositive(unsigned x, unsigned y, unsigned range) {
    return m_OurPosMap->GetMapPositive(x, y, range);
}
//---------------------------------------------------------------------------------------------------------------------------------------

inline int Spider_Base::GetPosMapDensity(unsigned x, unsigned y, unsigned range) {
    return m_OurPosMap->GetMapDensity(x, y, range);
}
//---------------------------------------------------------------------------------------------------------------------------------------

inline void Spider_Base::ClearPosMap(unsigned x, unsigned y) {
    return m_OurPosMap->ClearMapValue(x, y);
}
//---------------------------------------------------------------------------------------------------------------------------------------

inline void Spider_Base::SetPosMap(unsigned x, unsigned y) {
    m_OurPosMap->SetMapValue(x, y);
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Base::EndStep() {
    // Arbitrary pesticide influence code.
#ifdef __SPIDER_PESTICIDE
    m_pesticide_accum += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
    if (m_pesticide_accum > 1.0) {
        m_CurrentSpState = tosps_Dying;
        return;
    }
    m_pesticide_accum = 0;
#endif
}

//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START class Spider_Egg-----------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
Spider_Egg::Spider_Egg(int x, int y, Landscape* L, Spider_Population_Manager* SpPM, int Eggs) :  Spider_Base(x, y, L, SpPM) {
    m_DateLaid = L->SupplyDayInYear();
    m_NoEggs = Eggs;
    m_OurPosMap = SpPM->m_EggPosMap;
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Egg::ReInit(int x, int y, Landscape* L, Spider_Population_Manager* SpPM, int Eggs) {
    Spider_Base::ReInit(x, y, L, SpPM);
    m_DateLaid = L->SupplyDayInYear();
    m_NoEggs = Eggs;
    m_OurPopulationManager = SpPM;
    m_OurPosMap = SpPM->m_EggPosMap;
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Egg::BeginStep() {
    CheckManagement();
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Egg::Step() {
    if (m_StepDone || m_CurrentStateNo == -1) return;
#ifdef __CJTDebug_5
    if (IsAlive() != 0x0DEADC0DE) DEADCODEError();
#endif
    switch (m_CurrentSpState) {
    case tosps_Initiation: // Initial
        m_CurrentSpState = tosps_Develop;
        break;
    case tosps_Develop:
        m_CurrentSpState = st_Develop();
        break;
    case tosps_Hatch:
        st_Hatch();
        m_StepDone = true;
        break;

    case tosps_Dying:
        st_Die();
        m_StepDone = true;
        break;
    case tosps_Destroy:
        // Only used when an object is deleted effectively without passing through dying, e.g. egg object on hatch
        m_StepDone = true;
        break;
    default:
        m_OurLandscape->Warn("Spider Egg - unknown state", NULL);
        exit(1);
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Egg::st_Die() {
    m_OurPopulationManager->m_EggPosMap->ClearMapValue(m_Location_x, m_Location_y);
    m_CurrentStateNo = -1;
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Egg::st_Hatch() {
    Hatch(m_OurPopulationManager->GetEggSacSpread(), m_OurPopulationManager->GetDoubleEggSacSpread(), m_NoEggs, m_HatDensityDepMortConst);
    m_OurPopulationManager->m_EggPosMap->ClearMapValue(m_Location_x, m_Location_y);
    m_CurrentSpState = tosps_Destroy; //Destroys object
    m_CurrentStateNo = -1;
}
//---------------------------------------------------------------------------------------------------------------------------------------

TTypesOfSpiderState Spider_Egg::st_Develop() {
    //if (random(10000) < cfg_Erig_DailyEggMort.value()) return tosps_Dying; // stochastic mortality go to die
    if (random(10000) < m_DailyEggMortConst) return tosps_Dying; // stochastic mortality go to die
    if (m_OurPopulationManager->GetEggDevelDegrees(m_DateLaid) > m_EggDevelConst) { 
        //m_OurPopulation->WriteToTestFile(2, m_OurLandscape->SupplyDayInYear());
        return tosps_Hatch; // go to hatch
    }
    else
    {
        m_StepDone = true;
        return tosps_Develop; // carry on developing
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------
void Spider_Egg::Hatch(int a_eggsackspread, unsigned a_doubleeggsacspread, unsigned a_noeggs, unsigned a_range)
{
    // Intially spreads the spiders over a EggSacSpread x EggSacSpread area
    // so as to minimise local density dependence

    unsigned Eggs = 0;
    struct_Spider* SS;
    SS = new struct_Spider;
    SS->L = m_OurLandscape;
    SS->SpPM = m_OurPopulationManager;
    // do this to avoid testing from wrap around
    // - always moves co-ords away from edge
    int mx = m_Location_x;
    int my = m_Location_y;
    if (mx < a_eggsackspread) mx = a_eggsackspread;
    if (my < a_eggsackspread) my = a_eggsackspread;
    if (mx + a_eggsackspread >= m_SimW)  mx = m_SimW - (a_eggsackspread + 1);
    if (my + a_eggsackspread >= m_SimH)  my = m_SimH - (a_eggsackspread + 1);
    int tx, ty;
    int tries = 0;
    while ((Eggs < a_noeggs) & (tries < 500)) {
        tries++;
        tx = mx + random(a_doubleeggsacspread) - a_eggsackspread;
        ty = my + random(a_doubleeggsacspread) - a_eggsackspread;
        if (EggPosValid(tx, ty))
        {
            // Must write to the position map
            if (m_OurPopulationManager->m_JuvPosMap->GetMapValue(tx, ty) == 0) {
                if (!HatchDensityMort(tx, ty, a_range)) {
                    m_OurPopulationManager->m_JuvPosMap->SetMapValue(tx, ty); // This one needs specifically to access a position map that is not m_OurPosMap
                    SS->x = tx;
                    SS->y = ty;
                    m_OurPopulationManager->CreateObjects(tspi_Spiderling, this, SS, 1);
                }
                Eggs++;
            }
        }

    }
    delete SS;
}
//---------------------------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------START class Spider_Juvenile --------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

Spider_Juvenile::Spider_Juvenile(int x, int y, Landscape* L, Spider_Population_Manager* SpPM) : Spider_Base(x, y, L, SpPM)
{
    m_AgeDegrees = 0;
    m_Age = 0;
    m_OurPopulationManager = SpPM;
    m_OurPosMap = SpPM->m_JuvPosMap;
    m_BadHabitatDays = 0;
    m_MyDirection = (char)random(8);
    m_MustBalloon = false;
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Juvenile::ReInit(int x, int y, Landscape* L, Spider_Population_Manager* p_SpPM)
{
    Spider_Base::ReInit(x, y, L, p_SpPM);
    m_AgeDegrees = 0;
    m_Age = 0;
    m_OurPopulationManager = p_SpPM;
    m_OurPosMap = p_SpPM->m_JuvPosMap;
}
//---------------------------------------------------------------------------------------------------------------------------------------


void Spider_Juvenile::BeginStep() {
#ifdef __CJTDebug_5
    if (IsAlive() != 0x0DEADC0DE) DEADCODEError();
#endif
    CheckManagement(); // This can lead to death, or movement or no change
    // CJT 31-01-2007 if ( ( m_Age++ > 6 ) && ( m_Age < 10 ) ) {
    if (m_Age++ == 7) {
        ClearPosMap(m_Location_x, m_Location_y);
        // Seventh day of life so check for density dependent dispersal mortaltity
        unsigned int range = m_JuvDensityDepMortConst;
        // NB these two must be signed!
        int x1 = m_Location_x - range;
        int y1 = m_Location_y - range;
        int y2 = m_Location_y + range;
        if (y2 > m_SimH) range -= (++y2 - m_SimH);
        if (x1 < 0) x1 = 0;
        if (y1 < 0) y1 = 0;
        range = (range << 1); // *2
        if (GetPosMapPositive(x1, y1, range)) {
            m_CurrentStateNo = -1; // Die
            m_StepDone = true;
#ifdef __SpidDebug
            m_OurPopulationManager->RecordJDeath();
#endif
            return;
        }
    }
    SetPosMap(m_Location_x, m_Location_y);
}
//---------------------------------------------------------------------------------------------------------------------------------------


void Spider_Juvenile::Step() {
    if (m_StepDone || m_CurrentStateNo == -1) return;
#ifdef __CJTDebug_5
    if (IsAlive() != 0x0DEADC0DE) DEADCODEError();
#endif
    switch (m_CurrentSpState) {
    case tosps_Initiation: // Initial
        m_CurrentSpState = tosps_Develop;
        break;

    case tosps_Develop:
        // The last thing done each time step
        // next behaviour is either death, maturation, assess habitat or move
        m_CurrentSpState = st_Develop();
        m_StepDone = true;
        break;

    case tosps_AssessHabitat:
        // Is the first thing a spider does if not moving or dying or maturing
        // Results in either death or development or movment
        m_CurrentSpState = st_AssessHabitat();
        break;

    case tosps_Move:
        // Just a switch which determines whether to balloon or walk
        if (!m_OurPopulationManager->GetWalking()) m_CurrentSpState = tosps_JBalloon; else
            m_CurrentSpState = tosps_JWalk;
        break;

    case tosps_JBalloon:
        m_CurrentSpState = st_Balloon();
        break;

    case tosps_JWalk:
        // Results in either death or to Develop
        m_CurrentSpState = st_Walk();
        break;

    case tosps_Mature:
        Maturation();
        m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
        m_CurrentStateNo = -1;
        m_StepDone = true;
        break;

    case tosps_Dying:
        m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
        m_CurrentStateNo = -1;
        m_StepDone = true;
#ifdef __SpidDebug
        m_OurPopulationManager->RecordJDeath();
#endif
        break;

    default:
        m_OurLandscape->Warn("Spider Juvenile - unknown state", "");
        exit(1);
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------

TTypesOfSpiderState  Spider_Juvenile::st_Develop() {
    if (g_rand_uni() < m_OurPopulationManager->GetJuvMort()) {
#ifdef __SpidDebug
        m_OurPopulationManager->RecordDeath(6);
#endif
    return TTypesOfSpiderState::tosps_Dying;
}
    switch (AssessFood()) {
    case 0: // No Food
      // No development
        AddToBadHabitatDays();
        break;
        // Not much food
    case 1:
        m_AgeDegrees += m_OurPopulationManager->GetJuvDegrees_poor();
        m_BadHabitatDays = 0;
        break;
    case 2:
        m_AgeDegrees += m_OurPopulationManager->GetJuvDegrees_intermediate();
        m_BadHabitatDays = 0;
        break;
    case 3: // Lots of food
        m_AgeDegrees += m_OurPopulationManager->GetJuvDegrees_good();
        m_BadHabitatDays = 0;
        break;
    }
    if (m_AgeDegrees > m_JuvDevelConst) {
        return TTypesOfSpiderState::tosps_Mature; // go to mature
    }
    else
    {
        if (random(100) < (m_DispersalChance[m_BadHabitatDays])) {
            m_CurrentSpState = tosps_Move;
        }
    }
    return TTypesOfSpiderState::tosps_Develop; // carry on developing
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Juvenile::Maturation() {
    bool done = false;
    struct_Spider* aps;
    aps = new struct_Spider;
    aps->SpPM = m_OurPopulationManager;
    aps->L = m_OurLandscape;
    int x_ex, y_ex, tx, ty;
    if (m_Location_x < 5) x_ex = m_Location_x; else x_ex = 5;
    if (m_Location_y < 5) y_ex = m_Location_y; else y_ex = 5;
    for (int a = 0; a < y_ex; a++) {
        for (int b = 0; b < x_ex; b++) {
            tx = m_Location_x - b;
            ty = m_Location_y - a;

            if (spider_tole_juvenile_maturation_valid(m_OurLandscape, tx, ty))
            {
                // Must write to the position map
                if (m_OurPopulationManager->m_AdultPosMap->GetMapValue(tx, ty) == 0) 
                {
                    m_OurPopulationManager->m_AdultPosMap->SetMapValue(tx, ty);
                    aps->x = tx;
                    aps->y = ty;
                    m_OurPopulationManager->CreateObjects(tspi_Female, this, aps, 1);
                    done = true;
#ifdef __SpidDebug
                    m_OurPopulationManager->RecordMature();
#endif
                }
            }
            
            if (done == true) break;
        }
        if (done == true) break;
    }
    delete aps;
}
//---------------------------------------------------------------------------------------------------------------------------------------

TTypesOfSpiderState Spider_Juvenile::st_Balloon()
{
    // Results in either death or to Develop
    // First clear the old position
    ClearPosMap(m_Location_x, m_Location_y);
    switch (Balloon()) {
    case 0:
#ifdef __SpidDebug
        m_OurPopulationManager->RecordDeath(7);
#endif
        m_CurrentSpState = tosps_Dying;
        break;
    case 1: // Could not move to a new place
        m_CurrentSpState = tosps_Develop;
        break;
    case 2: // Moved to a new place
        m_MustBalloon = false;
        break;
    }
    m_OurPosMap->SetMapValue(m_Location_x, m_Location_y);
    return tosps_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------------

TTypesOfSpiderState Spider_Juvenile::st_Walk()
{
    m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
    switch (Walk()) {
    case 0:
        m_StepDone = true;
#ifdef __SpidDebug
        m_OurPopulationManager->RecordDeath(8);
#endif
        return tosps_Dying;
        break;
    case 1: //Could not move
    case 2: // Moved to a new place
        m_OurPosMap->SetMapValue(m_Location_x, m_Location_y);
        break;
    }
    return tosps_Develop;
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Juvenile::SpecialWinterMort() {
    if ((m_AgeDegrees) < (m_JuvDevelConst * 0.75)) {
#ifdef __SpidDebug
        m_OurPopulationManager->RecordDeath(9);
#endif
        m_CurrentSpState = tosps_Dying;
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------

int Spider_Juvenile::AssessFood() {
    int tov = CheckToleTovIndex();
    if (tov == 3) {
        double insects = m_OurLandscape->SupplyInsects(m_Location_x, m_Location_y);
        if (insects < cfg_NoFoodLevel.value()) return 0; else if (insects < cfg_LowFoodLevel.value()) return 1;
        else if (insects > cfg_HighFoodLevel.value()) return 3; else
            return 2;
    }
    else if (tov == 2) {
        double insects = m_OurLandscape->SupplyInsects(m_Location_x, m_Location_y) * 0.5;
        if (insects < cfg_NoFoodLevel.value()) return 0; else if (insects < cfg_LowFoodLevel.value()) return 1;
        else if (insects > cfg_HighFoodLevel.value()) return 3; else
            return 2;
    }
    return 0;
}
//---------------------------------------------------------------------------------------------------------------------------------------

bool Spider_Juvenile::BallooningMortality(int dist) {
    //Mortality related directly to distance travelled
    if (random(100) < dist * m_OurPopulationManager->GetBallooningMortalityPerMeter()) {

       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------------------------

int Spider_Juvenile::Balloon() {
    if (m_OurPopulationManager->GetBTimeToday() > 0) {
        // can disperse
        // Step 3 Mortality
        int Distance = (int)(m_OurPopulationManager->GetDispDist(random(100)) * m_OurPopulationManager->GetBTimeToday());
        if (BallooningMortality(Distance)) {
            return 0; // Signal to die
        }
        // Step 4 where to go to
        int Direction = m_OurPopulationManager->GetWindDirection();
        return BalloonTo(Direction, Distance); // 0-2 Die, Balloon, Stay
    }
    /* } */
    return 1; // carry on
}
//---------------------------------------------------------------------------------------------------------------------------------------

int Spider_Juvenile::Walk() {
    if (m_OurPopulationManager->GetMinWalkTemp()) {
        // Step 1 should I disperse?
        if (random(100) < (m_DispersalChance[m_BadHabitatDays])) {
            // Want to disperse
            // Step 2 where to go to
            int Direction = m_MyDirection & -1;
            return WalkTo(Direction);
        }
    }
    return 1; // carry on
}
//---------------------------------------------------------------------------------------------------------------------------------------

int Spider_Juvenile::CheckToleTovIndex() {

    // Alternative version to see if we can increase speed using a movementmap
    int mmapval = m_OurPopulationManager->m_MoveMap->GetMapValue(m_Location_x, m_Location_y);
    if (mmapval == 1) {

        return (spider_tov_index(m_OurLandscape, m_Location_x, m_Location_y));
        
    }
    return mmapval;
}
//---------------------------------------------------------------------------------------------------------------------------------------

TTypesOfSpiderState Spider_Juvenile::AssessHabitat() {
    if (m_MustBalloon) {
        m_MustBalloon = true;
        return m_CurrentSpState = tosps_Move;
    }
    int result = CheckToleTovIndex();
    switch (result) {
    case 0:
#ifdef __SpidDebug
        m_OurPopulationManager->RecordDeath(5);
#endif
        return tosps_Dying; // Code for dying
    case 2:
        // Extra Mortality for competition sake
        if (random(10000) < cfg_ExtraCompetionMortality.value())
        {
#ifdef __SpidDebug
            m_OurPopulationManager->RecordDeath(4);
#endif
            return tosps_Dying;
        }
        if (m_OurPopulationManager->IsBallooningWeather()) return tosps_Move; // try to balloon 
        else
        {
            AddToBadHabitatDays();
            return tosps_Develop;
        }
    case 3:
        if (!m_OurPopulationManager->CheckHumidity(m_Location_x, m_Location_y)) {
            // Is too dry
            if (m_OurPopulationManager->IsBallooningWeather()) return tosps_Move; // try to balloon
            else
            {
                AddToBadHabitatDays();
                return tosps_Develop;
            }
        }
        else {
            return tosps_Develop;
        }
    default:
        m_OurLandscape->Warn("AssessHabitat - Bad return code", NULL);
        exit(1);
    }
    return tosps_Develop; // Compiler happiness
}
//---------------------------------------------------------------------------------------------------------------------------------------

int Spider_Juvenile::BalloonTo(int direction, int distance) {
    // moves the spider 'distance' metres in 'direction' direction
    // returns 2 if the new location is not lethal otherwise returns 0
      // For small landscapes ballooning round the world and back can be a problem
      // First clears its positions since it might try to count itself
    if (distance >= m_SimW) distance -= m_SimW;
    //
    switch (direction) {
    case 0: // North
        m_Location_y -= distance;
        if (m_Location_y < 0) m_Location_y += m_SimW;
        break;
    case 1: // NE
        m_Location_x += distance;
        m_Location_y -= distance;
        if (m_Location_y < 0) m_Location_y += m_SimW;
        if (m_Location_x >= m_SimW)  m_Location_x -= m_SimW;
        break;
    case 2: // E
        m_Location_x += distance;
        if (m_Location_x >= m_SimW) m_Location_x -= m_SimW;
        break;
    case 3: // SE
        m_Location_x += distance;
        m_Location_y += distance;
        if (m_Location_x >= m_SimW) m_Location_x -= m_SimW;
        if (m_Location_y >= m_SimW) m_Location_y -= m_SimW;
        break;
    case 4: // S
        m_Location_y += distance;
        if (m_Location_y >= m_SimW) m_Location_y -= m_SimW;
        break;
    case 5: // SW
        m_Location_x -= distance;
        m_Location_y += distance;
        if (m_Location_x < 0) m_Location_x += m_SimW;
        if (m_Location_y >= m_SimW) m_Location_y -= m_SimW;
        break;
    case 6: // W
        m_Location_x -= distance;
        if (m_Location_x < 0) m_Location_x += m_SimW;
        break;
    case 7: // NW
        m_Location_x -= distance;
        m_Location_y -= distance;
        if (m_Location_x < 0) m_Location_x += m_SimW;
        if (m_Location_y < 0) m_Location_y += m_SimW;
        break;
    }
    
    // Now find out if the habitat is lethal
    if (spider_tole_lethal( m_OurLandscape, m_Location_x, m_Location_y))
    {
        return 0; //Die
    }

    unsigned int range = cfg_BalDensityDepMortConst.value();
    // NB these two must be signed!
    int x1 = m_Location_x - range;
    int y1 = m_Location_y - range;
    int y2 = m_Location_y + range;
    if (x1 < 0) x1 = 0;
    if (y1 < 0) y1 = 0;
    if (y2 >= m_SimW) range -= (++y2 - m_SimW);
    range = (range << 1); // *2
    if (GetPosMapPositive(x1, y1, range)) return 0;
    return 2;
}
//---------------------------------------------------------------------------------------------------------------------------------------

int Spider_Juvenile::WalkTo(int direction) {
    // moves the spider 1 metre in 'direction' direction
    // returns 2 if the new location is OK otherwise returns 0 for death or 1 for no move
    int tx = m_Location_x;
    int ty = m_Location_y;
    switch (direction) {
    case 0: // North
        ty -= 1;
        if (ty < 0) ty += m_SimH;
        break;
    case 1: // NE
        tx += 1;
        ty -= 1;
        if (ty < 0) ty += m_SimH;
        if (tx >= m_SimW) tx -= m_SimW;
        break;
    case 2: // E
        tx += 1;
        if (tx >= m_SimW) tx -= m_SimW;
        break;
    case 3: // SE
        tx += 1;
        ty += 1;
        if (tx >= m_SimW) tx -= m_SimW;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 4: // S
        ty += 1;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 5: // SW
        tx -= 1;
        ty += 1;
        if (tx < 0) tx += m_SimW;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 6: // W
        tx -= 1;
        if (tx < 0) tx += m_SimW;
        break;
    case 7: // NW
        tx -= 1;
        ty -= 1;
        if (tx < 0) tx += m_SimW;
        if (ty < 0) ty += m_SimH;
        break;
    }
    // Now find out if the habitat is lethal
    TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType(tx, ty);
    int sum = 0;
    
    if (spider_tole_lethal(m_OurLandscape, tx, ty))
    {
        return 1;
    }
    else
    {
        int x1, y1, x2, y2;
        // Define extent
        x1 = m_Location_x - cfg_WalDensityDepMortConst.value();
        x2 = m_Location_x + cfg_WalDensityDepMortConst.value();
        y1 = m_Location_y - cfg_WalDensityDepMortConst.value();
        y2 = m_Location_y + cfg_WalDensityDepMortConst.value();
        if (x1 < 0) x1 = 0;
        if (y1 < 0) y1 = 0;
        if (x2 > m_SimW) x2 = m_SimW;
        if (y2 > m_SimW) y2 = m_SimW;
        for (int i = x1; i < x2; i++) {
            for (int j = y1; j < y2; j++) {
                //if position not empty die (density dependence)
                sum += CheckPosMap(i, j);
            }
        }
        if (sum > m_DenDependenceConst0) return 0; //die
    }

    m_Location_x = tx;
    m_Location_y = ty;
    return 2; // Moved OK
}
//---------------------------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------START class Spider_Female ----------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

Spider_Female::Spider_Female(int x, int y, Landscape* L, Spider_Population_Manager* SpPM) : Spider_Juvenile(x, y, L, SpPM) {
    m_EggSacDegrees = 0;
    m_lifespan = 0;
    m_EggsProduced = 0;
    m_OurPosMap = SpPM->m_AdultPosMap;
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Female::ReInit(int x, int y, Landscape* L, Spider_Population_Manager* EPM) {
    Spider_Juvenile::ReInit(x, y, L, EPM);
    m_EggSacDegrees = 0;
    m_lifespan = 0;
    m_EggsProduced = 0;
    m_OurPosMap = EPM->m_AdultPosMap;
}
//---------------------------------------------------------------------------------------------------------------------------------------

TTypesOfSpiderState Spider_Female::st_Walk() 
{
    m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
    switch (Walk()) {
    case 0:
        m_StepDone = true;
        return tosps_Dying;
        break;
    case 1: //Could not move
    case 2: // Moved to a new place
        m_OurPosMap->SetMapValue(m_Location_x, m_Location_y);
        break;
    }
    return  tosps_Reproduce;
}
//---------------------------------------------------------------------------------------------------------------------------------------

TTypesOfSpiderState Spider_Female::st_Balloon()
{
    // First clear the old position
    m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
    switch (Balloon()) {
    case 0: // killed
        m_StepDone = true;
        return tosps_Dying;
        break;
    case 1: //Could not move
        break;
    case 2: // Moved to a new place
        m_MustBalloon = false;
        break;
    }
    m_OurPosMap->SetMapValue(m_Location_x, m_Location_y);
    return tosps_Reproduce;
}
//---------------------------------------------------------------------------------------------------------------------------------------
void Spider_Female::BeginStep() {
    // Can result in death or movement or no change
    // if we run out of life we die
    m_lifespan++;
    if (m_lifespan == 365) {
        m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
        m_CurrentStateNo = -1;
        m_StepDone = true;
    }
    else {
        if (random(10000) < m_DailyFemaleMort) {
            m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
            m_CurrentStateNo = -1;
            m_StepDone = true;
        }
        else {
            /* Commented out to because it is never used under DK conditions   - too wet Reimplement as needed in descendent classes
            float biomass=m_OurLandscape->SupplyVegBiomass(m_Location_x, m_Location_y); int index;
            if (biomass>cfg_mediumplantbiomass.value()) index=2; else if (biomass<=cfg_lowplantbiomass.value()) index=0;
            else index=1; if (m_OurPopulation->SupplyTodaysDroughtSc(index)) { m_droughtCounter++;
            if (random(100)<DroughtMort[m_droughtCounter]) { m_CurrentSpState=tosps_Dying; m_StepDone=true; } }
            else m_droughtCounter=0; */
            CheckManagement();
        }
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Female::Step() {
    /**
    * This state is based on the Erigone code and may need to be overridden in descendent classes of other species.
    */
    if (m_StepDone || m_CurrentStateNo == -1) return;
    switch (m_CurrentSpState) {
    case tosps_Initiation: // Initial
        m_CurrentSpState = tosps_AssessHabitat;
        m_StepDone = true;
        break;
    case tosps_Develop:
    case tosps_Reproduce:
        // The last thing done each time step
        // results in either death or AssessHabitat
        switch (st_Reproduce()) {
        case 0:
            m_CurrentSpState = tosps_Dying;
            break;
        case 1:
            m_CurrentSpState = tosps_AssessHabitat;
            break;
        }
        m_StepDone = true;
        break;
    case tosps_AssessHabitat:
        // The first thing done each day unless the CheckManagement says Move
        m_CurrentSpState = st_AssessHabitat();
        if (random(100) < (m_DispersalChance[m_BadHabitatDays])) {
            m_CurrentSpState = tosps_Move;
        }
        break;
    case tosps_Move:
        // Just a switch to decide whether to balloon or walk
        // Can arrive here from CheckManagement or AssessHabitat
        if (!m_OurPopulationManager->GetWalking()) m_CurrentSpState = tosps_FBalloon; else
            m_CurrentSpState = tosps_FWalk;
        break;
    case tosps_FBalloon:
        m_CurrentSpState = st_Balloon();
        break;
    case tosps_FWalk:
        m_CurrentSpState = st_Walk();
        break;
    case tosps_Dying:
        m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y);
        m_CurrentStateNo = -1;
        m_StepDone = true;
        break;
    default:
        m_OurLandscape->Warn("Spider_Female - unknown state", "");
        exit(1);
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------

int Spider_Female::st_Reproduce() {
    //Get EggSac Degrees
    switch (AssessFood()) {
    case 0:
        AddToBadHabitatDays();
        break;
    case 1:
        m_EggSacDegrees += m_OurPopulationManager->GetEggDegreesPoor();
        m_BadHabitatDays = 0;
        break;
    case 2:
        m_EggSacDegrees += m_OurPopulationManager->GetEggDegreesInt();
        m_BadHabitatDays = 0;
        break;
    case 3:
        m_EggSacDegrees += m_OurPopulationManager->GetEggDegreesGood();
        m_BadHabitatDays = 0;
        break;
    }
    if (m_EggSacDegrees > m_EggProducConst) {
        if (ProduceEggSac()) {
            ZeroEggSacDegrees();
            if (m_EggsProduced > m_Max_Egg_Production) {
                return 0; // Die
            }
        }
    }
    return 1;
}
//---------------------------------------------------------------------------------------------------------------------------------------
bool Spider_Female::ProduceEggSac() {
    int NoEggs = CalculateEggsPerEggSac();
    if (NoEggs > 0) {
        if (m_OurPopulationManager->m_EggPosMap->GetMapValue(m_Location_x, m_Location_y)) {
            /**
            *     This version has maximum density dependence and may need to be overridden in descendent classes
            */
            m_EggsProduced += NoEggs; // They are dead but she produced them
            return false;
        }
        else {
            CreateEggSac(NoEggs);
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------------------------

void Spider_Female::CreateEggSac(int a_NoEggs)
{
    struct_Spider* SS;
    SS = new struct_Spider;
    SS->x = m_Location_x;
    SS->y = m_Location_y;
    SS->L = m_OurLandscape;
    SS->SpPM = m_OurPopulationManager;
    SS->noEggs = a_NoEggs;
    m_OurPopulationManager->CreateObjects(tspi_Egg, this, SS, 1);
    m_MustBalloon = true; //**per** line to prevent females to produce more than one eggsac in one spot
    m_EggsProduced += a_NoEggs;
    m_OurPopulationManager->m_EggPosMap->SetMapValue(m_Location_x, m_Location_y);
    delete SS;
}
//---------------------------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------START class Spider_Population_Manager-----------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

Spider_Population_Manager::Spider_Population_Manager(Landscape* p_L, int N) : Population_Manager(p_L, N)
{
    m_AdultPosMap = new SimplePositionMap(m_TheLandscape);
    m_JuvPosMap = new SimplePositionMap(m_TheLandscape);
    m_EggPosMap = new SimplePositionMap(m_TheLandscape);
    // Other variables
    m_MinWalkTemp = false;
    // Load List of Animal Classes
    m_ListNames[0] = "Egg";
    m_ListNames[1] = "Juvenile";
    m_ListNames[2] = "Female";
    m_ListNameLength = 3;

    // Load State Names
    StateNames[tosps_Initiation] = "Initiation";
    // Egg
    StateNames[tosps_Develop] = "Developing";
    StateNames[tosps_Hatch] = "Hatching";
    StateNames[tosps_Dying] = "Dying";
    // Juvenile
    StateNames[tosps_Develop] = "Developing";
    StateNames[tosps_JBalloon] = "Ballooning";
    StateNames[tosps_AssessHabitat] = "AssessHabitat";
    StateNames[tosps_Mature] = "Maturation";
    StateNames[tosps_Dying] = "Dying";
    // Female
    StateNames[tosps_Reproduce] = "Reproduce";
    StateNames[tosps_FBalloon] = "Ballooning";
    StateNames[tosps_Dying] = "Dying";
    // Destroy
    StateNames[tosps_Destroy] = "Destroy";
    // Ensure that spider's execution order is shuffled after each time step
    // or do nothing if that is prefered
    BeforeStepActions[0] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
    BeforeStepActions[1] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
    BeforeStepActions[2] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing

    m_BallooningWeather = false;

    // Zero the egg degrees
    for (int i = 0; i < 365; i++) m_EggDegrees[i] = 0;
    // 3. Dispersal Time
    int count = 0;
    for (int i = 0; i < 52; i++)
        for (int j = 0; j < 7; j++)
        {
            BallooningHrs[count++] = BallooningHours[i];
        }
    // Zero the rain counter
    m_DaysSinceRain = 0;

    // Default set spiders to walking
    m_WalkingOnly = true;
    // Set out value for juv mort
    m_EggProdThresholdPassed = false;
    m_EggProdDDegsPoor = 0;
    m_EggProdDDegsInt = 0;
    m_EggProdDDegsGood = 0;
#ifdef __RECORD_RECOVERY_POLYGONS
    /* Open the output file and append */
    ofstream ofile("RecoveryPolygonsCounter.txt", ios::out);
    ofile << "This file records the number of females in each polygon each day" << endl;
    ofile.close();
    /* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
    ifstream ifile("RecoveryPolygonsList.txt", ios::in);
    int n;
    ifile >> n;
    m_RecoveryPolygons[0] = n;
    for (int i = 0; i < n; i++) ifile >> m_RecoveryPolygons[1 + i];
    for (int i = 0; i < n; i++) m_RecoveryPolygonsC[1 + i] = 0;
    ifile.close();
#endif
#ifdef __SpidDebug
    for (int i=0; i<10; i++) m_Deaths[i] = 0;
    m_JDeaths = 0;
    m_Mature = 0;
#endif
}

Spider_Population_Manager::~Spider_Population_Manager()
{
    delete m_AdultPosMap;
    delete m_JuvPosMap;
    delete m_EggPosMap;
}

void Spider_Population_Manager::Init()
{
    ;
}

bool Spider_Population_Manager::InSquare(int p_x, int p_y, int p_sqx, int p_sqy, int p_range)
{
    // Simply checks if co-ordinates p_x,p_y are in the square formed by
    // p_sqx,p_sqy,p_sqx+p_range,p_sqy+p_range
    // The function is long but reasonably efficient - I think

    int x_extent = p_sqx + p_range;
    int y_extent = p_sqy + p_range;
    if (x_extent >= SimW)
    {
        if (y_extent >= SimH)  // overlaps TR corner of sim area
        {
            // Top right square (limited by SimAreaHeight & SimAreaWidth
            if ((p_x >= p_sqx) && (p_y >= p_sqy)) return true;
            // Top Left Square (limited by 0,SimAreaHeight)
            if ((p_y >= p_sqy) && (p_y < y_extent - SimH)) return true;
            // Bottom Left square (limited by 0,0)
            if ((p_x >= x_extent - SimW) &&
                (p_y < y_extent - SimH)) return true;
            // Bottom Right square (limited by SimAreaWidth,0)
            if ((p_x >= p_sqx) && (p_y < y_extent - SimH)) return true;

        }
        else // Overlaps the west edge of the sim area
        {
            if ((p_x >= p_sqx) && (p_y >= p_sqy) && (p_y < y_extent)) return true;
            if ((p_x < x_extent - SimW) && (p_y >= p_sqy) && (p_y < y_extent)) return true;
        }
    }
    else
    {
        if (y_extent >= SimH) // overlaps top of simulation area
        {
            if ((p_x >= p_sqx) && (p_x < x_extent) &&
                (p_y >= p_sqy)) return true;
            if ((p_x >= p_sqx) && (p_x < x_extent) &&
                (p_y < y_extent - SimH)) return true;
        }
        else // territory does not overlap end of simulation area
        {
            if ((p_x >= p_sqx) && (p_x < x_extent) &&
                (p_y >= p_sqy) && (p_y < y_extent)) return true;
        }
    }
    return false; // not in territory
}
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------END class Spider_Population_Manager-------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

