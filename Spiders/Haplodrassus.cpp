/*
*******************************************************************************************************
Copyright (c) 2019, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Haplodrassus.cpp
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file Haplodrassus.cpp
Version of  07 April 2021 \n
By Luis Amaro OLIVESIM Team \n
*/


#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PositionMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Spiders/Spider_BaseClasses.h"
#include "../Spiders/Haplodrassus.h"
#include "../Spiders/Haplodrassus_Population_Manager.h"

// Files Used to debbug the code
// They are created inside the same folder where ALMaSS_debug is
ofstream _Debugger { "_DEBUGGER", ios::out | ios::trunc}; // Haplodrassus_Female class Debugger file
ofstream _Debugger_Egg { "_DEBUGGER_EGG", ios::out | ios::trunc}; // Haplodrassus_Egg class Debugger file
ofstream _Debugger_Juvenile { "_DEBUGGER_JUVENILE", ios::out | ios::trunc }; // Haplodrassus_Juvenile class Debugger file 

extern MapErrorMsg* g_msg;

using namespace std;

const int HaplodrassusEggsPerSac[13] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

/* Female Static Member Atributes Initialization */
// 29/10/2020 - We added a restriction to the Eggsac production during Reproduction
/*1 Female can only produce 1 Eggsac (contains 10 Eggs)
  NOTE: There is chance that this could be changed because it is very debatable the hypothesis of the female producing 2 Eggsacs. */
int m_Max_Egg_Production = 10;

// 06/11/2020
/* Eggsac Development Simulation Variables */
int m_EggDevelConst = 320; // Day Degrees that the eggs inside the eggsac need to accumalate in order to hatch

// 02/12/2020
const double m_MoultingMort = 0.60; // 60% Chance of Juvenile Mortality
// Daily Mortality contains all the events that can kill the Haplodrassus that are considered non-important to be programmed
const double m_DailyMortality = 0.001; // 0.1% for Eggsacs and Juveniles
const double m_Female_DailyMortality = 0.0005; // 0.05% for Females

// Female OverWintering Threshold
const int m_OverWinterThreshold1 = 700; // Threshold for the first Overwintering Period - 1 January to 21 March
const int m_OverWinterThreshold2 = 783; // Threshold for the second Overwintering Period - 21 December to 21 March (Subsequent Year)

// Lower Threshold (Soil Temperature - TS) for Juvenile Development
const int m_DevelopThreshold = 5; // 5 �C is the lower temperature threshold for development

// Lower Threshold For Movement
const int m_lower_threshold_mov = 4;
// Upper Threshold For Movement
const int m_upper_threshold_mov = 32;

//---------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------- START class Haplodrassus_Egg -------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
Haplodrassus_Egg::Haplodrassus_Egg(int a_x, int a_y, Landscape* p_L, Haplodrassus_Population_Manager* p_NPM, int a_noEggs) : Spider_Egg(a_x, a_y, p_L, p_NPM, a_noEggs)
{
    m_Egg_Devel_Degrees = 0; // accumulates degrees for eggsac development
}

Haplodrassus_Egg::~Haplodrassus_Egg()
{
}


// 18/11/2020
void Haplodrassus_Egg::BeginStep() {
    auto day = m_OurLandscape->SupplyDayInYear(); // gets the day
    _Debugger_Egg << "[ BeginStep() -> Simulation Day: " << day << " ]" <<  endl;
}


// 17/11/2020
void Haplodrassus_Egg::Step() {
    
    // if the egg has already performed the Step () method for that day (m_StepDone = true) 
    // OR if it is destroyed (m_CurrentStateNo = -1) 
    if (m_StepDone || m_CurrentStateNo == -1) return; // The Egg skips the execution of this method
#ifdef __CJTDebug_5
    if (IsAlive() != 0x0DEADC0DE) DEADCODEError();
#endif

    _Debugger_Egg << "[ Step() -> Method Call ]" << endl;
    switch (m_CurrentSpState) { // The behavior are tested
    case tosps_Initiation: // Initial -> The Eggsac is laid
        _Debugger_Egg << "  -> case tosps_Initiation -> m_CurrentSpState = tosps_Develop " << endl;
        m_CurrentSpState = tosps_Develop;
        break;

    case tosps_Develop: // Develop Behavior
        _Debugger_Egg << "  -> case tosps_Develop -> m_CurrentSpState = tosps_Develop " << endl;
        switch (st_Develop()) { // Tests the possible outcomes from this behaviour
        case tosps_Dying:
            _Debugger_Egg << " case tosps_Develop -> switch( st_Develop() ) -> m_CurrentSpState = tosps_Dying." << endl;
            m_CurrentSpState = tosps_Dying;
            break;
        case tosps_Hatch:
            _Debugger_Egg << " case tosps_Develop -> switch( st_Develop() ) -> m_CurrentSpState = tosps_Hatch." << endl;
            m_CurrentSpState = tosps_Hatch;
            break;
        case tosps_Develop:
            _Debugger_Egg << " case tosps_Develop -> switch( st_Develop() ) -> m_CurrentSpState = tosps_Develop." << endl;
            m_CurrentSpState = tosps_Develop;
            m_StepDone = true;
            break;
        }
        m_StepDone = true;
        break;

    case tosps_Hatch: // Hatch Behavior
        _Debugger_Egg << "  -> case tosps_Hatch -> m_CurrentSpState = tosps_Hatch " << endl;
        st_Hatch();
        m_StepDone = true;
        break;

    case tosps_Dying:
        _Debugger_Egg << "  -> case tosps_Dying -> m_CurrenttSpState = tosps_Dying " << endl;
        st_Die();
        m_StepDone = true;
        break;

    case tosps_Destroy: // Only used when an object is deleted effectively without passing through dying, e.g. egg object on hatch
        _Debugger_Egg << "  -> case tosps_Destroy -> m_CurrenttSpState = tosps_Destroy " << endl;
        m_StepDone = true;
        break;

    default:
        m_OurLandscape->Warn("Haplodrassus Egg - unknown state", NULL);
        exit(1);
    }
    _Debugger_Egg << "[ Step() -> Method Call Ended ]\n" << endl;
}


//------------------------------------------------- DEVELOP -----------------------------------------------------------------------------

// 18/11/2020
/* The m_OurPopulationManager->GetEggDevelDegrees(m_DateLaid) is not present in the Spider_Population_Manager,
   but i was able to see its defenition on the ODox for the Spider_Population_Manager. 
   Given the fact that this method is not defined in the Pop. Man. class for our spider, and that returns always 0,
   we will do a "simulation" fo the Egg Development [something similar to the Female st_Protect() behavior] */
TTypesOfSpiderState Haplodrassus_Egg::st_Develop() {
    _Debugger_Egg << "  [ st_Develop() - Method Call ]" << endl;

    // Get Day of the Year
    auto day = m_OurLandscape->SupplyDayInYear();
    // Get Soil Temperature of a Specific Day
    auto soil_temperature = m_OurLandscape->SupplySoilTemp(day);

    // This if is used to fix a bug that appeard in some egg objects
    // Some Eggsac objects when initialized, had the value of the variable m_Egg_devel_Degrees set to 420,
    // even thought the constructor sets its value to 0
    if (this->m_Egg_Devel_Degrees > m_EggDevelConst) {
        _Debugger_Egg << "      -> m_Egg_Devel_Degrees = " << m_Egg_Devel_Degrees << endl;
        this->m_Egg_Devel_Degrees = 0;
        _Debugger_Egg << "      -> m_Egg_Devel_Degrees > m_EggDevelConst : Reseting value to 0." << endl;
    }

    // All eggsacs accumulate the difference between the lower threshold and the soil temperature
    this->m_Egg_Devel_Degrees += (soil_temperature - m_DevelopThreshold );

    // Debugging
    _Debugger_Egg << "      -------------------------------------------- " << endl;
    _Debugger_Egg << "      -> Ts = " << soil_temperature << endl;
    _Debugger_Egg << "      -> Ts inf = " << m_DevelopThreshold << endl;
    _Debugger_Egg << "      -> Ts - Ts inf = " << soil_temperature - m_DevelopThreshold << endl;
    _Debugger_Egg << "      -> Eggsac has accumulated " << this->m_Egg_Devel_Degrees << " DD." << endl;
    _Debugger_Egg << "      -------------------------------------------- " << endl;

    // Check for Mortality
    if (g_rand_uni() < m_DailyMortality) {
        _Debugger_Egg << "      -> Date Laid : " << m_DateLaid << " - Eggsac died while developing." << endl;
        return tosps_Dying;
    }

    // Development
    if (this->m_Egg_Devel_Degrees >= m_EggDevelConst) { // Are we ready to hatch?
        _Debugger_Egg << "      -> m_Egg_Devel_Degrees = " << this->m_Egg_Devel_Degrees << endl;
        _Debugger_Egg << "      -> Date Laid: " << m_DateLaid << " -> Eggsac Developed." << endl;
        _Debugger_Egg << "  [ st_Develop() - Method Call Ended]" << endl;
        return tosps_Hatch; // Yes - go to hatch
    }
    else // No - Continue Developing
    {
        _Debugger_Egg << "      -> Date Laid: " << m_DateLaid << " - Day Degrees Accumulated: " << this->m_Egg_Devel_Degrees << endl;
        _Debugger_Egg << "  [ st_Develop() - Method Call Ended]" << endl;
        return tosps_Develop; // carry on developing
    }

}


//--------------------------------------------------- HATCH -----------------------------------------------------------------------------

// 20/11/2020

// Describes hatch behavioural state
void Haplodrassus_Egg::st_Hatch() {
    _Debugger_Egg << "[ st_Hatch() - Method Call ]" << endl;

    _Debugger_Egg << " -> Date Laid: " << m_DateLaid << endl;
    // Hatch Method is called
    Hatch(m_OurPopulationManager->GetEggSacSpread(), m_OurPopulationManager->GetDoubleEggSacSpread(), m_NoEggs, m_HatDensityDepMortConst);
    m_OurPopulationManager->m_EggPosMap->ClearMapValue(m_Location_x, m_Location_y); // eggsac position is cleared
    m_CurrentSpState = tosps_Destroy; // Destroys object
    _Debugger_Egg << " -> m_CurrentStateNo = " << m_CurrentStateNo << endl;
    m_CurrentStateNo = -1; // Destroys object
    _Debugger_Egg << " -> m_CurrentStateNo = " << m_CurrentStateNo << endl;
    

    _Debugger_Egg << "[ st_Hatch() - Method Call Ended ]" << endl;
}


/* Determines the number and location of spiderlings to survive hatching and triggers creation of juvenile objects */
void Haplodrassus_Egg::Hatch(int a_eggsackspread, unsigned a_doubleeggsacspread, unsigned a_noeggs, unsigned a_range) {
	_Debugger_Egg << "  [ Hatch() - Method Call ]" << endl;
	// The Juveniles will be spread over a EggSacSpread x EggSacSpread area, in order do minimize local density dependence

	int Eggs = 0; // Saves the number of eggs
	struct_Spider* SS; // creates a pointer of type struct_Spider
	SS = new struct_Spider; // creates a new object of the type struct_Spider and saves its address inside the pointer
	SS->L = m_OurLandscape; // landscape atribute
	SS->SpPM = m_OurPopulationManager; // population manager
	// do this to avoid testing from wrap around
	// - always moves co-ords away from edge
	int mx = m_Location_x; // save the value of x in a variable
	int my = m_Location_y; // save the value of y in a variable
	if (mx < a_eggsackspread) mx = a_eggsackspread; 
	if (my < a_eggsackspread) my = a_eggsackspread;
	if (mx + a_eggsackspread >= m_SimW)  mx = m_SimW - (a_eggsackspread + 1); // checks if we are too near the edge
	if (my + a_eggsackspread >= m_SimH)  my = m_SimH - (a_eggsackspread + 1); // checks if we are too near the edge
	int tx, ty;
	int tries = 0;
	while ((Eggs < a_noeggs) & (tries < 500)) {
		tries++;
		tx = mx + random(a_doubleeggsacspread) - a_eggsackspread;
		ty = my + random(a_doubleeggsacspread) - a_eggsackspread;
		if (m_OurPopulationManager->m_JuvPosMap->GetMapValue(tx, ty) == 0) { // is the location free?
			m_OurPopulationManager->m_JuvPosMap->SetMapValue(tx, ty);  // Yes - place the juvenile there
			SS->x = tx;
			SS->y = ty;
			m_OurPopulationManager->CreateObjects(tspi_Spiderling, this, SS, 1); // Juvenile object is created
			_Debugger_Egg << "      -> Juvenile Object Created: (" << SS->x << ";" << SS->y << ")" << endl;
			Eggs++;
		}
	}
	_Debugger_Egg << "  [ Hatch() - Method Call Ended ]" << endl;
}



//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- END class Haplodrassus_Egg ------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------- START class Haplodrassus_Spiderling -----------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
Haplodrassus_Spiderling::Haplodrassus_Spiderling(int a_x, int a_y, Landscape* p_L, Haplodrassus_Population_Manager* p_NPM) : Spider_Juvenile(a_x, a_y, p_L, p_NPM)
{
    m_AgeDegrees = 0; // Age of the juvenile in degrees
}



Haplodrassus_Spiderling::~Haplodrassus_Spiderling()
{
}



void Haplodrassus_Spiderling::BeginStep() {
    auto day = m_OurLandscape->SupplyDayInYear();
    _Debugger_Juvenile << "[ BeginStep() - Simulation Day: " << day << " ] " << endl;
}



void Haplodrassus_Spiderling::Step() {
    // if the egg has already performed the Step () method for that day (m_StepDone = true) 
    // OR if it is destroyed (m_CurrentStateNo = -1) 
    if (m_StepDone || m_CurrentStateNo == -1) return; // The Juvenile skips this method 
#ifdef __CJTDebug_5
    if (IsAlive() != 0x0DEADC0DE) DEADCODEError();
#endif

    _Debugger_Juvenile << "[ Step() - Method Call ]" << endl;
    switch (m_CurrentSpState) {
    case tosps_Initiation: // Initial - Juvenile hatchs from the Eggsac
        _Debugger_Juvenile << " -> case tosps_Initiation -> m_CurrentSpState = tosps_Develop. " << endl;
        m_CurrentSpState = tosps_Develop;
        break;

    case tosps_Develop: // Develop Behavior
        _Debugger_Juvenile << " -> case tosps_Develop -> m_CurrentSpState = tosps_Develop. " << endl;
        switch (st_Develop()) { // Tests the possible outcomes from this behaviour
        case tosps_Dying:
            _Debugger_Juvenile << " -> case tosps_Develop -> switch (st_Develop() ) - m_CurrenSpState = tosps_Dying" << endl;
            m_CurrentSpState = tosps_Dying;
            break;
        case tosps_Mature:
            _Debugger_Juvenile << " -> case tosps_Develop -> switch (st_Develop() ) - m_CurrenSpState = tosps_Mature" << endl;
            m_CurrentSpState = tosps_Mature;
            break;
        case tosps_AssessHabitat:
            _Debugger_Juvenile << " -> case tosps_Develop -> switch (st_Develop() ) - m_CurrenSpState = tosps_AssessHabitat" << endl;
            m_CurrentSpState = tosps_AssessHabitat;
            break;
        case tosps_Develop:
            _Debugger_Juvenile << " -> case tosps_Develop -> switch (st_Develop() ) - m_CurrenSpState = tosps_Develop" << endl;
            m_CurrentSpState = tosps_Develop;
            m_StepDone = true;
            break;
        }
        m_StepDone = true;
        break;

    case tosps_AssessHabitat: // Assess Habitat Behavior
        _Debugger_Juvenile << " -> case tosps_AssessHabitat -> m_CurrentSpState = tosps_AssessHabitat. " << endl;
        switch (st_AssesHabitat()) { // Tests the possible outcomes from this behaviour
        case tosps_Dying:
            _Debugger_Juvenile << " -> case tosps_AssessHabitat -> switch ( st_AssessHabitat() ) - m_CurrentSpState = tosps_Dying " << endl;
            m_CurrentSpState = tosps_Dying;
            break;
        case tosps_JWalk:
            _Debugger_Juvenile << " -> case tosps_AssessHabitat -> switch ( st_AssessHabitat() ) - m_CurrentSpState = JWalk " << endl;
            m_CurrentSpState = tosps_JWalk;
            break;
        case tosps_Develop:
            _Debugger_Juvenile << " -> case tosps_AssessHabitat -> switch ( st_AssessHabitat() ) - m_CurrentSpState = tosps_Develop " << endl;
            m_CurrentSpState = tosps_Develop;
            break;
        }
        break;

    case tosps_JWalk: // Walk Behavior 
        _Debugger_Juvenile << " -> case tosps_JWalk -> m_CurrentSpState = tosps_JWalk. " << endl;
        m_CurrentSpState = st_Walk();
        break;

    case tosps_Mature: // Mature Behavior
        _Debugger_Juvenile << " -> case tosps_Mature -> m_CurrentSpState = tosps_Mature. " << endl;
        Maturation(); // Female Object is created
        m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y); // Juvenile Map position is cleared
        m_CurrentStateNo = -1; // Juvenile Object is detroyed
        m_StepDone = true;
        break;

    case tosps_Dying: // Die Behavior
        _Debugger_Juvenile << " -> case tosps_Dying -> m_CurrentSpState = tosps_Dying. " << endl;
        m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y); // Juvenile Map position is cleared
        m_CurrentStateNo = -1; // Juvenile Object is detroyed
        m_StepDone = true;
        break;

    default:
        m_OurLandscape->Warn("Spider Juvenile - unknown state", "");
        exit(1);
    }
    _Debugger_Juvenile << "[ Step() - Method Call Ended ]\n" << endl;
}





//------------------------------------------------- DEVELOP -----------------------------------------------------------------------------

// 23/11/2020
 /*Juvenile developing depends on the soil temperature and food availability (in this version they will not be taken into account).
   We will also check for mortality associated to moulting. */

TTypesOfSpiderState Haplodrassus_Spiderling::st_Develop() {
    _Debugger_Juvenile << " [ st_Develop() - Method Call ]" << endl;

    auto day = m_OurLandscape->SupplyDayInYear();
    auto soilTemperature = m_OurLandscape->SupplySoilTemp(day);
    //_Debugger_Juvenile << "     ->  m_JuvDevelConst VALUE: " << m_JuvDevelConst << " DD. " << endl;
    _Debugger_Juvenile << "     ->  Before developing the Juvenile has accumulated " << m_AgeDegrees << " DD. " << endl;

    // Daily Degree Accumulation
    if (soilTemperature >= m_DevelopThreshold) { // If the soil temperature is bigger then the lower threshold 
        this->m_AgeDegrees += (soilTemperature - m_DevelopThreshold);
        _Debugger_Juvenile << "     ->  -------------------------------------------------------------------" << endl;
        _Debugger_Juvenile << "     ->  Ts = " << soilTemperature << endl;
        _Debugger_Juvenile << "     ->  Ts inf = " << m_DevelopThreshold << endl;
        _Debugger_Juvenile << "     ->  Ts - Ts inf  = " << soilTemperature - m_DevelopThreshold << endl;
        _Debugger_Juvenile << "     ->  -------------------------------------------------------------------" << endl;
        _Debugger_Juvenile << "     ->  After developing the Juvenile has accumulated " << m_AgeDegrees << " DD. " << endl;
    }
    
    // When the Juveniles finish developing the mortality associated with the moulting is tested
     // Check for Mortality associated to the moulting process
    if (this->m_AgeDegrees >= m_JuvDevelConst && g_rand_uni() < m_MoultingMort) {
        _Debugger_Juvenile << "     -> Juvenile died while moulting. " << endl;
        return tosps_Dying;
    }

    // Can the Juvenile Mature?
    if (this->m_AgeDegrees >= m_JuvDevelConst) {
        _Debugger_Juvenile << "     -> Juvenile will mature." << endl;
        return tosps_Mature; // YES - go to mature
    }
    else // NO
    {
        if (g_rand_uni() < 0.8) { // While the Juvenile is in development, he has a chance of assessing the habitat
            _Debugger_Juvenile << "     -> Juvenile will assess the habitat. It has accumulated " << m_AgeDegrees << " DD." << endl;
            return tosps_AssessHabitat;
        }
    }

    _Debugger_Juvenile << " [ st_Develop() - Method Call Ended ]" << endl;
    return tosps_Develop; // carry on developing
}



//------------------------------------------------- MATURATION --------------------------------------------------------------------------

// 03/12/2020
/* This version of the Maturation process does not take into account the type of the polygon because we don't have our landscape. */

void Haplodrassus_Spiderling::Maturation() {
	_Debugger_Juvenile << " [ Maturation() - Method Call ]" << endl;

	bool done = false;
    	_Debugger_Juvenile << "     -> Maturation Started. " << endl;
    	struct_Spider* aps;
	aps = new struct_Spider;
	aps->SpPM = m_OurPopulationManager;
	aps->L = m_OurLandscape;
	int x_ex, y_ex, tx, ty; // x_ex and y_ex represent the value of x and y on the map extremities
	if (m_Location_x < 5) x_ex = m_Location_x; else x_ex = 5; // These two ifs check if we are near the edges of the map, preventing our objects to spawn outside of it
	if (m_Location_y < 5) y_ex = m_Location_y; else y_ex = 5; // The spread radius is always 5x5. It is only adjusted near the edge.
	for (int a = 0; a < y_ex; a++) {
		for (int b = 0; b < x_ex; b++) {
			tx = m_Location_x - b;
			ty = m_Location_y - a;
			TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType(tx, ty);
			switch (ele) { // In this version of the code the typo of the polygon will not be taken into account, 
				// because we don't have our landscape - but we will leave it here, because its doens't affect the functioning of the method
			case tole_Pond: // Checks if the polygon/habitat is uninhabitable
			case tole_Freshwater:
			case tole_Saltwater:
			case tole_River:
            case tole_ActivePit:
            case tole_DrainageDitch:
            case tole_HeritageSite:
            case tole_Marsh:
            case tole_Railway:
            case tole_RiverBed:
            case tole_Stream:
				break;
			default:
				if (m_OurPopulationManager->m_AdultPosMap->GetMapValue(tx, ty) == 0) { // Is the location occupied?
					m_OurPopulationManager->m_AdultPosMap->SetMapValue(tx, ty); // No - Create the Juvenile
					aps->x = tx;
					aps->y = ty;
					m_OurPopulationManager->CreateObjects(tspi_Female, this, aps, 1);
					done = true;
					_Debugger_Juvenile << "     -> Maturation Completed. " << endl;
					break;
				} // Yes - Try again
			} // End of switch(ele)
			if (done == true) break;
		}
		if (done == true) break;
	}
	delete aps;

	_Debugger_Juvenile << " [ Maturation() - Method Call Ended ]" << endl;
}


//---------------------------------------------------- MOVEMENT -------------------------------------------------------------------------

// 04/12/2020

// Describes what happens when the Juvenile Walks
TTypesOfSpiderState Haplodrassus_Spiderling::st_Walk() {
    _Debugger_Juvenile << " [ st_Walk() - Method Call ]" << endl;

    m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y); // Clears map location
    _Debugger_Juvenile << "      -> Map Position is cleared. " << endl;
    switch (Walk()) { // Calls the Walk() method
        _Debugger_Juvenile << "  -> switch (Walk()): " << endl;
    case 2: // Moved to a new place
        m_OurPosMap->SetMapValue(m_Location_x, m_Location_y); // Sets the new map location
        _Debugger_Juvenile << "      -> New map position Set. " << endl;
        _Debugger_Juvenile << "  -> Juvenile - Age " << this->m_AgeDegrees << " - continues Developing.  " << endl;
        break;
    }

    _Debugger_Juvenile << " [ st_Walk() - Method Call Ended ]" << endl;
    return  tosps_Develop; // After moving, continues developing

}


// Makes the Juvenile move in a certain direction
int Haplodrassus_Spiderling::Walk() {
    _Debugger_Juvenile << "     [ Walk() - Method Call ]" << endl;

    double soil_temperature = m_OurLandscape->SupplySoilTemp();
    if (soil_temperature >= m_lower_threshold_mov && soil_temperature <= m_upper_threshold_mov) {
        _Debugger_Juvenile << "          -> Soil Temperature is between [4;32] Degrees " << endl;
        if (g_rand_uni() < 0.900) { // gave a simple probability of movement 
            _Debugger_Juvenile << "          -> g_rand_uni() < 0.900 is true " << endl;
            int Direction = m_MyDirection & -1;
            _Debugger_Juvenile << "          -> the Direction is " << Direction << endl;
            _Debugger_Juvenile << "      [ Walk() - Method Call Ended ] " << endl;
            return WalkTo(Direction);
        }
    }

    _Debugger_Juvenile << "     [ Walk() - Method Call Ended ]" << endl;
    return 1; // Success

}


// Moves the spider 2 meters in the direction given by the variable direction
int Haplodrassus_Spiderling::WalkTo(int direction) {
    _Debugger_Juvenile << "         [ WalkTo() - Method Call ]" << endl;

       //returns 2 if the movement is successfull
    int tx = m_Location_x, ty = m_Location_y; // hold the current value of the x and y coordinate respectively
    _Debugger_Juvenile << "              -> Value of x: " << m_Location_x << " - Value of y: " << m_Location_y << endl;

    // Gets the Distance based on the refuge level
    int dist = this->GetDistBasedOnRefuge();

    switch (direction) { // tests all the possible directions
    case 0: // North
        ty -= dist;
        if (ty < 0) ty += m_SimH;
        break;
    case 1: // NE
        tx += dist;
        ty -= dist;
        if (ty < 0) ty += m_SimH;
        if (tx >= m_SimW) tx -= m_SimW;
        break;
    case 2: // E
        tx += dist;
        if (tx >= m_SimW) tx -= m_SimW;
        break;
    case 3: // SE
        tx += dist;
        ty += dist;
        if (tx >= m_SimW) tx -= m_SimW;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 4: // S
        ty += dist;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 5: // SW
        tx -= dist;
        ty += dist;
        if (tx < 0) tx += m_SimW;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 6: // W
        tx -= dist;
        if (tx < 0) tx += m_SimW;
        break;
    case 7: // NW
        tx -= dist;
        ty -= dist;
        if (tx < 0) tx += m_SimW;
        if (ty < 0) ty += m_SimH;
        break;
    }
    m_Location_x = tx; // save the new x coordinate tx on m_Location_x
    m_Location_y = ty; // save the new y coordinate ty on m_location_y
    _Debugger_Juvenile << "             -> New value of x: " << m_Location_x << " - New value of y: " << m_Location_y << endl;

    _Debugger_Juvenile << "         [ WalkTo() - Method Call Ended ]" << endl;
    return 2; // Moved OK
}


// 12/03/2021
 /* This method is used to get the distance that the Juvenile can walk, based on the level of refuge.
    The higher the refuge level is the lower the distance is:
        Level 3 or 2 - 1 Step
        Level 1 or 0 - 2 Steps
         */
int Haplodrassus_Spiderling::GetDistBasedOnRefuge() {
    _Debugger_Juvenile << "              [ GetDistBasedOnRefuge - Method Call ]" << endl;

    int tx = m_Location_x; // x coordinate
    int ty = m_Location_y; // y coordinate
    TTypesOfLandscapeElement element = m_OurLandscape->SupplyElementType(tx, ty); // Get the tole of (x,y) location

    switch (element) {
        // Refuge Level 3 
    case tole_HedgeBank:
    case tole_Hedges:
    case tole_Orchard:
    case tole_PermanentSetaside:
    case tole_StoneWall:
        
        // Refuge Level 2
    case tole_Field:
    case tole_FieldBoundary:
    case tole_Heath:
    case tole_PitDisused:
    case tole_Scrub:
    case tole_UnsprayedFieldMargin:
        _Debugger_Juvenile << "               -> Refuge Level 3 or Level 2 - Walk Distance: 1 Step." << endl;
        _Debugger_Juvenile << "              [ GetDistBasedOnRefuge - Method Call Ended ]" << endl;
        return 1; // Walks 1 step
        break;

    default: // Refuge Level 1 or Level 0
        _Debugger_Juvenile << "               -> Refuge Level 0 or Level 1 - Walk Distance: 2 Steps." << endl;
        _Debugger_Juvenile << "              [ GetDistBasedOnRefuge - Method Call Ended ]" << endl;
        return 2; // Walks 5 steps

    }

}

//----------------------------------------------------- ASSESS HABITAT ------------------------------------------------------------------

/*
    10/12/2020
    Check if the Assess Habitat behavior works.
    Given the fact that we don't have the our landscape ready,
    we will do a simple test to see if the Haplodrassus Juvenile dies, moves our continues developing.
*/

TTypesOfSpiderState Haplodrassus_Spiderling::AssessHabitat() {
    _Debugger_Juvenile << " [ AssessHabitat() - Method Call ]" << endl;

    
    if (g_rand_uni() < 0.001) { // Check for mortality associated with habitat assessment
        _Debugger_Juvenile << "     -> Juvenile - Age " << this->m_AgeDegrees << " DD, died." << endl;
        _Debugger_Juvenile << " [ AssessHabitat() - Method Call Ended ]" << endl;
        return tosps_Dying;
    }
    else if (g_rand_uni() < 0.5) { // Do i want to walk?
        _Debugger_Juvenile << "     -> Juvenile - Age " << this->m_AgeDegrees << " DD, decided to walk." << endl;
        _Debugger_Juvenile << " [ AssessHabitat() - Method Call Ended ]" << endl;
        return tosps_JWalk; // Yes - Decides to Walk
    }

    else { // No - Continues Developing
        _Debugger_Juvenile << "     -> Juvenile - Age " << this->m_AgeDegrees << " DD, continues developing." << endl;
        _Debugger_Juvenile << " [ AssessHabitat() - Method Call Ended ]" << endl;
        return tosps_Develop;
    }
}



//---------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------ END Haplodrassus_Spiderling ----------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START Haplodrassus_Female -------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
Haplodrassus_Female::Haplodrassus_Female(int a_x, int a_y, Landscape* p_L, Haplodrassus_Population_Manager* p_NPM) : Spider_Female(a_x, a_y, p_L, p_NPM )
{
    m_CanReproduce = false; // Checks the reproductive availability
    m_Eggsacs_Produced = 0; // saves the number of eggsacs produced
    m_EggSacDegrees = 0; // accumulates degrees for eggsac development
    m_Overwinter_DDAccum = 0; // accumulates degrees for overwintering
    
}


Haplodrassus_Female::~Haplodrassus_Female()
{
}


int Haplodrassus_Female::CalculateEggsPerEggSac() {
    /**
    * Determines the number of eggs per egg sac
    */
    return HaplodrassusEggsPerSac[m_OurPopulationManager->GetTodaysMonth()];
}

//-------------------------------------------------- BEGIN STEP -----------------------------------------------------------------------------

// 20/10/2020
void Haplodrassus_Female::BeginStep() {
    auto day = m_OurLandscape->SupplyDayInYear(); // gets the day
    _Debugger << "[ BeginStep() - Simulation Day Number " << day << " ]" << endl;
}

//-------------------------------------------------------- STEP -----------------------------------------------------------------------------

// 04/11/2020

void Haplodrassus_Female::Step() {
    // if the female has already performed the Step () method for that day (m_StepDone = true) 
    // OR if the female object is destroyed (m_CurrentStateNo = -1) 
    if (m_StepDone || m_CurrentStateNo == -1) return; // The female skips the execution of this method
    _Debugger << "[ Step() -> Method Call ]" << endl;
    switch (m_CurrentSpState) { // The behaviors are tested 
    
    case tosps_Initiation: // Initial state - Spider object is created
        m_CurrentSpState = tosps_FOverWintering; // state is changed to Overwintering
        _Debugger << "  -> case tosps_Initiation -> m_CurrentSpState = tosps_FOverWintering " << endl; 
        m_StepDone = true;
        break;

    case tosps_FOverWintering: // Overwintering Behavior 
        _Debugger << "  -> case tosps_FOverWintering -> m_CurrenttSpState = tosps_FOverWintering " << endl;
        switch (st_OverWintering()) { // Tests the possible outcomes from this behavior
        case tosps_Dying:
            m_CurrentSpState = tosps_Dying;
            _Debugger << "  -> case tosps_FOverWintering -> switch( st_OverWintering() ) -> m_CurrenttSpState = tosps_Dying " << endl;
            break;
        case tosps_AssessHabitat:
            m_CurrentSpState = tosps_AssessHabitat;
            m_StepDone = true;
            _Debugger << "  -> case tosps_FOverWintering -> switch( st_OverWintering() ) -> m_CurrenttSpState = tosps_AssessHabitat " << endl;
            break;

        }
        m_StepDone = true;
        break;

    case tosps_AssessHabitat: // 05/11/2020 - AssessHabitat behaviour 
        _Debugger << "  -> case tosps_AssessHabitat -> m_CurrentSpState = tosps_AssessHabitat " << endl;
        switch (AssessHabitat()) { // Tests the possible outcomes from this behavior
        case tosps_Dying:
            _Debugger << "  -> case tosps_AssessHabitat -> switch( AssessHabitat() ) -> m_CurrenttSpState = tosps_Dying " << endl;
            m_CurrentSpState = tosps_Dying;
            break;
        case tosps_FWalk:
            _Debugger << "  -> case tosps_AssessHabitat -> switch( AssessHabitat() ) -> m_CurrenttSpState = tosps_FWalk " << endl;
            m_CurrentSpState = st_Walk();
            m_StepDone = true;
            break;
        case tosps_Reproduce:
            _Debugger << "  -> case tosps_AssessHabitat -> switch( AssessHabitat() ) -> m_CurrenttSpState = tosps_Reproduce " << endl;
            m_CurrentSpState = tosps_Reproduce;
            m_StepDone = true;
            break;
        case tosps_FOverWintering: // 04/03/2021 - Test to see if the female will overwinter in day 355
            _Debugger << "  -> case tosps_AssessHabitat -> switch( AssessHabitat() ) -> m_CurrenttSpState = tosps_FOverWintering " << endl;
            m_CurrentSpState = tosps_FOverWintering;
            m_StepDone = true;
            break;
        }
        m_StepDone = true;
        break;

    case tosps_Reproduce: // Reproduce behaviour 
        _Debugger << "  -> case tosps_Reproduce -> m_CurrentSpState = tosps_Reproduce " << endl;
        switch (st_Reproduce()) { // Tests the possible outcomes from this behavior
        case 1:
            m_CurrentSpState = tosps_Protect;
            m_StepDone = true;
            _Debugger << "  -> case tosps_Reproduce -> switch (st_Reproduce()) -> m_CurrentSpState = tosps_Protect " << endl;
            break;
        case 2:
            m_CurrentSpState = tosps_FWalk;
            m_StepDone = true;
            _Debugger << "  -> case tosps_Reproduce -> switch (st_Reproduce()) -> m_CurrentSpState = tosps_FWalk " << endl;
            break;
        case 3:
            m_CurrentSpState = tosps_AssessHabitat;
            _Debugger << "  -> case tosps_Reproduce -> switch (st_Reproduce()) -> m_CurrentSpState = tosps_AssessHabitat " << endl;
            break;
        }
        m_StepDone = true;
        break;

    case tosps_Protect: // 06/11/2020 - Describes the protection behavior
        _Debugger << "  -> case tosps_Protect -> m_CurrenttSpState = tosps_Protect " << endl;
        switch (st_Protect()) { // Tests the possible outcomes from this behavior
        case tosps_Protect:
            _Debugger << "  -> case tosps_Protect -> switch( st_Protect() ) -> m_CurrenttSpState = tosps_Protect " << endl;
            m_CurrentSpState = tosps_Protect;
            m_StepDone = true;
            break;
        case tosps_FWalk:
            _Debugger << "  -> case tosps_Protect -> switch( st_Protect() ) -> m_CurrenttSpState = tosps_FWalk " << endl;
            m_CurrentSpState = st_Walk();
            m_StepDone = true;
            break;
        }
        m_StepDone = true;
        break;

    case tosps_FWalk: // Walk Behavior
        _Debugger << "  -> case tosps_FWalk -> m_CurrenttSpState = tosps_FWalk " << endl;
        m_CurrentSpState = st_Walk();
        m_StepDone = true;
        break;

    case tosps_Dying: // Does what need to be done to kill the female
        _Debugger << "  -> case tosps_Dying -> m_CurrenttSpState = tops_Dying " << endl;
        m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y); // The map position of the object is cleared
        m_CurrentStateNo = -1; // The object is destroyed
        m_StepDone = true;
        break;

    default:
        m_OurLandscape->Warn("Spider_Female - unknown state", "");
        exit(1);
    }
    _Debugger << "[ Step() -> Method Call Ended. ]\n" << endl;
}

//--------------------------------------------------- END STEP -----------------------------------------------------------------------------

// 04/03/2021
void Haplodrassus_Female::EndStep() {
    //_Debugger << "[ EndStep() -> Method Call. ]" << endl;

    /* Reset to 0 the value of the variable m_Overwinter_DDAccum in order to use it in the second overwintering period, 
       instead of creating a new variable to count the DD accumulation for that said period*/
    auto day = m_OurLandscape->SupplyDayInYear(); // Get the day of the year
   // auto year = m_OurLandscape->SupplyYearNumber();
   // _Debugger << "  -> Day of Simulation -> " << day << endl;
    //_Debugger << "  -> Year of Simulation -> " << year << endl;
    if (day == 81) { 
        //_Debugger << "  -> m_Overwinter_DDAccum reseted. " << endl;
        m_Overwinter_DDAccum = 0; // resets the value to 0 when we end the first overwintering period
    } 

    //_Debugger << "[ EndStep() -> Method Call Ended. ]\n" << endl;
}

//--------------------------------------- FEMALE OVERWINTERING -----------------------------------------------------------------------------

// 09/03/2021 
    /* The Overwintering behavior is the first behavior to be called when the simulation starts.
       There are 2 OverWintering Periods:
        -> 1 January to 21 March: can be stoped if we pass day 80 or if the day degree accumulation is greater than 667 DD. 
        -> 21 December to 21 March (Subsequent Year): can be stoped if we pass day 80 or if the day degree accumulation is greater than 695 DD. 
       When overwintering is done, the Female will assess the habitat to decide what to do next. */
    
TTypesOfSpiderState Haplodrassus_Female::st_OverWintering() {
    _Debugger << "      [ st_OverWintering() - Method Call ]" << endl;

	auto day = m_OurLandscape->SupplyDayInYear(); // Gets the day of the year
	auto year = m_OurLandscape->SupplyYearNumber(); // Gets number of the year
	
    
	if (year == 1 && (day >= 1 && day <= 80)) { // First OverWintering Period - 1 January to 21 March
		_Debugger << "      -> Simulation Day = " << m_OurLandscape->SupplyDayInYear() << " First OverWintering Period [1 January to 21 March] " << endl;
		OverWinterMort(); // Check for OverWinter mortality
		m_Overwinter_DDAccum += m_OurLandscape->SupplySoilTemp(day); // Accumulate the soil temperature of that specific day
		_Debugger << "      -> Day Degree Accumulation = " << m_Overwinter_DDAccum << endl;
		if (m_Overwinter_DDAccum > m_OverWinterThreshold1) { // tests to see if day degree accumulation is greater than the threshold
			_Debugger << "      -> Female Stoped Overwintering Due to DD Accum." << endl;
			return tosps_AssessHabitat; // Assess the habitat
		}
	}

	// Second OverWintering Period - 21 December (day 355) to 21 March (subsequent year)
	else if ((year == 1 && day >= 355) || (year != 1 && (day >= 0 && day <= 80)) || (year != 1 && (day >= 355))) { 
		_Debugger << "      -> Simulation Day = " << m_OurLandscape->SupplyDayInYear() << " Second OverWintering Period [22 December to 21 March] " << endl;
		OverWinterMort(); // Check for OverWinter mortality
		m_Overwinter_DDAccum += m_OurLandscape->SupplySoilTemp(day); // Accumulate the soil temperature of that specific day
		_Debugger << "      -> Day Degree Accumulation = " << m_Overwinter_DDAccum << endl;
		if (m_Overwinter_DDAccum > m_OverWinterThreshold2) { // tests to see if day degree accumulation is greater than the threshold
			_Debugger << "      -> Female Stoped Overwintering Due to DD Accum." << endl;
			return tosps_AssessHabitat; // Assess the habitat
		}
	}

	else { // If the Spider stops Overwintering by reaching the end of the of the overwintering period, she is going to assess the habitat
		_Debugger << "      -> Simulation Day: " << day << " - Day Degree Accumulation = " << m_Overwinter_DDAccum << endl;
		_Debugger << "      -> Spider stoped Overwintering -> Starts assessing the habitat. " << endl;
		return tosps_AssessHabitat;
	}

	_Debugger << "      [ st_OverWintering() - Method Call Ended ]" << endl;
}


// 09/03/2021
    /* The method OverWinterMort() will test the mortality associated with the OverWintering behavior. */

TTypesOfSpiderState Haplodrassus_Female::OverWinterMort() {
    _Debugger << "          [ OverWinterMort() - Method Call ]" << endl;

    if (g_rand_uni() < 0.0001) { // checks for mortality - 0.0001 = 0.01%
        _Debugger << "          -> g_rand_uni() < 0.0001 = true -> Female Dies" << endl;
        _Debugger << "          [ OverWinterMort() - Method Call Ended ]" << endl;
        return tosps_Dying; // dies
    }
    else {
        _Debugger << "          -> The Female Survided " << endl;
        _Debugger << "          [ OverWinterMort() - Method Call Ended ]" << endl;
    }
}



//--------------------------------------- REPRODUCTION -----------------------------------------------------------------------------
/* 07/04/2021
    Right now the female is able to produce only 1 Eggsac per simulation year. 
    This version of the method takes into account the reproductive periods and the refuge level of tole that the female stands in.
*/

int Haplodrassus_Female::st_Reproduce() { 
    _Debugger << "      [ st_Reproduce() - Method Call ]" << endl;

    CanReproduce();
    if (m_CanReproduce && CheckToleRefugeLvl() ) { // If the Female is able to reproduce
        if (this->m_Eggsacs_Produced < m_Max_Egg_Production) { // If it is the first Eggsac to be produced
            ProduceEggSac();
            _Debugger << "      [ st_Reproduce() - Method Call Ended ]" << endl;
            return 1; // The Female protects the Eggsac - tosps_Protect
        }
        else { // If the spider trys to produce another Eggsac
            _Debugger << "      -> Can't Reproduce Anymore" << endl;
            m_CanReproduce = false; // Female wont be able to Reproduce
            _Debugger << "      [ st_Reproduce() - Method Call Ended ]" << endl;
            return 2; // Changes the State to walk
        }
    }
    else // If the female is not able to reproduce, because the reproduction period is over
        return 3; // She will assess the habitat. 
    
    _Debugger << "      [ st_Reproduce() - Method Call Ended ]" << endl;
}



// 04/11/2020
/* 
 Now, THE Eggsac production is restrained to 1 Eggsac per Female, per simulation
*/
bool Haplodrassus_Female::ProduceEggSac() {
    _Debugger << "          [ ProduceEggSac() - Method Call ]" << endl;
    _Debugger << "          -> m_Eggsacs_Produced: " << this->m_Eggsacs_Produced << endl;

    CreateEggSac(10); // Creates 1 Eggsac that contains 10 Eggs
    this->m_Eggsacs_Produced += 10;
    

    _Debugger << "          -> Eggsac Produced." << endl;
    _Debugger << "          -> m_Eggsacs_Produced: " << this->m_Eggsacs_Produced << endl;

    _Debugger << "          [ ProduceEggSac() - Method Call Ended ]" << endl;
    return true; // Eggsac production was sucessful
}


/* 28/10/2020
    If we are between the day 93 (Middle of March - Beginning of Spring) and
      181 (End of June - Beginning of Summer) the Female will be able to reproduce */
void Haplodrassus_Female::CanReproduce() {
	_Debugger << "          [ CanReproduce() - Method Call ]" << endl;
	auto day = m_OurLandscape->SupplyDayInYear(); // Get the day of the year
	// 28/10/2020
	/* There are two reproduction intervals. In each of them has a diferent probability of
	   reproduction associated to it. */
	   // [81; 120] -> 0.011 = 1.1%
	if (day >= 81 && day <= 120) {
		_Debugger << "              -> Simulation Day = " << day << " [81; 120]" << endl;
		if (g_rand_uni() < 0.011) {
			_Debugger << "              -> g_rand_uni() < 0.011 = true -> Spider Can Reproduce" << endl;
			m_CanReproduce = true;
		}
	}

	// [121; 181] -> 0.900 = 90%
	else if (day >= 121 && day <= 181) { 
		_Debugger << "          -> Simulation Day = " << day << " [121; 181]" << endl;
		if (g_rand_uni() < 0.900) {
			_Debugger << "          -> g_rand_uni() < 0.900 = true -> Spider Can Reproduce" << endl;
			m_CanReproduce = true;
		}
	}
	// 30/10/2020 - The rest of the year the reproduction probability is 0
	else {
		m_CanReproduce = false;
		_Debugger << "          -> After Day 182 Spider is not able to Reproduce" << endl;
	}

	_Debugger << "          [ CanReproduce() - Method Call Ended ]" << endl;
}


// 08/10/2020
/* Allows to create an Egg Object and places the Eggsac in a specific map location*/
void Haplodrassus_Female::CreateEggSac(int a_NoEggs) {
    _Debugger << "              [ CreateEggSac() - Method Call ]" << endl;
    struct_Spider* SH; // create a base class pointer
    SH = new struct_Spider; // create a new objct of the struct_Spider inside the pointer
    SH->x = m_Location_x-1; // value of x
    SH->y = m_Location_y-1; // value o y
    SH->L = m_OurLandscape; // landscape
    SH->SpPM = m_OurPopulationManager; // population manager
    SH->noEggs = a_NoEggs; // number of eggs inside the eggsac
    (m_OurPopulationManager)->CreateObjects(tspi_Egg, this, SH, 1);  // create the eggsac object
    m_EggsProduced += a_NoEggs; // icremente the number of egg produced
    (m_OurPopulationManager)->m_EggPosMap->SetMapValue(m_Location_x-1, m_Location_y-1);  // set a location for the Eggsac
    _Debugger << "                  -> Female Location: ("<< m_Location_x <<";" << m_Location_y << ")" << endl;
    _Debugger << "                  -> Egg Sac Location: (" << m_Location_x-1 << ";" << m_Location_y-1 << ")" << endl;
    delete SH; // delete pointer
    _Debugger << "              [ CreateEggSac() - Method Call Ended]" << endl;
}


// 16/03/2021
/* This method checks the refuge level of the current tole that the female is on. 
    Only females that are in toles with refuge level 3 may initiate reproduction. */
bool Haplodrassus_Female::CheckToleRefugeLvl() {
    _Debugger << "          [ CheckToleRefugeLvl() - Method Call ]" << endl;

    // Saves the coordenates in temporary variables
    auto tx = m_Location_x, ty = m_Location_y; 
    TTypesOfLandscapeElement element = m_OurLandscape->SupplyElementType(tx, ty); // Get the tole present in the female location

    switch (element) {
        // Refuge Level 3 
    case tole_HedgeBank:
    case tole_Hedges:
    case tole_Orchard:
    case tole_PermanentSetaside:
    case tole_StoneWall:
        _Debugger << "            -> Refuge Level 3 - Valid Tole. " << endl;
        _Debugger << "          [ CheckToleRefugeLvl() - Method Call Ended ]" << endl;
        return true; // valid tole
        break;
    default: // Refuge Level 2, 1 and 0
        _Debugger << "            -> Invalid Tole. " << endl;
        _Debugger << "          [ CheckToleRefugeLvl() - Method Call Ended ]" << endl;
        return false; // invalid tole
    }
}

//--------------------------------------- MOVEMENT -----------------------------------------------------------------------------
// 07/04/2021

// Describes how the walk behavior works.
TTypesOfSpiderState Haplodrassus_Female::st_Walk() {
    _Debugger << "  [ st_Walk() - Method Call ] " << endl;

    m_OurPosMap->ClearMapValue(m_Location_x, m_Location_y); // The location is cleared
    _Debugger << "      -> Map Position is cleared. " << endl;
    switch (Walk()) { // the Walk behavior is called
        _Debugger << "  -> switch (Walk()): " << endl;
        case 2: // Moved to a new place
            m_OurPosMap->SetMapValue(m_Location_x, m_Location_y); // New map location set
            _Debugger << "      -> New map position Set. " << endl;
            _Debugger << "  -> Female is Assessing The Habitat.  " << endl;
            break;
    }
    
    _Debugger << "  [ st_Walk() - Method Call Ended ] " << endl;
    return  tosps_AssessHabitat; // 05/11/2020 - Every time she is done walking she will assess the habitat again 
    
}


// Check for the moviment conditions and generates the direction for the female to move 
int Haplodrassus_Female::Walk() {
	_Debugger << "      [ Walk() - Method Call ] " << endl;

	double soil_temperature = m_OurLandscape->SupplySoilTemp(); // get the soil temperature
	if (soil_temperature >= m_lower_threshold_mov && soil_temperature <= m_upper_threshold_mov) { // if the soil temperature is between [4;32] Degrees
        _Debugger << "          -> Soil Temperature is between [4;32] Degrees " << endl;
		if (g_rand_uni() < 0.900) { // gave a simple probability of movement 
			_Debugger << "          -> g_rand_uni() < 0.900 is true " << endl;
			int Direction = m_MyDirection & -1; // Direction is randomly generated
			_Debugger << "          -> the Direction is " << Direction << endl;
			_Debugger << "      [ Walk() - Method Call Ended ] " << endl;
			return WalkTo(Direction);
		}
	}

	_Debugger << "      [ Walk() - Method Call Ended ] " << endl;
	return 1; // If the conditions are not good for the movement - return 1
}

/* 16/03/2021
    This method deslocates the Female x steps in a certain direction 'direction'. 
    The number of steps (distance) dependes on the refuge level (obtained in GetDistBasedOnRefuge() method)
*/
int Haplodrassus_Female::WalkTo(int direction) {
    _Debugger << "          [ WalkTo() - Method Call ] " << endl;
    /* moves the spider x meters in 'direction' direction.
       returns 2 if the movement is successfull */

    int tx = m_Location_x, ty = m_Location_y; // hold the current value of the x and y coordinate respectively
    _Debugger << "              -> Value of x: " << m_Location_x << " - Value of y: "<< m_Location_y << endl;

    // Gets the Distance based on the refuge level
    int dist = this->GetDistBasedOnRefuge();
    
    switch (direction) { // tests all the possible directions
    case 0: // North
        ty -= dist;
        if (ty < 0) ty += m_SimH; // These if's are used to prevents the females from walking outside the borders of the map
        break;
    case 1: // NE
        tx += dist;
        ty -= dist;
        if (ty < 0) ty += m_SimH;
        if (tx >= m_SimW) tx -= m_SimW;
        break;
    case 2: // E
        tx += dist;
        if (tx >= m_SimW) tx -= m_SimW;
        break;
    case 3: // SE
        tx += dist;
        ty += dist;
        if (tx >= m_SimW) tx -= m_SimW;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 4: // S
        ty += dist;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 5: // SW
        tx -= dist;
        ty += dist;
        if (tx < 0) tx += m_SimW;
        if (ty >= m_SimH) ty -= m_SimH;
        break;
    case 6: // W
        tx -= dist;
        if (tx < 0) tx += m_SimW;
        break;
    case 7: // NW
        tx -= dist;
        ty -= dist;
        if (tx < 0) tx += m_SimW;
        if (ty < 0) ty += m_SimH;
        break;
    }

    m_Location_x = tx; // save the new x coordinate tx on m_Location_x
    m_Location_y = ty; // save the new y coordinate ty on m_location_y
    _Debugger << "              -> New value of x: " << m_Location_x << " - New value of y: " << m_Location_y << endl;

    _Debugger << "          [ WalkTo() - Method Call Ended ] " << endl;
    return 2; // success

}


// 12/03/2021
 /* This method is used to get the distance that the female can walk, based on the level of refuge.
    The higher the refuge level is the lower the distance is:
        Level 3 - 1 Step 
        Level 2 - 2 or 3 Steps
        Level 1 - 4 Steps
        Level 0 - 5 Steps */
int Haplodrassus_Female::GetDistBasedOnRefuge() {
    _Debugger << "              [ GetDistBasedOnRefuge - Method Call ]" << endl;

    int tx = m_Location_x; // x coordinate
    int ty = m_Location_y; // y coordinate
    TTypesOfLandscapeElement element = m_OurLandscape->SupplyElementType(tx, ty); // Get the tole of (x,y) location

    switch (element) {
        // Refuge Level 3 
        case tole_HedgeBank:
        case tole_Hedges:
        case tole_Orchard:
        case tole_PermanentSetaside:
        case tole_StoneWall:
            _Debugger << "               -> Refuge Level 3 - Walk Distance: 1 Step." << endl;
            _Debugger << "              [ GetDistBasedOnRefuge - Method Call Ended ]" << endl;
            return 1; // Walks 1 Step
            break;
        // Refuge Level 2
        case tole_Field:
        case tole_FieldBoundary:
        case tole_Heath:
        case tole_PitDisused:
        case tole_Scrub:
        case tole_UnsprayedFieldMargin:
            _Debugger << "               -> Refuge Level 2 - Walk Distance: 2 Step." << endl;
            _Debugger << "              [ GetDistBasedOnRefuge - Method Call Ended ]" << endl;
            return 2; // Walks 2 step
            break;
        // Refuge Level 1
        case tole_BareRock:
        case tole_Building:
        case tole_Carpark:
        case tole_Coast:
        case tole_ConiferousForest:
        case tole_DeciduousForest:
        case tole_Fence:
        case tole_Garden:
        case tole_IndividualTree:
        case tole_LargeRoad:
        case tole_MixedForest:
        case tole_MownGrass:
        case tole_NaturalGrassDry:
        case tole_NaturalGrassWet:
        case tole_OrchardBand:
        case tole_Parkland:
        case tole_PermPasture:
        case tole_PermPastureLowYield:
        case tole_PermPastureTussocky:
        case tole_PlantNursery:
        case tole_Pylon:
        case tole_RiversidePlants:
        case tole_RiversideTrees:
        case tole_RoadsideSlope:
        case tole_RoadsideVerge:
        case tole_SmallRoad:
        case tole_Track:
        case tole_UrbanNoVeg:
        case tole_UrbanPark:
        case tole_UrbanVeg:
        case tole_WindTurbine:
        case tole_YoungForest:
            _Debugger << "               -> Refuge Level 1 - Walk Distance: 4 Step." << endl;
            _Debugger << "              [ GetDistBasedOnRefuge - Method Call Ended ]" << endl;
            return 4; // Walks 4 steps
            break;
        default: // Refuge Level 0
            _Debugger << "               -> Refuge Level 0 - Walk Distance: 5 Step." << endl;
            _Debugger << "              [ GetDistBasedOnRefuge - Method Call Ended ]" << endl;
            return 5; // Walks 5 steps
            
    }

}


//------------------------------------------------ ASSESS HABITAT  -----------------------------------------------------------------------

/*
    05/11/2020
    Check if the Assess Habitat behavior works.
    Given the fact that we don't have the our landscape ready,
    we will do a simple test to see if the Spider dies, moves our stays in the same place when assessing the habitat.
*/
TTypesOfSpiderState Haplodrassus_Female::AssessHabitat() {
    _Debugger << "      [ AssessHabitat() - Method Call ]" << endl;

    int day = m_OurLandscape->SupplyDayInYear();
    
    // 04/03/2021 - Test to see if the female will overwinter in day 355
    // 12/03/2021 - If the female is in a invalid tole when she is about to overwinter, she will walk way from it, assess the habitat and try to overwinter again
    if ( (day >= 1 && day <= 80) || (day >= 355) ) { 
        _Debugger << "      -> Spider Will Overwinter. " << endl;
        _Debugger << "      [ AssessHabitat() - Method Call Ended ]" << endl;
        return tosps_FOverWintering;
    }

    if (g_rand_uni() < 0.001) { // Checks for mortality
        _Debugger << "      -> Spider died assessing the Habitat. " << endl;
        _Debugger << "      [ AssessHabitat() - Method Call Ended ]" << endl;
        return tosps_Dying;
    }
    else if (g_rand_uni() < 0.8) { // Do i want to walk?
        _Debugger << "      -> Spider is now moving. " << endl;
        _Debugger << "      [ AssessHabitat() - Method Call Ended ]" << endl;
        return tosps_FWalk;
    }
    
    // 16/12/2020
    /* If the females finish overwintering due to Day Degree accumutation, they will be able
       to move through the map, until they reach the Reproductive Period - day 81 until day 181.
     */
    if (day >= 81 && day <= 181) { // Am i in the reproductive period?
        _Debugger << "      -> Spider Can Reproduce. " << endl;
        _Debugger << "      [ AssessHabitat() - Method Call Ended ]" << endl;
        return tosps_Reproduce;
    }

    
    // If the female survives while assessing the habitat and doesn't want to walk or reproduce 
    // -> She will stay in the same location.
}



//--------------------------------------------------------- PROTECT  ----------------------------------------------------------------------

/*
    06/11/2020
    "After producing an Eggsac, the females are considered immobile during the Egg development period. This is because the
    walk for hunting always ends at the same place - the place were the Eggsac is, and its duration (twilight) is inferior
    to the time step of the model.In fact, Zolnerowich and Holner (1985) found adult females near the eggsaccs. Therefore, 
    it is expected that adult females will not go beyond a certain distance from their eggsacs, being therefore confined to 
    the area very near to the eggsacs. Even when the female leaves the eggsac for hunting during twilight it is plausible that 
    it may hunt not very far from the eggsac and returns to it as soon as possible." - Haplodrassus Model - Section 2.5
*/


TTypesOfSpiderState Haplodrassus_Female::st_Protect() {
	_Debugger << "      [ st_Protect() - Method Call ]" << endl;

    // Get Day of the Year
    auto day = m_OurLandscape->SupplyDayInYear();
    // Get Soil Temperature of a Specific Day
    auto soil_temperature = m_OurLandscape->SupplySoilTemp(day);

	if (m_CanReproduce) { // m_CanReproduce = true indicates that the Eggsac is produced
        m_EggSacDegrees += (soil_temperature - m_DevelopThreshold ); // The Eggsac accumulates Degrees - needs to accumulate 320 Day Degrees
		if (m_EggSacDegrees >= m_EggDevelConst) { // Is the eggsac developed? - Yes: the female moves 
			_Debugger << "          -> The Eggsac is developed, the spider can move." << endl;
			_Debugger << "      [ st_Protect() - Method Call Ended ]" << endl;
			return tosps_FWalk;
		}
		else { // No: the female continues protecting the eggsac
			_Debugger << "          -> Eggsac In Development -> Day Degrees Accumulated: " << m_EggSacDegrees << endl;
			_Debugger << "      [ st_Protect() - Method Call Ended ]" << endl;
			return tosps_Protect; // If the Eggsac is still developing the spider continues to protect it
		}
	}

}




//---------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------ END Haplodrassus_Female --------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------



