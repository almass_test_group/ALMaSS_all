// Version of 5th May 2020
/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

//---------------------------------------------------------------------------
#ifndef Spider_allH
#define Spider_allH
//---------------------------------------------------------------------------
// Forward Declarations

class Erigone_Population_Manager;
class Landscape;
class m_JuvPosMap;
class m_AdultPosMap;
class Spider_Base;
class struct_Spider;

//---------------------------------------------------------------------------------------------------------------------------------------

class Erigone_Egg : public Spider_Egg
{
public:
    Erigone_Egg(int x, int y,Landscape* L, Erigone_Population_Manager* EPM, int Eggs);
protected:
public:
    virtual bool OnFarmEvent(FarmToDo event);
};
//---------------------------------------------------------------------------------------------------------------------------------------

class Erigone_Juvenile : public Spider_Juvenile
{
public:
    Erigone_Juvenile(int x, int y,Landscape* L, Erigone_Population_Manager* EPM);
    virtual bool OnFarmEvent(FarmToDo event);
};
//---------------------------------------------------------------------------------------------------------------------------------------

class Erigone_Female : public Spider_Female
{
public:
    Erigone_Female(int x, int y, Landscape* L, Erigone_Population_Manager* EPM);
protected:
    virtual bool OnFarmEvent(FarmToDo event);
    /** \brief Determines the number of eggs per egg sac */
    virtual int CalculateEggsPerEggSac();
};
//---------------------------------------------------------------------------------------------------------------------------------------

#endif

