#ifndef Spider_toletov_H
#define Spider_toletov_H

#include "../Landscape/ls.h"

bool spider_tole_egss_position_valid(Landscape* m_OurLandscape, int x, int y);
bool spider_tole_juvenile_maturation_valid(Landscape* m_OurLandscape, int x, int y);
bool spider_tole_lethal(Landscape* m_OurLandscape, int x, int y);
int spider_tov_index(Landscape* m_OurLandscape, int x, int y);
int spider_tole_movemap_init(Landscape* m_OurLandscape, int x, int y);
#endif