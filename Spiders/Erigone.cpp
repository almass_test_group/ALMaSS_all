// Version of 5th May 2020
/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------


#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/PositionMap.h"
#include "../Spiders/Spider_BaseClasses.h"
#include "../Spiders/Erigone.h"
#include "../Spiders/Erigone_Population_Manager.h"

//---------------------------------------------------------------------------
//                         SPIDER CONSTANTS
//---------------------------------------------------------------------------

extern CfgBool cfg_ReallyBigOutput_used;

static CfgInt cfg_SpeciesRef( "SPID_SPECIES_REF", CFG_CUSTOM, 0 ); // default=Erigone
static CfgFloat cfg_Erig_mediumplantbiomass("ERIGONE_MEDIUMPLANTBIOMASS", CFG_CUSTOM, 100.0);
static CfgFloat cfg_Erig_lowplantbiomass("ERIGONE_LOWPLANTBIOMASS", CFG_CUSTOM, 10.0);
static CfgInt cfg_Erig_ExtraBadHabMortality("ERIGONE_EXTRABADHABMORTALITY", CFG_CUSTOM, 500);

// Parameter config variables that need to beused to set static attribute values 

// Parameter values only used in Erigone code
static CfgInt cfg_Erig_Adult_FireMort( "ERIGONE_ADULT_FIREMORT", CFG_CUSTOM, 500 );
static CfgInt cfg_Erig_Juv_FireMort( "ERIGONE_JUVENILE_FIREMORT", CFG_CUSTOM, 500 );
static CfgInt cfg_Erig_Egg_FireMort( "ERIGONE_EGG_FIREMORT", CFG_CUSTOM, 500 );
static CfgInt cfg_Erig_Adult_GrazingMort( "ERIGONE_ADULT_GRAZINGMORT", CFG_CUSTOM, 40 );
static CfgInt cfg_Erig_Juv_GrazingMort( "ERIGONE_JUVENILE_GRAZINGMORT", CFG_CUSTOM, 40 );
static CfgInt cfg_Erig_Adult_GrazingBalloon( "ERIGONE_ADULT_GRAZINGBALLOON", CFG_CUSTOM, 40 );
static CfgInt cfg_Erig_Juv_GrazingBalloon( "ERIGONE_JUVENILE_GRAZINGBALLOON", CFG_CUSTOM, 40 );
static CfgInt cfg_Erig_Egg_GrazingMort( "ERIGONE_EGG_GRAZINGMORT", CFG_CUSTOM, 40 );
// PESTICIDE RESPONSES
static CfgInt cfg_Erig_Adult_InsecticideApplication( "ERIGONE_ADULT_INSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Erig_Juvenile_InsecticideApplication( "ERIGONE_JUVENILE_INSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Erig_Egg_InsecticideApplication( "ERIGONE_EGG_INSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Erig_PesticideTrialAdultTreatmentMort( "ERIGONE_PESTICIDETRIALADULTTREATMENTMORT", CFG_CUSTOM, 900 );
static CfgInt cfg_Erig_PesticideTrialJuvenileTreatmentMort( "ERIGONE_PESTICIDETRIALJUVENILETREATMENTMORT", CFG_CUSTOM, 900 );
static CfgInt cfg_Erig_PesticideTrialJuvenileToxicMort( "ERIGONE_PESTICIDETRIALJUVENILETOXICMORT", CFG_CUSTOM, 900 );
static CfgInt cfg_Erig_PesticideTrialAdultToxicMort( "ERIGONE_PESTICIDETRIALADULTTOXICMORT", CFG_CUSTOM, 900 );
//

//
static CfgInt cfg_Erig_Adult_PloughMort( "ERIGONE_ADULT_PLOUGHMORT", CFG_CUSTOM, 380 );
static CfgInt cfg_Erig_Juv_PloughMort( "ERIGONE_JUV_PLOUGHMORT", CFG_CUSTOM, 380 );
static CfgInt cfg_Erig_Egg_PloughMort( "ERIGONE_EGG_PLOUGHMORT", CFG_CUSTOM, 380 );
static CfgInt cfg_Erig_Adult_HarrowMort( "ERIGONE_ADULT_HARROWMORT", CFG_CUSTOM, 250 );
static CfgInt cfg_Erig_Juv_HarrowMort( "ERIGONE_JUV_HARROWMORT", CFG_CUSTOM, 250 );
static CfgInt cfg_Erig_Egg_HarrowMort( "ERIGONE_EGG_HARROWMORT", CFG_CUSTOM, 250 );
static CfgInt cfg_Erig_Adult_StriglingMort( "ERIGONE_ADULT_STRIGLINGMORT", CFG_CUSTOM, 370 );
static CfgInt cfg_Erig_Juv_StriglingMort( "ERIGONE_JUVENILE_STRIGLINGMORT", CFG_CUSTOM, 370 );
static CfgInt cfg_Erig_Egg_StriglingMort( "ERIGONE_EGG_STRIGLINGMORT", CFG_CUSTOM, 370 );
static CfgInt cfg_Erig_Adult_HarvestMort( "ERIGONE_ADULT_HARVESTMORT", CFG_CUSTOM, 240 );
static CfgInt cfg_Erig_Adult_HarvestBalloon( "ERIGONE_ADULT_HARVESTBALLOON", CFG_CUSTOM, 320 );
static CfgInt cfg_Erig_Juv_HarvestMort( "ERIGONE_JUVENILE_HARVESTMORT", CFG_CUSTOM, 240 );
static CfgInt cfg_Erig_Juv_HarvestBalloon( "ERIGONE_JUVENILE_HARVESTBALLOON", CFG_CUSTOM, 320 );
static CfgInt cfg_Erig_Egg_HarvestMort( "ERIGONE_EGG_HARVESTMORT", CFG_CUSTOM, 240 );

const int ErigoneEggsPerSac[13] = { 0, 0, 0, 7, 4, 5, 4, 6, 5, 5, 0, 0, 0};
//**per** changed to match fec. paper
int DroughtMort[8] = { 0,0,10,20,40,60,80,100 }; // **per** drought mortality lines

//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START class Erigone_Egg ---------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

Erigone_Egg::Erigone_Egg( int x, int y, Landscape * L, Erigone_Population_Manager * SpPM, int Eggs ) : Spider_Egg(x, y, L, SpPM, Eggs) {

}
//---------------------------------------------------------------------------------------------------------------------------------------

bool Erigone_Egg::OnFarmEvent(FarmToDo event) {
    switch (event) {
    case autumn_harrow:
    case autumn_roll:
    case autumn_sow:
    case spring_harrow:
    case spring_roll:
    case spring_sow:
    case row_cultivation:
    case hilling_up:
    case strigling_sow:
    case stubble_harrowing:
        if (random(1000) < cfg_Erig_Egg_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case autumn_plough:
    case winter_plough:
    case deep_ploughing:
    case spring_plough:
    case autumn_or_spring_plough:
        if (random(1000) < cfg_Erig_Egg_PloughMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case sleep_all_day:
    case fp_npks:
    case fp_npk:
    case fp_pk:
	case fp_k:
	case fp_p:
    case fp_liquidNH3:
    case fp_slurry:
    case fp_manganesesulphate:
    case fp_manure:
    case fp_greenmanure:
    case fp_sludge:
    case fa_npk:
    case fa_pk:
	case fa_k:
	case fa_p:
    case fa_slurry:
    case fa_ammoniumsulphate:
    case fa_manure:
    case fa_greenmanure:
    case fa_sludge:
    case herbicide_treat:
    case growth_regulator:
    case fungicide_treat:
    case glyphosate:
    case product_treat:
    case molluscicide:
        break;
    case insecticide_treat:
        if (random(1000) < cfg_Erig_Egg_InsecticideApplication.value()) m_CurrentSpState = tosps_Dying;
        break;
    case strigling:
        if (random(1000) < cfg_Erig_Egg_StriglingMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case water:
        break;
    case pigs_out:
    case cattle_out:
        if (random(1000) < cfg_Erig_Egg_GrazingMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case swathing:
    case harvest:
    case cut_to_hay:
    case cut_to_silage:
    case straw_chopping:
    case hay_turning:
    case hay_bailing:
    case cut_weeds:
    case mow:
        if (random(1000) < cfg_Erig_Egg_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case flammebehandling:
    case burn_straw_stubble:
        if (random(1000) < cfg_Erig_Egg_FireMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case trial_insecticidetreat:
    case trial_toxiccontrol:
        if (random(1000) < cfg_Erig_PesticideTrialAdultToxicMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case trial_control:
        break;
    default:
        break;
    }
    return true;
}
//---------------------------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START class Erigone_Juvenile ----------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

Erigone_Juvenile::Erigone_Juvenile( int x, int y, Landscape * L, Erigone_Population_Manager * SpPM ) :
     Spider_Juvenile( x, y, L, SpPM ) {
}
//---------------------------------------------------------------------------------------------------------------------------------------

bool Erigone_Juvenile::OnFarmEvent( FarmToDo event ) {
  switch ( event ) {
    case sleep_all_day:
    break;
    case autumn_plough:
      if ( random( 1000 ) < cfg_Erig_Juv_PloughMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case autumn_harrow:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case autumn_roll:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case autumn_sow:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case winter_plough:
      if ( random( 1000 ) < cfg_Erig_Juv_PloughMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case deep_ploughing:
      if ( random( 1000 ) < cfg_Erig_Juv_PloughMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case spring_plough:
      if ( random( 1000 ) < cfg_Erig_Juv_PloughMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case spring_harrow:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case spring_roll:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case spring_sow:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
	case fp_k:
	case fp_p:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
	case fa_k:
	case fa_p:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case glyphosate:
	case product_treat:
	  break;
    case insecticide_treat:
    case syninsecticide_treat:
      if ( random( 1000 ) < cfg_Erig_Juvenile_InsecticideApplication.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case molluscicide:
    break;
    case row_cultivation:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case strigling:
      if ( random( 1000 ) < cfg_Erig_Juv_StriglingMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case hilling_up:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case water:
    break;
    case swathing:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case harvest:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cattle_out:
      if ( random( 1000 ) < cfg_Erig_Juv_GrazingBalloon.value() ) m_MustBalloon = true;
    break;
	case cattle_out_low:
		if (random(1000) < cfg_Erig_Juv_GrazingBalloon.value()) m_MustBalloon = true;
		break;
    case cut_to_hay:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_to_silage:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case straw_chopping:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case hay_turning:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case hay_bailing:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case flammebehandling:
      if ( random( 1000 ) < cfg_Erig_Juv_FireMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case stubble_harrowing:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case autumn_or_spring_plough:
      if ( random( 1000 ) < cfg_Erig_Juv_PloughMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case burn_straw_stubble:
      if ( random( 1000 ) < cfg_Erig_Juv_FireMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case mow:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_weeds:
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestMort.value() ) m_CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Erig_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case pigs_out:
      if ( random( 1000 ) < cfg_Erig_Juv_GrazingMort.value() ) m_CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Erig_Juv_GrazingBalloon.value() ) m_MustBalloon = true;
    break;
    case strigling_sow:
      if ( random( 1000 ) < cfg_Erig_Juv_HarrowMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case trial_insecticidetreat:
      if ( random( 1000 ) < cfg_Erig_PesticideTrialJuvenileTreatmentMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case trial_toxiccontrol:
      if ( random( 1000 ) < cfg_Erig_PesticideTrialJuvenileToxicMort.value() ) m_CurrentSpState = tosps_Dying;
    break;
    case trial_control:
    break;
      // **FN**
    default:
    break;
  }
#ifdef __SpidDebug
  if (m_CurrentSpState == tosps_Dying)  m_OurPopulationManager->RecordDeath(0);
#endif
return true;
}
//---------------------------------------------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START class Erigone_Female ------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

Erigone_Female::Erigone_Female( int x, int y, Landscape * L, Erigone_Population_Manager * SpPM ) : Spider_Female( x, y, L, SpPM ) {
}
//---------------------------------------------------------------------------------------------------------------------------------------

bool Erigone_Female::OnFarmEvent(FarmToDo event) {
    switch (event) {
    case sleep_all_day:
        break;
    case autumn_plough:
        if (random(1000) < cfg_Erig_Adult_PloughMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case autumn_harrow:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case autumn_roll:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case autumn_sow:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case winter_plough:
        if (random(1000) < cfg_Erig_Adult_PloughMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case deep_ploughing:
        if (random(1000) < cfg_Erig_Adult_PloughMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case spring_plough:
        if (random(1000) < cfg_Erig_Adult_PloughMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case spring_harrow:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case spring_roll:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case spring_sow:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case fp_npks:
        break;
    case fp_npk:
        break;
    case fp_pk:
	case fp_k:
	case fp_p:
        break;
    case fp_liquidNH3:
        break;
    case fp_slurry:
        break;
    case fp_manganesesulphate:
        break;
    case fp_manure:
        break;
    case fp_greenmanure:
        break;
    case fp_sludge:
        break;
    case fa_npk:
        break;
    case fa_pk:
	case fa_k:
	case fa_p:
        break;
    case fa_slurry:
        break;
    case fa_ammoniumsulphate:
        break;
    case fa_manure:
        break;
    case fa_greenmanure:
        break;
    case fa_sludge:
        break;
    case herbicide_treat:
        break;
    case growth_regulator:
        break;
    case fungicide_treat:
        break;
    case glyphosate:
    case product_treat:
        break;
    case insecticide_treat:
    case syninsecticide_treat:
        if (random(1000) < cfg_Erig_Adult_InsecticideApplication.value()) m_CurrentSpState = tosps_Dying;
        break;
    case molluscicide:
        break;
    case row_cultivation:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case strigling:
        if (random(1000) < cfg_Erig_Adult_StriglingMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case hilling_up:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case water:
        break;
    case swathing:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case harvest:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        if (random(1000) < cfg_Erig_Adult_HarvestBalloon.value()) m_MustBalloon = true;
        break;
    case cattle_out:
        if (random(1000) < cfg_Erig_Adult_GrazingBalloon.value()) m_MustBalloon = true;
        break;
    case cut_to_hay:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        if (random(1000) < cfg_Erig_Adult_HarvestBalloon.value()) m_MustBalloon = true;
        break;
    case cut_to_silage:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        if (random(1000) < cfg_Erig_Adult_HarvestBalloon.value()) m_MustBalloon = true;
        break;
    case straw_chopping:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case hay_turning:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case hay_bailing:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case flammebehandling:
        if (random(1000) < cfg_Erig_Adult_FireMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case stubble_harrowing:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case autumn_or_spring_plough:
        if (random(1000) < cfg_Erig_Adult_PloughMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case burn_straw_stubble:
        if (random(1000) < cfg_Erig_Adult_FireMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case mow:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        if (random(1000) < cfg_Erig_Adult_HarvestBalloon.value()) m_MustBalloon = true;
        break;
    case cut_weeds:
        if (random(1000) < cfg_Erig_Adult_HarvestMort.value()) m_CurrentSpState = tosps_Dying;
        if (random(1000) < cfg_Erig_Adult_HarvestBalloon.value()) m_MustBalloon = true;
        break;
    case pigs_out:
        if (random(1000) < cfg_Erig_Adult_GrazingMort.value()) m_CurrentSpState = tosps_Dying;
        if (random(1000) < cfg_Erig_Adult_GrazingBalloon.value()) m_MustBalloon = true;
        break;
    case strigling_sow:
        if (random(1000) < cfg_Erig_Adult_HarrowMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case trial_insecticidetreat:
        if (random(1000) < cfg_Erig_PesticideTrialAdultTreatmentMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case trial_toxiccontrol:
        if (random(1000) < cfg_Erig_PesticideTrialAdultToxicMort.value()) m_CurrentSpState = tosps_Dying;
        break;
    case trial_control:
        break;
    default:
        break;
    }
    return true;
}
//---------------------------------------------------------------------------------------------------------------------------------------
int Erigone_Female::CalculateEggsPerEggSac() {
    /** 
    * Determines the number of eggs per egg sac
    */
    return ErigoneEggsPerSac[m_OurPopulationManager->GetTodaysMonth()];
}
//---------------------------------------------------------------------------------------------------------------------------------------





