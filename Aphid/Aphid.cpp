/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Aphid.cpp
\brief <B>The main source code for Aphid class</B>
*/
/**  \file Aphid.cpp
Version of  Feb 2021 \n
By Xiaodong Duan \n \n
*/

#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/ALMaSS_Random.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"
#include "../Aphid/Aphid.h"
#include "../Aphid/Aphid_Population_Manager.h"

extern MapErrorMsg *g_msg;
using namespace std;
using namespace blitz;

static CfgFloat cfg_AphidSuitabilityWeight("APHID_SUITABILITY_WEIGHT", CFG_CUSTOM, 10);
/** \brief The number of nymphs that adults can reproduce. */
static CfgFloat cfg_AphidReproductionNymphNum("APHID_REPRODUCTION_NYMPH_NUM", CFG_CUSTOM, 2);
static CfgFloat cfg_AphidEggNumPerDay("APHID_EGG_NUM_PER_DAY", CFG_CUSTOM, 3);

extern CfgStr cfg_AphidSpeciesName; 

Aphid::Aphid(int p_x, int p_y, int p_w, int p_h, Landscape* p_L, Aphid_Population_Manager* p_NPM, bool a_empty_flag, double* p_suitability, int number, int a_index_x, int a_index_y, int a_SpeciesID) : SubPopulation(p_x,  p_y, p_L, p_NPM,  a_empty_flag, a_index_x, a_index_y, p_suitability)
{
	addAnimalNumGivenStageColumn(0,0,number); //always start with eggs.
}

void Aphid::calPopuDensity(void){
	double current_biomass = m_OurLandscape->SupplyGreenBiomass(m_Location_x, m_Location_y);
	*m_popu_density = m_whole_population_in_cell/current_biomass;
}


void Aphid::calSuitability(void){
	TTypesOfLandscapeElement current_le = m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y);
	TTypesOfAphidDevelopmentSeason current_development_season = TTypesOfAphidDevelopmentSeason(m_OurPopulationManager->supplyDevelopmentSeason());
	//winter
	if (m_OurPopulationManager->isWinterHost(current_le)){		
		if (current_development_season == toAphidFallDev || current_development_season == toAphidSpringDev){
			*m_suitability = 1.0;
		}
		if (current_development_season ==  toAphidSummerDev
		 || current_development_season == toAphidHibernate){
			 *m_suitability = 0.0;
		 }
		if (current_development_season == toAphidLateSpringDev){
			*m_suitability = 0.5;
		}
	}

	//summer
	if (m_OurPopulationManager->isSummerHost(current_le)){
		if (current_development_season == toAphidSummerDev || current_development_season == toAphidLateSpringDev){
			*m_suitability = 1.0;
		}
		if (current_development_season ==  toAphidFallDev
		 || current_development_season == toAphidSpringDev){
			 *m_suitability = 0.5;
		 }
		if (current_development_season == toAphidHibernate){
			*m_suitability = 0.0;
		}
	}

	/*
	//for now, I assume the biomass is average
	double biomasscount = m_OurPopulationManager->m_TheLandscape->SupplyVegBiomass(m_Location_x,m_Location_y) * m_OurPopulationManager->supplySizeSubpopulationCell();
	if (isnan(biomasscount)){
		biomasscount=0;
	}
	
	m_plant_juice += biomasscount * 0.01;

	//biomasscount -= AphidIntake();
	double aphid_num = getTotalSubpopulation();
	if (isnan(aphid_num)){
		cout<<"NaN"<<endl;
	}
	double temp_suitabilty = cfg_AphidSuitabilityWeight.value()*m_plant_juice/(aphid_num+1);
	*m_suitability = 2*((1+exp(-1*temp_suitabilty))-0.5);
	if((*m_suitability)>1) *m_suitability=1;
	if((*m_suitability)<0) *m_suitability=0;
	//if (aphid_num <= 0) aphid_num = 1000; //no aphid in this area, try if we can put 1000 here
    //double ave_biomass = biomasscount/aphid_num;
    //if (ave_biomass < cfg_AphidMinBiomassPerAphid.value()) {m_suitability=0;return 0.0;}
	//if (ave_biomass > cfg_AphidMaxBiomassPerAphid.value()) {m_suitability=1.0;return 1.0;}
    //m_suitability = (ave_biomass-cfg_AphidMinBiomassPerAphid.value())/(cfg_AphidMaxBiomassPerAphid.value()-cfg_AphidMinBiomassPerAphid.value());
	*/
}

void Aphid::doReproduction(void){
	for (int i = toa_Aptera; i<m_OurPopulationManager->supplyLifeStageNum(); i++){
		if (i==toa_Aptera || i == toa_Alate){
			int temp_re_number = cfg_AphidReproductionNymphNum.value() * getNumforOneLifeStage(toa_Aptera);
			if (temp_re_number > 0)
			{
				addAnimalNumGivenStageColumn(toa_Nymph, m_OurPopulationManager->supplyNewestIndex(int(toa_Nymph)), temp_re_number);
				//m_OurPopulationManager->updateWholePopulationArray(toa_Nymph, temp_re_number);
				//check if it is the first time
				if (m_OurPopulationManager->supplyFirstFlagLifeStage(toa_Nymph)){
					m_OurPopulationManager->setFirstFlagLifeStage(toa_Nymph, false);
					m_OurPopulationManager->setOldIndex(toa_Nymph, m_OurPopulationManager->supplyNewestIndex(toa_Nymph));
				}
			}
		}
		//laying eggs
		if (i==toa_UnwingedFemale){
			int temp_egg_number = cfg_AphidEggNumPerDay.value() * getNumforOneLifeStage(toa_UnwingedFemale);
			if (temp_egg_number > 0){
				addAnimalNumGivenStageColumn(toa_Egg, 0, temp_egg_number);
				//m_OurPopulationManager->updateWholePopulationArray(toa_Egg, temp_egg_number);
			}
		}
	}
}

void Aphid::doMovement(void){
	for( int i = 0; i<m_OurPopulationManager->supplyLifeStageNum();i++){
		//flying for the winged ones
		if(i == toa_Alate || i == toa_WingedMale || i == toa_WingedFemale){
			if(m_population_each_life_stage(i)>0 && *m_popu_density <= 0.5){
				m_OurPopulationManager->doFlying(i, m_index_x, m_index_y);
			}	
		}
	}
}

void Aphid::doDropingWings(void){
	int current_winged_num = getNumforOneLifeStage(toa_WingedFemale);
	if(current_winged_num>0){
		m_temp_droping_wings_array = m_animal_num_array(toa_WingedFemale, blitz::Range::all());
		//std::cout<<"++++"<<current_winged_num<<"++++"<<m_temp_droping_wings_array<<std::endl;
		//std::cout<<m_animal_num_array<<std::endl;
		addAnimalNumGivenStage(toa_UnwingedFemale, &m_temp_droping_wings_array);
		m_temp_droping_wings_array = -1*m_temp_droping_wings_array;
		//std::cout<<"----"<<m_temp_droping_wings_array<<std::endl;
		addAnimalNumGivenStage(toa_WingedFemale, &m_temp_droping_wings_array);
	}
}